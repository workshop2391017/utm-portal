/* eslint-disable space-before-function-paren */
import axios from "axios";
import store from "stores/store";
import { setPopUpExpired, setPopUpErrorBE } from "stores/actions/errorGeneral";
import { setUploadProgress } from "stores/actions/uploadProgress";
import { getLocalStorage, removeAllLocalStorage } from "utils/helpers";

// import { getApiPrivilege } from "../helpers/apiPrivilege";

export const requestInterceptors = async () => {
  await axios.interceptors.request.use(
    (config) => {
      config.headers["Content-Type"] = "application/json";

      const accessToken = getLocalStorage("portalAccessToken");
      const userLogin = getLocalStorage("portalUserLogin");
      if (accessToken) {
        config.headers.Authorization = `Bearer ${accessToken}`;
      }
      if (userLogin) {
        const dataUser = JSON.parse(userLogin);
        config.headers.username = dataUser.username;
        config.headers.channel = "OP";
        if (!config.headers?.menuid) {
          config.headers.menuid = store.getState()?.menus?.menuId || 0;
        }
        // if (dataUser?.username?.includes("superadmin")) {
        config.headers.privilege = 253;
        // }
      }
      if (config?.data?.isUpload) {
        config.onUploadProgress = (data) => {
          const dataConfig = JSON.parse(config.data);
          store.dispatch(
            setUploadProgress(
              Math.round((100 * data.loaded) / data.total),
              dataConfig.fileName || "default"
            )
          );
        };
      }
      return config;
    },
    (err) => {
      Promise.reject(err);
    }
  );
};

export const responseInterceptors = async () => {
  await axios.interceptors.response.use(
    (response) => {
      const isHtml =
        response.headers["Content-Type"] === "text/html;charset=utf-8";
      if (isHtml) {
        // Store.dispatch({ type: SET_ERROR_WAF, payload: response });
      }

      // if (
      //   response.data.opstatus === 9001 ||
      //   response.data.opstatus === 8004 ||
      //   response.data.opstatus === 8005
      // ) {
      //   return Store.dispatch({ type: SET_REQUEST_TIME_OUT, payload: true });
      // }

      return response;
    },
    (error) => {
      if (!error.response)
        return {
          data: {
            errorCode: "0",
            message: error.message,
          },
        };

      if (error.response.status === 401) {
        removeAllLocalStorage();
        store.dispatch(setPopUpExpired(true));
      }

      if (
        error.response.status === 503 ||
        error.response.status === 502 ||
        error.response?.data?.message?.includes("I/O")
      ) {
        store.dispatch(setPopUpErrorBE(true));
      }

      return error.response;
    }
  );
};
