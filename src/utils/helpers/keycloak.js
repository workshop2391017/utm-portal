import Keycloak from "keycloak-js";

const keyCloakConfig = {
  url: "http://35.219.86.225/auth/",
  realm: "portal",
  clientId: "portal-client",
};

const keycloak = new Keycloak(keyCloakConfig);

export default keycloak;
