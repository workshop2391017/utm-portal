const findParentNode = (nodes, childs) => {
  let parent;
  // Loop through input array
  // Check if the id of any node is matching with the parent id
  nodes.forEach((item) => {
    if (item.id === childs.parent_id) {
      parent = item;
      // Break loop
    } else if (item.childs && item.childs.length) {
      // Check the child nodes
      const newParent = findParentNode(item.childs, childs);
      parent = newParent || parent;
      // Break loop
    }
  });
  return parent;
};

export const groupingData = (data) =>
  data.reduce((acc, curr) => {
    // Use this if you have one level of nesting.
    // const parentNode = acc.find(node => node.id === curr.parent_id);

    // Find the parent node from the accumulator list and the current node
    const parentNode = findParentNode(acc, curr);

    // If parent node is found, push the current node to the child node of parent
    // Else push the node to accumulator
    if (parentNode) {
      if (parentNode.childs) {
        parentNode.childs.push(curr);
      } else {
        parentNode.childs = [curr];
      }
    } else {
      acc.push(curr);
    }
    return acc;
  }, []);
