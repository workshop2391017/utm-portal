import { formatMoneyRMG } from "utils/helpers";

export const capitalize = (label) => {
  let string = "";
  if (label?.length > 0) {
    const words = label.toLowerCase().split(" ");

    string = words
      .map((word) => word[0].toUpperCase() + word.substring(1))
      .join(" ");
  }
  return string;
};

export const detailTransaksi = (data) => {
  const { template } = data;
  const list = [];
  if (template) {
    template.forEach((e) => {
      if (
        !["TOKENNUMBER", "TOTALAMOUNT", "TOTAL", "FOOTER"].includes(e.fieldKey)
      ) {
        if (e.fieldType === "LABELADD") {
          const checkLabel = e.value.split(":");
          if (checkLabel.length < 2) {
            list.push({
              label: e.labelEn,
              value: e?.value !== "" ? e?.value : "-",
              type: "detail",
            });
          } else {
            list.push({
              label: checkLabel[0] ?? "",
              value: checkLabel.pop() ?? "-",
              type: "detail",
            });
          }
        } else if (
          e.fieldType !== "LABELOPT" ||
          (e.fieldType === "LABELOPT" && e.value.length > 0)
        ) {
          list.push({
            label: e.labelEn,
            value: e?.value !== "" ? e?.value : "-",
            type: "detail",
          });
        }
      } else if (["TOKENNUMBER"].includes(e.fieldKey)) {
        list.push({
          label: e.labelEn,
          value: e?.value !== "" ? e?.value : "-",
          type: "detailBold",
        });
      }
    });
  }
  const totalAdmin =
    template.find((e) => e.fieldKey === "TOTALDEBITAMOUNT") ||
    list.find((e) => e.label === "TOTAL DEBIT BIAYA ADMIN ");

  if (!totalAdmin) {
    list.push({
      label: "Total Debit Admin Fees",
      value: data?.adminFee
        ? `${formatMoneyRMG(`${data.adminFee + data.transactionFee}`, "IDR")}`
        : `${formatMoneyRMG(`${0 + data.transactionFee}`, "IDR")}`,
      type: "detail",
    });
  }

  const total = template.find((e) =>
    ["TOTALAMOUNT", "TOTAL"].includes(e.fieldKey)
  );

  if (total) {
    list.push({
      label: capitalize(total.labelEn),
      value: total.value !== "" ? total.value : "-",
      type: "total",
    });
  }
  const footer = template.find((e) => e.fieldKey === "FOOTER");
  if (footer) {
    template.forEach((e) => {
      if (e.fieldKey === "FOOTER") {
        list.push({
          label: e.labelEn,
          value: e.value !== "" ? e.value : "-",
          type: "footer",
        });
      }
    });
  } else {
    list.push({
      label: "-",
      value: "-",
      type: "note",
    });
  }
  return list;
};
