import { useEffect, useState } from "react";
import { getLocalStorage } from ".";

const EDIT = "EDIT";
const VIEW = "VIEW";
const DELETE = "DELETE";
const ADD = "ADD";
const EXECUTE = "EXECUTE";

const useGetCurrentAuth = (actionsMenu) => {
  const [access, setAccess] = useState({
    isEdit: true,
    isDelete: true,
    isAdd: true,
    isView: true,
    isExecute: true,
    hirarki: {},
  });

  const handleAccessAction = (lsMenuParse) => {
    if (lsMenuParse?.privilegeChild) {
      const accessMenu = (actionsMenu || (lsMenuParse?.privilegeChild ?? []))
        .filter((x) => x.privilegeUiType)
        .map((elm) => elm.privilegeMenu.split(" ")[0]?.toUpperCase() ?? "-");

      const actions = { ...access };

      if (accessMenu.includes(VIEW)) {
        actions.isView = true;
      }
      if (accessMenu.includes(EDIT)) {
        actions.isEdit = true;
      }
      if (accessMenu.includes(DELETE)) {
        actions.isDelete = true;
      }
      if (accessMenu.includes(ADD)) {
        actions.isAdd = true;
      }
      if (accessMenu.includes(EXECUTE)) {
        actions.isExecute = true;
      }

      setAccess(actions);
    }
  };

  const handleGetAccess = async () => {
    const lsMenu = await getLocalStorage("menuAuthorization");
    const lsMenuParse = lsMenu ? JSON.parse(lsMenu) : null;

    handleAccessAction(lsMenuParse);
  };

  useEffect(() => {
    handleGetAccess();
  }, [actionsMenu]);

  return access;
};

const useGetCurrentHirarki = (category) => {
  const [hirarki, setHirarki] = useState({
    menus: [],
    actions: [],
  });

  const handleFindHirarkiPrivCategory = (lsMenuParse, key, findCategory) => {
    lsMenuParse?.privilegeChild?.forEach((elm) => {
      if (elm.privilegeChild) {
        handleFindHirarkiPrivCategory(elm, key, findCategory);
      }

      if (elm.privilegeCategory === key) {
        findCategory.push(elm);
      }
    });
  };

  const handleGetAccess = async () => {
    const lsMenu = await getLocalStorage("menuAuthorization");
    const lsMenuParse = lsMenu ? JSON.parse(lsMenu) : null;

    const findCategory = [];
    const key = category;
    handleFindHirarkiPrivCategory(lsMenuParse, key, findCategory);

    if (findCategory.length) {
      const menus = findCategory
        .filter((elm) => elm.privilegeUiType === "MENU")
        .map((elm) => elm.privilegeMenu);

      const actions = findCategory.filter(
        (elm) => elm.privilegeUiType === "ACTION"
      );

      setHirarki({
        ...hirarki,
        menus,
        actions,
      });
    }
  };

  useEffect(() => {
    handleGetAccess();
  }, []);

  return hirarki;
};

export { useGetCurrentAuth, useGetCurrentHirarki };
