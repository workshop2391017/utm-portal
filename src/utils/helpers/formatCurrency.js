export const formatCurrency = (value) => {
  const hasil = value?.toLocaleString("en-US", {
    // style: 'currency',
    // currency: 'IDR',
    // currencyDisplay: 'symbol',
    minimumFractionDigits: 2,
  });

  const data = hasil.split("").map((item) => item.replace(",", "."));

  const hasilPanjang = data.length;

  const hasilTest = data[hasilPanjang - 3];

  data.splice(hasilPanjang - 3, 1, ",");

  // .join("");

  return data.join("");
};
