import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  handlevalidationDataLms,
  setAlreadyExist,
} from "stores/actions/validatedatalms";
import useDebounce from "./useDebounce";

export const useValidateLms = (
  {
    name,
    code,
    nameCode,
    codeCode,
    payload, // for customize payload, and have to add keys in options
    // optionalValidate,
  },
  isValidateTogether = false,
  options = { pause: false, customize: false, keys: [] }
) => {
  const dispatch = useDispatch();

  const debounceName = useDebounce(name, 1100);
  const debounceCode = useDebounce(code, 1100);

  useEffect(() => {
    if (debounceName && debounceCode && isValidateTogether && !options?.pause)
      dispatch(
        handlevalidationDataLms({
          name: debounceName?.trim(),
          table: nameCode || codeCode,
          code: debounceCode?.trim(),
          key: "nameAndCode",
        })
      );
    else dispatch(setAlreadyExist({ nameAndCode: false }));
  }, [debounceName, debounceCode]);

  useEffect(() => {
    if (debounceName && !isValidateTogether && !options?.pause)
      dispatch(
        handlevalidationDataLms({
          name: debounceName?.trim(),
          table: nameCode,
          code: "",
          key: "name",
        })
      );
    else dispatch(setAlreadyExist({ name: false }));
  }, [debounceName, nameCode]);

  useEffect(() => {
    if (debounceCode && !isValidateTogether && !options?.pause)
      dispatch(
        handlevalidationDataLms({
          code: debounceCode?.trim(),
          table: codeCode,
          name: "",
          key: "code",
        })
      );
    else dispatch(setAlreadyExist({ code: false }));
  }, [debounceCode]);
};
