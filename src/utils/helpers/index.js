/* eslint-disable no-useless-escape */
import SecureLS from "secure-ls";
import moment from "moment";
import "moment/locale/id";

const ls = new SecureLS({
  encodingType: "aes",
  encryptionSecret: "SLAskwqekasdfjSJdswqke",
});

// local storage action
export const setLocalStorage = (key, value) => ls.set(key, value);

export const getLocalStorage = (key) => ls.get(key);

export const removeLocalStorage = (key) => ls.remove(key);
export const removeAllLocalStorage = () => ls.removeAll();

// session storage action
export const setSessionStorage = (key, value) =>
  sessionStorage.setItem(key, value);
export const getSessionStorage = (key) => sessionStorage.getItem(key);
export const removeSessionStorage = (key) => sessionStorage.removeItem(key);
export const removeAllSessionStorage = () => sessionStorage.clear();

export const updateParamsLocalStorage = (payload) => {
  const params = JSON.parse(getLocalStorage("detailParams"));
  const newParams = {
    ...(params || {}),
    ...payload,
  };
  setLocalStorage("detailParams", JSON.stringify(newParams));
};

/* ----------- example format date: 19 Januari 2008 ----------- */
export const standardDate = (date) =>
  date ? moment(date).format("D MMMM YYYY") : "-";

/* ----------- example format date: 19 Januari 2008 ----------- */
export const standardDateApproval = (date) =>
  date ? moment(date).format("DD MMM YYYY") : "-";

/* ----------- example format From format date: 19 Januari 2008 ----------- */
export const standardDateAuditSaving = (date) => {
  moment.locale("en");
  return date
    ? moment(date, "DD/MM/YYYY | H:mm:ss").format("DD MMMM YYYY")
    : "-";
};

/* ----------- example format date: Januari 19, 2008 ----------- */
export const fullMonthName = (date) =>
  date ? moment(date).format("MMMM D, YYYY") : "-";

/* ----------- example format date: Jan 19, 2008 ----------- */
export const initialMonthName = (date) =>
  date ? moment(date).format("MMM D, YYYY") : "-";

/* ----------- example format date: 19 Jan, 3.20 PM ----------- */
export const dateInWordsWithTime = (date) =>
  date ? moment(date).format("D MMM, H:mm") : "-";

/* ----------- example format time: 10 menit yang lalu ----------- */
const regex = /dalam/gi;
export const distanceInWordsToNow = (date) =>
  date ? moment(date).startOf("hour").fromNow().replace(regex, "") : "";

/* ----------- example format time: Mar 20 ----------- */
export const dateMonthOnly = (date) =>
  date ? moment(date).format("MMM DD") : "-";

export const monthYearOnly = (date) => {
  moment.locale(getLocalStorage("language"));
  return date ? moment(date).format("MMMM YYYY") : "-";
};

/* ----------- example format date MM-YYYY to MM-DD-YYY ----------- */
export const specificFormatToAnother = (date, currentFormat, newFormat) => {
  moment.locale(getLocalStorage("language"));
  return date ? moment(date, currentFormat).format(newFormat) : "";
};

/* ----------- example format time: 20 Mar 2018 ----------- */
export const dateMonthYear = (date) =>
  date ? moment(date).format("DD MMM YYYY") : "-";

/* ----------- example format time: 20 Mar 2018 16:14:50 ----------- */
export const dateMonthYearHours = (date) =>
  date ? moment(date).format("DD MMM YYYY H:mm:ss") : "-";

export const dateMonthYearHoursWithSlash = (date) =>
  date ? moment(date).format("DD/MM/YYYY H:mm:ss") : "-";

export const dateMonthYearHour = (date) => {
  moment.locale("en");
  return date ? moment(date).format("DD/MM/YYYY H:mm:ss A") : "-";
};

export const dateMonthsYearHour = (date) => {
  moment.locale("en");
  return date ? moment(date).format("DD MMMM YYYY H:mm:ss A") : "-";
};

/* ----------- example format date: 19 Januari 2008 ----------- */
export const standardDateByCountry = (date, countryCode) => {
  moment.locale(countryCode);
  return date ? moment(date).format("DD MMMM YYYY") : "-";
};

export const standardDateWithMinutesByCountry = (date, countryCode) => {
  moment.locale(countryCode);
  return date ? moment(date).format("DD MMMM YYYY H:mm") : "-";
};

export const standardDateWithMinutesByCountryListMessage = (
  date,
  countryCode
) => {
  moment.locale(countryCode);
  return date ? moment(date).format("DD MMM YYYY H:mm") : "-";
};

export const standardDayMountMinutesByCountry = (date, countryCode) => {
  moment.locale(countryCode);
  return date ? moment(date).format("D MMMM H:mm") : "-";
};

export const resiFormatTimesByCountry = (date, countryCode) => {
  moment.locale(countryCode);
  return date ? moment(date).format("H:mm:ss") : "-";
};

export const dateInboxMessageByCountry = (date, countryCode) => {
  moment.locale(countryCode);
  return date ? moment(date).format("DD MMMM YYYY H:mm:ss") : "-";
};

export const dateHour = (date, countryCode) => {
  moment.locale("en");
  return date ? moment(date).format("LT") : "-";
};

export const dateHMS = (date, countryCode) => {
  moment.locale(countryCode);
  return date ? moment(date).format("HH:MM:SS") : "-";
};

/* ----------- example format time: 20-03-2018 ----------- */
export const inputDate = (date) =>
  date ? moment(date).format("DDMMYYYY") : "-";

/* ----------- example format time: 20/03/2018 ----------- */
export const inputDateForText = (date) =>
  date ? moment(date).format("YYYY/MM/DD") : "";

/* ----------- example format time: 20/03/2018 ----------- */
export const inputDateForTextOnly = (date) =>
  date ? moment(date).format("DD/MM/YYYY") : "";

/* ----------- example format time: 2018-03-20 ----------- */
export const backendDate = (date) =>
  date ? moment(date).format("YYYY-MM-DD") : "";

/* ----------- example format time: 2021-11-30 11:29:03.000 ----------- */
export const searchDate = (date, format) =>
  date ? moment(date).format(format || "YYYY-MM-DD HH:mm:ss") : null;

/* ----------- example format time: 2021-11-30 11:29:03.000 ----------- */
export const searchDateStart = (date) =>
  date ? `${moment(date).format("YYYY-MM-DD")} 00:00:00` : "";

/* ----------- example format time: 2021-11-30 11:29:03.000 ----------- */
export const searchDateEnd = (date) =>
  date ? `${moment(date).format("YYYY-MM-DD")} 23:59:59` : "";

/* ----------- Expired Date Cart ----------- */
export const isExpired = (date, duration) => {
  const expireDate = moment(date, "DD-MM-YYYY")
    .add(duration, "days")
    .format("DD-MM-YYYY");

  const dueDate = moment().isAfter(expireDate, "DD-MM-YYYY", "days");
  return dueDate;
};

// general dynamic format date
export const generalDateFormat = (date, format) => moment(date).format(format);

export const convertMonth = (month, year) => {
  switch (month) {
    case 0:
      return {
        id: 1,
        value: `1 ${year}`,
        text:
          getLocalStorage("language") === "id"
            ? `Januari ${year}`
            : `January ${year}`,
      };
    case 1:
      return {
        id: 2,
        value: `2 ${year}`,
        text:
          getLocalStorage("language") === "id"
            ? `Februari ${year}`
            : `February ${year}`,
      };
    case 2:
      return {
        id: 3,
        value: `3 ${year}`,
        text:
          getLocalStorage("language") === "id"
            ? `Maret ${year}`
            : `March ${year}`,
      };
    case 3:
      return {
        id: 4,
        value: `4 ${year}`,
        text: `April ${year}`,
      };
    case 4:
      return {
        id: 5,
        value: `5 ${year}`,
        text: `May ${year}`,
      };
    case 5:
      return {
        id: 6,
        value: `6 ${year}`,
        text: `June ${year}`,
      };
    case 6:
      return {
        id: 7,
        value: `7 ${year}`,
        text: `July ${year}`,
      };
    case 7:
      return {
        id: 8,
        value: `8 ${year}`,
        text: `August ${year}`,
      };
    case 8:
      return {
        id: 9,
        value: `9 ${year}`,
        text: `September ${year}`,
      };
    case 9:
      return {
        id: 10,
        value: `10 ${year}`,
        text: `October ${year}`,
      };
    case 10:
      return {
        id: 11,
        value: `11 ${year}`,
        text: `November ${year}`,
      };
    default:
      return {
        id: 12,
        value: `12 ${year}`,
        text: `December ${year}`,
      };
  }
};

/* ------------ Count Range Date ex: 9 ---------------- */
export const rangeDate = (start, end) => {
  const dStart = moment(start);
  const dEnd = moment(end);
  return Math.abs(dStart.diff(dEnd, "days")) + 1; // as a days
};

export const getPushUrl = () => {
  const apiUrl =
    process.env.NODE_ENV === "production"
      ? "https://dev-ibb.appx.istdigiplatform.com"
      : "http://localhost:3002";
  return apiUrl;
};

// dynamic privilege key
export const getPrevilegedata = (key, privilege) => {
  const privilegeData = [];
  (privilege?.privilegeList ?? []).forEach((item) => {
    privilegeData.push(item[key]);
    (item?.privilegeChild ?? []).forEach((subItem) =>
      privilegeData.push(subItem[key])
    );
  });
  return privilegeData;
};

// firt letter lowerCase
export const firstLowerCase = ([latter, ...rest]) =>
  latter.toLowerCase() + rest.join("");

export const firstUpperCase = ([latter, ...rest]) =>
  latter.toUpperCase() + rest.join("");

export const getSymbol = (currency) => {
  switch (currency) {
    case "IDR":
      return "Rp";
    case "USD":
      return "USD";
    case "SGD":
      return "SGD";
    case "AUD":
      return "AUD";
    case "GBP":
      return "GBP";
    case "JPY":
      return "JPY";
    case "EUR":
      return "EUR";
    case "CHF":
      return "CHF";
    case "CAD":
      return "CAD";
    case "NZD":
      return "NZD";
    case "HKD":
      return "HKD";
    case "CNY":
      return "CNY";
    case "THB":
      return "THB";
    default:
      return "Rp";
  }
};

// ==> START formatPhoneStripNoSpace 4442-23234-23234
export function formatPhoneStripNoSpace(number) {
  const space = /^\s*$/;
  let formatted = number;
  if (!space.test(number) && number) {
    formatted = formatted
      .replace(/\s/g, "")
      .match(/.{1,4}/g)
      .join("-");
  }

  return formatted;
}
// ==> END formatPhoneNumberStripNoSpace

// ==> START formatPhoneNumberStripNoSpace 021-23234-23234
export function formatPhoneNumberStripNoSpace(number) {
  if (!number) return null;
  const space = /^\s*$/;
  let formatted = number;
  if (!space.test(number)) {
    formatted = formatted.toString();
    //   .replace(/-/g, "")
    //   .replace(/[^0-9]/g, "")
    //   .replace(/^(?=[0-9])([0-9]{3,4})([0-9]{4})([0-9]{4})$/g, "$1 $2 $3");

    if (formatted.length >= 5 && formatted.length < 10) {
      return formatted.replace(/^(?=[0-9])([0-9]{4})([0-9]{1,5})$/g, "$1 $2");
    }
    return formatted.replace(
      /^(?=[0-9])([0-9]{3,4})([0-9]{3,4})([0-9]{3,5})$/g,
      "$1 $2 $3"
    );
  }
  return formatted;
}

export function formatPhoneNumber(number, maxDigit = 16) {
  // this function adds space every after 4 digits and removes the last digit when it reaches the max digit (space included)
  // example: the phone number should have 13 digits max, so you need to pass in the maxDigit argument = 15 if the text field has an addon such as +62 where you don't need to type in the zero, maxDigit = 16 if you have to type in the zero, maxDigit = 18 if you intend to display it as a view only and the BE sends you the number in this format --> "087773333579", use it like this --> formatPhoneNumber(dataFromBE, 18).replace(/^0/g, "+62 ") to get this result --> "+62 877 7333 3579"
  // formatPhoneNumber(e.target.value.replace(/^0+/g, "") --> use it in the onChange property to remove leading zeros
  const space = /^\s*$/;
  let formatted = number;

  if (!space.test(number) && number) {
    formatted = formatted
      .replace(/\s/g, "")
      .match(/.{1,4}/g)
      .join(" ");

    if (formatted.length > maxDigit) formatted = formatted.replace(/\d$/g, "");
  } else {
    formatted = formatted.replace(/\s/g, "");
  }

  return formatted;
}

// input: "add tiering" => output: "Add Tiering"
export const capitalizeFirstLetterFromLowercase = (string, splitBy = " ") => {
  // foolproof
  if (!string || typeof string !== "string") {
    return "-";
  }

  return string
    .split(splitBy)
    .map((word) => `${word.charAt(0).toUpperCase()}${word.slice(1)}`)
    .join(" ");
};

// input: "ADD TIERING" => output: "Add Tiering"
export const capitalizeFirstLetterFromUppercase = (string, splitBy = " ") => {
  // foolproof
  if (!string || typeof string !== "string") {
    return "-";
  }

  return string
    .split(splitBy) // " " for "ADD TIERING", "_" for "ADD_TIERING", and so on...
    .map((word) => `${word.charAt(0)}${word.slice(1).toLowerCase()}`)
    .join(" ");
};

export const handleExceptionError = (statusCode) => {
  switch (statusCode) {
    case 401:
      return false;
    case 502:
      return false;
    case 503:
      return false;
    default:
      return true;
  }
};

export const compare = (a, b) => {
  if (a.order < b.order) {
    return -1;
  }
  if (a.order > b.order) {
    return 1;
  }
  return 0;
};

export const getDateApproval = (date) => {
  if (!date) return "";
  const arrayDate = date.split(" || ");
  const dateFormated = standardDateApproval(arrayDate[0]);
  const time = arrayDate[1];
  console.warn(arrayDate);
  return `${dateFormated} | ${time}`;
};

// format Rp 100,000,000
export const formatAmountCurrency = (input = "", currency = "IDR") => {
  const symbol = getSymbol(currency);
  if (!input) return input;
  const inputNumber = input.replace(/,/g, "").replace(/[^0-9]/g, "");

  if (Number(inputNumber) === 0) return "";
  if (inputNumber[0] === "0" || inputNumber[3] === "0")
    return `${symbol} ${Number(inputNumber)
      .toString()
      .replace(/,/g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  // eslint-disable-next-line consistent-return
  if (inputNumber.length >= 43)
    return `${symbol} ${inputNumber
      .slice(0, -1)
      .replace(/,/g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  // eslint-disable-next-line consistent-return
  return `${symbol} ${inputNumber
    .replace(/,/g, "")
    .replace(/[^0-9]/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
};

// format Rp 100.000.000
export const formatAmountDotCurrency = (input = "", currency = "IDR") => {
  const symbol = getSymbol(currency);
  if (!input) return input;
  const inputNumber = input.replace(/\./g, "").replace(/[^0-9]/g, "");

  if (Number(inputNumber) === 0) return "";
  if (inputNumber[0] === "0" || inputNumber[3] === "0")
    return `${symbol} ${Number(inputNumber)
      .toString()
      .replace(/\./g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
  // eslint-disable-next-line consistent-return
  if (inputNumber.length >= 43)
    return `${symbol} ${inputNumber
      .slice(0, -1)
      .replace(/\./g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
  // eslint-disable-next-line consistent-return
  return `${symbol} ${inputNumber
    .replace(/\./g, "")
    .replace(/[^0-9]/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
};

// format 100,000,000
export const formatAmount = (input = "") => {
  if (!input) return input;
  const inputNumber = input
    ?.toString()
    .replace(/,/g, "")
    .replace(/[^0-9]/g, "");

  if (Number(inputNumber) === 0) return "";
  if (inputNumber[0] === "0" || inputNumber[3] === "0")
    return `${Number(inputNumber)
      .toString()
      .replace(/,/g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  // eslint-disable-next-line consistent-return
  if (inputNumber.length >= 43)
    return `${inputNumber
      .slice(0, -1)
      .replace(/,/g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  // eslint-disable-next-line consistent-return
  return `${inputNumber
    .replace(/,/g, "")
    .replace(/[^0-9]/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
};

// format 100.000.000
export const formatAmountDot = (input = "") => {
  if (!input) return input;
  const inputNumber = input
    .toString()
    .replace(/\./g, "")
    .replace(/[^0-9]/g, "");

  if (Number(inputNumber) === 0) return "";
  if (inputNumber[0] === "0" || inputNumber[3] === "0")
    return `${Number(inputNumber)
      .toString()
      .replace(/\./g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
  // eslint-disable-next-line consistent-return
  if (inputNumber.length >= 43)
    return `${inputNumber
      .slice(0, -1)
      .replace(/\./g, "")
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
  // eslint-disable-next-line consistent-return
  return `${inputNumber
    .replace(/\./g, "")
    .replace(/[^0-9]/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
};

// format only number
export const parseNumber = (val) => {
  const normalize = val && val.replace(/,\./g, "").replace(/[^0-9]/g, "");
  // ?.substring(0, 15);
  return normalize;
};

// format only number and max length
export const numberIgnoreZero = (val) => {
  const normalize =
    val &&
    val
      .replace(/,\./g, "")
      .replace(/[^0-9]/g, "")
      ?.substring(0, 15);
  return normalize;
};

export const NumberWithMaxLength = (val, length = 15) => {
  const normalize =
    val &&
    val
      .replace(/,\./g, "")
      .replace(/[^0-9]/g, "")
      ?.substring(0, 15);
  return normalize;
};

export const ipAddressInput = (val, length = 15) => {
  const normalize =
    val &&
    val
      .replace(/[^0-9.]/g, "")
      .replace(/\.{2,}/g, ".")
      .substring(0, length);
  return normalize;
};

// ==>  formatPhoneNumber 0819-2728-4401
export function formatPhoneNumberStrip(number) {
  const space = /^\s*$/;
  let formatted = number;
  if (!space.test(number)) {
    formatted = formatted
      .replace(/\s/g, "")
      .match(/.{1,4}/g)
      .join(" - ");
  }

  return formatted;
}
// ==> END formatPhoneNumber

// ==> START formatCardNumber 1112 1124 9403 1429
export function formatCardNumber(number) {
  const space = /^\s*$/;
  let formatted = number;
  if (!space.test(number) && number) {
    formatted = formatted
      .replace(/\s/g, "")
      .match(/.{1,4}/g)
      .join(" ");
  }

  return formatted;
}
// ==> END formatCardNumber

// ==> START formatCreditCard 03/2022
export function formatCreditCard(number) {
  const formatted = number
    .replace(
      /^([1-9]\/|[2-9])$/g,
      "0$1/" // 3 > 03/
    )
    .replace(
      /^(0[1-9]|1[0-2])$/g,
      "$1/" // 11 > 11/
    )
    .replace(
      /^([0-1])([3-9])$/g,
      "0$1/$2" // 13 > 01/3
    )
    .replace(
      /^(0?[1-9]|1[0-2])([0-9]{2})$/g,
      "$1/$2" // 141 > 01/41
    )
    .replace(
      /^([0]+\/|[0]+)$/g,
      "0" // 0/ > 0 and 00 > 0
    )
    .replace(
      /[^\d\/]|^[\/]*$/g,
      "" // To allow only digits and `/`
    )
    .replace(
      /\/\//g,
      "/" // Prevent entering more than 1 `/`
    );

  return formatted;
}
// ==> END formatCardNumber

export const validateMinMax = (value, min = 1, max) => {
  const valueNew = value?.toString()?.trim();
  if (!valueNew?.length) return true;
  if (valueNew?.length < min) return false;
  if (max && valueNew.length > max) return false;
  if (valueNew === "!") return false;
  return true;
};

export const containsWhitespace = (str) => {
  if (!str.length) return false;
  return /\s/.test(str);
};

export const validateName = (val) => {
  const regEx = /^[a-zA-Z0-9 ]{2,25}$/g;
  if (!val?.length) return true;
  if (regEx.test(val)) return true;
  if (!regEx.test(val)) return false;
};

export const validateCode = (val) => {
  const regEx = /^[0-9]{2,8}$/g;
  if (!val?.length) return true;
  if (regEx.test(val)) return true;
  if (!regEx.test(val)) return false;
};

// checklis helper

export const checkAll = (data, key) =>
  data.filter((item) => item[key]).length === data.length;

export const handleCheckAll = (data, key, isCheckAll) => {
  const result = data.findIndex((item) => item[key]);

  return data?.map((item) => ({
    ...item,
    [key]: isCheckAll ? false : result > -1 ? true : !(result > -1),
  }));
};

export const indeterminate = (data, key, checkAll) =>
  data.filter((a) => a[key]).length && !checkAll;

// end checklis helper

// format only Text and number
export const parseAlphaNumericNoSpace = (val) => {
  const normalize =
    val &&
    val
      .replace(/\s/g, "")
      .replace(/,\./g, "")
      .replace(/[^0-9a-zA-Z ]/g, "");
  return normalize;
};

// format only Text and number
export const parseAlphaNumeric = (val) => {
  const normalize =
    val && val.replace(/,\./g, "").replace(/[^0-9a-zA-Z ]/g, "");
  return normalize;
};

// format only Text
export const parseAplha = (val) => {
  const normalize = val && val.replace(/,\./g, "").replace(/[^a-zA-Z ]/g, "");
  return normalize;
};

// format only Text and number and start space
export const parseTextNumFirstSpace = (val) => {
  const normalize = parseAlphaNumeric(val);
  return normalize.trimStart();
};

// format only Text and number, and some special charcter
export const parseTextNumFirstSpecialChar = (val, regex) => {
  // parseTextNumFirstSpacialChar = (e.target.value,  /[^A-Za-z0-9-:,]/g)
  const normalize = val && val.replace(/,\./g, "").replace(regex, "");
  return normalize.trimStart();
};

// format only number
export const parseNum = (val) => {
  const normalize = val
    ? val
        ?.toString()
        ?.replace(/,\./g, "")
        ?.replace(/[^0-9]/g, "")
    : // ?.substring(0, 15)
      "";
  return normalize?.trim();
};

const handleUpperCase = ([a, ...rest]) => [a.toUpperCase(), ...rest].join("");

export const handleConvertRole = (value) =>
  value
    ?.replaceAll("_", " ")
    ?.toLowerCase()
    ?.split(" ")
    ?.map((m) => handleUpperCase(m))
    ?.join(" ") || "-";

export const emailRegex = (email) =>
  email &&
  email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );

export const gridSize = (type) => {
  switch (type) {
    case "LIMIT_PACKAGE_DETAIL":
      return 12;
    case "CORPORATE_MANAGEMENT_DETAIL":
      return 12;
    case "CORPORATE_ACCOUNT_LIMIT_CONFIG":
      return 12;
    case "PRIVILEGE_SETTINGS_DETAIL":
      return 12;
    case "PRODUCT_DETAIL":
      return 12;
    case "ACCOUNT_PRODUCT_DETAIL":
      return 12;
    case "LIMIT_TRANSACTION_DETAIL":
      return 12;
    case "PARAMETER_SETTINGS_DETAIL":
      return 12;
    case "SEGMENTATION_BRANCH_DETAIL":
      return 12;
    case "COMPANY_LIMIT_DETAIL":
      return 12;
    case "COMPANY_CHARGE_DETAIL":
      return 12;
    case "SEGMENTATION_DETAIL":
      return 12;
    case "APPROVAL_MATRIX_DETAIL":
      return 12;
    case "ACCOUNT_TYPE_DETAIL":
      return 12;
    case "TIERING_DETAIL":
      return 12;
    case "MENU_PACKAGE_DETAIL":
      return 12;
    case "SPECIAL_MENU_DETAIL":
      return 12;
    case "ACCOUNT_CONFIGURATION_DETAIL":
      return 12;
    case "CHARGE_PACKAGE_DETAIL":
      return 12;
    case "USER_CORPORATE_DETAIL":
      return 12;
    default:
      return 6;
  }
};

export const handleValueActivityDetailAudit = (activityName) => {
  const activity = activityName.split(" ");
  if (["Execute", "Create", "Input", "Edit"].includes(activity[0])) {
    return `${activity[0]} ${activity[1]}`;
  }
  return activityName;
};

export const formatMoneyRMG = (amount = "0", currency = "IDR") => {
  const symbol = getSymbol(currency);
  const numberString = amount?.toString() || "";
  if (!numberString) {
    return numberString;
  }
  const split = numberString.split(".");
  let decimal = split[1];
  const formatted = split[0].replace(/(\d)(?=(\d{3})+$)/g, "$&.");

  if (decimal === undefined) {
    decimal = "00";
  } else if (decimal[1] === undefined) {
    decimal = `${decimal}0`;
  }
  return `${symbol} ${formatted},${decimal}`;
};

// ==> START formatCardNumberInput 1112 1124 9403 1429
export function formatCardNumberInput(number) {
  const normalizeNumber = parseNumber(number);
  const space = /^\s*$/;
  let formatted = normalizeNumber;
  if (!space.test(normalizeNumber) && normalizeNumber) {
    formatted = formatted
      .replace(/\s/g, "")
      .match(/.{1,4}/g)
      .join(" ");
  }

  return formatted;
}
// ==> END formatCardNumber
// format only FullName
export const parseFullName = (val) => {
  const normalize =
    val && val.replace(/,\./g, "").replace(/[^a-zA-Z\s-\',.]/g, "");
  return normalize;
};

export const parseUsernameHandlingOfficer = (val) => {
  const normalize =
    val && val.replace(/,\./g, "").replace(/[^a-zA-Z\s-_\',.]/g, "");
  return normalize;
};

// format only Address
export const parseAddress = (val) => {
  const normalize =
    val && val.replace(/,\./g, "").replace(/[^a-zA-Z0-9\s-\',.]/g, "");
  return normalize;
};

// format to delete first space
export const parseFirstSpace = (val) => {
  const normalize = val && val?.replace(/^\s+/, "");
  return normalize;
};

// url validation
export const isValidUrl = (url) => {
  const urlRegex = /^(?:https?:\/\/)?(?:www\.)?[\w\-]+\.[\w\-]+[^\s]*$/;
  return urlRegex.test(url);
};

export const makeId = (length) => {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
};
