const getDataPDF = (bytesBase64, mimeType, fileName) => {
  const fileUrl = `data:${mimeType};base64,${bytesBase64}`;
  fetch(fileUrl)
    .then((response) => response.blob())
    .then((blob) => {
      const link = window.document.createElement("a");
      link.href = window.URL.createObjectURL(blob, { type: mimeType });
      link.target = "_blank";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
};

export default getDataPDF;
