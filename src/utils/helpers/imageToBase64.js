import Compressor from "compressorjs";
import { useState } from "react";

export const blobToBase64 = (blob) => {
  const reader = new FileReader();
  reader.readAsDataURL(blob);
  return new Promise((resolve) => {
    reader.onloadend = () => {
      resolve(reader.result);
    };
  });
};

export const useCompressFile = () => {
  const [base64, setBase64] = useState(null);
  // eslint-disable-next-line no-new
  const handleCompressFile = (file) => {
    // eslint-disable-next-line no-new

    Compressor.abort();
  };

  return { handleCompressFile, base64 };
};
