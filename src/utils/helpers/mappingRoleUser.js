/* eslint-disable import/prefer-default-export */
export const mappingRoleUser = (type) => {
  switch (type) {
    case 0:
      return "adminMaker";
    case 1:
      return "adminApproval";
    case 2:
      return "userMaker";
    case 3:
      return "userChecker";
    case 4:
      return "userApproval";
    default:
      return "";
  }
};
