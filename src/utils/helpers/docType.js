export const DocType = (base64) => {
  if (base64?.includes("private/kyc") || !base64) {
    return {
      type: "image",
      base64: `data:image/png;base64,`,
      extention: "png",
    };
  }
  if (base64.substring(0, 10) === "iVBORw0KGg") {
    return {
      type: "image",
      base64: `data:image/png;base64,${base64}`,
      extention: "png",
    };
  }

  if (base64.substring(0, 5) === "/9j/4") {
    return {
      type: "image",
      base64: `data:image/jpeg;base64,${base64}`,
      extention: "jpeg",
    };
  }

  if (base64.substring(0, 6) === "JVBERi") {
    return {
      type: "file",
      base64: `data:application/pdf;base64,${base64}`,
      extention: "pdf",
    };
  }

  //   default
  return {
    type: "image",
    base64: `data:image/png;base64,${base64}`,
    extention: "png",
  };
};
