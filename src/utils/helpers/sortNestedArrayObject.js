/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
const sortNestedArrayObject = (prop, arr) => {
  if (!arr) {
    return [];
  }
  prop = prop.split(".");
  const len = prop.length;

  arr.sort((a, b) => {
    let i = 0;
    while (i < len) {
      a = a[prop[i]];
      b = b[prop[i]];
      i++;
    }
    if (a.toLowerCase() < b.toLowerCase()) {
      return -1;
    }
    if (a.toLowerCase() > b.toLowerCase()) {
      return 1;
    }
    return 0;
  });
  return arr;
};

export default sortNestedArrayObject;
