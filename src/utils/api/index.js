/* eslint-disable no-unused-vars */
import axios from "axios";
import UserService from "services/UserService";

const sitURL = "https://be.arccelera.cloud/api";
const apiUrl =
  process.env.NODE_ENV === "production" ? process.env.REACT_APP_API : sitURL;

const getHeadersPrivilege = (privilegeId) => ({
  headers: {
    privilege: privilegeId,
  },
});

// // Login Service
// export const serviceLogin = (payload) =>
//   axios.post(`${apiUrl}/portalservice/loginldap`, payload, {
//     headers: {
//       menuid: "81",
//     },
//   });

// Login Service
export const serviceLogin = (payload) =>
  axios.post(`${apiUrl}/portalservice/login`, payload, {
    headers: {
      privilege: "81",
    },
  });

export const serviceLoginKeycloak = (payload) =>
  axios.post(`${apiUrl}/portalservice/logininfo`, payload, {
    headers: {
      Authorization: `${UserService.getToken()}`,
    },
  });

// Logout Service
export const serviceLogout = (payload) =>
  axios.post(`${apiUrl}/portalservice/logout`, payload, {
    headers: {
      menuid: "81",
    },
  });

// DASHBOARD SERVICES

// Trasnsaction History page
export const serviceGetTransactionHistoryPage = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/gettransactionhistorypage`, payload);

// Transaction History page
export const serviceGetUserActifityDetails = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getuseractivitydetail`, payload);

// Compare Transaction
export const serviceCompareTransaction = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getcomparetransaction`, payload);

// Transaction Chart Service
export const serviceTransactionChart = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/gettransactionchart`, payload);

// General Parameter Service
export const serviceGeneralParameter = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getgeneralparameter`, payload);

// General parameters Request Update Paramenets
export const serviceUpdateGeneralParameter = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/requestupdateparameter`, payload);

// General Parameter Service
export const serviceManagamentUser = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getalluseradminpaging`, payload, {
    headers: { menuid: 74 },
  });

// Request Save User Admin
export const serviceSaveUserAdmin = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/requestsaveuseradmin`, payload);

// Request Delete User Admin
export const serviceDeleteUserAdmin = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/requestdeleteuseradmin`, payload);

// Request All Approval User data -> comment dulu, udah diganti approval config
// export const serviceApprovalUser = (payload) =>
//   axios.post(`${apiUrl}/portalservice/getallapprovaluseradminpaging`, payload);

// Approval Useradmin Detail -> comment dulu, udah diganti approval config
// export const serviceApprovalUserAdminDetail = (payload) =>
//   axios.post(
//     `${apiUrl}/portalservice/opendataapprovaluseradmindetail`,
//     payload
//   );

// Approval User admin -> comment dulu, udah diganti approval config
// export const serviceApprovalUserAdmin = (payload) =>
//   axios.post(`${apiUrl}/portalservice/approveuseradmin`, payload);

// Approval Multple User admin -> comment dulu, udah diganti approval config
// export const serviceApprovalMultipleUserAdmin = (payload) =>
//   axios.post(`${apiUrl}/portalservice/userapprove`, payload);

// Approva Config
export const serviceApprovalConfig = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/approvalconfigpaging`, payload);

// Approval config detail
export const ServiceApprovalConfigDetail = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/approvalconfigdetail`, payload);

// Service config approve
export const ServiceConfigApprove = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/configapprove`, payload);

// Management Role
export const serviceManagementRole = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getallrole`, payload);

// Gett all workflow Role
export const getAllWorkFlowRole = (payload) =>
  axios.post(`${apiUrl}/portalservice/getallrole`, payload);

// Service getAll Provilege
export const serviceGetAllPrivilege = (privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getallprivilege`);

// Service getRole detail
export const serviceGetRoleDetail = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getroledetail`, payload);

// Service Request Save Role
export const serviceRequestSaveRole = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/requestsaverole`, payload);

// Service request delete role
export const serviceRequestDeleteRole = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/requestdeleterole`, payload);

// Service LDAP users
export const servicesGetLdapUser = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getldapuser`, payload);

// Service request managment-company
export const serviceRequestManagementCompany = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/get`, payload);

export const serviceRequestManagementCompanyDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/detail`, payload);

export const serviceRequestManagementCompanyEditProfile = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/edit`, payload);

export const serviceRequestManagementCompanyGetProvenci = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getprovince`, payload);

export const serviceRequestManagementCompanyGetKabupaten = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getdistrict`, payload);

export const serviceRequestManagementCompanyGetSubDistrict = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getsubdistrict`, payload);

export const serviceRequestManagementCompanyGetVillage = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getvillage`, payload);

export const serviceRequestManagementCompanyGetPostCode = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getpostalcodelist`, payload);

export const serviceRequestManagementCompanyToken = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/token`, payload);

export const serviceRequestManagementCompanyTokenExecute = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);

export const serviceRequestManagementCompanyRekeningSetting = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/accountsetting`, payload);

// Service request workFlow
export const serviceRequestWorkFlow = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowtemplate/get`, payload);

export const serviceRequestWorkFlowAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowtemplate/add`, payload);

export const serviceRequestPengaturanAlurKerja = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenugroup`, payload);

export const serviceRequestPengaturanAlurKerjaAccessMaster = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuaccessmaster`, payload);

export const serviceRequestPengaturanAlurKerjaSuccess = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuaccess`, payload);

export const serviceRequestPengaturanAlurKerjaUserByType = (payload) =>
  axios.post(`${apiUrl}/portalservice/getuserbytype`, payload);

export const serviceRequestPengaturanAlurKerjaTambah = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowadd`, payload);

export const serviceRequestPengaturanAlurKerjaEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenugroup/edit`, payload);

export const serviceRequestBranchPengaturan = (payload) =>
  axios.post(`${apiUrl}/portalservice/branch`, payload, {
    headers: {
      privilege: 37,
    },
  });

export const serviceRequestBranchPengaturanSync = (payload) =>
  axios.post(`${apiUrl}/portalservice/branch/sync`, payload);

export const serviceGetAllBranch = (privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getallbranch`);

export const serviceRequestGlobalLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/limit/global/get`, payload);

export const serviceRequestWorkFlowRole = (payload) =>
  axios.post(`${apiUrl}/portalservice/getallworkflowrole`, payload);
export const serviceRequestPengaturanPengguna = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/user/get`, payload);
export const serviceRequestPengaturanPenggunaEndSession = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/user/destroysession`, payload);

export const serviceRequestUserGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/getusercorporategroup`, payload);

export const serviceRequestPengaturanPenggunaDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/user/detail`, payload);

export const serviceRequestPengaturanPenggunaAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/user/add`, payload);
export const servicePengaturanPenggunaValidationAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/checkusernameexist`, payload);
export const servicePengaturanPenggunaValidationPhoneNumber = (payload) =>
  axios.post(`${apiUrl}/portalservice/checkphonenumberexist`, payload);
export const servicePengaturanPenggunaValidationEmail = (payload) =>
  axios.post(`${apiUrl}/portalservice/checkemailexist`, payload);
export const serviceRequestPengaturanPenggunaEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/user/update`, payload);

export const serviceRequestPengaturanPenggunaEditStatus = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/user/editstatus`, payload);

export const serviceRequestPengaturanPenggunaApprovalLevel = (payload) =>
  axios.post(`${apiUrl}/portalservice/getlimitscheme`, payload);

export const serviceRequestBankDomestic = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbankpage`, payload);

export const serviceRequestBankDomesticCreateValidation = (payload) =>
  axios.post(`${apiUrl}/portalservice/inlinevalidatecreatebank`, payload);

export const serviceRequestBankDomesticCreate = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestsavebank`, payload);

export const serviceRequestBankDomesticDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbank`, payload);

export const serviceRequestBankDomesticList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbanklist`, payload);

export const serviceRequestBankDomesticChangeStatus = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestactivatebank`, payload);

export const serviceRequestBankDomesticDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestdeletebank`, payload);

export const serviceRequestBankDomesticError = (payload) =>
  axios.post(`${apiUrl}/portalservice/errorcode/get`, payload);

export const serviceGetPendingTaskRegistration = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/registration/corporate/get`, payload);

export const serviceGetPendingTaskRegistrationDetail = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/registration/corporate/detail`, payload);

export const serviePendingTaskSoftToken = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/corporate/softtoken/get`, payload);

export const servicePendingTaskSoftTokenExecute = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);

export const servicePendingTaskSoftTokenBulkExecute = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute/bulk`, payload);

export const serviceSoftTokenUbahOtirisasi = (payload, privilegeId) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/upgraderegistration/get`,
    payload
  );

export const serviceSoftTokenUbahOtirisasiDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/upgraderegistration/detail`,
    payload
    // getHeadersPrivilege(37)
  );

export const serviceSoftTokenRegistrationExecute = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/registration/corporate/execute`, payload);

export const serviceRequestManagementCompanyKonfigurasiRekeningValidationLimitGet =
  (payload) =>
    axios.post(`${apiUrl}/portalservice/validatationdatalms `, payload);
export const serviceRequestManagementCompanyKonfigurasiRekeningGet = (
  payload
) => axios.post(`${apiUrl}/portalservice/accountlimit/get`, payload);
export const serviceRequestManagementCompanyKonfigurasiRekeningAccunt = (
  payload
) => axios.post(`${apiUrl}/portalservice/corporate/account`, payload);

export const serviceRequestManagementCompanyKonfigurasiRekeningAccuntUpdate = (
  payload
) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/accountlimitconfig/updateaccountlimitconfig`,
    payload
  );

// registration Portal
export const serviceRegistrationList = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/get/waitingapproval`, payload);

export const servcieRegistrationTC = (payload) =>
  axios.post(`${apiUrl}/portalservice/getsystemparameter`, payload);

export const servcieRegistrationSteps = (payload) =>
  axios.post(`${apiUrl}/portalservice/updateregistrationstep`, payload);

export const servcieRegistrationAccountvalidate = (payload) =>
  axios.post(`${apiUrl}/portalservice/validateaccount`, payload);

export const serviceRegisProvince = () =>
  axios.post(`${apiUrl}/portalservice/register/getprovince`);

export const serviceRegisDistrict = (payload) =>
  axios.post(`${apiUrl}/portalservice/register/getdistrict`, payload);

export const serviceUplaodFile = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporateregistrationuploaddocs`,
    payload
  );

export const serviceRegisNearBranch = (payload) =>
  axios.post(`${apiUrl}/portalservice/getnearestbranch`, payload);

export const serviceGenerateOTP = (payload) =>
  axios.post(`${apiUrl}/portalservice/generateotptoemail`, payload);
// end

export const serviceRequestHostErrorMapping = (payload) =>
  axios.post(`${apiUrl}/portalservice/errorcode/get`, payload);

export const serviceRequestHostErrorMappingAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/errorcode/add`, payload);

export const serviceRequestHostErrorMappingEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/errorcode/edit`, payload);

export const serviceRequestKelompokRekeningPupUp = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcorporateaccountlistingroup`, payload);

export const serviceGroupNameValidation = (payload) =>
  axios.post(`${apiUrl}/portalservice/checkallvalidationmams`, payload);

export const serviceRequestManagementCompanyRekening = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/getcorporateaccountgroup/detail`,
    payload
  );

export const serviceRequestKelompokRekeningAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcorporateaccountgroup/add`, payload);

export const serviceRequestKelompokRekeningEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcorporateaccountgroup/edit`, payload);

export const serviceRequestKelompokRekeningDelete = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/getcorporateaccountgroup/delete`,
    payload
  );

export const serviceGetKelompokRekening = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcorporateaccountgroup`, payload);

export const serviceRequestPengaturanPenerimaTransfer = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/beneficiaryconfig/get`,
    payload
  );

export const serviceRequestPengaturanPenerimaTransferEdit = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/beneficiaryconfig/edit`,
    payload
  );

export const serviceRequestHandlingOfficer = (payload) =>
  axios.post(`${apiUrl}/portalservice/handlingofficer/get`, payload);

export const serviceRequestHandlingOfficerAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/handlingofficer/add`, payload);

export const serviceRequestHandlingOfficerEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/handlingofficer/edit`, payload);

export const serviceRequestInternationalBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbankpage`, payload, {
    headers: {
      // channel: "BN",
      privilege: 37,
    },
  });

export const serviceRequestGetServiceGlobalLimitTrx = (payload) =>
  axios.post(`${apiUrl}/portalservice/getservice`, payload);

export const serviceRequestGetTransactionCategoryGlobalLimitTrx = (payload) =>
  axios.post(`${apiUrl}/portalservice/gettransactioncategory`, payload);

export const serviceRequestGetTransactionCurrencyMatrixGlobalLimitTrx = (
  payload
) => axios.post(`${apiUrl}/portalservice/getcurrencymatrix`, payload);

export const serviceRequestAddGlobalLimitTrx = (payload) =>
  axios.post(`${apiUrl}/portalservice/service/add`, payload);

export const serviceRequestValidationLimitManagementService = (payload) =>
  axios.post(`${apiUrl}/portalservice/validatationdatalms`, payload);

export const serviceRequestGetTransactionCurrencyMatrix = (payload) =>
  axios.post(`${apiUrl}/portalservice/getservicecurrencymatrix`, payload);

export const serviceRequestGetGloalLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/globallimit/get`, payload);

export const serviceLimitPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitpackage/get`, payload);

export const serviceLimitPackageDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitpackage/detail`, payload);

export const editGlobalLimitPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/globallimit/add`, payload);

export const serviceAddLimitPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitpackage/add`, payload);

export const serviceCurrencyMatrix = (payload) =>
  axios.post(`${apiUrl}/portalservice/getservicecurrencymatrix`, payload);

export const serviceRequestGetTearingSlab = (payload) =>
  axios.post(`${apiUrl}/portalservice/tiering/get`, payload);

export const serviceRequestGetTearingSlabDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/tiering/detail`, payload);

export const serviceRequestAddTearingSlab = (payload) =>
  axios.post(`${apiUrl}/portalservice/tiering/add`, payload);

export const serviceRequestAddGloalLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/globallimit/add`, payload);

export const serviceRequestGetCurrency = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcurrency`, payload);

export const serviceRequestGetProductAccountType = (payload) =>
  axios.post(`${apiUrl}/portalservice/product/getaccounttype`, payload);

export const serviceRequestGetProduct = (payload) =>
  axios.post(`${apiUrl}/portalservice/product/get`, payload);

export const serviceRequestAddProduct = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/product/add`, payload);

export const serviceRequestGetMenuGlobal = (payload, privilegeId) =>
  axios.post(`${apiUrl}/portalservice/getmenuglobal`, payload);

export const serviceRequestExcuteMenuGlobal = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestsavemenuglobal`, payload);

export const serviceRequestGetRekeningProduk = (payload) =>
  axios.post(`${apiUrl}/portalservice/producttype/get`, payload);

export const serviceRequestAddRekeningProduk = (payload) =>
  axios.post(`${apiUrl}/portalservice/productdetail/add`, payload);

export const serviceRequestExecuteRekeningProduk = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);

export const serviceRequestProductDetailGet = (payload) =>
  axios.post(`${apiUrl}/portalservice/productdetail/get`, payload);

export const serviceRequestProductGet = (payload) =>
  axios.post(`${apiUrl}/portalservice/product/get`, payload);

export const serviceRequestGetAccountType = (payload) =>
  axios.post(`${apiUrl}/portalservice/product/getaccounttype`);

export const serviceRequestGetWorkFlow = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowapproval/get`, payload);

export const serviceRequestGetWorkFlowDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowapproval/detail`, payload);

export const serviceRequestGetWorkFlowExcute = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);

export const serviceRequestGetWorkFlowBulkExcute = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute/bulk`, payload);

export const serviceRequestGetMenuPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/getmenu`, payload);

export const serviceRequestGetMenuPackageDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/detail`, payload);

export const serviceRequestGetMenuPackageMaster = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuglobal`, payload);

export const serviceRequestAddMenuPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/add`, payload);

export const serviceSendTakenAjaLink = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/sendtekenajalink`, payload, {
    headers: {
      url: `${process.env.REACT_APP_CUSTOMER_DOMAIN}/customer-signature`,
      menuid: 71,
    },
  });

export const serviceSendTakenAjaLinkOtorisasi = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/sendtekenajalink`, payload, {
    headers: {
      url: `${process.env.REACT_APP_CUSTOMER_DOMAIN}/customer-signature/change-autorization`,
      menuid: 71,
    },
  });

export const serviceRequestGetSegmentCabang = (payload) =>
  axios.post(`${apiUrl}/portalservice/segmentbranch/get`, payload);

export const serviceRequestAddSegmentCabang = (payload) =>
  axios.post(`${apiUrl}/portalservice/segmentbranch/add`, payload);

export const serviceRequestGetBranchWithoutPagination = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbranch`, payload, {
    headers: {
      privilege: 37,
    },
  });

export const serviceRequestGetBranchDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/segmentbranch/detail`, payload, {
    headers: {
      privilege: 37,
    },
  });
export const serviceRequestGetCompanyCharge = (payload) =>
  axios.post(`${apiUrl}/portalservice/companycharge/get`, payload);

export const serviceRequestGetCompanyChargeDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/companycharge/detail`, payload);

export const serviceRequestAddCompanyCharge = (payload) =>
  axios.post(`${apiUrl}/portalservice/companycharge/add`, payload);

export const serviceRequestSaveCompanyCharge = (payload) =>
  axios.post(`${apiUrl}/portalservice/companycharge/save`, payload);

export const serviceRequestGetServiceCharges = (payload) =>
  axios.post(`${apiUrl}/portalservice/servicecharges/get`, payload);

export const serviceRequestAddServiceCharges = (payload) =>
  axios.post(`${apiUrl}/portalservice/servicecharges/add`, payload);

export const serviceRequestAllChargesDropdown = (payload) =>
  axios.post(`${apiUrl}/portalservice/chargesdropdownmenu/get`, payload);

export const serviceRequestGetCompanyLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/companylimit/get`, payload);
export const serviceRequestGetValidationCompanyLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/validatationdatalms`, payload);
export const serviceRequestGetCompanyLimitDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/companylimit/detail`, payload);

export const serviceRequestAddCompanyLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/companylimit/add`, payload);

export const serviceRequestAllCurrencyCompanyLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcurrency`, payload);

export const serviceRequestDownloadFileCompanyCharge = (payload) =>
  axios.post(`${apiUrl}/portalservice/downloadfile`, payload, {
    responseType: "blob",
  });

export const serviceGetPrivilegeHakaksesGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/privilegegroup/get`, payload);

export const serviceInsertPrivilegeHakaksesGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/privilegegroup/add`, payload);

export const serviceDeletePrivilegeHakaksesGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/privilegegroup/delete`, payload);

export const serviceMenuGlobalApprovalMatrix = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuglobal`, payload);

export const serviceMenuGlobalApprovalMatrixGetSquence = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrix/get`, payload);

export const serviceApprovalMatrixAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrix/add`, payload);

export const serviceDeleteApprovalMatrix = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrix/delete`, payload);

export const serviceApprovalMatrixAllMenuType = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrix/getallservice`, payload);

export const serviceGetListOfficeUserRole = (payload) =>
  axios.post(`${apiUrl}/portalservice/branchlist`, payload);

export const serviceGetUserRole = (payload) =>
  axios.post(`${apiUrl}/portalservice/userrole`, payload);
export const serviceGetRoleUserDetails = (payload) =>
  axios.post(`${apiUrl}/portalservice/getuserbyroleandgroup`, payload);
export const serviceGetListMenuUserRole = (payload) =>
  axios.post(`${apiUrl}/portalservice/privilegegroup/get`, payload);

export const serviceGetSegmentasi = (payload) =>
  axios.post(`${apiUrl}/portalservice/segmentation/get`, payload);

export const serviceAddSegmentasi = (payload) =>
  axios.post(`${apiUrl}/portalservice/segmentation/add`, payload);

export const serviceChangeAutorizationDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/upgraderegistration/detail`,
    payload
    // getHeadersPrivilege(37)
  );

export const serviceChangeAuthorizaionGet = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/upgraderegistration/manualupgraderegistration/get`,
    payload
    // getHeadersPrivilege(37)
  );

export const serviceChangeAuthorizaionDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/getregistrationdata`,
    payload
    // getHeadersPrivilege(37)
  );

export const serviceGetGroupByBranchId = (payload) =>
  axios.post(`${apiUrl}/portalservice/privilegegroupbybranchid/get`, payload);

export const serviceParameterSetting = (payload) =>
  axios.post(`${apiUrl}/portalservice/parametersetting/get`, payload);

export const serviceParameterSettingDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/parametersetting/detail`, payload);

export const serviceParameterSettingExecute = (payload) =>
  axios.post(`${apiUrl}/portalservice/parametersetting/add`, payload);

export const serviceRequestMenuKhusus = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/getmenu`, payload);

export const serviceRequestMenuKhususGlobal = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuglobal`, payload);

export const serviceRequestMenuKhususAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/add`, payload);

export const serviceRequestMenuKhususDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/detail`, payload);
export const serviceRequestPengaturanKelompok = (payload) =>
  axios.post(`${apiUrl}/portalservice/getgroup`, payload);
export const serviceRequestPengaturanKelompokDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getgroup/detail`, payload);
export const serviceRequestPengaturanKelompokUserFeatures = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenubysegmentation`, payload);
export const serviceRequestPengaturanKelompokLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/getgloballimitbysegmentation`, payload);
export const serviceRequestPengaturanKelompokDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/getgroup/delete`, payload);
export const serviceRequestPengaturanKelompokAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/getgroup/add`, payload);
export const serviceValidateCodeAndName = (payload) =>
  axios.post(`${apiUrl}/portalservice/validatecodeandname`, payload);

// * -----START: Services Non Financial-----
export const serviceGetMenuGlobal = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuglobal`, payload);

export const serviceGetApprovalMatrix = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrixnonfinance/get`, payload);

export const serviceGetAllService = (payload) =>
  axios.post(
    `${apiUrl}/portalservice//approvalmatrixnonfinance/getallservice`,
    payload
  );

export const serviceGetWorkflowGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowgroup/get`, payload);

export const serviceDeleteNonFinancialMatrixApproval = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/approvalmatrixnonfinance/delete`,
    payload
  );

export const serviceAddNonFinancialMatrixApproval = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrixnonfinance/add`, payload);
// * -----END: Services Non Financial-----

// * -----START: Services Product Information-----
export const serviceAccountProductGet = (payload) =>
  axios.post(`${apiUrl}/portalservice/accountproduct/get`, payload);

export const serviceGetAllProductList = () =>
  axios.post(`${apiUrl}/portalservice/getallproductlist`);

export const serviceGetAllProductCodeList = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/getallproductcodelistbyproductesb`,
    payload
  );

export const serviceInquiryProduct = (payload) =>
  axios.post(`${apiUrl}/portalservice/inquiryproduct`, payload);

export const serviceAccountProductDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/accountproduct/detail`, payload);

export const serviceAccountProductAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/accountproduct/add`, payload);

export const serviceAccountProductEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/accountproduct/edit`, payload);

export const serviceAccountProductDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/accountproduct/editstatus`, payload);
// * -----END: Services Product Information-----

// * -----START: Services Cut Off Time-----
export const serviceCutOffTimeGet = (payload) =>
  axios.post(`${apiUrl}/portalservice/cutofftime/get`, payload);

export const serviceCutOffTimeSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/cutofftime/save`, payload);
// * -----END: Services Cut Off Time-----

export const serviceRequestAddApprovalmatrixNonfinance = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrixfinance/add`, payload);

export const serviceRequestApprovelMatrixFinanceSidebar = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenuglobal`, payload);

export const serviceRequestApprovelMatrixFinanceDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrixfinance/get`, payload);

export const serviceRequestApprovelMatrixFinanceAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrixfinance/add`, payload);

export const serviceRequestApprovelMatrixFinanceDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/approvalmatrixfinance/delete`, payload);

export const serviceRequestApprovelMatrixFinanceAllService = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/approvalmatrixfinance/getallservice`,
    payload
  );

export const serviceRequestApprovalMatrixFinanceGetCurrencye = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcurrency`, payload);

export const serviceRequestApprovelMatrixFinanceGetGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowgroup/get`, payload);

export const serviceRequestApprovelMatrixFinanceGetGroupSchemaList = (
  payload
) => axios.post(`${apiUrl}/portalservice/getlimitscheme`, payload);

export const serviceNewSeqGetChargePackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/getchargepackage`, payload);

export const serviceNewSeqGetMenuPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenupackage`, payload);

export const serviceNewSeqGetLimitPackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/getlimitpackage`, payload);

export const serviceNewSeqGetPrpoductType = (payload) =>
  axios.post(`${apiUrl}/portalservice/getproducttype`, payload);

export const serviceNewSeqGetSegmentBranch = (payload) =>
  axios.post(`${apiUrl}/portalservice/getsegmentbranch`, payload);

export const serviceSeqParameterAdvanceUplaod = (payload, segmentasiId) =>
  axios.post(`${apiUrl}/portalservice/parameteradvance/validate`, payload, {
    headers: {
      "content-type": "multipart/form-data",
      segmentationId: segmentasiId,
    },
  });

export const serviceGetbusinessfield = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getbusinessfield`, payload);
export const servicePersetujuanApprovalMatrix = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowapproval/get`, payload);

export const serviceRequestVirtualaccountinstitution = (payload) =>
  axios.post(`${apiUrl}/portalservice/virtualaccountinstitution/get`, payload);

export const serviceRequestVirtualaccountinstitutionDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/virtualaccountinstitution/detail`,
    payload
  );

export const serviceApprovalWorkflowDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowapproval/detail`, payload);

export const serviceApprovalWorkflowExecute = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);

export const serviceApprovalWorkflowBulkExecute = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute/bulk`, payload);

export const serviceRequestAllCurrencyInternationalBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/getcurrency`, payload);

export const serviceRequestSaveInternationalBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestsavebank`, payload);

export const serviceRequestDeleteInternationalBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestdeletebank`, payload);

export const serviceRequestActiveInternationalBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestactivatebank`, payload);

export const serviceSeqmenMenuDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/menupackage/detail`, payload);

export const serviceSeqmenLimitPackageDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitpackage/detail`, payload);

export const serviceSeqmenProductTypeDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/productdetail/get`, payload);

export const serviceSeqmenBranchDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/segmentbranch/detail`, payload);

export const serviceSeqmenChargePackageDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getchargepackage`, payload);

export const serviceGetChargePackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/chargepackage/get`, payload);

export const serviceDetailChargePackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/chargepackage/detail`, payload);

export const serviceExecuteChargePackage = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);
export const serviceChargeList = (payload) =>
  axios.post(`${apiUrl}/portalservice/charges/get`, payload);

export const serviceAddChargesList = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/charges/add`,
    payload,
    getHeadersPrivilege(37)
  );

export const serviceGetAuth = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/list`, payload);

export const serviceRequestAddLevelSettings = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitscheme/edit`, payload);

export const serviceChargePackageSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/chargepackage/add`, payload);

// ---- START Pending Task VA --------
export const servicePendingtaskVaList = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/registva/get`, payload);

export const servicePendingtaskDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/registva/detail`, payload);

export const serviceChargeListExecute = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/execute`, payload);
// ---- END Pending Task VA --------

export const requestDeleteHandlingOfficer = (payload) =>
  axios.post(`${apiUrl}/portalservice/handlingofficer/delete`, payload);

export const serviceChargeLIstWithoutpaging = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/servicechargeswithoutpaging/get`,
    payload
  );

export const serviceLimitPackageAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/imitpackage/add`, payload);

export const serviceDownloadTemplate = (payload) =>
  axios.post(`${apiUrl}/portalservice/downloadfile`, payload, {
    responseType: "blob",
  });

export const serviceSegmentasiSettingManagementPerusahaan = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporatesegmentation/save`, payload);

export const requestDeleteLimit = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitscheme/delete`, payload);

export const requestDeleteHostErrorMapping = (payload) =>
  axios.post(`${apiUrl}/portalservice/errorcode/delete`, payload);

export const requestDownloadCsvDomesticBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/exportbankmanagercsv`, payload, {
    responseType: "blob",
  });

export const requestBisnisFielManagmentCompany = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/getbusinessfield`, payload);

export const hanldeValidationDataLms = (payload) =>
  axios.post(`${apiUrl}/portalservice/validatationdatalms`, payload);

export const serviceSegmenTypeSelect = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/getsegmentproductwithoutpagging`,
    payload
  );

export const serviceRequestPeriodicalChargeList = (payload) =>
  axios.post(`${apiUrl}/portalservice/periodicalcharge/get`, payload);

export const serviceRequestStatusTransaksi = (payload) =>
  axios.post(`${apiUrl}/portalservice/gettransactionbystatus`, payload);

export const serviceRequestBulkStatusTransaksi = (payload) =>
  axios.post(`${apiUrl}/portalservice/getdetailbulktransaction`, payload);

export const serviceRequestStatusTransaksiDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/gettransactiondetail`, payload);

export const serviceGetAllTransactionStatus = (payload) =>
  axios.post(`${apiUrl}/portalservice/getalltransactionstatus`, payload);

export const serviceGetAllTransactionCategory = (payload) =>
  axios.post(`${apiUrl}/portalservice/getalltransactioncategory`, payload);

export const serviceGetAllTransactionGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/getalltransactiongroup`, payload);

export const serviceGetNonTransactionStatus = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/inquiry/getnontransactionalstatus`,
    payload
  );

export const serviceGetNonTransactionStatusDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/inquiry/getnontransactionalstatus/detail`,
    payload
  );

export const serviceGetNonTransactionStatusTaskInformation = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/gettaskinformationnontransaction`,
    payload
  );

export const requestTypeSegementGet = (payload) =>
  axios.post(`${apiUrl}/portalservice/validatationdatalms`, payload);

// ------------ AUDIT TRAIL START ---------
export const getAuditTrail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getaudittrail`, payload, {
    headers: {
      menuid: "81",
    },
  });

export const serviceDownloadExcelAuditTrail = (payload) =>
  axios.post(`${apiUrl}/portalservice/downloadexcelaudittrail`, payload, {
    responseType: "blob",
    headers: {
      menuid: "81",
    },
  });

export const getTransactionDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/gettransactionhistory`, payload, {
    headers: {
      menuid: "81",
    },
  });

// ------------ AUDIT TRAIL END ---------

export const serviceUpgraderegistrastionGet = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/upgraderegistration/manualupgraderegistration/get`,
    payload
  );

export const serviceUpgraderegistrastionDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/upgraderegistration/manualupgraderegistration/detail`,
    payload
  );

export const handleServiceRegistrastionManual = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/registration/manualregistration/get`,
    payload
  );

export const handleServiceRegistrastionManualDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/registration/manualregistration/detail`,
    payload
  );

export const serviceRegisMenuPackageDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmenupackagedetail`, payload);

export const serviceListProvince = (payload) =>
  axios.post(`${apiUrl}/portalservice/province/list`, payload);

export const serviceListDistrict = (payload) =>
  axios.post(`${apiUrl}/portalservice/district/list`, payload);

export const serviceListSubDistrict = (payload) =>
  axios.post(`${apiUrl}/portalservice/subdistrict/list`, payload);

export const serviceListVillage = (payload) =>
  axios.post(`${apiUrl}/portalservice/village/list`, payload);

export const serviceListPostalCode = (payload) =>
  axios.post(`${apiUrl}/portalservice/getpostalcodelist`, payload);

export const serviceGetSegmentasiRegistrastion = (payload) =>
  axios.post(`${apiUrl}/portalservice/registration/segmentation/get`, payload);

export const serviceGetInfoCurrency = (payload) =>
  axios.post(`${apiUrl}/portalservice/getinfocurrency`, payload);

export const serviceConsolidationCorporate = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/report/getcorporate`, payload, {
    headers: {
      menuid: 37,
    },
  });

export const serviceConsolidationFinancial = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/report/getfinancial`, payload, {
    headers: {
      menuid: 37,
    },
  });

export const serviceConsolidationNonFinancial = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/getnonfinancial`,
    payload,
    {
      headers: {
        menuid: 37,
      },
    }
  );

export const serviceConsolidationCompanyLogin = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/getcompanylogin`,
    payload,
    {
      headers: {
        menuid: 37,
      },
    }
  );

export const serviceConsolidationCompanyLoginDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/companylogindetail`,
    payload,
    {
      headers: {
        menuid: 37,
      },
    }
  );

export const serviceConsolidationCorporateDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/getcorporatedetail`,
    payload,
    {
      headers: {
        menuid: 37,
      },
    }
  );

export const serviceDownloadConsolidationCmpnyLogin = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/downloadcompanyloginreport`,
    payload,
    {
      responseType: "blob",
    }
  );

export const serviceDownloadConsolidationNonFinancial = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/downloadnonfinancialreport`,
    payload,
    {
      responseType: "blob",
    }
  );

export const serviceDownloadConsolidationFinancial = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/downloadfinancialreport`,
    payload,
    {
      responseType: "blob",
    }
  );

export const serviceDownloadConsolidationLimitUsage = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitusage/download`, payload, {
    responseType: "blob",
  });

export const serviceWorkflowGroup = (payload) =>
  axios.post(`${apiUrl}/portalservice/workflowgroup/get`, payload);

export const serviceBlockUser = (payload) =>
  axios.post(`${apiUrl}/portalservice/user/blocked`, payload);

export const serviceUnBlockUser = (payload) =>
  axios.post(`${apiUrl}/portalservice/user/unblocked`, payload);

export const serviceBlockedDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/user/blocked/detail`, payload);

export const serviceEndSession = (payload) =>
  axios.post(`${apiUrl}/portalservice/user/endsession`, payload);

export const serviceContentManagementGet = (payload) =>
  axios.post(`${apiUrl}/portalservice/contentmanagement/get`, payload);

export const serviceContentManagementSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/contentmanagement/save`, payload);

export const serviceRequestDownloadStatusTransaksi = (payload) =>
  axios.post(`${apiUrl}/portalservice/downloadresi`, payload, {
    responseType: "blob",
  });

export const serviceValidateTaskPortal = (payload) =>
  axios.post(`${apiUrl}/portalservice/validatetaskportal`, payload);

export const serviceSegmentationDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getsegmentationdetail`, payload);

export const serviceBranchDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbranchdetail`, payload);

export const serviceRegisLimitPackageDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getlimitpackagedetail`, payload);

export const serviceRegisTnc = (payload) =>
  axios.post(`${apiUrl}/portalservice/gettnc`, payload);

export const serviceChargeListNoPaging = (payload) =>
  axios.post(`${apiUrl}/portalservice/servicecharges/getlist`, payload);

export const serviceNotifInformation = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/getnotificationportalpagination`,
    payload
  );

export const serviceNotifInformationDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getnotificationportaldetail`, payload);

export const serviceCalendaerEventList = (payload) =>
  axios.post(`${apiUrl}/portalservice/calendarevent/get`, payload);

export const serviceCalendaerEventDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/calendarevent/detail`, payload);

export const serviceCalendarEventAdd = (payload) =>
  axios.post(`${apiUrl}/portalservice/calendarevent/add`, payload);

export const serviceCalendarEventEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/calendarevent/edit`, payload);

export const serviceCalendarEventDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/calendarevent/delete`, payload);

export const servicePromoCategory = (payload) =>
  axios.post(`${apiUrl}/portalservice/getpromocategorypaging`, payload);

export const serviceGetPromoCategory = (payload) =>
  axios.post(`${apiUrl}/portalservice/getpromocategory`, payload);

export const servicePromoType = (payload) =>
  axios.post(`${apiUrl}/portalservice/getpromotype`, payload);

export const servicePromoCategorySave = (payload) =>
  axios.post(`${apiUrl}/portalservice/savepromocategory`, payload);

export const servicePromoTypeSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/savepromotype`, payload);

export const serviceMerchantList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getmerchantpage`, payload);

export const serviceSyncMerchantList = (payload) =>
  axios.post(`${apiUrl}/portalservice/syncmerchantdata`, payload);

export const serviceManagementPromoList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getpromotionpaging`, payload);

export const serviceManagementPromoDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getpromotionbyid`, payload);

export const serviceManagementPromoSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/storepromotion`, payload);

export const serviceManagementPromoGetMerchants = (payload) =>
  axios.post(`${apiUrl}/portalservice/getallmerchant`, payload);

export const serviceManagementPromoEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/storeupdatepromotion`, payload);

export const serviceManagementPromoCategory = (payload) =>
  axios.post(`${apiUrl}/portalservice/getgeneralparameter`, payload);

export const serviceManagementPromoDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/deletepromotion`, payload);

export const serviceReportLimitUsage = (payload) =>
  axios.post(`${apiUrl}/portalservice/limitusage/get`, payload);

export const serviceReportFinancialDetail = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/corporate/report/getdetailfinancial`,
    payload
  );

export const serviceConsolidationSofFilter = (payload) =>
  axios.post(`${apiUrl}/portalservice/corporate/sof/get`, payload);

// Online Submission Report
export const serviceVirtualAccountReport = (payload) =>
  axios.post(`${apiUrl}/portalservice/virtualaccountreport/get`, payload);

export const serviceVAReportDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/virtualaccountreport/detail`, payload);

export const serviceAutoCollectionReport = (payload) =>
  axios.post(`${apiUrl}/portalservice/autocollection/report/get`, payload);

export const serviceAutoCollectionReportDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/autocollection/report/detail`, payload);

// ------------ BLAST PROMO START ---------
export const serviceNotifications = (payload) =>
  axios.post(
    `${apiUrl}/portalservice/blastpromo/getallblastnotificationpaging`,
    payload
  );

export const serviceNotificationsSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/blastpromo/requestcreate`, payload, {
    headers: {
      menuid: 387,
    },
  });

export const serviceNotificationsDetail = (payload) =>
  axios.post(`${apiUrl}/portalservice/getnotificationsdetail`, payload);

export const serviceGetAllPromosTitle = (payload) =>
  axios.post(`${apiUrl}/portalservice/findallpromotion`, payload);

// ------------ BLAST PROMO END ---------

// ------------ PAYMENT SETUP START ---------
export const serviceBillerCategoryList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbillercategorypage`, payload);

export const serviceBillerCategoryDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestdeletebillercategory`, payload);

export const serviceBillerCategoryEdit = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestsavebillercategory`, payload);

export const serviceBillerCategoryGetCategories = () =>
  axios.post(`${apiUrl}/portalservice/getservicecategory`);

export const serviceBillerManagerList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbillermanagerpage`, payload);

export const serviceBillerManagerCategoriesGet = () =>
  axios.post(`${apiUrl}/portalservice/getchoosecategorylist`);

export const serviceBillerManagerTransactionCategoriesGet = () =>
  axios.post(`${apiUrl}/portalservice/gettransactioncategorylist`);

export const serviceBillerManagerSpecGet = () =>
  axios.post(`${apiUrl}/portalservice/getchoosespeclist`);

export const serviceBillerManagerTransactionGroupGet = () =>
  axios.post(`${apiUrl}/portalservice/gettransactiongrouplist`);

export const serviceBillerManagerGetByPayeeCode = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbillerproductbypayeecode`, payload);

export const serviceBillerManagerGetPrefixByPayeeCode = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbillerprefixbypayeecode`, payload);

export const serviceBillerManagerGetDenomListByPayeeCode = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbillerdenombypayeecode`, payload);

export const serviceBillerManagerGetTemplateListByPayeeCode = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbillertemplatebypayeecode`, payload);

export const serviceSaveBillerManager = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestsavebillermanager`, payload);

export const serviceSaveBillerManagerDelete = (payload) =>
  axios.post(`${apiUrl}/portalservice/requestdeletebillermanager`, payload);
// ------------ PAYMENT SETUP END ---------

// ENHANCE KODE POST AUTO FILL GET DATA LIST FROM registrationservice
export const serviceListProvinceRegis = (payload) =>
  axios.post(`${apiUrl}/registrationservice/province/list`, payload);

export const serviceListDistrictRegis = (payload) =>
  axios.post(`${apiUrl}/registrationservice/district/list`, payload);

export const serviceListSubDistrictRegis = (payload) =>
  axios.post(`${apiUrl}/registrationservice/subdistrict/list`, payload);

export const serviceListVillageRegis = (payload) =>
  axios.post(`${apiUrl}/registrationservice/village/list`, payload);

export const serviceRegisNearBranchRegis = (payload) =>
  axios.post(`${apiUrl}/registrationservice/getnearestbranch`, payload);

export const serviceBranchDetailRegis = (payload) =>
  axios.post(`${apiUrl}/portalservice/getbranchdetail`, payload);

export const downloadFileService = (payload) =>
  axios.post(`${apiUrl}/portalservice/downloadfile`, payload, {
    responseType: "blob",
  });

export const serviceGetFaq = (payload) =>
  axios.post(`${apiUrl}/portalservice/faq/get`, payload, {});

export const serviceGetFaqAdminBank = (payload) =>
  axios.post(`${apiUrl}/portalservice/faq/findall`, payload, {});

export const serviceFaqSave = (payload) =>
  axios.post(`${apiUrl}/portalservice/faq/save`, payload, {});

/* ------------------------------- HARD TOKEN ------------------------------- */
export const serviceCorporateList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getCorporateList`, payload);

export const serviceCorporateTokenList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getCorporateTokenList`, payload);

export const serviceCorporateUserList = (payload) =>
  axios.post(`${apiUrl}/portalservice/getCorporateUserList`, payload);

export const serviceBindToken = (payload) =>
  axios.post(`${apiUrl}/portalservice/bindtoken`, payload);

export const serviceUnbindToken = (payload) =>
  axios.post(`${apiUrl}/portalservice/unbindtoken`, payload);
