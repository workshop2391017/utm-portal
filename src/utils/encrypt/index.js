import crypto from "crypto";
/* eslint-disable import/prefer-default-export */
const publicKeyPassword =
  "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDE70mr7SEMIvUuLKf1g3/UdIVTVnpD7niRC/P4eT+fkn7YN7xLv9Su5MJ1ths+W/IsP0LVC5k+ZOUSV62rMsmQrhgF0xz2kG1RfUpX0DfTzsYNzMKC5ydx3K1aomDent16SWSTmCXDtDIw+YOmwltVhV55zoD9ylOVlnvkj0NutQIDAQAB\n-----END PUBLIC KEY-----";

const publicKeyOTP =
  "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDAM0xcg+ye+VJgvp/25Ls9pD4ROLr0HZvKWNHMXl3ENq9pC+jFb/RxUCn+nVtHT8ARnfMjb/V9zDj6dI14l5sG68aQKKV8bzUBfEdyTutCZmH71LWje5Rgidu3VsrnET/muvDU8PIcF0TUZysfySq2nWuJzOb4LYSF3sPeCqIjewIDAQAB\n-----END PUBLIC KEY-----";

export const encryptPassword = (password) => {
  const buffer = Buffer.from(password, "utf8");

  return crypto
    .publicEncrypt(
      {
        key: publicKeyPassword,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
      },
      buffer
    )
    .toString("base64");
};

export const decryptPassword = (password) => {
  const buffer = Buffer.from(password, "base64");

  const decrypted = crypto.publicDecrypt(
    {
      key: publicKeyPassword,
      padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
    },
    buffer
  );
  return decrypted.toString("utf-8");
};

export const decryptOTP = (challengeCode) => {
  const buffer = Buffer.from(challengeCode, "base64");

  const decrypted = crypto.publicDecrypt(
    {
      key: publicKeyOTP,
      padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
    },
    buffer
  );
  return decrypted.toString("utf-8");
};

export const encryptOTP = (softToken) => {
  const buffer = Buffer.from(softToken, "utf8");

  return crypto
    .publicEncrypt(
      {
        key: publicKeyOTP,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
      },
      buffer
    )
    .toString("base64");
};

const cipherAlg = "aes-128-cbc";
let encoding = "base64";
const dataIV = "123456PFO0123456";
export const keyData = "1234567890123456";

export const getKey = (key) => (key instanceof Buffer ? key : Buffer.from(key));

export const encryptText = (text, key) => {
  if (!key || !text) return false;
  // const data = getKey(key);
  const cipher = crypto.createCipheriv(
    cipherAlg,
    getKey(key),
    Buffer.from(dataIV)
  );

  encoding = encoding || "binary";

  let result = cipher.update(text, "utf8", encoding);

  result += cipher.final(encoding);

  return result;
};

export const decryptText = (text, key) => {
  if (!key || !text) return false;
  // const data = getKey(key);

  const decipher = crypto.createDecipheriv(
    cipherAlg,
    getKey(key),
    Buffer.from(dataIV)
  );

  encoding = encoding || "binary";

  let result = decipher.update(text, encoding);
  result += decipher.final();

  return result;
};

// const value = [
//   {
//     label: "Admin Maker Profile",
//     key: "userProfileAdminMaker",
//     role: "USER_MAKER",
//     userId: "Admin1",
//   },
//   {
//     label: "Admin Approver Profile",
//     key: "userProfileAdminApproval",
//     role: "USER_APPROVER",
//     userId: "Admin2",
//   },
// ];

// const decryptValue = JSON.parse(
//   decryptText(
//     "FIxq4TMPrKr6TzaJ0goUvo80hJzvZM/kkpAFcTM8hyROilg4K/x4YG1ptsVsGTL44PeOfAiY/IwBmSgWw4DmfgYV7I3uTkVj8MfdZbmd+RwexpeJWy55oxbfpNhYYFJadoeFQuhlxBXUvYC4UC6rrALCBuGhP3VLlagZ0FBx7k17EgQT0CgxrtdZorAKy2ImwgLC0aYa1l8kmWL90v47y/XEHmoOHRLG34hCQu4Su+8jdsUjQC8CxLmRA1fpFw8rdLT/VQhjVGV2NgLgRDiFm5Bv50sjY6QX51UdPZ1GJ24=",
//     keyData
//   )
// );

// console.warn("ini encrypt hasil", encryptText(JSON.stringify(value), keyData));
// console.warn(
//   "ini decrypt",
//   decryptText(
//     "FIxq4TMPrKr6TzaJ0goUvo80hJzvZM/kkpAFcTM8hyROilg4K/x4YG1ptsVsGTL44PeOfAiY/IwBmSgWw4DmfgYV7I3uTkVj8MfdZbmd+RwexpeJWy55oxbfpNhYYFJadoeFQuhlxBXUvYC4UC6rrALCBuGhP3VLlagZ0FBx7k17EgQT0CgxrtdZorAKy2ImwgLC0aYa1l8kmWL90v47y/XEHmoOHRLG34hCQu4Su+8jdsUjQC8CxLmRA1fpFw8rdLT/VQhjVGV2NgLgRDiFm5Bv50sjY6QX51UdPZ1GJ24=",
//     keyData
//   ),
//   decryptValue
// );
