import React, { useState, useEffect } from "react";
import { Button, makeStyles } from "@material-ui/core";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { setHandleClearError } from "stores/actions/managementuser";
import Title from "components/BN/Title";
import TableICBB from "components/BN/TableIcBB";
import Toast from "components/BN/Toats";
import trash from "assets/icons/BN/trash-21.svg";
import {
  getCorporateTokenList,
  unBindToken,
} from "stores/actions/managementHardToken";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as SvgPlus } from "assets/icons/BN/plus-white.svg";
import { useHistory } from "react-router-dom";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

const HardToken = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const router = useHistory();
  const [selectedDataToDelete, setSelectedDataToDelete] = useState({});
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const [showSuccessConfirmation, setShowSuccessConfirmation] = useState(false);
  const [page, setPage] = useState(1);

  const {
    isLoading,
    error,
    corporateTokenList: { content, totalElements, totalPages },
  } = useSelector(
    ({ managementHardToken }) => managementHardToken,
    shallowEqual
  );

  /* --------------------------------- EFFECT --------------------------------- */
  useEffect(() => {
    handleGetCorporateTokenList();
  }, [page]);

  /* ---------------------------- HANDLER FUNCTIONS --------------------------- */
  const handleGetCorporateTokenList = () => {
    dispatch(getCorporateTokenList({ pageIndex: page - 1, pageSize: 10 }));
  };

  const handleClickDelete = (data) => {
    setSelectedDataToDelete(data);
    setShowDeleteConfirmation(true);
  };

  const handleDeleteUser = (data) => {
    const payload = {
      corporateId: data.corporateTokenId,
      serialNumber: data.secretToken,
      userId: data.userProfileId,
    };
    dispatch(unBindToken(payload, handleShowSuccessConfirmation));
  };

  const handleShowSuccessConfirmation = () => setShowSuccessConfirmation(true);

  /* ------------------------------ TABLE CONFIG ------------------------------ */
  const tableConfig = (onClickDelete) => [
    {
      title: "Username",
      headerAlign: "left",
      align: "left",
      key: "username",
    },
    {
      title: "Company",
      headerAlign: "left",
      align: "left",
      key: "corporateName",
    },
    {
      title: "Hard Token",
      headerAlign: "left",
      align: "left",
      key: "secretToken",
    },
    {
      title: "",
      headerAlign: "right",
      width: "50px",
      fontFamily: "FuturaMdBT",
      render: (rowData) => (
        <Button
          startIcon={<img src={trash} alt="Menu" />}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          className={classes.menuButton}
          onClick={() => onClickDelete(rowData)}
        />
      ),
    },
  ];

  /* ---------------------------------- VIEWS --------------------------------- */
  return (
    <div className={classes.billerManager}>
      <Title label="Hard Token">
        <GeneralButton
          label="Bind Hard Token"
          iconPosition="startIcon"
          buttonIcon={<SvgPlus width={16.67} height={16.67} />}
          width="182px"
          height="44px"
          onClick={() => router.push("/hard-token/bind")}
        />
      </Title>
      <div className={classes.container}>
        <TableICBB
          headerContent={tableConfig(handleClickDelete)}
          dataContent={content}
          isLoading={isLoading}
          totalElement={totalElements}
          totalData={totalPages}
          page={page}
          setPage={setPage}
        />
      </div>

      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />

      <DeletePopup
        isOpen={showDeleteConfirmation}
        handleClose={() => setShowDeleteConfirmation(false)}
        onContinue={() => handleDeleteUser(selectedDataToDelete)}
        title="Confirmation"
        message={`Are You Sure To Delete Hard Token Data ${selectedDataToDelete.username}?`}
      />

      <SuccessConfirmation
        isOpen={showSuccessConfirmation}
        title="Success"
        message="Hard Token data has been deleted"
        handleClose={() => {
          setShowDeleteConfirmation(false);
          setShowSuccessConfirmation(false);
          handleGetCorporateTokenList();
        }}
      />
    </div>
  );
};

HardToken.propTypes = {};
HardToken.defaultProps = {};
export default HardToken;

/* ---------------------------------- STYLE --------------------------------- */
const useStyles = makeStyles({
  container: {
    padding: "20px 30px",
  },
  menuButton: {
    minWidth: 40,
    padding: 0,
    "& .MuiButton-label": {
      padding: 0,
      "& .MuiButton-startIcon": {
        margin: 0,
      },
    },
  },
});
