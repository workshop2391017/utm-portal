import { makeStyles } from "@material-ui/core";
import React, { useState } from "react";

// assets

import TableICBB from "components/BN/TableIcBB";
import Menu from "components/BN/Menus/MenuListPromo";
import { useDispatch, useSelector } from "react-redux";
import {
  handlePromoDelete,
  setPages,
  setPromoCode,
  setTablePage,
} from "stores/actions/promo";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import moment from "moment";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";

const SedangBerjalan = () => {
  const [deleteConfirmation, setDeleteConfirmation] = useState(false);
  const [successDelete, setSuccessDelete] = useState(false);
  const [deletePayload, setDeletePayload] = useState(null);

  const dispatch = useDispatch();
  const { isLoading, data, page, isExecute } = useSelector(
    (state) => state.promo
  );

  const useStyles = makeStyles({
    akanDatang: {},
    container: { paddingTop: "20px" },
  });

  const classes = useStyles();
  const dataHeader = [
    {
      title: "Promo Name",
      key: "promotionSubject",
    },
    {
      title: "Promo Code",
      key: "promotionCode",
    },

    {
      title: "Start Date",
      key: "startdate",
      render: (row) => moment(row?.startDate)?.format("DD-MM-YYYY | HH:mm:ss"),
    },
    {
      title: "End Date",
      key: "expired",
      render: (row) => moment(row?.endDate)?.format("DD-MM-YYYY | HH:mm:ss"),
    },
    {
      width: 50,
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
            }}
          >
            <div
              style={{
                display: "flex",

                justifyContent: "flex-end",
              }}
            >
              <Menu
                onDelete={() => {
                  // console.log("ini rowData", rowData);
                  setDeleteConfirmation(true);
                  setDeletePayload({
                    id: rowData?.id,
                    code: rowData?.promotionCode,
                    isDeleted: true,
                  });
                }}
                onEdit={() => {
                  dispatch(setPromoCode(rowData?.id));
                  dispatch(setPages(1));
                }}
                rowData={rowData}
              />
            </div>
          </div>
        </React.Fragment>
      ),
    },
  ];

  const handleDelete = () => {
    dispatch(
      handlePromoDelete(deletePayload, () => {
        setSuccessDelete(true);
        setDeleteConfirmation(false);
      })
    );
  };
  return (
    <div className={classes.akanDatang}>
      <DeletePopup
        isOpen={deleteConfirmation}
        onContinue={() => {
          dispatch(
            validateTask(
              {
                id: deletePayload.id,
                code: deletePayload.code,
                name: "DELETE",
                menuName: validateTaskConstant.LIST_PROMO,
              },
              {
                async onContinue() {
                  handleDelete();
                },
                onError() {
                  setDeleteConfirmation(false);
                },
              },
              {
                redirect: false,
              }
            )
          );
        }}
        handleClose={() => setDeleteConfirmation(false)}
        loading={isExecute}
      />
      <SuccessConfirmation
        isOpen={successDelete}
        submessage=""
        title="Please Wait For Approval"
        message="Deleted Successfully"
        handleClose={() => setSuccessDelete(false)}
      />
      <div className={classes.container}>
        <TableICBB
          isLoading={isLoading}
          collapse={page}
          headerContent={dataHeader}
          dataContent={data?.promotionList ?? []}
          page={page}
          setPage={(e) => dispatch(setTablePage(e))}
          totalData={data.pageTotal}
          totalElement={data.dataTotal}
        />
      </div>
    </div>
  );
};

SedangBerjalan.propTypes = {};

SedangBerjalan.defaultProps = {};

export default SedangBerjalan;
