// main
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {
  handlePromoList,
  setDetails,
  setPages,
  setPromoCode,
  setServiceAlready,
  setTablePage,
} from "stores/actions/promo";

// libraries
import { makeStyles, Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";

// components

import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Filter from "components/BN/Filter/GeneralFilter/periode";
import GeneralButton from "components/BN/Button/GeneralButton";
import NonaktifModal from "components/BN/Popups/Nonaktifkan";
import SuccesNonaktifkanModal from "components/BN/Popups/Nonaktifkan/SuccesNonaktifkan";
import useDebounce from "utils/helpers/useDebounce";

import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";
import { searchDateEnd, searchDateStart } from "utils/helpers";
import AkanDatang from "./AkanDatang";
import SedangBerjalan from "./SedangBerjalan";
import Selesai from "./Selesai";

const useStyles = makeStyles({
  manajemenPromo: {},
  container: {
    padding: "20px 30px 30px",
  },
  tabs: {
    "& .MuiTab-textColorPrimary.Mui-selected": {
      color: "#0061A7",
    },
    "& .MuiTab-textColorPrimary": {
      color: "#B3C1E7",
    },
  },
});

const defaultDataFilter = {
  date: { dateValue1: null, dateValue2: null },
  page: 1,
};

const ManajemenPromo = (props) => {
  const classes = useStyles();

  const [dataSearch, setDataSearch] = useState("");
  const dispatch = useDispatch();
  const { isLoading, data, page, promoCode, isExecute } = useSelector(
    (state) => state.promo
  );

  const [nonaktifModal, setNonaktifModal] = useState(false);
  const [succesNonaktifModal, setSuccesNonaktifModal] = useState(false);
  const [dataFilter, setDataFilter] = useState(defaultDataFilter);
  const debounc = useDebounce(dataSearch, 1300);

  const clickNonaktifContinue = () => {
    setNonaktifModal(false);
    setSuccesNonaktifModal(true);
  };

  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const getRenderPerusahaan = (list) => {
    switch (selectedIndex) {
      case 0:
        return <SedangBerjalan />;
      case 1:
        return <AkanDatang />;
      case 2:
        return <Selesai />;
      default:
        return null;
    }
  };
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    setDataFilter(defaultDataFilter);
  };

  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    const tabs = ["ongoing", "new", "done"];
    const payload = {
      search: debounc,
      startDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      endDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      pageNumber: dataFilter.page - 1,
      pageSize: 10,
      promoTab: tabs[selectedIndex],
    };
    dispatch(handlePromoList(payload));
  }, [dataFilter, selectedIndex, debounc]);

  useEffect(() => {
    setDataFilter({
      ...dataFilter,
      page,
    });
  }, [page]);

  return (
    <div className={classes.manajemenPromo}>
      <Title label="Promo List">
        <Search
          placeholder="Promo Name, Promo Code"
          style={{ width: "200px" }}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
        <GeneralButton
          label="Promo"
          iconPosition="startIcon"
          buttonIcon={<Plus />}
          width="109px"
          height="43px"
          style={{ marginLeft: "20px", marginTop: "8px" }}
          onClick={() => {
            if (promoCode) {
              dispatch(setPromoCode(null));
              dispatch(setDetails(null));
            }
            dispatch(setServiceAlready(false));
            dispatch(setPages(1));
          }}
        />
      </Title>

      {/* <Tabs
        className={classes.tabs}
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        left
        style={{ marginLeft: 20 }}
      >
        <Tab
          selected={selectedIndex === 0}
          onClick={(event) => handleListItemClick(event, 0)}
          label="On Going"
          style={{
            fontFamily: "FuturaMdBT",
            textTransform: "none",
            fontSize: 13,
            width: "content",
          }}
        />
        <Tab
          selected={selectedIndex === 1}
          onClick={(event) => handleListItemClick(event, 1)}
          label="New"
          style={{
            fontFamily: "FuturaMdBT",
            textTransform: "none",
            fontSize: 13,
            width: "content",
          }}
        />
        <Tab
          selected={selectedIndex === 2}
          onClick={(event) => handleListItemClick(event, 2)}
          style={{
            fontFamily: "FuturaMdBT",
            textTransform: "none",
            fontSize: 13,
            width: "content",
          }}
          label="Done"
        />
      </Tabs> */}
      <div className={classes.container}>
        {/* <Filter
          style={{ marginBottom: "20px" }}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
              dBlok: false,
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              dBlok: false,
            },
          ]}
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
        /> */}

        <div style={{ marginTop: 20 }}>
          <div style={{ marginTop: "-20px" }}>
            {/* {getRenderPerusahaan()} */}
            <SedangBerjalan />
          </div>
        </div>
      </div>
      <NonaktifModal
        isOpen={nonaktifModal}
        handleClose={() => setNonaktifModal(false)}
        onContinue={clickNonaktifContinue}
      />

      <SuccesNonaktifkanModal
        isOpen={succesNonaktifModal}
        handleClose={() => setSuccesNonaktifModal(false)}
        title="Berhasil"
      />
    </div>
  );
};

ManajemenPromo.propTypes = {};

ManajemenPromo.defaultProps = {};

export default ManajemenPromo;
