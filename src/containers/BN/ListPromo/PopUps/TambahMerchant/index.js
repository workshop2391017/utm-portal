// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";

import Grid from "@material-ui/core/Grid";

import DropdownAntd from "components/SC/Dropdown/DropdownAntd";
import CheckboxGroup from "components/SC/Checkbox/ChexboxGroup";
import TextField from "components/BN/TextField/AntdTextField";

// import Button from "@material-ui/core/Button";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Search from "components/BN/Search/SearchWithoutDropdown";
import DeletePopup from "components/BN/Popups/Delete";

import SuccessConfirm from "components/BN/Popups/BerhasilRekeningKelompok";
// assets

import checksvg from "assets/icons/BN/clipboard.svg";
import check from "assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    fontFamily: "FuturaBkBT",
    fontWeight: "400",
    fontSize: 13,
    width: "458px",
    textAlign: "left",
    marginTop: "22px",
  },
  paper: {
    width: 518,
    height: 584,
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const TambahMerchant = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();

  const [dropdownValue, setDropdownValue] = useState(
    "Pilih kategori preferensi"
  );
  const [checkboxGroup, setCheckboxGroup] = useState([
    "Platinum",
    "Regular",
    "Kids",
    "Gold",
    "Prioritas",
    " ",
  ]);
  const [modalLimit, setModalLimit] = useState(false);

  const [successConfirm, setSuccessConfirm] = useState(false);
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  // useEffect(() => {
  //   if (isOpen) {
  //     // setTimeout(() => {
  //     //   handleClose();
  //     // }, 2500);
  //   }
  // }, [handleClose, isOpen]);

  const handleCloseModal = () => {
    handleClose();
    // setCheckList([]);
  };

  const handleOpenConfirm = () => {
    handleCloseModal();
    setSuccessConfirm(true);
  };

  return (
    <div>
      <SuccessConfirm
        isOpen={successConfirm}
        handleClose={() => setSuccessConfirm(false)}
        message="Berhasil Menambah Preferensi Data"
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 2500,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid item xs={12}>
              <Typography
                style={{
                  color: "#374062",
                  fontSize: "22px",
                  fontWeight: "400",
                  fontFamily: "FuturaHvBt",
                  textAlign: "left",
                  // marginLeft: 40,
                }}
              >
                Add Merchant
              </Typography>
              <div width="458px">
                <div className={classes.card}>
                  <span>Merchant Type :</span>
                  <DropdownAntd
                    value={dropdownValue}
                    options={[
                      "Pilih kategori preferensi",
                      "Pilih kategori preferensi2",
                      "Pilih kategori preferensi3",
                      "Pilih kategori preferensi4",
                    ]}
                    onChange={(e) => setDropdownValue(e)}
                    style={{
                      width: 458,
                      borderRadius: "10px",
                      textAlign: "left",
                      marginBottom: "10px",
                      marginTop: "4px",
                      // fontFamily: "FuturaBkBT",
                      // fontSize: "13px",
                    }}
                  />
                </div>

                <Search
                  style={{
                    width: 458,
                    textAlign: "left",
                    marginBottom: "10px",
                    marginTop: "8px",
                  }}
                  marginBottom="20px"
                  options={["Gold", "Platinum"]}

                  //   dataSearch={dataSearch}
                  //   setDataSearch={setDataSearch}
                />
              </div>
              <div>
                <div
                  style={{
                    marginTop: "10px",
                    width: "458px",
                    borderRadius: "10px",
                    height: "252px",
                    border: "1px solid #BCC8E7",
                    alignContent: "space-between",
                    // marginLeft: 30,
                  }}
                >
                  <Grid container spacing={4}>
                    <Grid item xs={12} sm={6}>
                      <div
                        style={{
                          marginTop: "10px",
                          marginLeft: 50,
                          width: "100px",
                          textAlign: "left",
                        }}
                      >
                        <CheckboxGroup
                          value={checkboxGroup}
                          options={["Platinum", "Regular", "Kids"]}
                          onChange={(e) => setCheckboxGroup(e)}
                          direction="vertical"
                          margin="10px"
                          // alignText="left"
                        />
                      </div>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <div
                        style={{
                          marginTop: "10px",
                          marginLeft: 30,
                          textAlign: "left",
                        }}
                      >
                        <CheckboxGroup
                          value={checkboxGroup}
                          options={["Gold", "Prioritas", " "]}
                          onChange={(e) => setCheckboxGroup(e)}
                          direction="vertical"
                          margin="10px"
                        />
                      </div>
                    </Grid>
                  </Grid>
                </div>

                <ButtonOutlined
                  label="Cancel"
                  // iconPosition="startIcon"
                  // buttonIcon={<Plus />}
                  width="82px"
                  height="44px"
                  style={{
                    position: "absolute",
                    marginTop: 45,
                    left: 30,
                  }}
                  // onClick={() => setModal(true)}
                />
                <GeneralButton
                  label="Add"
                  // iconPosition="startIcon"
                  // buttonIcon={<Plus />}
                  width="92px"
                  height="43px"
                  style={{
                    position: "absolute",
                    marginTop: 45,
                    // marginRight: 100,
                    // position: "relative",
                    // position: 'absolute',
                    // bottom: '8px',
                    right: "30px",
                    // marginLeft: 30
                  }}
                  onClick={handleOpenConfirm}
                />
              </div>
            </Grid>

            <DeletePopup
              isOpen={modalLimit}
              handleClose={() => setModalLimit(false)}
              // onContinue={handleBerhasil}
              title="Konfirmasi"
              message="Anda Yakin Menyetujui Data?"
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

TambahMerchant.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

TambahMerchant.defaultProps = {
  title: "Transfer",
  message: "Atur Limit Rekening",
};

export default TambahMerchant;
