// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import { Typography } from "antd";

import Grid from "@material-ui/core/Grid";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Search from "components/BN/Search/SearchWithoutDropdown";
import DeletePopup from "components/BN/Popups/Delete";

import { useDispatch, useSelector } from "react-redux";
import { handleSegmentationGet, setSegmentation } from "stores/actions/promo";
import ScrollCustom from "components/BN/ScrollCustom";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import Colors from "helpers/colors";
import useDebounce from "utils/helpers/useDebounce";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    fontFamily: "FuturaBkBT",
    fontWeight: "400",
    fontSize: 13,
    width: "458px",
    textAlign: "left",
    marginTop: "22px",
  },
  paper: {
    width: 518,
    height: "auto",
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },

  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 25,
  },
}));

export const NoData = () => (
  <p
    style={{
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      color: Colors.gray.medium,
    }}
  >
    No Data
  </p>
);

const TambahPromoType = ({
  isOpen,
  handleClose,
  title,
  selectSegmentasi,
  setSelectSegmentasi,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [selected, setSelected] = useState([]);

  const { segmentation, isLoadingCategory, detail } = useSelector(
    (e) => e.promo
  );

  useEffect(() => {
    setSelected(selectSegmentasi);
  }, [selectSegmentasi]);

  const [dataSearch, setDataSearch] = useState("");
  const searchDebounced = useDebounce(dataSearch, 1000);
  const [segmentsToShow, setSegmentsToShow] = useState([]);

  const [modalLimit, setModalLimit] = useState(false);

  const handleCloseModal = () => {
    handleClose();
  };

  const onCancel = () => {
    handleCloseModal();
  };

  const handleOpenConfirm = () => {
    setSelectSegmentasi(selected);
    handleCloseModal();
  };

  useEffect(() => {
    const payload = {
      accountNumber: "",
      segmentationName: null,
    };
    dispatch(handleSegmentationGet(payload, detail));
  }, [detail]);

  const clickSegmentasi = (segmentation) => {
    const select = [...selected];
    if (selected?.findIndex((fi) => fi.name === segmentation.name) !== -1) {
      setSelected(selected.filter((e) => e.name !== segmentation.name));
    } else {
      select.push(segmentation);
      setSelected(select);
    }
  };

  const handleCheck = (e) => selected?.findIndex((fi) => fi.name === e) !== -1;
  // console.log("ini selected", selected);
  useEffect(() => {
    if (
      searchDebounced?.length > 2 ||
      searchDebounced?.length === 0 ||
      searchDebounced === null
    ) {
      // do filter data to show
      const filteredItems = (segmentation ?? [])?.filter((e) =>
        e?.name?.toLowerCase()?.includes(dataSearch?.toLowerCase())
      );
      const limit100Items = filteredItems?.slice(0, 100);
      setSegmentsToShow(limit100Items);
    }
  }, [searchDebounced, segmentation]);
  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid item xs={12}>
              <Typography
                style={{
                  color: "#374062",
                  fontSize: "22px",
                  fontWeight: "400",
                  fontFamily: "FuturaHvBt",
                  textAlign: "center",
                }}
              >
                Add Segmentation
              </Typography>
              <div width="458px">
                <Search
                  style={{
                    width: 458,
                    textAlign: "left",
                    marginBottom: "10px",
                    marginTop: "8px",
                  }}
                  marginBottom="20px"
                  dataSearch={dataSearch}
                  setDataSearch={setDataSearch}
                  placeholder={false}
                />
              </div>
              <div>
                <div
                  style={{
                    marginTop: "10px",
                    width: "458px",
                    borderRadius: "10px",
                    height: "332px",
                    border: "1px solid #BCC8E7",
                  }}
                >
                  <ScrollCustom height="332px">
                    <Grid
                      container
                      spacing={3}
                      style={{ padding: "15px 30px" }}
                    >
                      {segmentsToShow?.length > 0 ? (
                        (segmentsToShow ?? []).map((e, i) => (
                          <Grid
                            item
                            xs={6}
                            sm={6}
                            key={i}
                            style={{
                              display: "flex",
                              width: "100%",
                              justifyContent: "start",
                            }}
                          >
                            <CheckboxSingle
                              checked={handleCheck(e?.name)}
                              label={e.name}
                              onChange={() => {
                                // console.log("ini segmentation", e);
                                clickSegmentasi(e);
                              }}
                            />
                          </Grid>
                        ))
                      ) : (
                        <NoData />
                      )}
                    </Grid>
                  </ScrollCustom>
                </div>
              </div>
            </Grid>

            <DeletePopup
              isOpen={modalLimit}
              handleClose={() => setModalLimit(false)}
              // onContinue={handleBerhasil}
              title="Konfirmasi"
              message="Anda Yakin Menyetujui Data?"
            />
            <div className={classes.buttonGroup}>
              <ButtonOutlined
                label="Cancel"
                width="216px"
                height="44px"
                onClick={onCancel}
              />
              <GeneralButton
                label="Add"
                width="216px"
                height="44px"
                onClick={handleOpenConfirm}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

TambahPromoType.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
};

TambahPromoType.defaultProps = {
  title: "Transfer",
};

export default TambahPromoType;
