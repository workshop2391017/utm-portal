// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";

import Grid from "@material-ui/core/Grid";

import DropdownAntd from "components/SC/Dropdown/DropdownAntd";
import CheckboxGroup from "components/SC/Checkbox/ChexboxGroup";
import TextField from "components/BN/TextField/AntdTextField";

// import Button from "@material-ui/core/Button";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Search from "components/BN/Search/SearchWithoutDropdown";
import DeletePopup from "components/BN/Popups/Delete";

import SuccessConfirm from "components/BN/Popups/BerhasilRekeningKelompok";
// assets

import checksvg from "assets/icons/BN/clipboard.svg";
import check from "assets/images/BN/illustration.png";
import { useDispatch, useSelector } from "react-redux";
import {
  actionGetMerchants,
  getPromoCategory,
  handlePromoCategory,
  setMerchants,
  setPromoCategory,
} from "stores/actions/promo";
import ScrollCustom from "components/BN/ScrollCustom";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import useDebounce from "utils/helpers/useDebounce";
import { NoData } from "../popupsSegmentation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    fontFamily: "FuturaBkBT",
    fontWeight: "400",
    fontSize: 13,
    width: "458px",
    textAlign: "left",
    marginTop: "22px",
  },
  paper: {
    width: 518,
    height: "auto",
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },

  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 25,
  },
}));

const PopupMerchants = ({
  isOpen,
  handleClose,
  title,
  selectMerchants,
  setSelectMerchants,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { promoCategory, merchants, isLoadingCategory, detail } = useSelector(
    (e) => e.promo
  );

  const [modalLimit, setModalLimit] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [selected, setSelected] = useState([]);

  const searchDebounced = useDebounce(dataSearch, 1000);
  //   console.log("ini selected", selected);
  const handleCloseModal = () => {
    handleClose();
  };

  const onCancel = () => {
    handleCloseModal();
  };

  const handleOpenConfirm = () => {
    setSelectMerchants(selected);
    handleCloseModal();
  };

  useEffect(() => {
    setSelected(selectMerchants);
  }, [selectMerchants]);

  useEffect(() => {
    // const payload = {
    //   direction: "DESC",
    //   pageNumber: 0,
    //   pageSize: 1,
    //   sortBy: "id",
    //   module: "PROMOTION",
    // };
    // dispatch(handlePromoCategory(payload, detail));
    // const payload = {
    //   search: searchDebounced,
    // };
    // if (searchDebounced) {
    //   const filteredMerchants = merchants.filter(({ merchantName }) =>
    //     merchantName.toLowerCase().includes(searchDebounced.toLowerCase())
    //   );
    //   dispatch(setMerchants(filteredMerchants));
    // } else {
    //   dispatch(actionGetMerchants());
    // }
    const payload = {
      pageSize: 100,
      search: searchDebounced,
    };
    if (
      searchDebounced?.length > 2 ||
      searchDebounced?.length === 0 ||
      searchDebounced === null
    ) {
      dispatch(actionGetMerchants(payload));
    }
  }, [searchDebounced]);

  const clickCategory = (category) => {
    const select = [...selected];
    if (
      selected?.findIndex((fi) => fi.merchantId === category.merchantId) !== -1
    ) {
      setSelected(select.filter((e) => e.merchantId !== category.merchantId));
    } else {
      select.push(category);
      setSelected(select);
    }
  };

  const handleCheck = (e) =>
    selected?.findIndex((fi) => fi.merchantId === e) !== -1;
  // console.log("ini selected", selected);
  // console.log("ini merchants", merchants);
  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid item xs={12}>
              <Typography
                style={{
                  color: "#374062",
                  fontSize: "22px",
                  fontWeight: "400",
                  fontFamily: "FuturaHvBt",
                  textAlign: "center",
                }}
              >
                Add Merchants
              </Typography>
              <div width="458px">
                <Search
                  style={{
                    width: 458,
                    textAlign: "left",
                    marginBottom: "10px",
                    marginTop: "8px",
                  }}
                  marginBottom="20px"
                  dataSearch={dataSearch}
                  setDataSearch={setDataSearch}
                  placeholder={false}
                />
              </div>
              <div>
                <div
                  style={{
                    marginTop: "10px",
                    width: "458px",
                    borderRadius: "10px",
                    height: "332px",
                    border: "1px solid #BCC8E7",
                  }}
                >
                  <ScrollCustom height="332px">
                    <Grid
                      container
                      spacing={3}
                      style={{ padding: "15px 30px" }}
                    >
                      {merchants.length > 0 ? (
                        merchants?.map((e, i) => (
                          <Grid
                            item
                            xs={6}
                            sm={6}
                            key={i}
                            style={{
                              display: "flex",
                              width: "100%",
                              justifyContent: "start",
                            }}
                          >
                            <CheckboxSingle
                              checked={handleCheck(e.merchantId)}
                              label={e.merchantName}
                              onChange={() => {
                                clickCategory(e);
                              }}
                            />
                          </Grid>
                        ))
                      ) : (
                        <NoData />
                      )}
                    </Grid>
                  </ScrollCustom>
                </div>
              </div>
            </Grid>

            <DeletePopup
              isOpen={modalLimit}
              handleClose={() => setModalLimit(false)}
              // onContinue={handleBerhasil}
              title="Konfirmasi"
              message="Anda Yakin Menyetujui Data?"
            />
            <div className={classes.buttonGroup}>
              <ButtonOutlined
                label="Cancel"
                width="216px"
                height="44px"
                onClick={onCancel}
              />
              <GeneralButton
                label="Add"
                width="216px"
                height="44px"
                onClick={handleOpenConfirm}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupMerchants.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
};

PopupMerchants.defaultProps = {
  title: "Transfer",
};

export default PopupMerchants;
