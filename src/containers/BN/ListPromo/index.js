import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import { setPages } from "stores/actions/promo";
import Home from "./Home";
import UbahPromo from "./TambahPromo";

const ListPromo = (props) => {
  const dispatch = useDispatch();
  const { pages } = useSelector((state) => state.promo);

  useEffect(() => {
    dispatch(setPages(0));
  }, []);

  return (
    <React.Fragment>{pages === 0 ? <Home /> : <UbahPromo />}</React.Fragment>
  );
};

ListPromo.propTypes = {};

export default ListPromo;
