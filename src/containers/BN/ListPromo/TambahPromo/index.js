/* eslint-disable array-callback-return */
/* eslint-disable no-new */
// main
import { Button, makeStyles, CircularProgress } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { ReactComponent as Plusmini } from "assets/icons/BN/plusmini.svg";
import React, { Fragment, useContext, useEffect, useState } from "react";
import Compressor from "compressorjs";
// components
import Title from "components/BN/Title";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DatePickerWhite from "components/BN/DatePicker/WhiteDatePicker";
import InputImage from "components/BN/Inputs/InputImage";

import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import TagsGroup from "components/BN/Tags/TagsGroupPromo";
import TextField from "components/BN/TextField/AntdTextField";
import PopupsSegmentasi from "containers/BN/ListPromo/PopUps/popupsSegmentation";
import PopupsPromoCategory from "containers/BN/ListPromo/PopUps/popupsCategory";
import PopupsLocation from "containers/BN/ListPromo/PopUps/popupsLocation";

import DeletePopup from "components/BN/Popups/Delete";

import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import RichText from "components/BN/RichText";
import { useDispatch, useSelector } from "react-redux";
import {
  handlePromoEdit,
  handlePromoLDetail,
  setLoading,
  handlePromoSave,
  setBranch,
  setPages,
  setPromoCategory,
  setDetails,
  setPromoCode,
  getValidationService,
  setServiceAlready,
} from "stores/actions/promo";
import FailedConfirmation from "components/BN/Popups/FailedConfirmationData";
import FormSectionTitle from "components/BN/Form/formSectionTitle";
import Colors from "helpers/colors";
import FormField from "components/BN/Form/InputGroupValidation";
import { blobToBase64 } from "utils/helpers/imageToBase64";
import moment from "moment";
import { getLocalStorage, isValidUrl } from "utils/helpers";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import useDebounce from "utils/helpers/useDebounce";
import RichTextSunEditor from "components/BN/RichTextSunEditor";
import PopupMerchants from "../PopUps/popupsMerchants";

const dateNow = moment();
const dates = dateNow.format("DD/MM/YYYY");
const defaultErrorMaxBefore = {
  isOpen: false,
  message:
    "A maximum of 5 promos that are displayed before logging in on that day",
};

const UbahPromo = (props) => {
  const useStyles = makeStyles({
    ubahPromo: {},
    paper1: {
      height: "auto",
      backgroundColor: "#fff",
      padding: 30,
      borderRadius: 10,
      marginBottom: 150,
      boxShadow: "none",
    },
    card: {
      position: "relative",
      fontFamily: "FuturaBQ",
      fontWeight: "400",
      fontSize: 13,
    },
    paper21: {
      height: 84,
      display: "flex",
      justifyContent: "space-between",
      padding: "20px 22px",
      backgroundColor: "white",
      position: "fixed",
      left: 200,
      right: 0,
      bottom: 0,
      zIndex: 30,
    },
    paper2: {
      display: "flex",
      alignItems: "center",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "89%",
      height: "100px",
      backgroundColor: "#fff",
      position: "absolute",
      bottom: "0px",
    },
    textfield: {
      height: 40,
      backgroundColor: "#fff",
      paddingVertical: 10,
      paddingHorizontal: 15,
      borderColor: "#ccc",
      borderWidth: 1,
      borderRadius: 15,
      fontSize: 16,
    },
    title: {
      fontWeight: 900,
      fontFamily: "FuturaHvBT",
      fontSize: 24,
      color: "#0061A7",
      margin: "28px 30px 20px",
    },
    title1: {
      fontWeight: 900,
      fontFamily: "FuturaHvBT",
      fontSize: 16,
      margin: "20px 20px 25px",
    },
    title2: {
      fontWeight: 700,
      fontFamily: "FuturaHvBT",
      fontSize: 14,
      margin: "10px 20px 5px",
    },
    title3: {
      fontWeight: 700,
      fontFamily: "FuturaHvBT",
      fontSize: 13,
      color: "#BCC8E7",
      margin: "2px 20px 20px",
    },
    title5: {
      fontWeight: 700,
      fontFamily: "FuturaHvBT",
      fontSize: 20,
      marginBottom: "20px",
      marginTop: "30px",
    },
    input: {
      "&::placeholder": {
        color: "#BCC8E7",
      },
    },
    input2: {
      color: "#374062",
      "&::placeholder": {
        color: "#BCC8E7",
      },
    },
  });

  const initForm = {
    descriptionEn: "",
    descriptionId: "",
    endDate: null,
    isRecommend: true,
    location: [],
    promoCode: "",
    promoLink: "",
    promoSubject: "",
    segmentation: [],
    startDate: null,
    tncEn: "",
    tncId: "",
    createdBy: null,
    base64PromoImage: null,
    base64PromoImage2: null,
    base64PromoImage3: null,
    base64PromoImage4: null,
    isAfterLogin: false,
    isBeforeLogin: false,
  };

  const isFormTouched = {
    promoCode: null,
    promoLink: null,
    promoSubject: null,
  };

  const acceptedPromoFile = "image/png, image/jpeg";

  const classes = useStyles();
  const {
    detail,
    isLoading,
    promoCode,
    isExecute,
    segmentation,
    branch,
    promoCategory,
    isSubmitting,
    isAlreadyExist,
  } = useSelector((state) => state.promo);
  const [tambahSegementasi, setTambahSegmentasi] = useState(false);
  const history = useHistory();

  const [promoForm, setPromoForm] = useState({
    ...initForm,
  });
  const [isPromoFormTouched, setIsPromoFormTouched] = useState({
    ...isFormTouched,
  });

  const [selectMerchants, setSelectMerchants] = useState([]);
  const [selectCategory, setSelectCategory] = useState([]);
  const [selectSegmentasi, setSelectSegmentasi] = useState([]);

  const [tambahPromoType, setTambahPromoType] = useState(false);
  const [tambahPromoMerchants, setTambahPromoMerchants] = useState(false);
  const [tambahUmur, setTambahUmur] = useState(false);

  const [successConfirm, setSuccessConfirm] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [errorMaxBeforeLogin, setErrorMaxBeforeLogin] = useState(
    defaultErrorMaxBefore
  );

  const [kodeSegmentasi, setKodeSegmentasi] = useState(null);
  const [kodeUmur, setKodeUmur] = useState(null);
  const [kodePreferensi, setKodePreferensi] = useState(null);
  const [locationSelected, setLocationSelected] = useState([]);
  const [errSizeUpload, setErrorSizeUpload] = useState({
    imageTabungan: null,
    imageKtp: null,
    imageAdditionalSuratKuasa: [],
  });

  const [cekPromoCode, setCekPromoCode] = useState(null);
  const codeValidation = useDebounce(cekPromoCode, 1000);

  const isFormInvalid =
    isSubmitting ||
    promoForm.promoSubject.trim().length < 3 ||
    promoForm.promoCode.trim().length < 3 ||
    promoForm.promoLink.trim().length < 3 ||
    !promoForm.base64PromoImage ||
    !promoForm.base64PromoImage2 ||
    !promoForm.base64PromoImage3 ||
    !promoForm.base64PromoImage4 ||
    !promoForm.startDate ||
    !promoForm.endDate ||
    promoForm.descriptionId === "<p></p>\n" ||
    promoForm.descriptionEn === "<p></p>\n" ||
    !promoForm.descriptionId ||
    !promoForm.descriptionEn ||
    promoForm.tncId === "<p></p>\n" ||
    promoForm.tncEn === "<p></p>\n" ||
    !promoForm.tncId ||
    !promoForm.tncEn ||
    selectSegmentasi.length === 0 ||
    selectMerchants.length === 0 ||
    selectCategory.length === 0;

  const isUrlValid = promoForm.promoLink
    ? isValidUrl(promoForm.promoLink)
    : true;

  const dispatch = useDispatch();

  const clickKembali = () => {
    if (promoCode) {
      dispatch(setPromoCode(null));
      dispatch(setDetails(null));
    }
    dispatch(setPages(0));
    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  useEffect(() => {
    const payload = {
      id: promoCode,
    };

    if (promoCode) {
      dispatch(handlePromoLDetail(payload));
    }
  }, [promoCode]);

  const onChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setPromoForm((prevPromoForm) => ({
      ...prevPromoForm,
      [name]: name === "promoCode" ? value.replace(/[^a-zA-Z0-9]/g, "") : value,
    }));
  };

  useEffect(() => {
    if (codeValidation) {
      if (codeValidation !== detail?.promotionCode) {
        dispatch(
          getValidationService({
            code: codeValidation,
            table: "48",
          })
        );
      } else {
        dispatch(setServiceAlready(false));
      }
    }
  }, [codeValidation]);

  const handleUploadImage = (fileType) => async (e) => {
    // console.log("ini fileType", fileType);
    const file = e?.target?.files[0];
    if (file === undefined) return;
    // console.log("ini file", file);
    const fileSize = e.target.files[0]?.size;
    const fileName = file?.name;
    const listAcceptArray = ["image/png", "image/jpeg"];

    // ? nama file tanpa format
    // console.log("ini", file?.name?.split(`.${file?.type?.split("/")[1]}`)[0]);

    // handle error format
    if (!listAcceptArray.includes(file.type)) {
      setErrorSizeUpload((prevErrors) => ({
        ...prevErrors,
        [fileType]: "Document does not conform to the specified format",
      }));
    }

    // handle error size
    if (fileSize > 10 * (1024 * 1024)) {
      // console.log("masuk error size", fileSize);
      setErrorSizeUpload((prevErrors) => ({
        ...prevErrors,
        [fileType]: "Can't upload more than 10 MB",
      }));
    }

    if (file.type !== "application/pdf") {
      // ? ini cleanup atau apa
      // setErrorSizeUpload((prevErrors) => ({
      //   ...prevErrors,
      //   [fileType]: null,
      // }));

      new Compressor(file, {
        quality: 0.6,
        convertTypes: ["image/png", "image/jpg", "image/jpeg"],
        convertSize: 1000000,
        async success(result) {
          const base64Image = await blobToBase64(file);
          // console.log("ini blob", base64Image);
          setPromoForm((prevPromoForm) => ({
            ...prevPromoForm,
            [fileType]: { image: base64Image, fileName },
          }));
        },
        error(err) {
          return err.message;
        },
      });
    }
  };
  // console.log("ini promoForm", promoForm);
  // console.log("ini errSizeUpload", errSizeUpload);
  // console.log("ini promoCode", promoCode);
  const removeWrongFormat = (fileName) => {
    if (fileName) {
      return fileName?.slice(0, fileName?.length - 4);
    }
  };

  useEffect(() => {
    // console.log("ini detail", detail);
    if (detail) {
      setCekPromoCode(detail?.promotionCode);
      setPromoForm({
        id: detail?.id,
        promoSubject: detail?.promotionSubject,
        promoCode: detail?.promotionCode,
        promoLink: detail?.promotionLink,
        tncEn: detail?.termsAndConditionsEn,
        tncId: detail?.termsAndConditions,
        descriptionEn: detail?.descriptionEn,
        descriptionId: detail?.description,
        base64PromoImage: {
          fileName: removeWrongFormat(
            detail?.promotionImageLink?.split("digitalbanking/public/promo/")[1]
          ),
          image: null,
        },
        base64PromoImage2: {
          fileName: removeWrongFormat(
            detail?.promotionImageLink2?.split(
              "digitalbanking/public/promo/"
            )[1]
          ),
          image: null,
        },
        base64PromoImage3: {
          fileName: removeWrongFormat(
            detail?.promotionImageLink3?.split(
              "digitalbanking/public/promo/"
            )[1]
          ),
          image: null,
        },
        base64PromoImage4: {
          fileName: removeWrongFormat(
            detail?.promotionImageLink4?.split(
              "digitalbanking/public/promo/"
            )[1]
          ),
          image: null,
        },
        startDate: moment(detail?.startDate),
        endDate: moment(detail?.endDate),
        isAfterLogin: detail?.isAfterLogin,
        isBeforeLogin: detail?.isBeforeLogin,
      });
      const setLoc = [];
      detail?.location?.map((item) => {
        if (item) {
          setLoc.push(item);
        }
      });
      setLocationSelected(setLoc);
      setSelectCategory(
        (detail?.promotionCategory || []).map(
          ({ promoCategory }) => promoCategory
        )
      );
      setSelectSegmentasi(
        (detail?.segmentation || []).map(({ id, name }) => ({ id, name }))
      );
      setSelectMerchants(
        (detail?.merchant || []).map(({ merchantId, name }) => ({
          merchantId,
          merchantName: name,
        }))
      );
    }
  }, [detail]);
  // console.log("ini locationSelected", locationSelected);
  // console.log("ini selectSegmentasi", selectSegmentasi);
  // console.log("ini selectCategory", selectCategory);
  // console.log("ini selectMerchants", selectMerchants);
  const handleSave = () => {
    const userLogin = getLocalStorage("portalUserLogin");
    const dataUser = JSON.parse(userLogin);

    const payload = { ...promoForm, createdBy: dataUser.username };
    payload.startDate = moment(payload.startDate).format("YYYY-MM-DD HH:mm:ss");
    payload.endDate = moment(payload.endDate).format("YYYY-MM-DD HH:mm:ss");
    payload.location = locationSelected;
    payload.segmentation = selectSegmentasi.map(({ id, name }) => ({
      id,
      name,
    }));
    payload.promoCategory = selectCategory.map(({ id, name }) => ({
      id,
      name,
    }));
    payload.merchant = selectMerchants.map(({ merchantId, merchantName }) => ({
      idStr: merchantId,
      name: merchantName,
    }));
    payload.base64PromoImage = !payload.base64PromoImage.image
      ? {
          image: null,
          fileName: removeWrongFormat(detail?.promotionImageLink),
        }
      : payload.base64PromoImage;
    payload.base64PromoImage2 = !payload.base64PromoImage2.image
      ? {
          image: null,
          fileName: removeWrongFormat(detail?.promotionImageLink2),
        }
      : payload.base64PromoImage2;
    payload.base64PromoImage3 = !payload.base64PromoImage3.image
      ? {
          image: null,
          fileName: removeWrongFormat(detail?.promotionImageLink3),
        }
      : payload.base64PromoImage3;
    payload.base64PromoImage4 = !payload.base64PromoImage4.image
      ? {
          image: null,
          fileName: removeWrongFormat(detail?.promotionImageLink4),
        }
      : payload.base64PromoImage4;

    if (promoCode && detail) {
      dispatch(
        handlePromoEdit(
          payload,
          () => {
            setConfirmSuccess(true);
            setSuccessConfirm(false);
          },
          (message) => {
            setErrorMaxBeforeLogin({
              isOpen: true,
              message: message || defaultErrorMaxBefore.message,
            });
            setSuccessConfirm(false);
          }
        )
      );
    } else {
      dispatch(
        handlePromoSave(
          payload,
          () => {
            setConfirmSuccess(true);
            setSuccessConfirm(false);
          },
          (message) => {
            setErrorMaxBeforeLogin({
              isOpen: true,
              message: message || defaultErrorMaxBefore.message,
            });
            setSuccessConfirm(false);
          }
        )
      );
    }
  };

  return (
    <div className={classes.ubahPromo}>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "15px" }}
            alt="Back"
          />
        }
        onClick={clickKembali}
      />
      <Title label="Promo Management">{/* title */}</Title>
      <div style={{ padding: "5px 20px" }}>
        <Paper className={classes.paper1}>
          <FormSectionTitle
            title="Detail Promo"
            color={Colors.gray.medium}
            borderColor={Colors.gray.medium}
          />
          <div style={{ marginBottom: 20 }} />
          {isLoading ? (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                height: 500,
                alignItems: "center",
              }}
            >
              <CircularProgress size={50} />
            </div>
          ) : (
            <Fragment>
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <FormField
                      label="Promo Name :"
                      error={
                        promoForm.promoSubject.trim().length < 3 &&
                        isPromoFormTouched.promoSubject
                      }
                      errorMessage="Minimum 3 characters"
                      absolute={false}
                    >
                      <TextField
                        className={classes.input}
                        placeholder="Enter Promo Name"
                        value={promoForm.promoSubject}
                        name="promoSubject"
                        onChange={onChange}
                        onBlur={() =>
                          setIsPromoFormTouched((prev) => ({
                            ...prev,
                            promoSubject: true,
                          }))
                        }
                        style={{ width: "100%" }}
                        maxLength={50}
                      />
                    </FormField>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <FormField
                      label="Promo Code :"
                      error={
                        (promoForm.promoCode.trim().length < 3 &&
                          isPromoFormTouched.promoCode) ||
                        isAlreadyExist
                      }
                      errorMessage={
                        isAlreadyExist
                          ? "Promo Code is already exist!"
                          : "Minimum 3 characters"
                      }
                      absolute={false}
                    >
                      <TextField
                        placeholder="Enter Promo Code"
                        className={classes.input}
                        name="promoCode"
                        value={promoForm.promoCode}
                        onChange={(e) => {
                          onChange(e);
                          setCekPromoCode(e.target.value);
                        }}
                        onBlur={() =>
                          setIsPromoFormTouched((prev) => ({
                            ...prev,
                            promoCode: true,
                          }))
                        }
                        style={{ width: "100%" }}
                        maxLength={50}
                      />
                    </FormField>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div>
                    <FormField
                      label="Promo Links :"
                      error={
                        !isUrlValid ||
                        (promoForm.promoLink.trim().length < 3 &&
                          isPromoFormTouched.promoLink)
                      }
                      errorMessage={
                        isUrlValid
                          ? "Minimum 3 characters"
                          : "Promo url isn't valid"
                      }
                      absolute={false}
                    >
                      <TextField
                        placeholder="Enter Promo Link"
                        className={classes.input}
                        name="promoLink"
                        value={promoForm.promoLink}
                        onChange={onChange}
                        onBlur={() =>
                          setIsPromoFormTouched((prev) => ({
                            ...prev,
                            promoLink: true,
                          }))
                        }
                        style={{ width: "100%" }}
                        type="url"
                        maxLength={50}
                      />
                    </FormField>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <div>
                      <FormField
                        label={<span>Upload Promo Image (90 x 90) :</span>}
                        error={errSizeUpload?.base64PromoImage}
                        errorMessage={errSizeUpload?.base64PromoImage}
                        absolute={false}
                      >
                        <InputImage
                          error={errSizeUpload?.base64PromoImage}
                          id="base64PromoImage"
                          height="30px"
                          placeholder="Upload Promo Image"
                          width="100%"
                          value={promoForm?.base64PromoImage?.fileName}
                          onChange={handleUploadImage("base64PromoImage")}
                          accept={acceptedPromoFile}
                        />
                      </FormField>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <div>
                      <FormField
                        label={<span>Upload Promo Image (328 x 120) :</span>}
                        error={errSizeUpload?.base64PromoImage2}
                        errorMessage={errSizeUpload?.base64PromoImage2}
                        absolute={false}
                      >
                        <InputImage
                          error={errSizeUpload?.base64PromoImage2}
                          id="base64PromoImage2"
                          height="30px"
                          placeholder="Upload Promo Image"
                          width="100%"
                          value={promoForm?.base64PromoImage2?.fileName}
                          onChange={handleUploadImage("base64PromoImage2")}
                          accept={acceptedPromoFile}
                        />
                      </FormField>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <div>
                      <FormField
                        label={<div>Upload Promo Image (636 x 136) :</div>}
                        error={errSizeUpload?.base64PromoImage3}
                        errorMessage={errSizeUpload?.base64PromoImage3}
                        absolute={false}
                      >
                        <InputImage
                          error={errSizeUpload?.base64PromoImage3}
                          id="base64PromoImage3"
                          height="30px"
                          placeholder="Upload Promo Image"
                          width="100%"
                          value={promoForm?.base64PromoImage3?.fileName}
                          onChange={handleUploadImage("base64PromoImage3")}
                          accept={acceptedPromoFile}
                        />
                      </FormField>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <div>
                      <FormField
                        label={<span>Upload Promo Image (676 x 364) :</span>}
                        error={errSizeUpload?.base64PromoImage4}
                        errorMessage={errSizeUpload?.base64PromoImage4}
                        absolute={false}
                      >
                        <InputImage
                          error={errSizeUpload?.base64PromoImage4}
                          id="base64PromoImage4"
                          height="30px"
                          placeholder="Upload Promo Image"
                          width="100%"
                          value={promoForm?.base64PromoImage4?.fileName}
                          onChange={handleUploadImage("base64PromoImage4")}
                          accept={acceptedPromoFile}
                        />
                      </FormField>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <div>
                      <div>Start Date :</div>
                      <DatePickerWhite
                        style={{ width: "100%" }}
                        placeholder="12/12/2020"
                        name="startDate"
                        value={promoForm.startDate}
                        onChange={(e) =>
                          onChange({
                            target: { value: e, name: "startDate" },
                          })
                        }
                        format="DD/MM/YYYY"
                      />
                    </div>
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.card}>
                    <div>
                      <div>Expired Date :</div>
                      <DatePickerWhite
                        style={{ width: "100%" }}
                        placeholder="12/12/2020"
                        name="endDate"
                        value={promoForm.endDate}
                        onChange={(e) =>
                          onChange({ target: { value: e, name: "endDate" } })
                        }
                        disabledDate={(current) =>
                          promoForm.startDate
                            ? current <
                              moment(promoForm.startDate, "DD/MM/YYYY")
                            : current < moment(dates, "DD/MM/YYYY")
                        }
                        format="DD/MM/YYYY"
                      />
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} style={{ marginBottom: 30 }}>
                  <span>Recommendation (Optional) :</span>

                  <div
                    style={{
                      display: "flex",
                      gap: "20px",
                      marginTop: "4px",
                    }}
                  >
                    <CheckboxSingle
                      checked={promoForm.isBeforeLogin}
                      onChange={(e) =>
                        setPromoForm({
                          ...promoForm,
                          isBeforeLogin: !promoForm.isBeforeLogin,
                        })
                      }
                      label="Before Login"
                    />
                    <CheckboxSingle
                      checked={promoForm.isAfterLogin}
                      onChange={(e) =>
                        setPromoForm({
                          ...promoForm,
                          isAfterLogin: !promoForm.isAfterLogin,
                        })
                      }
                      label="After Login"
                    />
                  </div>
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={6} sm={6}>
                  <div>
                    <div>
                      <div>
                        <div>T&C ID :</div>
                      </div>
                      <div
                        style={{
                          height: "auto",
                          width: "97%",
                          padding: "10px",
                          border: "1px solid #BCC8E7",
                          borderRadius: "10px",
                        }}
                      >
                        <RichTextSunEditor
                          placeholder="Enter T&C ID"
                          formName="update"
                          customClassName
                          content={promoForm.tncId}
                          onChange={(e) =>
                            onChange({ target: { value: e, name: "tncId" } })
                          }
                        />
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>
                    <span>T&C EN :</span>
                  </div>
                  <div
                    style={{
                      height: "auto",
                      width: "97%",
                      padding: "10px",
                      border: "1px solid #BCC8E7",
                      borderRadius: "10px",
                      position: "relative",
                    }}
                  >
                    <RichTextSunEditor
                      placeholder="Enter T&C EN"
                      formName="update"
                      customClassName
                      content={promoForm?.tncEn}
                      onChange={(e) =>
                        onChange({ target: { value: e, name: "tncEn" } })
                      }
                    />
                  </div>
                </Grid>

                <Grid item xs={6} sm={6}>
                  <div style={{ marginTop: 15 }}>Promo Description ID :</div>
                  <div
                    style={{
                      height: "auto",
                      width: "97%",
                      padding: "10px",
                      border: "1px solid #BCC8E7",
                      borderRadius: "10px",
                      position: "relative",
                    }}
                  >
                    <RichTextSunEditor
                      placeholder="Enter Promo Description ID"
                      formName="update"
                      customClassName
                      content={promoForm?.descriptionId}
                      onChange={(e) =>
                        onChange({
                          target: { value: e, name: "descriptionId" },
                        })
                      }
                    />
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>
                    <div style={{ marginTop: 15 }}>
                      <span>Promo Description EN :</span>
                    </div>
                    <div
                      style={{
                        height: "auto",
                        width: "97%",
                        padding: "10px",
                        border: "1px solid #BCC8E7",
                        borderRadius: "10px",
                        position: "relative",
                      }}
                    >
                      <RichTextSunEditor
                        placeholder="Enter Promo Description EN"
                        formName="update"
                        customClassName
                        content={promoForm?.descriptionEn}
                        onChange={(e) =>
                          onChange({
                            target: { value: e, name: "descriptionEn" },
                          })
                        }
                      />
                    </div>
                  </div>
                </Grid>

                <Grid item xs={6} sm={6}>
                  <div style={{ marginTop: "20px" }} className={classes.card}>
                    <div>Segmentation :</div>
                    <div
                      style={{
                        width: "97%",
                        borderRadius: "10px",
                        minHeight: "118px",
                        border: "1px solid #BCC8E7",
                        position: "relative",
                      }}
                    >
                      <div
                        style={{
                          margin: "5px 5px 5px 5px",
                          display: "flex",
                        }}
                      >
                        <div style={{ width: "124px !important" }}>
                          <GeneralButton
                            label="Select Segmentation"
                            iconPosition="startIcon"
                            buttonIcon={<Plusmini />}
                            width="124px"
                            height="24px"
                            style={{
                              margin: "5px 5px 5px 5px",
                              marginLeft: "5px",
                              marginRight: "10px",
                              padding: "2px",
                              fontSize: "9px",
                              fontFamily: "FuturaMdBT",
                            }}
                            value={kodeSegmentasi}
                            onChange={(e) => setKodeSegmentasi(e.target.value)}
                            // onClick={clickTambah}
                            onClick={() => {
                              setTambahSegmentasi(true);
                            }}
                          />
                        </div>

                        <TagsGroup
                          tags={selectSegmentasi}
                          setTags={setSelectSegmentasi}
                          // scroll
                        />
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div style={{ marginTop: "20px" }} className={classes.card}>
                    <span>Promo Category :</span>
                    <div
                      style={{
                        width: "97%",
                        borderRadius: "10px",
                        minHeight: "118px",
                        border: "1px solid #BCC8E7",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                        }}
                      >
                        <div style={{ width: "124px !important" }}>
                          <GeneralButton
                            label="Select Promo Category"
                            iconPosition="startIcon"
                            buttonIcon={<Plusmini />}
                            width="124px"
                            height="24px"
                            style={{
                              margin: "5px 5px 5px 5px",
                              marginLeft: "5px",
                              marginRight: "10px",
                              padding: "2px",
                              fontSize: "9px",
                              fontFamily: "FuturaMdBT",
                            }}
                            value={kodePreferensi}
                            onChange={(e) => {
                              // console.log("ini promoCategory", e.target.value);
                              setKodePreferensi(e.target.value);
                            }}
                            onClick={() => {
                              setTambahPromoType(true);
                            }}
                          />
                        </div>
                        <div style={{ width: "auto" }}>
                          <TagsGroup
                            tags={selectCategory}
                            setTags={setSelectCategory}
                            // scroll
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div className={classes.card}>
                    <span>Location :</span>
                    <div
                      style={{
                        width: "97%",
                        borderRadius: "10px",
                        minHeight: "118px",
                        border: "1px solid #BCC8E7",
                      }}
                    >
                      <div
                        style={{
                          margin: "5px 5px 5px 5px",
                          display: "flex",
                        }}
                      >
                        <div style={{ width: "124px !important" }}>
                          <GeneralButton
                            label="Select Location"
                            iconPosition="startIcon"
                            buttonIcon={<Plusmini />}
                            width="124px"
                            height="24px"
                            style={{
                              margin: "5px 5px 5px 5px",
                              marginLeft: "5px",
                              marginRight: "10px",
                              padding: "2px",
                              fontSize: "9px",
                              fontFamily: "FuturaMdBT",
                            }}
                            value={kodeUmur}
                            onChange={(e) => setKodeUmur(e.target.value)}
                            onClick={() => {
                              setTambahUmur(true);
                            }}
                          />
                        </div>
                        <TagsGroup
                          tags={locationSelected}
                          setTags={setLocationSelected}
                          // scroll
                        />
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div className={classes.card}>
                    <span>Merchant :</span>
                    <div
                      style={{
                        width: "97%",
                        borderRadius: "10px",
                        minHeight: "118px",
                        border: "1px solid #BCC8E7",
                      }}
                    >
                      <div
                        style={{
                          margin: "5px 5px 5px 5px",
                          display: "flex",
                        }}
                      >
                        <div style={{ width: "124px !important" }}>
                          <GeneralButton
                            label="Select Merchant"
                            iconPosition="startIcon"
                            buttonIcon={<Plusmini />}
                            width="124px"
                            height="24px"
                            style={{
                              margin: "5px 5px 5px 5px",
                              marginLeft: "5px",
                              marginRight: "10px",
                              padding: "2px",
                              fontSize: "9px",
                              fontFamily: "FuturaMdBT",
                            }}
                            // value={kodeUmur}
                            // onChange={(e) => setKodeUmur(e.target.value)}
                            onClick={() => {
                              setTambahPromoMerchants(true);
                            }}
                          />
                        </div>
                        <TagsGroup
                          tags={selectMerchants}
                          setTags={setSelectMerchants}
                          // scroll
                        />
                      </div>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Fragment>
          )}
        </Paper>

        {isLoading ? null : (
          <div className={classes.paper21}>
            <ButtonOutlined
              disabled={isSubmitting}
              label="Cancel"
              width="83px"
              height="43px"
              onClick={() => {
                setErrorSizeUpload({
                  imageTabungan: null,
                  imageKtp: null,
                  imageAdditionalSuratKuasa: [],
                });
                clickKembali();
              }}
            />
            <GeneralButton
              disabled={isFormInvalid || isAlreadyExist}
              label="Save"
              width="100px"
              height="43px"
              onClick={() => setSuccessConfirm(true)}
            />
          </div>
        )}
      </div>
      <PopupsSegmentasi
        isOpen={tambahSegementasi}
        selectSegmentasi={selectSegmentasi}
        setSelectSegmentasi={setSelectSegmentasi}
        handleClose={() => {
          setTambahSegmentasi(false);
        }}
      />
      <PopupsPromoCategory
        selectCategory={selectCategory}
        setSelectCategory={setSelectCategory}
        isOpen={tambahPromoType}
        handleClose={() => {
          setTambahPromoType(false);
        }}
      />
      <PopupMerchants
        selectMerchants={selectMerchants}
        setSelectMerchants={setSelectMerchants}
        isOpen={tambahPromoMerchants}
        handleClose={() => {
          setTambahPromoMerchants(false);
        }}
      />
      <PopupsLocation
        setLocationSelected={setLocationSelected}
        locationSelected={locationSelected}
        isOpen={tambahUmur}
        handleClose={() => {
          setTambahUmur(false);
        }}
      />

      <DeletePopup
        isOpen={successConfirm}
        handleClose={() => setSuccessConfirm(false)}
        message={`
Are You Sure To ${detail ? "Edit Your" : "Add"} Data?`}
        loading={isExecute}
        onContinue={() => {
          dispatch(
            validateTask(
              {
                ...(detail && {
                  id: detail?.id,
                }),
                code: promoForm.promoCode,
                name: detail ? "EDIT" : "ADD",
                menuName: validateTaskConstant.LIST_PROMO,
              },
              {
                async onContinue() {
                  handleSave();
                },
                onError() {
                  setSuccessConfirm(false);
                },
              },
              {
                redirect: false,
              }
            )
          );
        }}
      />

      <SuccessConfirmation
        title="Please Wait For Approval"
        isOpen={confirmSuccess}
        submessage=""
        message="Saved Successfully"
        handleClose={() => {
          setConfirmSuccess(false);
          setPromoForm(initForm);
          dispatch(setPages(0));
          setErrorSizeUpload({
            imageTabungan: null,
            imageKtp: null,
            imageAdditionalSuratKuasa: [],
          });
          setSelectCategory([]);
          setSelectSegmentasi([]);
          setLocationSelected([]);
          history.push(pathnameCONFIG.MANAGEMENT_PROMO.LIST_PROMO);
        }}
      />

      <FailedConfirmation
        handleClose={() => setErrorMaxBeforeLogin(defaultErrorMaxBefore)}
        isOpen={errorMaxBeforeLogin.isOpen}
        title=""
        message="Can't Choose Before Login"
        submessage={errorMaxBeforeLogin.message}
      />
    </div>
  );
};

UbahPromo.propTypes = {};

UbahPromo.defaultProps = {};

export default UbahPromo;
