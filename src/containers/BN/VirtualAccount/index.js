import React from "react";

const VirtualAccount = () => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      width: "100%",
      height: "100%",
    }}
  >
    <iframe
      src="http://34.101.130.102:3017"
      title="Virtual Account"
      height={800}
      style={{ border: "none" }}
    />
  </div>
);
export default VirtualAccount;
