/* eslint-disable no-return-assign */
/* eslint-disable array-callback-return */
import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

import { Grid, makeStyles, Typography } from "@material-ui/core";

import Colors from "helpers/colors";
import useDebounce from "utils/helpers/useDebounce";

import { ReactComponent as AddWhiteIcon } from "assets/icons/BN/AddWhite.svg";

import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

import {
  actionNotificationGet,
  actionNotificationSave,
  actionNotificationDelete,
  setSuccess,
  promoMediaOptions,
} from "stores/actions/notifications";

import AddEditBlastPromoPopup from "./AddEditBlastPromo";
import MenuNotifikasi from "./MenuNotifikasi";

const now = moment();

const useStyles = makeStyles({
  page: {
    backgroundColor: Colors.gray.ultrasoft,
    padding: "16px 40px 20px 20px",
    minHeight: "100%",
  },
  header: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    marginBottom: 37,
  },
});

const dataHeader = [
  {
    title: "Date & Time",
    render: (rowData) => (
      <Fragment>
        <Typography variant="subtitle1" style={{ marginBottom: "8px" }}>
          {rowData?.scheduleDate &&
            moment.unix(rowData?.scheduleDate).format("DD MMM YYYY")}
        </Typography>
        <Typography variant="subtitle1">
          {rowData?.scheduleDate &&
            moment.unix(rowData?.scheduleDate).format("HH:mm:ss")}
        </Typography>
      </Fragment>
    ),
  },
  {
    title: "Promo Name",
    render: (rowData) => (
      <Typography variant="subtitle1">{rowData.title}</Typography>
    ),
  },
  {
    width: "40%",
    title: "Promotional Media",
    render: (rowData) => (
      <Grid container spacing={1}>
        {rowData.media.map((item) => (
          <Grid item>
            <Badge
              outline
              styleBadge={{ whiteSpace: "nowrap" }}
              type="primary-border"
              label={item}
            />
          </Grid>
        ))}
      </Grid>
    ),
  },
  {
    title: "Status",
    render: (rowData) => {
      const status = rowData?.promoStatus?.toLowerCase();
      if (rowData?.promoStatus) {
        return (
          <Badge
            type={
              status?.includes("failed")
                ? "red"
                : status?.includes("success")
                ? "green"
                : status?.includes("pending")
                ? "blue"
                : ""
            }
            label={rowData?.promoStatus}
            outline
          />
        );
      }
    },
  },
];

const Notifikasi = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { isLoading, data, isSuccess } = useSelector(
    ({ notifications }) => notifications
  );

  const [formData, setFormData] = useState([]);
  const [dataFilter, setDataFilter] = useState(null);
  const [dataSearch, setDataSearch] = useState("");
  const [actionType, setActionType] = useState("");
  const [blastPromoPopup, setBlastPromoPopup] = useState(false);
  const [confirmationPopup, setConfirmationPopup] = useState(false);
  const [page, setPage] = useState(1);

  const searchDebounced = useDebounce(dataSearch, 1000);

  const actionTypeHandler = (type) => setActionType(type);
  const openConfirmationHandler = () => setConfirmationPopup(true);
  const closeConfirmationHandler = () => setConfirmationPopup(false);
  const openBlastPromoHandler = () => setBlastPromoPopup(true);
  const closeBlastPromoHandler = () => setBlastPromoPopup(false);

  const notificationFetchHandler = (payload) => {
    dispatch(actionNotificationGet(payload));
  };

  const notificationSaveHandler = () => {
    const payload = formData;
    dispatch(actionNotificationSave(payload, closeConfirmationHandler));
  };

  const formDataHandler = (payload) => {
    openConfirmationHandler();
    setFormData(payload);
  };

  const notificationDeleteHandler = () => {
    const payload = {};
    dispatch(actionNotificationDelete(payload, closeConfirmationHandler));
  };

  useEffect(() => {
    const media = {};
    const filterValue = dataFilter?.tabs?.tabValue3;
    if (filterValue) {
      // FIND Keyname of dropdown1 Value
      promoMediaOptions.map((item, index) => {
        if (filterValue === index + 1) {
          // SET TRUE
          media[item.key] = true;
        }
      });
    }
    const payload = {
      page: page - 1,
      limit: 10,
      title: searchDebounced,
      startDate: dataFilter?.date?.dateValue1?.format("YYYY-MM-DD") || null,
      endDate: dataFilter?.date?.dateValue2?.format("YYYY-MM-DD") || null,
      ...media,
    };
    notificationFetchHandler(payload);
  }, [page, searchDebounced, dataFilter]);

  return (
    <div className={classes.page}>
      <div className={classes.header}>
        <Typography variant="h2" style={{ marginRight: "auto" }}>
          Blast Promo
        </Typography>
        <GeneralButton
          label="Promo Blast"
          width="155px"
          height="44px"
          iconPosition="startIcon"
          buttonIcon={<AddWhiteIcon />}
          style={{ marginRight: 20, borderRadius: 10 }}
          onClick={() => {
            actionTypeHandler("Add");
            setBlastPromoPopup(true);
          }}
        />
        <Search
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placement="bottom"
          placeholder="Promo Name"
          style={{ width: 280 }}
        />
      </div>
      <Filter
        dataFilter={dataFilter}
        setDataFilter={setDataFilter}
        style={{ marginBottom: 20 }}
        title="Show :"
        options={[
          {
            id: 1,
            type: "date",
            placeholder: now.format("DD/MM/YYYY"),
          },
          {
            id: 2,
            type: "date",
            placeholder: now.format("DD/MM/YYYY"),
            disabled: true,
          },
          {
            id: 3,
            type: "tabs",
            options: ["All", ...promoMediaOptions.map(({ value }) => value)],
            defaultValue: 0,
          },
        ]}
      />
      <TableICBB
        allowNullStripped
        headerContent={dataHeader}
        dataContent={data?.dataTable ?? []}
        page={page}
        setPage={setPage}
        totalElement={data?.totalElement}
        totalData={data?.totalPage}
        isLoading={isLoading}
      />
      <AddEditBlastPromoPopup
        isOpen={blastPromoPopup}
        actionType={actionType}
        onContinue={formDataHandler}
        handleClose={closeBlastPromoHandler}
      />
      <DeletePopup
        isOpen={confirmationPopup}
        loading={isLoading}
        message={`Are You Sure to ${
          actionType === "Edit"
            ? "Edit Your"
            : actionType === "Delete"
            ? "Delete the"
            : actionType
        } Data?`}
        submessage="You cannot undo this action"
        handleClose={closeConfirmationHandler}
        onContinue={
          actionType === "Delete"
            ? notificationDeleteHandler
            : notificationSaveHandler
        }
      />
      <SuccessConfirmation
        isOpen={isSuccess}
        submessage=""
        message={`${
          actionType === "Delete" ? "Deleted" : "Saved"
        } Successfully`}
        handleClose={() => {
          if (actionType !== "Delete") {
            setBlastPromoPopup(false);
          }
          dispatch(setSuccess(false));
        }}
      />
    </div>
  );
};

export default Notifikasi;
