// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import { ReactComponent as FrameTag } from "assets/icons/BN/frametag.svg";
import { ReactComponent as FrameMessage } from "assets/icons/BN/framemessage.svg";
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";
import ScrollCustom from "components/BN/ScrollCustom";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import Tabs from "components/BN/Tabs/GeneralTabs";
import GeneralButton from "components/BN/Button/GeneralButton";

// assets
import check from "assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 323,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 36,
    fontWeight: 900,
    color: Colors.dark.hard,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
}));

const NotifikasiTransaksi = ({ isOpen, handleClose, onContinue }) => {
  const classes = useStyles();
  const [checkboxGroupA, setCheckboxGroupA] = useState([]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 900,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div>
              <h1
                style={{
                  fontFamily: "FuturaHvBT",
                  fontWeight: 400,
                  fontSize: 32,
                  letterSpacing: "0.03em",
                  color: "#374062",
                }}
              >
                Notifikasi Transaksi
              </h1>
            </div>
            <div
              style={{
                display: "flex",

                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                }}
              >
                <span style={{ marginBottom: 2, marginTop: "12px" }}>
                  Pilih Media Notifikasi :
                </span>
                <CheckboxGroup
                  value={checkboxGroupA}
                  options={["Aplikasi", "SMS", "Whatsapp", "E-mail"]}
                  onChange={(e) => setCheckboxGroupA(e)}
                  margin="20px"
                  width="160px"
                />
              </div>
            </div>
            <div
              style={{
                marginTop: "2px",
                width: "100%",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <ButtonOutlined
                onClick={handleClose}
                label="Kembali"
                width="162.5px"
                height="44px"
              />
              <GeneralButton
                onClick={onContinue}
                label="Simpan"
                width="162.5px"
                height="44px"
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

NotifikasiTransaksi.propTypes = { onContinue: PropTypes.func };

NotifikasiTransaksi.defaultProps = {
  onContinue: () => {},
};

export default NotifikasiTransaksi;
