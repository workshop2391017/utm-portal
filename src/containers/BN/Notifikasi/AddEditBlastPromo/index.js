/* eslint-disable array-callback-return */
import React, { Fragment, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";

import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  IconButton,
} from "@material-ui/core";

import Colors from "helpers/colors";

import ScrollCustom from "components/BN/ScrollCustom";
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import GeneralDatePicker from "components/BN/DatePicker/GeneralDatePicker";
import AntdTimePicker from "components/BN/DatePicker/TimePicker";

import { ReactComponent as TrashIcon } from "assets/icons/BN/trash-2.svg";

import { actionNotificationPromotionTitlesGet } from "stores/actions/notifications";
import AntdSelectObjectItem from "components/BN/Select/AntdSelectObjectItem";

const useStyles = makeStyles((theme) => ({
  modal: {
    width: "100%",
    height: "100%",
  },
  paper: {
    backgroundColor: "white",
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 730,
    minHeight: 700,
    padding: 20,
    borderRadius: 20,
  },
  title: {
    ...theme.typography.h2,
    textAlign: "center",
    fontSize: 28,
    marginBottom: 20,
  },
  label: {
    ...theme.typography.subtitle1,
    letterSpacing: "0.03em",
  },
  button: {
    ...theme.typography.actionButton,
    borderRadius: 10,
  },
  scheduleContainer: {
    border: `1px solid ${Colors.neutral.gray.medium}`,
    borderRadius: 10,
    marginBottom: 20,
  },
  scheduleRow: {
    borderBottom: `1px solid ${Colors.neutral.gray.medium}`,
    padding: "4px 20px",
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    "&:last-child": {
      borderBottom: 0,
    },
  },
  filterContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-end",
    marginBottom: 20,
    "&>*": {
      display: "flex",
      flexDirection: "column",
    },
  },
  footer: {
    position: "absolute",
    bottom: "2rem",
    width: "94%",
    display: "flex",
    justifyContent: "space-between",
  },
}));

const now = moment();

export const promoMediaOptions = [
  { key: "isSendToPush", value: "Application" },
  { key: "isSendToSMS", value: "SMS" },
  { key: "isSendToEmail", value: "Email" },
  { key: "isSendToWhatsapp", value: "WhatsApp" },
];

const checkboxOptions = [...promoMediaOptions.map((a) => a.value)];

const AddEditBlastPromo = ({ isOpen, handleClose, onContinue }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { promotionTitles, isSuccess } = useSelector(
    ({ notifications }) => notifications
  );

  const [options, setOptions] = useState([]);
  const [checkboxGroup, setCheckboxGroup] = useState([]);
  const [dateBlast, setDateBlast] = useState(null);
  const [timeBlast, setTimeBlast] = useState(now);
  const [promotionTitle, setPromotionTitle] = useState(null);
  const [schedule, setSchedule] = useState([]);

  const promotionTitleChangeHandler = (value) => {
    setPromotionTitle(value);
  };

  const dateBlastChangeHandler = (date) => {
    setDateBlast(date);
  };

  const timeBlastChangeHandler = (time) => {
    setTimeBlast(time);
  };

  const checkboxHandler = (event) => setCheckboxGroup(event);

  const addScheduleHandler = () => {
    setSchedule((prevSchedule) => {
      const newSchedule = {
        dateBlast,
        timeBlast,
      };

      return prevSchedule.concat(newSchedule);
    });
  };

  const deleteScheduleHandler = (index) => () => {
    setSchedule((prevSchedule) => prevSchedule.filter((_, i) => i !== index));
  };

  const submitHandler = () => {
    const scheduleDate = schedule.map(
      (item) =>
        `${item.dateBlast.format("DD-MM-YYYY")} ${item.timeBlast.format(
          "HH:mm"
        )}`
    );

    const media = {
      isSendToEmail: false,
      isSendToWhatsapp: false,
      isSendToPush: false,
      isSendToSMS: false,
    };

    promoMediaOptions.map((item) => {
      checkboxGroup.map((itemCheck) => {
        if (itemCheck === item.value) {
          media[item.key] = true;
        }
      });
    });

    const payload = {
      scheduleDate,
      promoCode: promotionTitle,
      ...media,
      type: "PROMO_BLAST",
    };

    onContinue(payload);
  };

  useEffect(() => {
    if (isOpen) {
      const payload = {
        search: "",
        startDate: null,
        endDate: null,
        promoTab: "ongoing",
      };
      dispatch(actionNotificationPromotionTitlesGet(payload));
    }
  }, [isOpen]);

  useEffect(() => {
    if (options.length === 0 && promotionTitles.length > 0) {
      setOptions(promotionTitles);
    }
  }, [options, promotionTitles]);

  const disableAddSchedule = !promotionTitle || !dateBlast || !timeBlast;
  const disableScheduleButton =
    schedule.length === 0 || checkboxGroup.length === 0;

  function clearState() {
    setOptions([]);
    setCheckboxGroup([]);
    setDateBlast(null);
    setTimeBlast(null);
    setPromotionTitle(null);
    setSchedule([]);
  }
  useEffect(() => {
    if (isSuccess) {
      clearState();
    }
  }, [isSuccess]);

  return (
    <Modal
      className={classes.modal}
      open={isOpen}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
    >
      <Fade in={isOpen}>
        <div className={classes.paper}>
          <h2 className={classes.title}>Blast Promo</h2>
          <div className={classes.filterContainer}>
            <div>
              <span className={classes.label}>Select Promotion Title :</span>
              <AntdSelectObjectItem
                style={{ width: 200 }}
                options={options}
                value={promotionTitle}
                placeholder="Select Promotion Title"
                onChange={promotionTitleChangeHandler}
              />
            </div>
            <div>
              <span className={classes.label}>Blast Date :</span>
              <GeneralDatePicker
                value={dateBlast}
                width={125}
                placeholder="DD/MM/YYYY"
                format="DD/MM/YYYY"
                onChange={dateBlastChangeHandler}
                disabled={!promotionTitle}
                disabledDate={(current) => {
                  let endDate = null;
                  if (promotionTitle) {
                    const findPromo = options.find(
                      (o) => o.value === promotionTitle
                    );
                    if (findPromo) {
                      endDate = moment
                        .unix(findPromo.endDate / 1000)
                        .add(1, "days");
                    }
                  }
                  return moment() >= current || (endDate && current > endDate);
                }}
              />
            </div>
            <div>
              <span className={classes.label}>Blast Time :</span>
              <AntdTimePicker
                value={timeBlast}
                width={125}
                placeholder="00:00"
                format="HH:mm"
                onChange={timeBlastChangeHandler}
              />
            </div>
            <GeneralButton
              label="Add"
              disabled={disableAddSchedule}
              className={classes.button}
              onClick={addScheduleHandler}
            />
          </div>
          {schedule.length !== 0 && (
            <Fragment>
              <span className={classes.label}>Blast Promo Schedule :</span>
              <div className={classes.scheduleContainer}>
                {schedule.length < 6 ? (
                  schedule.map((item, index) => (
                    <div className={classes.scheduleRow} key={index}>
                      <span>
                        {item.dateBlast.format("DD/MM/YYYY")},{" "}
                        {item.timeBlast.format("HH:mm")}
                      </span>
                      <IconButton
                        style={{ padding: 10 }}
                        onClick={deleteScheduleHandler(index)}
                      >
                        <TrashIcon />
                      </IconButton>
                    </div>
                  ))
                ) : (
                  <ScrollCustom height={280}>
                    {schedule.map((item, index) => (
                      <div className={classes.scheduleRow} key={index}>
                        <span>
                          {item.dateBlast}, {item.timeBlast}
                        </span>
                        <IconButton
                          style={{ padding: 10 }}
                          onClick={deleteScheduleHandler(index)}
                        >
                          <TrashIcon />
                        </IconButton>
                      </div>
                    ))}
                  </ScrollCustom>
                )}
              </div>
            </Fragment>
          )}
          <span className={classes.label}>Select Promotional Media :</span>
          <CheckboxGroup
            value={checkboxGroup}
            options={checkboxOptions}
            onChange={checkboxHandler}
            margin="10px 80px 10px 4px"
            width="250px"
          />
          <div className={classes.footer}>
            <ButtonOutlined
              label="Cancel"
              className={classes.button}
              width={93}
              onClick={() => {
                clearState();
                handleClose();
              }}
            />
            <GeneralButton
              label="Schedule"
              className={classes.button}
              width={100}
              onClick={submitHandler}
              disabled={disableScheduleButton}
            />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default AddEditBlastPromo;
