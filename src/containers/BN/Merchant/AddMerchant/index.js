// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import Title from "../../../../components/BN/Title";
import TextField from "../../../../components/BN/TextField/AntdTextField";
import GeneralButton from "../../../../components/BN/Button/GeneralButton";
import ButtonOutlined from "../../../../components/BN/Button/ButtonOutlined";
import SuccessConfirm from "../../../../components/BN/Popups/BerhasilRekeningKelompok";

const AddMerchant = (props) => {
  const useStyles = makeStyles({
    merchant: {},
    container: {
      padding: "20px 30px",
    },
    forms: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
    },
    form: {
      width: "601px",
      height: "414px",
      background: " #FFFFFF",
      display: "flex",
      flexDirection: "column",
      padding: "32px 32px 32px 32px",
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      borderRadius: "20px",
    },
    h1: {
      fontFamily: "FuturaMdBT",
      fontStyle: "normal",
      fontWeight: 400,
      fontSize: "16px",
      color: "#374062",
      lineHeight: "19px",
      letterSpacing: "0.03em",
    },
    span: {
      fontFamily: "FuturaBkBT",
      fontStyle: "normal",
      fontWeight: "400",
      fontSize: "13px",
      lineHeight: "16px",
      color: "#374062",
      letterSpacing: "0.01em",
      marginTop: "20px",
    },
    card: {
      fontFamily: "FuturaBQ",
      fontWeight: "400",
      fontSize: 13,
      width: "300px",
      marginBottom: 15,
    },
    card2: {
      fontFamily: "FuturaBQ",
      fontWeight: "400",
      fontSize: 13,
      width: "300px",
      marginBottom: 15,
      display: "flex",
      flexDirection: "column",
    },
    input: {
      "&::placeholder": {
        color: "#BCC8E7",
      },
    },
    buttons: {
      width: "100%",
      display: "flex",
      justifyContent: "space-between",
      background: " #FFFFFF",
      height: "84px",
      paddingTop: "18px",
      marginTop: "56px",
      padding: "8px 8px",
      alignItems: "center",
    },
  });
  const history = useHistory();
  const classes = useStyles();
  const [save, setSave] = useState(false);
  const handleOpenConfirm = () => {
    setSave(true);
  };

  return (
    <div>
      <SuccessConfirm
        isOpen={save}
        handleClose={() => setSave(false)}
        message="Perubahan Disimpan"
      />
      <Title
        label=" Tambah List Merchant"
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      />

      <div className={classes.forms}>
        <div className={classes.form}>
          <h1
            className={{
              fontFamily: "FuturaMdBT",
              fontStyle: "normal",
              fontWeight: 700,
              fontSize: "16px",
              color: "#374062",
              lineHeight: "19px",
              letterSpacing: "0.03em",
            }}
          >
            Form Tambah List Merchant
          </h1>
          <span
            className={{
              fontFamily: "FuturaBkBT",
              fontStyle: "normal",
              fontWeight: "400",
              fontSize: "13px",
              lineHeight: "16px",
              color: "#374062",
              letterSpacing: "0.01em",
              marginTop: "20px",
            }}
          >
            Silahkan isi data dibawah ini untuk melakukan tambah list merchant
          </span>

          <div style={{ display: "flex", marginTop: "23px" }}>
            <div>
              <div>
                <div className={classes.card}>
                  <div>
                    <span>Merchant ID :</span>
                    <TextField
                      className={classes.input}
                      placeholder="Masukkan Merchant ID"
                      style={{ width: "260px", marginTop: "4px" }}
                    />
                  </div>
                </div>
                <div className={classes.card}>
                  <div>
                    <span>Name Merchant :</span>
                    <TextField
                      className={classes.input}
                      placeholder="Masukkan nama merchant"
                      style={{ width: "260px", marginTop: "4px" }}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div style={{ marginLeft: "-18px" }}>
              <div>
                <div className={classes.card}>
                  <div>
                    <span>Jenis Merchant :</span>
                    <TextField
                      className={classes.input}
                      placeholder="Masukan jenis merchant "
                      style={{ width: "260px", marginTop: "4px" }}
                    />
                  </div>
                </div>
                <div className={classes.card}>
                  <div>
                    <span>Kota : &nbsp;&nbsp;&nbsp; </span>
                    <TextField
                      className={classes.input}
                      placeholder="Masukan kota merchant"
                      style={{ width: "260px", marginTop: "4px" }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ width: "100%" }} className={classes.card}>
            <span>Kota :</span>
            <TextField
              type="textArea"
              className={classes.input}
              placeholder="Masukkan alamat merchant "
              style={{
                height: "97px",
                width: "540px",
                resize: "none",
                marginTop: "4px",
              }}
            />
          </div>
        </div>
      </div>

      <div className={classes.buttons}>
        <ButtonOutlined
          label="Batal"
          style={{
            marginLeft: 15,
            fontFamily: "FuturaMdBT",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "20px",
          }}
          onClick={() => history.push("/portal/bisnis-pengaturan/merchant")}
          width="82px"
          height="44px"
        />
        <GeneralButton
          label="Simpan"
          style={{
            marginRight: 15,
            fontFamily: "FuturaMdBT",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "20px",
          }}
          onClick={handleOpenConfirm}
          width="100px"
          height="44px"
        />
      </div>
    </div>
  );
};

AddMerchant.propTypes = {};

AddMerchant.defaultProps = {};

export default AddMerchant;
