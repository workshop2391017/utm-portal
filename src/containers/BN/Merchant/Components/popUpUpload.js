// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";
import Colors from "helpers/colors";

// components
import TelusuriImage from "components/BN/Inputs/TelusuriImage";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";

import { ReactComponent as UploadIcon } from "assets/icons/BN/upload-white.svg";
import { ReactComponent as DownloadIcon } from "assets/icons/BN/unduh.svg";
import { ReactComponent as CloseIcon } from "assets/icons/BN/close-blue.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: "584px",
    height: 399,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  message: {
    fontFamily: "FuturaMdBT",
    fontSize: 17,
    fontWeight: 400,
    letterSpacing: "0.01em",
    marginTop: "18px",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: "#374062",
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#374062",
    borderRadius: 10,
    marginRight: 10,
  },
  footer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginTop: "48px",
  },
}));

const EditMerchant = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [inputID, setInputID] = useState(null);
  const [inputName, setInputName] = useState(null);
  const [inputCity, setInputCity] = useState(null);
  const [inputAddress, setInputAddress] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);

  return (
    <Modal
      className={classes.modal}
      open={isOpen}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 900,
      }}
    >
      <Fade in={isOpen}>
        <div className={classes.paper}>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            {" "}
            <CloseIcon onClick={handleClose} />
          </div>

          <div
            style={{ width: "100%", display: "flex", justifyContent: "center" }}
          >
            <span className={classes.title}>Upload File</span>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              marginTop: "14px",
              gap: 5,
            }}
          >
            <TelusuriImage
              id="unggahFileSegmentasi"
              width="100%"
              //   value={uploadFile?.name}
              //   placeholder="Browse Library"
              //   onChange={(e) => dispatch(setUploadFile(e.target.files[0]))}
            />
            <span className={classes.message}>Document Upload Terms</span>
            <div className={classes.listGroup}>
              <div className={classes.bullets} />
              Uploaded documents can use the .xlsx and .csv formats
            </div>
            <div className={classes.listGroup}>
              <div className={classes.bullets} />
              The maximum size for each document is 1MB
            </div>
            <div className={classes.listGroup}>
              <div className={classes.bullets} />
              Make sure the document you upload is clearly visible
            </div>
          </div>
          <div className={classes.footer}>
            <ButtonOutlined
              buttonIcon={<DownloadIcon />}
              iconPosition="startIcon"
              label="Download Template File"
              width="250px"
              onClick={handleClose}
            />
            <GeneralButton
              iconPosition="startIcon"
              buttonIcon={<UploadIcon />}
              label="Upload file"
              width="150px"
              onClick={onContinue}
            />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

EditMerchant.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditMerchant.defaultProps = { onContinue: () => {} };

export default EditMerchant;
