// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";
import Colors from "helpers/colors";

// components

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Tables from "components/BN/TableIcBB";

// Icons
import { ReactComponent as UploadIcon } from "assets/icons/BN/upload-white.svg";
import { ReactComponent as DownloadIcon } from "assets/icons/BN/unduh.svg";
import { ReactComponent as CloseIcon } from "assets/icons/BN/close-blue.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: "584px",
    height: "620px",
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  message: {
    fontFamily: "FuturaMdBT",
    fontSize: 17,
    fontWeight: 400,
    letterSpacing: "0.01em",
    marginTop: "18px",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: "#374062",
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#374062",
    borderRadius: 10,
    marginRight: 10,
  },
  footer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginTop: "18px",
  },
}));
const dataHeader = () => [
  {
    title: "Merchant ID",
    render: (rowData) => <span>{rowData?.id}</span>,
  },
  {
    title: "Merchant Name",
    render: (rowData) => <span>{rowData?.merchantName}</span>,
  },
  {
    title: "City",
    render: (rowData) => <span>{rowData?.city}</span>,
  },
];
const dataDummy = [
  {
    id: "197329",
    merchantName: "OpenStore",
    city: "Jakarta Pusat ",
  },
  {
    id: "197329",
    merchantName: "OpenStore",
    city: "Jakarta Pusat ",
  },
  {
    id: "197329",
    merchantName: "OpenStore",
    city: "Jakarta Pusat ",
  },
  {
    id: "197329",
    merchantName: "OpenStore",
    city: "Jakarta Pusat ",
  },
  {
    id: "197329",
    merchantName: "OpenStore",
    city: "Jakarta Pusat ",
  },
  {
    id: "197329",
    merchantName: "OpenStore",
    city: "Jakarta Pusat ",
  },
];
const UploadListMerchant = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [inputID, setInputID] = useState(null);
  const [inputName, setInputName] = useState(null);
  const [inputCity, setInputCity] = useState(null);
  const [inputAddress, setInputAddress] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);

  return (
    <Modal
      className={classes.modal}
      open={isOpen}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 900,
      }}
    >
      <Fade in={isOpen}>
        <div className={classes.paper}>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            {" "}
            <CloseIcon onClick={handleClose} />
          </div>

          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              marginTop: "12px",
            }}
          >
            <span className={classes.title}>Upload Marchant List</span>
          </div>
          <div style={{ marginTop: "20px" }}>
            <Tables
              headerContent={dataHeader()}
              dataContent={dataDummy}
              totalData={1}
              totalElement={1}
              scroll
              scrollHeight={301}
            />
          </div>

          <div className={classes.footer}>
            <ButtonOutlined label="Cancel" onClick={handleClose} width="93px" />
            <GeneralButton label="Save" onClick={onContinue} width="78px" />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

UploadListMerchant.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

UploadListMerchant.defaultProps = { onContinue: () => {} };

export default UploadListMerchant;
