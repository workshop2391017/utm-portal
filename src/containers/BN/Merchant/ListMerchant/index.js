// main
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { pathnameCONFIG } from "configuration";

// import { tableDummy } from "../../../../../services/mockup_data/BN/dataMerchant";

// assets
import { ReactComponent as Refresh } from "assets/icons/BN/refresh-white.svg";
import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";
import { ReactComponent as Upload } from "assets/icons/BN/upload-blue.svg";
// components
import Title from "../../../../components/BN/Title";
import Search from "../../../../components/BN/Search/SearchWithoutDropdown/index3";
import Table from "../../../../components/BN/Table/GeneralTablePagination";
import GeneralButton from "../../../../components/BN/Button/GeneralButton";
import ButtonOutlined from "../../../../components/BN/Button/ButtonOutlined";
import TableICBB from "../../../../components/BN/TableIcBB";
// components Modal
import Modal from "../../../../components/BN/Popups/EditMerchant";

const tableDummy = {};
const Merchant = (props) => {
  // const {} = props;
  const useStyles = makeStyles({
    merchant: {},
    container: {
      padding: "20px 30px",
    },
  });

  const history = useHistory();
  const classes = useStyles();
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(1);
  const [modal, setModal] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);

  const [checkList, setCheckList] = useState([]);
  const [disabledList, setDisabledList] = useState(0);

  const tableDummy = [];

  const handleOpenConfirm = () => {
    // setOnlineSwitch(false);
    setConfirmSuccess(true);
  };

  const handleCheckbox = (item) => {
    if (!checkList.includes(item)) {
      setCheckList([...checkList, item]);
    } else {
      const arr = checkList.filter((n) => n !== item);
      setCheckList(arr);
    }
  };

  const handleClickAll = (e) => {
    if (e.target.checked) {
      const arr = tableDummy.map((item) => item.Id);
      setCheckList(arr);
    } else {
      setCheckList([]);
    }
  };

  const tableConfig = [
    {
      title: "Merchant ID",
      headerAlign: "left",
      align: "left",
      width: 100,
      render: (rowData) => (
        <span
          style={{
            fontFamily: "FuturaBkBT",
            fontStyle: "normal",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "18px",
            letterSpacing: "0.03em",
            color: " #44495B",
          }}
        >
          {rowData.id}
        </span>
      ),
    },
    {
      title: "Jenis Merchant",
      headerAlign: "left",
      align: "left",
      width: 120,
      render: (rowData) => (
        <span
          style={{
            fontFamily: "FuturaBkBT",
            fontStyle: "normal",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "18px",
            letterSpacing: "0.03em",
            color: " #44495B",
          }}
        >
          {rowData.jenis}
        </span>
      ),
    },
    {
      title: "Nama Merchant",
      headerAlign: "left",
      align: "left",
      width: 100,
      render: (rowData) => (
        <span
          style={{
            fontFamily: "FuturaBkBT",
            fontStyle: "normal",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "18px",
            letterSpacing: "0.03em",
            color: " #44495B",
          }}
        >
          {rowData.name}
        </span>
      ),
    },
    {
      title: "Kota",
      headerAlign: "left",
      align: "left",
      width: 100,
      render: (rowData) => (
        <span
          style={{
            fontFamily: "FuturaBkBT",
            fontStyle: "normal",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "18px",
            letterSpacing: "0.03em",
            color: " #44495B",
          }}
        >
          {rowData.city}
        </span>
      ),
    },
    {
      title: "Alamat",
      headerAlign: "left",
      align: "left",
      width: 380,
      render: (rowData) => (
        <span
          style={{
            fontFamily: "FuturaBkBT",
            fontStyle: "normal",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "18px",
            letterSpacing: "0.03em",
            color: " #44495B",
          }}
        >
          {rowData.address}
        </span>
      ),
    },
  ];

  // get dataTable
  useEffect(() => {
    if (tableDummy) {
      const dataArr = tableDummy;
      const len = Math.ceil(tableDummy.length / 10);
      const temp = [];
      for (let i = 0; i < len; i++) {
        temp.push(dataArr.slice(i * 10, (i + 1) * 10));
      }
      setDataTable(temp);
    }
  }, []);

  return (
    <div>
      <Title
        label="List Merchant"
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Search
          options={["Merchant ID", "Nama Merchant", "Kota", "Alamat"]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          style={{ marginRight: "18px" }}
        />

        <ButtonOutlined
          label="Data Excel"
          iconPosition="startIcon"
          buttonIcon={<Upload />}
          width="141px"
          height="44px"
          style={{
            marginRight: 18,
            marginTop: 5,
            marginLeft: 18,
            fontFamily: "FuturaMdBT",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "20px",
          }}
        />

        <GeneralButton
          label="Tambah Merchant "
          iconPosition="startIcon"
          buttonIcon={<Plus />}
          width="184px"
          height="44px"
          style={{
            marginRight: 15,
            marginTop: 5,
            fontFamily: "FuturaMdBT",
            fontWeight: 400,
            fontSize: "15px",
            lineHeight: "20px",
          }}
          onClick={() => history.push("/portal/bisnis-pengaturan/merchant/add")}
        />
      </Title>
      <div className={classes.container}>
        <div>
          <TableICBB
            headerContent={tableConfig}
            dataContent={dataTable && dataTable[page - 1]}
            page={page}
            setPage={setPage}
            totalData={dataTable && dataTable.length}
            totalElement={tableDummy && tableDummy.length}
          />
        </div>
      </div>
      {/* <Modal
        label='Ubah Merchant'
        isOpen={modal}
        handleClose={() => setModal(false)}
      /> */}
    </div>
  );
};

Merchant.propTypes = {};

Merchant.defaultProps = {};

export default Merchant;
