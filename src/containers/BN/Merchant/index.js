import React, { useEffect, useState } from "react";
import {
  makeStyles,
  Button as MUIButton,
  CircularProgress,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import iconSync from "assets/icons/BN/IconSync.svg";
// component
import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";

import GeneralButton from "components/BN/Button/GeneralButton";
import { useDispatch, useSelector } from "react-redux";
import useDebounce from "utils/helpers/useDebounce";
import { getMerchantList, syncMerchantList } from "stores/actions/merchant";
import usePrevious from "utils/helpers/usePrevious";

const dataHeader = ({ history }) => [
  {
    title: "Merchant ID",
    key: "merchantId",
  },
  {
    title: "Merchant Name",
    key: "merchantName",
  },
  {
    title: "City",
    key: "city",
    render: () => "-", // forced all value to "-"
  },
];

const MercentList = () => {
  const history = useHistory();
  const { data, isLoading, isLoadingSync } = useSelector((e) => e.merchant);

  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState("");
  const [page, setPage] = useState(1);

  const useStyles = makeStyles({
    handlingofficer: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();
  const dispatch = useDispatch();

  const debSerch = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(debSerch);

  // get dataTable
  useEffect(() => {
    const payload = {
      search: dataSearch,
      pageNumber: page - 1,
      pageSize: 10,
      direction: "asc",
    };

    if (page === 1) {
      dispatch(getMerchantList(payload));
    } else if (debSerch !== prevSearch) {
      setPage(1);
    } else {
      dispatch(getMerchantList(payload));
    }
  }, [page, debSerch]);

  return (
    <React.Fragment>
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />
      <Title label="Merchant List">
        <Search
          placeholder="Merchant ID, Merchant Name"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
        <GeneralButton
          iconPosition="startIcon"
          buttonIcon={
            <img
              src={iconSync}
              alt="iconsync"
              style={{
                width: 18,
                height: 15,
                marginRight: -3,
              }}
            />
          }
          style={{
            width: 96,
            height: 44,
            fontSize: 15,
            fontFamily: "FuturaMdBT",
            paddingTop: 9.5,
            paddingBottom: 9.5,
            paddingRight: 20,
            paddingLeft: 20,
            marginLeft: 20,
            marginBottom: -10,
            background: isLoadingSync ? "#BCC8E7" : "#0061A7",
            backgroundColor: "#0061A7",
          }}
          label={
            isLoadingSync ? (
              <CircularProgress color="primary" size={20} />
            ) : (
              "Sync"
            )
          }
          onClick={() => dispatch(syncMerchantList())}
        />
      </Title>
      <div className={classes.handlingofficer}>
        <TableICBB
          headerContent={dataHeader({ history })}
          dataContent={data?.content || []}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
          isLoading={isLoading}
        />
      </div>
    </React.Fragment>
  );
};

MercentList.propTypes = {};

MercentList.defaultProps = {};

export default MercentList;
