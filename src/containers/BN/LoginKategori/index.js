// main
// import axios from 'axios';
// libraries
import {
  Grid,
  IconButton,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import Colors from "helpers/colors";
import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { handleLogout } from "stores/actions/login";

import { ReactComponent as SvgLaptop } from "assets/icons/BN/laptop.svg";
import { ReactComponent as SvgPhone } from "assets/icons/BN/mobile-alt.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    overflow: "hidden",
  },
  paperLeft: {
    padding: 20,
    textAlign: "center",
    color: Colors.dark.hard,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "360px",
    height: "273px",
    background: "#ffffff",
    border: "1px solid #E6EAF3",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    borderRadius: "20px",
    margin: "auto 63px",
    "&:hover": {
      color: "white",
      backgroundColor: Colors.primary.hard,
      boxShadow: "0px 8px 10px rgba(188, 200, 231, 0.5)",
      cursor: "pointer",
      "& path": {
        fill: "white",
      },
    },
  },
  paperRight: {
    padding: 20,
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    color: Colors.dark.hard,
    width: "360px",
    height: "273px",
    background: "#FFFFFF",
    border: "1px solid #E6EAF3",
    boxSizing: "border-box",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    borderRadius: "20px",
    zIndex: 10,
    margin: "auto 63px",
    "&:hover": {
      color: "white",
      backgroundColor: Colors.primary.hard,
      cursor: "pointer",
      boxShadow: "0px 8px 10px rgba(188, 200, 231, 0.5)",
      "& path": {
        fill: "white",
      },
    },
  },
  blaptop: {
    "& path": {
      fill: Colors.primary.hard,
    },
  },
  titleContainer: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    marginTop: 97,
    marginBottom: 93,
  },
  title: {
    textAlign: "center",
    fontFamily: "FuturaMdBT",
    fontStyle: "normal",
    fontSize: "40px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
}));

export default function CenteredGrid() {
  const dispatch = useDispatch();
  const history = useHistory();

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.titleContainer}>
        <Typography className={classes.title}>Select Category</Typography>
      </div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Paper
          onClick={() => {
            window.location.href = `${process.env.REACT_APP_PORTAL_DOMAIN}/portal/`;
          }}
          className={classes.paperLeft}
        >
          <center>
            <div
              style={{
                textAlign: "center",
                marginBottom: 20,
              }}
            >
              <SvgLaptop className={classes.blaptop} width={50} height={50} />
            </div>
            <Typography
              style={{
                textAlign: "center",
                marginBottom: 10,
                fontSize: 17,
                fontFamily: "FuturaMdBT",
              }}
              // variant="h4"
              gutterBottom
              component="div"
            >
              Internet Banking Business
            </Typography>
            <Typography
              style={{
                textAlign: "center",
                fontSize: 13,
                fontFamily: "FuturaBkBT",
                padding: "0 50px",
              }}
              variant="body1"
              gutterBottom
              color="white"
            >
              E-banking services to perform financial and non-financial
              transactions with internet banking applications that are intended
              for business customers from the individual and corporate segments
            </Typography>
          </center>
        </Paper>
        <Paper
          onClick={() => {
            window.location.href = `${process.env.REACT_APP_PORTAL_MB_DOMAIN}/login`;
          }}
          className={classes.paperRight}
        >
          <center>
            <div
              style={{
                textAlign: "center",
                marginBottom: 20,
              }}
            >
              <SvgPhone width={50} height={50} />
            </div>
            <Typography
              style={{
                textAlign: "center",
                fontFamily: "FuturaMdBT",
                marginBottom: 10,
                fontSize: 17,
              }}
              // variant="h4"
              gutterBottom
              component="div"
            >
              Mobile Banking
            </Typography>
            <Typography
              style={{
                textAlign: "center",
                fontSize: 13,
                fontFamily: "FuturaBkBT",
                padding: "0 60px",
              }}
              variant="body1"
              gutterBottom
              color="white"
            >
              A service that allows bank customers to conduct banking
              transactions via mobile phones or smartphones
            </Typography>
          </center>
        </Paper>
      </div>
      <div></div>
    </div>
  );
}
