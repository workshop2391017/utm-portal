import { makeStyles, Typography } from "@material-ui/core";
import { useSelector } from "react-redux";

const WorkshopCustomPage = () => {
  const { data } = useSelector(({ workshop }) => ({
    data: workshop.data,
  }));

  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.content}>
        <Typography>Nama Depan: {data.namaDepan}</Typography>
        <Typography>Nama Belakang: {data.namaBelakang}</Typography>
        <Typography>Hobi: {data.hobi}</Typography>
      </div>
    </div>
  );
};

export default WorkshopCustomPage;

const useStyles = makeStyles({
  container: {
    padding: "20px",
    margin: "auto",
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    width: "400px",
    padding: "30px",
    backgroundColor: "#ffffff",
    display: "flex",
    flexDirection: "column",
    gap: "15px",
  },
});
