import React, { useEffect, useState } from "react";

import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";

// components
import TableICBB from "components/BN/TableIcBB";
import Badge from "components/BN/Badge";
import Date from "components/BN/DatePicker/GeneralDatePicker";
import DropdownSelect from "components/BN/Select/AntdSelectWithCheck";
import DropdownSelectNoBorder from "components/BN/Select/AntdNoBorder";
import { ReactComponent as Icon } from "assets/icons/BN/refresh-ccw.svg";
import useDebounce from "utils/helpers/useDebounce";
import GeneralButton from "components/BN/Button/GeneralButton";

import moment from "moment";
import {
  formatAmountDot,
  parseNumber,
  searchDateEnd,
  searchDateStart,
} from "utils/helpers";
import {
  getDataStatusTransaksi,
  getDataBulkStatusTransaksi,
  getDataStatusTransaksiDetail,
  setStepPages,
  getTransactionCategory,
  getTransactionStatus,
  getTransactionGroup,
  setUserProfileId,
  setTransactionGroup,
} from "stores/actions/statusTransaksi";
import Title from "../../../components/BN/Title";

import Search from "../../../components/BN/Search/SearchWithoutDropdown";
import Tabs from "../../../components/BN/Tabs/GeneralTabs";
import Filter from "../../../components/BN/Filter/GeneralFilterInquiry";

const dataHeader = ({ dataFilter, dispatch, classes }) => [
  {
    title: "Company Information",
    render: (rowData) => (
      <React.Fragment>
        <div className={classes.companyWrap}>
          <div>
            {moment(rowData.transactionDate).format("DD MMMM YYYY")}{" "}
            {moment(rowData.transactionDate).format("HH:MM:ss")}
          </div>
          <div className={classes.tableBold}>
            {rowData?.corporateProfile?.corporateName}
          </div>
          <div>{rowData?.corporateProfile?.corporateId}</div>
        </div>
      </React.Fragment>
    ),
  },
  {
    title: "User Information",
    render: (rowData) => (
      <div className={classes.companyWrap}>
        <div className={classes.tableBold}>{rowData.fullName}</div>
        <div>{rowData.userProfileId}</div>
      </div>
    ),
  },
  {
    title: "Account",
    render: (rowData) => (
      <div className={classes.companyWrap}>
        <div className={classes.tableBold}>{rowData.fromAccountOwnerName}</div>
        <div>{rowData.fromAccountNumber}</div>
      </div>
    ),
  },
  {
    title: "Amount",
    render: (rowData) => (
      <div>
        {`Rp. ${formatAmountDot(rowData?.transactionAmount?.toString())}`}
      </div>
    ),
  },
  {
    title: "Transaction Type",
    render: (rowData) => (
      <div style={{ textTransform: "capitalize" }}>
        {rowData.transactionGroup.toLowerCase()}
      </div>
    ),
  },
  {
    title: "Category",
    render: (rowData) => (
      <div style={{ textTransform: "capitalize" }}>
        {rowData.transactionCategory
          .toLowerCase()
          .replace("_", " ")
          .replace("_", " ")}
      </div>
    ),
  },
  {
    title: "Status",
    width: 100,
    align: "left",
    render: (rowData) => {
      if (rowData.transactionStatus === "SUCCESS") {
        return (
          <Badge
            label="Success"
            type="green"
            styleBadge={{
              border: "1px solid #75D37F",
              textAlign: "center",
              width: "59px",
            }}
            fitWidth
          />
        );
      }

      if (rowData.transactionStatus === "FAILED") {
        return (
          <Badge
            label="Failed"
            type="red"
            styleBadge={{
              border: "1px solid #D14848",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
      if (rowData.transactionStatus === "REJECTED") {
        return (
          <Badge
            label="Rejected"
            type="red"
            styleBadge={{
              border: "1px solid #D14848",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
      if (rowData.transactionStatus === "SUSPECT") {
        return (
          <Badge
            label="Diproses"
            type="orange"
            styleBadge={{
              border: "1px solid #FFA24B",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
      if (rowData.transactionStatus === "WAITING") {
        return (
          <Badge
            label="Waiting for Approval"
            type="blue"
            styleBadge={{
              border: " 1px solid #66A3FF",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
    },
  },
  {
    title: "",
    width: "100px",
    render: (rowData) => (
      <GeneralButton
        label="View Details"
        width="76px"
        height="23px"
        style={{ marginRight: 11, fontSize: 9, padding: 0 }}
        onClick={() =>
          rowData?.bulk === false
            ? (dispatch(
                getDataStatusTransaksiDetail({
                  transactionId: rowData.transactionId,
                  language: "IDN",
                  userProfileId: rowData.userProfileId,
                })
              ),
              [
                "TRANSFER_OA",
                "TRANSFER_ON_US",
                "TRANSFER_OFF_US",
                "TRANSFER",
                "TRANSFER_SKN",
                "TRANSFER_RTGS",
              ].includes(rowData?.transactionCategory)
                ? dispatch(setStepPages(2))
                : rowData?.transactionGroup === "SWEEPING"
                ? dispatch(setStepPages(3))
                : dispatch(setStepPages(4)),
              dispatch(setUserProfileId(rowData?.userProfileId)))
            : (dispatch(
                getDataBulkStatusTransaksi({
                  language: "ENG",
                  page: 1,
                  parentTransactionId: rowData?.parentTransactionId,
                  size: 10,
                  userProfileId: rowData?.userProfileId,
                })
              ),
              dispatch(setStepPages(1)),
              dispatch(setUserProfileId(rowData?.userProfileId)),
              dispatch(setTransactionGroup(rowData?.transactionGroup)))
        }
      />
    ),
  },
];

const useStyles = makeStyles(() => ({
  page: {
    padding: "20px",
  },
  button: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    marginTop: "-3px",
  },
  status: {
    width: "1054px",
    height: "416px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    background: "#FFFFFF",
    marginLeft: "28px",
    marginTop: "36px",
    borderRadius: "8px",
    flexDirection: "column",
    fontFamily: "FuturaMdBT",
    fontWeight: "400",
    fontSize: "21px",
    color: "#7B87AF",
  },
  table: {
    display: "flex",
    flexDirection: "column",
    marginTop: "10px",
  },
  filter: {
    display: "flex",
    flexDirection: "column",
    alignItems: "start",
    justifyContent: "center",
    minHeight: 70,
    height: "auto",
    backgroundColor: "#fff",
    borderRadius: 8,
    padding: "20px 20px",
  },
  childFilter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginBottom: "12px",
  },
  companyWrap: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "left",
    gap: "10px",
  },
  tableBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
  tableBlank: {
    width: "100%",
    height: 416,
    boxShadow: `0px 3px 10px rgba(188, 200, 231, 0.2)`,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginRight: -10,
  },
}));

const StatusTransaksi = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  const [pages, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const payloadSearch = useDebounce(search, 1500);
  const {
    dataTransaksi,
    isLoading,
    dataTransaksiCategory,
    dataTransaksiStatus,
    dataTransaksiGroup,
  } = useSelector((state) => state.statusTransaksi);

  const [dataFilter, setDataFilter] = useState(null);
  const [dataContentTable, setDataContentTable] = useState([]);

  const getCategory = () => {
    const payload = {};
    dispatch(getTransactionCategory(payload));
  };

  const getStatus = () => {
    const payload = {};
    dispatch(getTransactionStatus(payload));
  };

  const getGroup = () => {
    const payload = {};
    dispatch(getTransactionGroup(payload));
  };

  useEffect(() => {
    const payload = {
      channelCode: "CORPORATE_INTERNET_BANKING",
      dateFrom: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      dateTo: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      transactionStatus: dataFilter?.dropdown?.dropdown3,
      transactionGroup: dataFilter?.dropdownAdvanceVal?.type,
      transactionCategory: dataFilter?.dropdownAdvanceVal?.category,
      toBtnTransfer: null,
      transferOA: null,
      page: pages,
      size: 10,
      searchValue: payloadSearch,
      userProfileId: null,
    };
    dispatch(getDataStatusTransaksi(payload));
  }, [dataFilter, payloadSearch, pages]);

  useEffect(() => {
    if (!dataTransaksiCategory) {
      getCategory();
    }
  }, [dataTransaksiCategory]);

  useEffect(() => {
    if (!dataTransaksiStatus) {
      getStatus();
    }
  }, [dataTransaksiStatus]);

  useEffect(() => {
    if (!dataTransaksiGroup) {
      getGroup();
    }
  }, [dataTransaksiGroup]);

  useEffect(() => {
    if (Object.values(dataTransaksi).length !== 0) {
      setDataContentTable(dataTransaksi?.transactionHistoryList);
    }
  }, [dataTransaksi]);

  return (
    <div className={classes.page}>
      <Title label="Transaction Status">
        <Search
          placeholder="Company ID, Company Name"
          setDataSearch={setSearch}
          dataSearch={search}
          style={{
            width: "240px",
            height: "40px",
          }}
        />
      </Title>
      <div className={classes.filter}>
        <div className={classes.childFilter}>
          <Filter
            align="left"
            setDataFilter={(e) => setDataFilter(e)}
            dataFilter={dataFilter}
            options={[
              {
                id: 1,
                type: "datePicker",
                placeholder: "Start Date",
              },
              {
                id: 2,
                type: "datePicker",
                placeholder: "End Date",
                disabled: true,
              },
              {
                id: 3,
                type: "dropdown",
                options:
                  dataTransaksiStatus?.transactionStatusDtoList?.map((val) => ({
                    lable: val?.transactionStatusName,
                    value: val?.transactionStatusEnum,
                  })) ?? [],
                placeholder: "Choose Transaction Status",
                width: 250,
              },
            ]}
            isAdvance
            optionsAdvance={[
              {
                id: 4,
                type: "dropdown",
                options: dataTransaksiGroup?.transactionStatusDtoList?.map(
                  (val) => ({
                    lable: val?.transactionGroupName,
                    value: val?.transactionGroupEnum,
                  })
                ),
                placeholder: "Choose Transaction Type",
                width: 250,
              },
              {
                id: 5,
                type: "dropdown",
                options: dataTransaksiCategory?.transactionCategoryDtoList?.map(
                  (val) => ({
                    lable: val.transactionCategoryName,
                    value: val?.transactionCategoryEnum,
                  })
                ),
                placeholder: "Choose Category",
                width: 250,
              },
            ]}
            dropdownAdvance
          />
        </div>
      </div>

      <div className={classes.table}>
        <TableICBB
          isFullTable={false}
          isLoading={isLoading}
          headerContent={dataHeader({ dataFilter, history, dispatch, classes })}
          dataContent={dataContentTable ?? []}
          totalData={dataTransaksi?.totalPages}
          totalElement={dataTransaksi?.totalElements}
          page={pages}
          setPage={setPage}
        />
      </div>
    </div>
  );
};

StatusTransaksi.propTypes = {};

export default StatusTransaksi;
