import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import { useHistory } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import { formatAmountDot } from "utils/helpers";
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import {
  Typography,
  CardContent,
  CircularProgress,
  Card,
  Button as BackButton,
} from "@material-ui/core";
import moment from "moment";
import {
  getDataDownloadResi,
  setDataTransaksiDetail,
  setStepPages,
} from "stores/actions/statusTransaksi";
import { handleFinancialDetail } from "stores/actions/consolidationReport";

// components
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right-white.svg";
import Badge from "components/BN/Badge/BadgeCustom";
import Title from "../../../../components/BN/Title";
import Button from "../../../../components/BN/Button/GeneralButton";

import { ReactComponent as Icon } from "../../../../assets/icons/BN/unduh-white.svg";
import TaskInformation from "../Popup";

const useStyles = makeStyles((theme) => ({
  page: {
    padding: "10px 20px",
    display: "flex",
    flexDirection: "column",
  },
  backButton: {
    ...theme.typography.backButton,
    marginTop: "24px",
    padding: "10px 20px",
    marginLeft: "29px",
  },
  main: {
    height: "100%",
    background: "#FFFFFF",
    borderRadius: "10px",
    marginTop: "24px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "29px",
  },
  header: {
    gap: "10px",
    background: "#75D37F",
    display: "flex",
    flexDirection: "column",
    height: "66px",
    padding: "10px 20px",
    borderRadius: "10px 10px 0 0",
    alignItems: "flex-start",
  },
  headerDiproses: {
    gap: "10px",
    background: "#FFA24B",
    display: "flex",
    flexDirection: "column",
    height: "66px",
    padding: "10px 20px",
    borderRadius: "10px 10px 0 0",
    alignItems: "flex-start",
  },
  headerGagal: {
    gap: "10px",
    background: "#FF6F6F",
    display: "flex",
    flexDirection: "column",
    height: "92px",
    padding: "10px 20px",
    borderRadius: "10px 10px 0 0",
    alignItems: "flex-start",
  },
  headerDitolak: {
    gap: "10px",
    background: "#FF6F6F",
    display: "flex",
    flexDirection: "column",
    height: "66px",
    padding: "10px 20px",
    borderRadius: "10px 10px 0 0",
    alignItems: "flex-start",
  },
  headerWaiting: {
    gap: "10px",
    background: "#66A3FF",
    display: "flex",
    flexDirection: "column",
    height: "66px",
    padding: "10px 20px",
    borderRadius: "10px 10px 0 0",
    alignItems: "flex-start",
  },
  spanHeader1: {
    fontFamily: "FuturaHvBT",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "17px",
    lineHeight: "20px",
    letterSpacing: "0.01em",
    color: " #FFFFFF",
  },

  spanHeader2: {
    fontFamily: "FuturaMdBT",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    letterSpacing: "0.03em",
    color: " #FFFFFF",
  },
  container: {
    display: "flex",
    width: "100%",
    padding: "10px 20px",
  },
  leftContainer: {
    flex: "40%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  left: {
    padding: "0px 30px 0px 0px",
    display: "flex",
    flexDirection: "column",
  },
  leftDetail: {
    display: "flex",
    justifyContent: "flex-start",
    flexDirection: "column",
    gap: "10px",
    padding: "0px 30px 0px 0px",
  },
  leftName: {
    fontFamily: "FuturaHvBT",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "17px",
    lineHeight: "20px",
    letterSpacing: "0.03em",
    color: " #374062",
    marginTop: "12px",
  },
  right: {
    flex: "60%",
  },
  rightContent: {
    display: "flex",
    justifyContent: "space-between",
  },
  rightSpan1: {
    fontFamily: "FuturaBkBT",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.03em",
    color: " #374062",
  },
  rightSpan2: {
    fontFamily: "FuturaMdBT",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: " #374062",
  },
  badge: {
    padding: "8px 8px !important",
  },
  footer: {
    background: "#FFFFFF",
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    height: "84px",
    alignItems: "center",
    marginTop: "109px",
    padding: "20px",
  },
  taskInformation: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#F4F7FB",
    borderRadius: "10px",
    padding: "20px 20px",
    width: "350px",
    gap: "20px",
  },
  taskInformationTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: "20px",
    fontWeight: 700,
    lineHeight: "24px",
    letterSpacing: "0.01em",
    fontStyle: "bold",
  },
  taskInformationDetail: {
    fontFamily: "FuturaBkBT",
    fontSize: "15px",
    fontWeight: 400,
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
  loading: {
    display: "flex",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  divider: {
    borderTop: "1px solid #E6EAF3",
  },
}));

const SweepResi = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { dataTransaksiDetail, isLoading, userProfileId, transactionCategory } =
    useSelector((state) => state.statusTransaksi);
  const { financialDetail } = useSelector((state) => state.consolidationReport);
  const [dataDetail, setdataTransaksiDetail] = useState({});
  const [openTaskInformation, setOpenTaskInformation] = useState(false);
  const [template, setTemplate] = useState([]);

  const renderTotalAmount = () => {
    const data =
      dataTransaksiDetail?.transactionAmount +
      dataTransaksiDetail?.transactionFee;
    return formatAmountDot(data.toString());
  };

  const onClikOpenTaskInformation = () => {
    setOpenTaskInformation(true);
    const payload = {
      transactionGroupId: dataTransaksiDetail?.transactionGroupId,
      transactionId: dataTransaksiDetail?.transactionId,
      userProfileIdMaker: userProfileId,
    };
    dispatch(handleFinancialDetail(payload));
  };

  useEffect(() => {}, [dataTransaksiDetail]);
  const handleBack = () => {
    dispatch(setStepPages(0));
  };
  return (
    <div>
      <TaskInformation
        isOpen={openTaskInformation}
        handleClose={() => setOpenTaskInformation(false)}
      />
      <div>
        <BackButton
          disableElevation
          component="button"
          size="small"
          startIcon={<ArrowLeft />}
          className={classes.backButton}
          onClick={handleBack}
        >
          Back
        </BackButton>
      </div>
      <div className={classes.page}>
        <Title label="Transaction Receipt">
          <Button
            iconPosition="startIcon"
            buttonIcon={<Icon />}
            label="Download Receipt"
            width="auto"
            height="44px"
            style={{
              marginTop: 5,
              fontFamily: "FuturaMdBT",
              fontStyle: "normal",
              fontWeight: 700,
              fontSize: "15px",
              lineHeight: "24px",
              letterSpacing: "0.03em",
            }}
            onClick={() => {
              dispatch(
                getDataDownloadResi({
                  transactionId: dataTransaksiDetail?.transactionId,
                  userProfileId,
                })
              );
            }}
          />
        </Title>

        <div className={classes.main}>
          {dataTransaksiDetail?.transactionStatus === "SUCCESS" ? (
            <div className={classes.header}>
              <div className={classes.spanHeader1}>
                <span>Transaction Succesfull</span>
              </div>
              <div className={classes.spanHeader2}>
                <span>
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "DD/MM/YY"
                  )}
                  |
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "HH:MM:ss"
                  )}
                </span>
              </div>
            </div>
          ) : dataTransaksiDetail?.transactionStatus === "FAILED" ? (
            <div className={classes.headerGagal}>
              <div
                style={{ display: "flex", flexDirection: "column" }}
                className={classes.spanHeader1}
              >
                <span>Transaction Failed</span>
                <span
                  style={{
                    fontFamily: "FuturaMdBT",
                    fontStyle: "normal",
                    fontWeight: 400,
                    fontSize: "13px",
                    lineHeight: "16px",
                    letterSpacing: "0.03em",
                    marginTop: "8px",
                  }}
                >
                  505 - Request Time Out
                </span>
              </div>
              <div className={classes.spanHeader2}>
                <span>
                  {" "}
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "DD/MM/YY"
                  )}
                  |
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "HH:MM:ss"
                  )}
                </span>
              </div>
            </div>
          ) : dataTransaksiDetail?.transactionStatus === "PROCESSED" ? (
            <div className={classes.headerDiproses}>
              <div className={classes.spanHeader1}>
                <span>Transaction Processed</span>
              </div>
              <div className={classes.spanHeader2}>
                <span>
                  {" "}
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "DD/MM/YY"
                  )}
                  |
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "HH:MM:ss"
                  )}
                </span>
              </div>
            </div>
          ) : dataTransaksiDetail?.transactionStatus === "REJECTED" ? (
            <div className={classes.headerDitolak}>
              <div className={classes.spanHeader1}>
                <span>Transaction Rejected</span>
              </div>
              <div className={classes.spanHeader2}>
                <span>
                  {" "}
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "DD/MM/YY"
                  )}
                  |
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "HH:MM:ss"
                  )}
                </span>
              </div>
            </div>
          ) : dataTransaksiDetail?.transactionStatus === "WAITING" ? (
            <div className={classes.headerWaiting}>
              <div className={classes.spanHeader1}>
                <span>Transactions Awaiting Approval</span>
              </div>
              <div className={classes.spanHeader2}>
                <span>
                  {" "}
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "DD/MM/YY"
                  )}
                  |
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "HH:MM:ss"
                  )}
                </span>
              </div>
            </div>
          ) : dataTransaksiDetail?.transactionStatus === "SUSPECT" ? (
            <div className={classes.headerWaiting}>
              <div className={classes.spanHeader1}>
                <span>Transactions Awaiting Approval</span>
              </div>
              <div className={classes.spanHeader2}>
                <span>
                  {" "}
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "DD/MM/YY"
                  )}
                  |
                  {moment(dataTransaksiDetail?.transactionDate).format(
                    "HH:MM:ss"
                  )}
                </span>
              </div>
            </div>
          ) : null}
          {isLoading ? (
            <div className={classes.loading}>
              <CircularProgress color="primary" size={40} />
            </div>
          ) : (
            <div className={classes.container}>
              <div className={classes.leftContainer}>
                <div className={classes.leftDetail}>
                  <div className={classes.left}>
                    <span>Fund Source Account:</span>
                    <span className={classes.leftName}>
                      {dataTransaksiDetail?.fromAccountOwnerName}
                    </span>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        marginTop: "12px",
                      }}
                    >
                      <span>No. Account</span>
                      <span>{dataTransaksiDetail?.toAccountNumber ?? "-"}</span>
                    </div>
                  </div>
                  <div className={classes.divider} />
                  <div className={classes.left}>
                    <span>Account:</span>
                    <span className={classes.leftName}>
                      {dataTransaksiDetail?.toAccountOwnerName ?? "-"}
                    </span>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        marginTop: "12px",
                      }}
                    >
                      <span>No. Account</span>
                      <span>{dataTransaksiDetail?.toAccountNumber ?? "-"}</span>
                    </div>
                  </div>
                </div>
                {dataTransaksiDetail?.isMultiple !== true ? (
                  <div className={classes.taskInformation}>
                    <Typography className={classes.taskInformationTitle}>
                      View Task Information
                    </Typography>
                    <Typography className={classes.taskInformationDetail}>
                      Monitor the status of your task
                    </Typography>
                    <Button
                      width={250}
                      buttonIcon={<ArrowRight />}
                      label="View Task Information"
                      onClick={onClikOpenTaskInformation}
                    />
                  </div>
                ) : null}
              </div>
              <div className={classes.right}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    gap: "16px",
                  }}
                >
                  <React.Fragment>
                    <div className={classes.rightContent}>
                      <span className={classes.rightSpan1}>ID Transaksi :</span>
                      <span className={classes.rightSpan2}>
                        3576447103910003
                      </span>
                    </div>
                    <div className={classes.rightContent}>
                      <span className={classes.rightSpan1}>No Ref :</span>
                      <span className={classes.rightSpan2}>
                        {dataTransaksiDetail?.transactionId}
                      </span>
                    </div>
                    <div className={classes.rightContent}>
                      <span className={classes.rightSpan1}>
                        Kategori Account Sweeping :
                      </span>
                      <span className={classes.rightSpan2}>
                        {dataTransaksiDetail?.categoryName}
                      </span>
                    </div>
                    <div className={classes.rightContent}>
                      <span className={classes.rightSpan1}>
                        Pilihan Account Sweeping :
                      </span>
                      <span className={classes.rightSpan2}>
                        {dataTransaksiDetail?.transactionChoice}
                      </span>
                    </div>
                    <div className={classes.rightContent}>
                      <span className={classes.rightSpan1}>
                        Nominal Tersisa :
                      </span>
                      <span className={classes.rightSpan2}>
                        Rp{" "}
                        {formatAmountDot(
                          dataTransaksiDetail?.maintainBalance?.toString()
                        )}
                      </span>
                    </div>
                    <div className={classes.rightContent}>
                      <span className={classes.rightSpan1}>
                        Nominal Ditransfer :
                      </span>
                      <span className={classes.rightSpan2}>
                        Rp{" "}
                        {formatAmountDot(
                          dataTransaksiDetail?.transactionAmoun?.toString()
                        )}
                      </span>
                    </div>
                  </React.Fragment>
                  <div className={classes.rightContent}>
                    <span className={classes.rightSpan1}>Biaya Transaksi</span>
                    <span className={classes.rightSpan2}>
                      Rp{" "}
                      {formatAmountDot(
                        dataTransaksiDetail?.transactionFee?.toString()
                      )}
                    </span>
                  </div>
                  <div className={classes.rightContent}>
                    <span className={classes.rightSpan1}>Biaya Admin</span>
                    <span className={classes.rightSpan2}>
                      Rp{" "}
                      {formatAmountDot(
                        dataTransaksiDetail?.feeTotal?.toString()
                      )}
                    </span>
                  </div>
                  <div className={classes.rightContent}>
                    <span className={classes.rightSpan1}>Total Biaya</span>
                    <span className={classes.rightSpan2}>
                      Rp{" "}
                      {formatAmountDot(
                        dataTransaksiDetail?.transactionAmount?.toString()
                      )}
                    </span>
                  </div>
                </div>
                <hr
                  style={{
                    border: 0,
                    borderBottom: "1px dashed #E6EAF3",
                    marginTop: "20px",
                  }}
                />
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between	",
                    marginTop: "28px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "FuturaBkBT",
                      fontStyle: "normal",
                      fontWeight: 400,
                      fontSize: "15px",
                      lineHeight: "18px",
                      letterSpacing: "0.03em",
                      color: " #0061A7",
                    }}
                  >
                    Total Transaksi
                  </span>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <span
                      // pan
                      style={{
                        fontFamily: "FuturaMdBT",
                        fontStyle: "normal",
                        fontWeight: 400,
                        fontSize: "15px",
                        lineHeight: "24px",
                        letterSpacing: "0.03em",
                        color: " #0061A7",
                      }}
                    >
                      Rp{" "}
                      {formatAmountDot(
                        dataTransaksiDetail?.totalTransaction?.toString()
                      )}
                    </span>
                  </div>
                </div>

                <div style={{ marginTop: "12px" }}>
                  <Badge
                    label="Resi ini adalah bukti transaksi yang sah."
                    type="blue"
                    styleBadge={{
                      border: " 1px solid #66A3FF",
                      textAlign: "center",
                      padding: "8px 8px !important",
                      width: "630px",
                      height: "36px",
                      borderRadius: "5px",
                    }}
                    padding="12px 12px !important"
                    className={classes.badge}
                    fitWidth
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className={classes.footer}>
        <Button
          label="Close"
          width="84px"
          height="44px"
          style={{
            borderRadius: "10px",
          }}
          onClick={() => {
            setdataTransaksiDetail({});
            dispatch(setDataTransaksiDetail({}));
            dispatch(setStepPages(0));
          }}
        />
      </div>
    </div>
  );
};

SweepResi.propTypes = {};

export default SweepResi;
