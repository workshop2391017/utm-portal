import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Title from "components/BN/Title";
import { Button, Typography } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right-white.svg";
import { ReactComponent as CalendarBlue } from "assets/icons/BN/calendarblue.svg";
import TableICBB from "components/BN/TableIcBB";
import Badge from "components/BN/Badge";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { handleFinancialDetail } from "stores/actions/consolidationReport";
import { useDispatch, useSelector } from "react-redux";
import {
  getDataStatusTransaksiDetail,
  setStepPages,
} from "stores/actions/statusTransaksi";
import { formatAmountDotCurrency } from "utils/helpers";
import moment from "moment";
import TaskInformation from "../Popup";

const dataHeader = ({ classes, dispatch, userProfileId }) => [
  {
    title: "Account",
    render: (rowData) => (
      <React.Fragment>
        <div className={classes.accountWrap}>
          <div className={classes.tableBold}>
            {rowData?.fromAccountOwnerName}
          </div>
          <div>{rowData?.fromAccountNumber}</div>
        </div>
      </React.Fragment>
    ),
  },
  {
    title: "Fund Source Account",
    render: (rowData) => (
      <React.Fragment>
        <div className={classes.accountWrap}>
          <div className={classes.tableBold}>{rowData?.toAccountOwnerName}</div>
          <div>{rowData?.toAccountNumber}</div>
        </div>
      </React.Fragment>
    ),
  },
  {
    title: "Amount",
    render: (rowData) => (
      <div>
        {`${formatAmountDotCurrency(rowData?.totalAmmount?.toString())},00`}
      </div>
    ),
  },
  {
    title: "Status",
    width: 200,
    align: "left",
    render: (rowData) => {
      if (rowData.transactionStatus === "SUCCESS") {
        return (
          <Badge
            label="Success"
            type="green"
            styleBadge={{
              border: "1px solid #75D37F",
              textAlign: "center",
              width: "59px",
            }}
            fitWidth
          />
        );
      }

      if (rowData.transactionStatus === "FAILED") {
        return (
          <Badge
            label="Failed"
            type="red"
            styleBadge={{
              border: "1px solid #D14848",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
      if (rowData.transactionStatus === "REJECTED") {
        return (
          <Badge
            label="Rejected"
            type="red"
            styleBadge={{
              border: "1px solid #D14848",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
      if (rowData.transactionStatus === "SUSPECT") {
        return (
          <Badge
            label="Diproses"
            type="orange"
            styleBadge={{
              border: "1px solid #FFA24B",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
      if (rowData.transactionStatus === "WAITING") {
        return (
          <Badge
            label="Waiting for Approval"
            type="blue"
            styleBadge={{
              border: " 1px solid #66A3FF",
              textAlign: "center",
              width: "69px",
            }}
            fitWidth
          />
        );
      }
    },
  },
  {
    title: "",
    render: (rowData) => (
      <GeneralButton
        label="View Details"
        width="76px"
        height="23px"
        style={{ marginRight: 11, fontSize: 9, padding: 0 }}
        onClick={() => {
          dispatch(
            getDataStatusTransaksiDetail({
              transactionId: rowData.transactionId,
              language: "IDN",
              userProfileId,
            })
          );
          if (
            [
              "TRANSFER_OA",
              "TRANSFER_ON_US",
              "TRANSFER_OFF_US",
              "TRANSFER",
              "TRANSFER_SKN",
              "TRANSFER_RTGS",
            ].includes(rowData?.transactionCategory)
          )
            dispatch(setStepPages(2));
          else if (rowData?.transactionCategory === "SWEEPING")
            dispatch(setStepPages(3));
          else dispatch(setStepPages(4));
        }}
      />
    ),
  },
];

const useStyles = makeStyles(() => ({
  container: {
    display: "flex",
    flexDirection: "column",
    padding: "30px 30px 30px 30px",
    gap: "30px",
  },
  title: {
    display: "flex",
    flexDirection: "column",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: "10px",
    padding: "10px 10px",
  },
  transactionContainer: {
    display: "grid",
    gridTemplateColumns: "auto auto",
    justifyContent: "space-between",
    padding: "10px 10px",
    gap: "25px",
  },
  transactionDetail: {
    display: "flex",
    flexDirection: "column",
    gap: "13px",
  },
  taskInformation: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#F4F7FB",
    borderRadius: "10px",
    padding: "20px 20px",
    gap: "20px",
  },
  taskInformationTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: "20px",
    fontWeight: 700,
    lineHeight: "24px",
    letterSpacing: "0.01em",
    fontStyle: "bold",
  },
  taskInformationDetail: {
    fontFamily: "FuturaBkBT",
    fontSize: "15px",
    fontWeight: 400,
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
  date: {
    display: "flex",
    gap: "10px",
  },
  accountWrap: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "left",
    gap: "10px",
  },
  tableBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
}));

const BulkTransaksi = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openTaskInformation, setOpenTaskInformation] = useState(false);
  const { dataBulkTransaksi, isLoading, userProfileId, transactionGroup } =
    useSelector((state) => state.statusTransaksi);

  const [pages, setPage] = useState(1);
  const handleBack = () => {
    dispatch(setStepPages(0));
  };

  const trxGroupList = [
    "OPENING_ACCOUNT",
    "PURCHASE ",
    "BILLPAYMENT",
    "TRANSFER",
    "SWEEPING",
    "CARDLESS",
    "BANCASSURANCE",
    "PROXY_BIFAST",
    "MUTUAL_FUND",
    "SBN",
    "MY_ACCOUNT",
    "DEPOSITO",
    "VIMART",
    "INVESTMENT",
    "PAYROLL",
    "AUTO_COLLECTION",
  ];

  const TransactionGroupId = trxGroupList.indexOf(transactionGroup);

  const onClikOpenTaskInformation = () => {
    setOpenTaskInformation(true);
    const payload = {
      transactionGroupId: TransactionGroupId,
      parentTransactionId: dataBulkTransaksi?.parentTransactionId,
      userProfileIdMaker: userProfileId,
    };
    dispatch(handleFinancialDetail(payload));
  };

  return (
    <div className={classes.container}>
      <TaskInformation
        isOpen={openTaskInformation}
        handleClose={() => setOpenTaskInformation(false)}
      />
      <div className={classes.title}>
        <Button
          style={{ width: "20px" }}
          startIcon={<img src={arrowLeft} alt="Kembali" />}
          onClick={handleBack}
        />
        <Typography className={classes.taskInformationTitle}>
          Transaction Receipt
        </Typography>
      </div>
      <div className={classes.paper}>
        <div className={classes.transactionContainer}>
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              Total Transaction:
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              {`${formatAmountDotCurrency(
                dataBulkTransaksi?.totalAmmount?.toString()
              )},00`}
            </Typography>
          </div>
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              Transaction Amount:
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              {dataBulkTransaksi?.numberOfElements}
            </Typography>
          </div>
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              Scheduler:
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              <Badge
                label={dataBulkTransaksi?.dataTransfer?.transactionChoice}
                type="blue"
                styleBadge={{
                  width: "100px",
                  textAlign: "center",
                }}
              />
            </Typography>
          </div>
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              Transaction ID:
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              {dataBulkTransaksi?.parentTransactionId}
            </Typography>
          </div>
          <div className={classes.date}>
            <CalendarBlue />
            <Typography className={classes.taskInformationDetail}>
              Start From
              {moment(
                dataBulkTransaksi?.transactionHistoryList?.[0]
                  ?.dateTimeTransaction
              )?.format("DD/MM/YYYY") || null}
            </Typography>
            <CalendarBlue />
            <Typography className={classes.taskInformationDetail}>
              End At
              {moment(
                dataBulkTransaksi?.transactionHistoryList?.[
                  dataBulkTransaksi?.transactionHistoryList?.length
                ]?.dateTimeTransaction
              )?.format("DD/MM/YYYY") || null}
            </Typography>
          </div>
        </div>
        <div>
          {dataBulkTransaksi?.isMultiple ? (
            <div className={classes.taskInformation}>
              <Typography className={classes.taskInformationTitle}>
                View Task Information
              </Typography>
              <Typography className={classes.taskInformationDetail}>
                Monitor the status of your task
              </Typography>
              <GeneralButton
                width={250}
                buttonIcon={<ArrowRight />}
                label="View Task Information"
                onClick={onClikOpenTaskInformation}
              />
            </div>
          ) : null}
        </div>
      </div>
      <TableICBB
        isFullTable={false}
        isLoading={isLoading}
        headerContent={dataHeader({ dispatch, classes, userProfileId })}
        dataContent={dataBulkTransaksi?.transactionHistoryList ?? []}
        totalData={dataBulkTransaksi?.totalPages}
        totalElement={dataBulkTransaksi?.totalElements}
        page={pages}
        setPage={setPage}
      />
    </div>
  );
};

export default BulkTransaksi;
