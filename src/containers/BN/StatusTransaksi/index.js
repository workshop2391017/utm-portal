import React from "react";
import { useSelector } from "react-redux";
import ResiTransaksi from "./ResiTransaksi";
import StatusTransaksi from "./StatusTransaksi";
import BulkResi from "./ResiTransaksi/BulkResi";
import BulkTransaksi from "./ResiTransaksi/BulkTransaksi";
import SweepResi from "./ResiTransaksi/SweepResi";
import TransferResi from "./ResiTransaksi/Transfer";

const TransactionStatus = () => {
  const { pages } = useSelector((state) => state.statusTransaksi);

  // 0 = StatusTransaksi
  // 1 = BulkTransaksi
  // 2 = ResiSweeping
  // 3 = ResiTransfer
  // 4 = ResiTranferIncoming
  // 5 = ResiTemplate

  return (
    <React.Fragment>
      {pages === 0 ? (
        <StatusTransaksi />
      ) : pages === 1 ? (
        <BulkTransaksi />
      ) : pages === 2 ? (
        <TransferResi />
      ) : pages === 3 ? (
        <SweepResi />
      ) : (
        <ResiTransaksi />
      )}
    </React.Fragment>
  );
};

export default TransactionStatus;
