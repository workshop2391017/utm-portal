import { Card, makeStyles } from "@material-ui/core";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import FormField from "components/BN/Form/InputGroupValidation";
import DeleteConfirmation from "components/BN/Popups/Delete";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import AntdTextField from "components/BN/TextField/AntdTextField";
import Title from "components/BN/Title";
import Colors from "helpers/colors";
import React from "react";
import { useHistory } from "react-router-dom";
import profilchecklist from "assets/images/BN/profilchecklist.png";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

const useStyle = makeStyles({
  container: {
    padding: "26px 20px",
  },
  content: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: 450,
  },
  containerForm: {
    width: 700,
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    height: 310,
    borderRadius: 20,
    padding: 30,
    "& .title": {
      fontFamily: "FuturaMdBT",
      fontSize: 16,
      color: Colors.dark.hard,
    },
    "& .subtitle": {
      fontFamily: "FuturaBkBT",
      fontSize: 13,
      color: Colors.dark.hard,
      marginTop: 10,
    },
    "& .formRow": {
      width: "100%",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
  },
  buttonActionGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    height: 83,
    backgroundColor: "white",
    padding: "0 20px",
  },
});

const AddUser = () => {
  const history = useHistory();
  const classes = useStyle();

  return (
    <div style={{ positon: "relative" }}>
      <Title label="Tambah Pengguna" />
      <div className={classes.container}>
        <div className={classes.content}>
          <Card className={classes.containerForm}>
            <div className="title">Form Tambah Pengguna</div>
            <div className="subtitle">
              Silahkan isi data dibawah ini untuk melakukan tambah pengguna
            </div>
            <div className="formRow" style={{ marginTop: 45 }}>
              <div>
                <FormField label="Nama :" />
                <AntdTextField style={{ width: 310 }} />
              </div>
              <div>
                <FormField label="Email :" />
                <AntdTextField style={{ width: 310 }} />
              </div>
            </div>
            <div className="formRow" style={{ marginTop: 10 }}>
              <div>
                <FormField label="No Telp :" />
                <AntdTextField style={{ width: 310 }} />
              </div>
              <div>
                <FormField label="Peran :" />
                <SelectWithSearch
                  options={[{ label: "Pilih Peran", value: "Pilih Peran" }]}
                  placeholder="Pilih Peran"
                  style={{ width: 310 }}
                />
              </div>
            </div>
          </Card>
        </div>
      </div>

      <div className={classes.buttonActionGroup}>
        <ButtonOutlined label="Batal" onClick={() => history.goBack()} />
        <GeneralButton label="Simpan" />
      </div>

      <SuccessConfirmation
        isOpen={0}
        img={profilchecklist}
        title=" "
        message="Pengguna Ditambah"
        submessage="Anda telah berhasil melakukan tambah pengguna,
Sedang menunggu persetujuan"
      />
    </div>
  );
};

export default AddUser;
