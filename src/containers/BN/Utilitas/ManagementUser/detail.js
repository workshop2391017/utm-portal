import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import moment from "moment";

import { makeStyles, Button, Typography } from "@material-ui/core";

import Colors from "helpers/colors";
import { capitalizeFirstLetterFromUppercase } from "utils/helpers";

import { ReactComponent as IconUser } from "assets/icons/BN/userblue.svg";
import { ReactComponent as IconEnd } from "assets/icons/BN/log-outblue.svg";
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Badge from "components/BN/Badge";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import ConfirmationPopup from "components/BN/Popups/Delete";

import { pathnameCONFIG } from "configuration";

import {
  handleBlockedUser,
  handleEndSession,
  handleUnBlockedUser,
  setSuccess,
} from "stores/actions/bankUserSetting";
import { validateTask } from "stores/actions/validateTaskPortal";

const useStyle = makeStyles((theme) => ({
  page: {
    position: "relative",
    height: "100%",
    backgroundColor: Colors.gray.ultrasoft,
  },
  backButton: {
    ...theme.typography.backButton,
    marginTop: 8,
    marginLeft: 20,
  },
  card: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "510px",
    minHeight: "369px",
    padding: "20px",
    borderRadius: "10px",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    backgroundColor: "white",

    "& > div:first-child": {
      display: "flex",
      alignItems: "center",
      gap: "8px",
      marginBottom: "20px",
    },
  },
  bankUserInformation: {
    ...theme.typography.subtitle2,
    fontFamily: "FuturaHvBT",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 16,
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "8px",
  },
  footer: {
    backgroundColor: "white",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    padding: "22px 40px",
    position: "absolute",
    bottom: 0,
  },
}));

const freezeDate = moment().format("YYYY-MM-DD HH:mm:ss");

const UserBlock = () => {
  const classes = useStyle();
  const history = useHistory();
  const dispatch = useDispatch();

  const { isSuccess, isLoading } = useSelector(
    ({ bankUserSetting }) => bankUserSetting
  );

  const detailUser = history.location.state;

  const [openConfirmation, setOpenConfirmation] = useState({
    action: null,
    isOpen: false,
  });

  const clickKembali = () => {
    history.goBack();
    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  const handleBlock = () => {
    dispatch(
      validateTask(
        {
          menuName: "187",
          id: detailUser?.idUser,
        },
        {
          async onContinue() {
            dispatch(
              handleBlockedUser(
                {
                  id: detailUser?.idUser,
                  modifiedReason: "",
                  status: 0,
                  username: detailUser?.username,
                  passwordTried: 0,
                  freezeDate,
                },
                setOpenConfirmation
              )
            );
          },
          onError() {
            setOpenConfirmation((prev) => ({
              action: prev.action,
              isOpen: false,
            }));
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const handleUnBlock = () => {
    dispatch(
      validateTask(
        {
          menuName: "187",
          id: detailUser?.idUser,
        },
        {
          async onContinue() {
            dispatch(
              handleUnBlockedUser(
                {
                  id: detailUser?.idUser,
                  modifiedReason: "",
                  status: 1,
                  username: detailUser?.username,
                  passwordTried: 0,
                  freezeDate: null,
                },
                setOpenConfirmation
              )
            );
          },
          onError() {
            setOpenConfirmation((prev) => ({
              action: prev.action,
              isOpen: false,
            }));
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const handleUserEndSession = () =>
    dispatch(
      handleEndSession({
        id: detailUser?.idUser,
      })
    );

  return (
    <div className={classes.page}>
      <Button
        startIcon={<ArrowLeft />}
        onClick={clickKembali}
        className={classes.backButton}
      >
        Back
      </Button>
      <Typography variant="h2" style={{ marginLeft: 20 }}>
        Bank User Details
      </Typography>

      <div className={classes.card}>
        <div>
          <IconUser />
          <span className={classes.bankUserInformation}>
            Bank User Information
          </span>
        </div>

        <div className={classes.container}>
          <Typography variant="subtitle2">Name</Typography>
          <span className={classes.value}>{detailUser?.name}</span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">Username</Typography>
          <span className={classes.value}>{detailUser?.username}</span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">NIP</Typography>
          <span className={classes.value}>{detailUser?.nip}</span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">E-mail</Typography>
          <span className={classes.value}>{detailUser?.email}</span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">Role</Typography>
          <span className={classes.value}>
            {capitalizeFirstLetterFromUppercase(detailUser?.role, "_").replace(
              "Admin ",
              ""
            )}
          </span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">Group</Typography>
          <span className={classes.value}>
            {detailUser?.portalGroup || "-"}
          </span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">Branch</Typography>
          <span className={classes.value}>
            {capitalizeFirstLetterFromUppercase(detailUser?.branch)}
          </span>
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">Login Status</Typography>
          <Badge
            outline
            label={detailUser?.loginStatus ? "Active" : "Inactive"}
            type={detailUser?.loginStatus ? "green" : "red"}
          />
        </div>
        <div className={classes.container}>
          <Typography variant="subtitle2">Status User</Typography>
          <Badge
            outline
            label={detailUser?.status === 0 ? "Inactive" : "Active"}
            type={detailUser?.status === 0 ? "red" : "green"}
          />
        </div>
      </div>

      <div className={classes.footer}>
        <GeneralButton
          label={detailUser?.status === 0 ? "Unblock" : "Block"}
          onClick={
            detailUser?.status === 0
              ? () => {
                  setOpenConfirmation({ action: "unblock", isOpen: true });
                }
              : () => {
                  setOpenConfirmation({ action: "block", isOpen: true });
                }
          }
        />
        <ButtonOutlined
          width={158}
          label="End Session"
          iconPosition="startIcon"
          buttonIcon={<IconEnd />}
          onClick={() => {
            setOpenConfirmation({ action: "end session", isOpen: true });
          }}
          disabled={!detailUser?.loginStatus}
        />
      </div>
      {console.log("ini openConfirmation", openConfirmation)}
      <ConfirmationPopup
        loading={isLoading}
        isOpen={openConfirmation.isOpen}
        handleClose={() => {
          setOpenConfirmation({ action: null, isOpen: false });
        }}
        onContinue={
          openConfirmation.action === "block"
            ? handleBlock
            : openConfirmation.action === "unblock"
            ? handleUnBlock
            : handleUserEndSession
        }
        message={
          openConfirmation.action === "block"
            ? "Are You Sure to Block User"
            : openConfirmation.action === "unblock"
            ? "Are You Sure to Unblock User"
            : "Are You Sure to End Session?"
        }
        submessage="You cannot undo this action"
      />
      <SuccessConfirmation
        isOpen={isSuccess}
        handleClose={() => {
          history.push(pathnameCONFIG.UTILITAS.MANAGEMENT_USER);
          dispatch(setSuccess(false));
          setOpenConfirmation({ action: null, isOpen: false });
        }}
        message={
          openConfirmation.action?.includes("block")
            ? "Saved Successfully"
            : "Session Successfully Ended"
        }
        submessage=""
      />
    </div>
  );
};

export default UserBlock;
