import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { makeStyles, Typography } from "@material-ui/core";

import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";

import Colors from "helpers/colors";
import useDebounce from "utils/helpers/useDebounce";
import {
  capitalizeFirstLetterFromUppercase,
  handleConvertRole,
} from "utils/helpers";

import { pathnameCONFIG } from "configuration";

import { handleUserData, setUserId } from "stores/actions/bankUserSetting";

const useStyles = makeStyles((theme) => ({
  page: {
    background: Colors.gray.ultrasoft,
    padding: "20px 30px",
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "46px",
  },
  name: {
    ...theme.typography.subtitle1,
    fontFamily: "FuturaMdBT",
  },
}));

const dataHeader = ({ classes, history, dispatch }) => [
  {
    title: "Name & NIP",
    render: (rowData) => (
      <Fragment>
        <h6 className={classes.name}>{rowData.name}</h6>
        <Typography variant="subtitle1">{rowData.nip}</Typography>
      </Fragment>
    ),
  },
  {
    title: "Role",
    render: (rowData) => {
      if (rowData.role === "ADMIN_MAKER_APPROVER") {
        return (
          <div style={{ display: "flex", gap: "5px" }}>
            <Badge outline label="Maker" type="blue" />
            <Badge outline label="Approver" type="green" />
          </div>
        );
      }
      if (rowData.role === "ADMIN_MAKER") {
        return <Badge outline label="Maker" type="blue" />;
      }
      return <Badge outline label="Approver" type="green" />;
    },
  },
  {
    title: "Group",
    render: (rowData) => (
      <Typography variant="subtitle1">{rowData.portalGroup ?? "-"}</Typography>
    ),
  },
  {
    title: "Status",
    render: (rowData) =>
      rowData.status === 0 ? (
        <Badge outline label="Inactive" type="red">
          Inactive
        </Badge>
      ) : (
        <Badge outline label="Active" type="green">
          Active
        </Badge>
      ),
  },
  {
    width: 100,
    title: "",
    render: (rowData) => (
      <GeneralButton
        label="View Details"
        onClick={() => {
          dispatch(setUserId(rowData?.username));
          history.push(pathnameCONFIG.UTILITAS.MANAGEMENT_USER_DETAIL, rowData);
        }}
        style={{
          padding: 0,
          fontSize: 9,
          width: 76,
          height: 23,
        }}
      />
    ),
  },
];

const UtilitasManagementUser = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { data, isLoading } = useSelector((e) => e.bankUserSetting);

  const [page, setPage] = useState(1);
  const [openModal, setOpenModal] = useState(false);
  const [dataFilter, setDataFilter] = useState(null);
  const [dataSearch, setDataSearch] = useState("");

  const searchDebounced = useDebounce(dataSearch, 1000);

  useEffect(() => {
    const payload = {
      isActive: dataFilter !== null ? dataFilter?.tabs?.tabValue3 === 0 : true, // default active
      limit: 10,
      page: page - 1,
      role:
        dataFilter?.dropdown?.dropdown1 === "All Role"
          ? null
          : dataFilter?.dropdown?.dropdown1 === "Maker"
          ? "ADMIN_MAKER"
          : dataFilter?.dropdown?.dropdown1 === "Approver"
          ? "ADMIN_APPROVER"
          : dataFilter?.dropdown?.dropdown1 === "Maker, Approver"
          ? "ADMIN_MAKER_APPROVER"
          : null, // "ADMIN_APPROVER","ADMIN_MAKER","ADMIN_MAKER_APPROVER" or default null
      searchValue: searchDebounced,
    };

    dispatch(handleUserData(payload));
  }, [page, searchDebounced, dataFilter]);

  useEffect(() => {
    setPage(1);
  }, [dataFilter, searchDebounced]);

  const dropDownOpt = ["All role", "Approver", "Maker", "Maker, Approver"];

  const options = [
    {
      id: 1,
      type: "dropdown",
      placeholder: "Choose Role",
      options: dropDownOpt,
    },
    {
      id: 3,
      type: "tabs",
      options: ["Active", "Inactive"],
    },
  ];

  return (
    <div className={classes.page}>
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <div className={classes.header}>
        <Typography variant="h2">Bank User Settings</Typography>
        <Search
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Name, Nip"
          placement="bottom"
          style={{ width: 286 }}
        />
      </div>

      <Filter
        title="Show :"
        options={options}
        dataFilter={dataFilter}
        setDataFilter={(e) => {
          setDataFilter(e);
        }}
        style={{ marginBottom: 20 }}
      />

      <TableICBB
        headerContent={dataHeader({ classes, history, dispatch })}
        dataContent={data?.listPortalUser}
        page={page}
        setPage={setPage}
        isLoading={isLoading}
        totalData={data?.totalPage}
        totalElement={data?.totalElement}
      />
    </div>
  );
};

export default UtilitasManagementUser;
