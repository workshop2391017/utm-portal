import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import Search from "components/BN/Search/SearchWithoutDropdown";
import GeneralButton from "components/BN/Button/GeneralButton";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import TableICBB from "../../../../components/BN/TableIcBB";
import { dataTableContentAccess } from "../../../../services/mockup_data/BN/dataAccessContent";

const Title = styled.div`
  color: #2b2f3c;
  font-size: 20px;
  font-weight: 900;
  font-family: FuturaHvBT;
`;

const useStyles = makeStyles({
  page: {
    backgroundColor: "#F4F7FB",
    height: "100%",
    padding: "20px",
  },
  cardTitle: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "46px",
  },
});

const ContentAccess = (props) => {
  const classes = useStyles();

  const history = useHistory();
  const dataHeader = [
    {
      title: "Company ID",
      key: "company_id",
    },
    {
      title: "Company Name",

      key: "company_name",
    },
    {
      title: "",
      render: (rowData) => (
        <div
          style={{
            display: "flex",
            width: "100%",
            justifyContent: "flex-end",
          }}
        >
          <GeneralButton
            label="Lihat Detail"
            style={{
              width: "76px",
              height: "23px",
              fontSize: "9px",
              fontFamily: "FuturaMdBT",
              fontWeight: 700,
              padding: "6px 10px",
            }}
            onClick={() => {
              history.push(pathnameCONFIG.UTILITAS.CONTENT_ACCESS_DETAIL);
            }}
          />
        </div>
      ),
    },
  ];

  const [searchValue, setSearchValue] = useState("");
  return (
    <div className={classes.page}>
      <div className={classes.cardTitle}>
        <Title>Content Access</Title>

        <Search
          style={{
            width: 265,
          }}
          placeholder="pencarian..."
          dataSearch={searchValue}
          setDataSearch={setSearchValue}
        />
      </div>

      <TableICBB
        headerContent={dataHeader}
        dataContent={dataTableContentAccess}
        page={1}
        totalElement={120}
        totalData={10}
      />
    </div>
  );
};

ContentAccess.propTypes = {};

export default ContentAccess;
