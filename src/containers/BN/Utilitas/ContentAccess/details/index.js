import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import Search from "components/BN/Search/SearchWithoutDropdown";
import GeneralButton from "components/BN/Button/GeneralButton";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import Pena from "../../../../../assets/icons/BN/pena.svg";

const data = [
  {
    title: "CM02",
    checked: true,
  },
  {
    title: "Financial Transaction List",
    checked: false,
  },
  {
    title: "Forex",
    checked: false,
  },
  {
    title: "tya",
    checked: false,
  },
  {
    title: "Forex Rate Inquiry",
    checked: true,
  },
  {
    title: "Forex",
  },
  {
    title: "try",
    checked: true,
  },
  {
    title: "Promotion",
    checked: false,
  },
  {
    title: "Forex",
    checked: true,
  },
];

const Title = styled.div`
  color: #2b2f3c;
  font-size: ${(props) => (props.content ? "17px" : "20px")};
  font-family: FuturaHvBT;
  font-weight: 900;
`;

const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: FuturaBkBT;
  font-weight: 400;
  margin-top: 15px;
`;

const SubContent = styled.div`
  color: #44495b;
  font-size: 15px;
  font-family: FuturaBkBT;
  font-weight: 400;
`;

const useStyles = makeStyles({
  page: {
    backgroundColor: "#F4F7FB",
    padding: "20px",
    height: "100%",
    width: "100%",
  },
  cardHeader: {
    display: "flex",
    justifyContent: "space-between",
  },
  card: {
    padding: "24px 20px",
    width: "100%",
    height: "113px",
    marginTop: "36px",
    borderRadius: "10px",
    backgroundColor: "#fff",
  },
  cardContent: {
    padding: "20px",
    width: "100%",
    borderRadius: "10px",
    backgroundColor: "#fff",
    marginTop: "20px",
    height: "214px",
  },
  circle: {
    width: 7,
    height: 7,
    borderRadius: 7 / 2,
    backgroundColor: "#0061A7",
    marginRight: "10px",
  },
  cardDataContent: {
    display: "flex",
    flexWrap: "wrap",
    marginLeft: "20px",
    marginTop: "20px",
  },
  cardIsiContent: {
    width: "313px",
    height: "26px",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    marginBottom: "14px",
  },
});

const ContentAccessDetail = (props) => {
  const classes = useStyles();
  const [searchValue, setSearchValue] = useState("");
  const history = useHistory();
  return (
    <div className={classes.page}>
      <div className={classes.cardHeader}>
        <Title>Detail Content Access</Title>
        <div
          style={{
            display: "flex",
          }}
        >
          <Search
            style={{
              width: 286,
            }}
            placeholder="Pencarian Nama Produk"
            dataSearch={searchValue}
            setDataSearch={setSearchValue}
          />

          <GeneralButton
            label="Ubah Content Access"
            iconPosition="start"
            buttonIcon={
              <img src={Pena} alt="pena" height="17px" width="17px" />
            }
            style={{
              color: "#fff",
              fontSize: "15px",
              fontFamily: "FuturaMdBT",
              fontWeight: 700,
              width: 224,
              height: 44,
              marginLeft: "20px",
            }}
            onClick={() => {
              console.warn("testing saja");
              history.push(pathnameCONFIG.UTILITAS.CONTENT_ACCESS_EDIT);
            }}
          />
        </div>
      </div>

      <div className={classes.card}>
        <Title>PT Telkom Indonesia</Title>
        <Label>123321</Label>
      </div>
      <div className={classes.cardContent}>
        <Title content>Content</Title>

        <div className={classes.cardDataContent}>
          {data?.map((item) => (
            <div className={classes.cardIsiContent}>
              <div className={classes.circle} />
              <SubContent>{item.title}</SubContent>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

ContentAccessDetail.propTypes = {};

export default ContentAccessDetail;
