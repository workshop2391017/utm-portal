import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import Search from "components/BN/Search/SearchWithoutDropdown";
import GeneralButton from "components/BN/Button/GeneralButton";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import { useHistory } from "react-router-dom";
import DeletePopup from "components/BN/Popups/Delete";
import SetujuData from "components/BN/Popups/SetujuData";
import Pena from "../../../../../assets/icons/BN/pena.svg";

const data = [
  {
    title: "CM02",
    checked: true,
  },
  {
    title: "Financial Transaction List",
    checked: false,
  },
  {
    title: "Forex",
    checked: false,
  },
  {
    title: "tya",
    checked: false,
  },
  {
    title: "Forex Rate Inquiry",
    checked: true,
  },
  {
    title: "Forex",
  },
  {
    title: "try",
    checked: true,
  },
  {
    title: "Promotion",
    checked: false,
  },
  {
    title: "Forex",
    checked: true,
  },
];

const Title = styled.div`
  color: #2b2f3c;
  font-size: ${(props) => (props.content ? "17px" : "20px")};
  font-family: FuturaHvBT;
  font-weight: 900;
`;

const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: FuturaBkBT;
  font-weight: 400;
  margin-top: 15px;
`;

const SubContent = styled.div`
  color: #44495b;
  font-size: 15px;
  font-family: FuturaBkBT;
  font-weight: 400;
`;

const useStyles = makeStyles({
  page: {
    backgroundColor: "#F4F7FB",
    padding: "20px",
    height: "100%",
    width: "100%",
    position: "relative",
  },
  cardHeader: {
    display: "flex",
    justifyContent: "space-between",
  },
  card: {
    padding: "24px 20px",
    width: "100%",
    height: "113px",
    marginTop: "36px",
    borderRadius: "10px",
    backgroundColor: "#fff",
  },
  cardContent: {
    padding: "20px",
    width: "100%",
    borderRadius: "10px",
    backgroundColor: "#fff",
    marginTop: "20px",
    height: "252px",
  },

  cardDataContent: {
    display: "flex",
    flexWrap: "wrap",
    marginLeft: "20px",
    marginTop: "20px",
  },
  cardIsiContent: {
    width: "313px",
    height: "26px",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    marginBottom: "14px",
  },
});

const ContentAccessEdit = (props) => {
  const classes = useStyles();
  const [searchValue, setSearchValue] = useState("");
  const [openSuccess, setOpenSuccess] = useState(false);
  const [modalYakin, setModalYakin] = useState(false);
  const history = useHistory();
  return (
    <div className={classes.page}>
      <SetujuData
        isOpen={modalYakin}
        handleClose={() => setModalYakin(false)}
        onContinue={() => {
          setOpenSuccess(true);
        }}
        title="Konfirmasi"
        message="Anda Yakin Menyetujui Data?"
      />

      <SuccessConfirmation
        isOpen={openSuccess}
        title="Perubahan Berhasil"
        message="Perubahan Data Berhasil Disimpan"
        submessage="Silakan menunggu persetujuan"
        handleClose={() => setOpenSuccess(false)}
      />

      <div className={classes.cardHeader}>
        <Title>Detail Content Access</Title>
        <div
          style={{
            display: "flex",
          }}
        >
          <Search
            style={{
              width: 286,
            }}
            placeholder="Pencarian Nama Produk"
            dataSearch={searchValue}
            setDataSearch={setSearchValue}
          />
        </div>
      </div>

      <div className={classes.card}>
        <Title>PT Telkom Indonesia</Title>
        <Label>123321</Label>
      </div>
      <div className={classes.cardContent}>
        <Title content>Content</Title>
        <CheckboxSingle
          style={{
            fontSize: "15px",
            color: "#44495B",
            fontFamily: "FuturaHvBT",
            fontWeight: 900,
            marginTop: 24,
            // marginBottom: 24,
          }}
          label="Izinkan Semua Content"
          name="btn"
          onChange={(e) => console.warn("e:", e)}
        />

        <div className={classes.cardDataContent}>
          {data?.map((item) => (
            <div className={classes.cardIsiContent}>
              <CheckboxSingle
                label={item.title}
                name="btn"
                onChange={(e) => console.warn("e:", e)}
                checked={item.checked}
              />
            </div>
          ))}
        </div>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: 70,
          position: "absolute",
          bottom: "20px",
          left: 0,
          right: 0,
          backgroundColor: "#fff",
          padding: "20px",
        }}
      >
        <div>
          <ButtonOutlined
            label="Batal"
            width="86px"
            color="#0061A7"
            onClick={() => history.goBack()}
          />
        </div>

        <div>
          <GeneralButton
            onClick={() => setModalYakin(true)}
            label="Simpan"
            width="100px"
            height="43px"
          />
        </div>
      </div>
    </div>
  );
};

ContentAccessEdit.propTypes = {};

export default ContentAccessEdit;
