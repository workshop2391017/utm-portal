import { CircularProgress, makeStyles } from "@material-ui/core";
import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import Title from "components/BN/Title";
import GeneralButton from "components/BN/Button/GeneralButton";
import moment from "moment";
import { ReactComponent as PlusSvg } from "assets/icons/BN/plus-white.svg";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeletePopup from "components/BN/Popups/Delete";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Colors from "helpers/colors";
import "moment/locale/en-gb";
import GeneralDatePicker from "components/BN/DatePicker/GeneralDatePicker";
import { ReactComponent as ChevronLeft } from "assets/icons/BN/chevron-left-blue.svg";
import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right-blue-alt.svg";
import { useDispatch, useSelector } from "react-redux";
import {
  getEventList,
  setEvent,
  setEventByDate,
} from "stores/actions/calendarEvent";
import Tabs from "components/BN/Tabs/GeneralTabs";
import Year from "./year2";
import "./style.scss";

import NewEvent from "./AddNew";
import PopupDetailEvent from "./popupDetail";

const icons = {
  1: "",
  2: "",
  3: "",
  4: "",
  5: "",
};

const useStyles = (view) =>
  makeStyles({
    container: {},
    content: {
      // display: "flex",
      // justifyContent: "center",
      // marginTop: 16,
      margin: 20,
    },
    listevent: {
      width: 430,
      height: 513,
      backgroundColor: "white",
      borderRadius: 20,
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      overflow: "hidden",
      position: "relative",
      marginLeft: 30,
    },
    whiteBottom: {
      height: 80,
      width: "100%",
      backgroundColor: "white",
      opacity: 0.4,
      backgroundImage:
        "linear-gradient(to top, rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%)",
      position: "absolute",
      bottom: 0,
    },
    title: {
      color: "#374062",
      fontSize: 22,
      fontFamily: "FuturaHvBT",
      paddingLeft: 20,
      paddingTop: 30,
    },
    titleHeader: {
      display: "flex",
      alignItems: "center",
    },
    calendar: {
      "& .rbc-calendar": {
        ...(view !== "year" && { backgroundColor: "white", padding: "20px" }),
        position: "relative",
        marginTop: "120px",
        "& *": {
          borderWidth: "0.5px !important",
        },
      },
      "& .rbc-toolbar": {
        display: "flex",
        justifyContent: "flex-start",

        height: 72,
        backgroundColor: "white",
        borderRadius: 10,
        padding: "20px",
        fontSize: 15,
        overflow: "hidden",
        alignItems: "center",
        position: "absolute",
        width: "100%",
        top: "-100px",
        marginTop: "0",
        left: "0",
        "& .date-container": {
          display: "flex",
          alignItems: "center",
        },
        "& .rbc-toolbar-label": {
          color: Colors.primary.hard,
          fontSize: 15,
          position: "relative",
          cursor: "pointer",
          border: "none",
          backgroundColor: "white",
          fontWeight: 400,
        },
        "& .rbc-btn-group": {
          border: "1px solid #0061A7",
          borderRadius: "10px",
          overflow: "hidden",
          display: "flex",
          alignItems: "center",
          height: 40,
        },
        "& .button-prev, & .button-next": {
          border: "none",
          backgroundColor: "white",
        },
        "& .button-prev": {},
        "& .ant-picker": {
          border: "none",
          width: "141px",
          textAlign: "center",
        },
        "& .ant-picker-suffix": {
          display: "none",
        },
      },
      "& .rbc-row": {
        "& .rbc-date-cell": {
          display: "flex",
          justifyContent: "center",
          "& .rbc-button-link": {
            color: `${Colors.dark.hard}`,
            fontFamily: "FuturaBkBT",
            fontSize: 15,
            marginTop: 5.5,
          },
        },
      },

      "& .rbc-row-bg": {
        backgroundColor: "white",
      },
      "& .rbc-off-range-bg": {
        backgroundColor: "white",
      },
      "& .rbc-off-range": {
        "& .rbc-button-link": {
          color: `${Colors.dark.soft} !important`,
        },
      },
      "& .rbc-time-view": {
        border: "none",
        backgroundColor: "white",
        "& .rbc-header": {
          borderBottom: "none",
          marginTop: 5.5,
        },
        "& *": {
          borderColor: "#BCC8E7",
        },
        "& .rbc-time-slot": {
          width: "70px",
          "& .rbc-label": {
            display: "flex",
            height: "88px",
            justifyContent: "center",
            alignItems: "center",
            padding: 0,
            margin: 0,
            fontFamily: "FuturaBQ",
            fontSize: 13,
            color: Colors.dark.medium,
          },
        },
        "& .rbc-time-content > * + * > *": {
          borderColor: "#BCC8E7",
        },
        "& .rbc-day-slot .rbc-time-slot": {
          border: "none",
        },
        "& .rbc-timeslot-group": {
          borderColor: "#BCC8E7",
          minHeight: "90px !important",
        },

        "& .rbc-today": {
          backgroundColor: "white",
        },
      },
      "& .rbc-month-view": {
        borderTop: "none",
        borderColor: "#BCC8E7",
        marginBottom: 20,
        backgroundColor: "white",

        "& .rbc-header": {
          borderBottom: "none !important",
          backgroundColor: "white",
          fontFamily: "FuturaHvBT",
          color: Colors.dark.medium,
          fontSize: 12,
          marginTop: 5.5,
          "& .rbc-button-link ": {
            "& span": {
              wordSpacing: "100px",
              height: "200px",
              wordBreak: "break-all",
            },
          },
        },
        "& .rbc-event::before": {
          content: " ",
          display: "block",
          width: "10px",
          height: "10px",
          backgroundColor: "red",
        },
        "& .rbc-event": {
          color: Colors.dark.hard,
          backgroundColor: "transparent",
          "& .rbc-event::before": {
            content: " ",
            display: "block",
            width: "10px",
            height: "10px",
            backgroundColor: "red",
          },
        },
        "& .rbc-header + .rbc-header": {
          borderColor: "#BCC8E7",
        },
        "& .rbc-month-row + .rbc-month-row": {
          borderColor: "#BCC8E7",
        },
        "& .rbc-day-bg + .rbc-day-bg": {
          borderColor: "#BCC8E7",
        },
        "& .rbc-month-row": {
          "&:nth-child(1)": {
            borderTop: "none",
          },
        },
        "& .rbc-now": {
          "& .rbc-button-link": {
            width: 29,
            height: 29,
            backgroundColor: Colors.primary.hard,
            borderRadius: 50,
            color: "white !important",
            verticalAlign: "middle",
            textAlign: "center",
            margin: "auto",
            padding: 0,
            marginTop: 5,
            fontFamily: "FuturaBkBT",
            fontSize: 15,
          },
        },
      },
    },
    calendarView: {
      "& .title-date": {
        color: Colors.dark.hard,
        fontSize: 17,
        fontFamily: "FuturaHvBT",
        marginBottom: "20px",
      },
    },
  });

export const navigateContants = {
  PREVIOUS: "PREV",
  NEXT: "NEXT",
  TODAY: "TODAY",
  DATE: "DATE",
};

export const views = {
  MONTH: "month",
  // WEEK: "week",
  // WORK_WEEK: "work_week",
  // DAY: "day",
  AGENDA: "agenda",
};

const CalendarEvent = () => {
  const [openModal, setOpenModal] = useState(false);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [view, setView] = useState("month");
  const [selectedDate, setSelectedDate] = useState(
    moment().format("YYYY-MM-DD")
  );

  const [openEvent, setOpenEvent] = useState({ isOpen: false, data: {} });
  const refCalendar = useRef(null);

  const { data: event, isLoading } = useSelector((e) => e.calendarEvent);

  const classes = useStyles(view)();
  const dispatch = useDispatch();

  useEffect(() => {
    let payload = {};
    if (view === "month") {
      payload = {
        calendarEventViewMode: "MONTH",
        month: Number(moment(selectedDate).format("M")),
        year: Number(moment(selectedDate).format("YYYY")),
      };
    } else {
      payload = {
        calendarEventViewMode: "YEAR",
        month: null,
        year: Number(moment(selectedDate).format("YYYY")),
      };
    }
    dispatch(getEventList(payload));
  }, [view, selectedDate]);

  const formats = {
    dayFormat: (date, culture, localizer) =>
      `${localizer.format(date, "dddd", culture)}`,
    timeGutterFormat: (date, culture, localizer) =>
      localizer.format(date, "H A", culture),
  };

  const localizer = momentLocalizer(moment);

  const CustomToolbar = ({ onNavigate, label, messages, ...rest }) => {
    const label2 =
      view === "year"
        ? moment(rest?.date).format("YYYY")
        : moment(rest?.date).format("MMMM YYYY");

    const [openDatePicker, setOpenDatePicker] = useState(false);

    return (
      <div>
        <div className="rbc-toolbar">
          <div className="date-container">
            <div style={{ fontFamily: "FuturaHvBT" }}>Show : &nbsp;</div>
            <div>
              <Tabs
                feature="calendar"
                value={view === "month" ? 0 : 1}
                onChange={(e, value) => setView(value === 0 ? "month" : "year")}
                tabs={["Monthly", "Yearly"]}
                style={{ marginRight: 10 }}
              />
            </div>
            <span className="rbc-btn-group">
              <button
                className="button-prev"
                type="button"
                style={{ cursor: "pointer" }}
                onClick={() => {
                  onNavigate(navigateContants.PREVIOUS);

                  setSelectedDate(
                    moment(rest?.date).subtract(1, view).format("YYYY-MM-DD")
                  );
                }}
              >
                <ChevronLeft style={{ height: 10 }} />
              </button>
              <button
                className="rbc-toolbar-label"
                onClick={() => setOpenDatePicker(true)}
                disabled={view === "year"}
              >
                {label2 || ""}
                <GeneralDatePicker
                  style={{
                    visibility: "hidden",
                    height: 0,
                    padding: 0,
                    width: 0,
                    position: "absolute",
                  }}
                  value={moment(selectedDate, "YYYY-MM-DD")}
                  open={openDatePicker}
                  onOpenChange={setOpenDatePicker}
                  onChange={(val) =>
                    setSelectedDate(moment(val).format("YYYY-MM-DD"))
                  }
                />
              </button>
              <button
                type="button"
                className="button-next"
                style={{ cursor: "pointer" }}
                onClick={() => {
                  onNavigate(navigateContants.NEXT);
                  setSelectedDate(
                    moment(rest?.date).add(1, view).format("YYYY-MM-DD")
                  );
                }}
              >
                <ChevronRight style={{ height: 10 }} />
              </button>
            </span>
          </div>

          {/* <img
            src={''}
            alt="pattern"
            style={{ position: "absolute", right: 0, top: 0 }}
          /> */}
        </div>
        {view !== "year" ? (
          <div className="title-date">{label2 || ""}</div>
        ) : null}
      </div>
    );
  };

  const onSelectSlot = (ev, a, b) => {
    const filterEventSlot = event.filter(
      (e) =>
        moment(e.start).format("YYYY-MM-DD") ===
        moment(ev.start).format("YYYY-MM-DD")
    );
    dispatch(setEventByDate(filterEventSlot));
    if (filterEventSlot.length) {
      setOpenEvent({ isOpen: true, data: ev });
    }
  };

  return (
    <div>
      <PopupDetailEvent
        isOpen={openEvent.isOpen}
        handleClose={() => setOpenEvent({ isOpen: false, data: {} })}
        setOpenModal={setOpenModal}
      />
      <div className={classes.container}>
        <NewEvent
          isOpen={openModal}
          handleClose={() => setOpenModal(false)}
          onContinue={() =>
            setOpenSuccess({
              isOpen: true,
              message:
                "Anda telah berhasil menambahkan event, Sedang menunggu persetujuan",
            })
          }
        />
        <div>
          <div>
            <div className={classes.titleHeader}>
              <Title label="Calendar Event" />
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  paddingRight: 30,
                }}
              >
                <GeneralButton
                  onClick={() => {
                    dispatch(setEvent({}));
                    setOpenModal(true);
                  }}
                  label={
                    <div style={{ display: "flex" }}>
                      <PlusSvg /> Events
                    </div>
                  }
                />
              </div>
            </div>
          </div>
          <div className={classes.content}>
            <div style={{ position: "relative" }}>
              {isLoading && (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    zIndex: 999,
                    backgroundColor: "black",
                    alignItems: "center",
                    opacity: 0.03,
                  }}
                >
                  <CircularProgress size={50} color="#ffffff" />
                </div>
              )}
              <div className={classes.calendar}>
                <div className={classes.calendarView} ref={refCalendar}>
                  <Calendar
                    localizer={localizer}
                    // components={{}}
                    events={event}
                    date={selectedDate}
                    components={{ toolbar: CustomToolbar }}
                    defaultView="year"
                    view={view}
                    onSelectEvent={(e) => {
                      dispatch(setEvent({ ...e }));
                      setOpenModal(true);
                    }}
                    views={{
                      // day: true,
                      // week: true,
                      month: true,
                      year: Year,
                    }}
                    messages={{ year: "Year" }}
                    dayLayoutAlgorithm="no-overlap"
                    // onSelectEvent={onSelectEvent}
                    onSelectSlot={(ev) => onSelectSlot(ev)}
                    formats={formats}
                    selectable
                    popup
                    startAccessor="start"
                    endAccessor="end"
                    showMultiDayTimes
                  />
                </div>
              </div>
              {/* )} */}
            </div>
          </div>
        </div>

        {/* popup */}
        <SuccessConfirmation
          isOpen={openSuccess?.isOpen}
          title=""
          handleClose={() => setOpenSuccess({ isOpen: false, message: "" })}
          message="Event Ditambah"
          submessage={openSuccess?.message}
          height={425}
        />
        <DeletePopup
          isOpen={deleteConfirm}
          handleClose={() => setDeleteConfirm(false)}
          onContinue={() => {
            setDeleteConfirm(false);
            setOpenSuccess({
              isOpen: true,
              message:
                "Anda telah berhasil menghapus event,Sedang menunggu persetujuan",
            });
          }}
          title=""
          message="Hapus Event ?"
          submessage="Apakah Anda yakin ingin menghapus event ini ?"
          height={407}
        />
      </div>
    </div>
  );
};

export default CalendarEvent;
