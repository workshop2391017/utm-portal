// main
import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Select from "components/BN/Select/AntdSelect";
import TextField from "components/BN/TextField/AntdTextField";
import TextFieldDate from "components/BN/TextfieldDate";
import FormField from "components/BN/Form/InputGroupValidation";
import GeneralDatePicker from "components/BN/DatePicker/GeneralDatePicker";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import GeneralSwitch from "components/BN/Switch/GeneralSwitch";
import ButtonOutline from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import AntdTimePicker from "components/BN/DatePicker/TimePicker";
import {
  getValidationEvent,
  handleEditCalendarEvent,
  handleSaveCalendarEvent,
  setEvent,
  setEventAlreadyExist,
} from "stores/actions/calendarEvent";
import { useDispatch, useSelector } from "react-redux";
import DeletePopup from "components/BN/Popups/Delete";
import moment from "moment";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import useDebounce from "utils/helpers/useDebounce";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import usePrevious from "utils/helpers/usePrevious";
import RadioGroup from "components/BN/Radio/RadioGroupWorkFlow";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 452,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: 400,
    marginBottom: 18,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
  error: {
    fontSize: 13,
    color: "#FF6F6F",
    position: "absolute",
    left: 8,
    transition: "0.3s ease-in-out",
  },
  regularEvent: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 33,
  },
}));

const NewEvent = ({ isOpen, handleClose }) => {
  const classes = useStyles();
  const formikRef = useRef();

  const { isSubmitting, event, isAlreadyExist } = useSelector(
    (e) => e.calendarEvent
  );

  const [inputData, setInputData] = useState({
    id: null,
    eventDate: null,
    startTime: null,
    endTime: null,
    name: null,
    type: null,
    isRegularEvent: false,
    regularEventType: null,
  });
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [cekEventExist, setCekEventExist] = useState(null);
  const [cekEventDateExist, setCekEventDateExist] = useState(null);

  const eventValidation = useDebounce(cekEventExist, 1000);
  const prevSearch = usePrevious(eventValidation);

  const dispatch = useDispatch();
  const clearData = () => {
    setCekEventDateExist(null);
    formikRef.current.resetForm();
    dispatch(setEvent({}));
    dispatch(setEventAlreadyExist(false));
  };

  useEffect(() => {
    if (isOpen) {
      setCekEventDateExist(null);
      if (event.id) {
        formikRef.current?.setFieldValue("id", event.id ?? null);
        if (event?.isRegularEvent) {
          formikRef.current?.setFieldValue(
            "startTime",
            moment(event?.startTime) ?? null
          );
          formikRef.current?.setFieldValue(
            "endTime",
            moment(event?.endTime) ?? null
          );
          formikRef.current?.setFieldValue(
            "endTime",
            moment(event?.regularEventType) ?? null
          );
        } else {
          formikRef.current?.setFieldValue(
            "eventDate",
            moment(event?.eventDate) ?? null
          );
        }
        setCekEventDateExist(
          event?.eventDate
            ? moment(event?.eventDate).format("YYYY-MM-DD")
            : null
        );
        formikRef.current?.setFieldValue("name", event.title ?? null);
        setCekEventExist(event.title);
        formikRef.current?.setFieldValue("type", event.type ?? null);
        formikRef.current?.setFieldValue(
          "isRegularEvent",
          event.isRegularEvent ?? false
        );
      }
    }
  }, [event, isOpen]);

  const handleCloseModal = () => {
    handleClose();
  };

  const handleSave = () => {
    dispatch(
      validateTask(
        {
          ...(event?.id && {
            id: event?.id,
          }),
          name: event?.id ? event?.title : inputData.name,
          eventDate: event?.id
            ? moment(event?.eventDate || event?.startTime).format("YYYY-MM-DD")
            : moment(inputData.eventDate || inputData.startTime).format(
                "YYYY-MM-DD"
              ),
          menuName: validateTaskConstant.CALENDAR_EVENT,
        },
        {
          async onContinue() {
            dispatch(
              handleEditCalendarEvent(
                {
                  ...inputData,
                  eventDate: inputData.eventDate
                    ? moment(inputData.eventDate).format("YYYY-MM-DD")
                    : null,
                  startTime: inputData.startTime
                    ? moment(inputData.startTime).format("YYYY-MM-DD")
                    : null,
                  endTime: inputData.endTime
                    ? moment(inputData.endTime).format("YYYY-MM-DD")
                    : null,
                  isDelete: false,
                },
                () => {
                  setConfirmation(false);
                  setConfirmSuccess(true);
                  clearData();
                }
              )
            );
          },
          onError() {
            setConfirmation(false);
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const initValues = {
    id: null,
    eventDate: null,
    startTime: null,
    endTime: null,
    name: null,
    type: null,
    isRegularEvent: false,
    regularEventType: null,
  };

  const FormSchema = Yup.object().shape({
    // eventDate: Yup.string().nullable().required("Start Date is required"),
    // endTime: Yup.string().nullable().required("End Date is required"),
    name: Yup.string()
      .nullable()
      .required("Event Name is required")
      .min(2, "Event Name Minimum 2 Characters")
      .max(100, "Event Name Maximum 100"),
    type: Yup.string().nullable().required("Event Type is required"),
  });

  const getSchema = () => FormSchema;

  useEffect(() => {
    function validCheck() {
      let result = false;
      if (event?.id) {
        // EDIT
        if (
          cekEventDateExist === moment(event?.eventDate).format("YYYY-MM-DD")
        ) {
          result =
            eventValidation !== event.title && eventValidation !== prevSearch;
        } else {
          result = true;
        }
      } else {
        // ADD
        result = true;
      }
      return result;
    }

    if (eventValidation && cekEventDateExist) {
      if (validCheck()) {
        dispatch(
          getValidationEvent({
            ...(event?.id && {
              id: event?.id,
            }),
            name: eventValidation,
            eventDate: cekEventDateExist,
            table: "49",
          })
        );
      } else {
        dispatch(setEventAlreadyExist(false));
      }
    }
  }, [eventValidation, cekEventDateExist]);

  return (
    <div>
      <DeletePopup
        isOpen={confirmation}
        handleClose={() => {
          clearData();
          setConfirmation(false);
        }}
        onContinue={handleSave}
        loading={isSubmitting}
        message={
          event?.id
            ? "Are You Sure To Edit Your Data?"
            : "Are You Sure To Add Data?"
        }
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => {
          setConfirmSuccess(false);
          handleCloseModal();
          clearData();
        }}
      />
      <Formik
        initialValues={initValues}
        validationSchema={getSchema()}
        onSubmit={(values) => {
          // remove id att as payload if null
          if (!values?.id) {
            delete values.id;
          }
          console.log("+++ values", values);
          setInputData(values);
          setConfirmation(true);
          handleCloseModal();
        }}
        innerRef={formikRef}
      >
        {({
          errors,
          values,
          touched,
          handleChange,
          handleBlur,
          setFieldValue,
          isValid,
        }) => (
          <Modal
            className={classes.modal}
            open={isOpen}
            onClose={() => {
              handleCloseModal();
              clearData();
            }}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 900,
            }}
          >
            <Fade in={isOpen}>
              <div className={classes.paper}>
                <h1 className={classes.title}>
                  {event?.id ? "Edit Event" : "Add Event"}
                </h1>

                <Form>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <FormField
                        label="Event Name :"
                        error={
                          (errors?.name && touched?.name) || isAlreadyExist
                        }
                        errorMessage={
                          isAlreadyExist
                            ? "Event Name is already exist!"
                            : errors?.name
                        }
                      >
                        <TextField
                          style={{ width: "100%" }}
                          placeholder="Enter event name"
                          onChange={(e) => {
                            const val = e.target.value;
                            setFieldValue("name", val);
                            setCekEventExist(val);
                          }}
                          name="name"
                          value={values.name}
                          onBlur={handleBlur}
                        />
                      </FormField>
                    </Grid>
                    <Grid item>
                      <FormField
                        label="Event Type :"
                        error={errors?.type && touched?.type}
                        errorMessage={errors?.type}
                      >
                        <RadioGroup
                          value={values.type}
                          onChange={(e) =>
                            setFieldValue("type", e.target.value)
                          }
                          options={[
                            { label: "Event", value: "EVENT" },
                            { label: "Holiday", value: "HOLIDAY" },
                          ]}
                          direction="horizontal"
                          name="type"
                        />
                      </FormField>
                    </Grid>

                    <Grid item>
                      <div className={classes.regularEvent}>
                        <Grid container justifyContent="space-between">
                          <Grid item>Repeat ? </Grid>
                          <Grid item>
                            <GeneralSwitch
                              checked={values.isRegularEvent}
                              onChange={(e) => {
                                const bool = e.target.checked;
                                setFieldValue("isRegularEvent", bool);
                                setFieldValue("regularEventType", null);
                                setFieldValue("startTime", null);
                                setFieldValue("endTime", null);
                                setFieldValue("eventDate", null);
                                setCekEventDateExist(null);
                              }}
                            />
                          </Grid>
                        </Grid>
                      </div>
                      <FormField
                        label=""
                        error={touched?.regularEventType}
                        errorMessage={
                          values.isRegularEvent &&
                          !values.regularEventType &&
                          "Repeat Type is required"
                        }
                      >
                        <RadioGroup
                          value={values.regularEventType}
                          onChange={(e) =>
                            setFieldValue("regularEventType", e.target.value)
                          }
                          options={[
                            { label: "Monthly", value: "MONTHLY" },
                            { label: "Yearly", value: "YEARLY" },
                          ]}
                          direction="horizontal"
                          name="regularEventType"
                          disabled={!values.isRegularEvent}
                        />
                      </FormField>
                    </Grid>

                    <Grid item>
                      <Grid container spacing={2}>
                        {values.isRegularEvent ? (
                          <React.Fragment>
                            <Grid item xs={6}>
                              <FormField
                                label="Start Date :"
                                error={touched?.startTime && !values.startTime}
                                // errorMessage={errors?.startTime}
                                errorMessage={
                                  values.isRegularEvent &&
                                  !values.startTime &&
                                  "Start Date is required"
                                }
                              >
                                <GeneralDatePicker
                                  placeholder="12/12/2022"
                                  format="DD/MM/YYYY"
                                  style={{ width: "100%" }}
                                  onChange={(e) => {
                                    setFieldValue("startTime", e);
                                    setCekEventDateExist(
                                      e ? moment(e).format("YYYY-MM-DD") : null
                                    );
                                  }}
                                  value={values.startTime}
                                  name="startTime"
                                  onBlur={handleBlur}
                                />
                              </FormField>
                            </Grid>
                            <Grid item xs={6}>
                              <FormField
                                label="End Date :"
                                error={touched?.endTime && !values.endTime}
                                // errorMessage={errors?.endTime}
                                errorMessage={
                                  values.isRegularEvent &&
                                  !values.endTime &&
                                  "End Date is required"
                                }
                              >
                                <GeneralDatePicker
                                  placeholder="12/12/2022"
                                  format="DD/MM/YYYY"
                                  style={{ width: "100%" }}
                                  onChange={(e) => {
                                    setFieldValue("endTime", e);
                                  }}
                                  value={values.endTime}
                                  name="endTime"
                                  onBlur={handleBlur}
                                />
                              </FormField>
                            </Grid>
                          </React.Fragment>
                        ) : (
                          <Grid item xs={6}>
                            <FormField
                              label="Event Date :"
                              error={touched?.eventDate && !values.eventDate}
                              // errorMessage={errors?.eventDate}
                              errorMessage={
                                !values.isRegularEvent &&
                                !values.eventDate &&
                                "Event Date is required"
                              }
                            >
                              <GeneralDatePicker
                                placeholder="12/12/2022"
                                format="DD/MM/YYYY"
                                style={{ width: "100%" }}
                                onChange={(e) => {
                                  setFieldValue("eventDate", e);
                                  setCekEventDateExist(
                                    e ? moment(e).format("YYYY-MM-DD") : null
                                  );
                                }}
                                value={values.eventDate}
                                name="eventDate"
                                onBlur={handleBlur}
                              />
                            </FormField>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  </Grid>

                  <div className={classes.buttonGroup}>
                    <ButtonOutline
                      label="Cancel"
                      style={{ width: 157.5 }}
                      onClick={() => {
                        handleCloseModal();
                        clearData();
                      }}
                    />
                    <GeneralButton
                      label="Save"
                      style={{ width: 157.5 }}
                      disabled={
                        !isValid ||
                        isAlreadyExist ||
                        (!values.isRegularEvent && !values.eventDate) ||
                        (values.isRegularEvent && !values.regularEventType) ||
                        (values.isRegularEvent && !values.startTime) ||
                        (values.isRegularEvent && !values.endTime)
                      }
                      type="submit"
                    />
                  </div>
                </Form>
              </div>
            </Fade>
          </Modal>
        )}
      </Formik>
    </div>
  );
};

NewEvent.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

NewEvent.defaultProps = {};

export default NewEvent;
