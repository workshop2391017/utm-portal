import React from "react";
import * as dates from "date-arithmetic";
import { navigate } from "react-big-calendar/lib/utils/constants";
import Calendar from "./year";

class Year extends React.Component {
  render() {
    const { date } = this.props;
    const range = Year.range(date);
    const months = [];
    const firstMonth = dates?.startOf(date, "year");

    for (let i = 0; i < 12; i++) {
      months.push(
        <Calendar
          key={i + 1}
          {...this.props}
          date={dates?.add(firstMonth, i, "month")}
        />
      );
    }

    return <div className="year">{months?.map((month) => month)}</div>;
  }
}

// Day.propTypes = {
//   date: PropTypes.instanceOf(Date).isRequired,
// }

Year.range = (date) => [dates?.startOf(date, "year")];

Year.navigate = (date, action) => {
  switch (action) {
    case navigate.PREVIOUS:
      return dates?.add(date, -1, "year");

    case navigate.NEXT:
      return dates?.add(date, 1, "year");

    default:
      return date;
  }
};

Year.title = (date, { localizer }) =>
  localizer.format(date, "yearHeaderFormat");

export default Year;
