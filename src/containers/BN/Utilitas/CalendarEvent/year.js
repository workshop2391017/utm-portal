/* eslint-disable no-param-reassign */
/* eslint-disable react/destructuring-assignment */
import React from "react";
import moment from "moment";

export const createCalendar = (currentDate) => {
  if (!currentDate) {
    currentDate = moment();
  } else {
    currentDate = moment(currentDate);
  }

  const first = currentDate.clone().startOf("month");
  const last = currentDate.clone().endOf("month");
  const weeksCount = Math.ceil((first.day() + last.date()) / 7);
  const calendar = Object.assign([], { currentDate, first, last });

  for (let weekNumber = 0; weekNumber < weeksCount; weekNumber++) {
    const week = [];
    calendar.push(week);
    calendar.year = currentDate.year();
    calendar.month = currentDate.month();

    for (let day = 7 * weekNumber; day < 7 * (weekNumber + 1); day++) {
      const date = currentDate.clone().set("date", day + 1 - first.day());
      date.calendar = calendar;
      week.push(date);
    }
  }

  return calendar;
};

function CalendarDate(props) {
  const { dateToRender, dateOfMonth, event } = props;

  const areThereEvent = () => {
    const event = props.events.filter(
      (e) =>
        moment(e.start).format("YYYY-MM-DD") ===
        dateToRender.format("YYYY-MM-DD")
    );

    return event;
  };

  const today =
    dateToRender.format("YYYY-MM-DD") === moment().format("YYYY-MM-DD")
      ? "today"
      : "";

  const className = areThereEvent().length ? "event-fill" : null;
  if (dateToRender.month() < dateOfMonth.month()) {
    return (
      <button
        disabled
        className="date prev-month"
        onClick={() => props.onSelectSlot({ start: dateToRender })}
      >
        <div className={className}>
          {dateToRender.date()}
          {areThereEvent().length ? (
            <div className="rbc-event-content rbc-event-content-yearly" />
          ) : null}
        </div>
      </button>
    );
  }

  if (dateToRender.month() > dateOfMonth.month()) {
    return (
      <button
        disabled
        className="date next-month"
        onClick={() => props.onSelectSlot({ start: dateToRender })}
      >
        <div className={className}>
          {dateToRender.date()}
          {areThereEvent().length ? (
            <div className="rbc-event-content rbc-event-content-yearly" />
          ) : null}
        </div>
      </button>
    );
  }

  return (
    <button
      className={`date in-month ${today}`}
      // eslint-disable-next-line react/destructuring-assignment
      onClick={() => props.onSelectSlot({ start: dateToRender })}
    >
      <div className={className}>
        {dateToRender.date()}
        {areThereEvent().length ? (
          <div className="rbc-event-content rbc-event-content-yearly " />
        ) : null}
      </div>
    </button>
  );
}

class Calendar extends React.Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {
    calendar: undefined,
  };

  componentDidMount() {
    // eslint-disable-next-line react/destructuring-assignment

    this.setState({ calendar: createCalendar(this?.props?.date) });
  }

  componentDidUpdate(prevProps) {
    if (this?.props?.date !== prevProps.date) {
      // eslint-disable-next-line react/destructuring-assignment
      this.setState({ calendar: createCalendar(this?.props?.date) });
    }
  }

  render() {
    if (!this.state.calendar) {
      return null;
    }

    return (
      <div className="month">
        <div className="month-name">
          {this.state.calendar.currentDate.format("MMMM").toUpperCase()}
        </div>
        {["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"].map((day, index) => (
          <span key={index} className="day">
            {day}
          </span>
        ))}
        {this.state.calendar.map((week, index) => (
          <div key={index}>
            {week.map((date) => (
              <CalendarDate
                key={date.date()}
                dateToRender={date}
                dateOfMonth={this?.state?.calendar?.currentDate}
                events={this.props.events}
                onSelectSlot={this.props.onSelectSlot}
              />
            ))}
          </div>
        ))}
      </div>
    );
  }
}

export default Calendar;
