// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ReactComponent as CloseIcon } from "assets/icons/BN/close-blue.svg";
import ScrollCustom from "components/BN/ScrollCustom";
import Colors from "helpers/colors";
import { ReactComponent as Trash } from "assets/icons/BN/trash-2.svg";
import { ReactComponent as Edit } from "assets/icons/BN/edit.svg";
import moment from "moment";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import {
  handleDeleteCalendarEvent,
  setEvent,
} from "stores/actions/calendarEvent";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 554,
    height: 396,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 36px",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: 400,
    marginBottom: 40,
    textAlign: "center",
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 112,
  },
  //   eventList: { height },
  event: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 20,
    alignItems: "center",
    "& .event-category": {
      fontFamily: "FuturaMdBT",
      fontSize: 13,
      color: Colors.primary.hard,
    },
    "& .event-name": {
      fontFamily: "FuturaHvBT",
      fontSize: 15,
      color: Colors.dark.hard,
      marginTop: 6,
    },
    "& .event-time": {
      fontFamily: "FuturaBkBT",
      color: Colors.dark.medium,
      marginTop: 6,
      fontSize: 15,
    },
  },
}));

const PopupDetailEvent = ({ isOpen, handleClose, setOpenModal }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [confirmation, setConfirmation] = useState(false);
  const [success, setSuccess] = useState(false);

  const { eventByDate, event, isSubmitting } = useSelector(
    (e) => e.calendarEvent
  );

  const handleDelete = () => {
    dispatch(
      validateTask(
        {
          ...(event?.id && {
            id: event?.id,
          }),
          name: event.title,
          eventDate: moment(event?.eventDate).format("YYYY-MM-DD"),
          menuName: validateTaskConstant.CALENDAR_EVENT,
        },
        {
          async onContinue() {
            dispatch(
              handleDeleteCalendarEvent(
                {
                  id: event.id,
                  eventDate: moment(event?.eventDate).format("YYYY-MM-DD"),
                  name: event.title,
                  type: event.type,
                  regularEvent: event.regularEvent,
                  isDelete: true,
                },
                () => {
                  setConfirmation(false);
                  setSuccess(true);
                }
              )
            );
          },
          onError() {
            setConfirmation(false);
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  return (
    <div>
      <DeletePopup
        isOpen={confirmation}
        handleClose={() => {
          setConfirmation(false);
        }}
        onContinue={handleDelete}
        loading={isSubmitting}
        message="Are You Sure to Delete the Data?"
      />
      <SuccessConfirmation
        isOpen={success}
        message="Deleted Successfully"
        handleClose={() => {
          setSuccess(false);
          dispatch(setEvent({}));
          handleClose();
        }}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Event</h1>
            <CloseIcon
              style={{
                position: "absolute",
                right: 0,
                top: 0,
                marginTop: 43,
                marginRight: 26,
                cursor: "pointer",
              }}
              onClick={handleClose}
            />
            <div>
              <ScrollCustom height={238} scroll={eventByDate?.length >= 3}>
                <div className={classes.eventList}>
                  {(eventByDate ?? []).map((elm, i) => (
                    <Fragment key={i}>
                      <div className={classes.event}>
                        <div>
                          <div className="event-category">
                            {elm?.regularEvent ? "Regular Event" : null}
                          </div>
                          <div className="event-name">{elm.title}</div>
                          <div className="event-time">
                            {moment(elm.start).format("MMM DD, YYYY")}
                          </div>
                        </div>
                        <div style={{ display: "flex" }}>
                          <Edit
                            width={24}
                            height={24}
                            style={{ marginRight: 18, cursor: "pointer" }}
                            onClick={() => {
                              dispatch(setEvent({ ...elm }));
                              setOpenModal(true);
                              handleClose();
                            }}
                          />
                          <Trash
                            width={24}
                            height={24}
                            style={{ marginRight: 15, cursor: "pointer" }}
                            onClick={() => {
                              dispatch(setEvent(elm));
                              setConfirmation(true);
                            }}
                          />
                        </div>
                      </div>
                    </Fragment>
                  ))}
                </div>
              </ScrollCustom>
              {!eventByDate.length ? (
                <div
                  style={{
                    textAlign: "center",
                    fontFamily: "FuturaMdBT",
                    fontSize: 21,
                    color: Colors.dark.medium,
                    marginTop: 120,
                  }}
                >
                  No Data
                </div>
              ) : null}
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupDetailEvent.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

PopupDetailEvent.defaultProps = {};

export default PopupDetailEvent;
