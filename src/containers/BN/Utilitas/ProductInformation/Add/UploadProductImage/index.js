import React, { Fragment, useState, memo } from "react";

import {
  Button,
  FormLabel,
  Grid,
  makeStyles,
  Typography,
  useMediaQuery,
} from "@material-ui/core";

import AntdTextField from "components/BN/TextField/AntdTextField";

import Colors from "helpers/colors";

const useStyles = makeStyles((theme) => ({
  button: {
    ...theme.typography.actionButton,
    ...theme.containedButton,
    width: 154,
    height: 40,
  },
  label: {
    ...theme.typography.subtitle1,
    color: Colors.dark.hard,
    margin: 0,
  },
}));

const UploadProductImage = (props) => {
  const {
    onUpload,
    onGetImageFormat,
    imageName,
    onGetImageName,
    error,
    onError,
  } = props;

  const classes = useStyles();

  const matches = useMediaQuery("(max-width:1400px)");

  const clickHandler = () => document.getElementById("getFile").click();

  const convertBase64 = (file) =>
    new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = (event) => {
        const image = new Image();
        image.src = event.target.result;
        resolve(fileReader.result);
      };

      fileReader.onerror = (error) => reject(error);
    });

  const uploadHandler = async (event) => {
    onError("");

    const file = event.target.files[0];
    const format = `.${file.name.split(".").slice(-1)}`;

    onGetImageName(file.name);

    if (
      (format === ".jpg" || format === ".png" || format === ".jpeg") &&
      file.size < 2000000
    ) {
      const base64 = await convertBase64(file);

      onUpload(base64);
      onGetImageFormat(format);
    } else {
      onError("Re-upload with appropriate file type and size");
    }
  };

  return (
    <Fragment>
      <FormLabel
        component="label"
        className={classes.label}
        style={{
          color: error.length ? Colors.warning.medium : undefined,
        }}
      >
        Upload Product Image :
      </FormLabel>
      <Grid container justifyContent="space-between" item>
        <AntdTextField
          readOnly
          value={imageName}
          style={{
            width: matches ? "65%" : "70%",
            color: error.length ? Colors.warning.medium : undefined,
            borderColor: error.length ? Colors.warning.medium : undefined,
          }}
        />
        <Button
          onClick={clickHandler}
          variant="contained"
          color="primary"
          className={classes.button}
          disableElevation
        >
          Upload Image
        </Button>
        <input
          type="file"
          accept="image/png, image/jpeg, image/jpg"
          id="getFile"
          style={{ display: "none" }}
          onChange={uploadHandler}
        />
      </Grid>
      <Typography variant="subtitle1" style={{ color: Colors.warning.medium }}>
        {error}
      </Typography>
    </Fragment>
  );
};

export default memo(UploadProductImage);
