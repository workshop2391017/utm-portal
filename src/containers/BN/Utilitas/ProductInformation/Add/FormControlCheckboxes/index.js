import React, { memo } from "react";

import {
  FormControl,
  FormLabel,
  FormGroup,
  Grid,
  FormControlLabel,
  Checkbox,
  makeStyles,
} from "@material-ui/core";

import Colors from "helpers/colors";

const useStyles = makeStyles((theme) => ({
  subtitle1: {
    ...theme.typography.subtitle1,
  },
  checkbox: {
    "&:not(.Mui-checked)": {
      color: Colors.neutral.gray.medium,
    },
  },
  legend: {
    ...theme.typography.subtitle1,
    color: Colors.dark.hard,
    margin: 0,
  },
  ul: { listStyle: "none", padding: 0, margin: 0 },
}));

const FormControlCheckboxes = (props) => {
  const { xs = 12, checkboxes, onChange, label, style } = props;

  const classes = useStyles();

  return (
    <FormControl component="fieldset" fullWidth style={style}>
      <FormLabel component="legend" className={classes.legend}>
        {label}
      </FormLabel>
      <FormGroup>
        <Grid container component="ul" className={classes.ul}>
          {checkboxes &&
            checkboxes.map((item) => (
              <Grid item xs={xs} component="li" key={item.label}>
                <FormControlLabel
                  label={item.label}
                  control={
                    <Checkbox
                      color="primary"
                      name={item.name}
                      checked={item.checked}
                      onChange={onChange}
                      className={classes.checkbox}
                    />
                  }
                  classes={{ label: classes.subtitle1 }}
                />
              </Grid>
            ))}
        </Grid>
      </FormGroup>
    </FormControl>
  );
};

export default memo(FormControlCheckboxes);
