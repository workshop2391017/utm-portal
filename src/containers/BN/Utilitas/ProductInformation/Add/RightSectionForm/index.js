import React, { Fragment, useState, useEffect, useMemo, memo } from "react";

import { Grid, Typography, makeStyles } from "@material-ui/core";

import AntdTextField from "components/BN/TextField/AntdTextField";
import AntdSelectWithCheck from "components/BN/Select/AntdSelectWithCheck";

import Colors from "helpers/colors";
import formatNumber from "helpers/formatNumber";

import UploadProductImage from "../UploadProductImage";
import ImagePreview from "../ImagePreview";
import FormControlCheckboxes from "../FormControlCheckboxes";

const useStyles = makeStyles((theme) => ({
  imagePreviewContainer: {
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    border: `1px solid ${Colors.gray.soft}`,
  },
  label: {
    ...theme.typography.subtitle1,
    color: Colors.dark.hard,
    margin: 0,
  },
}));

const RightSectionForm = (props) => {
  const {
    // states
    currencyCode,
    minInitialDeposit,
    minBalance,
    holdAmount,
    base64ProductImage,
    isShowToNasabah,
    disabledCurrency,
    imageName,
    // setters
    onChangeCurrency,
    onChangeInitialDeposit,
    onChangeMinBalance,
    onChangeHoldAmount,
    onUpload,
    onGetImageFormat,
    onChangeShowToNasabah,
    onGetImageName,
    // mapping
    currencies,
  } = props;

  const classes = useStyles();

  // const [currencyOptions, setCurrencyOptions] = useState([]);
  const [uploadError, setUploadError] = useState("");

  const checkbox = useMemo(
    () => [
      {
        checked: isShowToNasabah,
        name: "isShowToNasabah",
        label: "Add product information on the customer side",
      },
    ],
    [isShowToNasabah]
  );

  return (
    <Fragment>
      {/* -----Upload Image Block----- */}
      <Grid container direction="column" item style={{ marginBottom: 20 }}>
        <UploadProductImage
          error={uploadError}
          imageName={imageName}
          onError={setUploadError}
          onUpload={onUpload}
          onGetImageFormat={onGetImageFormat}
          onGetImageName={onGetImageName}
        />
      </Grid>

      {/* -----Image Preview Block----- */}
      <Grid
        container
        direction="column"
        item
        className={classes.imagePreviewContainer}
      >
        <ImagePreview image={base64ProductImage} error={uploadError} />
      </Grid>

      {/* -----Currency Block----- */}
      <Grid container direction="column" item style={{ marginBottom: 20 }}>
        <Typography variant="subtitle1" component="label">
          Select Currency :
        </Typography>
        <AntdSelectWithCheck
          value={currencyCode}
          onChange={onChangeCurrency}
          options={currencies}
          placeholder="Choose Currency"
          disabled={disabledCurrency}
        />
      </Grid>

      {/* -----Initial Deposit Block----- */}
      <Typography variant="subtitle1" component="label">
        Initial Deposit :
      </Typography>
      <AntdTextField
        value={formatNumber(minInitialDeposit)}
        onChange={onChangeInitialDeposit}
        maxLength={19}
        style={{ width: "100%", marginBottom: 20 }}
      />

      {/* -----Minimum Balance Block----- */}
      <Typography variant="subtitle1" component="label">
        Minimum Balance :
      </Typography>
      <AntdTextField
        value={formatNumber(minBalance)}
        onChange={onChangeMinBalance}
        maxLength={19}
        style={{ width: "100%", marginBottom: 20 }}
      />

      {/* -----Hold Amount Block----- */}
      <Typography variant="subtitle1" component="label">
        Hold Amount :
      </Typography>
      <AntdTextField
        value={formatNumber(holdAmount)}
        onChange={onChangeHoldAmount}
        maxLength={19}
        style={{ width: "100%", marginBottom: 20 }}
      />

      {/* -----Show To Nasabah Block----- */}
      <FormControlCheckboxes
        checkboxes={checkbox}
        onChange={onChangeShowToNasabah}
      />
    </Fragment>
  );
};

export default memo(RightSectionForm);
