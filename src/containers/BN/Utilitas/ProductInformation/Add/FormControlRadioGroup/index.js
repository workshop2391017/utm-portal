import React, { memo } from "react";

import {
  FormControl,
  FormLabel,
  makeStyles,
  Grid,
  FormControlLabel,
  RadioGroup,
  Radio,
} from "@material-ui/core";

import Colors from "helpers/colors";

const useStyles = makeStyles((theme) => ({
  legend: {
    ...theme.typography.subtitle1,
    color: Colors.dark.hard,
    margin: 0,
  },
  subtitle1: {
    ...theme.typography.subtitle1,
  },
  radio: {
    "&:not(.Mui-checked)": {
      color: Colors.neutral.gray.medium,
    },
  },
  ul: { listStyle: "none", padding: 0, margin: 0 },
}));

const FormControlRadioGroup = (props) => {
  const { label, options, value, onChange, disabled, style } = props;

  const classes = useStyles();

  return (
    <FormControl component="fieldset" fullWidth style={style}>
      <FormLabel component="legend" className={classes.legend}>
        {label}
      </FormLabel>
      <RadioGroup value={value} onChange={onChange}>
        <Grid container component="ul" className={classes.ul}>
          {options &&
            options.map((option) => (
              <Grid item xs={6} key={option.value} component="li">
                <FormControlLabel
                  label={option.label}
                  value={option.value}
                  control={
                    <Radio
                      color="primary"
                      className={classes.radio}
                      disabled={disabled}
                    />
                  }
                  classes={{ label: classes.subtitle1 }}
                />
              </Grid>
            ))}
        </Grid>
      </RadioGroup>
    </FormControl>
  );
};

export default memo(FormControlRadioGroup);
