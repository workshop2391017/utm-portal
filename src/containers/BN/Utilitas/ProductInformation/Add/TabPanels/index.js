import React, { Fragment, memo } from "react";

import { Typography } from "@material-ui/core";

import AntdTabs from "components/BN/Tabs/AntdTabs";
import TabPanel from "components/BN/Tabs/TabPanel";
import RichText from "components/BN/RichText";
import RichTextSunEditor from "components/BN/RichTextSunEditor";

const gap = { display: "flex", flexDirection: "column", gap: 20 };

const marginTop = { marginTop: 20 };

const TabPanels = (props) => {
  const {
    // states
    activeTab,
    productBenefitIdn,
    productBenefitEng,
    productInfoIdn,
    productInfoEng,
    // setters
    onChangeActiveTab,
    onChangeProductBenefitIdn,
    onChangeProductBenefitEng,
    onChangeProductInfoIdn,
    onChangeProductInfoEng,
    errorMinMaxValidateBenefitIdn,
    errorMinMaxValidateBenefitEng,
    errorMinMaxValidateInfoEng,
    errorMinMaxValidateInfoIdn,
  } = props;

  return (
    <div style={marginTop}>
      <AntdTabs
        tabs={["Product Benefits", "Product Description"]}
        onChange={onChangeActiveTab}
      />
      <TabPanel value="1" currentValue={activeTab} style={gap}>
        <div>
          <Typography variant="subtitle1" component="label">
            Product Benefits ID :
          </Typography>
          <RichTextSunEditor
            formName="update"
            placeholder="Enter Product Benefits"
            content={productBenefitIdn}
            onChange={onChangeProductBenefitIdn}
          />
          <span style={{ color: "#D14848" }}>
            {errorMinMaxValidateBenefitIdn}
          </span>
        </div>
        <div>
          <Typography variant="subtitle1" component="label">
            Product Benefits EN :
          </Typography>
          <RichTextSunEditor
            formName="update"
            placeholder="Enter Product Benefits"
            content={productBenefitEng}
            onChange={onChangeProductBenefitEng}
          />
          <span style={{ color: "#D14848" }}>
            {errorMinMaxValidateBenefitEng}
          </span>
        </div>
      </TabPanel>
      <TabPanel value="2" currentValue={activeTab} style={gap}>
        <div>
          <Typography
            variant="subtitle1"
            component="label"
            style={{ marginTop: 20 }}
          >
            Product Description ID :
          </Typography>
          <RichTextSunEditor
            formName="update"
            placeholder="Enter Product Description"
            content={productInfoIdn}
            onChange={onChangeProductInfoIdn}
          />
          <span style={{ color: "#D14848" }}>{errorMinMaxValidateInfoIdn}</span>
        </div>
        <div>
          <Typography variant="subtitle1" component="label">
            Product Description EN :
          </Typography>
          <RichTextSunEditor
            formName="update"
            placeholder="Enter Product Description"
            content={productInfoEng}
            onChange={onChangeProductInfoEng}
          />
          <span style={{ color: "#D14848" }}>{errorMinMaxValidateInfoEng}</span>
        </div>
      </TabPanel>
    </div>
  );
};

export default memo(TabPanels);
