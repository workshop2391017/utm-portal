import React, { Fragment, memo } from "react";

import { Grid, Typography, makeStyles } from "@material-ui/core";

import { ReactComponent as ImagePreviewIcon } from "assets/icons/BN/image-preview.svg";

import Colors from "helpers/colors";

const useStyles = makeStyles({
  container: {
    width: 296,
    height: 185,
    borderRadius: 10,
    border: `1px solid ${Colors.gray.medium}`,
  },
  message: {
    marginTop: 18,
    marginBottom: 6,
    letterSpacing: "0.03em",
    color: Colors.neutral.gray.medium,
  },
  submessage: {
    width: 121,
    fontSize: 10,
    textAlign: "center",
    color: Colors.neutral.gray.medium,
  },
  image: { borderRadius: 10, overflow: "hidden" },
});

const ImagePreview = (props) => {
  const { image, error } = props;

  const classes = useStyles();

  return (
    <Fragment>
      <Typography variant="subtitle1" component="label">
        Product Image Preview :
      </Typography>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        item
        className={classes.container}
      >
        {!image?.length || error?.length ? (
          <Fragment>
            <ImagePreviewIcon />
            <Typography variant="subtitle2" className={classes.message}>
              Upload Product Image
            </Typography>
            <Typography variant="subtitle2" className={classes.submessage}>
              Image Size 296x185 and File Type JPEG or PNG
            </Typography>
          </Fragment>
        ) : (
          <img src={image} alt="product" className={classes.image} />
        )}
      </Grid>
    </Fragment>
  );
};

export default memo(ImagePreview);
