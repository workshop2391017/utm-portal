import React, {
  Fragment,
  useState,
  useEffect,
  useReducer,
  useMemo,
} from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  Grid,
  Card,
  CardContent,
  Typography,
  Button,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";

import Footer from "components/BN/Footer";
import ConfirmationPopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";

import Colors from "helpers/colors";

import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import {
  actionAccountProductDetail,
  actionGetAllProductList,
  actionGetAllProductCodeList,
  actionGetInfoCurrency,
  actionAccountProductAdd,
  actionAccountProductEdit,
  actionInquiryProduct,
  actionGetAccountType,
  setInitData,
} from "stores/actions/productInformation";

import LeftSectionForm from "./LeftSectionForm";
import RightSectionForm from "./RightSectionForm";
import TabPanels from "./TabPanels";
import { Currency } from "../../../../../helpers/currency";
import { useValidateLms } from "../../../../../utils/helpers/validateLms";
import { validateMinMax } from "../../../../../utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: 13,
    background: Colors.gray.ultrasoft,
  },
  backButton: {
    ...theme.typography.backButton,
  },
  loading: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
}));

const savingTypes = [
  {
    value: "0",
    label: "Transactional",
  },
  {
    value: "3",
    label: "Investment",
  },
  {
    value: "2",
    label: "Futures",
  },
  {
    value: "1",
    label: "Cardless",
  },
  {
    value: "4",
    label: "Program (Investment)",
  },
];

const depositTypes = [
  {
    value: "1",
    label: "eDeposito",
  },
  {
    value: "3",
    label: "Deposito Valas",
  },
  {
    value: "2",
    label: "Deposito",
  },
];

const initialFormState = {
  product: null,
  productCode: null,
  productName: "",
  accountType: "",
  savingType: "",
  depositType: "",
  maximumAge: "",
  currencyCode: null,
  minInitialDeposit: "",
  minBalance: "",
  holdAmount: "",
  isShowToNasabah: false,
  productBenefitIdn: "",
  productBenefitEng: "",
  productInfoIdn: "",
  productInfoEng: "",
  base64ProductImage: "",
  imageName: "",
  extensionImage: "",
};

const formReducer = (state, action) => {
  switch (action.type) {
    case "SET_PRODUCT":
      return {
        ...state,
        product: action.product,
        productCode: action.productCode,
        productName: action.productName,
        accountType: "",
      };
    case "SET_PRODUCT_CODE":
      return { ...state, productCode: action.productCode };
    case "SET_INIT_STATE":
      return {
        ...state,
        product: "",
        productCode: "",
        productName: "",
        accountType: "",
      };
    case "SET_PRODUCT_NAME_ACCOUNT_TYPE":
      return {
        ...state,
        productName: action.productName,
        accountType: action.accountType,
      };
    case "SET_SAVING_TYPE":
      return { ...state, savingType: action.savingType };
    case "SET_DEPOSIT_TYPE":
      return { ...state, depositType: action.depositType };
    case "SET_MAXIMUM_AGE":
      return { ...state, maximumAge: action.maximumAge };
    case "SET_CURRENCY_CODE":
      return { ...state, currencyCode: action.currencyCode };
    case "SET_MIN_INITIAL_DEPOSIT":
      return { ...state, minInitialDeposit: action.minInitialDeposit };
    case "SET_MIN_BALANCE":
      return { ...state, minBalance: action.minBalance };
    case "SET_HOLD_AMOUNT":
      return { ...state, holdAmount: action.holdAmount };
    case "SET_IS_SHOW_TO_NASABAH":
      return { ...state, isShowToNasabah: action.isShowToNasabah };
    case "SET_PRODUCT_BENEFIT_IDN":
      return { ...state, productBenefitIdn: action.productBenefitIdn };
    case "SET_PRODUCT_BENEFIT_ENG":
      return { ...state, productBenefitEng: action.productBenefitEng };
    case "SET_PRODUCT_INFO_IDN":
      return { ...state, productInfoIdn: action.productInfoIdn };
    case "SET_PRODUCT_INFO_ENG":
      return { ...state, productInfoEng: action.productInfoEng };
    case "SET_BASE_64":
      return { ...state, base64ProductImage: action.base64ProductImage };
    case "SET_EXTENSION_IMAGE":
      return { ...state, extensionImage: action.extensionImage };
    case "SET_IMAGE_NAME":
      return { ...state, imageName: action.imageName };
    case "POPULATE_DATA":
      return {
        product: action.product,
        productCode: action.productCode,
        productName: action.productName,
        accountType: action.accountType,
        savingType: action.savingType,
        depositType: action.depositType,
        maximumAge: action.maximumAge,
        currencyCode: action.currencyCode,
        minInitialDeposit: action.minInitialDeposit,
        minBalance: action.minBalance,
        holdAmount: action.holdAmount,
        isShowToNasabah: action.isShowToNasabah,
        productBenefitIdn: action.productBenefitIdn,
        productBenefitEng: action.productBenefitEng,
        productInfoIdn: action.productInfoIdn,
        productInfoEng: action.productInfoEng,
        base64ProductImage: action.base64ProductImage,
        imageName: action.imageName,
        extensionImage: action.extensionImage,
      };

    default:
      break;
  }
};

const convertAccountType = (type) => {
  switch (type) {
    case "D":
      return "CA";
    case "S":
      return "SA";
    case "L":
      return "LN";
    case "T":
      return "CD";

    default:
      break;
  }
};

export default function UtilitasProductInformationForm(props) {
  const { rowId, onGoBack, activePage } = props;

  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    isLoading,
    isAddLoading,
    detail,
    products,
    productNameAccountType,
    productCodes,
    currencies,
    isAddSuccess,
    isEdit,
  } = useSelector(({ productInformation }) => productInformation);

  // ? these states handle conditional rendering, storing raw data for the edit form from the backend, and disabling select currency dropdown.
  const [detailData, setDetailData] = useState(null);
  const [currencyOptions, setCurrencyOptions] = useState([]);
  const [activeTab, setActiveTab] = useState("1");
  const [isMounted, setIsMounted] = useState(false);
  const [confirmationPopup, setConfirmationPopup] = useState(false);
  const [disabledCurrency, setDisabledCurrency] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");
  const [count, setCount] = useState(0);
  const [errorMinMax, setErrorMinMax] = useState({
    benefitEng: "",
    benefitIdn: "",
    infoEng: "",
    infoIdn: "",
  });
  const { isAlreadyExist } = useSelector((e) => e.validatedatalms);

  // ? this is the state for handling the checked state of the checkboxes.
  const [allowedFor, setAllowedFor] = useState({
    openAccountAllowed: false,
    sofEdeposito: false,
    isRegisterAllowed: false,
    isValasAllowed: false,
    sofAllowed: false,
  });

  // ? all of the data relevant to the request payload except the "Allowed For" checkboxes are managed here.
  const [formState, dispatchForm] = useReducer(formReducer, initialFormState);
  useValidateLms({
    code: formState?.productCode,
    codeCode: "50",
  });

  const vProductBenefitEng = !validateMinMax(formState.productBenefitEng, 9);
  const vProductBenefitIdn = !validateMinMax(formState.productBenefitIdn, 9);
  const vProductInfoEng = !validateMinMax(formState.productInfoIdn, 9);
  const vProductInfoIdn = !validateMinMax(formState.productInfoEng, 9);

  // ! -----POPULATE EDIT FORM HANDLER-----
  const populateDataHandler = () => {
    const extensionImageTemp = detailData?.imageName?.split(".") ?? 0;
    const extensionImage = `.${
      extensionImageTemp[extensionImageTemp?.length - 1]
    }`;

    dispatchForm({
      type: "POPULATE_DATA",
      product: detailData.product,
      productCode: detailData.productCode,
      productName: detailData.productName,
      accountType: detailData.accountType,
      savingType: detailData.savingType?.toString(),
      depositType: detailData.depositType,
      maximumAge: detailData.maximumAge ?? "",
      currencyCode: Currency(detailData?.currencyCode),
      minInitialDeposit: detailData.minInitialDeposit,
      minBalance: detailData.minBalance,
      holdAmount: detailData.holdAmount,
      isShowToNasabah: detailData.isShowToNasabah,
      base64ProductImage: detailData?.imageName?.includes(".png")
        ? `data:image/${detailData?.extensionImage};base64, ${detailData?.base64ProductImage}`
        : `data:image/${detailData?.extensionImage};base64,/9j/ ${detailData?.base64ProductImage}`,
      imageName: detailData.imageName,
      extensionImage,
      productBenefitIdn: detailData.productBenefitIdn,
      productBenefitEng: detailData.productBenefitEng,
      productInfoIdn: detailData.productInfoIdn,
      productInfoEng: detailData.productInfoEng,
    });

    setAllowedFor({
      openAccountAllowed: detailData?.openAccountAllowed,
      sofEdeposito: detailData?.sofEdeposito,
      isRegisterAllowed: detailData?.isRegisterAllowed,
      isValasAllowed: detailData?.isValasAllowed,
      sofAllowed: detailData?.sofAllowed,
    });
  };

  // * -----START SIDE EFFECT HANDLERS-----
  useEffect(() => {
    if (rowId && activePage === 3) {
      dispatch(actionAccountProductDetail({ id: rowId }));
    }

    dispatch(actionGetAccountType());
    dispatch(actionGetInfoCurrency());
  }, [rowId, activePage]);

  useEffect(() => {
    // ? if detailData state hasn't been updated and the details are already stored in Redux, then we store the raw data in detailData and populateDataHandler runs.
    if (Object.keys(detail).length && !detailData) setDetailData(detail);

    if (detailData) populateDataHandler();
  }, [detail, detailData, currencies, productCodes]);

  useEffect(() => {
    // ? this useEffect must be separated from the one above because this makes sure we only send a request to the server for the product code list in the edit form when the page is rendered for the first time.
    if (detailData) {
      dispatch(actionGetAllProductCodeList({ product: detailData.product }));
    }
  }, [detailData]);

  useEffect(() => {
    // ? this makes sure we send a request to the server for the product code list everytime the product state is updated, and we will know if the product state is updated because not only the product state value is updated, but everytime it's being updated the product code state is also being resetted.
    if (formState.product && !formState.productCode) {
      dispatch(actionGetAllProductCodeList({ product: formState.product }));
    }
    // ? everytime the product code state is updated, if it has a truthy value we will send a request to the server to get the product name and account type--isMounted state is a workaround to make sure this server request doesn't run when the edit form is initially rendered, because we will be getting the product name and account type from the detail service response, so this server request won't be needed.
    if (formState.productCode && isMounted) {
      dispatch(
        actionInquiryProduct({
          product: formState.product,
          productCode: formState.productCode,
        })
      );
    }

    const timer = setTimeout(() => setIsMounted(true), 2000);

    return () => clearTimeout(timer);
  }, [formState.product, formState.productCode]);

  useEffect(() => {
    if (isEdit === false) {
      setErrorMessage(
        isAlreadyExist?.code === true &&
          formState?.productName !== null &&
          formState?.productName !== ""
          ? "Product Name is Already Exist"
          : ""
      );
    }
  }, [formState, isAlreadyExist]);

  useEffect(() => {
    setErrorMinMax({
      ...errorMinMax,
      benefitEng:
        vProductBenefitEng || formState.productBenefitEng === "<p><br></p>"
          ? "Min 2 Characters"
          : "",
      benefitIdn:
        vProductBenefitIdn || formState.productBenefitIdn === "<p><br></p>"
          ? "Min 2 Characters"
          : "",
      infoIdn:
        vProductInfoIdn || formState.productInfoEng === "<p><br></p>"
          ? "Min 2 Characters"
          : "",
      infoEng:
        vProductInfoEng || formState.productInfoEng === "<p><br></p>"
          ? "Min 2 Characters"
          : "",
    });
  }, [
    formState?.productBenefitEng,
    formState?.productBenefitIdn,
    formState?.productInfoEng,
    formState?.productInfoIdn,
  ]);

  useEffect(() => {
    // ? this useEffect is for populating the product name and account type states after receiving the response from actionInquiryProduct which will be stored in Redux, and storing them again in the formState.
    if (Object.keys(productNameAccountType).length) {
      dispatchForm({
        type: "SET_PRODUCT_NAME_ACCOUNT_TYPE",
        productName: productNameAccountType.description,
        accountType: convertAccountType(productNameAccountType.types),
      });
    }
  }, [productNameAccountType]);

  useEffect(() => {
    // ? reset logic for when these respective inputs are disabled.
    if (formState.accountType === "LN" || formState.accountType === "CA") {
      dispatchForm({
        type: "SET_SAVING_TYPE",
        savingType: "",
      });
      dispatchForm({
        type: "SET_DEPOSIT_TYPE",
        depositType: "",
      });
    }
    if (formState.accountType === "CD") {
      dispatchForm({
        type: "SET_SAVING_TYPE",
        savingType: "",
      });
    }
    if (formState.accountType === "SA") {
      dispatchForm({
        type: "SET_DEPOSIT_TYPE",
        depositType: "",
      });
    }
  }, [formState.accountType]);

  useEffect(() => {
    // ? reset logic for when these respective inputs are disabled.
    if (formState.savingType !== "2" && formState.maximumAge !== "") {
      dispatchForm({
        type: "SET_MAXIMUM_AGE",
        maximumAge: "",
      });
    }
  }, [formState.savingType]);

  useEffect(() => {
    // ? reset logic for when these respective inputs are disabled.
    if (disabledCurrency && formState.currencyCode) {
      dispatchForm({
        type: "SET_CURRENCY_CODE",
        currencyCode: null,
      });
    }
  }, [disabledCurrency, formState.currencyCode]);

  useEffect(() => {
    if (currencies && currencies.length > 0) {
      const data = currencies.map(({ nameEng, currencyCode }, i) => ({
        value: currencyCode,
        label: nameEng,
        key: i,
      }));
      setCurrencyOptions(data);
    }
  }, [currencies]);
  // * -----END SIDE EFFECT HANDLERS-----

  // ! -----START DISPATCH FORM HANDLERS-----
  const productHandler = (event) =>
    dispatchForm({
      type: "SET_PRODUCT",
      product: event,
      productCode: null,
      productName: "",
    });

  const productCodeHandler = (event) =>
    dispatchForm({
      type: "SET_PRODUCT_CODE",
      productCode: event,
    });

  const savingTypeHandler = (event) =>
    dispatchForm({
      type: "SET_SAVING_TYPE",
      savingType: event.target.value,
    });

  const clearData = () =>
    dispatchForm({
      type: "SET_INIT_STATE",
    });

  const depositTypeHandler = (event) =>
    dispatchForm({
      type: "SET_DEPOSIT_TYPE",
      depositType: event.target.value,
    });

  const maximumAgeHandler = (event) => {
    dispatchForm({
      type: "SET_MAXIMUM_AGE",
      maximumAge: event.target.value.replace(/^0/g, "").replace(/\D/g, ""),
    });
  };

  const currencyHandler = (event) =>
    dispatchForm({ type: "SET_CURRENCY_CODE", currencyCode: event });

  const minInitialDepositHandler = (event) =>
    dispatchForm({
      type: "SET_MIN_INITIAL_DEPOSIT",
      minInitialDeposit: event.target.value.replace(/\D/g, ""),
    });

  const minBalanceHandler = (event) =>
    dispatchForm({
      type: "SET_MIN_BALANCE",
      minBalance: event.target.value.replace(/\D/g, ""),
    });

  const holdAmountHandler = (event) =>
    dispatchForm({
      type: "SET_HOLD_AMOUNT",
      holdAmount: event.target.value.replace(/\D/g, ""),
    });

  const productBenefitIdnHandler = (event) =>
    dispatchForm({
      type: "SET_PRODUCT_BENEFIT_IDN",
      productBenefitIdn: event,
    });

  const productBenefitEngHandler = (event) =>
    dispatchForm({
      type: "SET_PRODUCT_BENEFIT_ENG",
      productBenefitEng: event,
    });

  const productInfoIdnHandler = (event) =>
    dispatchForm({ type: "SET_PRODUCT_INFO_IDN", productInfoIdn: event });

  const productInfoEngHandler = (event) =>
    dispatchForm({ type: "SET_PRODUCT_INFO_ENG", productInfoEng: event });

  const isShowToNasabahHandler = (event) =>
    dispatchForm({
      type: "SET_IS_SHOW_TO_NASABAH",
      isShowToNasabah: event.target.checked,
    });

  const base64Handler = (value) =>
    dispatchForm({ type: "SET_BASE_64", base64ProductImage: value });

  const extensionImageHandler = (value) =>
    dispatchForm({ type: "SET_EXTENSION_IMAGE", extensionImage: value });

  const imageNameHandler = (value) =>
    dispatchForm({ type: "SET_IMAGE_NAME", imageName: value });
  // ! -----END DISPATCH FORM HANDLERS-----

  // * -----START STATE UPDATE HANDLERS-----
  const allowedForHandler = (event) => {
    setAllowedFor((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.checked,
    }));
  };

  const disableCurrencyHandler = (isValasAllowed) => {
    setDisabledCurrency(isValasAllowed);
  };

  const activeTabHandler = (event) => setActiveTab(event);

  useEffect(() => {
    if (activePage === 2) {
      clearData();
      dispatch(setInitData());
    }
  }, [activePage]);

  const clickBackHandler = () => {
    dispatch(setInitData());
    clearData();
    onGoBack(1);
  };

  const openConfirmationHandler = (event) => {
    event.preventDefault();

    setConfirmationPopup(true);
  };

  const closeConfirmationHandler = () => setConfirmationPopup(false);
  // * -----END STATE UPDATE HANDLERS-----
  // ! -----REQUEST PAYLOAD-----
  const submitHandler = () => {
    const imageNameTemp = formState?.imageName?.split(".");
    const imageName = imageNameTemp
      ?.slice(0, imageNameTemp?.length - 1)
      .join(".");

    const payload = {
      id: rowId,
      accountType: formState.accountType,
      productCode: formState.productCode,
      product: formState.product,
      productName: formState.productName,
      savingType: +formState.savingType,
      deleted: false,
      isRegisterAllowed: allowedFor.isRegisterAllowed,
      sofAllowed: allowedFor.sofAllowed,
      openAccountAllowed: allowedFor.openAccountAllowed,
      sofEdeposito: allowedFor.sofEdeposito,
      isValasAllowed: allowedFor.isValasAllowed,
      depositType: Number(+formState?.depositType),
      productInfoIdn: formState.productInfoIdn,
      productInfoEng: formState.productInfoEng,
      productBenefitIdn: formState.productBenefitIdn,
      productBenefitEng: formState.productBenefitEng,
      base64ProductImage:
        formState.extensionImage === ".png"
          ? formState.base64ProductImage.split(
              `data:image/${formState.extensionImage.slice(1)};base64,`
            )[1]
          : formState.base64ProductImage.split(
              `data:image/jpeg;base64,/9j/`
            )[1],
      extensionImage: formState.extensionImage,
      imageName,
      currencyCode: formState.currencyCode,
      holdAmount: +formState.holdAmount,
      maximumAge: +formState.maximumAge,
      minBalance: +formState.minBalance,
      minInitialDeposit: +formState.minInitialDeposit,
      isShowToNasabah: formState.isShowToNasabah,
    };

    if (activePage === 2) {
      dispatch(
        validateTask(
          {
            menuName: validateTaskConstant.PRODUCT_INFORMATION,
            id: rowId || null,
            code: formState.productCode,
            name: formState.productName,
            validate: true,
          },
          {
            onContinue() {
              dispatch(
                actionAccountProductAdd(payload, closeConfirmationHandler)
              );
            },
            onError() {
              closeConfirmationHandler();
            },
          },
          {
            redirect: false,
          }
        )
      );
    } else {
      dispatch(
        validateTask(
          {
            menuName: validateTaskConstant.PRODUCT_INFORMATION,
            id: rowId || null,
            code: formState.productCode,
            name: formState.productName,
            validate: true,
          },
          {
            onContinue() {
              dispatch(
                actionAccountProductEdit(payload, closeConfirmationHandler)
              );
            },
            onError() {
              closeConfirmationHandler();
            },
          },
          {
            redirect: false,
          }
        )
      );
    }
  };

  // * -----START DISABLE SAVE BUTTON HANDLER-----
  let disableSaveButtonHandler = true;
  const disableSave =
    errorMessage !== "" ||
    errorMinMax.benefitEng !== "" ||
    errorMinMax.benefitIdn !== "" ||
    errorMinMax.infoIdn !== "" ||
    errorMinMax.infoEng !== "" ||
    formState.productBenefitEng === "<p><br></p>" ||
    formState.productBenefitIdn === "<p><br></p>" ||
    formState.productInfoEng === "<p><br></p>" ||
    formState.productInfoIdn === "<p><br></p>" ||
    formState.productBenefitEng === "" ||
    formState.productBenefitIdn === "" ||
    formState.productInfoEng === "" ||
    formState.productInfoIdn === "" ||
    formState.holdAmount === "" ||
    formState.holdAmount === undefined ||
    formState.minBalance === "" ||
    formState.minBalance === undefined ||
    formState.minInitialDeposit === "" ||
    formState.minInitialDeposit === undefined;

  if (formState.accountType === "LN" || formState.accountType === "CA") {
    if (disabledCurrency) {
      disableSaveButtonHandler =
        !formState.product ||
        !formState.productCode ||
        !formState.productName ||
        !formState.accountType ||
        formState.minInitialDeposit === "" ||
        formState.minBalance === "" ||
        formState.holdAmount === "" ||
        !formState.productBenefitEng ||
        formState.productBenefitEng === "<p></p>\n" ||
        !formState.productBenefitIdn ||
        formState.productBenefitIdn === "<p></p>\n" ||
        !formState.productInfoEng ||
        formState.productInfoEng === "<p></p>\n" ||
        !formState.productInfoIdn ||
        formState.productInfoIdn === "<p></p>\n" ||
        !formState.extensionImage ||
        !formState.base64ProductImage;
    } else {
      disableSaveButtonHandler =
        !formState.product ||
        !formState.productCode ||
        !formState.productName ||
        !formState.accountType ||
        !formState.currencyCode ||
        formState.minInitialDeposit === "" ||
        formState.minBalance === "" ||
        formState.holdAmount === "" ||
        !formState.productBenefitEng ||
        formState.productBenefitEng === "<p></p>\n" ||
        !formState.productBenefitIdn ||
        formState.productBenefitIdn === "<p></p>\n" ||
        !formState.productInfoEng ||
        formState.productInfoEng === "<p></p>\n" ||
        !formState.productInfoIdn ||
        formState.productInfoIdn === "<p></p>\n" ||
        !formState.extensionImage ||
        !formState.base64ProductImage;
    }
  }
  if (formState.accountType === "CD") {
    if (disabledCurrency) {
      disableSaveButtonHandler =
        !formState.product ||
        !formState.productCode ||
        !formState.productName ||
        !formState.accountType ||
        !formState.depositType ||
        formState.minInitialDeposit === "" ||
        formState.minBalance === "" ||
        formState.holdAmount === "" ||
        !formState.productBenefitEng ||
        formState.productBenefitEng === "<p></p>\n" ||
        !formState.productBenefitIdn ||
        formState.productBenefitIdn === "<p></p>\n" ||
        !formState.productInfoEng ||
        formState.productInfoEng === "<p></p>\n" ||
        !formState.productInfoIdn ||
        formState.productInfoIdn === "<p></p>\n" ||
        !formState.extensionImage ||
        !formState.base64ProductImage;
    } else {
      disableSaveButtonHandler =
        !formState.product ||
        !formState.productCode ||
        !formState.productName ||
        !formState.accountType ||
        !formState.depositType ||
        !formState.currencyCode ||
        formState.minInitialDeposit === "" ||
        formState.minBalance === "" ||
        formState.holdAmount === "" ||
        !formState.productBenefitEng ||
        formState.productBenefitEng === "<p></p>\n" ||
        !formState.productBenefitIdn ||
        formState.productBenefitIdn === "<p></p>\n" ||
        !formState.productInfoEng ||
        formState.productInfoEng === "<p></p>\n" ||
        !formState.productInfoIdn ||
        formState.productInfoIdn === "<p></p>\n" ||
        !formState.extensionImage ||
        !formState.base64ProductImage;
    }
  }
  if (formState.accountType === "SA") {
    if (formState.savingType !== "2") {
      if (disabledCurrency) {
        disableSaveButtonHandler =
          !formState.product ||
          !formState.productCode ||
          !formState.productName ||
          !formState.accountType ||
          !formState.savingType ||
          formState.minInitialDeposit === "" ||
          formState.minBalance === "" ||
          formState.holdAmount === "" ||
          !formState.productBenefitEng ||
          formState.productBenefitEng === "<p></p>\n" ||
          !formState.productBenefitIdn ||
          formState.productBenefitIdn === "<p></p>\n" ||
          !formState.productInfoEng ||
          formState.productInfoEng === "<p></p>\n" ||
          !formState.productInfoIdn ||
          formState.productInfoIdn === "<p></p>\n" ||
          !formState.extensionImage ||
          !formState.base64ProductImage;
      } else {
        disableSaveButtonHandler =
          !formState.product ||
          !formState.productCode ||
          !formState.productName ||
          !formState.accountType ||
          !formState.savingType ||
          !formState.currencyCode ||
          formState.minInitialDeposit === "" ||
          formState.minBalance === "" ||
          formState.holdAmount === "" ||
          !formState.productBenefitEng ||
          formState.productBenefitEng === "<p></p>\n" ||
          !formState.productBenefitIdn ||
          formState.productBenefitIdn === "<p></p>\n" ||
          !formState.productInfoEng ||
          formState.productInfoEng === "<p></p>\n" ||
          !formState.productInfoIdn ||
          formState.productInfoIdn === "<p></p>\n" ||
          !formState.extensionImage ||
          !formState.base64ProductImage;
      }
    }
    if (formState.savingType === "2") {
      if (disabledCurrency) {
        disableSaveButtonHandler =
          !formState.product ||
          !formState.productCode ||
          !formState.productName ||
          !formState.accountType ||
          !formState.savingType ||
          formState.maximumAge === "" ||
          formState.minInitialDeposit === "" ||
          formState.minBalance === "" ||
          formState.holdAmount === "" ||
          !formState.productBenefitEng ||
          formState.productBenefitEng === "<p></p>\n" ||
          !formState.productBenefitIdn ||
          formState.productBenefitIdn === "<p></p>\n" ||
          !formState.productInfoEng ||
          formState.productInfoEng === "<p></p>\n" ||
          !formState.productInfoIdn ||
          formState.productInfoIdn === "<p></p>\n" ||
          !formState.extensionImage ||
          !formState.base64ProductImage;
      } else {
        disableSaveButtonHandler =
          !formState.product ||
          !formState.productCode ||
          !formState.productName ||
          !formState.accountType ||
          !formState.savingType ||
          formState.maximumAge === "" ||
          !formState.currencyCode ||
          formState.minInitialDeposit === "" ||
          formState.minBalance === "" ||
          formState.holdAmount === "" ||
          !formState.productBenefitEng ||
          formState.productBenefitEng === "<p></p>\n" ||
          !formState.productBenefitIdn ||
          formState.productBenefitIdn === "<p></p>\n" ||
          !formState.productInfoEng ||
          formState.productInfoEng === "<p></p>\n" ||
          !formState.productInfoIdn ||
          formState.productInfoIdn === "<p></p>\n" ||
          !formState.extensionImage ||
          !formState.base64ProductImage;
      }
    }
  }
  // * -----END DISABLE SAVE BUTTON HANDLER-----

  return (
    <Grid
      container
      direction="column"
      component="section"
      className={classes.container}
    >
      {/* -----Header Block----- */}
      <Grid
        container
        direction="column"
        item
        component="header"
        style={{ paddingLeft: 23 }}
      >
        <Grid item>
          <Button
            size="small"
            startIcon={<ArrowLeft />}
            onClick={clickBackHandler}
            className={classes.backButton}
          >
            Back
          </Button>
        </Grid>
        <Typography variant="h2">
          {activePage === 3 ? "Edit" : "Add"} Product Information
        </Typography>
      </Grid>

      {/* -----Form Block----- */}
      <Card
        component="main"
        elevation={0}
        style={{ borderRadius: 8, margin: "33px 40px 33px 23px" }}
      >
        <CardContent
          id="form-product-information"
          component="form"
          onSubmit={openConfirmationHandler}
          style={{
            padding: "20px 30px",
            height:
              isLoading || (activePage === 3 && !detailData) ? 460 : undefined,
          }}
        >
          {isLoading || (activePage === 3 && !detailData) ? (
            <div className={classes.loading}>
              <div>
                <CircularProgress />
              </div>
            </div>
          ) : (
            <Fragment>
              <Grid container item>
                <Grid item xs={6}>
                  <LeftSectionForm
                    // states
                    product={formState.product}
                    productCode={formState.productCode}
                    productName={formState.productName}
                    accountType={formState.accountType}
                    savingType={formState.savingType}
                    allowedFor={allowedFor}
                    depositType={formState.depositType}
                    maximumAge={formState.maximumAge}
                    // setters
                    onChangeProduct={productHandler}
                    onChangeProductCode={productCodeHandler}
                    onChangeSavingType={savingTypeHandler}
                    onChangeAllowedFor={allowedForHandler}
                    onChangeDepositType={depositTypeHandler}
                    onChangeMaximumAge={maximumAgeHandler}
                    onValasChange={disableCurrencyHandler}
                    // mappings
                    products={products}
                    productCodes={productCodes}
                    savingTypes={savingTypes}
                    depositTypes={depositTypes}
                    errorMessage={errorMessage}
                  />
                </Grid>
                <Grid item xs={6}>
                  <RightSectionForm
                    // states
                    currencyCode={formState.currencyCode}
                    minInitialDeposit={formState.minInitialDeposit}
                    minBalance={formState.minBalance}
                    holdAmount={formState.holdAmount}
                    base64ProductImage={formState.base64ProductImage}
                    isShowToNasabah={formState.isShowToNasabah}
                    disabledCurrency={disabledCurrency}
                    imageName={formState.imageName}
                    // setters
                    onChangeCurrency={currencyHandler}
                    onChangeInitialDeposit={minInitialDepositHandler}
                    onChangeMinBalance={minBalanceHandler}
                    onChangeHoldAmount={holdAmountHandler}
                    onUpload={base64Handler}
                    onGetImageFormat={extensionImageHandler}
                    onGetImageName={imageNameHandler}
                    onChangeShowToNasabah={isShowToNasabahHandler}
                    // mapping
                    currencies={currencyOptions}
                  />
                </Grid>
              </Grid>
              <TabPanels
                // states
                activeTab={activeTab}
                productBenefitIdn={formState.productBenefitIdn}
                productBenefitEng={formState.productBenefitEng}
                productInfoIdn={formState.productInfoIdn}
                productInfoEng={formState.productInfoEng}
                // setters
                onChangeActiveTab={activeTabHandler}
                onChangeProductBenefitIdn={productBenefitIdnHandler}
                onChangeProductBenefitEng={productBenefitEngHandler}
                onChangeProductInfoIdn={productInfoIdnHandler}
                onChangeProductInfoEng={productInfoEngHandler}
                errorMinMaxValidateBenefitIdn={errorMinMax.benefitIdn}
                errorMinMaxValidateBenefitEng={errorMinMax.benefitEng}
                errorMinMaxValidateInfoEng={errorMinMax.infoEng}
                errorMinMaxValidateInfoIdn={errorMinMax.infoIdn}
              />
            </Fragment>
          )}
        </CardContent>
      </Card>
      {/* -----Button Group----- */}
      <Footer
        disableElevation
        formId="form-product-information"
        type="submit"
        onCancel={clickBackHandler}
        disabled={disableSaveButtonHandler || disableSave}
      />
      {/* -----Popups----- */}
      <ConfirmationPopup
        isOpen={confirmationPopup}
        loading={isAddLoading}
        message={`
Are You Sure To ${activePage === 3 ? "Edit Your" : "Add"} Data?`}
        submessage="You cannot undo this action"
        onContinue={submitHandler}
        handleClose={closeConfirmationHandler}
      />
      <SuccessConfirmation
        isOpen={isAddSuccess}
        handleClose={clickBackHandler}
      />
    </Grid>
  );
}
