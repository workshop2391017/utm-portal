import React, { Fragment, useState, useEffect, useMemo, memo } from "react";
import { useSelector, useDispatch } from "react-redux";

import { Grid, Typography, makeStyles } from "@material-ui/core";

import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import AntdTextField from "components/BN/TextField/AntdTextField";

import Colors from "helpers/colors";
import formatNumber from "helpers/formatNumber";

import FormControlRadioGroup from "../FormControlRadioGroup";
import FormControlCheckboxes from "../FormControlCheckboxes";

import { useValidateLms } from "../../../../../../utils/helpers/validateLms";
import { handlevalidationDataLms } from "../../../../../../stores/actions/validatedatalms";

const useStyles = makeStyles({
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 15,
    letterSpacing: "0.01em",
  },
  error: {
    color: "#D14848",
  },
});

const convertAccountType = (accountType) => {
  switch (accountType) {
    case "CA":
      return "Current Account";
    case "SA":
      return "Saving Account";
    case "LN":
      return "Loan Account";
    case "CD":
      return "Deposit Account";

    default:
      break;
  }
};

const LeftSectionForm = (props) => {
  const {
    // states
    product,
    productCode,
    productName,
    accountType,
    savingType,
    allowedFor,
    depositType,
    maximumAge,
    // setters
    onChangeProduct,
    onChangeProductCode,
    onChangeSavingType,
    onChangeAllowedFor,
    onChangeDepositType,
    onChangeMaximumAge,
    onValasChange,
    // mappings
    products,
    productCodes,
    savingTypes,
    depositTypes,
    errorMessage,
  } = props;

  const {
    openAccountAllowed,
    sofEdeposito,
    isRegisterAllowed,
    isValasAllowed,
    sofAllowed,
  } = allowedFor;

  const classes = useStyles();
  const dispatch = useDispatch();
  const [productOptions, setProductOptions] = useState([]);
  const [productCodeOptions, setProductCodeOptions] = useState([]);
  const { isEdit } = useSelector(
    ({ productInformation }) => productInformation
  );

  // useEffect(() => {
  //   dispatch(
  //     handlevalidationDataLms({
  //       name: productName,
  //       code: "13",
  //       table: "50",
  //     })
  //   );
  // }, [productName]);

  useEffect(() => {
    if (products.length) setProductOptions(products);
  }, [products]);

  useEffect(() => {
    if (productCodes.length) setProductCodeOptions(productCodes);
  }, [productCodes]);

  useEffect(() => {
    onValasChange(!isValasAllowed);
  }, [isValasAllowed]);

  const checkboxes = useMemo(
    () => [
      {
        checked: openAccountAllowed,
        name: "openAccountAllowed",
        label: "Open Second Account",
      },
      {
        checked: sofEdeposito,
        name: "sofEdeposito",
        label: "SOF e-Deposito",
      },
      {
        checked: isRegisterAllowed,
        name: "isRegisterAllowed",
        label: "Registration",
      },
      {
        checked: isValasAllowed,
        name: "isValasAllowed",
        label: "Valas",
      },
      {
        checked: sofAllowed,
        name: "sofAllowed",
        label: "SOF",
      },
    ],
    [
      openAccountAllowed,
      sofEdeposito,
      isRegisterAllowed,
      isValasAllowed,
      sofAllowed,
    ]
  );

  const disabledSavingType =
    accountType === "LN" ||
    accountType === "CA" ||
    accountType === "CD" ||
    !product ||
    !productCode;

  const disabledDepositType =
    accountType === "LN" ||
    accountType === "CA" ||
    accountType === "SA" ||
    !product ||
    !productCode;

  const disabledMaximumAge = savingType !== "2" || !product || !productCode;

  const productHardcode = [
    {
      value: "DD",
      label: "DD",
      key: "DD",
    },
    {
      value: "CD",
      label: "CD",
      key: "CD",
    },
    {
      value: "LN",
      label: "LN",
      key: "LN",
    },
  ];

  return (
    <Fragment>
      {/* -----Product & Product Code Block----- */}
      <Grid container item style={{ marginBottom: 20 }}>
        <Grid item xs={6}>
          <Typography variant="subtitle1" component="label">
            Product :
          </Typography>
          <SelectWithSearch
            dropdownMatchSelectWidth
            value={product}
            options={productHardcode}
            onChange={onChangeProduct}
            disabled={isEdit}
            placeholder="Choose Product"
            style={{ width: "90%" }}
          />
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1" component="label">
            Product Code :
          </Typography>
          <SelectWithSearch
            disabled={isEdit}
            dropdownMatchSelectWidth
            value={productCode}
            options={productCodeOptions}
            onChange={onChangeProductCode}
            placeholder="Choose Product Code"
            style={{ width: "90%" }}
          />
        </Grid>
      </Grid>

      {/* -----Product Name & Account Type Block----- */}
      <Grid container item style={{ marginBottom: 20 }}>
        <Grid item xs={6}>
          <Grid container direction="column">
            <Typography variant="subtitle1" component="label">
              Product Name :
            </Typography>
            <span className={classes.value}>{productName}</span>
            <span className={classes.error}>{errorMessage}</span>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Grid container direction="column">
            <Typography variant="subtitle1" component="label">
              Account Type :
            </Typography>
            <span className={classes.value}>
              {convertAccountType(accountType)}
            </span>
          </Grid>
        </Grid>
      </Grid>

      {/* -----Saving Type Block----- */}
      <FormControlRadioGroup
        label="Savings Type :"
        value={savingType}
        options={savingTypes}
        onChange={onChangeSavingType}
        style={{ marginBottom: 20 }}
        disabled={disabledSavingType}
      />

      {/* -----Allowed For Block----- */}
      <FormControlCheckboxes
        label="Allowed For :"
        checkboxes={checkboxes}
        onChange={onChangeAllowedFor}
        xs={6}
        style={{ marginBottom: 20 }}
      />

      {/* -----Deposit Type Block----- */}
      <FormControlRadioGroup
        label="Deposit Type :"
        value={depositType?.toString()}
        options={depositTypes}
        onChange={onChangeDepositType}
        style={{ marginBottom: 20 }}
        disabled={disabledDepositType}
      />

      {/* -----Maximum Age Block----- */}
      <Typography variant="subtitle1" component="label">
        Maximum Age :
      </Typography>
      <AntdTextField
        value={formatNumber(maximumAge)}
        onChange={onChangeMaximumAge}
        style={{ width: "95%", marginBottom: 20 }}
        disabled={disabledMaximumAge}
        maxLength={3}
      />

      {/* -----Paragraph Block----- */}
      <Typography
        paragraph
        align="justify"
        variant="subtitle1"
        style={{
          color: Colors.dark.medium,
          width: "95%",
        }}
      >
        Maximum age serves for the type of time savings, which will affect the
        time period displayed on the customers side.
      </Typography>
    </Fragment>
  );
};

export default memo(LeftSectionForm);
