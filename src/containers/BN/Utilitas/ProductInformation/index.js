import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { IconButton, Typography, makeStyles } from "@material-ui/core";

import Title from "components/BN/Title";
import GeneralButton from "components/BN/Button/GeneralButton";
import Search from "components/BN/Search/SearchWithoutDropdown";
import TableICBB from "components/BN/TableIcBB";
import Badge from "components/BN/Badge";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import ConfirmationPopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

import { ReactComponent as PlusIcon } from "assets/icons/BN/plus.svg";
import { ReactComponent as RotateIcon } from "assets/icons/BN/rotate-ccw.svg";

import Colors from "helpers/colors";
import useDebounce from "utils/helpers/useDebounce";
import { formatAmountDot } from "utils/helpers";

import {
  actionAccountProductDelete,
  actionAccountProductGet,
  setIsEdit,
  setSuccess,
} from "stores/actions/productInformation";

import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import UtilitasProductInformationForm from "./Add";

const useStyles = makeStyles({
  section: {
    background: Colors.gray.ultrasoft,
    height: "100%",
  },
  header: {
    display: "flex",
    alignItems: "center",
    gap: 20,
  },
  main: { padding: 20 },
});

const convertProductCodeHandler = (code) => {
  switch (code) {
    case "LN":
      return "Loan Account";
    case "CD":
      return "Deposito Account";
    case "SA":
      return "Saving Account";
    case "CA":
      return "Current Account";

    default:
      break;
  }
};

const dataHeader = ({ openConfirmationHandler, editHandler, setRowData }) => [
  {
    title: "Product",
    render: (rowData) => (
      <div style={{ wordWrap: "break-word" }}>
        <Typography variant="subtitle1">{rowData.product}</Typography>
      </div>
    ),
  },
  {
    title: "Product Code",
    render: (rowData) => (
      <div style={{ wordWrap: "break-word" }}>
        <Typography variant="subtitle1">{rowData.productCode}</Typography>
      </div>
    ),
  },
  {
    title: "Product Name",
    render: (rowData) => (
      <div style={{ wordWrap: "break-word" }}>
        <Typography variant="subtitle1">{rowData.productName}</Typography>
      </div>
    ),
  },
  {
    title: "Account Type",
    render: (rowData) => (
      <div style={{ wordWrap: "break-word" }}>
        <Typography variant="subtitle1">
          {convertProductCodeHandler(rowData.accountType)}
        </Typography>
      </div>
    ),
  },
  {
    title: "Initial Deposit",
    render: (rowData) => {
      if (rowData.minInitialDeposit) {
        return (
          <div style={{ wordWrap: "break-word" }}>
            <Typography variant="subtitle1">{`${
              rowData.currencyCode ?? "IDR"
            } ${formatAmountDot(rowData.minInitialDeposit)}`}</Typography>
          </div>
        );
      }
      return <Typography variant="subtitle1">-</Typography>;
    },
  },
  {
    title: "Minimum Balance",
    render: (rowData) => {
      if (rowData.minBalance) {
        return (
          <div style={{ wordWrap: "break-word" }}>
            <Typography variant="subtitle1">{`${
              rowData.currencyCode ?? "IDR"
            } ${formatAmountDot(rowData.minBalance)}`}</Typography>
          </div>
        );
      }
      return <Typography variant="subtitle1">-</Typography>;
    },
  },
  {
    title: "Status",
    render: (rowData) => (
      <div style={{ float: "left", clear: "left" }}>
        <Badge
          type={rowData.deleted ? "red" : "green"}
          label={rowData.deleted ? "Inactive" : "Active"}
          outline
        />
      </div>
    ),
  },
  {
    title: "",
    width: 50,
    render: (rowData) =>
      rowData.deleted ? (
        <IconButton
          onClick={() => {
            openConfirmationHandler();
            setRowData(rowData);
          }}
        >
          <RotateIcon />
        </IconButton>
      ) : (
        <GeneralMenu
          options={["edit", "inactive"]}
          onDelete={() => {
            openConfirmationHandler("DELETE");
            setRowData(rowData);
          }}
          onEdit={() => editHandler(rowData.id)}
        />
      ),
  },
];

export default function UtilitasProductInformation() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { data, isLoading, isAddLoading, isSuccess } = useSelector(
    ({ productInformation }) => productInformation
  );

  const { accountProductList, totalPages, totalElements } = data;

  const [confirmationPopup, setConfirmationPopup] = useState({
    type: undefined,
    open: false,
  });
  const [activePage, setActivePage] = useState(1);
  const [page, setPage] = useState(1);
  const [tableRows, setTableRows] = useState([]);
  const [rowId, setRowId] = useState(null);
  const [rowData, setRowData] = useState(null);
  const [searchValue, setSearchValue] = useState("");

  const searchDebounced = useDebounce(searchValue, 1000);
  useEffect(() => {
    if (searchDebounced) setPage(1);
  }, [searchDebounced]);

  const accountProductGetHandler = () =>
    dispatch(
      actionAccountProductGet({
        pageNumber: page - 1,
        totalPages: 10,
        searchValue: searchDebounced,
        sortBy: "productCode",
        direction: "asc",
      })
    );

  const openConfirmationHandler = (type) => {
    setConfirmationPopup({ type, open: true });
  };

  const closeConfirmationHandler = (type) => {
    setConfirmationPopup({ type, open: false });
  };

  const closeSuccessHandler = () => {
    dispatch(setSuccess(false));
    accountProductGetHandler();
  };

  const tableRowsHandler = (data) => setTableRows(data);

  const addHandler = () => {
    dispatch(setIsEdit(false));
    setActivePage(2);
  };

  const editHandler = (id) => {
    setRowId(id);
    setActivePage(3);
    dispatch(setIsEdit(true));
  };
  const activeInactiveHandler = () => {
    const payload = {
      id: rowData.id,
      accountType: rowData.accountType,
      product: rowData.product,
      productName: rowData.productName,
      productCode: rowData.productCode,
      savingType: rowData.savingType,
      deleted: confirmationPopup.type === "DELETE",
      isRegisterAllowed: rowData.isRegisterAllowed,
      sofAllowed: rowData.sofAllowed,
      openAccountAllowed: rowData.openAccountAllowed,
      sofEdeposito: rowData.sofEdeposito,
      isValasAllowed: rowData.isValasAllowed,
      depositType: rowData.depositType,
      productInfoIdn: rowData.productInfoIdn,
      productInfoEng: rowData.productInfoEng,
      productBenefitIdn: rowData.productBenefitIdn,
      productBenefitEng: rowData.productBenefitEng,
      base64ProductImage: rowData.base64ProductImage,
      extensionImage: rowData.extensionImage,
      currencyCode: rowData.currencyCode,
      holdAmount: rowData.holdAmount,
      maximumAge: rowData.maximumAge,
      minBalance: rowData.minBalance,
      minInitialDeposit: rowData.minInitialDeposit,
      isShowToNasabah: rowData.showToNasabah,
    };

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PRODUCT_INFORMATION,
          id: rowId || null,
          code: rowData.productCode,
          name: rowData.productName,
          validate: true,
        },
        {
          async onContinue() {
            dispatch(
              actionAccountProductDelete(payload, closeConfirmationHandler)
            );
          },
          onError() {
            closeConfirmationHandler();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  useEffect(() => {
    if (activePage === 1) accountProductGetHandler();
  }, [searchDebounced, page, activePage]);

  useEffect(() => {
    if (accountProductList) {
      tableRowsHandler(accountProductList);
    } else {
      tableRowsHandler([]);
    }
  }, [accountProductList]);

  return activePage === 1 ? (
    <section className={classes.section}>
      <Title label="Product Information">
        <div className={classes.header}>
          <Search
            placeholder="Product Name, Product Code"
            placement="bottom"
            style={{ width: 265 }}
            dataSearch={searchValue}
            setDataSearch={setSearchValue}
          />
          <GeneralButton
            label="Add Product Information"
            iconPosition="startIcon"
            buttonIcon={<PlusIcon />}
            width={261}
            height={44}
            onClick={addHandler}
          />
        </div>
      </Title>

      <main className={classes.main}>
        <TableICBB
          headerContent={dataHeader({
            editHandler,
            openConfirmationHandler,
            setRowData,
          })}
          dataContent={tableRows}
          page={page}
          setPage={setPage}
          totalData={totalPages}
          totalElement={totalElements}
          isLoading={isLoading}
        />
      </main>

      {/* -----Popups----- */}
      <ConfirmationPopup
        isOpen={confirmationPopup.open}
        loading={isAddLoading}
        message={`Are You Sure to ${
          confirmationPopup.type === "DELETE" ? "Inactive the" : "Active"
        } Data?`}
        submessage="You cannot undo this action"
        onContinue={activeInactiveHandler}
        handleClose={closeConfirmationHandler}
      />
      <SuccessConfirmation
        isOpen={isSuccess}
        handleClose={closeSuccessHandler}
        message={`${
          confirmationPopup.type === "DELETE" ? "Deleted" : "Saved"
        } Successfully`}
      />
    </section>
  ) : (
    <UtilitasProductInformationForm
      activePage={activePage}
      onGoBack={setActivePage}
      rowId={rowId}
    />
  );
}
