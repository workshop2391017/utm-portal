import { makeStyles } from "@material-ui/styles";
import MenuListCM from "components/BN/MenuList/MenuListContentManagement";
import TableCard from "components/BN/TableIcBB/TableCard";
import Title from "components/BN/Title";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import { useDispatch, useSelector } from "react-redux";
import {
  handleManagementContentGet,
  setContent,
} from "stores/actions/contentManagement";
import GeneralButton from "components/BN/Button/GeneralButton";
import moment from "moment";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import ContentManagementEdit from "./edit";

const useStyles = makeStyles({
  page: {
    height: "100%",
    padding: "20px",
  },
  container: {
    display: "flex",
  },
  content: {
    width: "100%",
  },
  search: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "white",
    height: 74,
    marginBottom: 10,
    padding: "17px 15px",
  },
});

const menuOptions = [
  {
    key: "1",
    name: "TNC",
  },
  {
    key: "2",
    name: "Information Page",
  },
  // {
  //   key: "2",
  //   name: "Receipt",
  // },
  // {
  //   key: "3",
  //   name: "On Boarding",
  // },
  // {
  //   key: "4",
  //   name: "Statement Letter",
  // },
];

const ContentAccess = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [selectedKey, setSelectedKey] = useState(1);
  const { data, isLoading, content } = useSelector((e) => e.contentManagement);
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const [onSearch, setOnSearch] = useState("");

  useEffect(() => {
    const category = [
      "TNC",
      "INFORMATION_PAGE",
      // "RECEIPT",
      // "ON_BOARDING",
      // "STATEMENT_LETTER",
    ];
    dispatch(
      handleManagementContentGet({
        category: category[selectedKey - 1],
        code: null,
        name: onSearch,
        page: page - 1,
        size: 10,
      })
    );
  }, [selectedKey, onSearch, page]);

  const goEdit = (item) => {
    dispatch(setContent(item));
    history.push(pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT_EDIT);
  };

  return (
    <div>
      <Title label="Content Management" />
      <div className={classes.page}>
        <div className={classes.container}>
          <div style={{ marginRight: 33 }}>
            <MenuListCM
              withHeader
              headerTitle="Category"
              mode="vertical"
              menuOptions={menuOptions}
              expandIcon={() => null}
              selectedKeys={[`${selectedKey}`]}
              onTitleClick={(e, key, idx) => setSelectedKey(idx + 1)}
            />
          </div>
          <div className={classes.content}>
            {selectedKey === null ? (
              <div
                style={{
                  height: 600,
                  textAlign: "center",
                  backgroundColor: "white",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "100%",
                }}
              >
                <div>
                  <div
                    style={{
                      fontFamily: "FuturaHvBT",
                      color: "#7B87AF",
                      fontSize: 17,
                    }}
                  >
                    You Haven&apos;t Selected a Category
                  </div>
                  <div
                    style={{
                      fontFamily: "FuturaBkBT",
                      color: "#7B87AF",
                      fontSize: 13,
                    }}
                  >
                    Choose a category first
                  </div>
                </div>
              </div>
            ) : (
              <React.Fragment>
                <div className={classes.search}>
                  <SearchWithDropdown
                    dataSearch={search}
                    setDataSearch={setSearch}
                    placeholder="Feature"
                  />
                  <GeneralButton
                    label="Apply"
                    onClick={() => setOnSearch(search)}
                  />
                </div>

                <TableCard
                  type="contentManagement"
                  background
                  data={(data?.listContentManagement ?? []).map((elm) => ({
                    ...elm,
                    title: elm.name,
                    keyID: "value",
                    keyEN: "value",
                    createdDate: moment(elm.modifiedDate).format("DD/MM/YYYY"),
                  }))}
                  // onDelete={() => {
                  //   console.warn("testing");
                  //   // setModalYakin(true);
                  // }}
                  onEdit={(item) => goEdit(item)}
                  totalData={data?.totalPages}
                  page={page}
                  setPage={setPage}
                  totalElement={data?.totalElements}
                  loading={isLoading}
                />
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

ContentAccess.propTypes = {};

export default ContentAccess;
