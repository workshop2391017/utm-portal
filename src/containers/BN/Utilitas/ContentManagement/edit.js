import { makeStyles } from "@material-ui/styles";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import { Button } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeleteConfirmation from "components/BN/Popups/Delete";
import SuccessAddConfirmation from "components/BN/Popups/SuccessConfirmation";
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import RichText from "components/BN/RichText";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  handleManagementContentSave,
  setSuccessSave,
  setContent,
} from "stores/actions/contentManagement";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import RichTextSunEditor from "components/BN/RichTextSunEditor";
import { validateTask } from "../../../../stores/actions/validateTaskPortal";

const useStyle = makeStyles({
  container: {
    width: 696,
    backgroundColor: "white",
    padding: 30,
    borderRadius: 20,
    margin: "0px auto",
    marginBottom: 100,
  },
  backButton: {
    textTransform: "none",
    fontFamily: "FuturaMdBT",
    fontWeight: 700,
    fontSize: 13,
    letterSpacing: "0.03em",
    color: "#0061A7",
  },
  pageTitle: {
    padding: "26px 20px 46px 20px",
    fontFamily: "FuturaHvBT",
    color: Colors.dark.hard,
    fontSize: 20,
    display: "flex",
    flexDirection: "column",
  },
  titleContainer: {
    marginBottom: 30,
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 17,
    color: Colors.dark.hard,
  },
  subTitle: {
    fontFamily: "FuturaBkBT",
    fontSize: 14,
    color: Colors.dark.hard,
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "white",
    padding: 20,
    position: "fixed",
    left: 200,
    right: 0,
    bottom: 0,
    zIndex: 99,
  },
  textEditorLanguageTitle: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    color: "#000000",
    marginTop: 20,
    textAlign: "left",
    marginBottom: 5,
  },
  label: {
    color: "#808080",
    fontFamily: "FuturaBkBT",
    fontSize: 10,
  },
  value: {
    color: "#374062",
    fontFamily: "FuturaHvBT",
    fontSize: 13,
  },
});

const ContentManagementEdit = () => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const history = useHistory();
  const { content } = useSelector((e) => e.contentManagement);
  const [isModalSuccess, setModalSuccess] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [disable, setDisable] = useState(true);

  const handleSave = () => {
    setConfirmation(false);
    const {
      page,
      size,
      keyID,
      keyEN,
      title,
      editionaldata,
      createdDate,
      modifiedBy,
      modifiedDate,
      createdBy,
      ...rest
    } = content;
    //   content;

    const payload = {
      ...rest,
      additionaldata: editionaldata,
    };
    dispatch(
      validateTask(
        {
          id: content?.id,
          menuName: "166",
        },
        {
          onContinue() {
            dispatch(
              handleManagementContentSave(payload, () => {
                setModalSuccess(true);
              })
            );
          },
          onError() {
            setConfirmation(false);
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const handleClose = () => {
    setModalSuccess(false);
    dispatch(setContent({}));
    history.push(pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT);
  };
  const handleDisable = () => {
    if (content?.value?.length - 8 === 0) setDisable(true);
    else setDisable(false);
  };

  useEffect(() => {
    handleDisable();
  }, [content]);

  const handleCancel = () => {
    dispatch(setContent({}));
    history.push(pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT);
  };

  useEffect(() => {
    if (!content?.id) {
      window.location.replace(pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT);
    }
  }, []);

  return (
    <div>
      <div className={classes.pageTitle}>
        <Button
          startIcon={<ArrowLeft />}
          onClick={() => handleCancel()}
          className={classes.backButton}
          style={{ width: "10px" }}
        >
          Back
        </Button>
        Edit Content Management Text
      </div>
      <div className={classes.container}>
        <div className={classes.titleContainer}>
          <div className={classes.title}>Form Edit Content Management</div>
          <div className={classes.subTitle}>
            Please change data content management
          </div>
        </div>
        <div className={classes.title}>{content?.name}</div>
        <div style={{ display: "flex", gap: "10px", marginTop: 10 }}>
          <div>
            <div className={classes.label}>Code :</div>
            <div className={classes.value}>{content?.code}</div>
          </div>
          <div>
            <div className={classes.label}>Feature :</div>
            <div className={classes.value}>{content?.category}</div>
          </div>
        </div>
        {/* <div className={classes.textEditorLanguageTitle}>
          Indonesian Text Editor
        </div>
        <RichText
          formName="update"
          value={content.value}
          onChange={(e) => {
            dispatch(
              setContent({
                ...content,
                value: e,
              })
            );
          }}
        /> */}
        <div className={classes.textEditorLanguageTitle}>Text Editor :</div>
        {/* <RichText
          placeholder="Enter text editor"
          formName="update"
          value={content.value}
          onChange={(e) => {
            console.log("+++ e", e);
            dispatch(
              setContent({
                ...content,
                value: e,
              })
            );
          }}
        /> */}
        <RichTextSunEditor
          content={content.value}
          onChange={(e) => {
            dispatch(
              setContent({
                ...content,
                value: e,
              })
            );
          }}
        />
      </div>

      <div className={classes.buttonGroup}>
        <ButtonOutlined label="Cancel" onClick={() => handleCancel()} />
        <GeneralButton
          label="Save"
          onClick={() => setConfirmation(true)}
          disabled={disable}
        />
      </div>

      <DeleteConfirmation
        message="Are You Sure To Edit Your Data?"
        isOpen={confirmation}
        handleClose={() => {
          setConfirmation(false);
        }}
        onContinue={handleSave}
      />

      <SuccessAddConfirmation
        isOpen={isModalSuccess}
        handleClose={handleClose}
      />
    </div>
  );
};

export default ContentManagementEdit;
