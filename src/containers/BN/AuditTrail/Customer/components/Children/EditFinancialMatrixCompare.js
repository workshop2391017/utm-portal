import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Colors from "helpers/colors";
import { Card, Grid } from "@material-ui/core";
import { ReactComponent as ChevRight } from "assets/icons/BN/chevron-right.svg";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import Collapse from "components/BN/Collapse/bordered";
import ModalViewServices from "./ModalViewServiceTypes";

const useStyles = makeStyles(() => ({
  card: {
    borderRadius: 10,
    padding: 20,
    border: "1px solid #B3C1E7",
    position: "relative",
  },
  payeLable: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  label: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.medium,
    marginBottom: 10,
  },
  value: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  groupLabel: {
    fontFamily: "FuturaHvBT",
    fontSize: 17,
    fontWeight: 400,
    color: Colors.dark.hard,
    marginBottom: 10,
  },
  currency: {
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    fontWeight: 400,
    color: "#0061A7",
  },
  viewButton: {
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    fontWeight: 700,
    backgroundColor: "transparent",
    border: "none",
    color: "#0061A7",
    cursor: "pointer",
    padding: 0,
  },
  tableContainer: {
    display: "block",
    position: "relative",
    overflow: "auto",
    maxHeight: 220,
    "&::-webkit-scrollbar": {
      width: 7,
    },
    "&::-webkit-scrollbar-track": {
      background: "#F4F7FB",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb": {
      background: "#0061A7",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb:hover": {
      background: "#004a80",
    },
  },
}));

const dataHeader = [
  {
    width: "25%",
    title: "Seq",
    key: "seq",
    render: (e) => e?.seq || "-",
  },
  {
    title: "User",
    key: "user",
    render: (e) => e?.user || "-",
  },
  {
    title: "Approval Level",
    key: "approvalLevel",
    render: (e) => e?.approvalLevel || "-",
  },
  {
    title: "Group",
    key: "group",
    render: (e) => e?.group || "-",
  },
  {
    title: "Group Target",
    key: "groupTarget",
    render: (e) => e?.target || "-",
  },
];

function EditFinancialMatrixCompare({ data }) {
  const classes = useStyles();

  const [openViewServices, setOpenViewServices] = useState(false);

  return (
    <div>
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Card elevation={0} className={classes.card}>
            <Grid
              container
              justifyContent="space-between"
              style={{ position: "relative" }}
            >
              <Grid item xs={6}>
                <Grid container direction="column" spacing={1}>
                  <Grid item>
                    <h2 className={classes.payeLable}>
                      {data?.paymentServices?.paymentName}
                    </h2>
                  </Grid>
                  <Grid item>
                    <p className={classes.currency}>
                      {data?.paymentServices?.paymentCurrency}
                    </p>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={6} style={{ textAlign: "right" }}>
                <h4 className={classes.label}>Type of Service Regulated:</h4>
                <p className={classes.value}>
                  {data?.paymentServices?.totalRegulatedServices || "-"}{" "}
                  Services
                </p>
                <button
                  className={classes.viewButton}
                  onClick={() => setOpenViewServices(true)}
                >
                  <Grid container alignItems="center">
                    <Grid item>View All Service Types</Grid>
                    <Grid item>
                      <ChevRight style={{ position: "relative", top: 3 }} />
                    </Grid>
                  </Grid>
                </button>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item>
          <h3 className={classes.groupLabel}>Matrix Approval List</h3>
        </Grid>
        <Grid item className={classes.tableContainer}>
          <Grid container direction="column" spacing={2}>
            {data?.matrixApprovalList.map((item) => (
              <Grid item>
                <Collapse
                  label={item.limit}
                  desc={`Number of Approvals : ${item?.dataTable?.length}`}
                >
                  <Grid container direction="column">
                    <Grid item>
                      <TableMenuBaru
                        headerContent={dataHeader}
                        dataContent={item?.dataTable ?? []}
                      />
                    </Grid>
                  </Grid>
                </Collapse>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>

      <ModalViewServices
        isOpen={openViewServices}
        data={data?.paymentServices || []}
        handleClose={() => setOpenViewServices(false)}
      />
    </div>
  );
}

export default EditFinancialMatrixCompare;
