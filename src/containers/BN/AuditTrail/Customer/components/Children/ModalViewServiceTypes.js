// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { Backdrop, Fade, Modal, makeStyles, Grid } from "@material-ui/core";

// assets
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import { ReactComponent as InfoAlert } from "assets/icons/BN/megaphone.svg";
import Badge from "components/BN/Badge";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& ::selection": {
      background: "#137EE1",
    },
  },
  paper: {
    width: 480,
    backgroundColor: "#fff",
    border: "none",
    padding: 20,
    alignItems: "center",
    borderRadius: 10,
    maxHeight: 450,
  },
  xContainer: {
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    color: Colors.dark.hard,
    marginBottom: 10,
    textAlign: "center",
  },
  content: {
    marginTop: 20,
    marginBottom: 20,
    display: "block",
    position: "relative",
    overflow: "auto",
    height: 250,
    paddingRight: 10,
    "&::-webkit-scrollbar": {
      width: 7,
    },
    "&::-webkit-scrollbar-track": {
      background: "#F4F7FB",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb": {
      background: "#0061A7",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb:hover": {
      background: "#004a80",
    },
  },
  subTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  footer: {
    width: "100%",
    padding: 10,
    borderRadius: 5,
    border: "1px solid #0061A7",
    backgroundColor: "#EAF2FF",
  },
  footerDesc: {
    color: "#0061A7",
    fontFamily: "FuturaBkBT",
    fontSize: 10,
    fontWeight: 400,
  },
}));

const renderStatus = (status) => {
  switch (status) {
    case "Done":
      return (
        <Badge
          label="Done"
          type="blue"
          styleBadge={{ display: "inline-block" }}
          outline
        />
      );
    case "Waiting for approval":
      return (
        <Badge
          label="Waiting for approval"
          styleBadge={{ display: "inline-block" }}
          outline
        />
      );

    default:
      return "";
  }
};

// eslint-disable-next-line react/prop-types
const ModalViewServiceTypes = ({ isOpen, handleClose, data }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <h3 className={classes.title}>Service Type</h3>
              </Grid>
              <Grid item>
                <div className={classes.xContainer}>
                  <XIcon
                    className="close"
                    onClick={handleClose}
                    width={18}
                    height={18}
                  />
                </div>
              </Grid>
            </Grid>
            <Grid container justifyContent="space-between">
              <Grid item style={{ width: "100%" }}>
                <div className={classes.content}>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <h3 className={classes.subTitle}>{data?.paymentName}</h3>
                    </Grid>
                    {data?.serviceTypes?.map((item) => (
                      <Grid
                        item
                        style={{ paddingLeft: "6px, 15px, 6px, 15px" }}
                      >
                        <Grid
                          container
                          justifyContent="space-between"
                          style={{ paddingLeft: 20 }}
                        >
                          <Grid item>{item.type}</Grid>
                          <Grid item>{renderStatus(item.status)}</Grid>
                        </Grid>
                      </Grid>
                    ))}
                  </Grid>
                </div>
              </Grid>
              <Grid item style={{ width: "100%" }}>
                <div className={classes.footer}>
                  <Grid container spacing={1} alignItems="center">
                    <Grid item>
                      <InfoAlert />
                    </Grid>
                    <Grid item>
                      <p className={classes.footerDesc}>
                        All limits that have been set will be automatically
                        changed if they are reset here.
                      </p>
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

ModalViewServiceTypes.propTypes = {
  isOpen: PropTypes.bool,
  handleClose: PropTypes.func,
  data: PropTypes.array,
};

ModalViewServiceTypes.defaultProps = {
  isOpen: false,
  handleClose: () => {},
  data: [],
};

export default ModalViewServiceTypes;
