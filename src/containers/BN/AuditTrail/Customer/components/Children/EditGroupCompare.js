import React, { Fragment, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Colors from "helpers/colors";
import { Grid } from "@material-ui/core";
import { ReactComponent as ChevRight } from "assets/icons/BN/chevron-right.svg";
import AntdTabs from "components/BN/Tabs/AntdTabs";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import ModalViewUsers from "./ModalViewUsers";

const useStyles = makeStyles(() => ({
  groupLabel: {
    fontFamily: "FuturaHvBT",
    fontSize: 17,
    fontWeight: 400,
    color: Colors.dark.hard,
    marginBottom: 10,
  },
  label: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.medium,
    marginBottom: 10,
  },
  value: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  valueChip: {
    padding: "5px 10px 5px 10px",
    borderRadius: 5,
    border: "1px solid #0061A7",
    background: "#EAF2FF",
    color: "#0061A7",
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    fontWeight: 400,
    width: "max-content",
  },
  viewUsers: {
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    fontWeight: 700,
    backgroundColor: "transparent",
    border: "none",
    color: "#0061A7",
    cursor: "pointer",
  },
  tabContainer: {
    display: "block",
    position: "relative",
    overflow: "auto",
    maxHeight: 200,
    "&::-webkit-scrollbar": {
      width: 7,
    },
    "&::-webkit-scrollbar-track": {
      background: "#F4F7FB",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb": {
      background: "#0061A7",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb:hover": {
      background: "#004a80",
    },
  },
  labelTab: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  valueTab: {
    fontFamily: "FuturaBkBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
    marginTop: 5,
    marginBottom: 5,
  },
}));

const dataHeader = [
  {
    title: "Currency Matrix",
    key: "currencyMatrix",
    render: (e) => e?.currentMatrix || "-",
  },
  {
    title: "Max. Nominal/Day",
    key: "maxNominalDay",
    render: (e) => (
      <Grid container direction="column">
        <Grid item>
          {e?.maxNominalDay ? e?.maxNominalDay.toLocaleString("id-ID") : "-"}
        </Grid>
        <Grid item>
          <span style={{ fontFamily: "FuturaMdBT", color: "#7B87AF" }}>
            Max. Transaction :{" "}
          </span>
          {e?.maxTransactionDay
            ? e?.maxTransactionDay.toLocaleString("id-ID")
            : "-"}
        </Grid>
      </Grid>
    ),
  },
  {
    title: "Max. Transaction",
    key: "maxTransaction",
    render: (e) =>
      e?.maxTransaction ? e?.maxTransaction.toLocaleString("id-ID") : "-",
  },
  {
    title: "Min. Transaction",
    key: "minTransaction",
    render: (e) =>
      e?.minTransaction ? e?.minTransaction.toLocaleString("id-ID") : "-",
  },
];

function EditGroupCompare({ data }) {
  const classes = useStyles();

  const [openViewUsers, setOpenViewUsers] = useState(false);
  const [tabsValue, setTabsValue] = useState("1");
  return (
    <div>
      <h3 className={classes.groupLabel}>
        {data?.companyInfo?.company || "-"}
      </h3>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <h4 className={classes.label}>Account Group :</h4>
          <p className={classes.valueChip}>
            {data?.companyInfo?.accountGroup || "-"}
          </p>
        </Grid>
        <Grid item xs={6}>
          <h4 className={classes.label}>Group Code & Name :</h4>
          <p className={classes.value}>
            {data?.companyInfo?.groupCodeName || "-"}
          </p>
        </Grid>
        <Grid item xs={6}>
          <h4 className={classes.label}>Number of Users:</h4>
          <p className={classes.value}>
            {data?.companyInfo?.numberofUsers || "-"}
          </p>
          <button
            className={classes.viewUsers}
            onClick={() => setOpenViewUsers(true)}
          >
            <Grid container alignItems="center">
              <Grid item>View All Users</Grid>
              <Grid item>
                <ChevRight style={{ position: "relative", top: 3 }} />
              </Grid>
            </Grid>
          </button>
        </Grid>
      </Grid>
      <hr style={{ border: "1px solid #B3C1E7", marginTop: 20 }} />
      <div>
        <AntdTabs
          activeKey={tabsValue}
          onChange={setTabsValue}
          tabs={["User Features", "Limit List"]}
          style={{ marginTop: 20 }}
          tabPosition="left"
        />
      </div>
      <div className={classes.tabContainer}>
        {tabsValue === "1" ? (
          <div>
            {data?.featAndLimit?.userFeatures.map((item) => (
              <Grid container direction="column" style={{ marginBottom: 10 }}>
                <Grid item>
                  <p className={classes.labelTab}>{item.featureName}</p>
                </Grid>
                {item.features && (
                  <Fragment>
                    <Grid item>
                      <hr style={{ border: "0.5px solid #E6EAF3" }} />
                    </Grid>
                    {item.features.map((feature) => (
                      <Grid item style={{ paddingLeft: 10 }}>
                        <p className={classes.valueTab}>{feature}</p>
                      </Grid>
                    ))}
                  </Fragment>
                )}
              </Grid>
            ))}
          </div>
        ) : (
          <div>
            {data?.featAndLimit?.limitList.map((item) => (
              <Grid container direction="column" style={{ marginBottom: 10 }}>
                <Grid item style={{ marginTop: 10, marginBottom: 10 }}>
                  <p className={classes.labelTab}>{item.limitName}</p>
                </Grid>
                <Grid item>
                  <TableMenuBaru
                    headerContent={dataHeader}
                    dataContent={item?.dataTable ?? []}
                  />
                </Grid>
              </Grid>
            ))}
          </div>
        )}
      </div>

      <ModalViewUsers
        isOpen={openViewUsers}
        data={data?.users || []}
        handleClose={() => setOpenViewUsers(false)}
      />
    </div>
  );
}

export default EditGroupCompare;
