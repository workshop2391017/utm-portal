// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  Backdrop,
  Fade,
  Typography,
  Modal,
  makeStyles,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";

// components

// assets
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& ::selection": {
      background: "#137EE1",
    },
  },
  paper: {
    width: 480,
    backgroundColor: "#fff",
    border: "none",
    padding: 20,
    alignItems: "center",
    borderRadius: 10,
    height: 450,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: 400,
    color: Colors.dark.hard,
    marginBottom: 10,
    textAlign: "center",
  },
  tableContainer: {
    overflow: "hidden",
  },
  tableHead: {
    display: "table",
    width: "100%",
    tableLayout: "fixed",
  },
  tr: {
    backgroundColor: Colors.primary.hard,
    "& th:first-child": {
      borderRadius: "10px 0 0 0",
      borderRight: 0,
    },
    "& th:last-child": {
      borderRadius: "0 10px 0 0",
      borderLeft: 0,
    },
  },
  tableBody: {
    display: "block",
    position: "relative",
    overflow: "auto",
    maxHeight: 300,
    "&::-webkit-scrollbar": {
      width: 7,
    },
    "&::-webkit-scrollbar-track": {
      background: "#F4F7FB",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb": {
      background: "#0061A7",
      borderRadius: 10,
    },
    "&::-webkit-scrollbar-thumb:hover": {
      background: "#004a80",
    },
  },
  tableRow: {
    display: "table",
    width: "100%",
    tableLayout: "fixed",
  },
  headCell: {
    height: 36,
    fontWeight: 900,
    fontSize: 13,
    fontFamily: "FuturaMdBT",
    lineHeight: "20px",
    border: "none",
    backgroundColor: "#0061A7",
    color: "#ffff",
    padding: "5px 30px 5px 30px",
  },
  cellRow: {
    fontSize: 13,
    fontFamily: "FuturaBkBT",
    height: 46,
    fontWeight: 400,
    border: "none",
    padding: "15px 30px 15px 30px",
  },
}));

// eslint-disable-next-line react/prop-types
const ModalViewUsers = ({ isOpen, handleClose, data }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.xContainer}>
              <XIcon
                className="close"
                onClick={handleClose}
                width={18}
                height={18}
              />
            </div>
            <h3 className={classes.title}>User List</h3>
            <div className={classes.tableContainer}>
              <TableContainer>
                <Table size="small">
                  <TableHead className={classes.tableHead}>
                    <TableRow className={classes.tr}>
                      <TableCell className={classes.headCell}>Name</TableCell>
                      <TableCell className={classes.headCell}>Role</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody className={classes.tableBody}>
                    <React.Fragment>
                      {data.map((row, index) => (
                        <TableRow key={row.id} className={classes.tableRow}>
                          <TableCell
                            key={`fullname-${index}`}
                            className={classes.cellRow}
                          >
                            <Typography style={{ fontFamily: "FuturaHvBT" }}>
                              {row.fullname}
                            </Typography>
                            <Typography style={{ fontFamily: "FuturaBkBT" }}>
                              {row.id}
                            </Typography>
                          </TableCell>
                          <TableCell
                            key={`role-${index}`}
                            className={classes.cellRow}
                          >
                            {row.role}
                          </TableCell>
                        </TableRow>
                      ))}
                    </React.Fragment>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

ModalViewUsers.propTypes = {
  isOpen: PropTypes.bool,
  handleClose: PropTypes.func,
  data: PropTypes.array,
};

ModalViewUsers.defaultProps = {
  isOpen: false,
  handleClose: () => {},
  data: [],
};

export default ModalViewUsers;
