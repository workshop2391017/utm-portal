import React, { Fragment, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Colors from "helpers/colors";
import { Grid } from "@material-ui/core";
import { ReactComponent as ChevRight } from "assets/icons/BN/chevron-right.svg";
import AntdTabs from "components/BN/Tabs/AntdTabs";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";

const useStyles = makeStyles(() => ({
  groupLabel: {
    fontFamily: "FuturaHvBT",
    fontSize: 17,
    fontWeight: 400,
    color: Colors.dark.hard,
    marginBottom: 20,
  },
  label: {
    fontFamily: "FuturaHvBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.neutral.gray.medium,
    marginBottom: 5,
  },
  value: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
}));

function EditLevelCompare({ data }) {
  const classes = useStyles();
  return (
    <div>
      <h3 className={classes.groupLabel}>{data?.level || "-"}</h3>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <h4 className={classes.label}>Currency :</h4>
          <p className={classes.value}>{data?.currency || "-"}</p>
        </Grid>
        <Grid item xs={6}>
          <h4 className={classes.label}>Limit Maker :</h4>
          <p className={classes.value}>
            {data?.limitMaker ? data?.limitMaker.toLocaleString("en-US") : "-"}
          </p>
        </Grid>
      </Grid>
      <hr
        style={{ border: "1px solid #B3C1E7", marginTop: 20, marginBottom: 20 }}
      />
      <h3 className={classes.groupLabel}>Approval Limit</h3>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <h4 className={classes.label}>Single Limit Approval :</h4>
          <p className={classes.value}>
            {data?.singleLimit
              ? data?.singleLimit.toLocaleString("en-US")
              : "-"}
          </p>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <h4 className={classes.label}>Same Group Approval Limit:</h4>
          <p className={classes.value}>
            {data?.sameGroupLimit
              ? data?.sameGroupLimit.toLocaleString("en-US")
              : "-"}
          </p>
        </Grid>
        <Grid item xs={6}>
          <h4 className={classes.label}>
            Limit Different Group Approval Limits:
          </h4>
          <p className={classes.value}>
            {data?.diffGroupLimit
              ? data?.diffGroupLimit.toLocaleString("en-US")
              : "-"}
          </p>
        </Grid>
      </Grid>
    </div>
  );
}

export default EditLevelCompare;
