import React from "react";
import { makeStyles } from "@material-ui/styles";
import Colors from "helpers/colors";

const useStyles = makeStyles(() => ({
  column: {
    display: "flex",
    flexDirection: "column",
  },
}));

function NoData() {
  const classes = useStyles();
  return (
    <div
      className={classes.column}
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "90%",
      }}
    >
      <h3
        style={{
          fontFamily: "FuturaMdBT",
          fontWeight: 400,
          fontSize: 20,
          color: Colors.dark.medium,
        }}
      >
        No Data
      </h3>
      <p
        style={{
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          fontSize: 13,
          color: Colors.dark.medium,
        }}
      >
        Not displaying old data
      </p>
    </div>
  );
}

export default NoData;
