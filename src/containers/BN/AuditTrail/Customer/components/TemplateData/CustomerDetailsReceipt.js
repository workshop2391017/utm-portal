import {
  Card,
  Grid,
  makeStyles,
  Button,
  CardHeader,
  CardContent,
} from "@material-ui/core";
import React, { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Badge from "components/BN/Badge";
import arrowLeft from "assets/icons/BN/arrow-left.svg";
import { setPagesMass } from "stores/actions/auditTrail";
import Colors from "helpers/colors";

const useStyles = makeStyles({
  card: {
    paddingTop: 20,
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 76,
    marginBottom: 20,
  },
  title: {
    backgroundColor: "transparent",
    fontSize: 20,
    fontWeight: 700,
    fontFamily: "FuturaHvBT",
    color: "#2B2F3C",
  },
  cardItem: {
    height: "100%",
    minHeight: 300,
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: 0,
    height: "100%",
    minHeight: 300,
  },
  bannerTitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.01em",
    color: "#FFFFFF",
  },
  bannerText: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#FFFFFF",
  },
  leftContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  leftContentName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumberTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumber: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  divider: {
    border: "1px solid #E6EAF3",
    borderBottom: 0,
  },
  rightContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  rightContentValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  rightContentDivider: {
    border: "1px dashed #B3C1E7",
    borderBottom: 0,
  },
  totalTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#0061A7",
  },
  totalValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#0061A7",
  },
});
const dataDummy = [
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
];
const CustomerDetailsReceipt = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { pagesMass } = useSelector((state) => state.auditTrail);
  // nanti dibersihin
  const Banner = ({ title, dateTime }) => (
    <Grid item>
      <Grid
        container
        direction="column"
        spacing={1}
        style={{
          padding: "16px 26px",
          background:
            title === "Successful Transaction"
              ? "#75D37F"
              : title === "Transaction Is Being Processed"
              ? "#FFA24B"
              : title === "Waiting for Approval"
              ? "#FFA24B"
              : title === "Transaction Failed"
              ? "#FF6F6F"
              : undefined, // nanti pakai ternary
        }}
      >
        <Grid item className={classes.bannerTitle}>
          {title}
        </Grid>
        <Grid item className={classes.bannerText}>
          {dateTime}
        </Grid>
      </Grid>
    </Grid>
  );

  // nanti dibersihin
  const LeftContent = ({ props }) => {
    const { source, destination } = props; // nanti disesuaikan

    return (
      <Fragment>
        {/* -----Source----- */}
        {source.length !== 0
          ? source.map((el, i) => (
              <Grid
                key={`${el?.name}${i}`}
                container
                direction="column"
                spacing={1}
                style={{ padding: "0px 15px" }}
              >
                {i === 0 && (
                  <Grid item className={classes.leftContentTitle}>
                    Source of Funds Account :
                  </Grid>
                )}
                <Grid item className={classes.leftContentName}>
                  {el?.name}
                </Grid>
                <Grid item>
                  <Grid container justifyContent="space-between">
                    <Grid item className={classes.accountNumberTitle}>
                      Account Number
                    </Grid>
                    <Grid item className={classes.accountNumber}>
                      {el?.accountNumber}
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <hr className={classes.divider} />
                </Grid>
              </Grid>
            ))
          : null}

        {/* -----Destination----- */}
        {destination.length !== 0
          ? destination.map((el, i) => (
              <Grid
                key={`${el?.name}${i}`}
                container
                direction="column"
                spacing={1}
                style={{ padding: "0px 15px" }}
              >
                {i === 0 && (
                  <Grid item className={classes.leftContentTitle}>
                    Destination Account:
                  </Grid>
                )}
                <Grid item className={classes.leftContentName}>
                  {el?.name}
                </Grid>
                <Grid item>
                  <Grid container justifyContent="space-between">
                    <Grid item className={classes.accountNumberTitle}>
                      Account Number
                    </Grid>
                    <Grid item className={classes.accountNumber}>
                      {el?.accountNumber}
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <hr className={classes.divider} />
                </Grid>
              </Grid>
            ))
          : null}
      </Fragment>
    );
  };

  // nanti dibersihin
  const RightContent = () => (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Transaction ID
          </Grid>
          <Grid item className={classes.rightContentValue}>
            ID7439201
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Ref No
          </Grid>
          <Grid item className={classes.rightContentValue}>
            4215710
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Transaction Type
          </Grid>
          <Grid item className={classes.rightContentValue}>
            Pindah Dana
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Transfer Options
          </Grid>
          <Grid item className={classes.rightContentValue}>
            <Badge type="blue" label="Berulang Bulanan" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Start From
          </Grid>
          <Grid item className={classes.rightContentValue}>
            12/12/2020
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Ended On
          </Grid>
          <Grid item className={classes.rightContentValue}>
            12/11/2021
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Nominal Transfer
          </Grid>
          <Grid item className={classes.rightContentValue}>
            2.500.000,00
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Transaction Fee
          </Grid>
          <Grid item className={classes.rightContentValue}>
            65.000,00
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Transaction ID
          </Grid>
          <Grid item className={classes.rightContentValue}>
            ID7439201
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Admin Fee
          </Grid>
          <Grid item className={classes.rightContentValue}>
            6.500,00
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            Total Cost
          </Grid>
          <Grid item className={classes.rightContentValue}>
            6.500,00
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.rightContentTitle}>
            News
          </Grid>
          <Grid item className={classes.rightContentValue}>
            Pembayaran SPP Bulan Juni
          </Grid>
        </Grid>
      </Grid>
      <Grid item style={{ padding: "0px 24px" }}>
        <hr className={classes.rightContentDivider} />
      </Grid>
      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.totalTitle}>
            Total Transaction
          </Grid>
          <Grid item className={classes.totalValue}>
            7.578.000,00
          </Grid>
        </Grid>
      </Grid>
      <Grid container justifyContent="center" item>
        <Badge
          type="blue"
          label="This receipt is a valid proof of transaction."
          styleBadge={{ width: 630, textAlign: "center" }}
        />
      </Grid>
    </Grid>
  );
  return (
    <Grid className={classes.card} spacing={2} container direction="column">
      <Grid xs={12} item>
        <header className={classes.header}>
          <div>
            <Button
              onClick={() => dispatch(setPagesMass(1))}
              startIcon={<img src={arrowLeft} alt="Back" />}
              style={{
                fontFamily: "FuturaMdBT",
                fontWeight: 700,
                fontSize: 13,
                color: "#0061A7",
                textTransform: "capitalize",
              }}
            >
              Back
            </Button>

            <div className={classes.title}>Transaction Receipt Details</div>
          </div>
        </header>
      </Grid>
      <Grid xs={12} item>
        <Card elevation={0} className={classes.cardItem}>
          <CardContent className={classes.cardContent}>
            <Grid container direction="column" spacing={3}>
              {/* -----Banner----- */}
              <Banner
                title="Successful Transaction"
                dateTime="12/09/2021|12:04:15"
              />

              {/* -----Main Content----- */}
              <Grid item>
                <Grid container>
                  {/* -----Left Content----- */}
                  <Grid item xs={4}>
                    <LeftContent
                      // nanti dibersihin
                      props={{
                        source: [
                          {
                            name: "Adam Wahyudi",
                            accountNumber: "20044****8192",
                          },
                          {
                            name: "Adam Wahyudi 2",
                            accountNumber: "20044****8192",
                          },
                        ],
                        destination: [
                          {
                            name: "Lili Aryanti",
                            accountNumber: "2003351227017",
                          },
                          {
                            name: "Lili Aryanti 2",
                            accountNumber: "2003351227017",
                          },
                        ],
                      }}
                    />
                  </Grid>

                  {/* -----Right Content----- */}
                  <Grid item xs={8}>
                    <RightContent />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default CustomerDetailsReceipt;
