import React, { Fragment, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Box,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";
import { shallowEqual, useSelector } from "react-redux";
import { formatMoneyRMG, standardDateAuditSaving } from "utils/helpers";
import moment from "moment";

const useStyles = makeStyles({
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardItem: {
    height: "100%",
    minHeight: 300,
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: "0 20px",
    height: "100%",
    minHeight: 300,
  },
  divider: {
    border: "1px solid #E6EAF3",
    borderBottom: 0,
  },
  itemSectionName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "16px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  itemLabel: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  itemValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    color: "#374062",
  },
});

export default function OpeningSecondaryAccountDetails({ data }) {
  const classes = useStyles();

  const { dataOpeningSecondaryAccount } = useSelector(
    (state) => state.auditTrail,
    shallowEqual
  );

  const ItemField = ({ label, value, sectionName }) => (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: "8px",
        height: "100%",
        justifyContent: "flex-end",
      }}
    >
      {sectionName ? (
        <Typography variant="body1" className={classes.itemSectionName}>
          {sectionName}
        </Typography>
      ) : null}
      <Box display="flex" flexDirection="column" gap="5px">
        <Typography variant="body1" className={classes.itemLabel}>
          {label} :
        </Typography>
        <Typography variant="body1" className={classes.itemValue}>
          {value}
        </Typography>
      </Box>
    </Box>
  );

  const MainContent = ({ dataset }) => (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <ItemField
          sectionName="Owner Information"
          label="Full Name"
          value={dataset?.openingAccountData?.fullName || "-"}
        />
      </Grid>
      <Grid item container spacing={3}>
        <Grid item xs={6}>
          <ItemField
            sectionName="Source of Funds"
            label="Fund Source Account"
            value={
              dataset?.openingAccountData?.fullName &&
              dataset?.openingAccountData?.sourceAccountNumber
                ? `${dataset?.openingAccountData?.fullName} - ${dataset?.openingAccountData?.sourceAccountNumber}`
                : "-"
            }
          />
        </Grid>
        <Grid item xs={6}>
          <ItemField
            label="Fund Balance"
            value={
              formatMoneyRMG(dataset?.openingAccountData?.fundBalance, "IDR") ||
              "-"
            }
          />
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        <Grid item xs={6}>
          <ItemField
            sectionName="Product Information"
            label="Product Type"
            value={dataset?.openingAccountData?.productName || "-"}
          />
        </Grid>
        <Grid item xs={6}>
          <ItemField
            label="Account Opening Date"
            value={standardDateAuditSaving(dataset?.transactionDate) || "-"}
          />
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        <Grid item xs={6}>
          <ItemField
            sectionName="Account Information"
            label="Full Name"
            value={dataset?.toAccountName || "-"}
          />
        </Grid>
        <Grid item xs={6}>
          <ItemField
            label="Account Number"
            value={dataset?.toAccountNumber || "-"}
          />
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        <Grid item xs={6}>
          {dataset?.openingAccountData?.installment ? (
            <ItemField
              sectionName="Monthly Deposit"
              label="Monthly Deposit"
              value={formatMoneyRMG(
                dataset?.openingAccountData?.installment,
                dataset?.openingAccountData?.currency
              )}
            />
          ) : null}
          {dataset?.openingAccountData?.amountDeposit ? (
            <ItemField
              sectionName="Initial Deposit"
              label="Initial Deposit"
              value={formatMoneyRMG(
                dataset?.openingAccountData?.amountDeposit,
                dataset?.openingAccountData?.currency
              )}
            />
          ) : null}
        </Grid>
        <Grid item xs={6}>
          {dataset?.openingAccountData?.monthTerm ? (
            <ItemField
              label="Time Period"
              value={
                parseInt(dataset?.openingAccountData?.monthTerm) % 12 === 0
                  ? `${parseInt(
                      dataset?.openingAccountData?.monthTerm / 12
                    )} Year`
                  : `${dataset?.openingAccountData?.monthTerm} Month`
              }
            />
          ) : null}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <Card elevation={0} className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Card elevation={0} className={classes.cardItem}>
            <CardHeader className={classes.cardHeader} title="Data" />
            <CardContent className={classes.cardContent}>
              <Grid container direction="column" spacing={3}>
                {/* -----Main Content----- */}
                <Grid item container>
                  <MainContent dataset={dataOpeningSecondaryAccount} />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}
