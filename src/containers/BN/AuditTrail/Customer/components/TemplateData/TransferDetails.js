import React, { Fragment, useEffect, useState } from "react";

import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Box,
  Typography,
} from "@material-ui/core";

import Badge from "components/BN/Badge";

import Colors from "helpers/colors";
import { shallowEqual, useSelector } from "react-redux";
import moment from "moment";
import { formatMoneyRMG } from "utils/helpers";

const useStyles = makeStyles({
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardItem: {
    height: "100%",
    minHeight: 300,
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: 0,
    height: "100%",
    minHeight: 300,
  },
  bannerTitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.01em",
    color: "#FFFFFF",
  },
  bannerText: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#FFFFFF",
  },
  leftContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  leftContentName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumberTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumber: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  divider: {
    border: "1px solid #E6EAF3",
    borderBottom: 0,
  },
  rightContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  rightContentValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  rightContentDivider: {
    border: "1px dashed #B3C1E7",
    borderBottom: 0,
  },
  totalTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#0061A7",
  },
  totalValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#0061A7",
  },
});

export default function TransferDetails({ data }) {
  const classes = useStyles();

  const [dataset, setDataset] = useState(null);

  const { dataResiTransfer: dataResi } = useSelector(
    (state) => state.auditTrail,
    shallowEqual
  );

  const scheduled = [
    {
      label: "At the date of",
      value: moment(dataResi?.scheduleDate, "DD/MM/YYYY | H:mm:ss").format(
        "DD/MM/YYYY"
      ),
    },
  ];

  const recur = [
    // {
    //   label: "Schedule",
    //   value:
    //     dataResi?.recurringCategory === "Harian"
    //       ? "Daily"
    //       : dataResi?.recurringCategory === "Mingguan"
    //       ? "Weekly"
    //       : dataResi?.recurringCategory === "Bulanan"
    //       ? "Monthly"
    //       : dataResi?.recurringCategory === "Triwulan"
    //       ? "Quarterly"
    //       : dataResi?.recurringCategory === "Semester"
    //       ? "Semesters"
    //       : dataResi?.recurringCategory === "Tahunan"
    //       ? "Annual"
    //       : "-",
    // },
    {
      label: "Starting From",
      value: moment(dataResi?.recurringStart, "DD/MM/YYYY | HH:mm:ss").format(
        "DD/MM/YYYY"
      ),
    },
    {
      label: "Ends On",
      value: moment(dataResi?.recurringEnd, "DD/MM/YYYY | HH:mm:ss").format(
        "DD/MM/YYYY"
      ),
    },
  ];

  useEffect(() => {
    let selectedSweep = [];
    let badge;
    if (
      dataResi?.transactionChoice === null ||
      String(dataResi?.transactionChoice).toLowerCase() === "saat ini"
    ) {
      badge = { type: "green", label: "Immediately" };
    } else if (
      String(dataResi?.transactionChoice).toLowerCase() === "terjadwal" ||
      String(dataResi?.transactionChoice).toLowerCase() === "tanggal tertentu"
    ) {
      selectedSweep = scheduled;
      badge = { type: "orange", label: "Future Date" };
    } else {
      selectedSweep = recur;
      badge = {
        type: "blue",
        label:
          dataResi?.recurringCategory === "Harian"
            ? "Daily Recurring"
            : dataResi?.recurringCategory === "Mingguan"
            ? "Weekly Recurring"
            : dataResi?.recurringCategory === "Bulanan"
            ? "Monthly Recurring"
            : dataResi?.recurringCategory === "Triwulan"
            ? "Quarterly Recurring"
            : dataResi?.recurringCategory === "Semester"
            ? "Semesters Recurring"
            : dataResi?.recurringCategory === "Tahunan"
            ? "Annual Recurring"
            : "-",
      };
    }
    const rows = [
      {
        label: "Transaction ID",
        value: dataResi.transactionId,
      },
      {
        label: "Ref No",
        value: dataResi.retrievalReferenceNumber,
      },
      {
        label: "Transfer Category",
        value:
          dataResi?.transactionCategory === "Pindah Dana"
            ? "Own Account Transfer"
            : dataResi?.transactionCategory === "Transfer Bank Lain"
            ? "Domestic Bank Transfer"
            : dataResi?.transactionCategory === "Sesama Bank"
            ? "Sesama Transfer"
            : "-",
      },
      {
        label: "Transfer Options",
        value: (
          <Badge
            type={badge.type}
            label={badge.label}
            style={{ textTransform: "capitalize" }}
            outline
          />
        ),
      },
      ...selectedSweep,
      {
        label: "Transfer Amount",
        value: formatMoneyRMG(`${dataResi.transactionAmount}`),
      },
      {
        label: "Transaction Fee",
        value: formatMoneyRMG(`${dataResi.transactionFee}`),
      },
      {
        label: "Admin Fee",
        value: formatMoneyRMG(`${dataResi.adminFee}`),
      },
      {
        label: "Total Fee",
        value: formatMoneyRMG(`${dataResi.totalFee}`),
      },
      {
        label: "Remark",
        value: dataResi?.transactionNote,
      },
    ];

    if (dataResi?.transactionCategory === "Transfer Bank Lain") {
      rows.splice(3, 0, {
        label: "Transfer Type",
        value:
          dataResi?.transactionType === "ONLINE"
            ? "Online"
            : dataResi?.transactionType,
      });
    }

    setDataset(rows);
  }, [dataResi]);

  // nanti dibersihin
  const Banner = ({ title, dateTime }) => (
    <Grid item>
      <Grid
        container
        direction="column"
        spacing={1}
        style={{
          padding: "16px 26px",
          background:
            title === "SUCCESS"
              ? "#75D37F"
              : title === "SUSPECT"
              ? "#FFA24B"
              : title === "WAITING"
              ? "#FFA24B"
              : title === "FAILED" || title === "REJECTED"
              ? "#FF6F6F"
              : undefined, // nanti pakai ternary
        }}
      >
        <Grid item className={classes.bannerTitle}>
          {title === "SUCCESS" &&
          dataResi?.transactionChoice &&
          dataResi?.transactionChoice?.toString()?.toLowerCase() !== "saat ini"
            ? "Transaction Successful Scheduled"
            : title === "SUCCESS"
            ? "Transaction Successful"
            : title === "SUSPECT"
            ? "Transaction Processed"
            : title === "WAITING"
            ? "Transactions Awaiting Approval"
            : title === "FAILED"
            ? "Transaction Failed"
            : title === "REJECTED"
            ? "Transaction Rejected"
            : "-"}
        </Grid>
        <Grid item className={classes.bannerText}>
          {dateTime}
        </Grid>
      </Grid>
    </Grid>
  );

  const ItemField = ({ label, value }) => (
    <Grid item>
      <Grid
        container
        justifyContent="space-between"
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.rightContentTitle}>
          {label}
        </Grid>
        <Grid item className={classes.rightContentValue}>
          {value || "-"}
        </Grid>
      </Grid>
    </Grid>
  );

  // nanti dibersihin
  const LeftContent = ({ props }) => (
    <Fragment>
      {/* -----Source----- */}
      <Grid
        container
        direction="column"
        spacing={1}
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.leftContentTitle}>
          Source of Fund :
        </Grid>
        <Grid item className={classes.leftContentName}>
          {dataResi.fromAccountName || "-"}
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between">
            <Grid item className={classes.accountNumberTitle}>
              Account No.
            </Grid>
            <Grid item className={classes.accountNumber}>
              {dataResi.fromAccountNumber || "-"}
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <hr className={classes.divider} />
        </Grid>
      </Grid>

      {/* -----Destination----- */}
      <Grid
        container
        direction="column"
        spacing={1}
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.leftContentTitle}>
          Destination Account:
        </Grid>
        <Grid item className={classes.leftContentName}>
          {dataResi.toAccountName || "-"}
        </Grid>
        {dataResi?.toBankName !== "Bank" ? (
          <Grid item>
            <Grid container justifyContent="space-between">
              <Grid item className={classes.accountNumberTitle}>
                Bank Name
              </Grid>
              <Grid item className={classes.accountNumber}>
                {dataResi.toBankName || "-"}
              </Grid>
            </Grid>
          </Grid>
        ) : null}
        <Grid item>
          <Grid container justifyContent="space-between">
            <Grid item className={classes.accountNumberTitle}>
              Account No.
            </Grid>
            <Grid item className={classes.accountNumber}>
              {dataResi.toAccountNumber || "-"}
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <hr className={classes.divider} />
        </Grid>
      </Grid>
    </Fragment>
  );

  const RightContent = ({ dataset }) => (
    <Grid container direction="column" spacing={2}>
      {dataset &&
        dataset.map((item, index) => (
          <ItemField label={item.label} value={item.value} />
        ))}
      <Grid item style={{ padding: "0px 24px" }}>
        <hr className={classes.rightContentDivider} />
      </Grid>

      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.totalTitle}>
            Total Transactions
          </Grid>
          <Grid item className={classes.totalValue}>
            {formatMoneyRMG(`${dataResi.totalAmount}`) || "-"}
          </Grid>
        </Grid>
      </Grid>
      <Grid container justifyContent="center" item>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            width: "calc(100% - 24px)",
            color: "#66A3FF",
            backgroundColor: "#EAF2FF",
            border: "1px solid #66A3FF",
            borderRadius: "5px",
            padding: "10px",
          }}
        >
          <Typography variant="body">
            This receipt is valid proof of transaction.
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );

  return (
    <Card elevation={0} className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Card elevation={0} className={classes.cardItem}>
            <CardHeader className={classes.cardHeader} title="Transfer Data" />
            <CardContent className={classes.cardContent}>
              <Grid container direction="column" spacing={3}>
                {/* -----Banner----- */}
                <Banner
                  title={dataResi.transactionStatus}
                  dateTime={dataResi.transactionDate}
                />

                {/* -----Main Content----- */}
                <Grid item>
                  <Grid container>
                    {/* -----Left Content----- */}
                    <Grid item xs={4}>
                      <LeftContent />
                    </Grid>

                    {/* -----Right Content----- */}
                    <Grid item xs={8}>
                      <RightContent dataset={dataset} />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}
