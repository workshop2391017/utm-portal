import { Card, Grid, makeStyles, Button } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TableICBB from "components/BN/TableIcBB";
import Filter from "components/BN/Filter/GeneralFilter";
import GeneralButton from "components/BN/Button/GeneralButton";
import Badge from "components/BN/Badge";
import arrowLeft from "assets/icons/BN/arrow-left.svg";
import { setPagesMass } from "stores/actions/auditTrail";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";

const useStyles = makeStyles({
  card: {
    paddingTop: 20,
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 76,
    marginBottom: 20,
  },
  title: {
    backgroundColor: "transparent",
    fontSize: 20,
    fontWeight: 700,
    fontFamily: "FuturaHvBT",
    color: "#2B2F3C",
  },
});
const dataDummy = [
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
  {
    name: "Adam Wahyudi",
    rek: "20044****8192",
    toRek: "20044****8192",
    toName: "Galih Dantri Nugroho",
    status: "Success",
    nominal: "102.000.000.000,00",
  },
];
const CustomerDetailsTable = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { pagesMass } = useSelector((state) => state.auditTrail);
  const [dataFilter, setDataFilter] = useState(null);
  const [dataSearch, setDataSearch] = useState("");
  const [page, setPage] = useState(0);
  const tableConfig = [
    {
      title: "Rekening Sumber Dana",
      render: (rowData) => (
        <div
          style={{
            display: "flex",
            height: 70,
            flexDirection: "column",
            gap: 7,
            paddingTop: 12.5,
          }}
        >
          <span
            style={{ fontSize: 13, fontWeight: 400, fontFamily: "FuturaHvBT" }}
          >
            {rowData?.name}
          </span>
          <span
            style={{ fontSize: 13, fontWeight: 400, fontFamily: "FuturaBkBT" }}
          >
            {rowData?.rek}
          </span>
        </div>
      ),
    },
    {
      title: "Rekening Tujuan",
      render: (rowData) => (
        <div style={{ display: "flex", gap: 7, flexDirection: "column" }}>
          <span
            style={{ fontSize: 13, fontWeight: 400, fontFamily: "FuturaHvBT" }}
          >
            {rowData?.toName}
          </span>
          <span
            style={{ fontSize: 13, fontWeight: 400, fontFamily: "FuturaBkBT" }}
          >
            {rowData?.toRek}
          </span>
        </div>
      ),
    },
    {
      title: "Nominal Transfer",
      render: (rowData) => (
        <div style={{ display: "flex" }}>
          <span
            style={{ fontSize: 13, fontWeight: 400, fontFamily: "FuturaBkBT" }}
          >
            Rp {rowData?.nominal}
          </span>
        </div>
      ),
    },
    {
      title: "Status",
      headerAlign: "left",
      width: 300,
      align: "left",
      render: (rowData) => (
        <Badge
          label={
            rowData.status === "Failed"
              ? "Failed"
              : rowData.status === "Waiting"
              ? "Waiting"
              : "Success"
          }
          type={
            rowData.status === "Failed"
              ? "red"
              : rowData.status === "Waiting"
              ? "orange"
              : "green"
          }
        />
      ),
    },
    {
      title: "",
      width: 120,
      render: (rowData) => (
        <GeneralButton
          label="View Details"
          width={110}
          height={24}
          // disabled={!listArrayActivityDetail.includes(rowData.activityName)}
          onClick={() => dispatch(setPagesMass(2))}
          style={{
            fontFamily: "FuturaMdBT",
            fontSize: 10,
          }}
        />
      ),
    },
  ];
  return (
    <Grid className={classes.card} spacing={2} container direction="column">
      <Grid item>
        <header className={classes.header}>
          <div>
            <Button
              onClick={() => dispatch(setPagesMass(0))}
              startIcon={<img src={arrowLeft} alt="Back" />}
              style={{
                fontFamily: "FuturaMdBT",
                fontWeight: 700,
                fontSize: 13,
                color: "#0061A7",
                textTransform: "capitalize",
              }}
            >
              Back
            </Button>

            <div className={classes.title}>Transaction Receipt Details</div>
          </div>
          <SearchWithDropdown
            placeholder="User, Activity Name"
            placement="bottom"
            style={{ marginRight: 20, width: 240 }}
            dataSearch={dataSearch}
            setDataSearch={setDataSearch}
          />
        </header>
      </Grid>
      <Grid item>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={(e) => {
            setDataFilter(e);
          }}
          title="Show :"
          align="left"
          options={[
            {
              id: 1,
              type: "tabs",
              options: [
                "All",
                "Waiting for Approval",
                "Success",
                "Failed",
                "Processed",
                "Rejected",
              ],
            },
          ]}
        />
      </Grid>
      <Grid item>
        <TableICBB
          headerContent={tableConfig}
          dataContent={dataDummy}
          isLoading={false}
          totalElement={10}
          totalData={10}
        />
      </Grid>
    </Grid>
  );
};

export default CustomerDetailsTable;
