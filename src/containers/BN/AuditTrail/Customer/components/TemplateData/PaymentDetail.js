import React, { Fragment, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Box,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";
import { shallowEqual, useSelector } from "react-redux";
import { formatMoneyRMG } from "utils/helpers";

const useStyles = makeStyles({
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardItem: {
    height: "100%",
    minHeight: 300,
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: 0,
    height: "100%",
    minHeight: 300,
  },
  bannerTitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.01em",
    color: "#FFFFFF",
  },
  bannerText: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#FFFFFF",
  },
  leftContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  leftContentName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumberTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumber: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  divider: {
    border: "1px solid #E6EAF3",
    borderBottom: 0,
  },
  rightContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  rightContentValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  rightContentDivider: {
    border: "1px dashed #B3C1E7",
    borderBottom: 0,
  },
  totalTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#0061A7",
  },
  totalValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#0061A7",
  },
});

export default function PaymentDetail({ data }) {
  const classes = useStyles();

  const { dataResiPayment, dataMasterResiPayment } = useSelector(
    (state) => state.auditTrail,
    shallowEqual
  );

  // nanti dibersihin
  const Banner = ({ title, dateTime, message, errorCode }) => (
    <Grid item>
      <Grid
        container
        direction="column"
        spacing={1}
        style={{
          padding: "16px 26px",
          background:
            title === "SUCCESS"
              ? "#75D37F"
              : title === "SUSPECT"
              ? "#FFA24B"
              : title === "WAITING"
              ? "#FFA24B"
              : title === "FAILED" || title === "REJECTED"
              ? "#FF6F6F"
              : undefined, // nanti pakai ternary
        }}
      >
        <Grid item className={classes.bannerTitle}>
          {title === "SUCCESS"
            ? "Transaction Successful"
            : title === "SUSPECT"
            ? "Transaction Processed"
            : title === "WAITING"
            ? "Transactions Awaiting Approval"
            : title === "FAILED"
            ? "Transaction Failed"
            : title === "REJECTED"
            ? "Transaction Rejected"
            : "-"}
        </Grid>
        {message && errorCode && (
          <Grid item className={classes.bannerText}>
            {errorCode} - {message}
          </Grid>
        )}
        <Grid item className={classes.bannerText}>
          {dateTime}
        </Grid>
      </Grid>
    </Grid>
  );

  const ItemField = ({ label, value, type }) => (
    <React.Fragment>
      {type === "detail" && label.toLowerCase() !== "source of funds" ? (
        <Grid item>
          <Grid
            container
            justifyContent="space-between"
            style={{ padding: "0px 15px" }}
          >
            <Grid item className={classes.rightContentTitle}>
              {label}
            </Grid>
            <Grid
              item
              className={classes.rightContentValue}
              style={{ margin: !label ? "0px auto" : 0 }}
            >
              {value || "-"}
            </Grid>
          </Grid>
        </Grid>
      ) : null}
      {type === "total" ? (
        <React.Fragment>
          <Grid item style={{ padding: "0px 24px" }}>
            <hr className={classes.rightContentDivider} />
          </Grid>

          <Grid item>
            <Grid
              container
              justifyContent="space-between"
              style={{ padding: "0px 15px" }}
            >
              {/* Total transaction diubah jadi total persentase */}
              <Grid item className={classes.totalTitle}>
                {label}
              </Grid>
              <Grid item className={classes.totalValue}>
                {value.includes("Rp") ? value : formatMoneyRMG(value, "IDR")}
              </Grid>
            </Grid>
          </Grid>
        </React.Fragment>
      ) : null}
      {type === "footer" ? (
        <Grid container justifyContent="center" item>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "calc(100% - 24px)",
              color: "#66A3FF",
              backgroundColor: "#EAF2FF",
              border: "1px solid #66A3FF",
              borderRadius: "5px",
              padding: "10px",
            }}
          >
            {!value.includes("\n") ? (
              <Typography variant="body">{value}</Typography>
            ) : (
              value.split("\n").map((item) => (
                <Typography key={item} variant="body">
                  {item}
                </Typography>
              ))
            )}
          </Box>
        </Grid>
      ) : null}
      {type === "note" ? (
        <Grid container justifyContent="center" item>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              width: "calc(100% - 24px)",
              color: "#66A3FF",
              backgroundColor: "#EAF2FF",
              border: "1px solid #66A3FF",
              borderRadius: "5px",
              padding: "10px",
            }}
          >
            <Typography variant="body">
              This receipt is a valid proof of transaction.
            </Typography>
          </Box>
        </Grid>
      ) : null}
    </React.Fragment>
  );

  // nanti dibersihin
  const LeftContent = ({ props }) => (
    <Fragment>
      {/* -----Source----- */}
      <Grid
        container
        direction="column"
        spacing={1}
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.leftContentTitle}>
          Source of Fund :
        </Grid>
        <Grid item className={classes.leftContentName}>
          {dataMasterResiPayment.fromAccountName || "-"}
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between">
            <Grid item className={classes.accountNumberTitle}>
              Account Number
            </Grid>
            <Grid item className={classes.accountNumber}>
              {dataMasterResiPayment.fromAccountNumber || "-"}
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <hr className={classes.divider} />
        </Grid>
      </Grid>
    </Fragment>
  );

  // nanti dibersihin
  const RightContent = ({ dataset }) => (
    <Grid container direction="column" spacing={2}>
      {dataset &&
        dataset?.map((item, index) => (
          <ItemField label={item.label} value={item.value} type={item.type} />
        ))}
    </Grid>
  );

  return (
    <Card elevation={0} className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Card elevation={0} className={classes.cardItem}>
            <CardHeader className={classes.cardHeader} title="Transfer Data" />
            <CardContent className={classes.cardContent}>
              <Grid container direction="column" spacing={3}>
                {/* -----Banner----- */}
                <Banner
                  title={dataMasterResiPayment.transactionStatus}
                  dateTime={dataMasterResiPayment.transactionDate}
                  message={dataMasterResiPayment.message}
                  errorCode={dataMasterResiPayment.resultCode}
                />

                {/* -----Main Content----- */}
                <Grid item>
                  <Grid container>
                    {/* -----Left Content----- */}
                    <Grid item xs={4}>
                      <LeftContent />
                    </Grid>

                    {/* -----Right Content----- */}
                    <Grid item xs={8}>
                      <RightContent dataset={dataResiPayment} />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}
