import React, { Fragment, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Box,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";
import { shallowEqual, useSelector } from "react-redux";
import { formatMoneyRMG, standardDateAuditSaving } from "utils/helpers";
import moment from "moment";

const useStyles = makeStyles({
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardItem: {
    height: "100%",
    minHeight: "177px",
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: "0 20px",
    height: "100%",
    minHeight: "100px",
  },
  divider: {
    border: "1px solid #E6EAF3",
    borderBottom: 0,
  },
  itemSectionName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "16px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  itemLabel: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  itemValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    color: "#374062",
  },
});

export default function LoginDetails({ data }) {
  const classes = useStyles();

  const { detailActivity } = useSelector(
    (state) => state.auditTrail,
    shallowEqual
  );

  const ItemField = ({ label, value, sectionName }) => (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: "8px",
        height: "100%",
        justifyContent: "flex-end",
      }}
    >
      {sectionName ? (
        <Typography variant="body1" className={classes.itemSectionName}>
          {sectionName}
        </Typography>
      ) : null}
      <Box display="flex" flexDirection="column" gap="5px">
        <Typography variant="body1" className={classes.itemLabel}>
          {label} :
        </Typography>
        <Typography variant="body1" className={classes.itemValue}>
          {value}
        </Typography>
      </Box>
    </Box>
  );

  const MainContent = ({ dataset }) => (
    <Grid container spacing={3} alignItems="flex-start">
      <Grid item xs={3}>
        <ItemField label="Channel" value={dataset?.channel || "-"} />
      </Grid>
      <Grid item xs={3}>
        <ItemField label="OS" value={dataset?.os || "-"} />
      </Grid>
      <Grid item xs={3}>
        <ItemField label="IP Address" value={dataset?.ipAddress || "-"} />
      </Grid>
      <Grid item xs={3}>
        <ItemField label="Phone Type" value={dataset?.phoneType || "-"} />
      </Grid>
      <Grid item xs={3}>
        <ItemField label="Phone Brand" value={dataset?.phoneBrand || "-"} />
      </Grid>
      <Grid item xs={3}>
        <ItemField label="Device ID" value={dataset?.deviceId || "-"} />
      </Grid>
      <Grid item xs={3}>
        <ItemField label="Location" value={dataset?.location || "-"} />
      </Grid>
    </Grid>
  );

  return (
    <Card elevation={0} className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Card elevation={0} className={classes.cardItem}>
            <CardHeader className={classes.cardHeader} title="Data" />
            <CardContent className={classes.cardContent}>
              <Grid container direction="column" spacing={3}>
                {/* -----Main Content----- */}
                <Grid item container>
                  <MainContent dataset={detailActivity?.userActivityDetail} />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}
