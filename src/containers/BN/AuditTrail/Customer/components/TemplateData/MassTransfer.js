import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import CustomerMassTransfer from "./CustomerMassTransfer";
import CustomerDetailsTable from "./CustomerDetailsTable";
import CustomerDetailsReceipt from "./CustomerDetailsReceipt";

const MassTransfer = (props) => {
  const { pagesMassTransfer } = useSelector((state) => state.auditTrail);
  return (
    <React.Fragment>
      {pagesMassTransfer === 0 ? (
        <CustomerMassTransfer />
      ) : pagesMassTransfer === 1 ? (
        <CustomerDetailsTable />
      ) : pagesMassTransfer === 2 ? (
        <CustomerDetailsReceipt />
      ) : null}
    </React.Fragment>
  );
};

MassTransfer.propTypes = {};

export default MassTransfer;
