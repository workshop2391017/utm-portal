import React, { Fragment, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Box,
  Typography,
} from "@material-ui/core";

import Badge from "components/BN/Badge";

import Colors from "helpers/colors";
import { shallowEqual, useSelector } from "react-redux";
import moment from "moment";
import {
  capitalizeFirstLetterFromUppercase,
  formatMoneyRMG,
} from "utils/helpers";

const useStyles = makeStyles({
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardItem: {
    height: "100%",
    minHeight: 300,
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: 0,
    height: "100%",
    minHeight: 300,
  },
  bannerTitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.01em",
    color: "#FFFFFF",
  },
  bannerText: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#FFFFFF",
  },
  leftContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  leftContentName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumberTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  accountNumber: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  divider: {
    border: "1px solid #E6EAF3",
    borderBottom: 0,
  },
  rightContentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  rightContentValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  rightContentDivider: {
    border: "1px dashed #B3C1E7",
    borderBottom: 0,
  },
  totalTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#0061A7",
  },
  totalValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#0061A7",
  },
});

export default function SweepDetails({ data }) {
  const classes = useStyles();
  const [dataset, setDataset] = useState(null);

  const { dataResiTransfer: dataResi } = useSelector(
    (state) => state.auditTrail,
    shallowEqual
  );

  const scheduled = [
    {
      label: "At The Date Of",
      value: dataResi?.scheduleDate
        ? moment(dataResi?.scheduleDate, "DD/MM/YYYY | H:mm:ss").format(
            "DD/MM/YYYY"
          )
        : "-",
    },
  ];

  const recur = [
    {
      label: "Starting From",
      value: dataResi?.recurringStart
        ? moment(dataResi?.recurringStart, "DD/MM/YYYY | HH:mm:ss").format(
            "DD/MM/YYYY"
          )
        : "-",
    },
    {
      label: "Ends On",
      value: dataResi?.recurringEnd
        ? moment(dataResi?.recurringEnd, "DD/MM/YYYY | HH:mm:ss").format(
            "DD/MM/YYYY"
          )
        : "-",
    },
    {
      label: "Time",
      value: dataResi?.recurringStart
        ? `${moment(dataResi?.recurringStart, "DD/MM/YYYY | HH:mm:ss").format(
            "HH:mm"
          )} WIB`
        : "-",
    },
  ];

  useEffect(() => {
    let selectedSweep = [];
    let badge;
    if (
      String(dataResi?.transactionChoice).toLowerCase() === "saat ini" ||
      !dataResi?.transactionChoice
    ) {
      badge = { type: "green", label: "Immediately" };
    } else if (
      dataResi?.transactionChoice.toLowerCase() === "terjadwal" ||
      String(dataResi?.transactionChoice).toLowerCase() === "tanggal tertentu"
    ) {
      selectedSweep = scheduled;
      badge = { type: "orange", label: "Future Date" };
    } else {
      selectedSweep = recur;
      badge = {
        type: "blue",
        label:
          dataResi?.recurringCategory === "Mingguan"
            ? "Weekly Recurring"
            : dataResi?.recurringCategory === "Harian"
            ? "Daily Recurring"
            : "Monthly Recurring",
      };
    }
    const categoryTransaction =
      String(dataResi?.transactionCategory)?.toLowerCase() || "";

    if (categoryTransaction === "sweep out") {
      selectedSweep.push({
        label: dataResi.percentage
          ? "Percentage Transferred"
          : "Nominal Transferred",
        value: dataResi.percentage
          ? `${dataResi.percentage}% (${formatMoneyRMG(
              dataResi.transactionAmount
            )})`
          : formatMoneyRMG(`${dataResi.transactionAmount}`),
      });
    } else {
      if (dataResi.maintainBalance || dataResi.maintainPercentage) {
        selectedSweep.push({
          label: dataResi.maintainPercentage
            ? "Percentage Remained"
            : "Amount Remained",
          value: dataResi.maintainPercentage
            ? `${dataResi.maintainPercentage}% ${
                dataResi.maintainBalance
                  ? `(${formatMoneyRMG(dataResi.maintainBalance)})`
                  : ""
              }`
            : dataResi.maintainBalance
            ? formatMoneyRMG(`${dataResi.maintainBalance}`)
            : "-",
        });
      }
      selectedSweep.push({
        label: dataResi.percentage
          ? "Percentage Transferred"
          : "Nominal Transferred",
        value: dataResi.percentage
          ? `${dataResi.percentage}% (${formatMoneyRMG(
              dataResi.transactionAmount
            )})`
          : formatMoneyRMG(`${dataResi.transactionAmount}`),
      });
    }

    const rows = [
      {
        label: "Transaction ID",
        value: dataResi.transactionId,
      },
      {
        label: "Ref No",
        value: dataResi.retrievalReferenceNumber,
      },
      {
        label: "Type of Transaction",
        value: capitalizeFirstLetterFromUppercase(
          dataResi?.transactionCategory
        ),
      },
      {
        label: "Sweeping Account Options",
        value: (
          <Badge
            type={badge.type}
            label={badge.label}
            style={{ textTransform: "capitalize" }}
            outline
          />
        ),
      },
      ...selectedSweep,
      {
        label: "Transaction Fee",
        value: formatMoneyRMG(`${dataResi.transactionFee}`),
      },
      {
        label: "Admin Fee",
        value: formatMoneyRMG(`${dataResi.adminFee}`),
      },
      {
        label: "Total Fee",
        value: formatMoneyRMG(`${dataResi.totalFee}`),
      },
    ];
    setDataset(rows);
  }, [dataResi]);

  // nanti dibersihin
  const Banner = ({
    title,
    dateTime,
    transactionChoice,
    recurringCategory,
  }) => (
    <Grid item>
      <Grid
        container
        direction="column"
        spacing={1}
        style={{
          padding: "16px 26px",
          background:
            title === "SUCCESS"
              ? "#75D37F"
              : title === "SUSPECT"
              ? "#FFA24B"
              : title === "WAITING"
              ? "#FFA24B"
              : title === "FAILED"
              ? "#FF6F6F"
              : undefined, // nanti pakai ternary
        }}
      >
        <Grid item className={classes.bannerTitle}>
          {title === "SUCCESS"
            ? dataResi?.transactionChoice === "Terjadwal" ||
              String(dataResi?.transactionChoice).toLowerCase() ===
                "tanggal tertentu" ||
              String(dataResi?.transactionChoice).toLowerCase() === "berulang"
              ? "Your Transaction Successfully Scheduled"
              : "Transaction Successful"
            : title === "SUSPECT"
            ? "Transaction Processed"
            : title === "WAITING"
            ? "Transactions Awaiting Approval"
            : title === "FAILED"
            ? "Transaction Failed"
            : "-"}
        </Grid>
        <Grid item className={classes.bannerText}>
          {dateTime}
        </Grid>
      </Grid>
    </Grid>
  );

  const ItemField = ({ label, value }) => (
    <Grid item>
      <Grid
        container
        justifyContent="space-between"
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.rightContentTitle}>
          {label}
        </Grid>
        <Grid item className={classes.rightContentValue}>
          {value ?? "-"}
        </Grid>
      </Grid>
    </Grid>
  );

  // nanti dibersihin
  const LeftContent = ({ props }) => (
    <Fragment>
      {/* -----Source----- */}
      <Grid
        container
        direction="column"
        spacing={1}
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.leftContentTitle}>
          Source of Fund :
        </Grid>
        <Grid item className={classes.leftContentName}>
          {dataResi.fromAccountName || "-"}
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between">
            <Grid item className={classes.accountNumberTitle}>
              Account No.
            </Grid>
            <Grid item className={classes.accountNumber}>
              {dataResi.fromAccountNumber || "-"}
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <hr className={classes.divider} />
        </Grid>
      </Grid>

      {/* -----Destination----- */}
      <Grid
        container
        direction="column"
        spacing={1}
        style={{ padding: "0px 15px" }}
      >
        <Grid item className={classes.leftContentTitle}>
          Destination Account :
        </Grid>
        <Grid item className={classes.leftContentName}>
          {dataResi.toAccountName || "-"}
        </Grid>
        <Grid item>
          <Grid container justifyContent="space-between">
            <Grid item className={classes.accountNumberTitle}>
              Account No.
            </Grid>
            <Grid item className={classes.accountNumber}>
              {dataResi.toAccountNumber || "-"}
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <hr className={classes.divider} />
        </Grid>
      </Grid>
    </Fragment>
  );

  // nanti dibersihin
  const RightContent = ({ dataset }) => (
    <Grid container direction="column" spacing={2}>
      {dataset &&
        dataset.map((item, index) => (
          <ItemField label={item.label} value={item.value} />
        ))}
      <Grid item style={{ padding: "0px 24px" }}>
        <hr className={classes.rightContentDivider} />
      </Grid>

      <Grid item>
        <Grid
          container
          justifyContent="space-between"
          style={{ padding: "0px 15px" }}
        >
          <Grid item className={classes.totalTitle}>
            Total Transactions
          </Grid>
          <Grid item className={classes.totalValue}>
            {formatMoneyRMG(`${dataResi.totalAmount}`)}
          </Grid>
        </Grid>
      </Grid>
      <Grid container justifyContent="center" item>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            width: "calc(100% - 24px)",
            color: "#66A3FF",
            backgroundColor: "#EAF2FF",
            border: "1px solid #66A3FF",
            borderRadius: "5px",
            padding: "10px",
          }}
        >
          <Typography variant="body">
            This receipt is a valid proof of transaction.
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );

  return (
    <Card elevation={0} className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Card elevation={0} className={classes.cardItem}>
            <CardHeader className={classes.cardHeader} title="Transfer Data" />
            <CardContent className={classes.cardContent}>
              <Grid container direction="column" spacing={3}>
                {/* -----Banner----- */}
                <Banner
                  title={dataResi?.transactionStatus}
                  dateTime={dataResi.transactionDate}
                  transactionChoice={dataResi?.transactionChoice}
                  recurringCategory={dataResi?.recurringCategory}
                />

                {/* -----Main Content----- */}
                <Grid item>
                  <Grid container>
                    {/* -----Left Content----- */}
                    <Grid item xs={4}>
                      <LeftContent />
                    </Grid>

                    {/* -----Right Content----- */}
                    <Grid item xs={8}>
                      <RightContent dataset={dataset} />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}
