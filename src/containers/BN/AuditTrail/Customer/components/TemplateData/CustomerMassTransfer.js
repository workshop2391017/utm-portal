import React, { Fragment } from "react";

import { makeStyles } from "@material-ui/styles";
import { Card, CardHeader, CardContent, Grid } from "@material-ui/core";

import GeneralButton from "components/BN/Button/GeneralButton";
import Badge from "components/BN/Badge";

import { ReactComponent as CircleSuccess } from "assets/icons/BN/circle-success.svg";
import { ReactComponent as CircleFailed } from "assets/icons/BN/circle-failed.svg";
import { ReactComponent as CircleRejected } from "assets/icons/BN/circle-rejected.svg";
import { ReactComponent as CircleProcess } from "assets/icons/BN/circle-process.svg";
import { ReactComponent as CircleWaiting } from "assets/icons/BN/circle-waiting.svg";
import { ReactComponent as CalendarBlue } from "assets/icons/BN/calendarblue.svg";

import Colors from "helpers/colors";
import { setPagesMass } from "stores/actions/auditTrail";
import { useDispatch } from "react-redux";

const useStyles = makeStyles({
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardItem: {
    height: "100%",
    minHeight: 300,
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  cardHeader: {
    background: Colors.primary.hard,
    color: Colors.white,
    marginBottom: 30,
    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  cardContent: {
    padding: 0,
    height: "100%",
    minHeight: 300,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "16px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  transactions: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  headerContainer: {
    padding: "0px 16px",
    marginBottom: 30,
  },
  contentTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  contentValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function CustomerMassTransfer({ data }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <Card elevation={0} className={classes.card}>
      <Grid container>
        <Grid item xs={12}>
          <Card elevation={0} className={classes.cardItem}>
            <CardHeader className={classes.cardHeader} title="Transfer Data" />
            <CardContent className={classes.cardContent}>
              <Grid container>
                {/* -----Header----- */}
                <Grid item xs={12} className={classes.headerContainer}>
                  <Grid container justifyContent="space-between">
                    <Grid item className={classes.title}>
                      Transaction
                    </Grid>
                    <Grid item>
                      <GeneralButton
                        label="View Details"
                        width={100}
                        height={25}
                        onClick={() => dispatch(setPagesMass(1))}
                        style={{
                          fontFamily: "FuturaMdBT",
                          fontSize: 9,
                        }}
                      />
                    </Grid>
                  </Grid>
                </Grid>

                {/* -----Left Content----- */}
                <Grid item xs={4} style={{ paddingLeft: 16 }}>
                  <Grid container direction="column" spacing={1}>
                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <CircleSuccess />
                        </Grid>
                        <Grid item className={classes.transactions}>
                          7 Successful Transfers
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <CircleFailed />
                        </Grid>
                        <Grid item className={classes.transactions}>
                          3 Failed Transfers
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <CircleRejected />
                        </Grid>
                        <Grid item className={classes.transactions}>
                          7 Rejected Transfers
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <CircleProcess />
                        </Grid>
                        <Grid item className={classes.transactions}>
                          7 Processed Transfers
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <CircleWaiting />
                        </Grid>
                        <Grid item className={classes.transactions}>
                          7 Transfers Waiting for Approval
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                {/* -----Middle Content----- */}
                <Grid item xs={4} style={{ paddingLeft: 10 }}>
                  <Grid container direction="column" spacing={1}>
                    <Grid item>
                      <Grid container direction="column">
                        <Grid item className={classes.contentTitle}>
                          Total Transaction :
                        </Grid>
                        <Grid item className={classes.contentValue}>
                          Rp 120.000.000.000,00
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container direction="column">
                        <Grid item className={classes.contentTitle}>
                          Transaction ID :
                        </Grid>
                        <Grid item className={classes.contentValue}>
                          ID7439201
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container direction="column">
                        <Grid
                          item
                          className={classes.contentTitle}
                          style={{ marginBottom: 2 }}
                        >
                          Time :
                        </Grid>
                        <Grid item className={classes.contentValue}>
                          <Grid container>
                            <Grid item style={{ marginRight: 16 }}>
                              <Grid container alignItems="center" spacing={1}>
                                <Grid item>
                                  <CalendarBlue width={20} height={20} />
                                </Grid>
                                <Grid item>Starting from 21</Grid>
                              </Grid>
                            </Grid>
                            <Grid item>
                              <Grid container alignItems="center" spacing={1}>
                                <Grid item>
                                  <CalendarBlue width={20} height={20} />
                                </Grid>
                                <Grid item>Ends on 21</Grid>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                {/* -----Right Content----- */}
                <Grid item xs={4} style={{ paddingLeft: 10 }}>
                  <Grid container direction="column" spacing={1}>
                    <Grid item>
                      <Grid container direction="column">
                        <Grid item className={classes.contentTitle}>
                          Transaction Amount :
                        </Grid>
                        <Grid item className={classes.contentValue}>
                          31 Transactions
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container direction="column">
                        <Grid item className={classes.contentTitle}>
                          Schedule :
                        </Grid>
                        <Grid item>
                          <Badge
                            styleBadge={{ textAlign: "center", width: 105 }}
                            type="blue"
                            label="Berulang Harian"
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}
