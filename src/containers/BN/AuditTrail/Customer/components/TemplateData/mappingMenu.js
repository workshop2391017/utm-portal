import React from "react";
import DetailLimit from "./DetailLimit";

export default function mappingMenu(type, props) {
  switch (type) {
    case "LIMIT_DETAIL":
      return <DetailLimit {...props} />;

    default:
      return null;
  }
}
