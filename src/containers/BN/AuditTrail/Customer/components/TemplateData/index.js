import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/styles";

// components
import { Card, CardHeader, CardContent, Grid } from "@material-ui/core";
// helpers
import Colors from "helpers/colors";
import mappingMenu from "./mappingMenu";

const useStyles = makeStyles((theme) => ({
  row: {
    display: "flex",
    flexDirection: "row",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  card: {
    borderRadius: 10,
    padding: 20,
  },
  firstCard: {
    display: "flex",
    position: "relative",
  },
  firstCard__title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 20,
    color: "#374062",
  },
  firstCard__subtitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 13,
    color: "#7B87AF",
  },
  thirdCardItem: {
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  thirCardItemHeader: {
    background: Colors.primary.hard,
    color: Colors.white,

    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  level: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  groupLabel: {
    fontFamily: "FuturaHvBT",
    fontSize: 16,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  label: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  featureList: {
    "& li": {
      listStylePosition: "outside",
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 15,
      color: Colors.dark.hard,
    },
  },
}));

function TempalateData({ oldData, newData, data, type }) {
  const classes = useStyles();

  return (
    <Card
      elevation={0}
      className={`${classes.card} ${classes.row}`}
      style={{ gap: 24 }}
    >
      <Grid container spacing={4}>
        <Grid item xs={6}>
          <Card
            elevation={0}
            className={classes.thirdCardItem}
            style={{ height: "100%", minHeight: 300 }}
          >
            <CardHeader
              className={classes.thirCardItemHeader}
              title="Old Data"
            />
            <CardContent
              style={{
                minHeight: 300,
                height: "100%",
              }}
            >
              <Fragment>
                {oldData !== undefined &&
                oldData !== null &&
                Object.keys(oldData).length !== 0 ? (
                  mappingMenu(type, { data: oldData })
                ) : (
                  <div
                    className={classes.column}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      minHeight: "90%",
                    }}
                  >
                    <h3
                      style={{
                        fontFamily: "FuturaMdBT",
                        fontWeight: 400,
                        fontSize: 20,
                        color: Colors.dark.medium,
                      }}
                    >
                      No Data
                    </h3>
                    <p
                      style={{
                        fontFamily: "FuturaBkBT",
                        fontWeight: 400,
                        fontSize: 13,
                        color: Colors.dark.medium,
                      }}
                    >
                      Not displaying old data
                    </p>
                  </div>
                )}
              </Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card
            elevation={0}
            className={classes.thirdCardItem}
            style={{ height: "100%", minHeight: 300 }}
          >
            <CardHeader
              className={classes.thirCardItemHeader}
              title="New Data"
            />
            <CardContent
              style={{
                minHeight: 300,
                height: "100%",
              }}
            >
              <Fragment>
                {newData !== undefined &&
                newData !== null &&
                Object.keys(newData).length !== 0 ? (
                  mappingMenu(type, { data: newData })
                ) : (
                  <div
                    className={classes.column}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      minHeight: "90%",
                    }}
                  >
                    <h3
                      style={{
                        fontFamily: "FuturaMdBT",
                        fontWeight: 400,
                        fontSize: 20,
                        color: Colors.dark.medium,
                      }}
                    >
                      No Data
                    </h3>
                    <p
                      style={{
                        fontFamily: "FuturaBkBT",
                        fontWeight: 400,
                        fontSize: 13,
                        color: Colors.dark.medium,
                      }}
                    >
                      Not displaying new data
                    </p>
                  </div>
                )}
              </Fragment>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );
}

export default TempalateData;
