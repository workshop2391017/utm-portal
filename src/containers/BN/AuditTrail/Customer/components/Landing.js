import React, { Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import moment from "moment";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

// Helpers
import useDebounce from "utils/helpers/useDebounce";
import {
  rangeDate,
  searchDateStart,
  searchDateEnd,
  handleValueActivityDetailAudit,
} from "utils/helpers";
import {
  setActivityName,
  setDetailActivity,
  getDataAudit,
  setPayloadGetData,
  setTypeDetail,
  setNewData,
  setOldData,
  getTransactionDetailAction,
} from "stores/actions/auditTrail";

// Components
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";

// Asstes
import { ReactComponent as SortAsc } from "assets/icons/BN/sort-numeric-down-alt.svg";
import { ReactComponent as SortDesc } from "assets/icons/BN/sort-numeric-up-alt.svg";

const today = new Date();

const useStyles = makeStyles((theme) => ({
  table: {
    display: "flex",
    flexDirection: "column",
    gap: 20,
  },
}));

// // Dummy
// const tableDummy = [
//   {
//     user: "User 1",
//     role: "Checker",
//     activityStatus: "Add",
//     activityName: "Approve Financial Matrix",
//     status: 0,
//   },
//   {
//     user: "User 2",
//     role: "Checker",
//     activityStatus: "Edit",
//     activityName: "Edit Level",
//     status: 1,
//   },
//   {
//     user: "User 3",
//     role: "Checker",
//     activityStatus: "Delete",
//     activityName: "Reject Financial Matrix",
//     status: 2,
//   },
// ];

function Landing({ setActivePage }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { dataTableAudit, payloadGetData, search } = useSelector(
    (state) => state.auditTrail
  );

  const listArrayActivityDetail = [
    "Execute Samebank",
    "Execute SKN",
    "Execute RTGS",
    "Execute Transfer",
    "Execute Own",
    "Execute Online",
    "Execute Purchase",
    "Execute Billpayment",
    "Execute Sweep",
    "Create Saving",
    "Login",
    "First Login",
    "Login Other Device",
    // Multi below
    "Edit Group",
    "Edit Financial",
    "Edit Non",
    "Edit Level",
    "Execute Payroll",
    "Execute BI-FAST",
  ];

  const [dataFilter, setDataFilter] = useState(null);
  // {
  //   tabs: {
  //     tabValue1: 0,
  //     tabValue2: 0,
  //   },
  //   date: {
  //     dateValue1: {
  //       startDate: null,
  //       endDate: null,
  //     },
  //     dateValue2: {
  //       startDate: null,
  //       endDate: null,
  //     },
  //   },
  // }

  useEffect(() => {
    dispatch(getDataAudit("customer"));
  }, [dispatch, payloadGetData]);

  const handleValueActivity = (type) => {
    switch (type) {
      case 1:
        return "SUCCESS";
      case 2:
        return "FAILED";
      default:
        return "";
    }
  };

  useEffect(() => {
    if (dataFilter || payloadGetData.searchValue !== search) {
      let startDateValue = payloadGetData.startDate;
      let endDateValue = payloadGetData.endDate;
      if (dataFilter?.date?.dateValue1 === null) {
        startDateValue = searchDateStart(today);
        endDateValue = searchDateEnd(today);
      }
      if (dataFilter?.date?.dateValue2 === null) {
        endDateValue = searchDateEnd(today);
      }
      const payload = {
        ...payloadGetData,
        direction: dataFilter?.tabs?.tabValue2 === 0 ? "ASC" : "DESC",
        startDate: dataFilter?.date?.dateValue1
          ? searchDateStart(dataFilter?.date?.dateValue1)
          : startDateValue,
        endDate: dataFilter?.date?.dateValue2
          ? searchDateEnd(dataFilter?.date?.dateValue2)
          : endDateValue,
        searchValue: search,
        activityStatus: handleValueActivity(
          dataFilter?.tabsAdvance?.tabValueAdvance1
        ),
        pageNumber: 0,
      };
      dispatch(setPayloadGetData(payload));
    }
  }, [dataFilter, search]);

  const getTypeDetailAudit = (activityName) => {
    if (activityName.includes("User Administration")) return "USER_DETAIL";
    if (activityName.includes("Transfer")) return "TRANSFER_DETAILS";

    return "";
  };

  const handleNextSuccess = (rowData) => {
    setActivePage(1);
    dispatch(setActivityName(rowData?.activityName));
    dispatch(setDetailActivity(rowData));
    dispatch(setTypeDetail(getTypeDetailAudit(rowData.activityName)));
  };

  const seeDetailHandle = (rowData) => {
    dispatch(getTransactionDetailAction(rowData, handleNextSuccess));
  };

  const getRole = (role) => {
    switch (role) {
      case "USER_RELEASER":
        return "Releaser";
      case "USER_MAKER":
        return "Maker";
      case "USER_APPROVER":
        return "Approver";
      case "USER_MAKER_APPROVER":
        return "Maker, Approver";
      case "USER_MAKER_RELEASER":
        return "Maker, Releaser";
      case "USER_MAKER_APPROVER_RELEASER":
        return "Maker, Approver, Releaser";
      case "USER_APPROVER_RELEASER":
        return "Approver, Releaser";
      case "ADMIN_USER_MAKER":
        return "Admin Maker";
      case "ADMIN_USER_RELEASER":
        return "Admin Approver";
      case "OWNER":
        return "User Single";
      default:
        return "";
    }
  };

  const tableConfig = [
    {
      title: "Date & Time",
      headerAlign: "left",
      align: "left",

      render: (rowData) => (
        <span>
          {moment(rowData.activityDate).format("YYYY-MM-DD | HH:mm:ss")}
        </span>
      ),
    },
    {
      title: "User",
      headerAlign: "left",
      align: "left",
      key: "fullName",
    },
    {
      title: "Role",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <span>{getRole(rowData.userActivityDetail.roleName)}</span>
      ),
    },
    {
      title: "Activity Name",
      headerAlign: "left",
      align: "left",
      key: "activityName",
    },
    {
      title: "Status",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <Badge
          label={
            rowData.status === "Failed"
              ? "Failed"
              : rowData.status === "Waiting"
              ? "Waiting"
              : "Success"
          }
          type={
            rowData.status === "Failed"
              ? "red"
              : rowData.status === "Waiting"
              ? "orange"
              : "green"
          }
        />
      ),
    },
    {
      title: "",
      render: (rowData) => (
        <GeneralButton
          label="View Details"
          width={110}
          height={24}
          disabled={
            !listArrayActivityDetail.includes(
              handleValueActivityDetailAudit(rowData.activityName)
            )
          }
          // onClick={() => console.log("ini", rowData)}
          onClick={() => seeDetailHandle(rowData)}
          style={{
            fontFamily: "FuturaMdBT",
            fontSize: 10,
          }}
        />
      ),
    },
  ];

  return (
    <Fragment>
      <Filter
        dataFilter={dataFilter}
        setDataFilter={(e) => {
          setDataFilter(e);
        }}
        title="Show :"
        align="left"
        options={[
          {
            id: 1,
            type: "datePicker",
            placeholder: "Start Date",
          },
          {
            id: 2,
            type: "datePicker",
            placeholder: "End Date",
            disabled: true,
          },
          {
            id: 2,
            type: "tabs",
            defaultValue: 1,
            options: [
              {
                label: "Date Asc",
                icon: SortAsc,
              },
              {
                label: "Date Desc",
                icon: SortDesc,
              },
            ],
          },
        ]}
        isAdvance
        optionsAdvance={[
          { id: 1, type: "tabs", options: ["All", "Success", "Failed"] },
        ]}
      />

      <section className={classes.table}>
        <TableICBB
          headerContent={tableConfig}
          dataContent={dataTableAudit?.userActivitylist}
          page={payloadGetData.pageNumber + 1}
          setPage={(page) => {
            const payload = {
              ...payloadGetData,
              pageNumber: page - 1,
            };
            dispatch(setPayloadGetData(payload));
          }}
          isLoading={false}
          totalElement={dataTableAudit.totalElement}
          totalData={dataTableAudit.totalPage}
        />
      </section>
    </Fragment>
  );
}

export default Landing;
