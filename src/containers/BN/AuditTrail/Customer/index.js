import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";

// redux
import {
  handleDownloadAuditTrail,
  setDataSearch as setDataSearchAction,
  clearPayloadGetData,
} from "stores/actions/auditTrail";
import { useDispatch, useSelector } from "react-redux";
import {
  Box,
  CircularProgress,
  Dialog,
  DialogContent,
} from "@material-ui/core";

// components
import Button from "@material-ui/core/Button";
import GeneralButton from "components/BN/Button/GeneralButton";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";

// libs
import useDebounce from "utils/helpers/useDebounce";

// asset
import arrowLeft from "assets/icons/BN/arrow-left.svg";
import { ReactComponent as UnduhIcon } from "assets/icons/BN/unduh-white.svg";

import Landing from "./components/Landing";
import Details from "./components/Details";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    marginBottom: 80,
    padding: "0px 30px",
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 76,
    marginBottom: 20,
  },
  title: {
    backgroundColor: "transparent",
    fontSize: 20,
    fontWeight: 700,
    fontFamily: "FuturaHvBT",
    color: "#2B2F3C",
  },
  main: {
    display: "flex",
    flexDirection: "column",
    gap: 20,
  },
}));
function Customer() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { isLoading, pagesMassTransfer } = useSelector(
    (state) => state.auditTrail
  );

  const [dataSearch, setDataSearch] = useState("");

  const [activePage, setActivePage] = useState(0);

  const dataSearchDebounce = useDebounce(dataSearch, 1000);

  useEffect(() => {
    dispatch(setDataSearchAction(dataSearchDebounce));
  }, [dataSearchDebounce]);

  useEffect(
    () => () => {
      dispatch(setDataSearchAction(""));
      dispatch(clearPayloadGetData());
    },
    []
  );

  return (
    <div className={classes.container}>
      <Dialog open={isLoading}>
        <DialogContent style={{ padding: "20px" }}>
          <Box sx={{ display: "flex" }}>
            <CircularProgress />
          </Box>
        </DialogContent>
      </Dialog>

      {pagesMassTransfer === 0 ? (
        <header className={classes.header}>
          <div>
            {activePage !== 0 && (
              <Button
                startIcon={<img src={arrowLeft} alt="Back" />}
                onClick={() => setActivePage(0)}
                style={{
                  fontFamily: "FuturaMdBT",
                  fontWeight: 700,
                  fontSize: 13,
                  color: "#0061A7",
                  textTransform: "capitalize",
                }}
              >
                Back
              </Button>
            )}
            <div className={classes.title}>
              {activePage === 0
                ? "Audit Trail Customer"
                : "Audit Trail Customer Details"}
            </div>
          </div>
          {activePage === 0 && (
            <div style={{ display: "flex" }}>
              <SearchWithDropdown
                placeholder="User, Activity Name"
                placement="bottom"
                style={{ marginRight: 20, width: 240 }}
                dataSearch={dataSearch}
                setDataSearch={setDataSearch}
              />
              <GeneralButton
                label="Download Excel"
                width={183}
                iconPosition="start"
                buttonIcon={<UnduhIcon />}
                onClick={() => dispatch(handleDownloadAuditTrail("customer"))}
              />
            </div>
          )}
        </header>
      ) : null}

      <main className={classes.main}>
        {activePage === 0 && <Landing setActivePage={setActivePage} />}
        {activePage === 1 && <Details />}
      </main>
    </div>
  );
}

export default Customer;
