import React from "react";
import CorporateAccountConf from "./Detail/CorporateAccountConf";
import CorporateDetail from "./Detail/CorporateManagement";
import CorporateSegmentation from "./Detail/CorporateSegmentation";
import DetailLevel from "./Detail/DetailLevel";
import DetailLimit from "./Detail/DetailLimit";
import DetailUser from "./Detail/DetailUser";
import DomesticBank from "./Detail/DomesticBank";
import GeneralParameter from "./Detail/GeneralParameter";
import InternationalBank from "./Detail/InternationalBank";
import LimitPackage from "./Detail/LimitPackage";
import ServiceChargelist from "./Detail/ServiceChargelist";
import PrivilegeSettings from "./Detail/PrivilegeSettings";
import ProductDetail from "./Detail/ProductDetail";
import AccountProductDetail from "./Detail/AccountProductDetail";
import TransactionLimit from "./Detail/TransactionLimit";
import DetailHostErrorMapping from "./Detail/DetailHostErrorMapping";
import DetailChargeList from "./Detail/DetailChargeList";
import Service from "./Detail/Service";
import ParameterSettings from "./Detail/ParameterSettings";
import SegmentationBranch from "./Detail/SegmentationBranch";
import ParameterAdvance from "./Detail/ParameterAdvance";
import CompanyLimit from "./Detail/CompanyLimit";
import SegmentationDetail from "./Detail/SegmentationDetail";
import ApprovalMatrix from "./Detail/ApprovalMatrix";
import AccountTypeDetail from "./Detail/AccountType";
import TieringDetail from "./Detail/TieringDetail";
import MenuPackage from "./Detail/MenuPackage";
import SpecialMenu from "./Detail/SpecialMenu";
import CompanyCharge from "./Detail/CompanyCharge";
import HandlingOfficer from "./Detail/HandlingOfficer";
import AccountConfiguration from "./Detail/AccountConfiguration";
import ChargePackage from "./Detail/ChargePackage";
import DetailUserCorporate from "./Detail/DetailUserCorporate";
import StatusUserCorporate from "./Detail/StatusUserCorporate";

export default function mappingMenu(type, props) {
  switch (type) {
    case "LIMIT_DETAIL":
      return <DetailLimit {...props} />;
    case "USER_DETAIL":
      return <DetailUser {...props} />;
    case "STATUS_USER_CORPORATE_DETAIL":
      return <StatusUserCorporate {...props} />;
    case "USER_CORPORATE_DETAIL":
      return <DetailUserCorporate {...props} />;
    case "LEVEL_DETAIL":
      return <DetailLevel {...props} />;
    case "SERVICE_CHARGE_LIST_DETAIL":
      return <ServiceChargelist {...props} />;
    case "LIMIT_PACKAGE_DETAIL":
      return <LimitPackage {...props} />;
    case "GENERAL_PARAMETER_DETAIL":
      return <GeneralParameter {...props} />;
    case "DOMESTIC_BANK_DETAIL":
      return <DomesticBank {...props} />;
    case "INTERNATIONAL_BANK_DETAIL":
      return <InternationalBank {...props} />;
    case "CORPORATE_MANAGEMENT_DETAIL":
      return <CorporateDetail {...props} />;
    case "CORPORATE_ACCOUNT_LIMIT_CONFIG":
      return <CorporateAccountConf {...props} />;
    case "CORPORATE_SEGMENTATION":
      return <CorporateSegmentation {...props} />;
    case "PRIVILEGE_SETTINGS_DETAIL":
      return <PrivilegeSettings {...props} />;
    case "PRODUCT_DETAIL":
      return <ProductDetail {...props} />;
    case "ACCOUNT_PRODUCT_DETAIL":
      return <AccountProductDetail {...props} />;
    case "ACCOUNT_TYPE_DETAIL":
      return <AccountTypeDetail {...props} />;
    case "LIMIT_TRANSACTION_DETAIL":
      return <TransactionLimit {...props} />;
    case "HOST_ERROR_MAPPING":
      return <DetailHostErrorMapping {...props} />;
    case "CHARGE_LIST_DETAIL":
      return <DetailChargeList {...props} />;
    case "SERVICE_DETAIL":
      return <Service {...props} />;
    case "PARAMETER_SETTINGS_DETAIL":
      return <ParameterSettings {...props} />;
    case "SEGMENTATION_BRANCH_DETAIL":
      return <SegmentationBranch {...props} />;
    case "PARAMETER_ADVANCE_DETAIL":
      return <ParameterAdvance {...props} />;
    case "COMPANY_LIMIT_DETAIL":
      return <CompanyLimit {...props} />;
    case "COMPANY_CHARGE_DETAIL":
      return <CompanyCharge {...props} />;
    case "SEGMENTATION_DETAIL":
      return <SegmentationDetail {...props} />;
    case "APPROVAL_MATRIX_DETAIL":
      return <ApprovalMatrix {...props} />;
    case "TIERING_DETAIL":
      return <TieringDetail {...props} />;
    case "MENU_PACKAGE_DETAIL":
      return <MenuPackage {...props} />;
    case "SPECIAL_MENU_DETAIL":
      return <SpecialMenu {...props} />;
    case "HANDLING_OFFICER_DETAIL":
      return <HandlingOfficer {...props} />;
    case "ACCOUNT_CONFIGURATION_DETAIL":
      return <AccountConfiguration {...props} />;
    case "CHARGE_PACKAGE_DETAIL":
      return <ChargePackage {...props} />;
    default:
      return null;
  }
}
