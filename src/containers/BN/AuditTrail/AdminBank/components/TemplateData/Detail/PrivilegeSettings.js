import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  column: {
    display: "flex",
    flexDirection: "column",
    gap: "8px",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function PrivilegeSettings({ data }) {
  const classes = useStyles();

  return (
    <Fragment>
      <Grid container spacing={2}>
        <Grid item xs={6} className={classes.column}>
          <span className={classes.title}>Office :</span>
          <span className={classes.value}>
            {data[0]?.portalGroup?.officeType === "KP"
              ? "Head Office"
              : data[0]?.portalGroup?.officeType === "KC"
              ? "Branch Office"
              : "-"}
          </span>
        </Grid>
        <Grid item xs={6} className={classes.column}>
          <span className={classes.title}>Group Name :</span>
          <span className={classes.value}>
            {data[0]?.portalGroup?.name ?? "-"}
          </span>
        </Grid>
        <Grid item xs={12} className={classes.column}>
          <span className={classes.title}>Group Features :</span>
        </Grid>
        {data[0]?.masterMenu?.map((el, i) => (
          <Fragment key={i}>
            <Grid item xs={12} className={classes.value}>
              <span>{el?.name_en ?? "-"}</span>
            </Grid>
            {el?.menuAccessChild?.map((el, i) => (
              <Grid item xs={6} key={i}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px",
                    marginLeft: i % 2 === 0 ? "30px" : "0px",
                  }}
                >
                  <BlueSphere />
                  <span className={classes.value}>{el?.name_en ?? "-"}</span>
                </div>
              </Grid>
            ))}
            {i !== data[0]?.masterMenu?.length - 1 && (
              <Grid item xs={12}>
                <hr style={{ border: "1px solid #EAF2FF", borderTop: "0px" }} />
              </Grid>
            )}
          </Fragment>
        ))}
      </Grid>
    </Fragment>
  );
}
