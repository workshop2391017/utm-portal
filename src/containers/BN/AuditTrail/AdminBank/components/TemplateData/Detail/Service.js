import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  row: {
    display: "flex",
    alignItems: "center",
    gap: "10px",
    marginLeft: "30px",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function Service({ data }) {
  const classes = useStyles();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Transaction Category :</span>
        <span className={classes.value}>
          {data?.service?.transactionCategory?.name ?? "-"}
        </span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Service Name :</span>
        <span className={classes.value}>{data?.service?.name ?? "-"}</span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Currency Matrix :</span>
      </Grid>
      {data?.currencyMatrixList?.map((el, i) => (
        <Grid item xs={12} key={i} className={classes.row}>
          <BlueSphere />
          <span className={classes.value}>{el?.name}</span>
        </Grid>
      ))}
    </Grid>
  );
}
