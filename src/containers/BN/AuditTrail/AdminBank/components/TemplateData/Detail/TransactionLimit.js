import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core";

import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import formatNumber from "helpers/formatNumber";

const useStyles = makeStyles({
  thead: {
    background: "#EAF2FF !important",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#374062 !important",
  },
});

export default function TransactionLimit({ data }) {
  const classes = useStyles();

  const [dataTable, setDataTable] = useState([]);

  useEffect(() => {
    let res = data?.serviceDtoList?.map((el) => ({
      name: el.serviceName,
      child: el?.serviceCurrencyMatrixDtoList?.map((el) => ({
        currencyMatrixName: el?.currencyMatrixName,
        currencyCode: el?.currencyCode,
        maxTransaction: `${formatNumber(el?.maxTransactionDay)}`,
        minAmountTransaction: `${formatNumber(el?.minTransactionAmount)}`,
        maxAmountTransaction: `${formatNumber(el?.maxTransactionAmount)}`,
        maxAmountTransactionOfDay: `${formatNumber(
          el?.maxTransactionDayAmount
        )}`,
      })),
    }));

    res = res.filter(
      (val, i, arr) => i === arr.findIndex((el) => el.name === val.name)
    );

    setDataTable(res);
  }, [data]);

  const dataHeader = [
    {
      title: "Currency Matrix",
      key: "currencyMatrixName",
    },
    {
      title: "Currency",
      key: "currencyCode",
    },
    {
      title: "Max Transactions / Day",
      key: "maxTransaction",
    },
    {
      title: "Minimum Transaction",
      key: "minAmountTransaction",
    },
    {
      title: "Maximum Transaction",
      key: "maxAmountTransaction",
    },
    {
      title: "Max Nominal / Day",
      key: "maxAmountTransactionOfDay",
    },
  ];

  return (
    <TableICBBCollapse
      headerContent={dataHeader}
      dataContent={dataTable}
      selecable={false} // hide selecable
      tableRounded={false}
      collapse={false} // hide icon collapse
      defaultOpenCollapseBreakdown // open collapse default
      headerClassName={classes.thead}
    />
  );
}
