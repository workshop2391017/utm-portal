import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import Badge from "components/BN/Badge/BadgeCustom";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function DomesticBank({ data }) {
  const classes = useStyles();

  const paddingBottom = () => {
    let transferTypeLength = 0;

    if (data?.transferOnlineAllowed) transferTypeLength += 1;
    if (data?.transferSknAllowed) transferTypeLength += 1;
    if (data?.transferRtgsAllowed) transferTypeLength += 1;
    if (data?.transferBiFastAllowed) transferTypeLength += 1;

    if (transferTypeLength === 0) return 57.5;
    if (transferTypeLength <= 2) return 52.2;
    if (transferTypeLength >= 3) return 12;
  };

  const GridItem = ({ title, value, xs }) => (
    <Grid item xs={xs}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.title}>
          {title}
        </Grid>
        <Grid item className={classes.value}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridTransferTypes = ({ value, label, type }) => (
    <Grid item xs={6} style={{ paddingBottom: paddingBottom() }}>
      <Grid container alignItems="center" spacing={1}>
        <Grid item>
          <Badge label={label} type={type} outline />
        </Grid>
        <Grid item>{value}</Grid>
      </Grid>
    </Grid>
  );

  return (
    <Grid container spacing={2}>
      <GridItem title="Bank Name :" value={data?.bankName ?? "-"} xs={12} />

      <GridItem
        title="Bank Alias Name :"
        value={data?.bankShortName ?? "-"}
        xs={12}
      />

      {/* -----Transfer Type----- */}
      <Grid item xs={12} className={classes.title}>
        Transfer Type :
      </Grid>
      <Grid item xs={12} className={classes.value}>
        <Grid container alignItems="center">
          {!data?.transferOnlineAllowed &&
            !data?.transferSknAllowed &&
            !data?.transferRtgsAllowed &&
            !data?.transferBiFastAllowed && (
              <Grid item style={{ paddingBottom: paddingBottom() }}>
                -
              </Grid>
            )}

          {data?.transferOnlineAllowed && (
            <GridTransferTypes
              label="Online"
              type="blue"
              value={data?.bankCode ?? "-"}
            />
          )}

          {data?.transferSknAllowed && (
            <GridTransferTypes
              label="SKN"
              type="orange"
              value={data?.sknCode ?? "-"}
            />
          )}

          {data?.transferRtgsAllowed && (
            <GridTransferTypes
              label="RTGS"
              type="red"
              value={data?.rtgsCode ?? "-"}
            />
          )}

          {data?.transferBiFastAllowed && (
            <GridTransferTypes
              label="BI Fast"
              type="purple"
              value={data?.biFastCode ?? "-"}
            />
          )}
        </Grid>
      </Grid>

      <GridItem title="BIC :" value={data?.swiftCode ?? "-"} xs={12} />

      <GridItem
        title="Online Switching :"
        value={
          data?.bankNetwork === "ALT"
            ? "ALTO"
            : data?.bankNetwork === "BCA"
            ? "PRIMA"
            : data?.bankNetwork === "SAT"
            ? "ARTAJASA"
            : data?.bankNetwork === "AMP"
            ? "JALIN"
            : data?.bankNetwork ?? "-"
        }
        xs={12}
      />

      <GridItem
        title="Status :"
        value={data?.deleted ? "Inactive" : "Active" || "-"}
        xs={12}
      />
    </Grid>
  );
}
