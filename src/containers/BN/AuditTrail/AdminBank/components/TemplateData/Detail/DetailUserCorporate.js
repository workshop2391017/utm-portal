import React from "react";

import { Grid } from "@material-ui/core";
import styled from "styled-components";

import Badge from "components/BN/Badge";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

import { formatPhoneNumber, handleConvertRole } from "utils/helpers";

const HeavyText = styled.span`
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: 16px;
  letter-spacing: 0.03em;
  color: #374062;
`;

const MediumText = styled.span`
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 15px;
  letter-spacing: 0.01em;
  color: #374062;
`;

const LightText = styled.span`
  font-family: FuturaBkBT;
  font-weight: 400;
  font-size: 13px;
  color: #7b87af;
`;

export default function DetailUserCorporate({ data }) {
  const options = [
    {
      title: "Id User :",
      value: data?.userId ?? "-",
    },
    {
      title: "Full Name :",
      value: data?.fullName ?? "-",
    },
    {
      title: "KTP Number :",
      value: data?.identityNumber ?? "-",
    },
    {
      title: "Email :",
      value: data?.email ?? "-",
    },
    {
      title: "Mobile Phone Number/HP :",
      value: formatPhoneNumber(data?.phoneNumber, 18) ?? "-",
    },
  ];

  const renderBadge = (status) =>
    status === 0
      ? {
          label: "Active",
          type: "green",
          styleBadge: { width: 53, textAlign: "center" },
        }
      : {
          label: "Inactive",
          type: "red",
          styleBadge: { width: 57, textAlign: "center" },
        };

  return (
    <Grid container direction="column" spacing={2}>
      <Grid container item xs>
        <Grid container direction="column" item xs={4}>
          <Grid item xs>
            <MediumText>Role : {handleConvertRole(data?.roleCode)}</MediumText>
          </Grid>
          <Grid item xs>
            <Badge
              type={renderBadge().type}
              label={renderBadge().label}
              styleBadge={renderBadge().styleBadge}
            />
          </Grid>
        </Grid>
        <Grid container direction="column" item xs={4}>
          <Grid item xs>
            <MediumText>Approval Level :</MediumText>
          </Grid>
          <Grid item xs>
            <MediumText>
              {data?.workflowLimitSchemeName
                ? `Level ${data?.workflowLimitSchemeName}`
                : "-"}
            </MediumText>
          </Grid>
        </Grid>
        <Grid container direction="column" item xs={4}>
          <Grid item xs>
            <MediumText>User Group :</MediumText>
          </Grid>
          <Grid item xs>
            <Badge
              type="blue"
              label={data?.workflowGroupName}
              styleBadge={{
                width: `${
                  data?.workflowGroupName?.length <= 10
                    ? `${data?.workflowGroupName?.length * 8.5}px`
                    : data?.workflowGroupName?.length >= 11 &&
                      data?.workflowGroupName?.length <= 13
                    ? `${data?.workflowGroupName?.length * 8}px`
                    : data?.workflowGroupName?.length >= 14 &&
                      data?.workflowGroupName?.length <= 50
                    ? `${data?.workflowGroupName?.length * 7.5}px`
                    : undefined
                }`,
                textAlign: "center",
              }}
            />
          </Grid>
        </Grid>
      </Grid>

      {/* -----User Data----- */}
      <Grid item xs>
        <HeavyText>User Data</HeavyText>
      </Grid>
      <Grid container item xs spacing={2}>
        {options.map((el, i) => (
          <Grid key={i} container direction="column" item xs={4}>
            <Grid item xs="auto">
              <LightText>{el.title}</LightText>
            </Grid>
            <Grid item xs style={{ wordWrap: "break-word" }}>
              <MediumText>{el.value}</MediumText>
            </Grid>
          </Grid>
        ))}
      </Grid>

      {/* -----Notification Settings----- */}
      <Grid item xs>
        <HeavyText>Notification Settings</HeavyText>
      </Grid>
      <Grid container direction="column" item xs>
        <Grid item xs>
          <LightText>Notifications are active if :</LightText>
        </Grid>
        {!data?.isGetNewTask && !data?.isGetTransactionProcessed && (
          <Grid item xs>
            <MediumText>-</MediumText>
          </Grid>
        )}
        {data?.isGetNewTask && (
          <Grid container item xs spacing={1} style={{ marginLeft: 16 }}>
            <Grid item xs="auto">
              <BlueSphere />
            </Grid>
            <Grid item xs>
              <MediumText>Got a New Assignment</MediumText>
            </Grid>
          </Grid>
        )}
        {data?.isGetTransactionProcessed && (
          <Grid container item xs spacing={1} style={{ marginLeft: 16 }}>
            <Grid item xs="auto">
              <BlueSphere />
            </Grid>
            <Grid item xs>
              <MediumText>Transaction Processed</MediumText>
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
}
