import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

import { formatAmountDot } from "utils/helpers";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
    overflowWrap: "anywhere",
  },
  h1: {
    margin: "20px 0px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.03em",
    color: "#7B87AF",
  },
  businessFieldsItem: {
    gap: "10px",
  },
  margin: {
    margin: "25px 0px 8px 0px",
  },
});

export default function ParameterSettings({ data }) {
  const classes = useStyles();

  return (
    <Grid container>
      {/* TYPE & SEGMENTATION */}
      <Grid item xs={4}>
        <Grid container spacing={1} direction="column">
          <Grid item classes={{ root: classes.title }}>
            Type :
          </Grid>
          <Grid item classes={{ root: classes.value }}>
            {data?.type ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={4}>
        <Grid container spacing={1} direction="column">
          <Grid item classes={{ root: classes.title }}>
            Segmentation :
          </Grid>
          <Grid item classes={{ root: classes.value }}>
            {data?.segmentation?.name ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* H1 */}
      <Grid item xs={12} classes={{ root: classes.h1 }}>
        Parameter
      </Grid>

      {/* AVERAGE BALANCE, AGE & POSITION */}
      <Grid item xs={4}>
        <Grid container spacing={1} direction="column">
          <Grid item classes={{ root: classes.title }}>
            Average Balance :
          </Grid>
          <Grid item classes={{ root: classes.value }}>
            {formatAmountDot(data?.averageBalance.toString()) ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={4}>
        <Grid container spacing={1} direction="column">
          <Grid item classes={{ root: classes.title }}>
            Age :
          </Grid>
          <Grid item classes={{ root: classes.value }}>
            {data?.age1 && data?.age2 ? data?.age1 : null}
            {data?.age1 && data?.age2 ? " - " : "-"}
            {data?.age1 && data?.age2 ? data?.age2 : null}
            {data?.age1 && data?.age2 ? " Years" : null}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={4}>
        <Grid container spacing={1} direction="column">
          <Grid item classes={{ root: classes.title }}>
            Position :
          </Grid>
          <Grid item classes={{ root: classes.value }}>
            {data?.division ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* BUSINESS FIELDS */}
      <Grid
        item
        xs={12}
        classes={{ root: `${classes.title} ${classes.margin}` }}
      >
        Business Fields :
      </Grid>

      <Grid item xs={8}>
        <Grid container>
          {data?.businessFieldList?.map((el) => (
            <Grid item xs={6} key={`${el?.name}${el?.id}`}>
              <Grid
                container
                justify="flex-start"
                alignItems="center"
                classes={{ root: classes.businessFieldsItem }}
              >
                <Grid item>
                  <BlueSphere />
                </Grid>
                <Grid item>{el?.name}</Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
}
