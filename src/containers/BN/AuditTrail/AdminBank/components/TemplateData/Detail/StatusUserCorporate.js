import React from "react";

import { Grid } from "@material-ui/core";
import styled from "styled-components";

import { handleConvertRole } from "utils/helpers";

const LightText = styled.span`
  font-family: FuturaBkBT;
  font-weight: 400;
  font-size: 13px;
  color: #7b87af;
`;

const MediumText = styled.span`
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 15px;
  letterspacing: 0.01em;
  color: #374062;
`;

export default function StatusUserCorporate({ data }) {
  const options = [
    {
      label: "User ID :",
      value: data?.detailUser?.userId ?? "-",
    },
    {
      label: "Full Name :",
      value: data?.detailUser?.fullName ?? "-",
    },
    {
      label: "Email :",
      value: data?.detailUser?.email ?? "-",
    },
    {
      label: "Role :",
      value: handleConvertRole(data?.detailUser?.roleCode),
    },
    {
      label: "Status :",
      value: data?.detailUser?.status === 4 ? "Inactive" : "Active",
    },
  ];

  return (
    <Grid container direction="column" spacing={2}>
      {options.map((el, i) => (
        <Grid key={i} container direction="column" item xs>
          <Grid item xs>
            <LightText>{el.label}</LightText>
          </Grid>
          <Grid item xs>
            <MediumText>{el.value}</MediumText>
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
}
