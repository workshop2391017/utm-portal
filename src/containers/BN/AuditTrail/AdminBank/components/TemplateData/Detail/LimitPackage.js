import React, { Fragment, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import formatNumber from "helpers/formatNumber";

const useStyles = makeStyles({
  thead: {
    background: "#EAF2FF !important",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#374062 !important",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  subtitle: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "12px",
    color: "#BCC8E7",
  },
});

export default function LimitPackage({ data }) {
  const classes = useStyles();

  const [dataTable, setDataTable] = useState([]);

  const getGroupingData = (dataCompare, compareKey, dataArray) =>
    dataArray.filter((item) => dataCompare === item[compareKey]);

  useEffect(() => {
    let res = data?.globalLimit?.map((el) => ({
      name: el.serviceName,
      child: getGroupingData(
        el.serviceName,
        "serviceName",
        data?.globalLimit
      )?.map((el) => ({
        currencyMatrixName: el?.serviceCurrencyMatrixName,
        currencyCode: el?.currencyCode,
        maxTransaction: el?.maxTransaction?.toString()?.includes(".")
          ? `${formatNumber(el?.maxTransaction?.split(".")[0])}`
          : `${formatNumber(el?.maxTransaction)}`,
        minAmountTransaction: el?.minAmountTransaction?.includes(".")
          ? `${formatNumber(el?.minAmountTransaction?.split(".")[0])}`
          : `${formatNumber(el?.minAmountTransaction)}`,
        maxAmountTransaction: el?.maxAmountTransaction?.includes(".")
          ? `${formatNumber(el?.maxAmountTransaction?.split(".")[0])}`
          : `${formatNumber(el?.maxAmountTransaction)}`,
        maxAmountTransactionOfDay: el?.maxAmountTransactionOfDay?.includes(".")
          ? `${formatNumber(el?.maxAmountTransactionOfDay?.split(".")[0])}`
          : `${formatNumber(el?.maxAmountTransactionOfDay)}`,
      })),
    }));

    res = res.filter(
      (val, i, arr) => i === arr.findIndex((el) => el.name === val.name)
    );

    setDataTable(res);
  }, [data]);

  const dataHeader = [
    {
      title: "Currency Matrix",
      key: "currencyMatrixName",
    },
    {
      title: "Currency",
      key: "currencyCode",
    },
    {
      title: "Number of Transaction",
      key: "maxTransaction",
    },
    {
      title: "Minimum Transaction Amount",
      key: "minAmountTransaction",
    },
    {
      title: "Maximum Nominal Transaction",
      key: "maxAmountTransaction",
    },
    {
      title: "Maximum Nominal / Day",
      key: "maxAmountTransactionOfDay",
    },
  ];

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.title}>
            {data?.limitPackage?.name ?? "-"}
          </Grid>
          <Grid item className={classes.subtitle}>
            {data?.limitPackage?.code ?? "-"}
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <TableICBBCollapse
          headerContent={dataHeader}
          dataContent={dataTable}
          selecable={false} // hide selecable
          tableRounded={false}
          collapse={false} // hide icon collapse
          defaultOpenCollapseBreakdown // open collapse default
          headerClassName={classes.thead}
        />
      </Grid>
    </Grid>
  );
}
