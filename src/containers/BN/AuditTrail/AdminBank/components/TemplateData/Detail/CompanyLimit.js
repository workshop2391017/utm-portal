import React, { Fragment, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import Badge from "components/BN/Badge";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import formatNumber from "helpers/formatNumber";

const useStyles = makeStyles({
  thead: {
    background: "#EAF2FF !important",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#374062 !important",
  },
  margin: {
    marginBottom: "24px",
    marginTop: "8px",
  },
  companyName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    lineHeight: "24px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  companyId: {
    fontFamily: "FuturaBQ",
    fontWeight: 500,
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.03em",
    color: "#7B87AF",
  },
});

export default function CompanyLimit({ data }) {
  const classes = useStyles();

  const [dataTable, setDataTable] = useState([]);

  const getGroupingData = (dataCompare, compareKey, dataArray) =>
    dataArray.filter(
      (item) =>
        dataCompare === item?.serviceCurrencyMatrix?.service?.[compareKey]
    );

  useEffect(() => {
    let res = data?.globalLimits?.map((el) => ({
      name: el?.serviceCurrencyMatrix?.service?.name,
      child: getGroupingData(
        el?.serviceCurrencyMatrix?.service?.name,
        "name",
        data?.globalLimits
      )?.map((el) => ({
        currencyMatrixName: el?.serviceCurrencyMatrix?.currencyMatrix?.name,
        maxTransaction: `${formatNumber(el?.maxTransaction)}`,
        currencyCode: el?.currencyCode,
        maxAmountTransaction: `${formatNumber(el?.maxAmountTransaction)}`,
      })),
    }));

    res = res.filter(
      (val, i, arr) => i === arr.findIndex((el) => el.name === val.name)
    );

    setDataTable(res);
  }, [data]);

  const dataHeader = [
    {
      title: "Currency Matrix",
      key: "currencyMatrixName",
    },
    {
      title: "Number of Transaction",
      key: "maxTransaction",
    },
    {
      title: "Currency",
      key: "currencyCode",
    },
    {
      title: "Max Transaction",
      key: "maxAmountTransaction",
    },
  ];

  return (
    <Fragment>
      <Grid
        container
        direction="column"
        classes={{ root: classes.margin }}
        spacing={2}
      >
        <Grid item classes={{ root: classes.companyName }}>
          {data?.companyLimits?.corporateName}
        </Grid>
        <Grid item>
          <Grid container alignItems="center" spacing={2}>
            <Grid item classes={{ root: classes.companyId }}>
              {data?.companyLimits?.corporateProfileId}
            </Grid>
            <Grid item classes={{ root: classes.companyId }}>
              {/* SEGMENTATION NAME KETINGGALAN DARI BE */}
              <Badge type="blue" label={data?.segmentation} outline />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <TableICBBCollapse
        headerContent={dataHeader}
        dataContent={dataTable}
        selecable={false} // hide selecable
        tableRounded={false}
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown // open collapse default
        headerClassName={classes.thead}
      />
    </Fragment>
  );
}
