import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  bullets: {
    gap: "10px",
  },
  marginBottom: {
    marginBottom: "8px",
  },
  marginLeft: {
    marginLeft: "30px",
  },
  segmentBranchName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    lineHeight: "24px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  segmentBranchCode: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "18px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
});

export default function SegmentationBranch({ data }) {
  const classes = useStyles();

  return (
    <Grid container>
      {/* SEGMENT BRANCH NAME & CODE */}
      <Grid
        container
        alignItems="center"
        spacing={2}
        className={classes.marginBottom}
      >
        <Grid item className={classes.segmentBranchName}>
          {data?.segmentBranch?.name ?? "-"}
        </Grid>
        <Grid item className={classes.segmentBranchCode}>
          {data?.segmentBranch?.code ?? "-"}
        </Grid>
      </Grid>

      {/* BRANCHES */}
      {data?.branch?.map(
        (parent, i) =>
          parent?.child?.length !== 0 && (
            <Grid container spacing={1} key={`${parent?.branchId}${i}`}>
              <Grid item xs={12} className={classes.value}>
                {parent?.name ?? "-"}
              </Grid>

              <Grid item xs={12}>
                <Grid container spacing={1}>
                  {parent?.child?.map((el, i) => (
                    <Grid item xs={6} key={`${el?.name}${el?.branchId}`}>
                      <Grid
                        container
                        justify="flex-start"
                        alignItems="center"
                        className={
                          i % 2 === 0
                            ? `${classes.bullets} ${classes.marginLeft}`
                            : classes.bullets
                        }
                      >
                        <Grid item>
                          <BlueSphere />
                        </Grid>
                        <Grid item className={classes.value}>
                          {el?.name ?? "-"}
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}

                  {i !== data?.branch?.length - 1 && (
                    <Grid item xs={12}>
                      <hr
                        style={{
                          border: "1px solid #EAF2FF",
                          borderTop: "0px",
                        }}
                      />
                    </Grid>
                  )}
                </Grid>
              </Grid>
            </Grid>
          )
      )}
    </Grid>
  );
}
