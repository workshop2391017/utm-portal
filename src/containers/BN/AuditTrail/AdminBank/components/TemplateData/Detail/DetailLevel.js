import React from "react";

import styled from "styled-components";

import { Grid } from "@material-ui/core";

import { formatAmountDot } from "utils/helpers";

const Label = styled.span`
  font-family: FuturaBkBT;
  font-weight: 400;
  font-size: 13px;
  color: #7b87af;
`;

const Value = styled.span`
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 15px;
  color: #374062;
`;

export default function DetailLevel({ data }) {
  const options = [
    {
      label: "Level Name :",
      value: data?.name ?? "-",
    },
    {
      label: "Currency :",
      value: data?.currency ?? "-",
    },
    {
      label: "Limit Maker :",
      value: formatAmountDot(data?.limitMaker?.toString()) ?? "-",
    },
    {
      label: "Single Limit Approval :",
      value: formatAmountDot(data?.limitSingleApproval?.toString()) ?? "-",
    },
    {
      label: "Same Group Approval Limit :",
      value: formatAmountDot(data?.limitIngroupApproval?.toString()) ?? "-",
    },
    {
      label: "Different Group Approval Limits:",
      value:
        formatAmountDot(data?.limitOutsidegroupApproval?.toString()) ?? "-",
    },
  ];

  console.warn("masuk detail level.");
  return (
    <Grid container direction="column" spacing={2}>
      {options.map((el, i) => (
        <Grid key={i} container direction="column" item xs spacing={1}>
          <Grid item xs>
            <Label>{el.label}</Label>
          </Grid>
          <Grid item xs>
            <Value>{el.value}</Value>
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
}
