import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/styles";

// components
import { Card, CardHeader, CardContent, Grid } from "@material-ui/core";
// helpers
import Colors from "helpers/colors";

const useStyles = makeStyles((theme) => ({
  row: {
    display: "flex",
    flexDirection: "row",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  card: {
    borderRadius: 10,
    padding: 20,
  },
  firstCard: {
    display: "flex",
    position: "relative",
  },
  firstCard__title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 20,
    color: "#374062",
  },
  firstCard__subtitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 13,
    color: "#7B87AF",
  },
  thirdCardItem: {
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  thirCardItemHeader: {
    background: Colors.primary.hard,
    color: Colors.white,

    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  level: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  groupLabel: {
    fontFamily: "FuturaHvBT",
    fontSize: 16,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  label: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  featureList: {
    "& li": {
      listStylePosition: "outside",
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 15,
      color: Colors.dark.hard,
    },
  },
}));

export default function DetailLimit({ data }) {
  const classes = useStyles();
  return (
    <div className={classes.column} style={{ gap: 10 }}>
      <h2 className={classes.level}>{data.level}</h2>
      {data.groupList.map((list) => (
        <Fragment>
          <h3 className={classes.groupLabel}>{list.groupLabel}</h3>
          {list.groupItems.map((item) => (
            <div style={{ marginLeft: 20 }}>
              <h4 className={classes.label}>{item.label} :</h4>
              <p className={classes.value}>{item.value}</p>
            </div>
          ))}
        </Fragment>
      ))}
    </div>
  );
}
