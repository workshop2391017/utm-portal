import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  column: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function GeneralParameter({ data }) {
  const classes = useStyles();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Module :</span>
        <span className={classes.value}>
          {data?.systemParameterList?.module ?? "-"}
        </span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Value :</span>
        <span className={classes.value}>
          {data?.systemParameterList?.value ?? "-"}
        </span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Code :</span>
        <span className={classes.value}>
          {data?.systemParameterList?.name ?? "-"}
        </span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Description :</span>
        <span className={classes.value}>
          {data?.systemParameterList?.description ?? "-"}
        </span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Encryption :</span>
        <span className={classes.value}>
          {data?.systemParameterList.isEncrypted === undefined
            ? "-"
            : data?.systemParameterList?.isEncrypted
            ? "Yes"
            : "No"}
        </span>
      </Grid>
    </Grid>
  );
}
