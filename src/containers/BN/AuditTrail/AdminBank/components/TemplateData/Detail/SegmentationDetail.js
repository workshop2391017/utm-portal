import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function SegmentationDetail({ data }) {
  const classes = useStyles();

  const GridItem = ({ spacing, title, value }) => (
    <Grid item xs={spacing}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.title}>
          {title}
        </Grid>
        <Grid item className={classes.value}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <Grid container direction="column" spacing={3}>
      {/* -----Name & Description----- */}
      <Grid item>
        <Grid container>
          <GridItem
            spacing={4}
            title="Segmentation Name :"
            value={data?.name && data?.name !== "" ? data?.name : "-"}
          />
          <GridItem
            spacing={8}
            title="Segmentation Description :"
            value={
              data?.description && data?.description !== ""
                ? data?.description
                : "-"
            }
          />
        </Grid>
      </Grid>

      {/* -----Product Account Type, Package Limits, Menu Package----- */}
      <Grid item>
        <Grid container>
          <GridItem
            spacing={4}
            title="Account Type :"
            value={
              data?.jenisProductId?.name && data?.jenisProductId?.name !== ""
                ? data?.jenisProductId?.name
                : "-"
            }
          />
          <GridItem
            spacing={4}
            title="Limit Package :"
            value={
              data?.limitPackageId?.name && data?.limitPackageId?.name !== ""
                ? data?.limitPackageId?.name
                : "-"
            }
          />
          <GridItem
            spacing={4}
            title="Menu Package :"
            value={
              data?.menuPackageId?.name && data?.menuPackageId?.name !== ""
                ? data?.menuPackageId?.name
                : "-"
            }
          />
        </Grid>
      </Grid>

      {/* -----Charge Packages, Special Menu, Branch Segment----- */}
      <Grid item>
        <Grid container>
          <GridItem
            spacing={4}
            title="Charge Package :"
            value={
              data?.chargePackageId?.name && data?.chargePackageId?.name !== ""
                ? data?.chargePackageId?.name
                : "-"
            }
          />
          <GridItem
            spacing={4}
            title="Special Menu :"
            value={
              data?.menuKhususId?.name && data?.menuKhususId?.name !== ""
                ? data?.menuKhususId?.name
                : "-"
            }
          />
          <GridItem
            spacing={4}
            title="Branch Segment :"
            value={
              data?.segmentBranchId?.name && data?.segmentBranchId?.name !== ""
                ? data?.segmentBranchId?.name
                : "-"
            }
          />
        </Grid>
      </Grid>

      {/* -----Advanced Parameters----- */}
      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.title}>
            Advanced Parameters :
          </Grid>
          {data?.saveParameterAdvance?.length ? (
            data?.saveParameterAdvance?.map((el, i) => (
              <Grid item className={classes.value} key={i}>
                {el?.accountNumber}
              </Grid>
            ))
          ) : (
            <Grid item className={classes.value}>
              -
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
}
