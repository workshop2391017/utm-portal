import React, { Fragment } from "react";

import { Grid } from "@material-ui/core";
import styled from "styled-components";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const LightText = styled.span`
  font-family: FuturaBkBT;
  font-weight: 400;
  font-size: 13px;
  color: #7b87af;
`;

const MediumText = styled.span`
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 15px;
  letter-spacing: 0.01em;
  color: #374062;
`;

export default function AccountProductDetail({ data }) {
  return (
    <Grid container direction="column" spacing={2}>
      <Grid item xs="auto">
        <LightText>Account Products :</LightText>
      </Grid>

      <Grid container item xs="auto">
        {data?.accountProductResponseList?.map((parent, i) => (
          <Fragment key={i}>
            <Grid item xs={12} style={{ marginBottom: 8 }}>
              <MediumText>
                {parent?.accountType === "SA"
                  ? "Savings"
                  : parent?.accountType === "CA"
                  ? "Current Account :"
                  : parent?.accountType === "SA"
                  ? "Savings :"
                  : parent?.accountType === "LN"
                  ? "Loan :"
                  : "Deposit"}
              </MediumText>
            </Grid>
            {parent?.accountProducts?.map((child, i) => (
              <Grid
                key={`${child?.id}${i}`}
                container
                item
                xs={4}
                spacing={1}
                style={{ paddingLeft: 16, marginBottom: 8 }}
              >
                <Grid item xs="auto">
                  <BlueSphere />
                </Grid>
                <Grid item xs>
                  <MediumText>{child?.productName ?? "-"}</MediumText>
                </Grid>
              </Grid>
            ))}
          </Fragment>
        ))}
      </Grid>
    </Grid>
  );
}
