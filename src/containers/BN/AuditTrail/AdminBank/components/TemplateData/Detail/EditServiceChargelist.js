import React, { useState, Fragment } from "react";
import { Grid, makeStyles } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  row1: {
    background: "#EAF2FF",
    width: "100%",
    height: "37px",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#374062",
    display: "flex",
    alignItems: "center",
  },
  row2: {
    width: "100%",
    height: "46px",
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.03em",
    color: "#374062",
    display: "flex",
    alignItems: "center",
  },
});

export default function EditServiceChargelist({ data }) {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item xs={6} className={classes.row1}>
        <span>Service Name</span>
      </Grid>
      <Grid item xs={6} className={classes.row1}>
        <span>Charge List Name</span>
      </Grid>
      {data?.map((el, i) => (
        <Fragment key={i}>
          <Grid item xs={6} className={classes.row2}>
            <span>{el?.serviceId?.name || el?.service?.name}</span>
          </Grid>
          <Grid item xs={6} style={{ height: "46px" }}>
            <div style={{ display: "flex", gap: "10px", flexWrap: "wrap" }}>
              {el?.chargesList?.map((el, i) => (
                <div
                  key={i}
                  style={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px",
                  }}
                >
                  <BlueSphere />
                  <span className={classes.row2}>{el?.chargeName}</span>
                </div>
              ))}
            </div>
          </Grid>
        </Fragment>
      ))}
    </Grid>
  );
}
