import React, { Fragment } from "react";

import { Grid } from "@material-ui/core";
import styled from "styled-components";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const LightText = styled.span`
  font-family: FuturaBkBT;
  font-weight: 400;
  font-size: 13px;
  letter-spacing: 0.01em;
  color: #7b87af;
`;

const MediumText = styled.span`
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 15px;
  letter-spacing: 0.01em;
  color: #374062;
`;

const HeavyText = styled.span`
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: 20px;
  letter-spacing: 0.03em;
  color: #374062;
`;

export default function MenuPackage({ data }) {
  return (
    <Grid container direction="column" spacing={2}>
      <Grid container item xs="auto" spacing={1} alignItems="center">
        <Grid item xs="auto">
          <HeavyText>{data?.menuPackage?.name ?? "-"}</HeavyText>
        </Grid>
        <Grid item xs="auto">
          <LightText>{data?.menuPackage?.code ?? "-"}</LightText>
        </Grid>
      </Grid>

      <Grid container item xs="auto">
        {data?.masterMenuList?.map(
          (parent, i) =>
            parent?.name_en !== "Account" && (
              <Fragment key={i}>
                <Grid item xs={12} style={{ marginBottom: 8 }}>
                  <MediumText>{parent?.name_en ?? "-"}</MediumText>
                </Grid>
                {parent?.menuAccessChild?.map((child, i) => (
                  <Grid
                    key={`${child?.id}${i}`}
                    container
                    item
                    xs={4}
                    spacing={1}
                    style={{ paddingLeft: 16, marginBottom: 8 }}
                  >
                    <Grid item xs="auto">
                      <BlueSphere />
                    </Grid>
                    <Grid item xs>
                      <MediumText>{child?.name_en ?? "-"}</MediumText>
                    </Grid>
                  </Grid>
                ))}
              </Fragment>
            )
        )}
      </Grid>
    </Grid>
  );
}
