import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { formatAmountDot } from "utils/helpers";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  h1: {
    margin: "20px 0px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.03em",
    color: "#7B87AF",
  },
  marginBottom: {
    marginBottom: "8px",
  },
});

export default function ParameterAdvance({ data }) {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item xs={12}>
        <Grid container alignItems="center" spacing={1}>
          <Grid item classes={{ root: classes.title }}>
            Type :
          </Grid>
          <Grid item classes={{ root: classes.value }}>
            {data[0]?.type ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={12} classes={{ root: classes.h1 }}>
        Details
      </Grid>

      {data.map((el, i) => (
        <Fragment>
          <Grid container>
            <Grid item xs={6} classes={{ root: classes.marginBottom }}>
              <Grid container direction="column" spacing={1}>
                <Grid item classes={{ root: classes.title }}>
                  Account Number :
                </Grid>
                <Grid item classes={{ root: classes.value }}>
                  {el?.accountNumber}
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={6} classes={{ root: classes.marginBottom }}>
              <Grid container direction="column" spacing={1}>
                <Grid item classes={{ root: classes.title }}>
                  Age :
                </Grid>
                <Grid item classes={{ root: classes.value }}>
                  {el?.age}
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={6} classes={{ root: classes.marginBottom }}>
              <Grid container direction="column" spacing={1}>
                <Grid item classes={{ root: classes.title }}>
                  Average Balance :
                </Grid>
                <Grid item classes={{ root: classes.value }}>
                  {formatAmountDot(el?.averageBalance.toString())}
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={6} classes={{ root: classes.marginBottom }}>
              <Grid container direction="column" spacing={1}>
                <Grid item classes={{ root: classes.title }}>
                  Business Field :
                </Grid>
                <Grid item classes={{ root: classes.value }}>
                  {el?.businessField}
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={6} classes={{ root: classes.marginBottom }}>
              <Grid container direction="column" spacing={1}>
                <Grid item classes={{ root: classes.title }}>
                  Position :
                </Grid>
                <Grid item classes={{ root: classes.value }}>
                  {el?.division}
                </Grid>
              </Grid>
            </Grid>

            {i !== data?.length - 1 && (
              <Grid item xs={12}>
                <hr style={{ border: "1px solid #EAF2FF", borderTop: "0px" }} />
              </Grid>
            )}
          </Grid>
        </Fragment>
      ))}
    </Grid>
  );
}
