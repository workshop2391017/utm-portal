import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import { formatAmountDot } from "utils/helpers";

import TableICBB from "components/BN/TableIcBB";

const useStyles = makeStyles({
  name: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  code: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
  rowData: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
});

export default function TieringDetail({ data }) {
  const classes = useStyles();

  const isTransactionAmount =
    !data?.tieringAmountList[0]?.numberOfTransaction ||
    data?.tieringAmountList[0]?.numberOfTransaction === "";
  const isNumberOfTransaction =
    !data?.tieringAmountList[0]?.transactionAmount ||
    data?.tieringAmountList[0]?.transactionAmount === "";

  let dataHeader;
  const dataHeaderPicker = () => {
    dataHeader = [
      {
        title: "Number Of Transaction",
        key: "numberOfTransaction",
        render: (rowData) => (
          <div className={classes.rowData}>
            {rowData?.isGreaterNumberOfTransaction &&
            rowData?.isEqualNumberOfTransaction
              ? ">= "
              : rowData?.isGreaterNumberOfTransaction &&
                !rowData?.isEqualNumberOfTransaction
              ? "> "
              : !rowData?.isGreaterNumberOfTransaction &&
                rowData?.isEqualNumberOfTransaction
              ? "<= "
              : !rowData?.isGreaterNumberOfTransaction &&
                !rowData?.isEqualNumberOfTransaction
              ? "< "
              : null}
            {formatAmountDot(rowData?.numberOfTransaction?.toString())}
          </div>
        ),
      },
      {
        title: "Transaction Amount",
        key: "transactionAmount",
        render: (rowData) => (
          <div className={classes.rowData}>
            {rowData?.isGreaterTransactionAmount &&
            rowData?.isEqualTransactionAmount
              ? ">= "
              : rowData?.isGreaterTransactionAmount &&
                !rowData?.isEqualTransactionAmount
              ? "> "
              : !rowData?.isGreaterTransactionAmount &&
                rowData?.isEqualTransactionAmount
              ? "<= "
              : !rowData?.isGreaterTransactionAmount &&
                !rowData?.isEqualTransactionAmount
              ? "< "
              : null}
            {formatAmountDot(rowData?.transactionAmount?.toString())}
          </div>
        ),
      },
      {
        title: "Charge / Transaction (Rp)",
        key: "charge",
        render: (rowData) => (
          <div>
            {rowData?.charge === 0
              ? "0"
              : formatAmountDot(rowData?.charge?.toString())}
          </div>
        ),
      },
    ];

    if (isTransactionAmount) {
      dataHeader = dataHeader.filter((el, i) => i !== 0);
    }
    if (isNumberOfTransaction) {
      dataHeader = dataHeader.filter((el, i) => i !== 1);
    }
    return dataHeader;
  };

  return (
    <Grid container direction="column" spacing={2}>
      {/* -----Tiering Name & Code----- */}
      <Grid item>
        <Grid container alignItems="center" spacing={2}>
          <Grid item className={classes.name}>
            {data?.tieringName && data?.tieringName !== ""
              ? data?.tieringName
              : "-"}
          </Grid>
          <Grid item className={classes.code}>
            {data?.tieringCode && data?.tieringCode !== ""
              ? data?.tieringCode
              : "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* -----Tiering Type----- */}
      <Grid item className={classes.name} style={{ fontSize: 15 }}>
        {isTransactionAmount
          ? "Transaction Amount"
          : isNumberOfTransaction
          ? "Number of Transaction"
          : "All Types of Tiers"}
      </Grid>

      {/* Table */}
      <Grid item>
        <TableICBB
          headerContent={dataHeaderPicker()}
          dataContent={data?.tieringAmountList ?? []}
        />
      </Grid>
    </Grid>
  );
}
