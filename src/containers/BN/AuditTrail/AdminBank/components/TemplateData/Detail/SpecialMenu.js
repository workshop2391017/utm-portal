import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  code: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    letterSpacing: "0.01emm",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function SpecialMenu({ data }) {
  const classes = useStyles();

  return (
    <Grid container direction="column" spacing={2}>
      {/* -----Name & Code----- */}
      <Grid item>
        <Grid container alignItems="center" spacing={2}>
          <Grid item className={classes.name}>
            {data?.menuPackage?.name && data?.menuPackage?.name !== ""
              ? data?.menuPackage?.name
              : "-"}
          </Grid>
          <Grid item className={classes.code}>
            {data?.menuPackage?.code && data?.menuPackage?.code !== ""
              ? data?.menuPackage?.code
              : "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* -----Menu Package----- */}
      {data?.masterMenuList?.map((parent, i) => (
        <Grid item key={i}>
          <Grid container>
            <Grid
              item
              className={classes.value}
              xs={12}
              style={{ marginBottom: 8 }}
            >
              {parent?.name_en && parent?.name_en !== ""
                ? parent?.name_en
                : "-"}
            </Grid>
            {parent?.menuAccessChild?.map((child, i) => (
              <Grid
                item
                className={classes.value}
                xs={4}
                key={`${child?.id}${i}`}
              >
                <Grid
                  container
                  alignItems="center"
                  spacing={1}
                  style={{ marginLeft: i % 3 === 0 ? 16 : undefined }}
                >
                  <Grid item>
                    <BlueSphere />
                  </Grid>
                  <Grid item>
                    {child?.name_en && child?.name_en !== ""
                      ? child?.name_en
                      : "-"}
                  </Grid>
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
}
