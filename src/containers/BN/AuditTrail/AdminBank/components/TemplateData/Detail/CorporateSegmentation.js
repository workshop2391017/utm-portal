import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  subtitle: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "12px",
    color: "#BCC8E7",
  },
  newSegmentation: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  segmentationName: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function CorporateSegmentation({ data }) {
  const classes = useStyles();

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.title}>
            {data?.corporate?.corporateName ?? "-"}
          </Grid>
          <Grid item className={classes.subtitle}>
            {data?.corporate?.corporateAuthorization
              ? `${data?.corporate?.corporateAuthorization} Authorization`
              : "-"}
          </Grid>
        </Grid>
      </Grid>

      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.newSegmentation}>
            {data?.segmentationCurrentName
              ? "Current Segmentation :"
              : "New Segmentation :"}
          </Grid>
          <Grid item className={classes.segmentationName}>
            {data?.segmentationCurrentName
              ? data?.segmentationCurrentName
              : data?.segmentationNewName ?? "-"}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
