import React, { Fragment, useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import moment from "moment";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import Badge from "components/BN/Badge";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import { ReactComponent as CalendarBlue } from "assets/icons/BN/calendarblue.svg";
import { ReactComponent as IconDoc } from "assets/icons/BN/icondoc.svg";

import { getDataDownloadFile } from "stores/actions/companyCharge";

import formatNumber from "helpers/formatNumber";

const useStyles = makeStyles((theme) => ({
  badge: {
    ...theme.typography.subtitle1,
    gap: 12.5,
    padding: 5,
    borderRadius: 5,
    border: "none",
    letterSpacing: "0.03em",
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F4F7FB",
    color: "#0061A7",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  thead: {
    background: "#EAF2FF !important",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#374062 !important",
  },
  margin: {
    marginBottom: "24px",
    marginTop: "8px",
  },
  companyName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    lineHeight: "24px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  companyId: {
    fontFamily: "FuturaBQ",
    fontWeight: 500,
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.03em",
    color: "#7B87AF",
  },
}));

export default function CompanyCharge({ data }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [dataTable, setDataTable] = useState([]);

  const getGroupingData = (dataCompare, compareKey, dataArray) =>
    dataArray.filter((item) => dataCompare === item?.service?.[compareKey]);

  useEffect(() => {
    let res = data?.serviceChargePackage?.map((el) => ({
      name: el?.service?.name,
      child: getGroupingData(
        el?.service?.name,
        "name",
        data?.serviceChargePackage
      )?.map((el) => ({
        chargeType: el?.serviceCharge?.charges?.name ?? "-",
        currency: el?.currencyId ?? "-",
        fixedAmount: formatNumber(el?.fee) ?? "-",
        tieringSlab: el?.tiering?.name ?? "-",
      })),
    }));

    res = res.filter(
      (val, i, arr) => i === arr.findIndex((el) => el.name === val.name)
    );

    setDataTable(res);
  }, [data]);

  const dataHeader = [
    {
      title: "Charge Type",
      key: "chargeType",
    },
    {
      title: "Currency",
      key: "currency",
    },
    {
      title: "Fixed Amount",
      key: "fixedAmount",
    },
    {
      title: "Tiering/Slab",
      key: "tieringSlab",
    },
  ];
  console.log("ini data", data);
  const formatFrom = "YYYY-DD-MM HH:mm:ss";
  const formatTo = "DD/MM/YYYY";

  const startDate = moment(data?.companyCharge?.starEffectiveDate)?.format(
    "DD/MM/YYYY"
  );
  const endDate = moment(data?.companyCharge?.endEffectiveDate)?.format(
    "DD/MM/YYYY"
  );

  const date = `${startDate} - ${endDate}`;

  const file = data?.companyCharge?.file;
  const fileName = file?.split("private/kyc/file/")[1];

  const documentDownloadHandler = () => {
    dispatch(getDataDownloadFile(file, fileName));
  };

  return (
    <Fragment>
      <Grid
        container
        direction="column"
        classes={{ root: classes.margin }}
        spacing={2}
      >
        <Grid item classes={{ root: classes.companyName }}>
          {data?.corporateName}
        </Grid>
        <Grid item>
          <Grid container alignItems="center" spacing={2}>
            <Grid item classes={{ root: classes.companyId }}>
              {data?.corporateId}
            </Grid>
            <Grid item classes={{ root: classes.companyId }}>
              <Badge type="blue" label={data?.segmentation} outline />
            </Grid>
          </Grid>
        </Grid>
        <Grid container style={{ gap: 15 }}>
          {startDate && endDate && (
            <div className={classes.badge}>
              <CalendarBlue />
              <span>{date ?? ""}</span>
            </div>
          )}
          {file && (
            <button
              type="button"
              className={classes.badge}
              onClick={documentDownloadHandler}
              style={{ cursor: "pointer" }}
            >
              <IconDoc />
              <span>{fileName}</span>
            </button>
          )}
        </Grid>
      </Grid>
      <TableICBBCollapse
        headerContent={dataHeader}
        dataContent={dataTable}
        selecable={false} // hide selecable
        tableRounded={false}
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown // open collapse default
        headerClassName={classes.thead}
      />
    </Fragment>
  );
}
