import React, { Fragment } from "react";

import { makeStyles } from "@material-ui/styles";
import { Card, CardHeader, CardContent, Grid } from "@material-ui/core";

import { formatAmountDot } from "utils/helpers";

const useStyles = makeStyles({
  name: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  code: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
  border: {
    borderRadius: 10,
    border: "1px solid #E8EEFF",
  },
  bold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  borderBottom: {
    borderBottom: "1px solid #E8EEFF",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#374062",
    background: "#EAF2FF",
    padding: "8px 16px",
  },
  value: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#374062",
    padding: 8,
  },
});

export default function ChargePackage({ data }) {
  const classes = useStyles();

  const handleGrouping = (payload) => {
    const arr = [];

    payload.forEach((elm) => {
      const fIdx = arr.findIndex((e) => e.serviceId === elm.serviceId);

      if (fIdx >= 0) {
        arr[fIdx].child.push(elm);
      } else {
        const assign = { ...elm, child: [] };
        assign.child.push(elm);
        arr.push(assign);
      }
    });

    return arr;
  };

  const transactional = handleGrouping(
    (
      data?.serviceChargePackageMeta?.filter((e) =>
        e?.chargeGroup?.includes("TRANSACTIONAL GROUP")
      ) || []
    ).map((elm) => ({
      ...elm,
      name: elm?.serviceName,
    }))
  );

  const periodical = handleGrouping(
    (
      data?.serviceChargePackageMeta?.filter((e) =>
        e?.chargeGroup?.includes("PERIODICAL GROUP")
      ) || []
    ).map((elm) => ({
      ...elm,
      name: elm?.serviceName,
    }))
  );

  console.warn("periodical", periodical);
  console.warn("transactional", transactional);

  const Table = ({ tableHeader, array }) => (
    <Grid item>
      <Card elevation={0} className={classes.border}>
        {/* -----Table Head 1----- */}
        <CardHeader
          classes={{ title: classes.bold, root: classes.borderBottom }}
          title={tableHeader}
        />
        <CardContent style={{ padding: 0 }}>
          <Grid container direction="column">
            {/* -----Table Head 2----- */}
            <Grid item>
              <Grid container className={classes.title}>
                <Grid item xs={3}>
                  Charge Type
                </Grid>
                <Grid item xs={3}>
                  Currency
                </Grid>
                <Grid item xs={3}>
                  Fixed Amount
                </Grid>
                <Grid item xs={3}>
                  Tiering/Slab
                </Grid>
              </Grid>
            </Grid>

            {array.map((parent, i) => (
              <Fragment key={i}>
                <Grid
                  item
                  className={classes.bold}
                  style={{ padding: "8px 16px" }}
                >
                  {parent?.serviceName ?? "-"}
                </Grid>

                {/* -----Table Row----- */}
                <Grid item style={{ padding: 8 }}>
                  <Grid
                    container
                    className={`${classes.value} ${classes.border}`}
                  >
                    {parent.child.map((el, i) => (
                      <Fragment>
                        <Grid
                          item
                          xs={3}
                          style={{
                            marginBottom:
                              i !== parent.child.length - 1 ? 8 : undefined,
                          }}
                        >
                          {el?.chargeName ?? "-"}
                        </Grid>
                        <Grid
                          item
                          xs={3}
                          style={{
                            marginBottom:
                              i !== parent.child.length - 1 ? 8 : undefined,
                          }}
                        >
                          {el?.currencyId ?? "-"}
                        </Grid>
                        <Grid
                          item
                          xs={3}
                          style={{
                            marginBottom:
                              i !== parent.child.length - 1 ? 8 : undefined,
                          }}
                        >
                          {el?.tieringName
                            ? "-"
                            : formatAmountDot(el?.fee) ?? "-"}
                        </Grid>
                        <Grid
                          item
                          xs={3}
                          style={{
                            marginBottom:
                              i !== parent.child.length - 1 ? 8 : undefined,
                          }}
                        >
                          {el?.tieringName ?? "-"}
                        </Grid>
                      </Fragment>
                    ))}
                  </Grid>
                </Grid>
              </Fragment>
            ))}
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );

  return (
    <Grid container direction="column" spacing={3}>
      <Grid item>
        <Grid container alignItems="center" spacing={1}>
          <Grid item className={classes.name}>
            {data?.chargePackage?.name ?? "-"}
          </Grid>
          <Grid item className={classes.code}>
            {data?.chargePackage?.code ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* -----Periodical----- */}
      {periodical.length !== 0 && (
        <Table tableHeader="Periodical Charge List" array={periodical} />
      )}

      {/* -----Transactional----- */}
      {transactional.length !== 0 && (
        <Table tableHeader="Transactional Charge List" array={transactional} />
      )}
    </Grid>
  );
}
