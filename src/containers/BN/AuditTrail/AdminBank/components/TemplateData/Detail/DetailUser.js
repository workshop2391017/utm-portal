import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { handleConvertRole } from "utils/helpers";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function DetailUser({ data }) {
  const classes = useStyles();

  const options = [
    {
      title: "Username :",
      value: data?.username ?? "-",
    },
    {
      title: "Name :",
      value: data?.fullName ?? "-",
    },
    {
      title: "NIP :",
      value: data?.nip ?? "-",
    },
    {
      title: "Group :",
      value: data?.portalGroupId?.name ?? "-",
    },
    {
      title: "Email :",
      value: data?.email ?? "-",
    },
    {
      title: "Role :",
      value: handleConvertRole(data?.idRole?.name),
    },
    {
      title: "Office :",
      value: data?.idBranch?.name ?? "-",
    },
    {
      title: "IP Address :",
      value: data?.ipAddress ?? "-",
    },
  ];

  return (
    <Grid container direction="column" spacing={2}>
      {options.map((el, i) => (
        <Grid item key={i}>
          <Grid container direction="column" spacing={1}>
            <Grid item className={classes.title}>
              {el.title}
            </Grid>
            <Grid item className={classes.value}>
              {el.value}
            </Grid>
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
}
