import React, { Fragment } from "react";

import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import Collapse from "components/BN/Collapse";
import { formatPhoneNumber } from "utils/helpers";

const useStyles = makeStyles({
  gridTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
  gridValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
    overflowWrap: "anywhere",
    width: "97%",
  },
});

export default function CorporateDetail({ data }) {
  const classes = useStyles();

  const GridItem = ({ title, value, style, size = 12 }) => (
    <Grid item xs={size} style={style}>
      {title !== "Complete Address :" ? (
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.gridTitle}>
            {title}
          </Grid>
          <Grid item className={classes.gridValue}>
            {value}
          </Grid>
        </Grid>
      ) : (
        <Grid container>
          <Grid item xs={5}>
            <Grid container direction="column" spacing={1}>
              <Grid item className={classes.gridTitle}>
                {title}
              </Grid>
              <Grid item className={classes.gridValue}>
                {value}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}
    </Grid>
  );

  const businessFieldProfile = [
    {
      title: "Corporate ID :",
      value: data?.corporateId ?? "-",
      size: 6,
      style: { paddingLeft: 28 },
    },
    {
      title: "Business Name :",
      value: data?.corporateName ?? "-",
      size: 6,
    },
    {
      title: "Business Fields :",
      value: data?.corporateFieldName ?? "-",
      size: 12,
      style: { paddingLeft: 28 },
    },
    {
      title: "Complete Address :",
      value: data?.corporateAddress ?? "-",
      size: 12,
      style: { paddingLeft: 28 },
    },
    {
      title: "Province :",
      value: data?.province ?? "-",
      size: 3,
      style: { paddingLeft: 28 },
    },
    {
      title: "City/District :",
      value: data?.district ?? "-",
      size: 3,
    },
    {
      title: "District :",
      value: data?.subdistrictName ?? "-",
      size: 3,
    },
    {
      title: "Sub-district :",
      value: data?.villageName ?? "-",
      size: 3,
    },
    {
      title: "Postcode :",
      value: data?.postCode ?? "-",
      size: 12,
      style: { paddingLeft: 28 },
    },
    {
      title: "Business Phone Number :",
      value: formatPhoneNumber(data?.corporateContact, 18) ?? "-",
      size: 6,
      style: { paddingLeft: 28 },
    },
    {
      title: "Amount of Soft Token Required :",
      value:
        data?.numberOfToken && data?.numberOfToken !== ""
          ? `${data?.numberOfToken}`
          : "-",
      size: 6,
    },
  ];

  const personInCharge = [
    {
      title: "User ID :",
      value: data?.username ?? "-",
      size: 6,
    },
    {
      title: "Full Name :",
      value: data?.fullName ?? "-",
      size: 6,
    },
    {
      title: "Position :",
      value: data?.jobPosition ?? "-",
      size: 6,
    },
    {
      title: "Department :",
      value: data?.division ?? "-",
      size: 6,
    },
    {
      title: "KTP / KITAS Number :",
      value: data?.identityNumber ?? "-",
      size: 6,
    },
    {
      title: "NPWP Number :",
      value: data?.npwp ?? "-",
      size: 6,
    },
    {
      title: "Home Phone Number :",
      value:
        data?.phoneHome && data?.phoneHome !== ""
          ? formatPhoneNumber(data?.phoneHome, 18)
          : "-",
      size: 6,
    },
    {
      title: "Mobile Phone Number :",
      value:
        formatPhoneNumber(data?.phoneNumber, 18).replace(/^0+/g, "+62 ") ?? "-",
      size: 6,
    },
    {
      title: "Email :",
      value: data?.email ?? "-",
      size: 6,
    },
  ];

  return (
    <Fragment>
      <div style={{ marginBottom: 16 }} />
      <Collapse label="Business Field Profile" defaultOpen>
        <div style={{ marginBottom: 30 }} />

        <Grid container spacing={3}>
          {businessFieldProfile.map((el, i) => (
            <GridItem
              key={i}
              title={el.title}
              value={el.value}
              size={el.size}
              style={el.style}
            />
          ))}
        </Grid>

        <div style={{ marginBottom: "30px" }} />
      </Collapse>

      <Collapse label="User Profile" defaultOpen>
        <div style={{ marginBottom: "30px" }} />

        <Grid container spacing={3}>
          {personInCharge.map((el, i) => (
            <GridItem
              key={el.title}
              title={el.title}
              value={el.value}
              size={el.size}
              style={{ paddingLeft: i % 2 === 0 ? 28 : undefined }}
            />
          ))}
        </Grid>

        <div style={{ marginBottom: 30 }} />
      </Collapse>
    </Fragment>
  );
}
