import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  code: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    letterSpacing: "0.01emm",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  h1: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
});

export default function AccountTypeDetail({ data }) {
  const classes = useStyles();

  const [accountTypes, setAccountTypes] = useState([]);

  const getGroupingData = (dataCompare, compareKey, dataArray) =>
    dataArray.filter(
      (item) => dataCompare === item?.accountProduct[compareKey]
    );

  useEffect(() => {
    let res = data?.map((el) => ({
      name: el?.accountProduct?.accountType,
      child: getGroupingData(
        el?.accountProduct?.accountType,
        "accountType",
        data
      )?.map((el) => ({
        productName: el?.accountProduct?.productName,
        productCode: el?.accountProduct?.productCode,
      })),
    }));

    res = res.filter(
      (val, i, arr) => i === arr.findIndex((el) => el.name === val.name)
    );

    setAccountTypes(res);
  }, [data]);

  return (
    <Grid container direction="column" spacing={2}>
      {/* -----Name & Code----- */}
      <Grid item>
        <Grid container alignItems="center" spacing={2}>
          <Grid item className={classes.h1}>
            {data?.productTypeName ?? "-"}
          </Grid>
          <Grid item className={classes.code}>
            {data?.productTypeCode ?? "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* -----Account Types----- */}
      {accountTypes?.map((parent, i) => (
        <Grid item key={`${parent?.accountType}${i}`}>
          <Grid container>
            <Grid
              item
              className={classes.value}
              xs={12}
              style={{ marginBottom: 8 }}
            >
              {parent?.accountType === "CA"
                ? "Current Account :"
                : parent?.accountType === "SA"
                ? "Savings :"
                : parent?.accountType === "LN"
                ? "Loan :"
                : "Deposit"}
            </Grid>
            {parent?.child?.map((child, i) => (
              <Grid
                item
                className={classes.value}
                xs={4}
                key={`${child?.id}${i}`}
                style={{ marginBottom: 8 }}
              >
                <Grid
                  container
                  alignItems="center"
                  spacing={1}
                  style={{ marginLeft: i % 3 === 0 ? 16 : undefined }}
                >
                  <Grid item>
                    <BlueSphere />
                  </Grid>
                  <Grid item>
                    {`${child?.productName} - ${child?.productCode}` ?? "-"}
                  </Grid>
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
}
