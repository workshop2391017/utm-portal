import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  column: {
    display: "flex",
    flexDirection: "column",
    gap: "8px",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function ProductDetail({ data }) {
  const classes = useStyles();

  const [products, setProducts] = useState([]);

  const getGroupingData = (dataCompare, compareKey, dataArray) =>
    dataArray.filter((item) => dataCompare === item.accountProduct[compareKey]);

  useEffect(() => {
    let res = data?.map((el) => ({
      name: el?.accountProduct?.accountType,
      child: getGroupingData(
        el?.accountProduct?.accountType,
        "accountType",
        data
      )?.map((el) => ({
        productName: el?.accountProduct?.productName,
      })),
    }));

    res = res.filter(
      (val, i, arr) => i === arr.findIndex((el) => el.name === val.name)
    );

    setProducts(res);
  }, [data]);

  return (
    <Fragment>
      <Grid container spacing={2}>
        <Grid item xs={12} className={classes.column}>
          <span className={classes.title}>Name :</span>
          <span className={classes.value}>
            {data[0]?.productType?.name ?? "-"}
          </span>
        </Grid>
        <Grid item xs={12} className={classes.column}>
          <span className={classes.title}>Account Products :</span>
        </Grid>
        {products.map((parent, i) => (
          <Fragment key={i}>
            <Grid item xs={12} className={classes.value}>
              <span>{parent?.name === "SA" ? "Savings" : "Giro"}</span>
            </Grid>
            {parent.child.map((el, i) => (
              <Grid item xs={4} key={i}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px",
                    marginLeft: i % 3 === 0 ? "30px" : "0px",
                  }}
                >
                  <BlueSphere />
                  <span className={classes.value}>
                    {el?.productName ?? "-"}
                  </span>
                </div>
              </Grid>
            ))}
          </Fragment>
        ))}
      </Grid>
    </Fragment>
  );
}
