import React, { Fragment, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import styled from "styled-components";
import AntdTextField from "components/BN/TextField/AntdTextField";
import Colors from "helpers/colors";
import formatNumber from "helpers/formatNumber";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const Subtitle = styled.div`
  color: #0061a7;
  font-size: 15px;
  font-family: FuturaHvBT;
  font-weight: 400;
  margin-bottom: 10px;
`;
const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: FuturaBQ;
  font-weight: 500;
`;
const TextContent = styled.div`
  color: #374062;
  font-size: 15px;
  font-weight: 400;
  font-family: FuturaMdBT;
`;

const TitleContent = styled.div`
  color: #374062;
  font-size: 14px;
  font-family: FuturaHvBT;
  font-weight: 400;
  margin-bottom: 10px;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  card: {
    display: "flex",
    marginBottom: "20px",
  },
  cardItem: {
    paddingTop: "10px",
    width: "100%",
  },
  cardContent: {
    paddingLeft: "20px",
  },
});

export default function CorporateAccountConf({ data }) {
  const classes = useStyles();

  const [objData, setObjData] = useState({});

  useEffect(() => {
    if (data) {
      const obj = {};
      data?.accountLimits?.forEach((elm) => {
        obj[elm.transactionCategory] = elm.maxAmountTransaction;
      });

      setObjData(obj);
    }
  }, [data]);

  const defineAccount = {
    BILLPAYMENT: {
      parent_label: "Payment",
      label: "Payment Limits",
    },
    PURCHASE: {
      parent_label: "Purchase",
      label: "Purchase Limit",
    },
  };

  const defineTransfer = [
    {
      label: "In House Transfer",
      child: [
        {
          key: "TRANSFER_ON_US",
          label: "Bank Fellow Limits",
        },
      ],
    },
    {
      label: "Domestic Transfer",
      child: [
        {
          key: "TRANSFER_OA",
          label: "Online Limits",
        },
        {
          key: "TRANSFER_SKN",
          label: "SKN Limits",
        },
        {
          key: "TRANSFER_RTGS",
          label: "RTGS Limits",
        },
      ],
    },
  ];
  return (
    <div>
      <Title>{data?.productName ?? "-"}</Title>
      <div
        style={{
          display: "flex",
          marginBottom: "20px",
        }}
      >
        <Lable>{data?.accountNumber ?? "-"}</Lable>
      </div>
      <div style={{ width: 600, display: "flex", flexWrap: "wrap" }}>
        {Object.keys(defineAccount).map((elm) => (
          <div style={{ width: 300 }}>
            <Subtitle>{defineAccount[elm]?.parent_label}</Subtitle>
            <div className={classes.cardContent}>
              <Label>{defineAccount[elm]?.label}</Label>
              <TextContent>
                {objData[elm] ? `IDR ${formatNumber(objData[elm])}` : "-"}
              </TextContent>
            </div>
          </div>
        ))}
      </div>

      <div
        style={{ width: 600, display: "flex", flexWrap: "wrap", marginTop: 20 }}
      >
        {defineTransfer.map((elm) => (
          <div style={{ width: 300 }}>
            <Subtitle>{elm?.label}</Subtitle>
            <div
              style={{
                display: "flex",
                flex: 1,
                flexWrap: "wrap",
              }}
            >
              {elm?.child?.map((cElm) => (
                <div
                  className={classes.cardContent}
                  style={{ width: 150, marginBottom: 20 }}
                >
                  <Label>{cElm?.label}</Label>
                  <TextContent>
                    {objData[cElm?.key]
                      ? `IDR ${formatNumber(objData[cElm?.key])}`
                      : "-"}
                  </TextContent>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
