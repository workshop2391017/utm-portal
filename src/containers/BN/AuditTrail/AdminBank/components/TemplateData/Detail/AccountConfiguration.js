import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import { formatAmountDot } from "utils/helpers";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  subtitle: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "12px",
    color: "#BCC8E7",
  },
  accountLimitTitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#0061A7",
  },
  subtitleBlackBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "14px",
    color: "#374062",
  },
  accountLimitSubtitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function AccountConfiguration({ data }) {
  const classes = useStyles();

  const findValue = (key) =>
    formatAmountDot(
      data?.accountLimits
        ?.find((el) => el?.transactionCategory === key)
        ?.maxAmountTransaction?.toString()
    );

  return (
    <Grid container direction="column" spacing={2}>
      {/* -----Header----- */}
      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.title}>
            {data?.corporate?.corporateName || "-"}
          </Grid>
          <Grid item className={classes.subtitle}>
            {data?.corporate?.corporateAuthorization
              ? `${data?.corporate?.corporateAuthorization} Authorization`
              : "-"}
          </Grid>
        </Grid>
      </Grid>

      {/* -----Account Limits----- */}
      <Grid item>
        <Grid container>
          <Grid item xs={6}>
            <Grid container direction="column" spacing={1}>
              <Grid item className={classes.accountLimitTitle}>
                Payment
              </Grid>
              <Grid
                item
                className={classes.accountLimitSubtitle}
                style={{ paddingLeft: 20 }}
              >
                Payment Limit :
              </Grid>
              <Grid item className={classes.value} style={{ paddingLeft: 20 }}>
                {findValue("BILLPAYMENT") || "-"}
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={6}>
            <Grid container direction="column" spacing={1}>
              <Grid item className={classes.accountLimitTitle}>
                Purchase
              </Grid>
              <Grid
                item
                className={classes.accountLimitSubtitle}
                style={{ paddingLeft: 20 }}
              >
                Purchase Limit :
              </Grid>
              <Grid item className={classes.value} style={{ paddingLeft: 20 }}>
                {findValue("PURCHASE") || "-"}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.accountLimitTitle}>
            Transfer
          </Grid>
          <Grid item style={{ paddingLeft: 20 }}>
            <Grid container>
              <Grid item xs={6}>
                <Grid container direction="column" spacing={1}>
                  <Grid item className={classes.subtitleBlackBold}>
                    In House Transfer
                  </Grid>
                  <Grid
                    item
                    className={classes.accountLimitSubtitle}
                    style={{ paddingLeft: 20 }}
                  >
                    Bank Transfer Limit :
                  </Grid>
                  <Grid
                    item
                    className={classes.value}
                    style={{ paddingLeft: 20 }}
                  >
                    {findValue("TRANSFER_ON_US") || "-"}
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={6}>
                <Grid container direction="column" spacing={1}>
                  <Grid item className={classes.subtitleBlackBold}>
                    Domestic Transfer
                  </Grid>
                  <Grid item>
                    <Grid container>
                      <Grid item xs={6} style={{ paddingLeft: 20 }}>
                        <Grid container direction="column" spacing={1}>
                          <Grid item className={classes.accountLimitSubtitle}>
                            Online Limit :
                          </Grid>
                          <Grid item className={classes.value}>
                            {findValue("TRANSFER_OA") || "-"}
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item xs={6} style={{ paddingLeft: 20 }}>
                        <Grid container direction="column" spacing={1}>
                          <Grid item className={classes.accountLimitSubtitle}>
                            SKN Limit :
                          </Grid>
                          <Grid item className={classes.value}>
                            {findValue("TRANSFER_SKN") || "-"}
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid
                        item
                        xs={6}
                        style={{ marginTop: 16, paddingLeft: 20 }}
                      >
                        <Grid container direction="column" spacing={1}>
                          <Grid item className={classes.accountLimitSubtitle}>
                            RTGS Limit :
                          </Grid>
                          <Grid item className={classes.value}>
                            {findValue("TRANSFER_RTGS") || "-"}
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
