import React, { Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import moment from "moment";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

// Helpers
import useDebounce from "utils/helpers/useDebounce";
import { rangeDate, searchDateStart, searchDateEnd } from "utils/helpers";
import {
  setActivityName,
  setDetailActivity,
  getDataAudit,
  setPayloadGetData,
  setTypeDetail,
  setNewData,
  setOldData,
} from "stores/actions/auditTrail";

// Components
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";

// Asstes
import { ReactComponent as SortAsc } from "assets/icons/BN/sort-numeric-down-alt.svg";
import { ReactComponent as SortDesc } from "assets/icons/BN/sort-numeric-up-alt.svg";

const today = new Date();

const useStyles = makeStyles((theme) => ({
  table: {
    display: "flex",
    flexDirection: "column",
    gap: 20,
  },
}));

// // Dummy
// const tableDummy = [
//   {
//     user: "User 1",
//     role: "Checker",
//     activityStatus: "Add",
//     activityName: "Approve Financial Matrix",
//     status: 0,
//   },
//   {
//     user: "User 2",
//     role: "Checker",
//     activityStatus: "Edit",
//     activityName: "Edit Level",
//     status: 1,
//   },
//   {
//     user: "User 3",
//     role: "Checker",
//     activityStatus: "Delete",
//     activityName: "Reject Financial Matrix",
//     status: 2,
//   },
// ];

function Landing({ setActivePage }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { dataTableAudit, payloadGetData, search } = useSelector(
    (state) => state.auditTrail
  );

  const [dataFilter, setDataFilter] = useState(null);

  useEffect(() => {
    dispatch(getDataAudit("admin"));
  }, [dispatch, payloadGetData]);

  const handleValueActivity = (type) => {
    switch (type) {
      case 1:
        return "SUCCESS";
      case 2:
        return "FAILED";
      default:
        return "";
    }
  };

  useEffect(() => {
    if (dataFilter || payloadGetData.searchValue !== search) {
      let startDateValue = payloadGetData.startDate;
      let endDateValue = payloadGetData.endDate;
      if (dataFilter?.date?.dateValue1 === null) {
        startDateValue = searchDateStart(today);
        endDateValue = searchDateEnd(today);
      }
      if (dataFilter?.date?.dateValue2 === null) {
        endDateValue = searchDateEnd(today);
      }
      const payload = {
        ...payloadGetData,
        direction: dataFilter?.tabs?.tabValue2 === 0 ? "ASC" : "DESC",
        startDate: dataFilter?.date?.dateValue1
          ? searchDateStart(dataFilter?.date?.dateValue1)
          : startDateValue,
        endDate: dataFilter?.date?.dateValue2
          ? searchDateEnd(dataFilter?.date?.dateValue2)
          : endDateValue,
        searchValue: search,
        activityStatus: handleValueActivity(
          dataFilter?.tabsAdvance?.tabValueAdvance1
        ),
        pageNumber: 0,
      };
      dispatch(setPayloadGetData(payload));
    }
  }, [dataFilter, search]);

  const getTypeDetailAudit = (activityName) => {
    if (activityName.includes("Status User Setting Corporate"))
      return "STATUS_USER_CORPORATE_DETAIL";
    if (activityName.includes("User Setting Corporate"))
      return "USER_CORPORATE_DETAIL";
    if (activityName.includes("User Setting")) return "USER_DETAIL";
    if (activityName.includes("Level")) return "LEVEL_DETAIL";
    if (activityName.includes("Service Chargelist"))
      return "SERVICE_CHARGE_LIST_DETAIL";
    if (activityName.includes("Limit Package")) return "LIMIT_PACKAGE_DETAIL";
    if (activityName.includes("Parameter Setting"))
      return "PARAMETER_SETTINGS_DETAIL";
    if (activityName.includes("Parameter Advance"))
      return "PARAMETER_ADVANCE_DETAIL";
    if (activityName.includes("Parameter")) return "GENERAL_PARAMETER_DETAIL";
    if (activityName.includes("Domestic Bank")) return "DOMESTIC_BANK_DETAIL";
    if (activityName.includes("International Bank"))
      return "INTERNATIONAL_BANK_DETAIL";
    if (activityName.includes("Account Limit Config"))
      return "CORPORATE_ACCOUNT_LIMIT_CONFIG";
    if (activityName.includes("Segmentation Setting"))
      return "CORPORATE_SEGMENTATION";
    if (activityName.includes("Privilege Setting"))
      return "PRIVILEGE_SETTINGS_DETAIL";
    if (activityName.includes("Product Detail")) return "PRODUCT_DETAIL";
    if (activityName.includes("Account Product"))
      return "ACCOUNT_PRODUCT_DETAIL";
    if (activityName.includes("Account Type")) return "ACCOUNT_TYPE_DETAIL";
    if (activityName.includes("Account Configuration"))
      return "ACCOUNT_CONFIGURATION_DETAIL";
    if (activityName.includes("Transaction Limit"))
      return "LIMIT_TRANSACTION_DETAIL";
    if (activityName.includes("Error")) return "HOST_ERROR_MAPPING";
    if (activityName.includes("Chargelist")) return "CHARGE_LIST_DETAIL";
    if (activityName.includes("Charge Package")) return "CHARGE_PACKAGE_DETAIL";
    if (activityName.includes("Service")) return "SERVICE_DETAIL";
    if (activityName.includes("Branch Segmentation"))
      return "SEGMENTATION_BRANCH_DETAIL";
    if (activityName.includes("Segmentation")) return "SEGMENTATION_DETAIL";
    if (activityName.includes("Company Limit")) return "COMPANY_LIMIT_DETAIL";
    if (activityName.includes("Company Charge")) return "COMPANY_CHARGE_DETAIL";
    if (activityName.includes("Company")) return "CORPORATE_MANAGEMENT_DETAIL";
    if (activityName.includes("Approval Matrix"))
      return "APPROVAL_MATRIX_DETAIL";
    if (activityName.includes("Menu Package")) return "MENU_PACKAGE_DETAIL";
    if (activityName.includes("Tiering")) return "TIERING_DETAIL";
    if (activityName.includes("Special Menu")) return "SPECIAL_MENU_DETAIL";
    if (activityName.includes("Handling Officer"))
      return "HANDLING_OFFICER_DETAIL";

    return "";
  };

  const seeDetailHandle = (rowData) => {
    if (rowData.activityName !== "Add Registration") setActivePage(1);
    else setActivePage(2);
    dispatch(setActivityName(rowData.activityName));
    dispatch(setDetailActivity(rowData));
    dispatch(setTypeDetail(getTypeDetailAudit(rowData.activityName)));
    if (rowData?.activityName.includes("Service Chargelist")) {
      dispatch(
        setNewData(
          rowData.userActivityDetail?.newData
            ? JSON.parse(rowData.userActivityDetail?.newData)?.serviceCharge
            : {}
        )
      );
    } else {
      dispatch(
        setNewData(
          rowData.userActivityDetail?.newData
            ? JSON.parse(rowData.userActivityDetail?.newData)
            : {}
        )
      );
    }

    dispatch(
      setOldData(
        rowData.userActivityDetail?.oldData
          ? JSON.parse(rowData.userActivityDetail?.oldData)
          : {}
      )
    );
  };

  const getRole = (role) => {
    switch (role) {
      case "ADMIN_RELEASER":
        return "Releaser";
      case "ADMIN_MAKER":
        return "Maker";
      case "ADMIN_APPROVER":
        return "Approver";
      case "ADMIN_MAKER_APPROVER":
        return "Maker, Approver";
      default:
        return "";
    }
  };

  const tableConfig = [
    {
      title: "Date & Time",
      headerAlign: "left",
      align: "left",

      render: (rowData) => (
        <span>
          {moment(rowData.activityDate).format("YYYY-MM-DD | HH:mm:ss")}
        </span>
      ),
    },
    {
      title: "User",
      headerAlign: "left",
      align: "left",
      key: "fullName",
    },
    {
      title: "Role",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <span>{getRole(rowData.userActivityDetail.roleName)}</span>
      ),
    },
    {
      title: "Activity Name",
      headerAlign: "left",
      align: "left",
      key: "activityName",
    },
    {
      title: "Status",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <Badge
          label={
            rowData.status === "Failed"
              ? "Failed"
              : rowData.status === "Waiting"
              ? "Waiting"
              : "Success"
          }
          type={
            rowData.status === "Failed"
              ? "red"
              : rowData.status === "Waiting"
              ? "orange"
              : "green"
          }
        />
      ),
    },
    {
      title: "",
      render: (rowData) => (
        <GeneralButton
          label="View Details"
          width={110}
          height={24}
          onClick={() => seeDetailHandle(rowData)}
          style={{
            fontFamily: "FuturaMdBT",
            fontSize: 10,
          }}
        />
      ),
    },
  ];

  return (
    <Fragment>
      <Filter
        dataFilter={dataFilter}
        setDataFilter={(e) => {
          setDataFilter(e);
        }}
        title="Show :"
        align="left"
        options={[
          {
            id: 1,
            type: "datePicker",
            placeholder: "Start Date",
          },
          {
            id: 2,
            type: "datePicker",
            placeholder: "End Date",
            disabled: true,
          },
          {
            id: 2,
            type: "tabs",
            defaultValue: 1,
            options: [
              {
                label: "Date Asc",
                icon: SortAsc,
              },
              {
                label: "Date Desc",
                icon: SortDesc,
              },
            ],
          },
        ]}
        isAdvance
        optionsAdvance={[
          { id: 1, type: "tabs", options: ["All", "Success", "Failed"] },
        ]}
      />

      <section className={classes.table}>
        <TableICBB
          headerContent={tableConfig}
          dataContent={dataTableAudit?.userActivitylist}
          page={payloadGetData.pageNumber + 1}
          setPage={(page) => {
            const payload = {
              ...payloadGetData,
              pageNumber: page - 1,
            };
            dispatch(setPayloadGetData(payload));
          }}
          isLoading={false}
          totalElement={dataTableAudit.totalElement}
          totalData={dataTableAudit.totalPage}
        />
      </section>
    </Fragment>
  );
}

export default Landing;
