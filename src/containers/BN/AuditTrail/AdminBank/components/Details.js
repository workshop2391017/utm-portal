import React from "react";
import { makeStyles } from "@material-ui/styles";
import moment from "moment";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

// helpers
import Colors from "helpers/colors";

// components
import { Card, CardHeader, CardContent, Grid } from "@material-ui/core";
import Badge from "components/BN/Badge";
import TemplateData from "./TemplateData";

// assets

const useStyles = makeStyles((theme) => ({
  row: {
    display: "flex",
    flexDirection: "row",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  card: {
    borderRadius: 10,
    padding: 20,
  },
  firstCard: {
    display: "flex",
    position: "relative",
  },
  firstCard__title: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 20,
    color: "#374062",
  },
  firstCard__subtitle: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 13,
    color: "#7B87AF",
  },
  thirdCardItem: {
    borderRadius: 10,
    border: `1px solid ${Colors.info.soft}`,
  },
  thirCardItemHeader: {
    background: Colors.primary.hard,
    color: Colors.white,

    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
    },
  },
  level: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  groupLabel: {
    fontFamily: "FuturaHvBT",
    fontSize: 16,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  label: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  featureList: {
    "& li": {
      listStylePosition: "outside",
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 15,
      color: Colors.dark.hard,
    },
  },
}));
function Details() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { activityName, detailActivity, oldData, newData, type } = useSelector(
    ({ auditTrail }) => auditTrail,
    shallowEqual
  );
  console.warn("detailActivity", detailActivity);
  console.warn("type", type);
  const getRole = (role) => {
    switch (role) {
      case "ADMIN_RELEASER":
        return { roleName: "Releaser", type: "green" };
      case "ADMIN_MAKER":
        return { roleName: "Maker", type: "blue" };
      case "ADMIN_APPROVER":
        return { roleName: "Approver", type: "orange" };
      case "ADMIN_MAKER_APPROVER":
        return { roleName: "Maker, Approver", type: "purple" };
      default:
        return {};
    }
  };
  const role = getRole(detailActivity?.userActivityDetail?.roleName);

  const UserDetail = ({ detailActivity }) => (
    <Card
      elevation={0}
      className={`${classes.card} ${classes.column}`}
      style={{ gap: 20 }}
    >
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Card
            elevation={0}
            className={classes.thirdCardItem}
            style={{ border: "none" }}
          >
            <CardHeader
              className={classes.thirCardItemHeader}
              title="Data User"
            />
            <CardContent
              style={{
                minHeight: 157,
                height: "100%",
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <div className={classes.label}>Date &amp; Time :</div>
                  <div className={classes.value}>
                    {moment(detailActivity.activityDate).format(
                      "YYYY-MM-DD | HH:mm:ss"
                    )}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.label}>Role :</div>
                  <div style={{ display: "inline-block" }}>
                    <Badge label={role.roleName} type={role.type} />
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.label}>Action :</div>
                  <div className={classes.value}>
                    {detailActivity?.userActivityDetail?.action ||
                      detailActivity?.activityName ||
                      "-"}
                  </div>
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <div className={classes.label}>User :</div>
                  <div className={classes.value}>
                    {detailActivity?.userActivityDetail?.fullName || "-"}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.label}>Group :</div>
                  <div className={classes.value}>
                    {detailActivity?.userActivityDetail?.groupName || "-"}{" "}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.label}>Office :</div>
                  <div className={classes.value}>
                    {detailActivity?.userActivityDetail?.branchName || "-"}
                  </div>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Card>
  );

  return (
    <div className={classes.column} style={{ gap: 20 }}>
      <Card elevation={0} className={`${classes.card} ${classes.firstCard}`}>
        {/* <img
          src={pattern}
          alt=""
          style={{ position: "absolute", top: 0, right: 0 }}
        /> */}
        <div className={classes.column}>
          <h2 className={classes.firstCard__title}>{activityName}</h2>
          <div
            className={classes.row}
            style={{ gap: 10, alignItems: "center" }}
          >
            <Badge
              label={
                detailActivity.status === "Failed"
                  ? "Failed"
                  : detailActivity.status === "Waiting"
                  ? "Waiting"
                  : "Success"
              }
              type={
                detailActivity.status === "Failed"
                  ? "red"
                  : detailActivity.status === "Waiting"
                  ? "orange"
                  : "green"
              }
            />
          </div>
        </div>
      </Card>
      {/* Template User Detail */}
      <UserDetail detailActivity={detailActivity} />
      <TemplateData
        oldData={oldData}
        newData={
          activityName.includes("Delete Approval Matrix") ? null : newData
        }
        type={type}
      />
    </div>
  );
}

export default Details;
