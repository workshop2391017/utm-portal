import React, { useEffect, useState, Fragment } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";

import { makeStyles } from "@material-ui/styles";
import {
  Box,
  Button,
  Card,
  CardHeader,
  CardContent,
  Grid,
} from "@material-ui/core";

import Collapse from "components/BN/Collapse";
import TableCollapse from "components/BN/TableIcBB/collapse";

import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right.svg";
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";

import Colors from "helpers/colors";
import formatNumber from "helpers/formatNumber";
import { formatPhoneNumber } from "utils/helpers";

import {
  getMenuPackageDetailAuditTrail,
  getSegmentationDetailAuditTrail,
} from "stores/actions/auditTrail";

const useStyles = makeStyles({
  imageSource: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#0061A7",
    border: "1px solid #0061A7",
    borderRadius: 10,
    paddingLeft: 10,
    height: 40,
    width: 350,
  },
  gridTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
  gridValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  gridValueBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  border: {
    borderRadius: 10,
    border: "1px solid #E8EEFF",
  },
  tableHeader: {
    background: Colors.primary.hard,

    "& .MuiCardHeader-title": {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "24px",
      letterSpacing: "0.01em",
      color: "#FFFFFF",
    },
  },
  card: {
    marginTop: 30,
    marginBottom: 30,
    border: "1px solid #E6EAF3",
    borderRadius: 10,
    marginLeft: 20,
    width: 350,
  },
  buttonStyle: {
    fontFamily: "FuturaMdBT",
    fontWeight: 700,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#0061A7",
    textTransform: "none",
    padding: 0,
  },
  label: {
    fontFamily: "FuturaHvBT",
    fontSize: 10,
    color: Colors.dark.medium,
    marginBottom: 7,
  },
  value: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    color: Colors.dark.hard,
    marginBottom: 7,
  },
});

export default function RegistrationDetail() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [activePage, setActivePage] = useState(1);
  const [imageSources, setImageSources] = useState([]);

  const { newData, segmentationDetail, menuPackageDetail } = useSelector(
    ({ auditTrail }) => auditTrail,
    shallowEqual
  );

  // untuk dapetin id menu package
  useEffect(() => {
    if (Object.keys(segmentationDetail).length === 0) {
      dispatch(
        getSegmentationDetailAuditTrail({
          segmentationId: newData?.metadata?.segmentationId,
        })
      );
    }
  }, []);

  // untuk dapetin menu package details untuk view menu package details
  useEffect(() => {
    if (Object.keys(segmentationDetail).length !== 0) {
      dispatch(
        getMenuPackageDetailAuditTrail({
          menuPackageId: segmentationDetail?.menuPackage[0]?.id,
        })
      );
    }
  }, [segmentationDetail]);

  const {
    userProfile,
    corporateProfile,
    branch,
    documents,
    segmentationLimits,
    additionalAccounts,
  } = newData?.metadata;

  const { signers } = newData;

  useEffect(() => {
    if (documents.length) {
      const arr = Object.keys(documents).map((el) => ({
        name:
          el === "ktp"
            ? "KTP / KITAS"
            : el === "tabungan"
            ? "Business Savings & Current Accounts"
            : el,
        source: documents[el].split("/")[3],
      }));

      setImageSources(arr);
    }
  }, [documents]);

  const tableData =
    activePage === 2
      ? segmentationLimits.map((el) => ({
          name: el?.serviceName,
          child: [
            {
              currencyMatrixName: el?.currencyMatrixName,
              currencyCode: el?.currencyCode,
              maxAmountTransactionOfDay: el?.maxAmountTransactionOfDay,
              maxTransaction: el?.maxTransaction,
              maxAmountTransaction: el?.maxAmountTransaction,
              minAmountTransaction: el?.minAmountTransaction,
            },
          ],
        }))
      : menuPackageDetail.masterMenuHierarchyDtoList?.map((el) => ({
          name: el?.name_en,
          child: el?.menuAccessChild?.map((child) => ({
            name_en: child?.name_en,
          })),
        }));

  const GridDataHeader = ({ title1, title2, value1, value2 }) => (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.label}>
            {title1}
          </Grid>
          <Grid item className={classes.value}>
            {value1}
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.label}>
            {title2}
          </Grid>
          <Grid item className={classes.value}>
            {value2}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );

  const dataHeaderPicker = () => {
    let dataHeader = [];

    if (activePage === 2) {
      dataHeader = [
        {
          title: "Currency Matrix",
          key: "currencyMatrixName",
        },
        {
          title: "Currency",
          key: "currencyCode",
        },
        {
          title: "Max. Nominal/Day",
          render: (rowData) => (
            <GridDataHeader
              title1="Max. Amount / Day :"
              title2="Max. Transaction / Day :"
              value1={formatNumber(rowData?.maxAmountTransactionOfDay) ?? "-"}
              value2={
                rowData?.maxTransaction
                  ? `${formatNumber(rowData?.maxTransaction)} Transaction`
                  : "-"
              }
            />
          ),
        },
        {
          title: "Max & Min Transaction",
          render: (rowData) => (
            <GridDataHeader
              title1="Max. Transaction :"
              title2="Min. Transaction :"
              value1={formatNumber(rowData?.maxAmountTransaction) ?? "-"}
              value2={formatNumber(rowData?.minAmountTransaction) ?? "-"}
            />
          ),
        },
      ];
    } else {
      dataHeader = [
        {
          title: "Menu",
          key: "name_en",
        },
      ];
    }

    return dataHeader;
  };

  const GridItem = ({ title, value, style, size = 6 }) => (
    <Grid item xs={size} style={style}>
      {title !== "Complete Address :" ? (
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.gridTitle}>
            {title}
          </Grid>
          <Grid item className={classes.gridValue}>
            {value}
          </Grid>
        </Grid>
      ) : (
        <Grid container>
          <Grid item xs={5}>
            <Grid container direction="column" spacing={1}>
              <Grid item className={classes.gridTitle}>
                {title}
              </Grid>
              <Grid item className={classes.gridValue}>
                {value}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}
    </Grid>
  );

  const businessFieldProfile = [
    {
      title: "Corporate ID :",
      value: corporateProfile?.corporateId ?? "-",
      style: { paddingLeft: 28 },
    },
    {
      title: "Business Name :",
      value: corporateProfile?.corporateName ?? "-",
    },
    {
      title: "Business Fields :",
      value: corporateProfile?.corporateCategoryName ?? "-",
      size: 12,
      style: { paddingLeft: 28 },
    },
    {
      title: "Complete Address :",
      value: corporateProfile?.corporateAddress ?? "-",
      size: 12,
      style: { paddingLeft: 28 },
    },
    {
      title: "Province :",
      value: corporateProfile?.province ?? "-",
      size: 3,
      style: { paddingLeft: 28 },
    },
    {
      title: "City/District :",
      value: corporateProfile?.district ?? "-",
      size: 3,
    },
    {
      title: "District :",
      value: corporateProfile?.subDistrict ?? "-",
      size: 3,
    },
    {
      title: "Sub-district :",
      value: corporateProfile?.village ?? "-",
      size: 3,
    },
    {
      title: "Postcode :",
      value: corporateProfile?.postalCode ?? "-",
      size: 12,
      style: { paddingLeft: 28 },
    },
    {
      title: "Business Phone Number :",
      value:
        formatPhoneNumber(corporateProfile?.corporatePhoneNumber, 18) ?? "-",
      style: { paddingLeft: 28 },
    },
    {
      title: "Amount of Soft Token Required :",
      value:
        corporateProfile?.numberOfTokens &&
        corporateProfile?.numberOfTokens !== ""
          ? `${corporateProfile?.numberOfTokens}`
          : "-",
    },
  ];

  const personInCharge = [
    {
      title: "User ID :",
      value: userProfile?.userId ?? "-",
    },
    {
      title: "Full Name :",
      value: userProfile?.fullName ?? "-",
    },
    {
      title: "Position :",
      value: userProfile?.jobPosition ?? "-",
    },
    {
      title: "Department :",
      value: userProfile?.division ?? "-",
    },
    {
      title: "KTP / KITAS Number :",
      value: userProfile?.identityNumber ?? "-",
    },
    {
      title: "NPWP Number :",
      value: userProfile?.npwp ?? "-",
    },
    {
      title: "Home Phone Number :",
      value:
        userProfile?.phoneHome && userProfile?.phoneHome !== ""
          ? formatPhoneNumber(userProfile?.phoneHome, 18)
          : "-",
    },
    {
      title: "Mobile Phone Number :",
      value:
        formatPhoneNumber(userProfile?.phoneNumber, 18).replace(
          /^0+/g,
          "+62 "
        ) ?? "-",
    },
    {
      title: "Email :",
      value: userProfile?.email ?? "-",
    },
  ];

  console.warn("ini additionalAccounts", additionalAccounts);
  console.warn("ini documents", imageSources);
  console.warn("ini newData", newData);
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        padding: "30px 80px",
      }}
    >
      <Card elevation={0} className={classes.border}>
        {/* -----Table Head----- */}
        <CardHeader className={classes.tableHeader} title="Applicant Data" />
        {/* -----Table Row----- */}
        <CardContent
          sx={{
            display: "flex",
            flexDirection: "column",
            padding: "16px 30px",
          }}
        >
          {activePage === 1 && (
            <Fragment>
              <Collapse label="Business Field Profile" defaultOpen>
                <Grid
                  container
                  spacing={3}
                  style={{ marginTop: 16, marginBottom: 16 }}
                >
                  {businessFieldProfile.map((el, i) => (
                    <GridItem
                      key={i}
                      title={el.title}
                      value={el.value}
                      size={el.size}
                      style={el.style}
                    />
                  ))}
                </Grid>
              </Collapse>

              <Collapse label="User Profile" defaultOpen>
                <Grid
                  container
                  spacing={3}
                  style={{ marginTop: 16, marginBottom: 16 }}
                >
                  {personInCharge.map((el, i) => (
                    <GridItem
                      key={el.title}
                      title={el.title}
                      value={el.value}
                      style={{ paddingLeft: i % 2 === 0 ? 28 : undefined }}
                    />
                  ))}
                </Grid>
              </Collapse>

              {/* <Collapse label="Addition of Other Accounts" defaultOpen>
                <Grid
                  container
                  spacing={3}
                  style={{ marginTop: 16, marginBottom: 16 }}
                >
                  <GridItem
                    title="Account Number 1 :"
                    value={newData?.accountNumber ?? "-"}
                    style={{ paddingLeft: 28 }}
                  />
                  {additionalAccounts &&
                    additionalAccounts.length &&
                    additionalAccounts.map((el, i) => (
                      <GridItem
                        key={`${el}${i}`}
                        title={`Account Number ${i + 2} :`}
                        value={el}
                        style={{ paddingLeft: i % 2 !== 0 ? 28 : undefined }}
                      />
                    ))}
                </Grid>
              </Collapse> */}

              <Collapse label="Specimen Signing Data" defaultOpen>
                <Grid
                  container
                  spacing={3}
                  style={{ marginTop: 16, marginBottom: 16 }}
                >
                  {signers.map((el, i) => (
                    <Fragment key={`${el.nik}${i}`}>
                      <Grid
                        item
                        xs={12}
                        className={classes.gridValueBold}
                        style={{ fontSize: 17, paddingLeft: 28 }}
                      >
                        Specimen {i + 1}
                      </Grid>
                      <GridItem
                        title="Full Name :"
                        value={el?.name ?? "-"}
                        style={{ paddingLeft: 28 }}
                      />
                      <GridItem
                        title="KTP / KITAS Number :"
                        value={el?.nik ?? "-"}
                        style={{ paddingLeft: 28 }}
                      />
                      <GridItem
                        title="Email :"
                        value={el?.email ?? "-"}
                        style={{ paddingLeft: 28 }}
                      />
                    </Fragment>
                  ))}
                </Grid>
              </Collapse>

              <Collapse label="Branch" defaultOpen>
                <Card elevation={0} className={classes.card}>
                  <CardContent>
                    <Grid container direction="column" spacing={1}>
                      <Grid item className={classes.gridValueBold}>
                        {branch?.name ?? "-"}
                      </Grid>
                      <Grid item className={classes.gridTitle}>
                        {branch?.phone
                          ? `Telephone : ${formatPhoneNumber(
                              branch?.phone,
                              18
                            )}`
                          : "Telephone : -"}
                      </Grid>
                      <Grid item className={classes.gridTitle}>
                        {branch?.fax
                          ? `Fax : ${formatPhoneNumber(branch?.fax, 18)}`
                          : "Fax : -"}
                      </Grid>
                      <Grid item className={classes.gridTitle}>
                        {branch?.address ?? "-"}
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Collapse>

              <Collapse label="Segmentation" defaultOpen>
                <Card elevation={0} className={classes.card}>
                  <CardContent>
                    <Grid container direction="column" spacing={3}>
                      <Grid item>
                        <Grid container direction="column" spacing={1}>
                          <Grid
                            item
                            className={classes.gridValueBold}
                            style={{ fontSize: 17 }}
                          >
                            {segmentationDetail?.name ?? "-"}
                          </Grid>
                          <Grid
                            item
                            className={classes.gridTitle}
                            style={{ color: "#374062" }}
                          >
                            {segmentationDetail?.description ?? "-"}
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item>
                        <Grid container direction="column" spacing={1}>
                          <Grid
                            item
                            className={classes.gridValueBold}
                            style={{ fontSize: 17 }}
                          >
                            Segmentation Limit :
                          </Grid>
                          <Grid
                            item
                            className={classes.gridTitle}
                            style={{ color: "#374062" }}
                          >
                            {segmentationDetail?.limitPackage?.name ?? "-"}
                          </Grid>
                          <Grid item>
                            <Button
                              variant="text"
                              endIcon={<ArrowRight />}
                              className={classes.buttonStyle}
                              onClick={() => setActivePage(2)}
                              disableRipple
                            >
                              View Details
                            </Button>
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item>
                        <Grid container direction="column" spacing={1}>
                          <Grid
                            item
                            className={classes.gridValueBold}
                            style={{ fontSize: 17 }}
                          >
                            Segmentation Menu :
                          </Grid>
                          <Grid
                            item
                            className={classes.gridTitle}
                            style={{ color: "#374062" }}
                          >
                            {menuPackageDetail?.menuPackage?.name ?? "-"}
                          </Grid>
                          <Grid item>
                            <Button
                              variant="text"
                              endIcon={<ArrowRight />}
                              className={classes.buttonStyle}
                              onClick={() => setActivePage(3)}
                              disableRipple
                            >
                              View Details
                            </Button>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Collapse>
            </Fragment>
          )}

          {/* -----View Details----- */}
          {activePage !== 1 && (
            <Grid container direction="column" spacing={3}>
              {/* -----Header----- */}
              <Grid item>
                <Grid container direction="column" spacing={1}>
                  <Grid item className={classes.gridValueBold}>
                    {activePage === 2 ? "Limit Package" : "Menu Package"}
                  </Grid>
                  <Grid item>
                    <Button
                      variant="text"
                      startIcon={<ArrowLeft />}
                      className={classes.buttonStyle}
                      onClick={() => setActivePage(1)}
                      disableRipple
                    >
                      Back
                    </Button>
                  </Grid>
                </Grid>
              </Grid>

              {/* -----Table View Details----- */}
              <Grid item>
                <TableCollapse
                  headerContent={dataHeaderPicker()}
                  dataContent={tableData ?? []}
                />
              </Grid>
            </Grid>
          )}
        </CardContent>
      </Card>

      {/* -----Documents----- */}
      {documents.length && (
        <Card
          elevation={0}
          className={classes.border}
          style={{ marginTop: 40 }}
        >
          {/* -----Table Head----- */}
          <CardHeader className={classes.tableHeader} title="Documents" />
          {/* -----Table Row----- */}
          <CardContent
            sx={{
              display: "flex",
              flexDirection: "column",
              padding: "16px 30px",
            }}
          >
            <Grid container justifyContent="center" spacing={2}>
              {imageSources.map((el, i) => (
                <Grid item xs={6} key={`${el.source}${i}`}>
                  <Grid container direction="column" style={{ gap: 8 }}>
                    <Grid
                      item
                      className={classes.gridTitle}
                      style={{ color: "#374062" }}
                    >
                      {el.name}
                    </Grid>
                    <Grid item className={classes.imageSource}>
                      {el.source}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </CardContent>
        </Card>
      )}
    </Box>
  );
}
