import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Title from "components/BN/Title";
import { Button, Typography } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right-white.svg";
import TableICBB from "components/BN/TableIcBB";
import Badge from "components/BN/Badge";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { useDispatch, useSelector } from "react-redux";
import { formatAmountDotCurrency } from "utils/helpers";
import TaskInformation from "../Popup";
import {
  getDataNonTransaksiDetailTaskInfo,
  setStepPages,
} from "../../../../stores/actions/statusNonTransaksi";

const dataHeader = ({ classes, dispatch, userProfileId }) => [
  {
    title: "Virtual Account",
    render: (rowData) => (
      <React.Fragment>
        <div className={classes.accountWrap}>
          <div className={classes.tableBold}>{rowData?.vaName}</div>
          <div>{rowData?.vaNumber}</div>
          <div className={classes.description}>Description :</div>
          <div>{rowData?.description}</div>
        </div>
      </React.Fragment>
    ),
  },
  {
    title: "Customer ID",
    key: "customerId",
  },
  {
    title: "Payment",
    render: (rowData) => (
      <div>
        {rowData?.billingName}
        <Badge
          label={rowData?.billingType}
          type={rowData?.billingType === "BILLING" ? "blue" : "green"}
          styleBadge={{
            border: "1px solid #75D37F",
            textAlign: "center",
            width: "100px",
          }}
        />
      </div>
    ),
  },
  {
    title: "Account Number",
    key: "accountNumber",
  },
  {
    title: "Expired Date",
    key: "expiredDate",
  },
  {
    title: "Bill",
    render: (rowData) => (
      <div>
        {`${formatAmountDotCurrency(rowData?.totalAmount?.toString())},00`}
      </div>
    ),
  },
];

const useStyles = makeStyles(() => ({
  container: {
    display: "flex",
    flexDirection: "column",
    padding: "30px 30px 30px 30px",
    gap: "30px",
  },
  title: {
    display: "flex",
    flexDirection: "column",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: "10px",
    padding: "10px 10px",
  },
  transactionContainer: {
    display: "grid",
    gridTemplateColumns: "auto auto",
    justifyContent: "space-between",
    padding: "10px 10px",
    gap: "25px",
  },
  transactionDetail: {
    display: "flex",
    flexDirection: "column",
    gap: "13px",
  },
  taskInformation: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#F4F7FB",
    borderRadius: "10px",
    padding: "20px 20px",
    gap: "20px",
  },
  taskInformationTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: "20px",
    fontWeight: 700,
    lineHeight: "24px",
    letterSpacing: "0.01em",
    fontStyle: "bold",
  },
  taskInformationDetail: {
    fontFamily: "FuturaBkBT",
    fontSize: "15px",
    fontWeight: 400,
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
  date: {
    display: "flex",
    gap: "10px",
  },
  accountWrap: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "left",
    gap: "5px",
  },
  tableBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
  description: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: "13px",
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
}));

const NonTrxDetailBulkVaAcc = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openTaskInformation, setOpenTaskInformation] = useState(false);
  const { dataNonTransaksiDetail, isLoading, userProfileId, transactionGroup } =
    useSelector((state) => state.statusNonTransaksi);

  const [pages, setPage] = useState(1);
  const handleBack = () => {
    dispatch(setStepPages(0));
  };

  const detail = dataNonTransaksiDetail?.workFlowGetDetailResponse;

  const onClikOpenTaskInformation = () => {
    setOpenTaskInformation(true);
    const payload = {
      workflowActivityId: [
        dataNonTransaksiDetail?.workFlowGetDetailResponse?.workflowActivityId,
      ],
      userProfileId: dataNonTransaksiDetail?.userProfileId,
    };
    dispatch(getDataNonTransaksiDetailTaskInfo(payload));
  };

  return (
    <div className={classes.container}>
      <TaskInformation
        isOpen={openTaskInformation}
        handleClose={() => setOpenTaskInformation(false)}
      />
      <div className={classes.title}>
        <Button
          style={{ width: "20px" }}
          startIcon={<img src={arrowLeft} alt="Kembali" />}
          onClick={handleBack}
        />
        <Typography className={classes.taskInformationTitle}>
          Detail Data
        </Typography>
      </div>
      <div className={classes.paper}>
        <div className={classes.transactionContainer}>
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              Data Maker:
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              {detail?.makerFullName}
            </Typography>
            <Typography
              className={classes.taskInformationDetail}
              style={{ marginTop: -5 }}
            >
              {detail?.makerUsername}
            </Typography>
          </div>
          <div />
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              File Name :
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              {detail?.bulkCreateVaTaskDetail?.fileName}
            </Typography>
          </div>
          <div className={classes.transactionDetail}>
            <Typography className={classes.taskInformationDetail}>
              Total Bill :
            </Typography>
            <Typography className={classes.taskInformationTitle}>
              {`${formatAmountDotCurrency(
                detail?.bulkCreateVaTaskDetail?.totalCharge?.toString()
              )},00`}
            </Typography>
          </div>
        </div>
        <div>
          {dataNonTransaksiDetail?.isMultiple ? (
            <div className={classes.taskInformation}>
              <Typography className={classes.taskInformationTitle}>
                View Task Information
              </Typography>
              <Typography className={classes.taskInformationDetail}>
                Monitor the status of your task
              </Typography>
              <GeneralButton
                width={250}
                buttonIcon={<ArrowRight />}
                label="View Task Information"
                onClick={onClikOpenTaskInformation}
              />
            </div>
          ) : null}
        </div>
      </div>
      <TableICBB
        isFullTable={false}
        isLoading={isLoading}
        headerContent={dataHeader({ dispatch, classes, userProfileId })}
        dataContent={detail?.bulkCreateVaTaskDetail?.vaDataList ?? []}
      />
    </div>
  );
};

export default NonTrxDetailBulkVaAcc;
