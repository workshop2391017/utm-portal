import React from "react";
import { makeStyles } from "@material-ui/styles";
import { Button, Typography } from "@material-ui/core";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { useDispatch } from "react-redux";
import { setStepPages } from "stores/actions/statusNonTransaksi";

const useStyles = makeStyles(() => ({
  container: {
    display: "flex",
    flexDirection: "column",
    padding: "30px 30px 30px 30px",
    gap: "30px",
  },
  title: {
    display: "flex",
    flexDirection: "column",
  },
  contentContainer: {
    display: "flex",
    gap: "20px",
    justifyContent: "center",
    "&>*:nth-child(1)": {
      height: "100%",
    },
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: "white",
    gap: "15px",
    borderRadius: "10px",
    padding: "20px 20px",
    width: "100%",
  },
  description: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
  },
  type: {
    fontStyle: "FuturaHvBT",
    color: "#7B87AF",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
  },
  detail: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
}));

const NonTrxDetailDaftarPenerimaTf = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const handleBack = () => {
    dispatch(setStepPages(0));
  };
  return (
    <div className={classes.container}>
      <div className={classes.title}>
        <Button
          style={{ width: "20px" }}
          startIcon={<img src={arrowLeft} alt="Kembali" />}
          onClick={handleBack}
        />
        <Typography className={classes.taskInformationTitle}>
          Detail Penerima Transfer
        </Typography>
      </div>
      <div className={classes.contentContainer}>
        <div className={classes.content}>
          <Typography>Informasi Penerima Transfer</Typography>
          <div className={classes.description}>
            <Typography className={classes.type}>Tipe Transaksi: </Typography>
            <div>Bank Lain</div>
          </div>
        </div>
        <div className={classes.content}>
          <div>
            <Typography>Dafter Penerima Transfer</Typography>
            <div className={classes.description}>
              <div>Paulus Jaharu</div>
              <Typography className={classes.type}>
                Jalan Senin Raya Nomor 1
              </Typography>
              <Typography className={classes.type}>
                Bank BCA 08123456789
              </Typography>
              <div
                style={{ margin: "10px 0 10px 0", border: "solid #B3C1E7 1px" }}
              />
            </div>
          </div>
          <div>
            <Typography>Dafter Penerima Transfer</Typography>
            <div className={classes.description}>
              <div>Paulus Jaharu</div>
              <Typography className={classes.type}>
                Jalan Senin Raya Nomor 1
              </Typography>
              <Typography className={classes.type}>
                Bank BCA 08123456789
              </Typography>
              <div
                style={{ margin: "10px 0 10px 0", border: "solid #B3C1E7 1px" }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NonTrxDetailDaftarPenerimaTf;
