import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

// MUI
import { Box, makeStyles, Button, Grid } from "@material-ui/core";

// antd
import { Typography } from "antd";

// components
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import Collapse from "components/BN/Collapse";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right-white.svg";

// Redux
import {
  setSelectedCompany,
  handleVAReportDetail,
} from "stores/actions/onlineSubmissonReport/vaReport";
import { useSelector, useDispatch } from "react-redux";
import { getLocalStorage } from "utils/helpers";
import GeneralButton from "components/BN/Button/GeneralButton";
import TaskInformation from "../Popup";
import {
  getDataNonTransaksiDetailTaskInfo,
  setStepPages,
} from "../../../../stores/actions/statusNonTransaksi";

const NonTrxDetailAutoCollection = () => {
  const history = useHistory();
  const [openTaskInformation, setOpenTaskInformation] = useState(false);
  const dispatch = useDispatch();

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    rowcontainer: {
      display: "flex",
      flexDirection: "row",
      //   justifyContent: "space-between",
      marginBottom: "30px",
      //   backgroundColor: "red",
    },
    mainContainer: {
      margin: "63px 115px",
      //   margin: "50px",
      borderRadius: "20px",
      overflow: "hidden",
    },
    mainContent: {
      backgroundColor: "#fff",
      padding: "50px",
      "& .title": {
        fontSize: "20px",
        color: "#374062",
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        marginBottom: "30px",
      },
    },
    titleCode: {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#7B87AF",
      fontWeight: 400,
    },
    gridTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    gridValue: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    gridValueBold: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    card: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      padding: "15px 20px",
      gap: "10px",
      width: "350px",
      height: "140px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
    cardva: {
      width: 335,
      height: 91,
      borderRadius: 10,
      border: "1px solid",
      borderColor: Colors.gray.medium,
      padding: "15px",
    },
    taskInformation: {
      display: "flex",
      flexDirection: "column",
      backgroundColor: "#F4F7FB",
      borderRadius: "10px",
      padding: "20px 20px",
      gap: "20px",
    },
    taskInformationTitle: {
      fontFamily: "FuturaHvBT",
      fontSize: "20px",
      fontWeight: 700,
      lineHeight: "24px",
      letterSpacing: "0.01em",
      fontStyle: "bold",
    },
    taskInformationDetail: {
      fontFamily: "FuturaBkBT",
      fontSize: "15px",
      fontWeight: 400,
      lineHeight: "18px",
      letterSpacing: "0.01em",
    },
  });

  const classes = useStyles();

  const { dataNonTransaksi, isLoading, dataNonTransaksiDetail } = useSelector(
    (state) => state.statusNonTransaksi
  );

  const detail =
    dataNonTransaksiDetail?.workFlowGetDetailResponse
      ?.autoCollectionRegisterDto;

  const badgeRender = (badge) => {
    if (badge === "NONBILLING") {
      return (
        <Badge
          type="green"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }

    if (badge === "BILLING") {
      return (
        <Badge
          type="blue"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }
  };

  const onClikOpenTaskInformation = () => {
    setOpenTaskInformation(true);
    const payload = {
      workflowActivityId: [
        dataNonTransaksiDetail?.workFlowGetDetailResponse?.workflowActivityId,
      ],
      userProfileId: dataNonTransaksiDetail?.userProfileId,
    };
    dispatch(getDataNonTransaksiDetailTaskInfo(payload));
  };

  const FieldVirtualAccount = ({ name, val, badge, code, accountName }) => (
    <div style={{ flex: 1 }}>
      <div className={classes.fieldTitle}>{name ? <div>{name}</div> : ""}</div>
      <div
        // weight={500}
        size="15px"
        width="350px"
        className={classes.subFieldTitle}
      >
        {accountName}
      </div>
      <div size="15px" width="350px" className={classes.subFieldTitle2}>
        {val}
      </div>
      <div>{badgeRender(badge)}</div>
    </div>
  );

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemParameterSettings = ({
    title,
    value,
    valueBold,
    parentSize = 6,
  }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title ?? <br />}
        </Grid>
        {valueBold && (
          <Grid item className={classes.gridValueBold}>
            {valueBold}
          </Grid>
        )}
        <Grid item className={classes.gridValue}>
          {value !== "BILLING" && value !== "NONBILLING"
            ? value
            : badgeRender(value)}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <TaskInformation
        isOpen={openTaskInformation}
        handleClose={() => setOpenTaskInformation(false)}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={() => dispatch(setStepPages(0))}
      />
      <Title label="Auto Collection" />
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">Data Pemohon</span>
        </div>
        <div className={classes.mainContent}>
          <div style={{ marginBottom: "30px" }} />

          <Collapse label="Addition of Other Accounts" defaultOpen>
            <div style={{ padding: "30px 15px 0px 15px" }}>
              {detail?.additionalAccounts?.map((val) => (
                <React.Fragment>
                  <div className={classes.rowcontainer}>
                    <FieldVirtualAccount
                      name="Account Number"
                      accountName={val.name}
                      val={val.accountNumber}
                    />
                    <div style={{ flex: 1 }}>
                      <div className={classes.fieldTitle}>Surat Kuasa :</div>
                      <input
                        placeholder={val.suratKuasa.split(/[\s/]+/).pop()}
                        style={{
                          width: "320px",
                          height: "40px",
                          borderRadius: "10px",
                          border: "1px solid #0061A7",
                          padding: "11px 10px",
                          fontSize: "15px",
                          position: "center",
                          textColor: "#0061A7",
                          color: "#0061A7",
                          fontFamily: "FuturaBkBT",
                        }}
                        type="text"
                      />
                    </div>
                  </div>
                </React.Fragment>
              ))}
            </div>
          </Collapse>

          <div style={{ marginBottom: "30px" }} />

          <Collapse label="Branches" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <div className={classes.card}>
              <h6 className={classes.gridValueBold}>
                {detail?.branchDetail?.name && detail?.branchDetail?.name !== ""
                  ? detail?.branchDetail?.name
                  : "-"}
              </h6>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span className={classes.gridTitle}>
                  {detail?.branchDetail?.phone &&
                  detail?.branchDetail?.phone !== ""
                    ? `Phone : ${detail?.branchDetail?.phone}`
                    : "Phone : -"}
                </span>
                <span className={classes.gridTitle}>
                  {detail?.branchDetail?.fax && detail?.branchDetail?.fax !== ""
                    ? `Fax : ${detail?.branchDetail?.fax}`
                    : "Fax : -"}
                </span>
              </div>
              <span className={classes.gridTitle}>
                {detail?.branchDetail?.address &&
                detail?.branchDetail?.address !== ""
                  ? detail?.branchDetail?.address
                  : "-"}
              </span>
            </div>
            <div style={{ marginBottom: "30px" }} />
          </Collapse>
          {dataNonTransaksiDetail?.isMultiple ? (
            <div className={classes.taskInformation}>
              <Typography className={classes.taskInformationTitle}>
                View Task Information
              </Typography>
              <Typography className={classes.taskInformationDetail}>
                Monitor the status of your task
              </Typography>
              <GeneralButton
                width={250}
                buttonIcon={<ArrowRight />}
                label="View Task Information"
                onClick={onClikOpenTaskInformation}
              />
            </div>
          ) : null}

          <Box sx={{ margin: "10px auto" }} />
        </div>
      </div>
    </div>
  );
};

NonTrxDetailAutoCollection.propTypes = {};

NonTrxDetailAutoCollection.defaultProps = {};

export default NonTrxDetailAutoCollection;
