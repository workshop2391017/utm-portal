import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

// MUI
import { Box, makeStyles, Button, Grid } from "@material-ui/core";

// antd
import { Typography } from "antd";

// components
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import Collapse from "components/BN/Collapse";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right-white.svg";

// Redux
import {
  setSelectedCompany,
  handleVAReportDetail,
} from "stores/actions/onlineSubmissonReport/vaReport";
import { useSelector, useDispatch } from "react-redux";
import { formatAmountDotCurrency, getLocalStorage } from "utils/helpers";
import GeneralButton from "components/BN/Button/GeneralButton";
import TaskInformation from "../Popup";
import { setStepPages } from "../../../../stores/actions/statusNonTransaksi";

const NonTrxDetailSingleVaAcc = () => {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [openTaskInformation, setOpenTaskInformation] = useState(false);
  const dispatch = useDispatch();

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    rowcontainer: {
      display: "flex",
      flexDirection: "row",
      //   justifyContent: "space-between",
      marginBottom: "30px",
      //   backgroundColor: "red",
    },
    mainContainer: {
      margin: "63px 115px",
      //   margin: "50px",
      borderRadius: "20px",
      overflow: "hidden",
    },
    mainContent: {
      backgroundColor: "#fff",
      padding: "50px",
      "& .title": {
        fontSize: "20px",
        color: "#374062",
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        marginBottom: "30px",
      },
    },
    titleCode: {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#7B87AF",
      fontWeight: 400,
    },
    gridTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    gridValue: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    gridValueBold: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    card: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      padding: "15px 20px",
      gap: "10px",
      width: "350px",
      height: "140px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
    cardva: {
      width: 335,
      height: 91,
      borderRadius: 10,
      border: "1px solid",
      borderColor: Colors.gray.medium,
      padding: "15px",
    },
    boldFont: {
      fontFamily: "FuturaHvBT",
      fontSize: "20px",
      fontWeight: 700,
      lineHeight: "24px",
      letterSpacing: "0.01em",
      fontStyle: "bold",
    },
  });

  const classes = useStyles();

  const { dataNonTransaksiDetail, isLoading } = useSelector(
    (state) => state.statusNonTransaksi
  );

  const detail =
    dataNonTransaksiDetail?.workFlowGetDetailResponse?.createVaTaskDetail;

  const badgeRender = (badge) => {
    if (badge === "NONBILLING") {
      return (
        <Badge
          type="green"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }

    if (badge === "BILLING") {
      return (
        <Badge
          type="blue"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }
  };

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemParameterSettings = ({
    title,
    value,
    subValue,
    valueBold,
    parentSize = 6,
  }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title ?? <br />}
        </Grid>
        {valueBold && (
          <Grid item className={classes.gridValueBold}>
            {valueBold}
          </Grid>
        )}
        <Grid item className={classes.gridValue}>
          {subValue}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value !== "BILLING" && value !== "NONBILLING"
            ? value
            : badgeRender(value)}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <TaskInformation
        isOpen={openTaskInformation}
        handleClose={() => setOpenTaskInformation(false)}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={() => dispatch(setStepPages(0))}
      />
      <Title label="Detail Data" />
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">Pembuatan Single Virtual Account</span>
        </div>
        <div className={classes.mainContent}>
          <div className={classes.boldFont}>{detail?.accountAlias}</div>
          <div style={{ marginBottom: "30px" }} />

          <Grid container spacing={3}>
            <GridItemParameterSettings
              title="Account Number :"
              value={
                detail?.accountNumber && detail?.accountNumber !== ""
                  ? detail?.accountNumber
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Code & Payment  Type :"
              value={
                detail?.billingType && detail?.billingType !== ""
                  ? detail?.billingType
                  : "-"
              }
              subValue={
                detail?.billingName && detail?.billingName !== ""
                  ? `${detail?.billingCode} - ${detail?.billingName}`
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Data Maker :"
              valueBold={
                dataNonTransaksiDetail?.workFlowGetDetailResponse
                  ?.makerFullName &&
                dataNonTransaksiDetail?.workFlowGetDetailResponse
                  ?.makerFullName !== ""
                  ? dataNonTransaksiDetail?.workFlowGetDetailResponse
                      ?.makerFullName
                  : "-"
              }
              subValue={
                dataNonTransaksiDetail?.workFlowGetDetailResponse
                  ?.makerUsername &&
                dataNonTransaksiDetail?.workFlowGetDetailResponse
                  ?.makerUsername !== ""
                  ? dataNonTransaksiDetail?.workFlowGetDetailResponse
                      ?.makerUsername
                  : "-"
              }
            />
          </Grid>

          <div
            style={{ margin: "30px 0 30px 0", border: "solid #B3C1E7 1px" }}
          />

          <div className={classes.boldFont} style={{ marginBottom: "30px" }}>
            Detail Virtual Account
          </div>
          <div className={classes.boldFont}>{detail?.vaName}</div>
          <div style={{ marginBottom: "30px" }} />
          <Grid container spacing={3}>
            <GridItemParameterSettings
              title="Customer ID :"
              value={
                detail?.customerId && detail?.customerId !== ""
                  ? detail?.customerId
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Amount of Bill :"
              value={
                detail?.totalAmount && detail?.totalAmount !== ""
                  ? `${formatAmountDotCurrency(
                      detail?.totalAmount?.toString()
                    )},00`
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Virtual Account Number :"
              value={
                detail?.vaNumber && detail?.vaNumber !== ""
                  ? detail?.vaNumber
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Expired Date :"
              value={
                detail?.expiredDate && detail?.expiredDate !== ""
                  ? detail?.expiredDate
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Description :"
              value={
                detail?.description && detail?.description !== ""
                  ? detail?.description
                  : "-"
              }
            />
          </Grid>

          <Box sx={{ margin: "10px auto" }} />
        </div>
      </div>
    </div>
  );
};

NonTrxDetailSingleVaAcc.propTypes = {};

NonTrxDetailSingleVaAcc.defaultProps = {};

export default NonTrxDetailSingleVaAcc;
