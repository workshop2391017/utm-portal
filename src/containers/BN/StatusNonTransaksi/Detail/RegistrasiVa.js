import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

// MUI
import { Box, makeStyles, Button, Grid } from "@material-ui/core";

// antd
import { Typography } from "antd";

// components
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import Collapse from "components/BN/Collapse";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right-white.svg";

// Redux
import {
  setSelectedCompany,
  handleVAReportDetail,
} from "stores/actions/onlineSubmissonReport/vaReport";
import { useSelector, useDispatch } from "react-redux";
import { getLocalStorage } from "utils/helpers";
import GeneralButton from "components/BN/Button/GeneralButton";
import TaskInformation from "../Popup";
import {
  getDataNonTransaksiDetailTaskInfo,
  setStepPages,
} from "../../../../stores/actions/statusNonTransaksi";

const NonTrxDetailRegistrasiVa = () => {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [openTaskInformation, setOpenTaskInformation] = useState(false);
  const dispatch = useDispatch();

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    rowcontainer: {
      display: "flex",
      flexDirection: "row",
      //   justifyContent: "space-between",
      marginBottom: "30px",
      //   backgroundColor: "red",
    },
    mainContainer: {
      margin: "63px 115px",
      //   margin: "50px",
      borderRadius: "20px",
      overflow: "hidden",
    },
    mainContent: {
      backgroundColor: "#fff",
      padding: "50px",
      "& .title": {
        fontSize: "20px",
        color: "#374062",
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        marginBottom: "30px",
      },
    },
    titleCode: {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#7B87AF",
      fontWeight: 400,
    },
    gridTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    gridValue: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    gridValueBold: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    card: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      padding: "15px 20px",
      gap: "10px",
      width: "350px",
      height: "140px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
    cardva: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      padding: "0px 0px 20px 20px",
      gap: "10px",
      width: "350px",
      height: "100px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
    taskInformation: {
      display: "flex",
      flexDirection: "column",
      backgroundColor: "#F4F7FB",
      borderRadius: "10px",
      padding: "20px 20px",
      gap: "20px",
    },
    taskInformationTitle: {
      fontFamily: "FuturaHvBT",
      fontSize: "20px",
      fontWeight: 700,
      lineHeight: "24px",
      letterSpacing: "0.01em",
      fontStyle: "bold",
    },
    taskInformationDetail: {
      fontFamily: "FuturaBkBT",
      fontSize: "15px",
      fontWeight: 400,
      lineHeight: "18px",
      letterSpacing: "0.01em",
    },
  });

  const classes = useStyles();

  const { dataNonTransaksi, isLoading, dataNonTransaksiDetail } = useSelector(
    (state) => state.statusNonTransaksi
  );

  const detail =
    dataNonTransaksiDetail?.workFlowGetDetailResponse?.registrationVaTaskDetail;
  const badgeRender = (badge) => {
    if (badge === "NONBILLING") {
      return (
        <Badge
          type="green"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }

    if (badge === "BILLING") {
      return (
        <Badge
          type="blue"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }
  };

  const onClikOpenTaskInformation = () => {
    setOpenTaskInformation(true);
    const payload = {
      workflowActivityId: [
        dataNonTransaksiDetail?.workFlowGetDetailResponse?.workflowActivityId,
      ],
      userProfileId: dataNonTransaksiDetail?.userProfileId,
    };
    dispatch(getDataNonTransaksiDetailTaskInfo(payload));
  };

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemParameterSettings = ({
    card,
    title,
    value,
    valueBold,
    parentSize = 6,
  }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <div className={card ? classes.cardva : null}>
          <Grid item className={classes.gridTitle}>
            {title ?? <br />}
          </Grid>
          {valueBold && (
            <Grid item className={classes.gridValueBold}>
              {valueBold}
            </Grid>
          )}
          <Grid item className={classes.gridValue}>
            {value !== "BILLING" && value !== "NONBILLING"
              ? value
              : badgeRender(value)}
          </Grid>
        </div>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <TaskInformation
        isOpen={openTaskInformation}
        handleClose={() => setOpenTaskInformation(false)}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={() => dispatch(setStepPages(0))}
      />
      <Title label="Detail Data" />
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">Registrasi Virtual Account</span>
        </div>
        <div className={classes.mainContent}>
          <Grid container spacing={3}>
            <GridItemParameterSettings
              title="Selected Account :"
              value={
                detail?.vaAccountNumber?.accountNumber &&
                detail?.vaAccountNumber?.accountNumber !== ""
                  ? detail?.vaAccountNumber?.accountNumber
                  : "-"
              }
              valueBold={
                detail?.vaAccountNumber?.accountAlias &&
                detail?.vaAccountNumber?.accountAlias !== ""
                  ? detail?.vaAccountNumber?.accountAlias
                  : "-"
              }
              parentSize={12}
            />
            <GridItemParameterSettings
              title="VA Number Length :"
              value={
                detail?.lengthVa && detail?.lengthVa !== ""
                  ? `${detail?.lengthVa} Characters`
                  : "-"
              }
            />
            <GridItemParameterSettings
              title="Customer ID Length :"
              value={
                detail?.lengthCustomerId && detail?.lengthCustomerId !== ""
                  ? `${detail?.lengthCustomerId} Characters`
                  : "-"
              }
            />
            {detail?.vaBillingPendingDto?.map((el, i) => (
              <GridItemParameterSettings
                card
                key={i}
                value={
                  el?.billingType && el?.billingType !== ""
                    ? el?.billingType
                    : "-"
                }
                valueBold={
                  el?.billingCode &&
                  el?.billingName &&
                  el?.billingCode !== "" &&
                  el?.billingName !== ""
                    ? `${el?.billingCode} - ${el?.billingName}`
                    : "-"
                }
              />
            ))}
          </Grid>

          <div style={{ marginBottom: "30px" }} />

          <Collapse label="Branches" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <div className={classes.card}>
              <h6 className={classes.gridValueBold}>
                {detail?.branchVapendingDto?.name &&
                detail?.branchVapendingDto?.name !== ""
                  ? detail?.branchVapendingDto?.name
                  : "-"}
              </h6>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span className={classes.gridTitle}>
                  {detail?.branchVapendingDto?.phone &&
                  detail?.branchVapendingDto?.phone !== ""
                    ? `Phone : ${detail?.branchVapendingDto?.phone}`
                    : "Phone : -"}
                </span>
                <span className={classes.gridTitle}>
                  {detail?.branchVapendingDto?.fax &&
                  detail?.branchVapendingDto?.fax !== ""
                    ? `Fax : ${detail?.branchVapendingDto?.fax}`
                    : "Fax : -"}
                </span>
              </div>
              <span className={classes.gridTitle}>
                {detail?.branchVapendingDto?.address &&
                detail?.branchVapendingDto?.address !== ""
                  ? detail?.branchVapendingDto?.address
                  : "-"}
              </span>
            </div>
            <div style={{ marginBottom: "30px" }} />
          </Collapse>
          {dataNonTransaksiDetail?.isMultiple ? (
            <div className={classes.taskInformation}>
              <Typography className={classes.taskInformationTitle}>
                View Task Information
              </Typography>
              <Typography className={classes.taskInformationDetail}>
                Monitor the status of your task
              </Typography>
              <GeneralButton
                width={250}
                buttonIcon={<ArrowRight />}
                label="View Task Information"
                onClick={onClikOpenTaskInformation}
              />
            </div>
          ) : null}

          <Box sx={{ margin: "10px auto" }} />
        </div>
      </div>
    </div>
  );
};

NonTrxDetailRegistrasiVa.propTypes = {};

NonTrxDetailRegistrasiVa.defaultProps = {};

export default NonTrxDetailRegistrasiVa;
