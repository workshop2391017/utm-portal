import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import {
  getDataNonTransaksi,
  getDataNonTransaksiDetail,
  setDataNonTransaksiDetail,
  setStepPages,
} from "stores/actions/statusNonTransaksi";
import GeneralButton from "components/BN/Button/GeneralButton";
import { searchDate } from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
// components
import Badge from "components/BN/Badge";
import Title from "../../../components/BN/Title";
import Filter from "../../../components/BN/Filter/GeneralFilterInquiry";
import Search from "../../../components/BN/Search/SearchWithoutDropdown";
import TableICBB from "../../../components/BN/TableIcBB";

// assets
import Illustration from "../../../assets/images/BN/illustrationred.png";
import usePrevious from "../../../utils/helpers/usePrevious";

const dataHeader = ({ dispatch, classes }) => [
  {
    title: "Date & Time",
    key: "dateTime",
    width: 180,
  },
  {
    title: "Company Information",
    width: 200,
    render: (rowData) => (
      <React.Fragment>
        <div className={classes.companyWrap}>
          <div className={classes.tableBold}>
            {rowData.corporateName ? rowData.corporateName : "-"}
          </div>
          <div>{rowData.corporateId}</div>
        </div>
      </React.Fragment>
    ),
  },
  {
    title: "User Information",
    width: 150,
    render: (rowData) => (
      <React.Fragment>
        <div className={classes.companyWrap}>
          <div className={classes.tableBold}>{rowData.userDetail.fullName}</div>
          <div>{rowData.userDetail.userProfileId}</div>
        </div>
      </React.Fragment>
    ),
  },
  {
    title: "Activity",
    key: "activity",
    width: 200,
  },
  {
    title: "Status",
    key: "status",
    width: 200,
    headerAlign: "center",
    align: "left",
    render: (rowData) => {
      if (rowData.status === "WAITING") {
        return (
          <div
            style={{
              maxWidth: "175px",
              minWidth: "125px",
              width: "100%",
              display: "flex",

              marginLeft: "-15px",
            }}
          >
            <Badge
              label="Waiting"
              type="blue"
              styleBadge={{
                border: "1px solid #75D37F",
                textAlign: "center",
                width: "69px",
              }}
              fitWidth
            />
          </div>
        );
      }

      if (rowData.status === "REJECTED") {
        return (
          <div
            style={{
              maxWidth: "175px",
              minWidth: "125px",
              width: "100%",
              display: "flex",

              marginLeft: "-15px",
            }}
          >
            <Badge
              label="REJECTED"
              type="red"
              styleBadge={{
                border: "1px solid #D14848",
                textAlign: "center",
                width: "69px",
              }}
              fitWidth
            />
          </div>
        );
      }
      if (rowData.status === "PROCESSED") {
        return (
          <div
            style={{
              maxWidth: "175px",
              minWidth: "125px",
              width: "100%",
              display: "flex",

              marginLeft: "-15px",
            }}
          >
            <Badge
              label="Processed"
              type="orange"
              styleBadge={{
                border: "1px solid #FFA24B",
                textAlign: "center",
                width: "69px",
              }}
              fitWidth
            />
          </div>
        );
      }
      if (rowData.status === "APPROVED") {
        return (
          <div
            style={{
              maxWidth: "175px",
              minWidth: "125px",
              width: "100%",
              display: "flex",

              marginLeft: "-15px",
            }}
          >
            <Badge
              label="Approved"
              type="green"
              styleBadge={{
                border: "1px solid #75D37F",
                textAlign: "center",
                width: "69px",
              }}
              fitWidth
            />
          </div>
        );
      }
      if (rowData.status === "FAILED") {
        return (
          <div
            style={{
              maxWidth: "175px",
              minWidth: "125px",
              width: "100%",
              display: "flex",
              marginLeft: "-15px",
            }}
          >
            <Badge
              label="Failed"
              type="red"
              styleBadge={{
                border: "1px solid #D14848",
                textAlign: "center",
                width: "69px",
              }}
              fitWidth
            />
          </div>
        );
      }
    },
  },
  {
    title: "",
    render: (rowData) => (
      <GeneralButton
        label="View Details"
        width={110}
        height={24}
        onClick={() => {
          dispatch(
            getDataNonTransaksiDetail({
              referenceNumber: rowData.reffNo,
              corporateId: rowData.corporateId,
            })
          );
          {
            const payload =
              rowData?.activity === "Registration Va"
                ? 1
                : rowData?.activity === "Auto Collection"
                ? 2
                : rowData?.activity === "Bulk Create Va"
                ? 3
                : rowData?.activity === "Create Va"
                ? 5
                : 0;
            dispatch(setStepPages(payload));
          }
        }}
        style={{
          fontFamily: "FuturaMdBT",
          fontSize: 10,
        }}
      />
    ),
  },
];
const useStyles = makeStyles((theme) => ({
  page: {
    padding: "30px 35px",
    display: "flex",
    flexDirection: "column",
  },
  status: {
    width: "1054px",
    height: "416px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    background: "#FFFFFF",
    marginLeft: "28px",
    marginTop: "25px",
    borderRadius: "8px",
    flexDirection: "column",
    fontFamily: "FuturaMdBT",
    fontWeight: "400",
    fontSize: "21px",
    color: "#7B87AF",
  },
  table: {
    display: "flex",
    flexDirection: "column",
    marginTop: "10px",
    marginLeft: "28px",
  },
  filter: {
    width: "1050px",
  },
  companyWrap: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "left",
    gap: "10px",
  },
  tableBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: "15px",
    lineHeight: "18px",
    letterSpacing: "0.01em",
  },
}));
const StatusNonTransaksi = (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [state, setState] = useState(false);
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const prevPage = usePrevious(page);
  const payloadSearch = useDebounce(search, 1500);
  const { dataNonTransaksi, isLoading } = useSelector(
    (state) => state.statusNonTransaksi
  );
  const [dataFilter, setDataFilter] = useState({
    date: {
      dateValue1: null,
      dateValue2: null,
    },
  });
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };

  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const getData = () => {
    const payload = {
      startDate: search
        ? null
        : dataFilter?.date?.dateValue1
        ? new Date(searchDate(dataFilter?.date?.dateValue1))
        : null,
      endDate: search
        ? null
        : dataFilter?.date?.dateValue2
        ? new Date(searchDate(dataFilter?.date?.dateValue2))
        : null,
      page: page - 1,
      size: 10,
      status: dataFilter.dropdown?.dropdown3 || null,
      menu: dataFilter.dropdown?.dropdown3 || null,
      searcValue: payloadSearch,
    };
    dispatch(getDataNonTransaksi(payload));
  };

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      getData();
    } else {
      setPage(1);
    }
  }, [payloadSearch, dataFilter, page]);

  useEffect(() => {
    getData();
  }, [payloadSearch, dataFilter]);

  useEffect(() => {
    setDataTable(dataNonTransaksi?.nonTransactionStatusList);
  }, [dataNonTransaksi]);

  const trxStatusList = [
    "Success",
    "Processed",
    "Failed",
    "Waiting For Approval",
    "Rejected",
  ];

  const activity = [
    "Registrasi",
    "Registrasi Va",
    "Pembuatan Va",
    "Auto Collection",
    "Supply Chain",
    "Trade Finance",
    "Bank Garansi",
    "Pengajuan API",
  ];

  return (
    <div className={classes.page}>
      <Title label="Status Non Transaksi">
        <Search
          placeholder="Company ID, Company Name"
          setDataSearch={setSearch}
          dataSearch={search}
          style={{
            width: "240px",
            height: "40px",
          }}
        />
      </Title>
      <Filter
        dataFilter={dataFilter}
        setDataFilter={(e) => {
          setDataFilter(e);
        }}
        align="left"
        options={[
          {
            id: 1,
            type: "datePicker",
            placeholder: "Start Date",
          },
          {
            id: 2,
            type: "datePicker",
            placeholder: "End Date",
            disabled: true,
          },
          {
            id: 3,
            type: "dropdown",
            options:
              trxStatusList.map((e) => ({
                lable: e,
                value: e,
              })) ?? [],
            placeholder: "Choose Transaction Status",
            width: 250,
          },
        ]}
        isAdvance
        optionsAdvance={[
          {
            id: 4,
            type: "dropdown",
            options:
              activity.map((e) => ({
                lable: e,
                value: e,
              })) ?? [],
            placeholder: "Choose Activity",
          },
        ]}
        dropdownAdvance
      />

      {state ? (
        <div className={classes.status}>
          <img src={Illustration} alt="illustration" />

          <span>
            Anda Belum Memiliki Daftar <br />
            &nbsp;&nbsp;&nbsp;&nbsp; Status Transaksi
          </span>
        </div>
      ) : (
        <div className={classes.table}>
          <TableICBB
            isLoading={isLoading}
            headerContent={dataHeader({ dispatch, classes })}
            dataContent={dataTable ?? []}
            totalData={dataNonTransaksi?.totalPages}
            totalElement={dataNonTransaksi?.totalElements}
            page={page}
            setPage={setPage}
          />
        </div>
      )}
    </div>
  );
};

StatusNonTransaksi.propTypes = {};

export default StatusNonTransaksi;
