import React from "react";
import { useSelector } from "react-redux";
import NonStatusTransaction from "./StatusNonTransaksi";
import NonTrxDetailRegistrasiVa from "./Detail/RegistrasiVa";
import NonTrxDetailAutoCollection from "./Detail/AutoCollection";
import NonTrxDetailDaftarPenerimaTf from "./Detail/DaftarPenerimaTf";
import NonTrxDetailBulkVaAcc from "./Detail/BulkVaAcc";
import NonTrxDetailSingleVaAcc from "./Detail/SIngleVaAcc";

const StatusNonTransaksi = () => {
  const { pages } = useSelector((state) => state.statusNonTransaksi);

  return (
    <React.Fragment>
      {pages === 0 ? (
        <NonStatusTransaction />
      ) : pages === 1 ? (
        <NonTrxDetailRegistrasiVa />
      ) : pages === 2 ? (
        <NonTrxDetailAutoCollection />
      ) : pages === 3 ? (
        <NonTrxDetailBulkVaAcc />
      ) : pages === 5 ? (
        <NonTrxDetailSingleVaAcc />
      ) : pages === 6 ? (
        <NonTrxDetailDaftarPenerimaTf />
      ) : (
        <NonStatusTransaction />
      )}
    </React.Fragment>
  );
};

export default StatusNonTransaksi;
