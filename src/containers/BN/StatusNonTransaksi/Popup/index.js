// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import Colors from "helpers/colors";
import "./index.css";
import { useSelector } from "react-redux";
import moment from "moment";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paperContainer: {
    width: 480,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    padding: "20px 20px 20px 20px",
  },
  title: {
    fontSize: 24,
    textAlign: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    marginBottom: 20,
  },
  taskWrapper: {
    display: "flex",
    flexDirection: "column",
    gap: "12px",
  },
  informationTitle: {
    fontFamily: "FuturaHvBT",
  },
  informationSubTitle: {
    fontFamily: "FuturaBkBT",
    color: "#7B87AF",
  },
  informationName: {
    fontFamily: "FuturaMdBT",
    color: "#374062",
  },
}));

const TaskInformation = ({ isOpen, handleClose, title }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);
  const { taskInformation, isLoading } = useSelector(
    (state) => state.statusNonTransaksi
  );
  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          {isLoading ? (
            <div className={classes.loading}>
              <CircularProgress color="primary" size={40} />
            </div>
          ) : (
            <div className={classes.paperContainer}>
              <div className={classes.title}>Task Information</div>
              <div>
                <div className="wrapper">
                  <ul className="StepProgress">
                    {taskInformation?.detailNonFinanceReportDtoList?.map(
                      (val) =>
                        val?.workFlowStatusEnum === "CREATED" ||
                        val?.workFlowStatusEnum === "APPROVED" ? (
                          <div className="StepProgress-item is-done">
                            <div className={classes.taskWrapper}>
                              <div>
                                <Typography
                                  className={classes.informationTitle}
                                >
                                  {val?.workFlowStatusEnum === "CREATED"
                                    ? "Task Created By Maker"
                                    : "Tasks Approved By Approval"}
                                </Typography>
                                <Typography
                                  className={classes.informationSubTitle}
                                >
                                  {moment(val?.createDateApproval).format(
                                    "DD MMMM YYYY HH:MM"
                                  )}{" "}
                                </Typography>
                              </div>
                              <div>
                                <Typography className={classes.informationName}>
                                  {val?.name}
                                </Typography>
                                <Typography
                                  className={classes.informationSubTitle}
                                >
                                  Group {val?.groupDto?.name}
                                </Typography>
                              </div>
                            </div>
                          </div>
                        ) : val?.workFlowStatusEnum === "REJECTED" ? (
                          <div className="StepProgress-item is-failed">
                            <div className={classes.taskWrapper}>
                              <div>
                                <Typography
                                  className={classes.informationTitle}
                                >
                                  Tasks Rejected By Approval
                                </Typography>
                                <Typography
                                  className={classes.informationSubTitle}
                                >
                                  {moment(val?.createDateApproval).format(
                                    "DD MMMM YYYY HH:MM"
                                  )}{" "}
                                </Typography>
                              </div>
                              <div>
                                <Typography className={classes.informationName}>
                                  {val?.name}
                                </Typography>
                                <Typography
                                  className={classes.informationSubTitle}
                                >
                                  Group {val?.groupDto?.name}
                                </Typography>
                              </div>
                            </div>
                          </div>
                        ) : val?.workFlowStatusEnum === "WAITING" ? (
                          <div className="StepProgress-item current">
                            <Typography className={classes.informationTitle}>
                              Task Waiting Approval
                            </Typography>
                          </div>
                        ) : null
                    )}
                  </ul>
                </div>
              </div>
            </div>
          )}
        </Fade>
      </Modal>
    </div>
  );
};

TaskInformation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
};

TaskInformation.defaultProps = {
  title: "",
};

export default TaskInformation;
