import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import {
  getDataListOffice,
  getDataUserRole,
  getDataUserDetails,
  setHandleClearError,
  setSavePayload,
  setGetUserRole,
  setPages,
  setMenuSelect,
} from "stores/actions/roleUser";
import useDebounce from "utils/helpers/useDebounce";
// components
import Title from "components/BN/Title";
import Tables from "components/BN/TableIcBB/collapse";
import GeneralButton from "components/BN/Button/GeneralButton";
import MenuListWithoutCollapse from "components/BN/MenuList/MenuListWithoutCollapse";
import Toast from "components/BN/Toats";

const useStyles = makeStyles({
  container: {
    padding: "6px 6px",
  },
  left: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#FFFFFF",
    flex: "20%",
    height: "544px",
    borderRadius: "8px",
    padding: "10px 20px",
  },
  right: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "blue",
    flex: "80%",
  },
  titleLeft: {},
  subtitle: {
    fontSize: 15,
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#374062",
    lineHeight: "18px",
  },
  card2: {
    // backgroundColor: "yellow",
    display: "flex",
    cursor: "pointer",
    "&:hover": {
      background: "#EAF2FF",
      borderRadius: "5px",
    },
  },
  container1: {
    // backgroundColor: "red",
    backgroundColor: "#fff",
    borderRadius: 10,
    marginRight: 30,
  },
  card: {
    width: "100%",
    backgroundColor: "#fff",
    height: 40,
    border: "none",
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingLeft: 15,
    paddingTop: 8,
    paddingBottom: 8,
  },
  title: {
    fontSize: 15,
    fontFamily: "FuturaHvBT",
    color: "#374062",
    fontWeight: 400,
    lineHeight: "18px",
    paddingTop: "16px",
  },
  table: {
    width: "750px",
    padding: "20px 20px",
    backgroundColor: "white",
    marginLeft: "28px",
  },
});
const PeranPengguna = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { data, isLoading, isOpen, error, dataUserRole, menuSelected } =
    useSelector((state) => state.roleUser);

  const [isEdit, setIsEdit] = useState(false);
  const [dataSearch, setDataSearch] = useState("");

  const hasilSearch = useDebounce(dataSearch, 1500);
  const handleClick = (payload) => {
    dispatch(setMenuSelect(payload?.branchCode));
    dispatch(getDataUserRole(payload));
  };
  const handleDetails = (payloads) => {
    dispatch(getDataUserDetails(payloads));
  };
  const menuOptions = [
    {
      key: "l1",
      label: "1-Bandung",
    },
    {
      key: "l2",
      label: "91 - Gatot Subroto - KCP Class 2",
    },
    {
      key: "l3",
      label: "15 - East Bandung - KCP Class 1A",
    },
    {
      key: "l4",
      label: "205 - Sumedang - KCP Class 1A",
    },
  ];
  const dataDummyTableCheckingAccount = [
    {
      id: 1,
      name: "SPV",
      child: [
        {
          id: 1,
          role: "Maker",
          totalUser: "100",
        },
        {
          id: 1,
          role: "Approver",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Releaser",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Maker, Approver, Releaser ",
          totalUser: "20",
        },
      ],
    },
    {
      id: 2,
      name: "CS",
      child: [
        {
          id: 1,
          role: "Maker",
          totalUser: "100",
        },
        {
          id: 1,
          role: "Approver",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Releaser",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Maker, Approver, Releaser ",
          totalUser: "20",
        },
      ],
    },
    {
      id: 3,
      name: "SME",
      child: [
        {
          id: 1,
          role: "Maker",
          totalUser: "100",
        },
        {
          id: 1,
          role: "Approver",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Releaser",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Maker, Approver, Releaser ",
          totalUser: "20",
        },
      ],
    },
    {
      id: 4,
      name: "CLO",
      child: [
        {
          id: 1,
          role: "Maker",
          totalUser: "100",
        },
        {
          id: 1,
          role: "Approver",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Releaser",
          totalUser: "20",
        },
        {
          id: 1,
          role: "Maker, Approver, Releaser ",
          totalUser: "20",
        },
      ],
    },
  ];

  const dataHeader = ({ isEdit }) => [
    {
      title: "Role",
      key: "roleName",
      render: (rowData) => {
        if (rowData.roleName === "ADMIN_MAKER") {
          return <span>Maker</span>;
        }
        if (rowData.roleName === "ADMIN_RELEASER") {
          return <span>Releaser</span>;
        }
        if (rowData.roleName === "ADMIN_APPROVER") {
          return <span>Approver</span>;
        }
        if (rowData.roleName === "ADMIN_MAKER_APPROVER") {
          return <span>Maker, Approver</span>;
        }
        if (rowData.roleName === "ADMIN_MAKER_RELEASER") {
          return <span>Maker, Releaser</span>;
        }
        if (rowData.roleName === "ADMIN_APPROVER_RELEASER") {
          return <span>Approver, Releaser</span>;
        }
        if (rowData.roleName === "ADMIN_MAKER_APPROVER_RELEASER") {
          return <span>Maker, Approver, Releaser</span>;
        }
      },
    },
    {
      title: "Total User",
      key: "totalUser",
      render: (rowData) => (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <div>
            <span>{rowData.totalUser}</span>
          </div>
          <GeneralButton
            style={{ fontSize: "9px", display: "flex" }}
            label="View Details"
            width={91}
            height={23}
            onClick={() => {
              dispatch(setPages(1));
              handleDetails({
                branchCode: rowData.branchCode,
                groupId: rowData.groupId,
                groupName: rowData.groupName,
                roleId: [rowData.roleId],
                searchUsername: "",
              });
              dispatch(
                setSavePayload({
                  branchCode: rowData.branchCode,
                  groupId: rowData.groupId,
                  groupName: rowData.groupName,
                  roleId: [rowData.roleId],
                  searchUsername: "",
                  roleName: rowData.roleName,
                  totalUser: rowData.totalUser,
                })
              );
            }}
          />
        </div>
      ),
    },
  ];
  useEffect(() => {
    const payload = {
      branchName: hasilSearch === "" ? "" : hasilSearch,
    };

    dispatch(getDataListOffice(payload));
  }, [hasilSearch]);

  useEffect(() => {
    if (data.length > 0 && hasilSearch !== "") {
      dispatch(setGetUserRole([]));
      dispatch(setMenuSelect(""));
    }
  }, [data, hasilSearch]);

  return (
    <div className={classes.container}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Title label="User Role" />
      <div
        style={{
          display: "flex",
          marginLeft: 23,
          marginRight: 37,
        }}
      >
        <div style={{ width: "352px" }}>
          <MenuListWithoutCollapse
            headerTitle="List Office"
            menuOptions={data}
            selectedKeys={menuSelected}
            search={dataSearch}
            setSearch={setDataSearch}
            width="320px"
            onClick={(e) => {
              handleClick({ branchCode: e.key });
            }}
          />
        </div>
        <div className={classes.table}>
          <Tables
            headerContent={dataHeader({ isEdit })}
            dataContent={dataUserRole}
            collapse // hide icon collaps
            // handleSelect={(e) => console.warn("EE =>", e)}
            // handleSelectAll={(e) => console.warn("HandleSelectAll =>", e)}
            selectedValue={[]}
            rowColor
            isLoading={isLoading}
            noDataMessage="Please Click List Office"
          />
        </div>
      </div>
    </div>
  );
};

PeranPengguna.propTypes = {};
PeranPengguna.defaultProps = {};

export default PeranPengguna;
