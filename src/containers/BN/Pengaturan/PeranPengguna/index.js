import React from "react";
import { useSelector } from "react-redux";

import PeranPengguna from "./Home";
import DetailPeranPengguna from "./Detail";

const RoleUser = () => {
  const { pages } = useSelector((state) => state.roleUser);

  return (
    <React.Fragment>
      {pages === 0 ? <PeranPengguna /> : <DetailPeranPengguna />}
    </React.Fragment>
  );
};

export default RoleUser;
