import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Button, makeStyles } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import {
  getDataListMenu,
  setHandleClearError,
  getDataUserDetails,
  setPages,
} from "stores/actions/roleUser";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import Title from "components/BN/Title";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import Badge from "components/BN/Badge";
import Tables from "components/BN/TableIcBB";
import { ReactComponent as View } from "assets/icons/BN/three-lines.svg";
import MenuRoleUser from "components/BN/Popups/MenuRoleUser";
import Toast from "components/BN/Toats";
import useDebounce from "utils/helpers/useDebounce";

const useStyles = makeStyles({
  container: {
    padding: "20px 30px",
  },
  cardHeader: {
    background: "#FFFFFF",
    display: "flex",
    justifyContent: "space-between",
    height: "113px",
    borderRadius: "10px",
    alignItems: "center",
  },
  span: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: 13,
    letterSpacing: "0.01em",
  },
});
const dataContent = [
  {
    userID: "123321",
    name: "Great Grace",
    username: "great",
    nip: "23221242",
    email: "agung@gmail.com",
  },
  {
    userID: "123321",
    name: "Great Grace",
    username: "great",
    nip: "23221242",
    email: "agung@gmail.com",
  },
  {
    userID: "123321",
    name: "Great Grace",
    username: "great",
    nip: "23221242",
    email: "agung@gmail.com",
  },
  {
    userID: "123321",
    name: "Great Grace",
    username: "great",
    nip: "23221242",
    email: "agung@gmail.com",
  },
  {
    userID: "123321",
    name: "Great Grace",
    username: "great",
    nip: "23221242",
    email: "agung@gmail.com",
  },
];
const dataHeader = ({
  history,
  classes,
  handleClick,
  dataPayload,
  setMenu,
}) => [
  {
    title: "User ID",
    key: "userID",
    render: (rowData) => <span className={classes.span}>{rowData.idUser}</span>,
  },
  {
    title: " Name ",
    key: "name",
    render: (rowData) => <span className={classes.span}>{rowData.name}</span>,
  },
  {
    title: "Username",
    key: "username",
    render: (rowData) => (
      <span className={classes.span}>{rowData.username}</span>
    ),
  },
  {
    title: "NIP",
    key: "nip",
    render: (rowData) => <span className={classes.span}>{rowData.nip}</span>,
    width: 150,
  },
  {
    title: "Email",
    key: "email",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%",
          alignItems: "center",
        }}
      >
        <span className={classes.span}>{rowData.email}</span>
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <span
            style={{
              fontFamily: "FuturaBkBT",
              fontWeight: 400,
              color: "#1753A7",
              fontSize: 15,
              letterSpacing: "0.01em",
            }}
          >
            View Menu
          </span>
          <View
            onClick={() => {
              handleClick({
                portalGroup: {
                  id: dataPayload?.groupId,
                },
              });
              setMenu(true);
            }}
            style={{ marginLeft: "6px", cursor: "pointer" }}
          />
        </div>
      </div>
    ),

    width: 400,
  },
];

const DetailPeranPengguna = (props) => {
  const history = useHistory();
  const [menu, setMenu] = useState(false);
  const classes = useStyles();
  const dispatch = useDispatch();
  const [dataSearch, setDataSearch] = useState(null);
  const hasilSearch = useDebounce(dataSearch, 1500);
  const [page, setPage] = useState(1);
  const { dataUserDetails, dataPayload, dataListMenu, error, isLoading } =
    useSelector((state) => state.roleUser);
  const roleNames = dataPayload?.roleName;

  const handleClick = (payload) => {
    dispatch(getDataListMenu(payload));
  };
  useEffect(() => {
    if (hasilSearch !== null) {
      const payload = {
        branchCode: dataPayload.branchCode,
        groupId: dataPayload.groupId,
        groupName: dataPayload.groupName,
        roleId: dataPayload.roleId,
        searchUsername: hasilSearch === "" ? "" : hasilSearch,
      };

      dispatch(getDataUserDetails(payload));
    }
  }, [hasilSearch]);

  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <MenuRoleUser isOpen={menu} handleClose={() => setMenu(false)} />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "15px" }}
            alt="Kembali"
          />
        }
        onClick={() => dispatch(setPages(0))}
      />
      <Title label="User Role Details">
        <SearchWithDropdown
          style={{ width: "200px", height: "40px", marginTop: "-30px" }}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Username"
        />
      </Title>
      <div className={classes.container}>
        <div className={classes.cardHeader}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              height: "100%",
              padding: "20px 20px",
            }}
          >
            <span
              style={{
                fontFamily: "FuturaHvBT",
                fontSize: "20px",
                color: "#374062",
                letterSpacing: "0.03em",
              }}
            >
              {dataPayload?.groupName}
            </span>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                width: "100%",
              }}
            >
              <Badge
                type="blue"
                label={
                  roleNames === "ADMIN_MAKER"
                    ? "Maker"
                    : roleNames === "ADMIN_RELEASER"
                    ? "Releaser"
                    : roleNames === "ADMIN_APPROVER"
                    ? "Approver"
                    : roleNames === "ADMIN_MAKER_APPROVER"
                    ? "Maker, Approver"
                    : roleNames === "ADMIN_MAKER_RELEASER"
                    ? "Maker, Releaser"
                    : roleNames === "ADMIN_MAKER_APPROVER_RELEASER"
                    ? "Maker, Approver, Releaser"
                    : ""
                }
              />
              <span
                style={{
                  fontFamily: "FuturaBkBT",
                  color: "#7B87AF",
                  fontSize: "13px",
                  marginLeft: "14px",
                }}
              >
                {dataPayload?.totalUser} users
              </span>
            </div>
          </div>

          {/* <img src={background} alt="pic" width="189.77px" /> */}
        </div>
        <div style={{ marginTop: "28px" }}>
          <Tables
            headerContent={dataHeader({
              history,
              classes,
              handleClick,
              dataPayload,
              setMenu,
            })}
            dataContent={dataUserDetails?.listUser}
            isLoading={isLoading}
            page={page}
            setPage={setPage}
            totalData={dataUserDetails?.totalPage}
            totalElement={dataUserDetails?.toElement}
          />
        </div>
      </div>
    </React.Fragment>
  );
};

DetailPeranPengguna.propTypes = {};

export default DetailPeranPengguna;
