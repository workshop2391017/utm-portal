import { CircularProgress, Paper } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import ScrollCustom from "components/BN/ScrollCustom";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import AntdTabs from "components/BN/Tabs/AntdTabs";
import Colors from "helpers/colors";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setSelectedMenu } from "stores/actions/pengaturanHakAkses";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import sortNestedArrayObject from "utils/helpers/sortNestedArrayObject";

const KelompokLis = ({
  classes,
  isShow,
  setIsAdd,
  setIsEdit,
  onChangeTabs,
  activeKey,
  handleDeleteGroup,
  setSearch,
  search,
}) => {
  const dispatch = useDispatch();
  const {
    selectedGroup,
    isLoading,
    data: { portalGroupDetailDto },
  } = useSelector((e) => e.pengaturanHakAkses);

  useEffect(() => {
    if (portalGroupDetailDto?.length) {
      dispatch(setSelectedMenu(portalGroupDetailDto[0]));
    }
  }, [portalGroupDetailDto]);

  const handleEdit = (elm) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PORTAL_GROUP,
          name: null,
          code: null,
          id: elm?.portalGroup?.id,
          validate: true,
        },
        {
          onContinue() {
            setIsEdit({ isEdit: true, ...elm });
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const handleDelete = (elm) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PORTAL_GROUP,
          name: null,
          code: null,
          id: elm?.portalGroup?.id,
          validate: true,
        },
        {
          onContinue() {
            handleDeleteGroup(elm);
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  if (!isShow) return null;
  return (
    <div>
      <div style={{ padding: 20 }}>
        <div className="title">Group</div>
        <AntdTabs
          activeKey={activeKey}
          tabs={["Head Office", "Branch office"]}
          onChange={onChangeTabs}
        />
        <SearchWithDropdown
          style={{ width: 246 }}
          dataSearch={search}
          setDataSearch={setSearch}
          placement="bottom"
          trigger="none"
          inputPlaceholder="Group Search"
        />
      </div>
      <div className={classes.containerList}>
        <ScrollCustom height={400}>
          {isLoading ? (
            <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
              <div style={{ margin: "auto" }}>
                <CircularProgress color="primary" size={40} />
              </div>
            </div>
          ) : null}
          {portalGroupDetailDto?.length === 0 && !isLoading && (
            <div
              style={{
                textAlign: "center",
                fontFamily: "FuturaMdBT",
                fontSize: 21,
                color: Colors.dark.medium,
                marginTop: 24,
              }}
            >
              No Data
            </div>
          )}

          {!isLoading &&
            sortNestedArrayObject("portalGroup.name", portalGroupDetailDto).map(
              (elm, index) => (
                <div>
                  <Paper
                    style={{
                      all: "unset",
                    }}
                    onClick={() => dispatch(setSelectedMenu(elm))}
                  >
                    <div className={classes.list} key={index}>
                      <div
                        style={{
                          ...(elm?.portalGroup?.name ===
                            selectedGroup?.portalGroup?.name && {
                            backgroundColor: Colors.info.soft,
                          }),
                          display: "flex",
                          justifyContent: "space-between",
                          width: "100%",
                          borderRadius: 5,
                          padding: "20px 0",
                        }}
                      >
                        <div
                          style={{
                            margin: "auto 20px",
                            overflowWrap: "anywhere",
                          }}
                        >
                          {elm.portalGroup.name}
                        </div>
                        <div style={{ paddingRight: 10 }}>
                          <GeneralMenu
                            options={["activate", "edit", "delete"]}
                            onEdit={() => handleEdit(elm)}
                            onDelete={() => handleDelete(elm)}
                            onActive={() => {}}
                          />
                        </div>
                      </div>
                    </div>
                  </Paper>
                </div>
              )
            )}
        </ScrollCustom>
      </div>
      <div className={classes.button}>
        <GeneralButton
          label="Add Group"
          width={246}
          style={{ marginTop: 20 }}
          onClick={() => setIsAdd(true)}
        />
      </div>
    </div>
  );
};

export default KelompokLis;
