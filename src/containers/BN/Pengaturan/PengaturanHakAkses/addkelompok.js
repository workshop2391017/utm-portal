import FormField from "components/BN/Form/InputGroupValidation";
import RadioGroup from "components/BN/Radio/RadioGroup";
import AntdTextField from "components/BN/TextField/AntdTextField";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import { parseTextNumFirstSpace, validateMinMax } from "utils/helpers";
import SelectWithCheckBox from "components/BN/Select/SelectWithCheckBox";
import Colors from "helpers/colors";

const AddKelompok = ({
  classes,
  isEdit = false,
  isShow,
  addForm,
  settAddForm,
}) => {
  const [dropdownSelect, setDropdownSelect] = useState([]);

  const { isAlreadyExist } = useSelector((e) => e.validatedatalms);

  if (!isShow) return null;
  return (
    <div style={{ padding: 20 }}>
      <div className="title">Group</div>

      {isEdit && (
        <FormField
          label={<div style={{ marginBottom: 5 }}>Office :</div>}
          style={{ marginBottom: 25, marginTop: 25 }}
        >
          <span
            style={{
              color: Colors.dark.hard,
              fontFamily: "FuturaMdBT",
            }}
          >
            Head Office
          </span>
        </FormField>
      )}
      <FormField
        label={<div style={{ marginBottom: 5 }}>Group name :</div>}
        error={isAlreadyExist.name || !validateMinMax(addForm?.name, 2, 25)}
        errorMessage={
          isAlreadyExist.name
            ? "Name already exists"
            : "Min 2 and Max 25 Character"
        }
      >
        <AntdTextField
          placeholder="Group name"
          style={{ width: 242 }}
          value={addForm?.name}
          maxLength="25"
          onChange={(e) =>
            settAddForm({
              ...addForm,
              name: parseTextNumFirstSpace(e.target.value),
            })
          }
        />
      </FormField>

      <div style={{ marginTop: 25 }}>
        {!isEdit && (
          <Fragment>
            <div>Office :</div>
            <RadioGroup
              disabled={isEdit}
              options={[
                {
                  label: "Head Office",
                  value: "KP",
                },
                {
                  label: "Branch office",
                  value: "KC",
                },
              ]}
              value={addForm.officeType}
              onChange={(e) =>
                settAddForm({ ...addForm, officeType: e.target.value })
              }
              style={{ fontSize: "15px" }}
            />
          </Fragment>
        )}

        {/* CR  */}
        {/* {isEdit && (
          <>
            <div
              style={{
                marginBottom: 5,
                color: Colors.dark.hard,
                fontFamily: "FuturaBkBT",
              }}
            >
              Segment Product :
            </div>
            <SelectWithCheckBox
              placeholder="Select Segment Product"
              value={dropdownSelect}
              options={[
                { label: "A", value: "A" },
                { label: "B", value: "B" },
              ]}
              onSelect={(e) => setDropdownSelect(e)}
            />
          </>
        )} */}
      </div>
    </div>
  );
};

export default AddKelompok;
