import ScrollCustom from "components/BN/ScrollCustom";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import React from "react";

const HandleRenderMenu = ({
  selectedGroup,
  dataMenus,
  setDataMenus,
  isAdd,
  isEdit,
}) => {
  const handleSelect = (pIndex, cIndex) => () => {
    const arr = [...dataMenus];

    arr[pIndex] = {
      ...arr[pIndex],
      thereIsSubSelected: true,
    };
    arr[pIndex].menuAccessChild[cIndex] = {
      ...arr[pIndex]?.menuAccessChild[cIndex],
      checked: !arr[pIndex]?.menuAccessChild[cIndex].checked,
    };

    setDataMenus(arr);
  };

  const handleSelectAll = (pIndex) => () => {
    const arr = [...dataMenus];

    if (arr[pIndex]?.menuAccessChild?.length) {
      arr[pIndex] = {
        ...arr[pIndex],
        thereIsSubSelected: !arr[pIndex]?.thereIsSubSelected,
        menuAccessChild: arr[pIndex]?.menuAccessChild?.map((elm) => {
          const filterCheck = arr[pIndex]?.menuAccessChild?.findIndex(
            (elm) => elm.checked
          );
          return {
            ...elm,
            checked: !(filterCheck > -1),
          };
        }),
      };
      setDataMenus(arr);
    } else {
      arr[pIndex] = {
        ...arr[pIndex],
        checked: !arr[pIndex]?.checked,
      };

      setDataMenus(arr);
    }
  };

  const handleParenCheckBox = (elm) => {
    const original = elm.menuAccessChild;
    const originalChecked = elm.menuAccessChild.filter(
      ({ checked }) => checked
    );

    if (original.length && original.length === originalChecked.length)
      return true;
    return false;
  };

  const handleParenIntermidate = (elm) => {
    const original = elm.menuAccessChild;
    const originalChecked = elm.menuAccessChild.filter(
      ({ checked }) => checked
    );

    if (original.length === originalChecked.length) return false;
    if (originalChecked.length) return true;
    return false;
  };

  return (
    <div>
      <ScrollCustom height={483} width="100%">
        <div className="list">
          {dataMenus?.map((elm, pIndex) => (
            <React.Fragment>
              <div className="parent">
                {isAdd ? (
                  <CheckboxSingle
                    indeterminate={handleParenIntermidate(elm)}
                    checked={elm?.checked || handleParenCheckBox(elm)}
                    clasName="checkbox"
                    onChange={handleSelectAll(pIndex)}
                    label={elm?.name_en}
                  />
                ) : (
                  elm?.name_en
                )}
              </div>
              {elm.menuAccessChild?.map((child, cIndex) => (
                <div className="child">
                  {isAdd ? (
                    <CheckboxSingle
                      onChange={handleSelect(pIndex, cIndex)}
                      checked={child?.checked}
                      clasName="checkbox"
                      label={child?.name_en}
                    />
                  ) : (
                    child?.name_en
                  )}
                </div>
              ))}
            </React.Fragment>
          ))}
        </div>
      </ScrollCustom>
    </div>
  );
};

export default React.memo(HandleRenderMenu);
