import { makeStyles } from "@material-ui/styles";
import Badge from "components/BN/Badge";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeleteConfirmation from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import AntdSelectWithCheck from "components/BN/Select/AntdSelectWithCheck";

import Title from "components/BN/Title";
import Toast from "components/BN/Toats";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  handleDeleteMenuGlobal,
  handleGetAllMenuGlobal,
  hanleGetPrivilegeHakaksesGroup,
  hanleInsertPrivilegeHakaksesGroup,
  setClear,
  setClearError,
} from "stores/actions/pengaturanHakAkses";
import { confValidateMenuName } from "stores/actions/validatedatalms";
import { validateMinMax } from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import { useValidateLms } from "utils/helpers/validateLms";
import AddKelompok from "./addkelompok";
import KelompokLis from "./kelompokLis";
import HandleRenderMenu from "./renderMenu";

const useStyles = makeStyles({
  container: {
    padding: 20,
    display: "flex",
  },
  sideContent: {
    width: 282,
    minHeight: 154,
    backgroundColor: "white",
    borderRadius: 8,
    marginRight: 20,
    paddingBottom: 20,
    "& .title": {
      color: Colors.dark.hard,
      fontFamily: "FuturaHvBT",
      fontSize: 17,
      marginBottom: 10,
    },
  },
  rightContent: {
    width: "100%",
    "& .papperHeader": {
      width: "100%",
      backgroundColor: "#0061A7",
      padding: "15px 20px",
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8,
      fontSize: 24,
      color: "white",
      fontFamily: "FuturaMdBT",
    },
    "& .content": {
      backgroundColor: "white",
      width: "100%",
      height: "auto",
      padding: 20,
      paddingBottom: 10,
      "& .segment-product": {
        fontFamily: "FuturaBQ",
        color: Colors.dark.medium,
        fontSize: 13,
        marginBottom: 8,
      },
      "& .segment-product-badge": {
        display: "flex",
        paddingBottom: 20,
      },
      "& .list": {
        "& .parent": {
          fontFamily: "FuturaHvBT",
          color: Colors.dark.hard,
          fontSize: 15,
          padding: "10px 0",
          borderBottom: "1px solid",
          borderBottomColor: Colors.gray.soft,
          "& .checkbox": {
            marginRight: 10,
          },
        },
        "& .child": {
          width: "100%",
          fontFamily: "FuturaBkBT",
          fontSize: 15,
          color: Colors.dark.hard,
          margin: "8px 0px",
          marginLeft: 15,
          "& .checkbox": {
            marginRight: 10,
          },
        },
      },
    },
  },
  containerList: {
    width: 282,
  },
  list: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    padding: "0 18px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  listActive: {
    backgroundColor: Colors.info.soft,
  },
  button: {
    display: "flex",
    justifyContent: "center",
  },
  buttonGroup: {
    height: 84,
    display: "flex",
    justifyContent: "space-between",
    padding: "22px 20px",
    backgroundColor: "white",
  },
});

const PengaturanHakAkses = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    menuGlobal: { menuAccessList },
    selectedGroup,
    isLoadingInsert,
    isSuccessInsert,
    isLoading,
    error,
    isDeleteSuccess,
    isLoadingDelete,
  } = useSelector((e) => e.pengaturanHakAkses);
  const { isAlreadyExist } = useSelector((e) => e.validatedatalms);

  const [dataMenus, setDataMenus] = useState([]);
  const [search, setSearch] = useState("");
  const [addForm, settAddForm] = useState({ name: "", officeType: "KP" });
  const [addFormReadOnly, setAddFormReadOnly] = useState({ name: "" });
  const [activeTabs, setActiveTabs] = useState("1");
  const [isAdd, setIsAdd] = useState(false);
  const [isEdit, setIsEdit] = useState({ isEdit: false });
  const [isDelete, setIsDelete] = useState({ isDelete: false, data: {} });
  const [openModalConfirmation, setOpenModalConfirmation] = useState({
    isOpen: false,
    message: "",
  });
  const [successConfirmation, setSuccessConfirmation] = useState({
    isOpen: false,
    message: "",
  });

  useEffect(() => {
    dispatch(
      handleGetAllMenuGlobal({
        group: "ADMIN",
        menuTypeId: null,
        searchBy: null,
        masterMenuIdList: null,
        parent_id: null,
        isApprovalMatrixMenu: true,
      })
    );
  }, []);

  const searchDebounce = useDebounce(search, 1100);

  useEffect(() => {
    const officeType = activeTabs === "1" ? "KP" : "KC";
    const payload = {
      portalGroup: {
        name: searchDebounce || null,
        officeType,
      },
    };

    dispatch(hanleGetPrivilegeHakaksesGroup(payload));
  }, [activeTabs, searchDebounce]);

  useEffect(() => {
    if (isEdit.isEdit) {
      settAddForm({
        name: selectedGroup?.portalGroup?.name,
        officeType: selectedGroup?.portalGroup?.officeType,
      });

      setAddFormReadOnly({
        name: selectedGroup?.portalGroup?.name,
        officeType: selectedGroup?.portalGroup?.officeType,
      });

      const selectedDetafult = (dataMenus ?? []).map((p) => ({
        ...p,
        menuAccessChild: (p?.menuAccessChild ?? []).map((elm) => ({
          ...elm,
          checked:
            (selectedGroup?.masterMenu ?? []).findIndex(
              (f) =>
                f.menuAccessChild.findIndex((fil) => fil.id === elm.id) >= 0
            ) >= 0,
        })),
      }));
      setDataMenus(selectedDetafult);
    }
  }, [isEdit]);

  useEffect(() => {
    if (menuAccessList) {
      setDataMenus(menuAccessList);
    }
  }, [menuAccessList]);

  const getIdsLIne = (data, ids) => {
    data.forEach((elm) => {
      if (elm.id) {
        getIdsLIne(elm.menuAccessChild, ids);
      }
      ids.push(elm);
    });
  };

  const handleNext = () => {
    setOpenModalConfirmation({ isOPen: false });
  };

  const handleSave = () => {
    const dataSelecetd = [];

    dataMenus.forEach((p) => {
      if (!p.menuAccessChild.length && p.checked) {
        dataSelecetd.push({
          id: p.id,
          name_id: p?.name_id,
          name_en: p?.name_en,
          parent_id: p?.parent_id,
        });
      }
      p.menuAccessChild
        .filter((e) => e.checked)
        .forEach((c, i) => {
          if (i === 0)
            dataSelecetd.push({
              id: p.id,
              name_id: p?.name_id,
              name_en: p?.name_en,
              parent_id: p?.parent_id,
            });

          dataSelecetd.push({
            id: c.id,
            name_id: c?.name_id,
            name_en: c?.name_en,
            parent_id: c?.parent_id,
          });
        });
    });

    dispatch(
      hanleInsertPrivilegeHakaksesGroup(
        {
          portalGroupDetailDto: [
            {
              portalGroup: {
                name: addForm?.name,
                officeType: addForm?.officeType,
                ...(isEdit.isEdit && { id: selectedGroup?.portalGroup?.id }),
              },
              masterMenu: dataSelecetd,
            },
          ],
        },
        { name: addForm?.name, code: addForm?.officeType },
        handleNext
      )
    );
  };

  const handleClearState = () => {
    dispatch(setClear());
    setDataMenus(
      dataMenus.map((elm) => ({
        ...elm,
        thereIsSubSelected: false,
        menuAccessChild: elm?.menuAccessChild?.map((c) => ({
          ...c,
          checked: false,
        })),
      }))
    );
    settAddForm({ officeType: "KP" });
    setIsEdit({ isEdit: false });
    setIsAdd(false);
  };

  const handleSaveButton = () => {
    if (!addForm?.name || !addForm?.officeType) {
      return true;
    }
    const dataSelecetd = [];

    dataMenus.forEach((p) => {
      if (!p.menuAccessChild.length && p.checked) {
        dataSelecetd.push(p.id);
      }

      p.menuAccessChild
        .filter((e) => e.checked)
        .forEach((c, i) => {
          dataSelecetd.push(c.id);
        });
    });

    if (!dataSelecetd.length) {
      return true;
    }

    return false;
  };

  useEffect(() => {
    if (isDeleteSuccess) {
      setOpenModalConfirmation({ isOPen: false });
      setSuccessConfirmation({
        isOpen: true,
        message: "Data Deleted Successfully",
      });
    }
  }, [isDeleteSuccess]);

  useEffect(() => {
    if (isSuccessInsert) {
      setOpenModalConfirmation({ isOPen: false });
      setSuccessConfirmation({
        isOpen: true,
        message: "Your data has been saved successfully",
      });
    }
  }, [isSuccessInsert]);

  const handleDeleteGroup = (elm) => {
    const dataSelecetd = [];

    elm?.masterMenu?.forEach((p) => {
      p?.menuAccessChild?.forEach((c, i) => {
        if (i === 0)
          dataSelecetd.push({
            id: p.id,
            name_id: p?.name_id,
            name_en: p?.name_en,
            parent_id: p?.parent_id,
          });

        dataSelecetd.push({
          id: c.id,
          name_id: c?.name_id,
          name_en: c?.name_en,
          parent_id: c?.parent_id,
        });
      });
    });

    const deletePaylaod = {
      portalGroupDetailDto: [
        {
          portalGroup: {
            name: elm?.portalGroup?.name,
            officeType: elm?.portalGroup?.officeType,
            id: elm?.portalGroup?.id,
            isDeleted: true,
          },
          masterMenu: dataSelecetd,
        },
      ],
    };

    dispatch(handleDeleteMenuGlobal(deletePaylaod));
  };

  // validate already exist
  useValidateLms({
    name:
      addFormReadOnly?.name &&
      addFormReadOnly?.name?.trim() === addForm?.name?.trim()
        ? ""
        : addForm.name,
    nameCode:
      addForm.officeType === "KP"
        ? confValidateMenuName.PRIVILEGE_HEAD
        : confValidateMenuName.PRIVILEGE_BRANCH,
  });

  return (
    <div style={{ position: "relative" }}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setClearError())}
      />
      <Title label="Privilege Settings" />

      <div className={classes.container}>
        <div>
          <div className={classes.sideContent}>
            <AddKelompok
              classes={classes}
              isShow={isAdd || isEdit.isEdit}
              isEdit={isEdit.isEdit}
              addForm={addForm}
              settAddForm={settAddForm}
            />
            <KelompokLis
              classes={classes}
              isShow={!isAdd && !isEdit.isEdit}
              setIsAdd={setIsAdd}
              isEdit={isEdit.isEdit}
              setIsEdit={(e) => {
                setIsDelete({ isDelete: false, data: null });
                setIsEdit(e);
              }}
              onChangeTabs={(e) => setActiveTabs(e)}
              activeKey={activeTabs}
              search={search}
              setSearch={(e) => setSearch(e)}
              handleDeleteGroup={(elm) => {
                setIsDelete({ isDelete: true, data: elm });
                setOpenModalConfirmation({
                  isOPen: true,
                  message: "Are You Sure You Are Deleting Data?",
                });
              }}
            />
          </div>
        </div>

        <div className={classes.rightContent}>
          <div className="papperHeader">
            {isAdd || isEdit?.isEdit
              ? "Feature Information"
              : `${selectedGroup?.portalGroup?.name ?? ""} Group`}
          </div>
          <div className="content">
            {/* CR  */}
            {/* <div className="segment-product">Segment Product :</div>
            <div className="segment-product-badge">
              <Badge type="blue" label="IBB" styleBadge={{ marginRight: 8 }} />
              <Badge type="blue" label="CMS" styleBadge={{ marginRight: 8 }} />
              <Badge type="blue" label="IBB Syariah" />
            </div> */}
            <HandleRenderMenu
              isEdit={isEdit}
              dataMenus={
                isAdd || isEdit.isEdit ? dataMenus : selectedGroup?.masterMenu
              }
              setDataMenus={setDataMenus}
              isAdd={isAdd || isEdit.isEdit}
            />
          </div>
        </div>
      </div>

      <DeleteConfirmation
        isOpen={openModalConfirmation?.isOPen}
        message={openModalConfirmation?.message}
        loading={isLoadingInsert || isLoadingDelete}
        handleClose={() => {
          setOpenModalConfirmation({ isOPen: false, message: "" });
        }}
        onContinue={() => {
          setOpenModalConfirmation({ isOPen: false, message: "" });
          if (isDelete.isDelete) {
            handleDeleteGroup(isDelete.data);
            setIsDelete({ isDelete: false, data: null });
          } else {
            handleSave();
          }
        }}
      />
      <SuccessConfirmation
        titlefontSize={28}
        isOpen={successConfirmation?.isOpen}
        message={
          isDelete?.isDelete && !isEdit?.isEdit
            ? "Deleted Successfully"
            : undefined
        }
        handleClose={() => {
          handleClearState();

          setSuccessConfirmation({ isOpen: false });
        }}
      />

      {isAdd || isEdit?.isEdit ? (
        <div className={classes.buttonGroup}>
          <ButtonOutlined
            label="Cancel"
            disabled={isLoadingInsert || isLoading}
            onClick={handleClearState}
          />
          <GeneralButton
            label="Save"
            onClick={() => {
              setOpenModalConfirmation({
                isOPen: true,
                message: isAdd
                  ? "Are You Sure To Add Data?"
                  : "Are You Sure To Edit Your Data?",
              });
            }}
            loading={isLoadingInsert}
            disabled={
              isLoading ||
              handleSaveButton() ||
              addForm?.length ||
              !validateMinMax(addForm?.name, 2, 25) ||
              isAlreadyExist.name
            }
          />
        </div>
      ) : null}
    </div>
  );
};

export default PengaturanHakAkses;
