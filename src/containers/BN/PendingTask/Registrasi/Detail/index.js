/* eslint-disable react/no-unknown-property */
import { makeStyles, Button, Grid, Paper } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  handleGetPendingTaskRegistrationDetail,
  handleRegistrationExecute,
  APPROVE,
  REJECT,
  setClearPtExecute,
  setHandleClearError,
  setOpenPreviewImage,
  setMenuPackageDetail,
} from "stores/actions/pendingTask/registration";
import { filterExecuteUnchacked } from "stores/actions/pendingTask";
import { useDispatch, useSelector } from "react-redux";

// component
import Title from "components/BN/Title";
import ButtonIcon from "components/SC/IconButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeletePopup from "components/BN/Popups/Delete";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import FailedConfirmation from "components/BN/Popups/FailedConfirmationData";
import PreviewImage from "components/BN/Popups/PreviewImage";

// assets
import { ReactComponent as CheckIcon } from "assets/icons/BN/check.svg";
import { ReactComponent as CheckClose } from "assets/icons/BN/close.svg";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { checkOnline } from "stores/actions/errorGeneral";
import Toast from "components/BN/Toats";
import times from "assets/images/BN/illustrationred.png";
import { pathnameCONFIG } from "configuration";

// helpers
import { getLocalStorage, removeLocalStorage } from "utils/helpers";
import PendingtaskregisDetailBeforeLogin from "./mapping";

const useStyles = makeStyles({
  container: {
    padding: "20px 30px",
    display: "flex",
    justifyContent: "center",
  },
  fieldTitle: {
    fontSize: 13,
    fontWeight: 400,
    fontFamily: "FuturaHvBT",
    color: "#7B87AF",
    marginBottom: "5px",
  },
  subFieldTitle: {
    fontSize: 14,
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    color: "#374062",
  },
  fieldGroup: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  paperDocument: {
    boxShadow: "none",
    width: 238,
    height: 237,
    borderRadius: 20,
    overflow: "hidden",
    border: "1px solid #BCC8E7",
    "& :hover": {
      cursor: "pointer",
    },
    "& .img": {
      width: "100%",
      height: "147px",
      overflow: "hidden",
      backgroundColor: "#F4F7FB",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      objectFit: "contain",
    },
    "& .footer": {
      display: "flex",
      width: "100%",
      padding: "17.5px 20px",
      alignItems: "center",
      justifyContent: "space-between",
    },
  },
  buttonAction: {
    width: "100%",
    backgroundColor: "white",
    padding: "22px 40px",
    marginTop: 59,
    display: "flex",
    justifyContent: "space-between",
  },
});

export const ButtonValidate = ({
  buttonKey,
  style,
  onClick,
  disabled = false,
  checked,
  disabledUncheck,
}) => (
  <div style={{ display: "flex", marginRight: 20, ...style }}>
    <ButtonIcon
      color={checked ? (disabled ? "#BCC8E7" : "#75D37F") : "#BCC8E7"}
      width={24}
      height={24}
      style={{ padding: 0, marginRight: 10 }}
      icon={<CheckIcon />}
      onClick={onClick}
      disabled={disabled}
    />
    <ButtonIcon
      color={
        disabledUncheck
          ? "#D14848"
          : !checked
          ? disabled
            ? "#BCC8E7"
            : "#D14848"
          : "#BCC8E7"
      }
      width={24}
      height={24}
      style={{ padding: 0 }}
      icon={<CheckClose />}
      disabled={disabled}
      onClick={onClick}
    />
  </div>
);

export const FieldVirtualAccount = ({ name, val, size = "15px" }) => {
  const classes = useStyles();
  return (
    <div style={{ flex: 1, fontSize: size, fontFamily: "FuturaBQ" }}>
      <div className={classes.fieldTitle}>
        {name === "empty field" ? null : `${name} : `}
      </div>
      <div width="350px" className={classes.subFieldTitle}>
        {name === "empty field" ? null : val}
      </div>
    </div>
  );
};

const PendingTaskRegistrationDetail = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    error,
    isExecuteError,
    isExecuteLoading,
    isLoading,
    idAndFase: { referenceNumber, fase },
    isExecuted,
    openPreviweImage,
    detail,
    // cr
    templateMapping,
    segmentation,
    branch,
  } = useSelector((e) => e.pendingTaskRegistration);

  // Untuk bedain antara WITH_ADMIN and NO_ADMIN
  const corporateAdmin =
    detail?.metadata?.corporateProfile?.corporateAdmin?.toLowerCase();
  const params = JSON.parse(getLocalStorage("pt_registration"));
  const FASE_LOGIN = fase || params?.fase;

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
  });
  const [referenceNumberState, setReferenceNumberState] = useState(null);
  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });

  const [openModalError, setOpenModalError] = useState(false);

  const handleBack = () => {
    history.push(pathnameCONFIG.PENDING_TASK.REGISTRATION);
    dispatch(setMenuPackageDetail([]));
  };

  const fetchingData = async () => {
    await dispatch(checkOnline);

    setReferenceNumberState(referenceNumber || params.referenceNumber);
    if (!params.referenceNumber && !referenceNumber) {
      removeLocalStorage("pt_registration");
      history.push(pathnameCONFIG.PENDING_TASK.REGISTRATION);
    }
    if (params.referenceNumber || referenceNumber) {
      dispatch(
        handleGetPendingTaskRegistrationDetail(
          {
            isPendingTask: true,
            referenceNumber: params.referenceNumber,
            corporateOpenAccountId: params?.corporateOpenAccountId,
          },
          { fase: params.fase }
        )
      );
    }
  };

  useEffect(() => {
    fetchingData();
  }, [FASE_LOGIN, referenceNumber, params?.corporateOpenAccountId]);

  const handleUnchecked = () => {
    // const unchecked = allData.find(({ checked }) => !checked);

    // return unchecked;
    let available = false;
    let docAvailable = false;

    Object.keys(templateMapping).forEach((key) => {
      if (key !== "documents" && !available) {
        available =
          templateMapping[key]?.template?.findIndex((elm) => {
            if (elm.key === "suratKuasa") return false;
            return elm?.fields?.findIndex((fi) => !fi.checked) >= 0;
          }) >= 0;
      }

      if (key === "documents" && !docAvailable) {
        docAvailable =
          templateMapping[key]?.documents?.findIndex((e) => !e?.checked) >= 0;
      }
    });

    return available || docAvailable;
  };

  const handleExecuteRegistrationDetail = async (action) => {
    await dispatch(checkOnline(dispatch));

    let lang = "label_en";

    if (detail?.metadata?.userProfile?.language === "IDN") {
      lang = "label_id";
    } else if (detail?.metadata?.userProfile?.language === "ENG") {
      lang = "label_en";
    }

    const additionalData = {
      corporateProfile: {},
      userProfile: {},
      additionalAccounts: [],
      branch: {},
      segmentationId: {},
      documents: {},
    };

    const rejectedKeys = {};

    Object.keys(templateMapping).forEach((key) => {
      if (key !== "documents") {
        templateMapping[key].template.forEach((elm) => {
          if (elm.key !== "suratKuasa")
            additionalData[elm.key] =
              filterExecuteUnchacked(elm.fields, elm.key, detail) || null;
        });

        templateMapping[key]?.template?.forEach((elm) => {
          if (elm?.fields?.findIndex((e) => !e.checked) >= 0) {
            const arr = detail?.metadata?.additionalAccounts ?? [];
            const label = "additionalAccounts";
            const unchecked = elm?.fields.filter((e) => !e.checked);

            if (
              arr.length &&
              unchecked.findIndex((e) =>
                e.label_en?.includes("Account Number")
              ) >= 0
            ) {
              additionalData[label] =
                detail?.metadata?.documents?.additionalSuratKuasa?.map(
                  (_) => null
                );

              unchecked.forEach((elm) => {
                if (elm.label_en?.includes("Account Number")) {
                  const fId = arr.findIndex((e) => e === elm.value);
                  additionalData[label][fId] = arr[fId];
                }
              });
            }

            if (elm?.rejectPayload) rejectedKeys[elm?.rejectPayload] = [];
            elm?.fields
              ?.filter((e) => !e.checked)
              .forEach((fe) => {
                if (elm?.rejectPayload) {
                  rejectedKeys[elm?.rejectPayload].push(fe[lang]);
                }
              });
          }
        });
      }
      if (key === "documents") {
        const suratKuasa = detail?.metadata?.documents?.additionalSuratKuasa;

        const docuncheck = filterExecuteUnchacked(
          templateMapping[key].documents,
          "doc",
          detail
        );

        additionalData[key] = {
          ...additionalData[key],

          ...(suratKuasa?.length &&
            Object.keys(docuncheck).findIndex((e) =>
              e?.includes("Power of Attorney")
            ) >= 0 && {
              additionalSuratKuasa: suratKuasa.map((_) => null),
            }),
        };

        Object.keys(docuncheck).forEach((docKey) => {
          if (
            docKey?.includes("Power of Attorney") &&
            additionalData[key].additionalSuratKuasa?.length
          ) {
            additionalData[key].additionalSuratKuasa[
              docKey.split(" ").pop() - 1
            ] = docuncheck[docKey];
          } else {
            additionalData[key] = {
              ...additionalData[key],
              [docKey]: docuncheck[docKey]?.length ? docuncheck[docKey] : null,
            };
          }
        });

        templateMapping[key]?.documents?.forEach((elm) => {
          if (!elm.checked) {
            if (!rejectedKeys[elm?.rejectPayload]?.length) {
              rejectedKeys[elm?.rejectPayload] = [];
            }

            rejectedKeys[elm?.rejectPayload].push(elm[lang]);
          }
        });
      }
    });

    Object.keys(additionalData).forEach((key) => {
      if (!Object.values(additionalData[key]).length) {
        delete additionalData[key];
      }
    });

    const payload = {
      status: action,
      rejectedReason: "",
      referenceNumber: referenceNumberState,
      workflowLimitTransactionId: null, // handle from be
      ...(action === REJECT && { additionalData }),
      ...rejectedKeys,
    };

    dispatch(handleRegistrationExecute(payload));
  };

  useEffect(() => {
    if (isExecuted.action === APPROVE && isExecuted.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Approved",
        submessage: "",
        isOpen: true,
      });
    }

    if (isExecuted.action === REJECT && isExecuted.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Rejected",
        isOpen: true,
      });
    }
  }, [isExecuted]);

  useEffect(() => {
    if (isExecuteError.isError) {
      setOpenModalError(true);
    }
  }, [isExecuteError]);

  function checkStatus() {
    if (
      params?.status === 1 || // <-- New Regis
      params?.status === 4 // <-- Revisi
    ) {
      return false;
    }
    return true;
  }
  return (
    <div>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <PreviewImage
        isOpen={openPreviweImage?.isOpen}
        handleClose={() =>
          dispatch(setOpenPreviewImage({ isOpen: false, image: null }))
        }
        image={openPreviweImage?.image}
        title={openPreviweImage?.name}
      />
      <div style={{ position: "relative" }}>
        <Button
          startIcon={
            <img
              src={arrowLeft}
              style={{ paddingLeft: "23px", paddingTop: "10px" }}
              alt="Kembali"
            />
          }
          onClick={handleBack}
        />
        <Title label="Registration" />

        <PendingtaskregisDetailBeforeLogin
          classes={classes}
          fase={FASE_LOGIN}
          params={params}
        />
      </div>

      <div className={classes.buttonAction}>
        <ButtonOutlined
          label="Reject"
          width="83px"
          style={{ marginRight: 11 }}
          disabled={
            !handleUnchecked() ||
            isLoading ||
            isExecuteLoading ||
            detail?.status === "APPROVE" ||
            detail?.status === "REJECT" ||
            !params?.isAllowedTransaction
          }
          onClick={() =>
            setConfirmationModal({
              message: "Are You Sure To Reject The Activity?",
              submessage: "You can't undo this action",
              isOpen: true,
              confirmType: REJECT,
            })
          }
          loading={confirmationModal.confirmType === REJECT && isExecuteLoading}
        />
        <GeneralButton
          label="Approve"
          width="118px"
          disabled={
            handleUnchecked() ||
            isLoading ||
            isExecuteLoading ||
            detail?.status === "APPROVE" ||
            detail?.status === "REJECT" ||
            !params?.isAllowedTransaction ||
            checkStatus()
          }
          loading={
            confirmationModal.confirmType === APPROVE && isExecuteLoading
          }
          onClick={() =>
            setConfirmationModal({
              message: "Are You Sure To Approve the Activity?",
              submessage: "You can't undo this action",
              isOpen: true,
              confirmType: APPROVE,
            })
          }
        />
      </div>
      <FailedConfirmation
        isOpen={openModalError}
        handleClose={() => {
          setOpenModalError(false);
          dispatch(setClearPtExecute());
        }}
        title="Fail"
        message=""
        submessage=""
      />
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        img={isExecuted.action === REJECT ? times : null}
        handleClose={() => {
          setSuccessConfirmModal({
            message: "",
            submessage: "",
            isOpen: false,
          });
          dispatch(setClearPtExecute());
          history.push(pathnameCONFIG.PENDING_TASK.REGISTRATION);
        }}
        title="Success"
        submessage={
          isExecuted?.isToReleaser?.toLowerCase()?.includes("waiting")
            ? "Please wait for approval"
            : ""
        }
        message={successConfirmModal.message}
      />
      <DeletePopup
        isOpen={confirmationModal.isOpen}
        handleClose={() => {
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          });
        }}
        onContinue={() => {
          if (confirmationModal.confirmType === APPROVE) {
            handleExecuteRegistrationDetail(APPROVE);
          } else if (confirmationModal.confirmType === REJECT) {
            handleExecuteRegistrationDetail(REJECT);
          }
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          });
        }}
        title="Confirmation"
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
      />
    </div>
  );
};

PendingTaskRegistrationDetail.propTypes = {};

PendingTaskRegistrationDetail.defaultProps = {};

export default PendingTaskRegistrationDetail;
