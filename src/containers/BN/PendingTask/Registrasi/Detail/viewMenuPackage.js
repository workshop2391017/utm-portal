import { Button, makeStyles } from "@material-ui/core";
import React, { useEffect } from "react";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";
import PapperContainer from "components/BN/PapperContainer";
import TableCollapse from "components/BN/TableIcBB/collapse";
import { useDispatch, useSelector } from "react-redux";
import { handleServiceRegisMenuPackageDetail } from "stores/actions/pendingTask/registration";

const useStyles = makeStyles({
  container: {
    borderRadius: 20,
    backgroundColor: "white",
    padding: 20,
    width: 850,
    position: "relative",
  },
  title: {
    fontSize: 20,
    fontFamily: "FuturaHvBT",
    color: Colors.dark.hard,
    marginBottom: 20,
  },
});

const dataHeader = () => [
  {
    title: "Menu",
    key: "name_en",
  },
];

const ViewMenuPackage = ({ onBack, id }) => {
  const { menuPackage, isLoading } = useSelector(
    (e) => e.pendingTaskRegistration
  );

  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    if (id && !menuPackage?.length) {
      dispatch(
        handleServiceRegisMenuPackageDetail({
          menuPackageId: id,
        })
      );
    }
  }, [id]);

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div className={classes.container}>
        <Button
          startIcon={
            <img src={arrowLeft} style={{ paddingTop: "10px" }} alt="Back" />
          }
          onClick={onBack}
        />
        <div className={classes.title}>Menu Package</div>

        <TableCollapse
          headerContent={dataHeader()}
          dataContent={(menuPackage ?? []).filter(
            (e) => e.name_en !== "Account"
          )}
          collapse
          bordered
          isLoading={isLoading}
        />
      </div>
    </div>
  );
};

export default ViewMenuPackage;
