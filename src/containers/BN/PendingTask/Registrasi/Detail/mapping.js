import React, { useEffect, useState } from "react";
import { Button, Grid, Paper } from "@material-ui/core";
import PapperContainer from "components/BN/PapperContainer";
import {
  downloadFileSignature,
  setMenuPackageDetail,
  setTemplate,
} from "stores/actions/pendingTask/registration";
import { useDispatch, useSelector } from "react-redux";
import previewPDF from "utils/helpers/previewPdf";
import { DocType } from "utils/helpers/docType";
import imageFile from "assets/images/BN/fileBg.png";
import Collapse from "components/BN/Collapse";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right.svg";
import styled from "styled-components";
import Colors from "helpers/colors";
import { compare, formatPhoneNumberStripNoSpace } from "utils/helpers";
import previewImageBase64 from "utils/helpers/previewImageBase64";
import { ButtonValidate, FieldVirtualAccount } from ".";
import ViewMenuPackage from "./viewMenuPackage";
import ViewLimitPackage from "./viewlimitPackage";

const ButtonViewDetail = styled(Button)(() => ({
  textTransform: "capitalize",
  all: "unset",
  position: "relative",
  fontWeight: 700,
  fontFamily: "FuturaMdBT",
  fontSize: 13,
  color: Colors.primary.hard,
  display: "flex",
  alignItems: "center",
  cursor: "pointer",
  marginBottom: 13,
}));

const MappingData = ({
  props: {
    templateMapping,
    classes,
    index,
    data,
    dispatch,
    label,
    keys,
    disabled,
    marginBottom,
    name,
  },
}) => {
  const { rejectedList } = useSelector((e) => e.pendingTaskRegistration);

  const handleCheckList = (i) => {
    const { containerKey } = keys;

    const fieldTarget = templateMapping;

    fieldTarget[containerKey].template[index].fields[i] = {
      ...fieldTarget[containerKey].template[index].fields[i],
      checked: !fieldTarget[containerKey].template[index].fields[i].checked,
    };

    const labels = [
      "Province",
      "City/Regency",
      "District",
      "Sub District",
      "Postcode",
    ];

    if (
      labels.includes(
        fieldTarget[containerKey].template[index].fields[i]?.label_en
      )
    ) {
      const filterLabels = labels.findIndex(
        (e) =>
          e === fieldTarget[containerKey].template[index].fields[i]?.label_en
      );

      const resFilter = labels.splice(filterLabels + 1);

      // child trigger
      fieldTarget[containerKey].template[index].fields = [
        ...fieldTarget[containerKey].template[index].fields
          .filter((e) => !resFilter.includes(e.label_en))
          .map((elm) => ({
            ...elm,
            ...(fieldTarget[containerKey].template[index].fields[i]
              ?.label_en !== elm.label_en &&
              labels.includes(elm.label_en) && {
                checked: true,
              }),
          })),
        ...fieldTarget[containerKey].template[index].fields
          .filter((e) => resFilter.includes(e.label_en))
          .map((elm) => ({
            ...elm,
            checked:
              !!fieldTarget[containerKey].template[index].fields[i].checked,
          })),
      ].sort(compare);
    }

    dispatch(setTemplate(fieldTarget));
  };

  const mappingValue = (key, el) => {
    if (key === "phoneNumber")
      return `+62 ${formatPhoneNumberStripNoSpace(parseInt(el))}`;
    if (key === "phoneHome")
      return el ? formatPhoneNumberStripNoSpace(el) : "-";
    if (key === "corporatePhoneNumber")
      return formatPhoneNumberStripNoSpace(el);
    if (key === "numberOfTokens") return el.toString();

    return el;
  };
  const arrayNotValidate = [
    "accountNumber",
    "accountType",
    "corporateId",
    "userId",
    "province",
    "district",
    "subDistrict",
    "village",
    "postalCode",
    "emptyKey",
  ];

  const { containerKey } = keys;
  const parentKyesNoValiedate = [
    "userProfileAdminApproval",
    "userProfileAdminMaker",
    "additionalAccounts",
  ];

  return (
    <div>
      <div
        style={{
          fontFamily: "FuturaMdBT",
          color: "#374062",
          fontSize: 20,
          marginBottom: marginBottom || 30,
        }}
      >
        {label}
      </div>

      <Grid container spacing={3}>
        {(data ?? []).map((e, i) => (
          <Grid item xs={6} key={i}>
            <div className={classes.fieldGroup}>
              <FieldVirtualAccount
                name={e.label_en === "" ? "" : e.label_en}
                val={mappingValue(e.originalKey, e.value) || "-"}
              />
              {!parentKyesNoValiedate.includes(
                templateMapping[containerKey].template[index].key
              ) &&
              name !== "suratKuasa" &&
              !arrayNotValidate.includes(e.originalKey) ? (
                <ButtonValidate
                  disabled={disabled || e?.disabled || e.value === null}
                  buttonKey={e.label_en}
                  checkList={data}
                  disabledUncheck={
                    [
                      "registeredAccount",
                      "userProfileMaker",
                      "userProfileReleaser",
                      "userProfileApproval",
                      "userProfileAdminMaker",
                      "userProfileAdminApproval",
                    ].includes(e.objKeys)
                      ? rejectedList?.includes(
                          [e.objKeys, e?.originalKey].join("_")
                        )
                      : rejectedList?.includes(e?.originalKey)
                  }
                  checked={e.checked}
                  onClick={() => handleCheckList(i)}
                />
              ) : null}
            </div>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

const BranchDetail = ({ branch }) => (
  <div
    style={{
      marginTop: 30,
      width: 350,
      height: 140,
      borderRadius: "10px",
      padding: "15px 20px",
      border: "1px solid #E6EAF3",
    }}
  >
    <div
      style={{
        fontFamily: "FuturaHvBT",
        fontSize: 17,
        fontWeight: 700,
        marginBottom: 11,
        color: Colors.dark.hard,
      }}
    >
      {branch?.branchName}
    </div>
    <div
      style={{
        fontFamily: "FuturaBQ",
        fontSize: 13,
        color: "#7B87AF",
      }}
    >
      <div>Telephone : {branch?.telephone ?? "-"}</div>
      <div>Fax : {branch?.fax ?? "-"}</div>
      <div>{branch?.address ?? "-"}</div>
    </div>
  </div>
);
const SegmentationDetail = ({
  props: { segmentation, setOpenLimitPackage, setOpenMenuPackage },
}) => (
  <div
    style={{
      marginTop: 30,
      width: 350,
      height: 387,
      borderRadius: "10px",
      padding: "15px 20px",
      border: "1px solid #E6EAF3",
    }}
  >
    <div
      style={{
        fontFamily: "FuturaHvBT",
        fontSize: 17,
        fontWeight: 700,
        color: Colors.dark.hard,
      }}
    >
      {segmentation?.name ?? "-"}
    </div>
    <div style={{ margin: "10px 0", height: 108 }}>
      {segmentation?.description ?? "-"}
    </div>
    <div
      style={{
        fontFamily: "FuturaHvBT",
        fontSize: 17,
        fontWeight: 700,
        color: Colors.dark.hard,
      }}
    >
      Segmentation Limit :
    </div>
    <div style={{ margin: "10px 0" }}>
      {segmentation?.limitPackage?.name || "-"}
    </div>

    <ButtonViewDetail onClick={() => setOpenLimitPackage(true)}>
      View Details <ArrowRight />
    </ButtonViewDetail>

    <div
      style={{
        fontFamily: "FuturaHvBT",
        fontSize: 17,
        fontWeight: 700,
        color: Colors.dark.hard,
      }}
    >
      Segmentation Menu :
    </div>
    <div style={{ margin: "10px 0" }}>
      {segmentation?.menuPackage?.[0]?.name || "-"}
    </div>
    <ButtonViewDetail onClick={() => setOpenMenuPackage(true)}>
      View Details <ArrowRight />
    </ButtonViewDetail>
  </div>
);

const PendingtaskregisDetailBeforeLogin = ({ classes, fase, params }) => {
  const dispatch = useDispatch();
  const [openLimitPackage, setOpenLimitPackage] = useState(false);
  const [openMenuPackage, setOpenMenuPackage] = useState(false);
  const {
    isLoading,
    isLoadingDownload,
    detail,
    rejectedList,
    templateMapping,
    segmentation,
    branch,
  } = useSelector((e) => e.pendingTaskRegistration);

  const checklistDocuments = (i, { keys }) => {
    const { containerKey } = keys;

    const fieldTarget = templateMapping;

    fieldTarget[containerKey].documents[i] = {
      ...fieldTarget[containerKey].documents[i],
      checked: !fieldTarget[containerKey]?.documents[i]?.checked,
    };

    if (
      fieldTarget[containerKey].documents[i]?.label?.includes(
        "Power of Attorney"
      )
    ) {
      const indexKey = fieldTarget[containerKey].documents[i].label
        .split(" ")
        .pop();
      fieldTarget.before.template[2].fields[indexKey - 1] = {
        ...fieldTarget.before.template[2].fields[indexKey - 1],
        checked: !fieldTarget.before.template[2].fields[indexKey - 1].checked,
      };
    }

    dispatch(setTemplate(fieldTarget));
  };

  const disabledBySatatus = () =>
    detail?.status === "APPROVE" ||
    detail?.status === "REJECT" ||
    !params?.isAllowedTransaction;

  const handleSliceText = (t) => {
    if (!t) return null;
    if (t?.length >= 15) return `${t.slice(0, 15)}...`;
    return t;
  };

  if (isLoading) {
    return (
      <div className={classes.container}>
        <div>
          <PapperContainer isLoading={isLoading} />
        </div>
      </div>
    );
  }

  if (openLimitPackage)
    return <ViewLimitPackage onBack={() => setOpenLimitPackage(false)} />;
  if (openMenuPackage)
    return (
      <ViewMenuPackage
        id={segmentation?.listSegmentation?.[0]?.menuPackage?.[0]?.id}
        onBack={() => {
          setOpenMenuPackage(false);
          dispatch(setMenuPackageDetail([]));
        }}
      />
    );

  return (
    <div className={classes.container}>
      <div>
        {Object.keys(templateMapping ?? {}).map((containerKey) => (
          <PapperContainer
            isLoading={isLoading}
            title={
              containerKey === "documents"
                ? "Documents"
                : templateMapping[containerKey].label
            }
            style={{ marginBottom: 40 }}
          >
            {containerKey !== "documents" &&
              templateMapping[containerKey].template.map((template, index) =>
                // for optional form regis
                template?.key === "segmentationId" ? (
                  <Collapse
                    label={template?.label}
                    defaultOpen
                    style={{ marginTop: index > 0 ? 50 : 0 }}
                  >
                    <SegmentationDetail
                      props={{
                        setOpenLimitPackage,
                        setOpenMenuPackage,
                        segmentation: segmentation?.listSegmentation?.[0],
                      }}
                    />
                  </Collapse>
                ) : template?.key === "branch" ? (
                  <Collapse
                    label={template?.label}
                    defaultOpen
                    style={{ marginTop: index > 0 ? 50 : 0 }}
                  >
                    <BranchDetail branch={branch} />
                  </Collapse>
                ) : template.key === "suratKuasa" ? (
                  <Collapse
                    label={template?.label}
                    defaultOpen
                    style={{ marginTop: index > 0 ? 50 : 0 }}
                  >
                    <div
                      style={{
                        fontFamily: "futuraHvBT",
                        fontSize: 17,
                        fontWeight: 400,
                        marginTop: 30,
                      }}
                    >
                      Admin Maker
                    </div>
                    <MappingData
                      props={{
                        index,
                        name: "suratKuasa",
                        templateMapping,
                        classes,
                        data: template.fields[1]?.ADMIN_USER_MAKER,
                        dispatch,
                        keys: { containerKey, template },
                        disabled: disabledBySatatus() || template?.disabled,
                      }}
                    />

                    <div
                      style={{
                        fontFamily: "futuraHvBT",
                        fontSize: 17,
                        fontWeight: 400,
                        marginTop: 30,
                      }}
                    >
                      Maker Approver
                    </div>
                    <MappingData
                      props={{
                        name: "suratKuasa",

                        index,
                        templateMapping,
                        classes,
                        data: template.fields[2]?.ADMIN_USER_RELEASER,
                        dispatch,
                        keys: { containerKey, template },
                        disabled: disabledBySatatus() || template?.disabled,
                      }}
                    />
                  </Collapse>
                ) : !template.fields.length ? null : (
                  <Collapse
                    label={template?.label}
                    defaultOpen
                    style={{ marginTop: index > 0 ? 50 : 0 }}
                  >
                    <MappingData
                      props={{
                        index,
                        templateMapping,
                        classes,
                        data: template.fields,
                        dispatch,
                        keys: { containerKey, template },
                        disabled: disabledBySatatus() || template?.disabled,
                      }}
                    />
                  </Collapse>
                )
              )}

            {containerKey === "documents" && (
              <Grid container spacing={3}>
                {templateMapping[containerKey]?.documents.map((name, i) => {
                  const { base64, type } = DocType(name?.image);
                  return (
                    <Grid item xs={4} key={i}>
                      <Paper className={classes.paperDocument}>
                        <Paper
                          onClick={() => {
                            if (type === "file") {
                              previewPDF(
                                name?.image,
                                "application/pdf",
                                "data.pdf"
                              );
                            } else {
                              previewImageBase64(base64);
                            }
                          }}
                        >
                          <div className="img">
                            <img
                              src={type === "file" ? imageFile : base64}
                              alt="ktp"
                              style={{
                                maxWidth: "100%",
                                maxHeight: "100%",
                                display: "block",
                              }}
                            />
                          </div>
                        </Paper>
                        <div
                          style={{
                            padding: "18px 20px",
                          }}
                        >
                          <span
                            style={{
                              fontSize: 13,
                              fontFamily: "FuturaBQ",
                              color: "#7B87AF",
                            }}
                          >
                            {name.label}
                          </span>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              alignItems: "center",
                            }}
                          >
                            <div>
                              <div>
                                <div>{handleSliceText(name[name?.key])}</div>
                              </div>
                            </div>
                            <ButtonValidate
                              section="doc"
                              disabled={disabledBySatatus() || name?.disabled}
                              style={{ margin: 0 }}
                              buttonKey={name.label}
                              checkList={[]}
                              checked={name.checked}
                              disabledUncheck={
                                name.key?.includes("Power of Attorney Account")
                                  ? detail?.additionalData?.documents
                                      ?.additionalSuratKuasa?.[
                                      name.key?.split(" ").pop() - 1
                                    ]
                                  : rejectedList?.includes(name?.key)
                              }
                              onClick={() =>
                                checklistDocuments(i, {
                                  keys: { containerKey, name },
                                })
                              }
                            />
                          </div>
                        </div>
                      </Paper>
                    </Grid>
                  );
                })}

                {detail?.downloadSignatureDocumentUrlList?.length ? (
                  <Grid item xs={4}>
                    <Paper className={classes.paperDocument}>
                      <Paper>
                        <div className="img">
                          <img
                            src={imageFile}
                            alt="ktp"
                            style={{
                              maxWidth: "100%",
                              maxHeight: "100%",
                              display: "block",
                            }}
                          />
                        </div>
                      </Paper>
                      <div className="footer">
                        <FieldVirtualAccount
                          name="Data Digital Signature"
                          val={
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <div>Download Data</div>
                              <div>
                                <GeneralButton
                                  label="Downloads"
                                  width="76px"
                                  height="23px"
                                  disabled={isLoadingDownload}
                                  style={{
                                    marginRight: 11,
                                    fontSize: 9,
                                    fontFamily: "FuturaMdBT",
                                    fontWeight: 700,
                                    padding: 0,
                                  }}
                                  onClick={() => {
                                    const signaturePath =
                                      detail
                                        ?.downloadSignatureDocumentUrlList?.[0];
                                    if (
                                      signaturePath.indexOf("http://") === 0 ||
                                      signaturePath.indexOf("https://") === 0
                                    ) {
                                      // Open File
                                      window.open(
                                        detail
                                          ?.downloadSignatureDocumentUrlList?.[0],
                                        "_blank",
                                        "noopener noreferrer"
                                      );
                                    } else {
                                      dispatch(
                                        downloadFileSignature(
                                          {
                                            file: signaturePath,
                                          },
                                          `Digital_Signature-${signaturePath
                                            .split("/")
                                            .pop()}`
                                        )
                                      );
                                    }
                                  }}
                                />
                              </div>
                            </div>
                          }
                          size="12px"
                        />
                      </div>
                    </Paper>
                  </Grid>
                ) : null}
              </Grid>
            )}
          </PapperContainer>
        ))}
      </div>
    </div>
  );
};

export default PendingtaskregisDetailBeforeLogin;
