import { Button, makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";
import TableCollapse from "components/BN/TableIcBB/collapse";
import formatNumber from "helpers/formatNumber";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as EditSVG } from "assets/icons/BN/edit-retangle-white.svg";
import AntdTextField from "components/BN/TextField/AntdTextField";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import { useDispatch, useSelector } from "react-redux";
// import {
//   handleServiceCurrencyInfo,
//   handleServiceRegisLimitPackageDetail,
//   setLimitPackageDetail,
// } from "stores/actions/managementPerusahaan/Registration";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import { handleServiceRegisMenuPackageDetail } from "stores/actions/pendingTask/registration";

const useStyles = makeStyles({
  container: {
    borderRadius: 20,
    backgroundColor: "white",
    padding: 20,
    width: 930,
    position: "relative",
  },
  title: {
    fontSize: 20,
    fontFamily: "FuturaHvBT",
    color: Colors.dark.hard,
    marginBottom: 20,
  },
  label: {
    fontFamily: "FuturaHvBT",
    fontSize: 10,
    color: Colors.dark.medium,
    marginBottom: 7,
  },
  value: {
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    color: Colors.dark.hard,
    marginBottom: 7,
  },
});

const dataHeader = ({ classes }) => [
  {
    title: "Currency Matriks",
    key: "currencyMatrixName",
    width: 150,
  },
  {
    title: "Currency",
    key: "currencyCode",
  },
  {
    title: "Max. Nominal/Day",
    key: "nominalday",
    render: (e) => (
      <div>
        <div className={classes.label}>Max. Nominal/Day</div>
        <div className={classes.value}>
          {formatNumber(e?.maxAmountTransactionOfDay)}
        </div>
        <div className={classes.label}>Max. Transaction/Day</div>
        <div className={classes.value}>{e?.maxTransaction} Transaction</div>
      </div>
    ),
  },
  {
    title: "Max & Min Transaction",
    key: "mintrx",
    render: (e) => (
      <div>
        <div className={classes.label}>Max. Transaction</div>
        <div className={classes.value}>
          {formatNumber(e?.maxAmountTransaction)}
        </div>
        <div className={classes.label}>Min. Transaction</div>
        <div className={classes.value}>
          {formatNumber(e?.minAmountTransaction)}{" "}
        </div>
      </div>
    ),
  },
];

const ViewLimitPackage = ({ onBack, id }) => {
  const classes = useStyles();

  const { limitPackage } = useSelector((e) => e.pendingTaskRegistration);

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div className={classes.container}>
        <Button
          startIcon={
            <img src={arrowLeft} style={{ paddingTop: "10px" }} alt="Back" />
          }
          onClick={onBack}
        />
        <div className={classes.title}>Limit Package</div>
        <TableCollapse
          headerContent={dataHeader({
            classes,
          })}
          dataContent={limitPackage?.data}
          collapse
          bordered
          selectedValue={limitPackage?.data?.map((elm) => elm.id)}
        />
      </div>
    </div>
  );
};

export default ViewLimitPackage;
