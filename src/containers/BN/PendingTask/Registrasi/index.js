import React, { useEffect, useState } from "react";
import { makeStyles, Button as MUIButton } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import DeletePopup from "components/BN/Popups/Delete";

// component
import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import { pathnameCONFIG } from "configuration";
import { useDispatch, useSelector } from "react-redux";
import { handleGetPendingTaskRegistration } from "stores/actions/pendingTask";
import moment from "moment";
import useDebounce from "utils/helpers/useDebounce";
import Filter from "components/BN/Filter/GeneralFilter";
import {
  setRegisterIdandFase,
  setHandleClearError,
} from "stores/actions/pendingTask/registration";
import { checkOnline } from "stores/actions/errorGeneral";
import Toast from "components/BN/Toats";
import GeneralButton from "components/BN/Button/GeneralButton";

// helpers
import { searchDate, setLocalStorage } from "utils/helpers";
import usePrevious from "utils/helpers/usePrevious";

// component
import Title from "../../../../components/BN/Title";

export const registrationFase = (fase) => {
  switch (fase) {
    case "BEFORE_LOGIN":
      return {
        label: fase?.split("_")?.join(" ").toLowerCase(),
        badge: "danger",
      };
    case "AFTER_LOGIN":
      return {
        label: fase?.split("_")?.join(" ").toLowerCase(),
        badge: "purpleTextDark",
      };
    case "AFTER_LOGIN_EXECUTE":
      return {
        label: fase
          ?.replace("AFTER_LOGIN_EXECUTE", "AFTER_LOGIN")
          ?.split("_")
          ?.join(" ")
          .toLowerCase(),
        badge: "purpleTextDark",
      };
    default:
      return {
        label: fase?.split("_")?.join(" ").toLowerCase(),
        badge: "",
      };
  }
};

const dataHeader = ({ history, clickToDetail }) => [
  {
    title: "Date & Time",
    key: "date",
    render: (rowData) =>
      moment(rowData.date, "DD/MM/YYYY,HH:mm:ss").format("YYYY-MM-DD HH:mm:ss"),
  },
  {
    title: "Company ID",
    key: "corporateId",
  },
  {
    title: "Company Name",
    key: "corporateName",
  },
  {
    title: "Status",
    key: "status",
    render: (rowData) => {
      let status = "";

      if (rowData?.approvedStatusSequence?.toLowerCase()?.includes("waiting")) {
        status = "blue";
      }

      if (
        rowData?.approvedStatusSequence?.toLowerCase()?.includes("approved")
      ) {
        status = "green";
      }

      if (
        rowData?.approvedStatusSequence?.toLowerCase()?.includes("rejected")
      ) {
        status = "red";
      }

      return (
        <Badge label={rowData?.approvedStatusSequence} type={status} outline />
      );
    },
  },
  {
    title: "Registration",
    key: "",
    render: (rowData) => (
      <Badge
        styleBadge={{ textTransform: "capitalize", fontFamily: "FuturaMdBT" }}
        label={registrationFase(rowData?.registrationType).label}
        type={registrationFase(rowData?.registrationType).badge}
      />
    ),
  },
  {
    title: "Registered By",
    key: "registeredBy",
    render: (rowData) =>
      rowData?.registeredBy === "0"
        ? "Nasabah"
        : rowData?.registeredBy === "1"
        ? "Admin"
        : "-",
  },
  {
    title: "",
    key: "message",
    width: 100,
    render: (rowData) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{
            padding: "6px 10px",
            fontSize: 9,
            fontWeight: "bold",
            fontFamily: "FuturaMdBT",
          }}
          onClick={() => clickToDetail(rowData)}
        />
      </div>
    ),
  },
];

const RegistrationPendingTask = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { data, error, isLoading } = useSelector(
    (e) => e.pendingTaskRegistration
  );

  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState("");
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);

  const prevPage = usePrevious(page);

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
      gap: "20px",
      display: "flex",
      flexDirection: "column",
    },
  });

  const debounce = useDebounce(dataSearch, 1000);

  const classes = useStyles();

  const handleFetchingData = async () => {
    await dispatch(checkOnline());
    const status = dataFilter?.tabs?.tabValue3;
    const statusOpt = [null, 1, 3, 2];

    dispatch(
      handleGetPendingTaskRegistration({
        startDate: dataSearch
          ? null
          : dataFilter?.date?.dateValue1
          ? new Date(searchDate(dataFilter?.date?.dateValue1))
          : null,
        endDate: dataSearch
          ? null
          : dataFilter?.date?.dateValue2
          ? new Date(searchDate(dataFilter?.date?.dateValue2))
          : null,
        pageNumber: page - 1,
        pageSize: 10,
        searchValue: dataSearch,
        statusRegistration: statusOpt[status],
      })
    );
  };

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      handleFetchingData();
    } else {
      setPage(1);
    }
  }, [debounce, dataFilter, page]);

  const clickToDetail = (rowData) => {
    dispatch(
      setRegisterIdandFase({
        referenceNumber: rowData.reffNo,
        fase: rowData?.registrationType,
        corporateOpenAccountId: rowData?.corporateOpenAccountId,
        isAllowedTransaction: rowData?.isAllowedTransaction,
        status: rowData?.status,
      })
    );
    const lsParams = {
      referenceNumber: rowData.reffNo,
      fase: rowData?.registrationType,
      corporateOpenAccountId: rowData?.corporateOpenAccountId,
      isAllowedTransaction: rowData?.isAllowedTransaction,
      status: rowData?.status,
      page: page - 1,
    };
    setLocalStorage("pt_registration", JSON.stringify(lsParams));
    history.push(pathnameCONFIG.PENDING_TASK.REGISTRATION_DETAIL);
  };

  return (
    <div className={classes.handlingofficer}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <Title label="Pending Task - Registration">
        <Search
          options={[
            "Nomor token",
            "Input nomor",
            "Input nomor",
            "Input nomor",
            "Input nomor",
            "Status",
          ]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Company ID, Company Name"
          placement="bottom"
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={(e) => {
            setDataFilter(e);
            setDataSearch("");
          }}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />

        <TableICBB
          headerContent={dataHeader({ history, clickToDetail })}
          dataContent={data?.pendingTaskRegistrationDtoList ?? []}
          page={page}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
          setPage={setPage}
          isLoading={isLoading}
        />
      </div>
    </div>
  );
};

RegistrationPendingTask.propTypes = {};

RegistrationPendingTask.defaultProps = {};

export default RegistrationPendingTask;
