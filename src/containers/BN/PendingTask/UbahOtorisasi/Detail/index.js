import { Button, makeStyles, Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

// component
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import PapperContainer from "components/BN/PapperContainer";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Title from "components/BN/Title";
import { ReactComponent as CheckIcon } from "assets/icons/BN/check.svg";
import { ReactComponent as CheckClose } from "assets/icons/BN/close.svg";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import illustrationX from "assets/images/BN/illustrationred.png";
import Toast from "components/BN/Toats";
import { useDispatch, useSelector } from "react-redux";

import {
  handleGetPendingTaskUbahOtorisasiDetail,
  handleUbahOtorisasiExecute,
  setClear,
  setDetailData,
  setHandleClearError,
  setTemplate,
} from "stores/actions/pendingTask/ubahOtorisasi";

import {
  detailFormData,
  filterExecuteUnchacked,
} from "stores/actions/pendingTask";
import AddComment from "components/BN/Popups/AddComment";
import {
  compare,
  formatPhoneNumberStripNoSpace,
  getLocalStorage,
} from "utils/helpers";

import ButtonIcon from "components/SC/IconButton";
import Collapse from "components/BN/Collapse";
import { pathnameCONFIG } from "configuration";
// import { MappingData } from "../../components";

const useStyles = makeStyles({
  institusiva: {},
  container: {
    padding: "20px 30px",
    display: "flex",
    justifyContent: "center",
  },
  rowcontainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "30px",
  },
  fieldTitle: {
    fontSize: 13,
    fontWeight: 400,
    fontFamily: "FuturaHvBT",
    color: "#7B87AF",
    marginBottom: "5px",
  },
  subFieldTitle: {
    fontSize: 14,
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    color: "#374062",
  },
  mainContainer: {
    margin: "63px 115px",
    borderRadius: "20px",
    overflow: "hidden",
    zIndex: 10,
  },
  mainContent: {
    backgroundColor: "#fff",
    padding: "50px",
  },
  fieldGroup: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonAction: {
    display: "flex",
    marginTop: 40,
    justifyContent: "center",
  },
  paperDocument: {
    boxShadow: "none",
    width: 237,
    height: 237,
    borderRadius: 20,
    overflow: "hidden",
    border: "1px solid #F4F7FB",
    "& .img": {
      width: "100%",
      height: "147px",
      backgroundColor: "#F4F7FB",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    "& .footer": {
      display: "flex",
      width: "100%",
      padding: "17.5px 20px",
      alignItems: "center",
      justifyContent: "space-between",
    },
  },

  buttonGroup: {
    width: "100%",
    backgroundColor: "white",
    padding: "22px 40px",
    marginTop: 59,
    display: "flex",
    justifyContent: "space-between",
  },
});

export const FieldVirtualAccount = ({ name, val, size = "15px" }) => {
  const classes = useStyles();
  return (
    <div style={{ flex: 1, fontSize: size, fontFamily: "FuturaBQ" }}>
      <div className={classes.fieldTitle}>{`${name} : `}</div>
      <div width="350px" className={classes.subFieldTitle}>
        {val}
      </div>
    </div>
  );
};

export const ButtonValidate = ({
  buttonKey,
  style,
  onClick,
  disabled = false,
  checked,
  disabledUncheck,
}) => (
  <div style={{ display: "flex", marginRight: 20, ...style }}>
    <ButtonIcon
      color={checked ? (disabled ? "#BCC8E7" : "#75D37F") : "#BCC8E7"}
      width={24}
      height={24}
      style={{ padding: 0, marginRight: 10 }}
      icon={<CheckIcon />}
      onClick={onClick}
      disabled={disabled}
    />
    <ButtonIcon
      color={
        disabledUncheck
          ? "#D14848"
          : !checked
          ? disabled
            ? "#BCC8E7"
            : "#D14848"
          : "#BCC8E7"
      }
      width={24}
      height={24}
      style={{ padding: 0 }}
      icon={<CheckClose />}
      disabled={disabled}
      onClick={onClick}
    />
  </div>
);

const MappingData = ({
  props: {
    templateMapping,
    classes,
    index,
    data,
    dispatch,
    label,
    keys,
    disabled,
    marginBottom,
    name,
  },
}) => {
  const { rejectedList } = useSelector((e) => e.pendingTaskChangeAutorization);

  const handleCheckList = (i) => {
    const { containerKey } = keys;

    const fieldTarget = templateMapping;

    fieldTarget[containerKey].template[index].fields[i] = {
      ...fieldTarget[containerKey].template[index].fields[i],
      checked: !fieldTarget[containerKey].template[index].fields[i].checked,
    };

    dispatch(setTemplate(fieldTarget));
  };

  const mappingValue = (key, el) => {
    if (key === "phoneNumber")
      return `+62 ${formatPhoneNumberStripNoSpace(parseInt(el))}`;
    if (key === "phoneHome")
      return el ? formatPhoneNumberStripNoSpace(el) : "-";
    if (key === "corporatePhoneNumber")
      return formatPhoneNumberStripNoSpace(el);

    return el;
  };
  const arrayNotValidate = [
    "accountNumber",
    "accountType",
    "corporateId",
    "userId",
    "province",
    "district",
    "subDistrict",
    "village",
    "postalCode",
  ];

  const parentKyesNoValiedate = [
    "userProfileAdminApproval",
    "userProfileAdminMaker",
    "additionalAccounts",
  ];

  const { containerKey } = keys;

  return (
    <div>
      <div
        style={{
          fontFamily: "FuturaMdBT",
          color: "#374062",
          fontSize: 20,
          marginBottom: marginBottom || 30,
        }}
      >
        {label}
      </div>

      <Grid container spacing={3}>
        {(data ?? []).map((e, i) => (
          <Grid item xs={6} key={i}>
            <div className={classes.fieldGroup}>
              <FieldVirtualAccount
                name={e.label_en}
                val={mappingValue(e.originalKey, e.value) || "-"}
              />
              {!parentKyesNoValiedate.includes(
                templateMapping[containerKey].template[index].key
              ) &&
              name !== "suratKuasa" &&
              !arrayNotValidate.includes(e.originalKey) ? (
                <ButtonValidate
                  disabled={disabled || e?.disabled || !e.value}
                  buttonKey={e.label_en}
                  checkList={data}
                  disabledUncheck={
                    [
                      "registeredAccount",
                      "userProfileMaker",
                      "userProfileReleaser",
                      "userProfileApproval",
                      "userProfileAdminMaker",
                      "userProfileAdminApproval",
                    ].includes(e.objKeys)
                      ? rejectedList?.includes(
                          [e.objKeys, e?.originalKey].join("_")
                        )
                      : rejectedList?.includes(e?.originalKey)
                  }
                  checked={e.checked}
                  onClick={() => handleCheckList(i)}
                />
              ) : null}
            </div>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
const PendingTaskChangeAuthorizationDetail = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const APPROVE = "APPROVE";
  const REJECT = "REJECT";
  const {
    detail,
    error,
    detailData,
    detailDataKeys,
    isExeucteSuccess,
    isLoadingExecute,
    templateMapping,
    isLoading,
  } = useSelector((e) => e?.pendingTaskChangeAutorization);

  const corporateAdmin =
    detail?.corporateOpenAccountMetadataDto?.corporateProfile?.corporateAdmin?.toLowerCase();

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
    isReject: false,
  });
  const [openModalCom, setopenModalCom] = useState(false);
  const [commentReject, setCommReject] = useState("");
  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });

  const params = JSON.parse(getLocalStorage("pt_ubahotorisasi"));

  useEffect(() => {
    const paramsPayload = {
      referenceNumber: params?.reffNo,
      corporateOpenAccountId: params?.corporateOpenAccountId,
      isPendingTask: true,
    };

    dispatch(handleGetPendingTaskUbahOtorisasiDetail(paramsPayload));
  }, [params?.corporateOpenAccountId]);

  const handleBack = () => {
    dispatch(setClear());
    history.push(pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION);
  };

  useEffect(() => {
    setCommReject("");
    if (isExeucteSuccess.action === APPROVE && isExeucteSuccess.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Approved",
        submessage: "",
        isOpen: true,
        isReject: false,
      });
    }

    if (isExeucteSuccess.action === REJECT && isExeucteSuccess.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Rejected",
        isOpen: true,
        isReject: true,
      });
    }
  }, [isExeucteSuccess]);

  const executeOtorisasi = async (action) => {
    let lang = "label_en";

    if (detail?.metadata?.userProfile?.language === "IDN") {
      lang = "label_id";
    } else if (detail?.metadata?.userProfile?.language === "ENG") {
      lang = "label_en";
    }

    const payload = {
      status: action,
      referenceNumber: params?.reffNo,

      rejectedReason: commentReject || "",
      workflowLimitTransactionId: detail?.workflowLimitTransactionId,
    };

    dispatch(handleUbahOtorisasiExecute(payload));
  };

  const disabledBySatatus = () =>
    detail?.status === "APPROVE" ||
    detail?.status === "REJECT" ||
    !params?.isAllowedTransaction;

  const handleUnchecked = () => {
    let available = false;

    available =
      templateMapping?.after?.template?.findIndex((elm) => {
        if (elm.key === "suratKuasa") return false;
        return elm?.fields?.findIndex((fi) => !fi.checked) >= 0;
      }) >= 0;

    return available;
  };

  if (isLoading) {
    return (
      <div className={classes.container}>
        <div>
          <PapperContainer isLoading={isLoading} />
        </div>
      </div>
    );
  }

  return (
    <div>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <div style={{ position: "relative" }}>
        <Button
          startIcon={
            <img
              src={arrowLeft}
              style={{ paddingLeft: "23px", paddingTop: "10px" }}
              alt="Kembali"
            />
          }
          onClick={handleBack}
        />
        <Title label="Detail Change Authorization" />
        <div className={classes.container}>
          <PapperContainer title="Detail Change Authorization">
            {templateMapping?.after.template.map((template, index) => (
              <div key={index}>
                {template.key === "suratKuasa" ? (
                  <Collapse
                    label={template?.label}
                    defaultOpen
                    style={{ marginTop: index > 0 ? 50 : 0 }}
                  >
                    <div
                      style={{
                        fontFamily: "futuraHvBT",
                        fontSize: 17,
                        fontWeight: 400,
                        marginTop: 30,
                      }}
                    >
                      Admin Maker
                    </div>
                    <MappingData
                      props={{
                        index,
                        name: "suratKuasa",
                        templateMapping,
                        classes,
                        data: template.fields[0]?.ADMIN_USER_MAKER,
                        dispatch,
                        keys: { containerKey: "after", template },
                        disabled: disabledBySatatus() || template?.disabled,
                      }}
                    />

                    <div
                      style={{
                        fontFamily: "futuraHvBT",
                        fontSize: 17,
                        fontWeight: 400,
                        marginTop: 30,
                      }}
                    >
                      Maker Approver
                    </div>
                    <MappingData
                      props={{
                        name: "suratKuasa",

                        index,
                        templateMapping,
                        classes,
                        data: template.fields[1]?.ADMIN_USER_RELEASER,
                        dispatch,
                        keys: { containerKey: "after", template },
                        disabled: disabledBySatatus() || template?.disabled,
                      }}
                    />
                  </Collapse>
                ) : (
                  <Collapse
                    label={template?.label}
                    defaultOpen
                    style={{ marginTop: index > 0 ? 50 : 0 }}
                  >
                    <MappingData
                      props={{
                        index,
                        templateMapping,
                        classes,
                        data: template.fields,
                        dispatch,
                        keys: { containerKey: "after", template },
                        disabled: disabledBySatatus() || template?.disabled,
                      }}
                    />
                  </Collapse>
                )}
              </div>
            ))}
          </PapperContainer>
        </div>
      </div>
      <div className={classes.buttonGroup}>
        <ButtonOutlined
          disabled={
            isLoading ||
            isLoadingExecute ||
            detail?.status === "APPROVE" ||
            detail?.status === "REJECT" ||
            !params?.isAllowedTransaction
          }
          label="Reject"
          onClick={() => setopenModalCom(true)}
        />
        <GeneralButton
          label="Approve"
          loading={isLoadingExecute}
          disabled={
            isLoading ||
            isLoadingExecute ||
            detail?.status === "APPROVE" ||
            detail?.status === "REJECT" ||
            !params?.isAllowedTransaction
          }
          onClick={() => {
            setCommReject("");
            setConfirmationModal({
              message: "Are you sure to approve the activity?",
              submessage: "You can't undo this action",
              isOpen: true,
              action: APPROVE,
            });
          }}
        />
      </div>

      <SuccessConfirmation
        img={successConfirmModal.isReject && illustrationX}
        isOpen={successConfirmModal.isOpen}
        handleClose={() => {
          setSuccessConfirmModal({
            message: "",
            submessage: "",
            isOpen: false,
            isReject: false,
          });
          dispatch(setClear());
          history.push(pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION);
        }}
        title="Success"
        message={successConfirmModal.message}
      />
      <DeletePopup
        isOpen={confirmationModal.isOpen}
        handleClose={() =>
          setConfirmationModal({ isOpen: false, message: "", submessage: "" })
        }
        onContinue={() => {
          setopenModalCom(false);
          if (confirmationModal.action === APPROVE) {
            executeOtorisasi(APPROVE);
          } else if (confirmationModal.action === REJECT) {
            executeOtorisasi(REJECT);
          }

          setConfirmationModal({
            isOpen: false,
            message: "",
            submessage: "",
            action: "",
          });
        }}
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
        loading={isLoadingExecute}
      />

      <AddComment
        title="Rejection Comment"
        isOpen={openModalCom}
        handleClose={() => setopenModalCom(false)}
        onContinue={(comment, message) => {
          setCommReject(message);
          setConfirmationModal({
            message: "Are you sure to reject the activity?",
            submessage: "You can't undo this action",
            isOpen: true,
            action: REJECT,
          });
        }}
      />
    </div>
  );
};

PendingTaskChangeAuthorizationDetail.propTypes = {};

PendingTaskChangeAuthorizationDetail.defaultProps = {};

export default PendingTaskChangeAuthorizationDetail;
