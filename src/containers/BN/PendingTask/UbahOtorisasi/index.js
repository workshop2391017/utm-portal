import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";

// component
import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";
import { pathnameCONFIG } from "configuration";
import { useDispatch, useSelector } from "react-redux";
import {
  handleGetPendingTaskUbahOtorisasi,
  setHandleClearError,
} from "stores/actions/pendingTask/ubahOtorisasi";
import GeneralButton from "components/BN/Button/GeneralButton";
import useDebounce from "utils/helpers/useDebounce";
import Filter from "components/BN/Filter/GeneralFilter";
import Toast from "components/BN/Toats";

// helpers
import { searchDate, setLocalStorage } from "utils/helpers";
import usePrevious from "utils/helpers/usePrevious";
import moment from "moment";

const dataHeader = ({ history, clickToDetail }) => [
  {
    title: "Date & Time",
    key: "date",
    render: (rowData) => rowData.date,
  },
  {
    title: "Company ID",
    key: "corporateId",
  },
  {
    title: "Company Name",
    key: "corporateName",
  },

  {
    title: "Status",
    key: "status",
    render: (rowData) => {
      let status = "";
      let message = "";

      switch (rowData.status) {
        case 1:
          status = "blue";
          message = "Waiting Approval";
          break;
        case 2:
          status = "green";
          message = "Approved";
          break;
        case 3:
          status = "red";
          message = "Rejected";
          break;

        default:
          status = "";
      }

      return <Badge label={message} type={status} outline />;
    },
  },
  {
    title: "Segmentation",
    key: "segmentationType",
  },
  {
    title: "By",
    key: "registeredBy",
    render: (rowData) =>
      rowData?.registeredBy === "0"
        ? "Nasabah"
        : rowData?.registeredBy === "1"
        ? "Admin"
        : "-",
  },
  {
    title: "",
    key: "message",
    width: 100,
    render: (rowData) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{
            padding: "6px 10px",
            fontSize: 9,
            fontWeight: "bold",
            fontFamily: "FuturaMdBT",
          }}
          onClick={() => clickToDetail(rowData)}
        />
      </div>
    ),
  },
];

const PendingTaskChangeAuthorization = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { data, error, isLoading, totalElements, totalPages } = useSelector(
    (e) => e?.pendingTaskChangeAutorization
  );

  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);
  const prevPage = usePrevious(page);

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
      gap: "20px",
      display: "flex",
      flexDirection: "column",
    },
  });
  const classes = useStyles();
  const debounceSearch = useDebounce(dataSearch, 1300);

  const loadData = () => {
    const status = dataFilter?.tabs?.tabValue3;
    const statusOpt = [null, 1, 3, 2];

    const paramsPayload = {
      startDate: dataFilter?.date?.dateValue1
        ? new Date(dataFilter?.date?.dateValue1)
        : null,
      endDate: dataFilter?.date?.dateValue2
        ? new Date(dataFilter?.date?.dateValue2)
        : null,
      pageNumber: page - 1,
      pageSize: 10,
      searchValue: debounceSearch,
      statusRegistration: statusOpt[status],
    };
    dispatch(handleGetPendingTaskUbahOtorisasi(paramsPayload));
  };

  // useEffect(() => {
  //   loadData();
  // }, [page]);

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      loadData();
    } else {
      setPage(1);
    }
  }, [debounceSearch, dataFilter, page]);

  const clickToDetail = (rowData) => {
    const lsParams = {
      ...rowData,
      referenceNumber: rowData.reffNo,
      fase: rowData?.registrationType,
      corporateOpenAccountId: rowData?.corporateOpenAccountId,
    };
    setLocalStorage("pt_ubahotorisasi", JSON.stringify(lsParams));
    history.push(pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION_DETAIL);
  };

  return (
    <div className={classes.handlingofficer}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <Title label="Pending Task - Change Authorization">
        <Search
          options={[
            "Nomor token",
            "Input nomor",
            "Input nomor",
            "Input nomor",
            "Input nomor",
            "Status",
          ]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Company ID"
          placement="bottom"
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={(e) => {
            setDataFilter(e);
            setDataSearch("");
          }}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />

        <TableICBB
          headerContent={dataHeader({ history, clickToDetail })}
          dataContent={data?.pendingTaskRegistrationDtoList ?? []}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          isLoading={isLoading}
          totalElement={data?.totalElements}
        />
      </div>
    </div>
  );
};

PendingTaskChangeAuthorization.propTypes = {};

PendingTaskChangeAuthorization.defaultProps = {};

export default PendingTaskChangeAuthorization;
