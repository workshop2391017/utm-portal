/* eslint-disable react/no-unknown-property */
import React, { useState, useEffect } from "react";
import { makeStyles, Button, Grid } from "@material-ui/core";
import Title from "components/BN/Title";
import { useHistory } from "react-router-dom";

// component
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import FailedConfirmation from "components/BN/Popups/FailedConfirmationData";
import DeletePopup from "components/BN/Popups/Delete";
import PapperContainer from "components/BN/PapperContainer";
import ButtonIcon from "components/SC/IconButton";
import Collapse from "components/BN/Collapse";
import { ReactComponent as CheckIcon } from "assets/icons/BN/check.svg";
import { ReactComponent as CheckClose } from "assets/icons/BN/close.svg";

import { checkOnline } from "stores/actions/errorGeneral";
import Colors from "helpers/colors";

// Redux
import {
  handlePandingTaskVaExecute,
  setSelectedCompany,
  handlePandingTaskVaDetail,
  setClearPtExecute,
  APPROVE,
  REJECT,
} from "stores/actions/pendingTask/va";
import AddComment from "components/BN/Popups/AddComment";
import { useDispatch, useSelector } from "react-redux";
import times from "assets/images/BN/illustrationred.png";

import {
  getLocalStorage,
  removeLocalStorage,
  formatPhoneNumber,
} from "utils/helpers";
import { pathnameCONFIG } from "configuration";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";

const useStyles = makeStyles({
  institusiva: {},
  container: {
    padding: "20px 30px",
    display: "flex",
    justifyContent: "center",
  },
  title: {
    fontSize: "24px",
    fontFamily: "FuturaMdBT",
    width: "100%",
    height: "59px",
    backgroundColor: "#0061A7",
    color: "#fff",
    display: "flex",
    alignItems: "center",
    paddingLeft: "50px",
  },
  rowcontainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: "30px",
    background: "pink",
  },
  rowcontainerLoop: {
    // display: "flex",
    // flexDirection: "row",
    // marginBottom: "30px",
  },
  fieldTitle: {
    fontSize: 13,
    fontWeight: 400,
    fontFamily: "FuturaBkBT",
    color: Colors.dark.medium,
    marginBottom: "5px",
  },
  subFieldTitle: {
    fontSize: 14,
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    color: "#374062",
  },
  mainContainer: {
    margin: "63px 115px",
    borderRadius: "20px",
    overflow: "hidden",
  },
  mainContent: {
    backgroundColor: "#fff",
    padding: "50px",
  },
  buttonAction: {
    display: "flex",
    justifyContent: "space-between",
    margin: "40px 115px",
  },
  fieldGroup: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  pt20: {
    paddingTop: "20px  !important",
  },
  fieldGroupLoop: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  sectionTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 20,
    fontWeight: 500,
    color: Colors.dark.hard,
    marginBottom: 30,
  },
  gridTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.01em",
    color: "#7B87AF",
  },
  gridValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  gridValueBold: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
  paddingRight: {
    paddingRight: "12px",
  },
  card: {
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "15px 20px",
    gap: "10px",
    width: "350px",
    height: "140px",
    background: "#FFFFFF",
    border: "1px solid #E6EAF3",
    borderRadius: "10px",
  },
});

const PendingTaskDetailVa = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const classes = useStyles();

  const { isLoadingExecute, selectedCompany, detail, isExecuted } = useSelector(
    (e) => e.pendingtaskVa
  );

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
  });

  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });

  const [openModalError, setOpenModalError] = useState(false);
  const [openModalCom, setopenModalCom] = useState(false);
  const [commRejec, setCommReject] = useState(null);
  const [billingCodeReject, setBillingCodeReject] = useState([]);

  const [fieldValue, setFieldValue] = useState({
    corporateName: "checked",
    address: "checked",
    email: "checked",
    city: "checked",
    province: "checked",
    zipCode: "checked",
    businessFields: "checked",
    ownerName: "checked",
    npwp: "checked",
    position: "checked",
    section: "checked",
    phoneNumber: "checked",
    homePhoneNumber: "checked",
    faxNumber: "checked",
    lengthVa: "checked",
    lengthCustomerId: "checked",
    vaAccountNumber: "checked",
    vaBillingPendingDto: {
      0: "checked",
    },
  });

  const handleCheckField = (fieldName, value, billingCode, index) => {
    setFieldValue({
      ...fieldValue,
      [index ? fieldName.index : fieldName]: value,
    });
    if (value === "checked") {
      if (billingCode !== undefined) {
        const filter = billingCodeReject?.filter(
          (item) => item !== billingCode
        );

        setBillingCodeReject(filter);
      }
    }
    if (value === "unchecked") {
      if (
        billingCode !== undefined &&
        billingCodeReject.every((item) => item !== billingCode)
      ) {
        const dataBillCode = [billingCode, ...billingCodeReject];
        setBillingCodeReject(dataBillCode);
      }
    }
  };

  useEffect(() => {
    if (isExecuted.action === APPROVE && isExecuted.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Approved",
        submessage: "",
        isOpen: true,
      });
    }

    if (isExecuted.action === REJECT && isExecuted.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Rejected",
        isOpen: true,
      });
    }
  }, [isExecuted]);

  useEffect(() => {
    if (detail?.vaBillingPendingDto?.length > 0) {
      let listObject = {};
      detail?.vaBillingPendingDto?.forEach((item, index) => {
        listObject = {
          ...listObject,
          [`vaBillingPendingDto${index}`]: "checked",
        };
      });
      setFieldValue({
        ...fieldValue,
        ...listObject,
      });
    }
  }, [detail]);

  const getStatusDisableReject = (objectData) => {
    const arrayKey = Object.getOwnPropertyNames(objectData);
    const status = "unchecked";
    let result = true;
    arrayKey.forEach((item) => {
      if (objectData[item] === status) result = false;
    });
    return result;
  };

  const getStatusDisableApprove = (objectData) => {
    const arrayKey = Object.getOwnPropertyNames(objectData);
    const status = "unchecked";
    let result = false;
    arrayKey.forEach((item) => {
      if (objectData[item] === status) result = true;
    });
    return result;
  };

  const handleExecuteRegistrationDetail = async (action) => {
    await dispatch(checkOnline());
    // const activeKeys =
    //   fase === "BEFORE_LOGIN"
    //     ? beforeLoginKeys
    //     : afterLoginKeys[corporateAdmin];
    // const activeValue =
    //   fase === "BEFORE_LOGIN"
    //     ? detailDataBeforeLogin
    //     : detailDataAfterLogin[corporateAdmin];
    // const additionalData = {};
    // activeKeys.forEach((elm) => {
    //   additionalData[elm.key] =
    //     filterExecuteUnchacked(activeValue[elm.key], elm.key, detail) || null;
    // });

    const payload = {
      action,
      modifiedReason: commRejec,
      referenceNumber: selectedCompany.reffNo,
      workFlowMenu: selectedCompany.menu,

      // additionalData,
      // ...rejectKeyList(activeKeys, activeValue),
    };
    // "profilBidangUsahaRejectedList": null,
    // "profilDokumenRejectedList": null,
    // "profilPemilikUsahaRejectedList": null,
    // "profilMakerRejectedList": null,
    // "profilApproverRejectedList": null,
    // "profilReleaserRejectedList": null
    if (action === 6) {
      dispatch(
        handlePandingTaskVaExecute({
          ...payload,
          virtualAccountNotValidData: {
            billingInvalidList: billingCodeReject,
            isAccountValid: fieldValue?.vaAccountNumber === "checked",
            isLengthCustomerIdValid: fieldValue?.lengthCustomerId === "checked",
            isLengthVaValid: fieldValue?.lengthVa === "checked",
          },
        })
      );
    }
    if (action === 5) {
      dispatch(handlePandingTaskVaExecute(payload));
    }
  };

  const handleBack = () => {
    removeLocalStorage("ptva");
    history.push(pathnameCONFIG.PENDING_TASK.ACTIVATION_VA);
  };

  useEffect(() => {
    if (!selectedCompany.reffNo) {
      const companyDataStorage = getLocalStorage("ptva");
      const companyData = companyDataStorage
        ? JSON.parse(companyDataStorage)
        : {};
      dispatch(setSelectedCompany(companyData));
    }
  }, []);

  useEffect(() => {
    if (selectedCompany.reffNo) {
      dispatch(
        handlePandingTaskVaDetail({ referenceNumber: selectedCompany.reffNo })
      );
    }
  }, [dispatch, selectedCompany]);

  const ButtonValidate = ({
    buttonKey,
    style,
    fieldName,
    disabled = false,
    billingCode,
  }) => {
    const handleCheck = (action) => {
      if (fieldValue[fieldName] === "checked" && action === "checked") {
        return true;
      }

      if (fieldValue[fieldName] === "unchecked" && action === "unchecked") {
        return true;
      }

      return false;
    };

    return (
      <div style={{ display: "flex", marginRight: 20, ...style }}>
        <ButtonIcon
          color={
            handleCheck("checked")
              ? disabled
                ? "#BCC8E7"
                : "#75D37F"
              : "#BCC8E7"
          }
          width={24}
          height={24}
          style={{ padding: 0, marginRight: 10 }}
          icon={<CheckIcon />}
          onClick={() => handleCheckField(fieldName, "checked", billingCode)}
          disabled={disabled}
        />
        <ButtonIcon
          color={
            (disabled &&
              fieldName === "vaAccountNumber" &&
              !detail?.virtualAccountNotValidData?.isAccountValid) ||
            (disabled &&
              fieldName === "lengthVa" &&
              !detail?.virtualAccountNotValidData?.isLengthVaValid) ||
            (disabled &&
              fieldName === "lengthCustomerId" &&
              !detail?.virtualAccountNotValidData?.isLengthCustomerIdValid) ||
            (disabled &&
              detail?.virtualAccountNotValidData?.billingInvalidList.includes(
                billingCode
              ))
              ? "#D14848"
              : handleCheck("unchecked")
              ? disabled
                ? "#BCC8E7"
                : "#D14848"
              : "#BCC8E7"
          }
          width={24}
          height={24}
          style={{ padding: 0 }}
          icon={<CheckClose />}
          disabled={disabled}
          onClick={() => handleCheckField(fieldName, "unchecked", billingCode)}
        />
      </div>
    );
  };

  const mappingStatus = (status) => {
    if (status?.toLowerCase()?.includes("waiting")) return false;
    return true;
  };

  const FieldVirtualAccount = ({ name, val, title, size = "15px" }) => (
    <div
      style={{
        flex: 1,
        fontSize: size,
        fontFamily: "FuturaBQ",
        maxWidth: "297px",
      }}
    >
      {name ? <div className={classes.fieldTitle}>{`${name} : `}</div> : null}
      {title ? (
        <div
          style={{
            fontFamily: "FuturaHvBT",
            fontSize: 15,
            color: Colors.dark.hard,
          }}
        >
          {title}
        </div>
      ) : null}
      <div size="15px" width="350px" className={classes.subFieldTitle}>
        {val}
      </div>
    </div>
  );

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemParameterSettings = ({
    title,
    value,
    valueBold,
    fieldName,
    parentSize = 6,
    billingCode,
  }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title ?? <br />}
        </Grid>
        <Grid item>
          <Grid container direction="column">
            {valueBold && (
              <Grid item className={classes.gridValueBold}>
                {valueBold}
              </Grid>
            )}
            <Grid item xs={parentSize !== 12 ? undefined : 6}>
              <Grid
                container
                alignItems="center"
                className={parentSize !== 12 ? undefined : classes.paddingRight}
              >
                <Grid item xs={6} className={classes.gridValue}>
                  {value}
                </Grid>
                <Grid item xs={6}>
                  <Grid container justify="flex-end">
                    <ButtonValidate
                      disabled={
                        !selectedCompany?.isAllowTransaction ||
                        mappingStatus(selectedCompany?.status)
                      }
                      billingCode={billingCode}
                      fieldName={fieldName}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );

  function renderPhoneOp(op, owner) {
    let phone = null;
    if (op) {
      phone = op;
    } else {
      phone = owner;
    }
    if (phone) {
      return formatPhoneNumber(phone, 18);
    }
    return "-";
  }

  return (
    <div>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Back"
          />
        }
        onClick={handleBack}
      />
      <Title label="Virtual Account Registration" />

      <div className={classes.container}>
        <PapperContainer
          title="Virtual Account Registration Details"
          style={{ marginBottom: 40 }}
          isLoading={isLoadingExecute}
          // withButton
          // onCancel={() =>
          //   setConfirmationModal({
          //     message: "Anda Yakin Penolakan Data?",
          //     submessage: "Anda tidak dapat membatalkan tindakan ini",
          //     isOpen: true,
          //     confirmType: "batal",
          //   })
          // }
          // onContinue={() =>
          //   setConfirmationModal({
          //     message: "Anda Yakin Menyetujui Data?",
          //     submessage: "Anda tidak dapat membatalkan tindakan ini",
          //     isOpen: true,
          //     confirmType: "setujui",
          //   })
          // }
          // prevTitle="Batal"
        >
          {/* <div
            style={{
              width: "500px",
              overflow: "auto",
            }}
          >
            <span>{JSON.stringify(fieldValue)}</span>
          </div> */}
          <Collapse defaultOpen label="Company Information">
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Company Name :"
                value={
                  detail?.corporateName && detail?.corporateName !== ""
                    ? detail?.corporateName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Email :"
                value={
                  detail?.email && detail?.email !== "" ? detail?.email : "-"
                }
                size={6}
              />
              <GridItem
                title="Current Resident Address :"
                value={
                  detail?.address && detail?.address !== ""
                    ? detail?.address
                    : "-"
                }
                size={12}
              />
              <GridItem
                title="Regency/City :"
                value={detail?.city && detail?.city !== "" ? detail?.city : "-"}
                size={6}
              />
              <GridItem
                title="Province :"
                value={
                  detail?.province && detail?.province !== ""
                    ? detail?.province
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Postal Code :"
                value={
                  detail?.zipCode && detail?.zipCode !== ""
                    ? detail?.zipCode
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Business Fields :"
                value={
                  detail?.businessFields && detail?.businessFields !== ""
                    ? detail?.businessFields
                    : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse defaultOpen label="Owner Information">
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Owner Name :"
                value={
                  detail?.ownerName && detail?.ownerName !== ""
                    ? detail?.ownerName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="NPWP :"
                value={detail?.npwp && detail?.npwp !== "" ? detail?.npwp : "-"}
                size={6}
              />
              <GridItem
                title="Position :"
                value={
                  detail?.position && detail?.position !== ""
                    ? detail?.position
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Department :"
                value={
                  detail?.section && detail?.section !== ""
                    ? detail?.section
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Phone Number (Operational) :"
                value={renderPhoneOp(
                  detail?.homePhoneNumber,
                  detail?.phoneNumber
                )}
                size={6}
              />
              <GridItem
                title="Mobile Phone Number :"
                value={
                  detail?.phoneNumber && detail?.phoneNumber !== ""
                    ? formatPhoneNumber(detail?.phoneNumber, 18)
                    : "-"
                }
                size={6}
              />
              {/* <GridItem
                title="Fax Number :"
                value={
                  detail?.faxNumber && detail?.faxNumber !== ""
                    ? detail?.faxNumber
                    : "-"
                }
                size={6}
              /> */}
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse defaultOpen label="Parameter Settings">
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItemParameterSettings
                title="Selected Account :"
                value={
                  detail?.vaAccountNumber?.accountNumber &&
                  detail?.vaAccountNumber?.accountNumber !== ""
                    ? detail?.vaAccountNumber?.accountNumber
                    : "-"
                }
                valueBold={
                  detail?.vaAccountNumber?.accountAlias &&
                  detail?.vaAccountNumber?.accountAlias !== ""
                    ? detail?.vaAccountNumber?.accountAlias
                    : "-"
                }
                parentSize={12}
                fieldName="vaAccountNumber"
              />
              <GridItemParameterSettings
                title="VA Number Length :"
                value={
                  detail?.lengthVa && detail?.lengthVa !== ""
                    ? `${detail?.lengthVa} Characters`
                    : "-"
                }
                fieldName="lengthVa"
              />
              <GridItemParameterSettings
                title="Customer ID Length :"
                value={
                  detail?.lengthCustomerId && detail?.lengthCustomerId !== ""
                    ? `${detail?.lengthCustomerId} Characters`
                    : "-"
                }
                fieldName="lengthCustomerId"
              />
              {detail?.vaBillingPendingDto?.map((el, i) => (
                <GridItemParameterSettings
                  key={i}
                  title={i === 0 ? "Payment Code :" : undefined}
                  value={
                    el?.billingType && el?.billingType !== ""
                      ? el?.billingType
                      : "-"
                  }
                  valueBold={`${el?.paymentType ?? ""}${
                    el?.billingCode ?? ""
                  } - ${el?.billingName ?? ""}`}
                  billingCode={el?.billingCode}
                  fieldName={`vaBillingPendingDto${i}`}
                />
              ))}
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse defaultOpen label="Branches">
            <div style={{ marginBottom: "30px" }} />

            <div className={classes.card}>
              <h6 className={classes.gridValueBold}>
                {detail?.branchVapendingDto?.name &&
                detail?.branchVapendingDto?.name !== ""
                  ? detail?.branchVapendingDto?.name
                  : "-"}
              </h6>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span className={classes.gridTitle}>
                  {detail?.branchVapendingDto?.phone &&
                  detail?.branchVapendingDto?.phone !== ""
                    ? `Telephone : ${formatPhoneNumber(
                        detail?.branchVapendingDto?.phone,
                        18
                      )}`
                    : "Telephone : -"}
                </span>
                <span className={classes.gridTitle}>
                  {detail?.branchVapendingDto?.fax &&
                  detail?.branchVapendingDto?.fax !== ""
                    ? `Fax : ${formatPhoneNumber(
                        detail?.branchVapendingDto?.fax,
                        18
                      )}`
                    : "Fax : -"}
                </span>
              </div>
              <span className={classes.gridTitle}>
                {detail?.branchVapendingDto?.address &&
                detail?.branchVapendingDto?.address !== ""
                  ? detail?.branchVapendingDto?.address
                  : "-"}
              </span>
            </div>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>
        </PapperContainer>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          height: 84,
          backgroundColor: "white",
          padding: "0 40px",
        }}
      >
        <ButtonOutlined
          label="Reject"
          width="83px"
          style={{ marginRight: 11 }}
          disabled={getStatusDisableReject(fieldValue)}
          onClick={() => setopenModalCom(true)}
        />
        <GeneralButton
          label="Approve"
          width="118px"
          disabled={
            getStatusDisableApprove(fieldValue) ||
            mappingStatus(selectedCompany?.status) ||
            !selectedCompany?.isAllowTransaction
          }
          onClick={() =>
            setConfirmationModal({
              message: "Are You Sure To Approve the Activity?",
              submessage: "You can't undo this action",
              isOpen: true,
              confirmType: APPROVE,
            })
          }
        />
      </div>

      <AddComment
        title="Rejection Comment"
        isOpen={openModalCom}
        handleClose={() => setopenModalCom(false)}
        onContinue={(comment, message) => {
          setCommReject(message);
          setConfirmationModal({
            message: "Are You Sure To Reject The Activity?",
            submessage: "You can't undo this action",
            isOpen: true,
            confirmType: REJECT,
          });
        }}
      />

      <FailedConfirmation
        isOpen={openModalError}
        handleClose={() => {
          setOpenModalError(false);
          dispatch(setClearPtExecute());
        }}
        title="Fail"
        message=""
        submessage=""
      />
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        img={isExecuted.action === REJECT ? times : null}
        handleClose={() => {
          setSuccessConfirmModal({
            message: "",
            submessage: "",
            isOpen: false,
          });
          dispatch(setClearPtExecute());
          history.push(pathnameCONFIG.PENDING_TASK.ACTIVATION_VA);
        }}
        title="Success"
        submessage={!isExecuted?.isToReleaser ? "Please wait for approval" : ""}
        message={successConfirmModal.message}
      />
      <DeletePopup
        isOpen={confirmationModal.isOpen}
        handleClose={() => {
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          });
        }}
        onContinue={() => {
          if (confirmationModal.confirmType === APPROVE) {
            handleExecuteRegistrationDetail(APPROVE);
          } else if (confirmationModal.confirmType === REJECT) {
            handleExecuteRegistrationDetail(REJECT);
          }
          setopenModalCom(false);
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          });
        }}
        title="Confirmation"
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
      />
    </div>
  );
};

PendingTaskDetailVa.propTypes = {};

PendingTaskDetailVa.defaultProps = {};

export default PendingTaskDetailVa;
