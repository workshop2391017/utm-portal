import React, { useEffect, useState } from "react";
import { makeStyles, Button as MUIButton } from "@material-ui/core";
import { useHistory } from "react-router-dom";

// component
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";

// helpers
import { setLocalStorage, searchDateStart, searchDateEnd } from "utils/helpers";

import {
  handlePendingtaskVaList,
  setSelectedCompany,
} from "stores/actions/pendingTask/va";
import { useDispatch, useSelector } from "react-redux";
import usePrevious from "utils/helpers/usePrevious";
import useDebounce from "utils/helpers/useDebounce";
import { pathnameCONFIG } from "configuration";

const dataHeader = ({ history, setConfirmationModal, dispatch }) => [
  {
    title: "Date & Time",

    key: "dateTime",
    render: (rowData) => <span>{rowData?.dateTime}</span>,
  },
  {
    title: "Company ID",
    key: "companyId",
  },
  {
    title: "Company Name",
    key: "company",
    render: (rowData) => <span>{rowData.corporateName}</span>,
  },
  {
    title: "Activity ",
    key: "activity",
    render: (rowData) => <span>{rowData.activityName}</span>,
  },
  {
    title: "Status",
    key: "status",
    render: (rowData) => {
      let color = null;

      if (rowData?.status?.toLowerCase()?.includes("approved")) {
        color = "green";
      }
      if (rowData?.status?.toLowerCase()?.includes("rejected")) {
        color = "red";
      }
      if (rowData?.status?.toLowerCase()?.includes("waiting")) {
        color = "blue";
      }
      return <Badge label={rowData?.status} type={color} />;
    },
  },
  {
    title: "",
    key: "message",
    width: 100,
    render: (rowData) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <GeneralButton
          style={{
            padding: 0,
            fontSize: 9,
            width: 76,
            height: 23,
          }}
          onClick={() => {
            dispatch(setSelectedCompany(rowData));
            setLocalStorage("ptva", JSON.stringify(rowData));
            history.push(pathnameCONFIG.PENDING_TASK.ACTIVATION_VA_DETAIL);
          }}
          label="View Details"
        />
      </div>
    ),
  },
];

const PendingTaskActivationVa = () => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState("");
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);
  const prevPage = usePrevious(page);
  const { isLoading, data } = useSelector((e) => e.pendingtaskVa);
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
  });

  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
      gap: "20px",
      display: "flex",
      flexDirection: "column",
    },
  });

  const history = useHistory();
  const classes = useStyles();

  const dataSearchDeb = useDebounce(dataSearch, 1300);

  const loadData = () => {
    const statusOpt = [null, "WAITING", "REJECTED", "APPROVED"];
    const payload = {
      fromDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      toDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      page: page - 1,
      size: 10,
      workFlowStatus: statusOpt[dataFilter?.tabs?.tabValue3],
      branchCode: null,
      additionalSearch: dataSearchDeb || null,
    };
    dispatch(handlePendingtaskVaList(payload));
  };

  // useEffect(() => {
  //   loadData();
  // }, [page]);

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      loadData();
    } else {
      setPage(1);
    }
  }, [dataSearchDeb, dataFilter, page]);

  return (
    <div className={classes.handlingofficer}>
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <Title label="Pending Task - Virtual Account Registration">
        <Search
          placeholder="Company ID, Company Name"
          placement="left"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />

        <TableICBB
          headerContent={dataHeader({
            history,
            setConfirmationModal,
            dispatch,
          })}
          dataContent={data?.listRegistVaTask ?? []}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
        />
      </div>

      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        handleClose={() =>
          setSuccessConfirmModal({ message: "", submessage: "", isOpen: false })
        }
        title="Berhasil"
        message={successConfirmModal.message}
      />
      <DeletePopup
        isOpen={confirmationModal.isOpen}
        handleClose={() =>
          setConfirmationModal({ isOpen: false, message: "", submessage: "" })
        }
        onContinue={() => {
          if (confirmationModal.confirmType === "setujui") {
            setSuccessConfirmModal({
              message: "Data Berhasil Disetujui",
              submessage: "Silakan menunggu persetujuan",
              isOpen: true,
            });
          } else if (confirmationModal.confirmType === "batal") {
            setSuccessConfirmModal({
              message: "Data Berhasil Ditolak",
              isOpen: true,
            });
          }

          setConfirmationModal({ isOpen: false, message: "", submessage: "" });
        }}
        title="Konfirmasi"
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
      />
    </div>
  );
};

PendingTaskActivationVa.propTypes = {};

PendingTaskActivationVa.defaultProps = {};

export default PendingTaskActivationVa;
