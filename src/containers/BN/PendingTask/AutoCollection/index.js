import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { makeStyles, Typography } from "@material-ui/core";
import moment from "moment";

import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";

import usePrevious from "utils/helpers/usePrevious";
import useDebounce from "utils/helpers/useDebounce";
import { setLocalStorage, searchDateStart, searchDateEnd } from "utils/helpers";

import {
  handlePendingtaskAutoCollectionList,
  setSelectedCompany,
} from "stores/actions/pendingTask/autocollection";

import { pathnameCONFIG } from "configuration";

const options = [
  {
    id: 1,
    type: "datePicker",
    placeholder: "Start Date",
  },
  {
    id: 2,
    type: "datePicker",
    placeholder: "End Date",
    disabled: true,
  },
  {
    id: 3,
    type: "tabs",
    options: ["All", "Waiting", "Rejected", "Approved"],
  },
];

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    padding: "20px 30px",
    gap: "20px",
  },
});

const viewDetailsButtonStyle = {
  padding: 0,
  fontSize: 9,
  width: 76,
  height: 23,
};

const dataHeader = ({ history, setConfirmationModal, dispatch }) => [
  {
    title: "Date & Time",
    render: (rowData) => (
      <Typography variant="subtitle1">
        {moment(rowData.date)?.format("DD-MM-YYYY | HH:mm:ss")}
      </Typography>
    ),
  },
  {
    title: "Company ID",
    render: (rowData) => (
      <Typography variant="subtitle1">
        {rowData.additionalData?.corporateId || "-"}
      </Typography>
    ),
  },
  {
    title: "Company",
    render: (rowData) => (
      <Typography variant="subtitle1">
        {rowData.additionalData?.corporateName || "-"}
      </Typography>
    ),
  },
  {
    title: "Status",
    render: (rowData) => {
      let color = null;
      if (rowData?.status?.toLowerCase()?.includes("approved")) {
        color = "green";
      } else if (rowData?.status?.toLowerCase()?.includes("rejected")) {
        color = "red";
      } else if (rowData?.status?.toLowerCase()?.includes("waiting")) {
        color = "blue";
      }
      return <Badge label={rowData?.status} type={color} outline />;
    },
  },
  {
    title: "",
    width: 100,
    render: (rowData) => (
      <GeneralButton
        style={viewDetailsButtonStyle}
        label="View Details"
        onClick={() => {
          dispatch(setSelectedCompany(rowData));
          setLocalStorage("ptac", JSON.stringify(rowData));
          history.push(pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION_DETAIL);
        }}
      />
    ),
  },
];

const PendingTaskAutoCollection = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const [openModal, setOpenModal] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
  });
  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });

  const [page, setPage] = useState(1);
  const prevPage = usePrevious(page);

  const [dataSearch, setDataSearch] = useState("");
  const [dataFilter, setDataFilter] = useState(null);

  const { isLoading, data } = useSelector((e) => e.pendingTaskAutoCollection);

  const dataSearchDeb = useDebounce(dataSearch, 1000);

  const loadData = () => {
    const statusOpt = [null, "WAITING", "REJECTED", "APPROVED"];
    const payload = {
      menuName: "AUTO_COLLECTION_REGISTER_PORTAL",
      fromDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      toDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      page: page - 1,
      size: 10,
      workFlowStatus: statusOpt[dataFilter?.tabs?.tabValue3],
      searchValue: dataSearchDeb || null,
      id: null,
      isMatrixApproval: false,
    };

    dispatch(handlePendingtaskAutoCollectionList(payload));
  };

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      loadData();
    } else {
      setPage(1);
    }
  }, [dataSearchDeb, dataFilter, page]);

  return (
    <React.Fragment>
      {/* -----Popups----- */}
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        handleClose={() =>
          setSuccessConfirmModal({ message: "", submessage: "", isOpen: false })
        }
        title="Berhasil"
        message={successConfirmModal.message}
      />
      <DeletePopup
        isOpen={confirmationModal.isOpen}
        handleClose={() =>
          setConfirmationModal({ isOpen: false, message: "", submessage: "" })
        }
        onContinue={() => {
          if (confirmationModal.confirmType === "setujui") {
            setSuccessConfirmModal({
              message: "Data Berhasil Disetujui",
              submessage: "Silakan menunggu persetujuan",
              isOpen: true,
            });
          } else if (confirmationModal.confirmType === "batal") {
            setSuccessConfirmModal({
              message: "Data Berhasil Ditolak",
              isOpen: true,
            });
          }

          setConfirmationModal({ isOpen: false, message: "", submessage: "" });
        }}
        title="Konfirmasi"
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
      />

      {/* -----Header----- */}
      <Title label="Pending Task - Auto Collection">
        <Search
          placeholder="Company ID, Company Name"
          placement="bottom"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>

      {/* -----Content----- */}
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={options}
        />
        <TableICBB
          headerContent={dataHeader({
            history,
            setConfirmationModal,
            dispatch,
          })}
          dataContent={data?.workflowApprovalDtoList ?? []}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
        />
      </div>
    </React.Fragment>
  );
};

export default PendingTaskAutoCollection;
