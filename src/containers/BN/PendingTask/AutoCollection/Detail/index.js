/* eslint-disable react/no-unknown-property */
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  makeStyles,
  Button,
  Typography,
  Grid,
  Card,
  CardHeader,
  CardContent,
} from "@material-ui/core";

import { pathnameCONFIG } from "configuration";

import {
  formatPhoneNumber,
  getLocalStorage,
  removeLocalStorage,
} from "utils/helpers";
import Colors from "helpers/colors";

import {
  handlePandingTaskAutoCollectionExecute,
  setSelectedCompany,
  handlePandingTaskAutoCollectionDetail,
  setClearPtExecute,
  APPROVE,
  REJECT,
} from "stores/actions/pendingTask/autocollection";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import times from "assets/images/BN/illustrationred.png";

import Collapse from "components/BN/Collapse";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import FailedConfirmation from "components/BN/Popups/FailedConfirmationData";
import AddComment from "components/BN/Popups/AddComment";
import ConfirmationPopup from "components/BN/Popups/Delete";
import Footer from "components/BN/Footer";
import { getDataDownloadFile } from "stores/actions/companyCharge";

const useStyles = makeStyles((theme) => ({
  page: {
    position: "relative",
    height: "100%",
    backgroundColor: Colors.gray.ultrasoft,
  },
  backButton: {
    ...theme.typography.backButton,
    margin: "8px 0 0 23px",
  },
  header: {
    marginLeft: 23,
    marginBottom: 60,
  },
  mainCard: {
    width: "80%",
    margin: "0 auto",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    borderRadius: 20,
  },
  collapseLabel: {
    ...theme.typography.h1,
    color: Colors.dark.hard,
    fontSize: 20,
  },
  value: {
    ...theme.typography.h1,
    color: Colors.dark.hard,
    fontSize: "15px",
    lineHeight: "24px",
  },
  card: {
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    margin: "30px 0",
    padding: "15px 20px",
    maxWidth: "350px",
    height: "140px",
    borderRadius: "10px",
    border: "1px solid #E6EAF3",
    background: "white",
  },
  textBold: {
    color: Colors.dark.hard,
    fontFamily: "FuturaHvBT",
    letterSpacing: "0.01em",
    fontSize: "15px",
    fontWeight: 400,
  },
  textMedium: {
    ...theme.typography.h1,
    color: Colors.dark.medium,
    fontSize: 14,
    marginTop: 5,
  },
  additionOfOtherAccounts: {
    listStyle: "none",
    marginTop: 30,
    paddingLeft: 15,
    gap: 5,
    maxWidth: 350,
  },
  downloadLink: {
    color: Colors.primary.hard,
    backgroundColor: "transparent",
    cursor: "pointer",
    border: 0,
    padding: 0,
    "&:hover": {
      borderBottom: `1px solid ${Colors.primary.hard}`,
    },
  },
  rowcontainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "30px",
  },
}));

const Detail = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const { isLoadingExecute, selectedCompany, detail, isExecuted } = useSelector(
    ({ pendingTaskAutoCollection }) => pendingTaskAutoCollection
  );
  console.log("ini detail", detail);

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
  });
  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });
  const [openModalError, setOpenModalError] = useState(false);
  const [openModalCom, setOpenModalCom] = useState(false);
  const [commReject, setCommReject] = useState(null);

  useEffect(() => {
    if (isExecuted.action === APPROVE && isExecuted.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Approved",
        submessage: "",
        isOpen: true,
      });
    }

    if (isExecuted.action === REJECT && isExecuted.isSuccess) {
      setSuccessConfirmModal({
        message: "Activity Successfully Rejected",
        isOpen: true,
      });
    }
  }, [isExecuted]);

  const handleExecuteRegistrationDetail = async (action) => {
    // {
    //   "action": 5, // 5 approve 6 reject
    //   "modifiedReason": "test rendi",
    //   "referenceNumber": "899823998895",
    //   "workFlowMenu": "AUTO_COLLECTION_REGISTER_PORTAL"
    // }

    const payload = {
      action,
      modifiedReason: commReject,
      referenceNumber: selectedCompany.reffNo,
      workFlowMenu: "AUTO_COLLECTION_REGISTER_PORTAL",
    };
    if (action === 6) {
      dispatch(
        handlePandingTaskAutoCollectionExecute({
          ...payload,
        })
      );
    }
    if (action === 5) {
      dispatch(handlePandingTaskAutoCollectionExecute(payload));
    }
  };

  const handleBack = () => {
    removeLocalStorage("ptac");
    history.push(pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION);
  };

  useEffect(() => {
    if (!selectedCompany.reffNo) {
      const companyDataStorage = getLocalStorage("ptac");
      const companyData = companyDataStorage
        ? JSON.parse(companyDataStorage)
        : {};
      dispatch(setSelectedCompany(companyData));
    }
  }, []);

  useEffect(() => {
    if (selectedCompany.reffNo) {
      dispatch(
        handlePandingTaskAutoCollectionDetail({
          reffNo: selectedCompany.reffNo,
        })
      );
    }
  }, [dispatch, selectedCompany]);

  const mappingStatus = (status) => {
    if (status?.toLowerCase()?.includes("waiting")) return false;
    return true;
  };

  const downloadHandler = (payload) => () => {
    dispatch(
      getDataDownloadFile(
        payload?.suratKuasa,
        payload?.suratKuasa?.split("/")[3] ?? null
      )
    );
  };

  const FieldValue = ({ name, value }) => (
    <div style={{ flex: 1 }}>
      <Typography
        variant="subtitle1"
        style={{ color: Colors.dark.medium }}
      >{`${name} :`}</Typography>
      <span className={classes.value}>{value}</span>
    </div>
  );

  return (
    <div className={classes.page}>
      <Button
        startIcon={<ArrowLeft />}
        onClick={handleBack}
        className={classes.backButton}
      >
        Back
      </Button>
      <Typography variant="h2" className={classes.header}>
        Auto Collection Registration Details
      </Typography>
      <Card elevation={0} className={classes.mainCard}>
        <CardHeader
          disableTypography
          title={
            <Typography variant="h1">Auto Collection Registration</Typography>
          }
          style={{ backgroundColor: Colors.primary.hard }}
        />
        <CardContent style={{ padding: 50 }}>
          <Collapse
            defaultOpen
            label="Business Field Profile"
            labelClassName={classes.collapseLabel}
          >
            <div style={{ padding: "30px 15px 0px 15px" }}>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Corporate ID"
                  value={
                    detail.metadata?.businessFieldProfile?.corporateId ?? "-"
                  }
                />
                <FieldValue
                  name="Business Name"
                  value={
                    detail.metadata?.businessFieldProfile?.businessName ?? "-"
                  }
                />
              </div>
              <div className={classes.rowcontainer} style={{ maxWidth: "40%" }}>
                <FieldValue
                  name="Business Fields"
                  value={
                    detail.metadata?.businessFieldProfile?.businessField ?? "-"
                  }
                />
              </div>
              <div className={classes.rowcontainer} style={{ maxWidth: "40%" }}>
                <FieldValue
                  name="Complete Address"
                  value={
                    detail.metadata?.businessFieldProfile?.completeAddress ??
                    "-"
                  }
                />
              </div>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Province"
                  value={detail.metadata?.businessFieldProfile?.province ?? "-"}
                />
                <FieldValue
                  name="City/District"
                  value={detail.metadata?.businessFieldProfile?.city ?? "-"}
                />
                <FieldValue
                  name="District"
                  value={detail.metadata?.businessFieldProfile?.district ?? "-"}
                />
                <FieldValue
                  name="Sub-district"
                  value={
                    detail.metadata?.businessFieldProfile?.subDistrict ?? "-"
                  }
                />
              </div>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Postcode"
                  value={detail.metadata?.businessFieldProfile?.postCode ?? "-"}
                />
              </div>

              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Business Phone Number"
                  value={
                    detail.metadata?.businessFieldProfile
                      ?.businessPhoneNumber ?? "-"
                  }
                />
              </div>
            </div>
          </Collapse>
          <Collapse
            defaultOpen
            label="Person in Charge"
            labelClassName={classes.collapseLabel}
          >
            <div style={{ padding: "30px 15px 0px 15px" }}>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="User ID"
                  value={detail.metadata?.personInCharge?.userId ?? "-"}
                />
                <FieldValue
                  name="Full Name"
                  value={detail.metadata?.personInCharge?.fullName ?? "-"}
                />
              </div>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Job Position"
                  value={detail.metadata?.personInCharge?.jobPosition ?? "-"}
                />
                <FieldValue
                  name="Division"
                  value={detail.metadata?.personInCharge?.division ?? "-"}
                />
              </div>

              <div className={classes.rowcontainer}>
                <FieldValue
                  name="KTP / KITAS Number"
                  value={detail.additionalData?.identityNumber ?? "-"}
                />
                <FieldValue
                  name="NPWP Number"
                  value={detail.metadata?.personInCharge?.npwpNumber ?? "-"}
                />
              </div>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Home Phone Number"
                  value={
                    detail.metadata?.personInCharge?.homePhoneNumber || "-"
                  }
                />
                <FieldValue
                  name="Mobile Phone Number"
                  value={
                    detail.metadata?.personInCharge?.mobilePhoneNumber
                      ? formatPhoneNumber(
                          detail.metadata?.personInCharge?.mobilePhoneNumber
                        )
                          ?.replace(/^0/g, "+62-")
                          ?.replace(/[ ]/g, "-")
                      : "-"
                  }
                />
              </div>
              <div className={classes.rowcontainer}>
                <FieldValue
                  name="Email"
                  value={detail.metadata?.personInCharge?.email ?? "-"}
                />
              </div>
            </div>
          </Collapse>
          <Collapse
            defaultOpen
            label="Branches"
            labelClassName={classes.collapseLabel}
          >
            <div className={classes.card}>
              <span className={classes.textBold} style={{ marginBottom: 10 }}>
                {detail?.metadata?.branchDetail?.name ?? "-"}
              </span>
              <Typography
                variant="subtitle1"
                style={{ color: Colors.dark.medium }}
              >
                {detail?.metadata?.branchDetail?.phone
                  ? `Telephone : ${formatPhoneNumber(
                      detail?.metadata?.branchDetail?.phone,
                      18
                    )}`
                  : "Telephone : -"}
              </Typography>
              <Typography
                variant="subtitle1"
                style={{ color: Colors.dark.medium }}
              >
                {detail?.metadata?.branchDetail?.fax
                  ? `Fax : ${formatPhoneNumber(
                      detail?.metadata?.branchDetail?.fax,
                      18
                    )}`
                  : "Fax : -"}
              </Typography>
              <Typography
                variant="subtitle1"
                style={{ color: Colors.dark.medium, marginTop: 10 }}
              >
                {detail?.metadata?.branchDetail?.address ?? "-"}
              </Typography>
            </div>
          </Collapse>
          <Collapse
            defaultOpen
            label="Addition of Other Accounts"
            labelClassName={classes.collapseLabel}
          >
            <Grid container>
              {detail.metadata?.additionalAccounts?.map((account, index) => (
                <Grid item xs={6} key={account.accountNumber}>
                  <ul className={classes.additionOfOtherAccounts}>
                    <li className={classes.textBold}>Rekening {index + 1}</li>
                    <li className={classes.textMedium}>
                      Name : {account.name}
                    </li>
                    <li className={classes.textMedium}>
                      Account Number : {account.accountNumber}
                    </li>
                    <li className={classes.textMedium}>
                      Power of attorney :{" "}
                      <button
                        className={classes.downloadLink}
                        onClick={downloadHandler(account)}
                      >
                        {account.suratKuasa?.split("/")[3]}
                      </button>
                    </li>
                  </ul>
                </Grid>
              ))}
            </Grid>
          </Collapse>
        </CardContent>
      </Card>
      <Footer
        disableElevation
        buttonOutlinedText="Reject"
        buttonContainedText="Approve"
        buttonOutlinedWidth={87}
        buttonContainedWidth={108}
        onCancel={() => setOpenModalCom(true)}
        onContinue={() =>
          setConfirmationModal({
            message: "Are You Sure To Approve the Activity?",
            submessage: "You cannot undo this action",
            isOpen: true,
            confirmType: APPROVE,
          })
        }
        outlinedDisabled={
          detail?.userDataApprovalWorkflowDto?.statusName === "Rejected" ||
          detail?.userDataApprovalWorkflowDto?.statusName === "Approved"
        }
        disabled={
          mappingStatus(selectedCompany?.status) ||
          !selectedCompany?.isAllowTransaction
        }
      />
      <AddComment
        title="Rejection Comment"
        isOpen={openModalCom}
        handleClose={() => setOpenModalCom(false)}
        onContinue={(comment, message) => {
          setCommReject(message);
          setConfirmationModal({
            message: "Are You Sure To Reject The Activity?",
            submessage: "You can't undo this action",
            isOpen: true,
            confirmType: REJECT,
          });
        }}
      />
      <FailedConfirmation
        isOpen={openModalError}
        handleClose={() => {
          setOpenModalError(false);
          dispatch(setClearPtExecute());
        }}
        title="Fail"
        message=""
        submessage=""
      />
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        img={isExecuted.action === REJECT ? times : null}
        handleClose={() => {
          setSuccessConfirmModal({
            message: "",
            submessage: "",
            isOpen: false,
          });
          dispatch(setClearPtExecute());
          history.push(pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION);
        }}
        title="Success"
        submessage={!isExecuted?.isToReleaser ? "Please wait for approval" : ""}
        message={successConfirmModal.message}
      />
      <ConfirmationPopup
        isOpen={confirmationModal.isOpen}
        handleClose={() => {
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          });
        }}
        onContinue={() => {
          if (confirmationModal.confirmType === APPROVE) {
            handleExecuteRegistrationDetail(APPROVE);
          } else if (confirmationModal.confirmType === REJECT) {
            handleExecuteRegistrationDetail(REJECT);
          }
          setOpenModalCom(false);
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          });
        }}
        title="Confirmation"
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
        loading={isLoadingExecute}
      />
    </div>
  );
};

export default Detail;
