/* eslint-disable react/no-unknown-property */
import React from "react";
import ButtonIcon from "components/SC/IconButton";
import { Grid, makeStyles } from "@material-ui/core";
import { handleChecklist } from "stores/actions/pendingTask";

import { ReactComponent as CheckIcon } from "assets/icons/BN/check.svg";
import { ReactComponent as CheckClose } from "assets/icons/BN/close.svg";

const useStyles = makeStyles({
  container: {
    padding: "20px 30px",
    display: "flex",
    justifyContent: "center",
  },
  fieldTitle: {
    fontSize: 13,
    fontWeight: 400,
    fontFamily: "FuturaHvBT",
    color: "#7B87AF",
    marginBottom: "5px",
  },
  subFieldTitle: {
    fontSize: 14,
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    color: "#374062",
  },
});

export const FieldVirtualAccount = ({ name, val, size = "15px" }) => {
  const classes = useStyles();
  return (
    <div style={{ flex: 1, fontSize: size, fontFamily: "FuturaBQ" }}>
      <div className={classes.fieldTitle}>{`${name} : `}</div>
      <div width="350px" className={classes.subFieldTitle}>
        {val}
      </div>
    </div>
  );
};

export const ButtonValidate = ({
  buttonKey,
  checkList,
  style,
  onClick,
  disabled = false,
  section,
}) => {
  const handleCheck = (action) => {
    const checked = (checkList ?? []).find((e) => {
      if (section === "doc") return e.label === buttonKey;
      return e.label_en === buttonKey;
    });

    if (checked?.checked && action === "checked") {
      return true;
    }

    if (!checked?.checked && action === "unchecked") {
      return true;
    }

    return false;
  };

  return (
    <div style={{ display: "flex", marginRight: 20, ...style }}>
      <ButtonIcon
        color={
          handleCheck("checked")
            ? disabled
              ? "#BCC8E7"
              : "#75D37F"
            : "#BCC8E7"
        }
        width={24}
        height={24}
        style={{ padding: 0, marginRight: 10 }}
        icon={<CheckIcon />}
        onClick={() => onClick("checked")}
        disabled={disabled}
      />
      <ButtonIcon
        color={
          handleCheck("unchecked")
            ? disabled
              ? "#BCC8E7"
              : "#D14848"
            : "#BCC8E7"
        }
        width={24}
        height={24}
        style={{ padding: 0 }}
        icon={<CheckClose />}
        disabled={disabled}
        onClick={() => onClick("unchecked")}
      />
    </div>
  );
};

export const MappingData = (props) => {
  const {
    classes,
    data,
    dispatch,
    label,
    keyObject,
    corporateAdmin,
    index,
    setData,
  } = props;
  return (
    <div style={{ marginTop: index > 0 ? 50 : 0 }}>
      <div
        style={{
          fontFamily: "FuturaMdBT",
          color: "#374062",
          fontSize: 20,
          marginBottom: 30,
        }}
      >
        {label}
      </div>
      <Grid container spacing={3}>
        {(data ?? []).map((e, i) => (
          <Grid item xs={6} key={i}>
            <div className={classes.fieldGroup}>
              <FieldVirtualAccount name={e.label} val={e.value} />
              <ButtonValidate
                buttonKey={e.label}
                checkList={data}
                onClick={() =>
                  dispatch(
                    handleChecklist(i, data, setData, keyObject, corporateAdmin)
                  )
                }
              />
            </div>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
