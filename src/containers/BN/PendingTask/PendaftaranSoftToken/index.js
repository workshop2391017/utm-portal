import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";

// redux
import {
  handlePendingTaskBulkExecute,
  handlePendingTaskExecute,
  setBulkReff,
  setHandleClearError,
} from "stores/actions/pendingTask/softToken";

// libs
import useDebounce from "utils/helpers/useDebounce";

// component
import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import { handleGetPendingTaskSoftToken } from "stores/actions/pendingTask";

import { useDispatch, useSelector } from "react-redux";
import Title from "components/BN/Title";
import Toast from "components/BN/Toats";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import usePrevious from "utils/helpers/usePrevious";

// helpers
import { searchDate } from "utils/helpers";
import moment from "moment";
import { el } from "date-fns/locale";

const dataHeader = ({
  setConfirmationModal,
  activeIndex,
  handleExecute,
  isExecute,
  setSelectedIds,
}) => [
  {
    title: "Date & Time",
    key: "date",
    render: (rowData) => moment(rowData.date).format("YYYY-MM-DD HH:mm:ss"),
  },
  {
    title: "User ID",
    key: "username",
  },
  {
    title: "Full Name",
    key: "fullName",
  },
  {
    title: "Role",
    key: "role",
  },
  {
    title: "Company Name",
    key: "corporateName",
  },
  {
    title: "Status",
    key: "statusCode",
    render: (rowData) => {
      let status = "";
      let message = "";

      if (rowData?.status === null) {
        return `-`;
      }
      if (rowData?.status) {
        if (rowData?.status?.toLowerCase()?.includes("waiting")) {
          status = "blue";
          message = "Waiting Approval";
        }
        if (rowData?.status?.toLowerCase()?.includes("approved")) {
          status = "green";
          message = "Approved";
        }
        if (rowData?.status?.toLowerCase()?.includes("rejected")) {
          status = "red";
          message = "Rejected";
        }
      }

      return <Badge label={rowData?.status} type={status} outline />;
    },
  },
  {
    title: "",
    key: "message",
    width: 200,
    render: (rowData, index) => (
      <div style={{ display: "flex" }}>
        <ButtonOutlined
          label="Reject"
          style={{
            fontSize: 9,
            width: 46,
            height: 23,
            padding: "6px 10px",
            borderRadius: 6,
          }}
          disabled={
            isExecute ||
            !rowData?.status?.toLowerCase()?.includes("waiting") ||
            rowData.isAllowTransaction === false
          }
          onClick={() => {
            setSelectedIds([]);
            handleExecute("cancel", rowData, index);
          }}
        />
        <GeneralButton
          label="Approve"
          style={{
            fontSize: 9,
            width: 46,
            height: 23,
            padding: "6px 10px",
            borderRadius: 6,
            marginLeft: 30,
          }}
          disabled={
            isExecute ||
            !rowData?.status?.includes("WAITING") ||
            rowData.isAllowTransaction === false
          }
          onClick={() => {
            setSelectedIds([]);
            handleExecute("approve", rowData, index);
          }}
        />
      </div>
    ),
  },
];

const PendingTaskRegistrationSftToken = () => {
  const dispatch = useDispatch();
  const disabledCondition = false;
  const { data, isLoading, isExecuted, isExecute, error } = useSelector(
    (e) => e.pendingTaskSoftToken
  );
  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState("");
  const [comment, setComment] = useState("");
  const [dataFilter, setDataFilter] = useState(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [page, setPage] = useState(1);
  const [activeIndex, setActiveIndex] = useState(null);
  const prevPage = usePrevious(page);

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    message: "",
    submessage: "",
    isOpen: false,
  });

  const [confirmationModal, setConfirmationModal] = useState({
    message: "",
    submessage: "",
    confirmType: "",
    isOpen: false,
  });

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
      gap: "20px",
      display: "flex",
      flexDirection: "column",
    },
  });

  const classes = useStyles();

  const handleExecute = (action, rowData, index) => {
    const data = { action, rowData, index };

    if (action === "approve") {
      setConfirmationModal({
        message: "Are you sure to approve the activity?",
        submessage: "You can't undo this action",
        isOpen: true,
        data,
      });
    }

    if (action === "cancel") {
      setConfirmationModal({
        message: "Are you sure to reject the activity?",
        submessage: "You can't undo this action",
        isOpen: true,
        data,
      });
    }
  };

  const execute = () => {
    const { action, rowData, index } = confirmationModal.data;

    const paylaod = {
      action: action === "approve" ? 5 : action === "cancel" ? 6 : null,
      referenceNumber: rowData.reffNo,
      workFlowMenu: "SOFT_TOKEN",
    };

    handlePendingTaskExecute(paylaod)(dispatch);
  };
  const executeBulk = () => {
    const { action } = confirmationModal.data;
    const payload = {
      action: action === "approve" ? 5 : action === "cancel" ? 6 : null,
      workFlowMenu: "SOFT_TOKEN",
      modifiedReason: comment,
    };
    dispatch(handlePendingTaskBulkExecute(payload));
  };

  const dataSearchDebounce = useDebounce(dataSearch, 1300);

  const loadData = () => {
    const status = dataFilter?.tabs?.tabValue3;

    const statusOpt = ["", "WAITING", "REJECTED", "APPROVED"];
    const paramsPayload = {
      startDate: dataFilter?.date?.dateValue1
        ? new Date(searchDate(dataFilter?.date?.dateValue1))
        : null,
      endDate: dataFilter?.date?.dateValue2
        ? new Date(searchDate(dataFilter?.date?.dateValue2))
        : null,
      pageNumber: page - 1,
      pageSize: 10,
      searchValue: dataSearchDebounce,
      status: statusOpt[status],
      // userMakerId: "",
    };

    handleGetPendingTaskSoftToken(paramsPayload)(dispatch);
  };

  // useEffect(() => {
  //   loadData();
  // }, [page]);

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      loadData();
    } else {
      setPage(1);
    }
  }, [dataSearchDebounce, dataFilter, page]);

  useEffect(() => {
    if (isExecuted.isSuccess) {
      if (selectedIds > 0 && confirmationModal?.data?.action === "approve") {
        setSuccessConfirmModal({
          message: `[${selectedIds.length}] Activity Successfully Approved`,
          submessage: "Some activity may need approval",
          isOpen: true,
        });

        setConfirmationModal({
          isOpen: false,
          message: "",
          submessage: "",
        });
      } else if (
        selectedIds > 0 &&
        confirmationModal?.data?.action === "cancel"
      ) {
        setSuccessConfirmModal({
          message: `[${selectedIds.length}] Activity Successfully Rejected`,
          submessage: "",
          isOpen: true,
        });

        setConfirmationModal({
          isOpen: false,
          message: "",
          submessage: "",
        });
      } else if (
        confirmationModal?.data?.action === "approve" &&
        isExecuted.isToReleaser === false
      ) {
        setSuccessConfirmModal({
          message: "Activity Successfully Approved",
          submessage: "Please wait for approval",
          isOpen: true,
        });

        setConfirmationModal({
          isOpen: false,
          message: "",
          submessage: "",
        });
      } else if (
        confirmationModal?.data?.action === "approve" &&
        isExecuted.isToReleaser === true
      ) {
        setSuccessConfirmModal({
          message: "Activity Successfully Approved",
          submessage: "",
          isOpen: true,
        });

        setConfirmationModal({
          isOpen: false,
          message: "",
          submessage: "",
        });
      } else if (confirmationModal?.data?.action === "cancel") {
        setSuccessConfirmModal({
          message: "Activity Successfully Rejected",
          isOpen: true,
        });

        setConfirmationModal({
          isOpen: false,
          message: "",
          submessage: "",
        });
      }
    }
  }, [isExecuted]);

  useEffect(() => {
    if (data?.data?.length > 0 && selectedIds?.length > 0) {
      const dataFilter = data?.data?.filter((el) =>
        selectedIds?.find((element) => element === el?.reffNo)
      );

      const dataSelected = dataFilter?.map((item) => ({
        referenceNumber: item?.reffNo,
      }));

      dispatch(setBulkReff(dataSelected));
    }
  }, [selectedIds, data]);

  return (
    <div className={classes.handlingofficer}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <Title label="Pending Task - Soft Token Registration">
        <Search
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="User ID, Full Name"
          placement="bottom"
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={(e) => {
            setDataFilter(e);
            setDataSearch("");
          }}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />
        <TableICBB
          headerContent={dataHeader({
            setConfirmationModal,
            handleExecute,
            isExecute,
            activeIndex,
            setSelectedIds,
          })}
          selecable
          handleSelect={setSelectedIds}
          handleSelectAll={setSelectedIds}
          selectedValue={selectedIds}
          dataContent={(data?.data ?? []).map((elm) => ({
            ...elm,
            id: elm?.reffNo,
            disabled:
              elm?.isAllowTransaction === false ? true : disabledCondition,
          }))}
          page={page}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
          isLoading={isLoading}
          setPage={setPage}
          extraComponents={
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                gap: 20,
              }}
            >
              <ButtonOutlined
                style={{ marginLeft: 220 }}
                onClick={() => handleExecute("cancel")}
                label="Reject All"
              />
              <GeneralButton
                onClick={() => handleExecute("approve")}
                style={{ marginRight: 30 }}
                label="Approve All"
                width="135px"
              />
            </div>
          }
        />
      </div>
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        handleClose={() => {
          setSuccessConfirmModal({
            message: "",
            submessage: "",
            isOpen: false,
          });
          loadData();
          setSelectedIds([]);
        }}
        title="Success"
        message={successConfirmModal.message}
        submessage={successConfirmModal?.submessage}
      />
      <DeletePopup
        isOpen={confirmationModal.isOpen}
        handleClose={() =>
          setConfirmationModal({
            ...confirmationModal,
            isOpen: false,
            message: "",
            submessage: "",
          })
        }
        onContinue={() => {
          if (selectedIds.length > 0) {
            executeBulk();
          } else {
            execute();
          }
        }}
        title="Confirmation"
        loading={isExecute}
        message={confirmationModal.message}
        submessage={confirmationModal.submessage}
      />
    </div>
  );
};

PendingTaskRegistrationSftToken.propTypes = {};

PendingTaskRegistrationSftToken.defaultProps = {};

export default PendingTaskRegistrationSftToken;
