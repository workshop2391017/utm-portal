import { Modal, Typography, makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import TableICBB from "components/BN/TableIcBB";
import { useSelector } from "react-redux";

const useStyle = makeStyles({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    width: "480px",
    Height: "442px",
    borderRadius: "10px",
    backgroundColor: "white",
    padding: "20px 20px 20px 20px",
  },
  content: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    textAlign: "center",
    height: "100%",
    "& p": {
      fontSize: "28px",
      fontFamily: "FuturaHvBT",
      color: "#374062",
    },
  },
});

const DetailPopup = ({ isOpen, data, onClose }) => {
  const [detail, setDetail] = useState([]);
  const { isLoading, dataPeriodicalChargeList } = useSelector(
    (state) => state.periodicalChargeList
  );
  const dataHeader = (rowData) => [
    {
      title: "Account",
      key: "accountNumber",
    },
    {
      title: "Currency",
      key: "currency",
    },
    {
      title: "Amount",
      key: "amount",
    },
  ];

  const classes = useStyle();
  let toDataArray = [];
  useEffect(() => {
    if (data) {
      toDataArray = [data];
      setDetail(toDataArray);
    }
  }, [data]);

  return (
    <Modal open={isOpen} className={classes.modal} onClose={onClose}>
      <div className={classes.container}>
        <div className={classes.content}>
          <p>Account</p>
          <TableICBB
            isLoading={isLoading}
            headerContent={dataHeader()}
            dataContent={detail ?? []}
            page={1}
            totalData={10}
            scroll
            scrollHeight="400px"
          />
        </div>
      </div>
    </Modal>
  );
};

DetailPopup.propType = {
  isOpen: PropTypes.bool,
};

export default DetailPopup;
