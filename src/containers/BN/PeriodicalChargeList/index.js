import React, { useEffect, useState } from "react";
import { Box, makeStyles, Button as MUIButton } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

// component
import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";

import Title from "components/BN/Title";
import Colors from "helpers/colors";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as IconDownload } from "assets/icons/BN/download-1.svg";
import Filter from "components/BN/Filter/GeneralFilter";
import DropdownSelect from "components/BN/Select/AntdSelect";
import { getPeriodicalChargeList } from "stores/actions/periodicalChargeList";
import usePrevious from "utils/helpers/usePrevious";
import useDebounce from "utils/helpers/useDebounce";
import { ArrowRight } from "@material-ui/icons";
import { searchDateStart, searchDateEnd } from "utils/helpers";
import moment from "moment";
import DetailPopup from "./DetailPopup";

const dataHeader = ({ setOpenModal }) => [
  {
    title: "Perusahaan",
    key: "perusahaan",
    render: (rowData) => (
      <div>
        <div
          style={{
            fontFamily: "FuturaHvBT",
            fontSize: 13,
            color: Colors.dark.hard,
          }}
        >
          {rowData.companyName}
        </div>
        <div>{rowData.companyId}</div>
      </div>
    ),
  },
  {
    title: "Rekening",
    key: "rekening",
    render: (rowData) => (
      <div>
        <div
          style={{
            fontFamily: "FuturaHvBT",
            fontSize: 13,
            color: Colors.dark.hard,
          }}
        >
          {rowData.accountNumber}
        </div>
        <Box
          style={{
            color: "#0061A7",
            fontWeight: 700,
            fontSize: 11,
            fontFamily: "FuturaMdBT",
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
          }}
          onClick={() => setOpenModal({ isOpen: true, data: rowData })}
        >
          Lihat Detail <ArrowRight width={18} />
        </Box>
      </div>
    ),
  },
  {
    title: "Segmentasi",
    key: "segmentation",
  },
  {
    title: "Type",
    key: "type",
  },
  {
    title: "Periode",
    key: "period",
  },
  {
    title: "Instruction Date",
    render: (rowData) => (
      <div>
        {moment(rowData.instructionDateNative)?.format("DD-MM-YYYY | HH:mm:ss")}
      </div>
    ),
  },
  {
    title: "Status",
    key: "status",
    render: (rowData) => (
      <Badge
        label={rowData?.status || ""}
        type={
          rowData?.status === "Menunggu"
            ? "blue"
            : rowData?.status === "Success"
            ? "green"
            : "redoutlined"
        }
        outline
      />
    ),
  },
];

const PeriodicalChargeInstruction = () => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState({ isOpen: false, data: null });
  const [dataSearch, setDataSearch] = useState(null);
  const [dataContentTable, setDataContentTable] = useState([]);
  const { isLoading, dataPeriodicalChargeList } = useSelector(
    (state) => state.periodicalChargeList
  );
  const [page, setPage] = useState(1);
  const prevPage = usePrevious(page);
  const dataSearchDeb = useDebounce(dataSearch, 1000);
  const [dataFilter, setDataFilter] = useState(null);

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
    },
    filter: {
      backgroundColor: "white",
      height: 72,
      padding: "16px 20px",
      display: "flex",
      justifyContent: "space-between",
      marginBottom: 11,
    },
    // dropdown: {},
  });

  const history = useHistory();
  const classes = useStyles();

  const getDataPeriodicalChargeList = () => {
    const payload = {
      startDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      endDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      companyName: dataSearch,
      pageNumber: page - 1,
      pageSize: 10,
      type: dataFilter?.dropdown?.dropdown3,
    };
    dispatch(getPeriodicalChargeList(payload));
    console.log(dataFilter);
  };

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      getDataPeriodicalChargeList();
    } else {
      setPage(1);
    }
  }, [dataSearchDeb, dataFilter, page]);

  const closeModal = () => {
    setOpenModal({ isOpen: false });
  };

  useEffect(() => {
    if (Object.values(dataPeriodicalChargeList).length !== 0) {
      setDataContentTable(
        dataPeriodicalChargeList?.trxHistoryPeriodicalChargeDtoList
      );
    }
  }, [dataPeriodicalChargeList, dataContentTable]);

  return (
    <div className={classes.handlingofficer}>
      <DetailPopup
        isOpen={openModal.isOpen}
        data={openModal.data}
        onClose={closeModal}
      />

      <Title label="Periodical Charge List">
        <Search
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Company Name"
        />
        <GeneralButton
          style={{ marginLeft: "20px" }}
          width="138px"
          label="Download"
          iconPosition="leftIcon"
          buttonIcon={<IconDownload />}
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "dropdown",
              placeholder: "Select Type",
              options: ["All", "Monthly", "Annual"],
            },
          ]}
        />
        <TableICBB
          headerContent={dataHeader({ setOpenModal })}
          isLoading={isLoading}
          dataContent={dataContentTable ?? []}
          page={page}
          setPage={setPage}
          totalData={10}
          totalElement={10}
          columnStyle={{ fontFamily: "FuturaBkBT", height: 80 }}
        />
      </div>
    </div>
  );
};

PeriodicalChargeInstruction.propTypes = {};

PeriodicalChargeInstruction.defaultProps = {};

export default PeriodicalChargeInstruction;
