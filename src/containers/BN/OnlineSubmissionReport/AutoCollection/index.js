import React, { useEffect, useState } from "react";
import { makeStyles, Button as MUIButton } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import {
  handleAutoCollectionReportData,
  setSelectedCompany,
} from "stores/actions/onlineSubmissonReport/autoCollectionReport";
import { searchDateStart, searchDateEnd, setLocalStorage } from "utils/helpers";

// component
import Badge from "components/BN/Badge";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";
import { pathnameCONFIG } from "configuration";
import Button from "components/SC/Button";

// assets
import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";
import Filter from "components/BN/Filter/GeneralFilter";
import GeneralButton from "components/BN/Button/GeneralButton";
import { useDispatch, useSelector } from "react-redux";
import useDebounce from "utils/helpers/useDebounce";
import moment from "moment";

const dataHeader = ({ history, dispatch }) => [
  {
    title: "Date & Time",
    align: "left",

    render: (rowData) => (
      <span>{moment(rowData.createdDate).format("DD-MM-YYYY | HH:mm:ss")}</span>
    ),
  },
  {
    title: "Company ID",
    key: "corporateId",
  },
  {
    title: "Company Name",
    key: "corporateName",
  },
  {
    title: "",
    key: "message",
    width: 100,
    render: (rowData) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{
            padding: "6px 10px",
            fontSize: 9,
            fontWeight: "bold",
            fontFamily: "FuturaMdBT",
          }}
          onClick={() => {
            dispatch(setSelectedCompany(rowData));
            setLocalStorage("autoCollectionReport", JSON.stringify(rowData));
            history.push(
              pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION_DETAIL
            );
          }}
        />
      </div>
    ),
  },
];

const ReportAutoCollection = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);
  const dataSearchDeb = useDebounce(dataSearch, 1300);
  const { isLoading, data } = useSelector((e) => e.autoCollectionReport);

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
    },
  });

  const getData = () => {
    const payload = {
      startDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      endDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      pageNumber: page,
      pageSize: 10,
      additionalSearch: dataSearchDeb || null,
    };
    dispatch(handleAutoCollectionReportData(payload));
  };

  useEffect(() => {
    getData();
  }, [dataSearchDeb, dataFilter]);

  const classes = useStyles();
  return (
    <div className={classes.handlingofficer}>
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <Title label="Auto Collection Report">
        <Search
          placeholder="Company ID, Company Name"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <div style={{ marginBottom: 20 }}>
          <Filter
            dataFilter={dataFilter}
            setDataFilter={(e) => setDataFilter(e)}
            align="left"
            options={[
              {
                id: 1,
                type: "datePicker",
                placeholder: "Start Date",
              },
              {
                id: 2,
                type: "datePicker",
                placeholder: "End Date",
              },
            ]}
          />
        </div>
        <TableICBB
          headerContent={dataHeader({ history, dispatch })}
          dataContent={data?.data ?? []}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
          noDataMessage="No Auto Collection Data"
        />
      </div>
    </div>
  );
};

export default ReportAutoCollection;
