import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getLocalStorage } from "utils/helpers";
import {
  setSelectedCompany,
  handleAutoCollectionReportDetail,
} from "stores/actions/onlineSubmissonReport/autoCollectionReport";

// MUI
import { Box, makeStyles, Button, Grid } from "@material-ui/core";

// antd
import { Typography } from "antd";

// components
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import Collapse from "components/BN/Collapse";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";

const AutoCollectionDetail = () => {
  const history = useHistory();
  const [dataDetail, setDataDetail] = useState({});

  const [open, setOpen] = useState(false);

  const handleBack = () => history.goBack();
  const dispatch = useDispatch();

  const { selectedCompany, detail } = useSelector(
    (e) => e.autoCollectionReport
  );

  useEffect(() => {
    if (!selectedCompany.autoCollectionId) {
      const companyDataStorage = getLocalStorage("autoCollectionReport");
      console.log(companyDataStorage);
      const companyData = companyDataStorage
        ? JSON.parse(companyDataStorage)
        : {};
      dispatch(setSelectedCompany(companyData));
      console.log(companyData);
    }
  }, []);

  useEffect(() => {
    const payload = {
      autoCollectionId: selectedCompany.autoCollectionId,
    };
    dispatch(handleAutoCollectionReportDetail(payload));
    console.log(detail);
  }, []);

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    rowcontainer: {
      display: "flex",
      flexDirection: "row",
      //   justifyContent: "space-between",
      marginBottom: "30px",
      //   backgroundColor: "red",
    },
    fieldTitle: {
      fontSize: 13,
      fontWeight: 400,
      fontFamily: "FuturaHvBT",
      color: "#7B87AF",
      marginBottom: "5px",
    },
    subFieldTitle: {
      fontSize: 14,
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      color: "#374062",
    },
    subFieldTitle2: {
      fontSize: 12,
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      color: "#374062",
    },
    mainContainer: {
      margin: "63px 115px",
      //   margin: "50px",
      borderRadius: "20px",
      overflow: "hidden",
    },
    mainContent: {
      backgroundColor: "#fff",
      padding: "50px",
      "& .title": {
        fontSize: "20px",
        color: "#374062",
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        marginBottom: "30px",
      },
    },
    titleCode: {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#7B87AF",
      fontWeight: 400,
    },
    gridTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    gridValue: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    gridValueBold: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    card: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      padding: "15px 20px",
      gap: "10px",
      width: "350px",
      height: "140px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
    cardva: {
      width: 335,
      height: 91,
      borderRadius: 10,
      border: "1px solid",
      borderColor: Colors.gray.medium,
      padding: "15px",
    },
  });

  const classes = useStyles();

  const badgeRender = (badge) => {
    if (badge === "NONBILLING") {
      return (
        <Badge
          type="green"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }

    if (badge === "BILLING") {
      return (
        <Badge
          type="blue"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }
  };

  const FieldVirtualAccount = ({ name, val, badge, code, accountName }) => (
    <div style={{ flex: 1 }}>
      <div className={classes.fieldTitle}>{name ? <div>{name}</div> : ""}</div>
      <div
        // weight={500}
        size="15px"
        width="350px"
        className={classes.subFieldTitle}
      >
        {accountName}
      </div>
      <div size="15px" width="350px" className={classes.subFieldTitle2}>
        {val}
      </div>
      <div>{badgeRender(badge)}</div>
    </div>
  );

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemAddition = ({ title, value, valueBold, parentSize = 6 }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title ?? <br />}
        </Grid>
        {valueBold && (
          <Grid item className={classes.gridValueBold}>
            {valueBold}
          </Grid>
        )}
        <Grid item className={classes.gridValue}>
          {value !== "BILLING" && value !== "NONBILLING"
            ? value
            : badgeRender(value)}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={handleBack}
      />
      <Title label="Auto Collection Report Details" />
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">Applicant Data</span>
        </div>
        <div className={classes.mainContent}>
          <Collapse label="Business Field Profile" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Corporate ID:"
                value={
                  detail?.businessFieldProfile?.corporateId &&
                  detail?.businessFieldProfile?.corporateId !== ""
                    ? detail?.businessFieldProfile?.corporateId
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Company Name :"
                value={
                  detail?.businessFieldProfile?.businessName &&
                  detail?.businessFieldProfile?.businessName !== ""
                    ? detail?.businessFieldProfile?.businessName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Business Field :"
                value={
                  detail?.businessFieldProfile?.businessField &&
                  detail?.businessFieldProfile?.businessField !== ""
                    ? detail?.businessFieldProfile?.businessField
                    : "-"
                }
                size={12}
              />
              <GridItem
                title="Complete Address :"
                value={
                  detail?.businessFieldProfile?.completeAddress &&
                  detail?.businessFieldProfile?.completeAddress !== ""
                    ? detail?.businessFieldProfile?.completeAddress
                    : "-"
                }
                size={12}
              />
              <GridItem
                title="Province :"
                value={
                  detail?.businessFieldProfile?.province &&
                  detail?.businessFieldProfile?.province !== ""
                    ? detail?.businessFieldProfile?.province
                    : "-"
                }
                size={3}
              />
              <GridItem
                title="City/District :"
                value={
                  detail?.businessFieldProfile?.city &&
                  detail?.businessFieldProfile?.city !== ""
                    ? detail?.businessFieldProfile?.city
                    : "-"
                }
                size={3}
              />
              <GridItem
                title="District :"
                value={
                  detail?.businessFieldProfile?.district &&
                  detail?.businessFieldProfile?.district !== ""
                    ? detail?.businessFieldProfile?.district
                    : "-"
                }
                size={3}
              />
              <GridItem
                title="Sub District :"
                value={
                  detail?.businessFieldProfile?.subDistrict &&
                  detail?.businessFieldProfile?.subDistrict !== ""
                    ? detail?.businessFieldProfile?.subDistrict
                    : "-"
                }
                size={3}
              />
              <GridItem
                title="Postal Code :"
                value={
                  detail?.businessFieldProfile?.postCode &&
                  detail?.businessFieldProfile?.postCode !== ""
                    ? detail?.businessFieldProfile?.postCode
                    : "-"
                }
                size={12}
              />
              <GridItem
                title="Business Phone Number:"
                value={
                  detail?.businessFieldProfile?.businessPhoneNumber &&
                  detail?.businessFieldProfile?.businessPhoneNumber !== ""
                    ? detail?.businessFieldProfile?.businessPhoneNumber
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Amount of Soft Token Requirement:"
                value={
                  detail?.businessFieldProfile?.softToken &&
                  detail?.businessFieldProfile?.softToken !== ""
                    ? detail?.businessFieldProfile?.softToken
                    : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Person in Charge" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="User ID :"
                value={
                  detail?.personInCharge?.userID &&
                  detail?.personInCharge?.userID !== ""
                    ? detail?.personInCharge?.userID
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Full Name :"
                value={
                  detail?.personInCharge?.fullName &&
                  detail?.personInCharge?.fullName !== ""
                    ? detail?.personInCharge?.fullName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Job Position :"
                value={
                  detail?.personInCharge?.jobPosistion &&
                  detail?.personInCharge?.jobPosistion !== ""
                    ? detail?.personInCharge?.jobPosistion
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Division :"
                value={
                  detail?.personInCharge?.division &&
                  detail?.personInCharge?.division !== ""
                    ? detail?.personInCharge?.division
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="KTP/KITAS Number :"
                value={
                  detail?.personInCharge?.ktpKitasNumber &&
                  detail?.personInCharge?.ktpKitasNumber !== ""
                    ? detail?.personInCharge?.ktpKitasNumber
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="NPWP Number:"
                value={
                  detail?.personInCharge?.npwpNumber &&
                  detail?.personInCharge?.npwpNumber !== ""
                    ? detail?.personInCharge?.npwpNumber
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Home Phone Number :"
                value={
                  detail?.personInCharge?.homePhoneNumber &&
                  detail?.personInCharge?.homePhoneNumber !== ""
                    ? detail?.personInCharge?.homePhoneNumber
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Mobile Phone Number :"
                value={
                  detail?.personInCharge?.mobilePhoneNumber &&
                  detail?.personInCharge?.mobilePhoneNumber !== ""
                    ? detail?.personInCharge?.mobilePhoneNumber
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Email: "
                value={
                  detail?.personInCharge?.email &&
                  detail?.personInCharge?.email !== ""
                    ? detail?.personInCharge?.email
                    : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Branches" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <div className={classes.card}>
              <h6 className={classes.gridValueBold}>
                {detail?.branchDetail?.name && detail?.branchDetail?.name !== ""
                  ? detail?.branchDetail?.name
                  : "-"}
              </h6>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span className={classes.gridTitle}>
                  {detail?.branchDetail?.phone &&
                  detail?.branchDetail?.phone !== ""
                    ? `Telepon : ${detail?.branchDetail?.phone}`
                    : "Phone : -"}
                </span>
                <span className={classes.gridTitle}>
                  {detail?.branchDetail?.fax && detail?.branchDetail?.fax !== ""
                    ? `Fax : ${detail?.branchDetail?.fax}`
                    : "Fax : -"}
                </span>
              </div>
              <span className={classes.gridTitle}>
                {detail?.branchDetail?.address &&
                detail?.branchDetail?.address !== ""
                  ? detail?.branchDetail?.address
                  : "-"}
              </span>
            </div>
            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Addition of Other Accounts" defaultOpen>
            <div style={{ padding: "30px 15px 0px 15px" }}>
              {detail?.additionalAccounts?.map((val) => (
                <React.Fragment>
                  <div className={classes.rowcontainer}>
                    <FieldVirtualAccount
                      name="Nomor Rekening"
                      accountName={val.name}
                      val={val.accountNumber}
                    />
                    <div style={{ flex: 1 }}>
                      <div className={classes.fieldTitle}>Surat Kuasa :</div>
                      <input
                        placeholder={val.suratKuasa.split(/[\s/]+/).pop()}
                        style={{
                          width: "320px",
                          height: "40px",
                          borderRadius: "10px",
                          border: "1px solid #0061A7",
                          padding: "11px 10px",
                          fontSize: "15px",
                          position: "center",
                          textColor: "#0061A7",
                          color: "#0061A7",
                          fontFamily: "FuturaBkBT",
                        }}
                        type="text"
                      />
                    </div>
                  </div>
                </React.Fragment>
              ))}
            </div>
          </Collapse>

          <Box sx={{ margin: "10px auto" }} />
        </div>
      </div>
    </div>
  );
};

AutoCollectionDetail.propTypes = {};

AutoCollectionDetail.defaultProps = {};

export default AutoCollectionDetail;
