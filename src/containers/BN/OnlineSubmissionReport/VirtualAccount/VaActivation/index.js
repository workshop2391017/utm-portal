import { makeStyles } from "@material-ui/styles";
import React, { useState, useEffect } from "react";
import { Formik, Form, useFormikContext } from "formik";
import * as Yup from "yup";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

// assets
import VaSvg from "assets/icons/BN/va.svg";

import Information from "./information";
import SyaratDanKetentuan from "./syaratDanKetentuan";
import FormActivation from "./formActivation";
import FormPreview from "./preview";

const useStyles = makeStyles({
  institusiva: {},
  container: {
    padding: "20px 30px",
    display: "flex",
    justifyContent: "center",
  },
});

const initialValue = {
  activate: false,
  syarat_dan_ketentuan: false,
  form: {
    company_name: null,
    npwp: null,
    address: null,
    owner: null,
    province: null,
    position: null,
    district: null,
    bagian: null,
    zip_Code: null,
    home_phone: null,
    email: null,
    phone: null,
    business_field: null,

    no_rek: null,
    va_length: null,
    id_cutomer_length: null,

    payment_name: null,
  },
};

const validationSchema = {
  validation: Yup.object().shape({
    form: Yup.object().shape({
      company_name: Yup.string().required("Nama Perusahaan harus diisi"),
      npwp: Yup.string().required("NPWP harus diisi"),
      address: Yup.string().required("Alamat harus diisi"),
      owner: Yup.string().required("Pemilik harus diisi"),
      province: Yup.string().required("Provinsi harus diisi"),
      position: Yup.string().required("Jabatan harus diisi"),
      district: Yup.string().required("kabupaten/Kota harus diisi"),
      bagian: Yup.string().required("Bagian harus diisi"),
      zip_code: Yup.string().required("Kode Pos harus diisi"),
      email: Yup.string().required("Email harus diisi"),
      phone: Yup.string().required("Nomor Hp harus diisi"),
      business_field: Yup.string().required("Bidang Usaha harus diisi"),

      no_rek: Yup.string().required("Nomor Rekening harus diisi"),
      va_length: Yup.string().required(
        "Panjang Nomor Virtual Account harus diisi"
      ),
      id_cutomer_length: Yup.string().required(
        "Panjang ID Pelanggan harus diisi"
      ),

      payment_name: Yup.string().required("Nama Pembayaran harus diisi"),
    }),
  }),
};

const RenderPages = () => {
  const [pagePreview, setPagePreview] = useState("information");

  const { validateForm, setTouched } = useFormikContext();

  const handleCheckErrorValidate = async () => validateForm();

  const handleFormNext = async () => {
    const validationKeys = await handleCheckErrorValidate();
    setTouched(validationKeys, true);

    if (validationKeys && Object.keys(validationKeys).length === 0) {
      setPagePreview("preview");
    }
  };

  const renderPerPage = () => {
    switch (pagePreview) {
      case "information":
        return (
          <Information
            onContinue={() => setPagePreview("syarat_dan_ketentuan")}
          />
        );
      case "syarat_dan_ketentuan":
        return (
          <SyaratDanKetentuan
            onBack={() => setPagePreview("information")}
            onContinue={() => setPagePreview("form_activation")}
          />
        );
      case "form_activation":
        return (
          <FormActivation
            onBack={() => setPagePreview("syarat_dan_ketentuan")}
            onContinue={handleFormNext}
          />
        );
      case "preview":
        return <FormPreview onEdit={() => setPagePreview("form_activation")} />;

      default:
        return <React.Fragment />;
    }
  };

  useEffect(() => {
    switch (pagePreview) {
      case "information":
        break;
      case "syarat_dan_ketentuan":
        break;
      case "form_activation":
        break;
      case "preview":
        break;

      default:
        return <React.Fragment />;
    }
  }, [pagePreview]);

  return <div>{renderPerPage()}</div>;
};

const VaActivation = () => {
  const classes = useStyles();
  const [popupSuccess, setPopupSuccess] = useState(false);

  return (
    <div className={classes?.container}>
      <Formik
        initialValues={initialValue}
        validationSchema={validationSchema?.validation}
        onSubmit={(dataVASubmit) => {
          console.warn("dataVASubmit =>", dataVASubmit);
          setPopupSuccess(true);
        }}
        validateOnBlur={false}
      >
        <Form>
          <RenderPages />
        </Form>
      </Formik>
      <SuccessConfirmation
        isOpen={popupSuccess}
        handleClose={() => setPopupSuccess(false)}
        img={VaSvg}
        title="Virtual Account"
        message="Anda Berhasil melakukan 
        pengaktifan Virtual Account"
        submessage="Nasabah Silahkan mengunduh dokumen, kemudian hubungi Customer Service di cabang terdekat."
        height="546px"
      />
    </div>
  );
};

export default VaActivation;
