import { makeStyles, Paper } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";

// assets
import ScrollCustom from "components/BN/ScrollCustom";
import GeneralButton from "components/BN/Button/GeneralButton";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import { useFormikContext } from "formik";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";

const useStyles = makeStyles({
  conatiner: {
    padding: "20px 30px",
    display: "flex",
    justifyContent: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 24,
    color: "#374062",
    textAlign: "center",
  },
  paper: {
    width: 645,
    borderRadius: 20,
    height: 720,
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    padding: 30,
    backgroundColor: "white",
    zIndex: 10,
    position: "relative",
    marginTop: 46,
    marginBottom: 24,
  },
  infoContainer: {
    border: "1px solid #E6EAF3",
    width: 590,
    height: 490,
    borderRadius: 10,
    marginTop: 20,
    position: "relative",
  },
  shadowWhite: {
    width: "100%",
    position: "absolute",
    bottom: 0,
    height: 30,
    backgroundColor: "white",
    zIndex: 11,
    opacity: 0.6,
    marginBottom: -5,
  },
  checkboxGroup: {
    display: "flex",
    alignItems: "center",
    marginTop: 12,
  },
  button: {
    marginTop: 41,
    marginBottom: 28,
    display: "flex",
    justifyContent: "space-between",
  },
});

const SyaratDanKetentuan = ({ onContinue, onBack }) => {
  const history = useHistory();
  const classes = useStyles();
  const { values, handleChange } = useFormikContext();

  return (
    <div>
      <Paper className={classes.paper}>
        <div className={classes.title}>Informasi Tahapan Registrasi</div>
        <div className={classes.infoContainer}>
          <ScrollCustom
            height={490}
            style={{ paddingTop: "28px", paddingLeft: 20, paddingRight: 20 }}
          >
            SYARAT DAN KETENTUAN PADA SAAT AKTIVASI INTERNET BANKING Bank 1.
            Istilah 1.1. INTERNET BANKING Bank adalah saluran distribusi Bank
            untuk mengakses rekening yang dimiliki Nasabah melalui jaringan
            internet dengan menggunakan perangkat lunak browser pada komputer.
            1.2. Bank adalah PT BANK TABUNGAN NEGARA (PERSERO) TBK yang meliputi
            Kantor Pusat dan kantor cabang serta kantor lainnya yang merupakan
            bagian yang tidak terpisahkan dari PT BANK TABUNGAN NEGARA (PERSERO)
            TBK. 1.3. Nasabah adalah perorangan pemilik rekening simpanan dalam
            mata uang rupiah berupa Tabungan Bank & Giro Rupiah Perorangan. 1.4.
            Nasabah Pengguna adalah Nasabah yang telah terdaftar sebagai
            pengguna layanan INTERNET BANKING Bank. 1.5. Daftar Rekening adalah
            nomor rekening Rupiah di semua cabang yang dimiliki oleh Nasabah di
            Bank yang telah didaftarkan dan karenanya dapat diakses oleh Nasabah
            Pengguna. 1.6. User ID adalah identitas yang dimiliki oleh setiap
            Nasabah Pengguna yang harus dicantumkan/diinput dalam setiap
            penggunaan layanan INTERNET BANKING Bank. 1.7. PIN Mobile Token
            (Personal Identification Number) INTERNET BANKING dan MOBILE BANKING
            Bank adalah nomor identifikasi pribadi yang bersifat rahasia dan
            hanya diketahui oleh Nasabah Pengguna serta harus
            dicantumkan/diinput oleh Nasabah Pengguna pada saat menggunakan
            layanan INTERNET BANKING dan MOBILE BANKING Bank. Bersama-sama
            dengan User ID, Password dan PIN Mobile Token digunakan untuk
            membuktikan bahwa nasabah bersangkutan adalah nasabah yang berhak
            atas layanan INTERNET BANKING Bank. 2. Syarat Pendaftaran INTERNET
            BANKING Bank 2.1. Nasabah mengisi dan menandatangani Formulir
            Aplikasi Internet Banking dapat diperoleh di cabang atau Melakukan
            pendaftaran melalui ATM Bank. 2.2. Menunjukkan bukti asli identitas
            diri yang sah (KTP, SIM, Paspor, KIMS) dan bukti kepemilikan
            pemegang rekening. 2.3. Nasabah harus memiliki alamat E-mail. 2.4.
            Nasabah Pengguna telah memperoleh Kode Registrasi (Registration
            Code) dari Bank untuk keperluan aktivasi online di situs INTERNET
            BANKING Bank. 2.5. Telah membaca dan memahami Syarat dan Ketentuan
            INTERNET BANKING Bank. 3. Ketentuan Penggunaan INTERNET BANKING Bank
            3.1. Nasabah Pengguna dapat menggunakan layanan INTERNET BANKING
            Bank untuk mendapatkan informasi dan atau melakukan transaksi
            Perbankan yang telah ditentukan oleh Bank. 3.2. Pada saat pertama
            kali menggunakan layanan INTERNET BANKING Bank, Nasabah Pengguna
            diharuskan melakukan aktivasi di situs INTERNET BANKING Bank dengan
            cara memasukkan Kode Registrasi (Registration Code) yang diperoleh
            dari Bank untuk diubah menjadi User ID, Password dan PIN MOBILE
            TOKEN INTERNET BANKING Bank. 3.3. Untuk setiap pelaksanaan
            transaksi: a. Nasabah Pengguna wajib memastikan ketepatan dan
            kelengkapan perintah transaksi (termasuk memastikan bahwa semua data
            yang diperlukan untuk transaksi telah diisi secara lengkap dan
            benar). Bank tidak bertanggung jawab terhadap segala dampak apapun
            yang mungkin timbul yang diakibatkan kelalaian, ketidaklengkapan,
            ketidakjelasan, atau ketidaktepatan perintah/data dari Nasabah
            Pengguna. b. Nasabah Pengguna memiliki kesempatan untuk memeriksa
            kembali dan atau membatalkan data yang telah diisi pada saat
            konfirmasi yang dilakukan secara otomatis oleh sistem sebelum adanya
            tanda persetujuan sebagaimana diatur di bawah ini. c. Apabila telah
            diyakini kebenaran dan kelengkapan data yang diisi, sebagai tanda
            persetujuan pelaksanaan transaksi maka Nasabah Pengguna wajib
            memasukkan PIN pada kolom yang telah disediakan pada halaman layanan
            transaksi INTERNET BANKING Bank. 3.4. Segala transaksi yang telah
            diperintahkan kepada Bank dan disetujui oleh Nasabah Pengguna tidak
            dapat dibatalkan. 3.5. Setiap perintah yang telah disetujui dari
            Nasabah Pengguna yang tersimpan pada pusat data Bank merupakan data
            yang benar yang diterima sebagai bukti perintah dari Nasabah
            Pengguna kepada Bank untuk melaksanakan transaksi yang dimaksud.
            3.6. Bank menerima dan menjalankan setiap perintah dari Nasabah
            Pengguna sebagai perintah yang sah berdasarkan penggunaan User ID,
            Password dan PIN Mobile Token Bank, dan untuk itu Bank tidak
            mempunyai kewajiban untuk meneliti atau menyelidiki keaslian maupun
            keabsahan atau kewenangan pengguna User ID, Password dan PIN Mobile
            Token Bank atau menilai maupun membuktikan ketepatan maupun
            kelengkapan perintah dimaksud, dan oleh karena itu perintah tersebut
            sah mengikat Nasabah Pengguna dengan sebagaimana mestinya, kecuali
            Nasabah Pengguna dapat membuktikan sebaliknya. 3.7. Bank berhak
            untuk tidak melaksanakan perintah dari Nasabah Pengguna, apabila: a.
            Saldo rekening Nasabah Pengguna di Bank tidak cukup. b. Bank
            mengetahui atau mempunyai alasan untuk menduga bahwa penipuan atau
            aksi kejahatan telah atau akan dilakukan. 3.8. Sebagai bukti bahwa
            transaksi yang diperintahkan Nasabah Pengguna telah berhasil
            dilakukan oleh Bank, Nasabah Pengguna akan mendapatkan bukti
            transaksi berupa nomor transaksi pada halaman transaksi layanan
            INTERNET BANKING Bank dan bukti tersebut akan tersimpan di dalam
            menu aktivitas transaksi selama 3 (tiga) bulan sejak tanggal
            transaksi. 3.9. Nasabah Pengguna menyetujui dan mengakui bahwa: a.
            Dengan dilaksanakannya transaksi melalui INTERNET BANKING dan MOBILE
            BANKING Bank, semua perintah dan komunikasi dari Nasabah Pengguna
            yang diterima Bank akan diperlakukan sebagai alat bukti yang sah
            meskipun tidak dibuat dokumen tertulis dan atau dikeluarkan dokumen
            yang tidak ditandatangani. b. Bukti atas perintah dari Nasabah
            Pengguna kepada Bank dan segala bentuk komunikasi antara Bank dan
            Nasabah Pengguna yang dikirim secara elektronik yang tersimpan pada
            pusat data Bank dan atau tersimpan dalam bentuk penyimpanan
            informasi dan data lainnya di Bank, baik yang berupa dokumen
            tertulis, catatan, tape/cartridge, print out komputer dan atau
            salinan, merupakan alat bukti yang sah yang tidak akan dibantah
            keabsahan, kebenaran atau keasliannya. 3.10. Atas pertimbangannya
            sendiri, Bank berhak untuk mengubah limit transaksi. 3.11. Semua
            komunikasi melalui e-mail yang aman dan memenuhi standar serta
            dianggap sah, otentik, asli dan benar serta memberikan efek yang
            sama sebagaimana bila hal tersebut dilakukan secara tertulis dan
            atau melalui dokumen tertulis. 3.12. Bank tidak diwajibkan untuk
            melaksanakan setiap perintah baik yang ditandatangani maupun tidak
            atau menjawab pertanyaan apapun yang diterima melalui e-mail yang
            tidak aman. Nasabah disarankan untuk tidak mengirim informasi
            rahasia melalui e-mail yang tidak aman. 3.13. Bank berhak
            menghentikan layanan INTERNET BANKING dan MOBILE BANKING Bank untuk
            sementara waktu maupun untuk jangka waktu tertentu yang ditentukan
            oleh Bank untuk keperluan pembaharuan, pemeliharaan atau untuk
            tujuan lain dengan alasan apapun yang dianggap baik oleh Bank, dan
            untuk itu Bank tidak berkewajiban mempertanggungjawabkannya kepada
            siapapun. 4. User ID, Password dan PIN Mobile Token Bank 4.1. User
            ID, Password dan PIN Mobile Token merupakan kode yang bersifat
            rahasia dan kewenangan penggunaannya ada pada Nasabah Pengguna. User
            ID bersifat tetap dan tidak dapat diubah kembali. 4.2. Nasabah wajib
            mengamankan User ID, Password dan PIN Mobile Token dengan cara: a.
            Tidak memberitahukan User ID, Password dan PIN Mobile Token kepada
            orang lain. b. Tidak mencatatkan User ID, Password dan PIN Mobile
            Token Bank pada kertas atau menyimpannya secara tertulis pada
            handphone atau sarana penyimpanan lainnya yang memungkinkan
            diketahui orang lain. c. Memusnahkan secepatnya Email yang bersifat
            rahasia dari Internet Banking & Mobile Banking Bank setelah
            menerimanya. d. Berhati-hati menggunakan User ID, Password dan PIN
            Mobile Token Bank agar tidak terlihat orang lain. e. Mengganti
            Password Internet Banking & Mobile Banking Bank secara berkala. 4.3.
            Dalam hal Nasabah Pengguna mengetahui atau menduga User ID, Password
            dan PIN Mobile Token telah diketahui oleh orang lain yang tidak
            berwenang, maka Nasabah Pengguna wajib segera melakukan pengamanan
            dengan melakukan perubahan User ID dan Password. Apabila karena
            suatu sebab Nasabah Pengguna tidak dapat melakukan perubahan User ID
            dan Password Internet Banking dan Mobile Banking Bank maka Nasabah
            Pengguna wajib memberitahukan kepada Bank. Sebelum diterimanya
            pemberitahuan secara tertulis oleh pejabat Bank yang berwenang, maka
            segala perintah, transaksi dan komunikasi berdasarkan penggunaan
            User ID, Password dan PIN Mobile Token oleh pihak yang tidak
            berwenang sepenuhnya menjadi tanggung jawab Nasabah Pengguna. 4.4.
            Penggunaan User ID, Password & PIN Mobile Token mempunyai kekuatan
            hukum yang sama dengan perintah tertulis yang ditandatangani oleh
            Nasabah Pengguna, sehingga karenanya Nasabah Pengguna dengan ini
            menyatakan bahwa penggunaan User ID, Password dan PIN Mobile Token
            dalam setiap perintah atas transaksi INTERNET BANKING Bank juga
            merupakan pemberian kuasa dari Nasabah Pengguna kepada Bank untuk
            melaksanakan transaksi termasuk namun tidak terbatas untuk melakukan
            pendebetan rekening Nasabah baik dalam rangka pelaksanaan transaksi
            yang diperintahkan maupun untuk pembayaran biaya transaksi yang
            telah dan atau akan ditetapkan kemudian oleh Bank. 4.5. Segala
            penyalahgunaan User ID, Password dan PIN Mobile Token INTERNET
            BANKING dan MOBILE BANKING Bank merupakan tanggung jawab Nasabah
            Pengguna. Nasabah Pengguna dengan ini membebaskan Bank dari segala
            tuntutan yang mungkin timbul, baik dari pihak lain maupun Nasabah
            Pengguna sendiri sebagai akibat penyalahgunaan User ID, Password dan
            PIN Mobile Token INTERNET BANKING dan MOBILE BANKING Bank. 5.
            Penghentian Akses Layanan INTERNET BANKING Bank 5.1. Akses layanan
            INTERNET BANKING Bank akan dihentikan oleh Bank apabila: a. Nasabah
            Pengguna meminta kepada Bank untuk menghentikan akses layanan
            INTERNET BANKING Bank secara permanen yang antara lain disebabkan
            oleh: (i) User ID, Password dan PIN Mobile Token Internet Banking
            dan Mobile Banking Nasabah Pengguna lupa. (ii) Nasabah Pengguna
            menutup semua rekening yang dapat diakses melalui layanan INTERNET
            BANKING Bank. b. Salah memasukkan User ID, Password dan PIN Mobile
            Token INTERNET BANKING dan MOBILE BANKING Bank sebanyak 3 (tiga)
            kali berturut-turut. c. Diterimanya laporan tertulis dari Nasabah
            Pengguna mengenai dugaan atau diketahuinya User ID, Password dan PIN
            Mobile Token oleh pihak lain yang tidak berwenang. d. Bank
            melaksanakan suatu keharusan sesuai ketentuan perundang-undangan
            yang berlaku. 5.2. Untuk melakukan aktivasi kembali karena
            penghentian akses layanan tersebut di atas, Nasabah Pengguna harus
            menghubungi Contact Center Bank atau melakukan pendaftaran ulang di
            cabang pengelola rekening. 6. Force Majeure Nasabah Pengguna akan
            membebaskan Bank dari segala tuntutan apapun, dalam hal Bank tidak
            dapat melaksanakan perintah dari Nasabah Pengguna baik sebagian
            maupun seluruhnya karena kejadian-kejadian atau sebab-sebab di luar
            kekuasaan atau kemampuan Bank termasuk namun tidak terbatas pada
            segala gangguan virus komputer atau sistem Trojan Horses atau
            komponen membahayakan yang dapat menggangu layanan INTERNET BANKING
            Bank, web browser atau komputer sistem Bank, Nasabah, atau Internet
            Service Provider, karena bencana alam, perang, huru-hara, keadaan
            peralatan, sistem atau transmisi yang tidak berfungsi, gangguan
            listrik, gangguan telekomunikasi, kebijakan pemerintah, serta
            kejadian-kejadian atau sebab-sebab lain di luar kekuasaan atau
            kemampuan Bank. 7. Lain-lain 7.1. Bukti perintah Nasabah Pengguna
            melalui layanan INTERNET BANKING dan MOBILE BANKING Bank adalah
            mutasi yang tercatat dalam Rekening Koran atau Buku Tabungan jika
            dicetak. 7.2. Nasabah Pengguna dapat menghubungi Contact Center Bank
            atas setiap permasalahan yang berkaitan dengan transaksi dan
            perubahan akses layanan INTERNET BANKING dan MOBILE BANKING BANK
            Bank. 7.3. Bank dapat mengubah syarat dan ketentuan ini setiap saat
            dengan pemberitahuan terlebih dahulu kepada Nasabah Pengguna dalam
            bentuk dan melalui sarana apapun. 7.4. Nasabah Pengguna tunduk pada
            ketentuan-ketentuan dan peraturan-peraturan yang berlaku pada Bank
            serta syarat-syarat pembukaan rekening dan syarat rekening gabungan,
            termasuk setiap perubahan yang akan diberitahukan terlebih dahulu
            oleh Bank dalam bentuk dan sarana apapun. 7.5. Kuasa-kuasa baik yang
            tersurat dalam Syarat dan Ketentuan ini merupakan kuasa yang sah
            yang tidak akan berakhir selama Nasabah Pengguna masih memperoleh
            layanan INTERNET BANKING dan MOBILE BANKING Bank atau masih adanya
            kewajiban lain dari Nasabah Pengguna kepada Bank.
          </ScrollCustom>
          <div className={classes.shadowWhite} />
          <div className={classes.checkboxGroup}>
            <CheckboxSingle
              onChange={handleChange}
              name="syarat_dan_ketentuan"
              checked={values?.syarat_dan_ketentuan}
            />
            <div style={{ marginLeft: 10 }}>
              Saya telah membaca, memahami dan menyetujui syarat & ketentuan
            </div>
          </div>
          <div className={classes.button}>
            <ButtonOutlined
              label="Kembali"
              style={{ width: 133 }}
              onClick={onBack}
            />
            <GeneralButton
              disabled={!values?.syarat_dan_ketentuan}
              label="selanjutnya"
              style={{ width: 133 }}
              onClick={onContinue}
            />
          </div>
        </div>
      </Paper>
    </div>
  );
};

SyaratDanKetentuan.propTypes = {
  onContinue: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired,
};

export default SyaratDanKetentuan;
