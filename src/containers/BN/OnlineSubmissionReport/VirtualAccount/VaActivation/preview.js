/* eslint-disable react/no-unknown-property */
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import FormField from "components/BN/Form/InputGroupValidation";
import PapperContainer from "components/BN/PapperContainer";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import AntdTextField from "components/BN/TextField/AntdTextField";
import TextFieldPhone from "components/BN/TextFieldPhone";
import Colors from "helpers/colors";
import React, { useEffect } from "react";

import { ReactComponent as Edit } from "assets/icons/BN/edit-3.svg";
import { ReactComponent as Chevron } from "assets/icons/BN/chevron-right-blue.svg";
import { useFormikContext } from "formik";

import PropTypes from "prop-types";

const dummyDataProfileBidang = [
  {
    id: 1,
    label: "Nama Perusahaan",
    value: "PT Infosys Solusi Terpadu",
  },
  {
    id: 2,
    label: "NPWP",
    value: "58.375.706.7-321.000",
  },
  {
    id: 3,
    label: "Alamat",
    value:
      "Wirosaban Baru, Jl. Pangeran Wirosobo No.54, RT.53/RW.14, Sorosutan, Umbulharjo, Yogyakarta City, Special Region of Yogyakarta 55162",
  },

  {
    id: 4,
    label: "Nama Pemilik",
    value: "Christina Susanti",
  },
  {
    id: 5,
    label: "Provinsi",
    value: "D.I Yogyakarta",
  },
  {
    id: 6,
    label: "Jumlah Kebutuhan Token",
    value: "12",
  },

  {
    id: 6,
    label: "Jabatan",
    value: "Manager",
  },
  {
    id: 6,
    label: "Kabupaten/Kota",
    value: "D.I Yogyakarta",
  },

  {
    id: 6,
    label: "Bagian",
    value: "Manager Operasional",
  },
  {
    id: 6,
    label: "Kode Pos",
    value: "66292",
  },

  {
    id: 6,
    label: "Nomor Telepon Rumah (Operasional)",
    value: "-",
  },
  {
    id: 6,
    label: "Email",
    value: "xyz@gmail.com",
  },

  {
    id: 6,
    label: "xyz@gmail.com",
    value: "+62 - 823 8475 3435",
  },
  {
    id: 6,
    label: "Bidang Usaha",
    value: "Teknologi, Media dan Telekomunikasi",
  },
];

const dummyadminMaker = [
  {
    id: 7,
    label: "Nama Lengkap",
    value: "Primanita",
  },
  {
    id: 9,
    label: "Jabatan",
    value: "Manager",
  },
  {
    id: 9,
    label: "Jenis Kelamin",
    value: "Perempuan",
  },
  {
    id: 8,
    label: "No KTP/Kitas",
    value: "12343434355",
  },
  {
    id: 8,
    label: "Kewarganegaraan",
    value: "WNI",
  },
  {
    id: 8,
    label: "Tempat Lahir",
    value: "Yogyakarta",
  },
  {
    id: 8,
    label: "Tanggal Lahir",
    value: "27 / 12 / 1985",
  },
  {
    id: 8,
    label: "No Telepon Rumah",
    value: "+62 - 823 8475 3435",
  },
  {
    id: 8,
    label: "Email",
    value: "+62 - 823 8475 3435",
  },
  {
    id: 8,
    label: "No Telepon Rumah",
    value: "+62 - 823 8475 3435",
  },
  {
    id: 10,
    label: "Alamat Usaha",
    value:
      "Wirosaban Baru, Jl. Pangeran Wirosobo No.54, RT.53/RW.14, Sorosutan, Umbulharjo, Yogyakarta City, Special Region of Yogyakarta 55162",
  },
];

const useStyles = makeStyles({
  inputWrapper: {
    display: "flex",
    marginBottom: 30,
    justifyContent: "space-between",
    width: "100%",
    "& .inputGroup": {
      flex: 1,
    },
    "& .formTitle": {
      marginBottom: 5,
    },
  },
  fieldTitle: {
    fontSize: 13,
    fontWeight: 400,
    fontFamily: "FuturaHvBT",
    color: "#7B87AF",
    marginBottom: "5px",
  },
  listTitle: {
    fontSize: 20,
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    marginTop: 68,
  },
});

const FieldVirtualAccount = ({ name, val, style, size = "15px" }) => {
  const classes = useStyles();
  return (
    <div style={{ flex: 1, fontSize: size, fontFamily: "FuturaBQ", ...style }}>
      <div className={classes.fieldTitle}>{`${name} : `}</div>
      <div width="350px" className={classes.subFieldTitle}>
        {val}
      </div>
    </div>
  );
};

const FormPreview = ({ onEdit }) => {
  const { handleSubmit, values } = useFormikContext();

  const classes = useStyles();

  return (
    <div>
      <PapperContainer
        withButton
        title="Pengaktifan Virtual Account"
        prevButton={false}
        nextTitle={
          <div style={{ display: "flex", alignItems: "center" }}>
            Simpan <Chevron style={{ marginLeft: 11.5 }} />
          </div>
        }
        onContinue={handleSubmit}
      >
        <div style={{ marginTop: 36 }}>
          <Grid container spacing={3}>
            {dummyDataProfileBidang.map((e, i) => (
              <Grid item xs={6} key={i}>
                <div className={classes.fieldGroup}>
                  <FieldVirtualAccount name={e.label} val={e.value} />
                </div>
              </Grid>
            ))}
          </Grid>
        </div>

        <div>
          <div className={classes.listTitle}>Pengaturan Parameter</div>
          <FieldVirtualAccount
            name="Nomor Rekening"
            val="2537 - 2618 - 0192"
            style={{ marginTop: 36 }}
          />
          <FieldVirtualAccount
            name="Panjang No. Virtual Account"
            val="10 Karakter"
            style={{ marginTop: 36 }}
          />
          <FieldVirtualAccount
            name="Panjang ID Pelanggan"
            val="2 Karakter"
            style={{ marginTop: 36 }}
          />
        </div>
        <div
          style={{ display: "flex", justifyContent: "center", marginTop: 53 }}
        >
          <ButtonOutlined
            label={
              <div style={{ display: "flex", justifyContent: "center" }}>
                <Edit /> Ubah
              </div>
            }
            onClick={onEdit}
          />
        </div>
      </PapperContainer>
    </div>
  );
};

export default FormPreview;

FormPreview.propTypes = {
  onEdit: PropTypes.func,
};

FormPreview.defaultProps = {
  onEdit: () => {},
};
