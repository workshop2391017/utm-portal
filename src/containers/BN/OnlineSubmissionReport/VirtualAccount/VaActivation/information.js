import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import GeneralButton from "components/BN/Button/GeneralButton";
import Colors from "helpers/colors";
import React from "react";
import PropTypes from "prop-types";
import { useFormikContext } from "formik";

const useStyles = makeStyles({
  papper: {
    height: 474,
    width: 645,
    borderRadius: 20,
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    padding: "30px 38px 21px 38px",
  },
  title: {
    fontSize: "24px",
    fontFamily: "FuturaMdBT",
    textAlign: "center",
    marginBottom: 30,
  },
  contentTitle: {
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginBottom: 2,
  },
  contentSubTitle: {
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
  },
  bottomButtonGroup: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: 100,
    "& .button": {
      fontFamily: "FuturaMdBT",
      fontSize: 13,
      width: "183px !important",
    },
  },
});

const Information = ({ onContinue }) => {
  const { setFieldValue } = useFormikContext();
  const classes = useStyles();

  return (
    <div>
      <Paper className={classes.papper}>
        <div className={classes.title}> Virtual Account</div>

        <div>
          <div className={classes.contentTitle}>Keamanan Virtual Account</div>
          <div className={classes.contentSubTitle}>
            Dengan fitur ini Anda dapat melakukan transaksi secara aman dan
            terjamin. Karena data tercatat secara otomatis dalam sistem.
          </div>
          <div className={classes.contentTitle} style={{ marginTop: "15px" }}>
            Kemudahan Bertransaksi
          </div>
          <div className={classes.contentSubTitle}>
            Melalui virtual account Anda dapat membuat proses transaksi menjadi
            lebih mudah dan cepat.
          </div>
          <div className={classes.contentTitle} style={{ marginTop: "15px" }}>
            Pemantauan Transaksi
          </div>
          <div className={classes.contentSubTitle}>
            Anda dapat melihat riwayat transaksi yang sudah dibayar dan yang
            belum dibayar melalui fitur laporan virtual account.
          </div>
        </div>

        <div className={classes.bottomButtonGroup}>
          <GeneralButton
            label="Aktifkan Virtual Account"
            className="button"
            onClick={() => {
              setFieldValue("activate", true);
              onContinue();
            }}
          />
        </div>
      </Paper>
    </div>
  );
};

export default Information;

Information.propTypes = {
  onContinue: PropTypes.func.isRequired,
};
