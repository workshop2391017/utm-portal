import { makeStyles } from "@material-ui/styles";
import FormField from "components/BN/Form/InputGroupValidation";
import PapperContainer from "components/BN/PapperContainer";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import AntdTextField from "components/BN/TextField/AntdTextField";
import TextFieldPhone from "components/BN/TextFieldPhone";
import Colors from "helpers/colors";
import React from "react";
import PropTypes from "prop-types";
import { useFormikContext } from "formik";

const useStyles = makeStyles({
  inputWrapper: {
    display: "flex",
    marginBottom: 30,
    justifyContent: "space-between",
    width: "100%",
    "& .inputGroup": {
      flex: 1,
    },
    "& .formTitle": {
      marginBottom: 5,
    },
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 20,
    color: Colors.dark.hard,
    marginBottom: 30,
  },
  example: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.gray.medium,
    marginTop: 10,
  },
});
const FormActivation = ({ onContinue, onBack }) => {
  const classes = useStyles();
  const { values, handleChange, handleBlur, errors, touched } =
    useFormikContext();

  return (
    <div>
      <PapperContainer
        title="Formulir Pengaktifan Virtual Account"
        withButton
        nextTitle="Simpan"
        onContinue={onContinue}
        onCancel={onBack}
      >
        <div className={classes.inputWrapper}>
          <FormField
            label="Nama Perusahaan :"
            error={errors?.form?.company_name && touched?.form?.company_name}
            errorMessage="Nama Perusahaan tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan nama perusahaan Anda"
              name="form.company_name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.company_name}
            />
          </FormField>
          <FormField
            label="NPWP :"
            error={errors?.form?.npwp && touched?.form?.npwp}
            errorMessage="NPWP tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan NPWP Anda"
              name="form.npwp"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.npwp}
            />
          </FormField>
        </div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Alamat :"
            error={errors?.form?.address && touched?.form?.address}
            errorMessage="Alamar tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350, height: 81 }}
              placeholder="Masukan alamat Anda"
              type="textArea"
              name="form.address"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.address}
            />
          </FormField>
          <FormField
            label="Nama Pemilik :"
            error={errors?.form?.owner && touched?.form?.owner}
            errorMessage="Nama Pemilik tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Nama Pemilik"
              name="form.owner"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.owner}
            />
          </FormField>
        </div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Provinsi :"
            error={errors?.form?.province && touched?.form?.province}
            errorMessage="Provinsi tidak boleh kosong"
          >
            <SelectWithSearch
              width={350}
              placeholder="D.I Yogyakarta"
              options={[{ label: "Aceh", value: "Aceh" }]}
              value={values.form.province}
              onSelect={handleChange("form.province")}
            />
          </FormField>
          <FormField
            label="Jabatan :"
            error={errors?.form?.position && touched?.form?.position}
            errorMessage="Position tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan jabatan Anda"
              name="form.position"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.position}
            />
          </FormField>
        </div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Kabupaten/Kota :"
            error={errors?.form?.district && touched?.form?.district}
            errorMessage="Kabupaten/Kota tidak boleh kosong"
          >
            <SelectWithSearch
              width={350}
              placeholder="D.I Yogyakarta"
              options={[{ label: "Aceh", value: "Aceh" }]}
              value={values.form.district}
              onSelect={handleChange("form.district")}
            />
          </FormField>
          <FormField
            label="Bagian :"
            error={errors?.form?.bagian && touched?.form?.bagian}
            errorMessage="Bagian tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan bagian Anda"
              name="form.bagian"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.bagian}
            />
          </FormField>
        </div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Kode Pos :"
            error={errors?.form?.zip_code && touched?.form?.zip_code}
            errorMessage="Kode Pos boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan kode pos Anda"
              name="form.zip_code"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.zip_code}
            />
          </FormField>
          <FormField label="Nomor Telepon Rumah (Opsional) :">
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Nomor telepon rumah Anda"
              name="form.home_phone"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.home_phone}
            />
          </FormField>
        </div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Email :"
            error={errors?.form?.email && touched?.form?.email}
            errorMessage="Email tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan email Anda"
              name="form.email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.email}
            />
          </FormField>
          <FormField
            label="Nomor HP :"
            error={errors?.form?.phone && touched?.form?.phone}
            errorMessage="Nomor HP tidak boleh kosong"
          >
            <TextFieldPhone
              style={{ width: 350 }}
              placeholder="829-5555-0108"
              name="form.phone"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.phone}
            />
          </FormField>
        </div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Bidang Usaha :"
            error={
              errors?.form?.business_field && touched?.form?.business_field
            }
            errorMessage="Bidang Usaha tidak boleh kosong"
          >
            <SelectWithSearch
              width={350}
              placeholder="D.I Yogyakarta"
              options={[{ label: "Aceh", value: "Aceh" }]}
              value={values.form.business_field}
              onSelect={handleChange("form.business_field")}
            />
          </FormField>
          <div className="inputGroup" />
        </div>

        <div className={classes.title}>Pengaturan Parameter</div>
        <div className={classes.inputWrapper}>
          <FormField
            label="Nomor Rekening :"
            error={errors?.form?.no_rek && touched?.form?.no_rek}
            errorMessage="Nomor Rekening tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Masukan nomor rekening Anda"
              name="form.no_rek"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.no_rek}
            />
          </FormField>
          <FormField
            label="Panjang Nomor Virtual Account :"
            error={errors?.form?.va_length && touched?.form?.va_length}
            errorMessage="Panjang Nomor Virtual Account tidak boleh kosong"
          >
            <SelectWithSearch
              width={350}
              placeholder="10 Karakter"
              options={[{ label: "10 Karakter", value: "10 Karakter" }]}
              value={values.form.va_length}
              onSelect={handleChange("form.va_length")}
            />
            <div className={classes.example}>Contoh : 122 6728 416</div>
          </FormField>
        </div>

        <div className={classes.inputWrapper}>
          <FormField
            label="Panjang ID Pelanggan :"
            error={
              errors?.form?.id_cutomer_length &&
              touched?.form?.id_cutomer_length
            }
            errorMessage="Panjang ID Pelanggan tidak boleh kosong"
          >
            <SelectWithSearch
              width={350}
              placeholder="2 Karakter"
              options={[{ label: "2 Karakter", value: "2 Karakter" }]}
              value={values.form.id_cutomer_length}
              onSelect={handleChange("form.id_cutomer_length")}
            />
            <div className={classes.example}>Contoh : 122 6728 416</div>
          </FormField>
          <div className="inputGroup" />
        </div>

        <div className={classes.title}>Nama Pembayaran :</div>

        <div className={classes.inputWrapper}>
          <FormField
            label=""
            error={errors?.form?.payment_name && touched?.form?.payment_name}
            errorMessage="Nama Pembayaran tidak boleh kosong"
          >
            <AntdTextField
              style={{ width: 350 }}
              placeholder="Listrik|"
              name="form.payment_name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.form.payment_name}
            />
          </FormField>
          <div className="inputGroup" />
        </div>
      </PapperContainer>
    </div>
  );
};

export default FormActivation;

FormActivation.propTypes = {
  onContinue: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired,
};
