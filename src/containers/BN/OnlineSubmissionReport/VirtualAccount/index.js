import React, { useEffect, useState } from "react";
import { makeStyles, Button as MUIButton } from "@material-ui/core";
import { useHistory } from "react-router-dom";

// component
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import TableICBB from "components/BN/TableIcBB";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";
import { pathnameCONFIG } from "configuration";

// assets
import Filter from "components/BN/Filter/GeneralFilter";
import GeneralButton from "components/BN/Button/GeneralButton";

import {
  handleVAReportList,
  setSelectedCompany,
} from "stores/actions/onlineSubmissonReport/vaReport";
import { useDispatch, useSelector } from "react-redux";

// helpers
import {
  setLocalStorage,
  searchDateStart,
  searchDateEnd,
  generalDateFormat,
} from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";
import moment from "moment";

const dataHeader = ({ history, dispatch }) => [
  {
    title: "Date & Time",
    headerAlign: "left",
    align: "left",

    render: (rowData) => (
      <span>{moment(rowData.nativeDate).format("DD-MM-YYYY | HH:mm:ss")}</span>
    ),
  },
  {
    title: "Company Id",
    key: "registration_id",
    render: (rowData) => <span>{rowData?.companyId}</span>,
  },
  {
    title: "Company Name",
    key: "company",
    render: (rowData) => <span>{rowData?.companyName}</span>,
  },
  {
    title: "",
    key: "message",
    width: 100,
    render: (rowData) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{
            padding: "6px 10px",
            fontSize: 9,
            fontWeight: "bold",
            fontFamily: "FuturaMdBT",
          }}
          onClick={() => {
            dispatch(setSelectedCompany(rowData));
            setLocalStorage("vaReport", JSON.stringify(rowData));
            history.push(pathnameCONFIG.ONLINE_SUBMISSION_REPORT.VA_DETAIL);
          }}
        />
      </div>
    ),
  },
];

const ReportVirtualAccount = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState("");
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);
  const prevPage = usePrevious(page);
  const { isLoading, data } = useSelector((e) => e.vaReport);
  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();
  const dataSearchDeb = useDebounce(dataSearch, 1300);

  const loadData = () => {
    const payload = {
      startDate: dataFilter?.date?.dateValue1
        ? generalDateFormat(dataFilter?.date?.dateValue1)
        : null,
      endDate: dataFilter?.date?.dateValue2
        ? generalDateFormat(dataFilter?.date?.dateValue2)
        : null,
      companyName: "",
      corporateId: "",
      pageNumber: page,
      pageSize: 10,
      addtionalSearch: dataSearchDeb || null,
    };
    dispatch(handleVAReportList(payload));
  };

  useEffect(() => {
    if (page === 1 || prevPage !== page) {
      loadData();
    } else {
      setPage(1);
    }
  }, [dataSearchDeb, dataFilter, page]);

  return (
    <div className={classes.handlingofficer}>
      <HandlingOfficerModal
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <Title label="Virtual Account Report">
        <Search
          placeholder="Company ID, Company Name"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <div style={{ marginBottom: 20 }}>
          <Filter
            dataFilter={dataFilter}
            setDataFilter={(e) => setDataFilter(e)}
            align="left"
            options={[
              {
                id: 1,
                type: "datePicker",
                placeholder: "Start Date",
              },
              {
                id: 2,
                type: "datePicker",
                placeholder: "End Date",
              },
            ]}
          />
        </div>
        <TableICBB
          headerContent={dataHeader({ history, dispatch })}
          dataContent={data?.vaReportDtoList ?? []}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
        />
      </div>
    </div>
  );
};

ReportVirtualAccount.propTypes = {};

ReportVirtualAccount.defaultProps = {};

export default ReportVirtualAccount;
