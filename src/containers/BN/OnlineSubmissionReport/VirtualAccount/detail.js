import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

// MUI
import { Box, makeStyles, Button, Grid } from "@material-ui/core";

// antd
import { Typography } from "antd";

// components
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import Collapse from "components/BN/Collapse";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import Colors from "helpers/colors";

// Redux
import {
  setSelectedCompany,
  handleVAReportDetail,
} from "stores/actions/onlineSubmissonReport/vaReport";
import { useSelector, useDispatch } from "react-redux";
import { getLocalStorage } from "utils/helpers";

const InstitusiVaDetail = () => {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const handleBack = () => history.goBack();
  const dispatch = useDispatch();

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    rowcontainer: {
      display: "flex",
      flexDirection: "row",
      //   justifyContent: "space-between",
      marginBottom: "30px",
      //   backgroundColor: "red",
    },
    fieldTitle: {
      fontSize: 13,
      fontWeight: 400,
      fontFamily: "FuturaHvBT",
      color: "#7B87AF",
      marginBottom: "5px",
    },
    subFieldTitle: {
      fontSize: 14,
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      color: "#374062",
    },
    mainContainer: {
      margin: "63px 115px",
      //   margin: "50px",
      borderRadius: "20px",
      overflow: "hidden",
    },
    mainContent: {
      backgroundColor: "#fff",
      padding: "50px",
      "& .title": {
        fontSize: "20px",
        color: "#374062",
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        marginBottom: "30px",
      },
    },
    titleCode: {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#7B87AF",
      fontWeight: 400,
    },
    gridTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    gridValue: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    gridValueBold: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    card: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      padding: "15px 20px",
      gap: "10px",
      width: "350px",
      height: "140px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
    cardva: {
      width: 335,
      height: 91,
      borderRadius: 10,
      border: "1px solid",
      borderColor: Colors.gray.medium,
      padding: "15px",
    },
  });

  const classes = useStyles();

  const { selectedCompany, detail } = useSelector((e) => e.vaReport);

  useEffect(() => {
    if (!selectedCompany.reffNoParent) {
      const companyDataStorage = getLocalStorage("vaReport");
      const companyData = companyDataStorage
        ? JSON.parse(companyDataStorage)
        : {};
      dispatch(setSelectedCompany(companyData));
    }
  }, []);

  useEffect(() => {
    if (selectedCompany.reffNoParent) {
      dispatch(handleVAReportDetail({ reffNo: selectedCompany.reffNoParent }));
    }
  }, [dispatch, selectedCompany]);

  const badgeRender = (badge) => {
    if (badge === "NONBILLING") {
      return (
        <Badge
          type="green"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }

    if (badge === "BILLING") {
      return (
        <Badge
          type="blue"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }
  };

  const FieldVirtualAccount = ({ name, val, badge, code }) => (
    <div style={{ flex: 1 }}>
      <div className={classes.fieldTitle}>{name ? <div>{name}</div> : ""}</div>
      <div
        // weight={500}
        size="15px"
        width="350px"
        className={classes.subFieldTitle}
      >
        {code}-{val}
      </div>
      <div>{badgeRender(badge)}</div>
    </div>
  );

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemParameterSettings = ({
    title,
    value,
    valueBold,
    parentSize = 6,
  }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title ?? <br />}
        </Grid>
        {valueBold && (
          <Grid item className={classes.gridValueBold}>
            {valueBold}
          </Grid>
        )}
        <Grid item className={classes.gridValue}>
          {value !== "BILLING" && value !== "NONBILLING"
            ? value
            : badgeRender(value)}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={handleBack}
      />
      <Title label="Virtual Account Report Details" />
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">Applicant Data</span>
        </div>
        <div className={classes.mainContent}>
          <Collapse label="Business Field Profile" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Company Name :"
                value={
                  detail?.companyName && detail?.companyName !== ""
                    ? detail?.companyName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Email :"
                value={
                  detail?.email && detail?.email !== "" ? detail?.email : "-"
                }
                size={6}
              />
              <GridItem
                title="Address :"
                value={
                  detail?.address && detail?.address !== ""
                    ? detail?.address
                    : "-"
                }
                size={12}
              />
              <GridItem
                title="Regency/City :"
                value={detail?.city && detail?.city !== "" ? detail?.city : "-"}
                size={6}
              />
              <GridItem
                title="Province :"
                value={
                  detail?.province && detail?.province !== ""
                    ? detail?.province
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Postal Code :"
                value={
                  detail?.postalCode && detail?.postalCode !== ""
                    ? detail?.postalCode
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Business Fields :"
                value={
                  detail?.fields && detail?.fields !== "" ? detail?.fields : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Person in Charge" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Owner Name :"
                value={
                  detail?.ownerName && detail?.ownerName !== ""
                    ? detail?.ownerName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="NPWP :"
                value={detail?.npwp && detail?.npwp !== "" ? detail?.npwp : "-"}
                size={6}
              />
              <GridItem
                title="Position :"
                value={
                  detail?.position && detail?.position !== ""
                    ? detail?.position
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Section :"
                value={detail?.part && detail?.part !== "" ? detail?.part : "-"}
                size={6}
              />
              <GridItem
                title="Home Phone Number (Operational) :"
                value={
                  detail?.homePhoneNo && detail?.homePhoneNo !== ""
                    ? detail?.homePhoneNo
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Mobile Phone Number :"
                value={
                  detail?.mobilePhoneNo && detail?.mobilePhoneNo !== ""
                    ? detail?.mobilePhoneNo
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Fax Number :"
                value={
                  detail?.faxNo && detail?.faxNo !== "" ? detail?.faxNo : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Branches" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <div className={classes.card}>
              <h6 className={classes.gridValueBold}>
                {detail?.branchInfo?.name && detail?.branchInfo?.name !== ""
                  ? detail?.branchInfo?.name
                  : "-"}
              </h6>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span className={classes.gridTitle}>
                  {detail?.branchInfo?.phone && detail?.branchInfo?.phone !== ""
                    ? `Telepon : ${detail?.branchInfo?.phone}`
                    : "Phone : -"}
                </span>
                <span className={classes.gridTitle}>
                  {detail?.branchInfo?.fax && detail?.branchInfo?.fax !== ""
                    ? `Fax : ${detail?.branchInfo?.fax}`
                    : "Fax : -"}
                </span>
              </div>
              <span className={classes.gridTitle}>
                {detail?.branchInfo?.address &&
                detail?.branchInfo?.address !== ""
                  ? detail?.branchInfo?.address
                  : "-"}
              </span>
            </div>
            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Parameter Settings" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItemParameterSettings
                title="Selected Account :"
                value={
                  detail?.accountNumber && detail?.accountNumber !== ""
                    ? detail?.accountNumber
                    : "-"
                }
                valueBold={
                  detail?.productName && detail?.productName !== ""
                    ? detail?.productName
                    : "-"
                }
                parentSize={12}
              />
              <GridItemParameterSettings
                title="VA Number Length :"
                value={
                  detail?.vaNoLength && detail?.vaNoLength !== ""
                    ? `${detail?.vaNoLength} Characters`
                    : "-"
                }
              />
              <GridItemParameterSettings
                title="Customer ID Length :"
                value={
                  detail?.customerIdLength && detail?.customerIdLength !== ""
                    ? `${detail?.customerIdLength} Characters`
                    : "-"
                }
              />
              {detail?.paymentBill?.map((el, i) => (
                <GridItemParameterSettings
                  key={i}
                  value={
                    el?.billingType && el?.billingType !== ""
                      ? el?.billingType
                      : "-"
                  }
                  valueBold={
                    el?.billingCode &&
                    el?.billingName &&
                    el?.billingCode !== "" &&
                    el?.billingName !== ""
                      ? `${el?.billingCode} - ${el?.billingName}`
                      : "-"
                  }
                />
              ))}
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Box sx={{ margin: "10px auto" }} />
        </div>
      </div>
    </div>
  );
};

InstitusiVaDetail.propTypes = {};

InstitusiVaDetail.defaultProps = {};

export default InstitusiVaDetail;
