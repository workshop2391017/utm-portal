// main
import { CircularProgress, makeStyles } from "@material-ui/core";

// assets
import iconSync from "assets/icons/BN/IconSync.svg";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Table from "components/BN/Table/TableCollapse";

// components
import Title from "components/BN/Title";

// libraries
import Toast from "components/BN/Toats";
import React, { useEffect, useState } from "react";
import {
  getDataBranchPengaturan,
  getDataBranchPengaturanButtonSync,
  setHandleClearError,
} from "stores/actions/branchPengaturan/branchPengaturan";
import { useDispatch, useSelector } from "react-redux";
import GeneralButton from "components/BN/Button/GeneralButton";
import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";

const BillerKategori = (props) => {
  const dispatch = useDispatch();
  const { data, isLoading, isLoadingSync, error } = useSelector(
    (state) => state.branchPengaturan
  );
  const useStyles = makeStyles({
    billerKategori: {},
    container: {
      padding: "20px 30px 30px",
    },
  });
  const classes = useStyles();

  const [page, setPage] = useState(1);

  const [searchValue, setSearchValue] = useState("");
  const hasil = useDebounce(searchValue, 1500);
  const prevSearch = usePrevious(hasil);

  const tableConfig = [
    {
      title: "Branch ID",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.branchId}</span>,
    },

    {
      title: "Office Type",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData?.branchTypeCode}</span>,
    },
    {
      title: "Outlet Name",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.name}</span>,
    },

    {
      title: "Province",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.province}</span>,
    },
    {
      title: "City",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.city}</span>,
    },
    {
      title: "Phone",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.phone}</span>,
    },
    {
      title: "Status",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <span
          style={{
            color:
              rowData.status === 1
                ? "rgba(117, 211, 127, 1)"
                : "rgba(55, 64, 98, 1)",
            fontSize: "13px",
          }}
        >
          {rowData.status === 1 ? "Aktif" : "Tidak Aktif"}
        </span>
      ),
    },
  ];

  useEffect(() => {
    const paylod = {
      pageNumber: parseInt(page) - 1,
      pageSize: 10,
      searchValue: hasil,
    };

    if (page === 1) {
      dispatch(getDataBranchPengaturan(paylod));
    } else if (hasil !== prevSearch) {
      setPage(1);
    } else {
      dispatch(getDataBranchPengaturan(paylod));
    }
  }, [hasil, page]);

  const onSyncData = () => {
    dispatch(getDataBranchPengaturanButtonSync());
  };

  return (
    <div className={classes.billerKategori}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Title label="Branch Setting">
        {/* dipakai */}
        <Search
          style={{
            width: 265,
          }}
          options={[
            "ID Cabang",
            "Jenis Kantor",
            "nama Outlet",
            "Provinsi",
            "Kota",
            "Telepone",
            "Status",
          ]}
          placeholder="Branch ID, Outlet Name"
          placement="bottom"
          dataSearch={searchValue}
          setDataSearch={setSearchValue}
        />
        <GeneralButton
          iconPosition="startIcon"
          buttonIcon={
            isLoadingSync === 1 ? null : (
              <img
                src={iconSync}
                alt="iconsync"
                style={{
                  width: 18,
                  height: 15,
                  marginRight: -3,
                }}
              />
            )
          }
          style={{
            width: 128,
            height: 44,
            fontSize: 15,
            fontFamily: "FuturaMdBT",
            paddingTop: 9.5,
            paddingBottom: 9.5,
            paddingRight: 20,
            paddingLeft: 20,
            marginLeft: 20,
            marginBottom: -10,
            background: isLoadingSync === 1 ? "#BCC8E7" : "#0061A7",
          }}
          label={
            isLoadingSync === 1 ? (
              <CircularProgress color="primary" size={20} />
            ) : (
              "Sync Data"
            )
          }
          onClick={onSyncData}
        />
      </Title>
      <div className={classes.container}>
        <Table
          data={data.branch ?? []}
          cols={tableConfig}
          page={page}
          setPage={setPage}
          isLoading={isLoading}
          totalData={data?.totalElements}
          totalPages={data?.totalPages}
          collapseLocation="Branch ID"
          disableCollapseCell={["Kode Tran", "Nomor Urutan"]}
        />
      </div>
    </div>
  );
};

BillerKategori.propTypes = {};

BillerKategori.defaultProps = {};

export default BillerKategori;
