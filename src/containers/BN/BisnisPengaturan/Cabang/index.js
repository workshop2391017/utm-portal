// main
import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";

// libraries

// components
import Title from "../../../../components/BN/Title";
import Search from "../../../../components/BN/Search/SearchWithDropdown";
import Table from "../../../../components/BN/Table/GeneralTablePagination";

// helpers

const Cabang = (props) => {
  // const {} = props;
  const useStyles = makeStyles({
    cabang: {},
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(1);
  const [dataSearch, setDataSearch] = useState(null);

  const tableConfig = [
    {
      title: "ID Cabang",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.id}</span>,
    },
    {
      title: "Nama Cabang",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.name}</span>,
    },
    {
      title: "Provinsi",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.province}</span>,
    },
    {
      title: "Kota",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.city}</span>,
    },
    {
      title: "Telepon",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.no}</span>,
    },
    {
      title: "Tipe",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <div>
          {rowData.type.map((val, i) => {
            let label;
            let background;
            let border;
            let color;
            let radius;
            let padding;
            switch (val) {
              case 0:
                label = "Konven";
                color = "#66A3FF";
                border = "1px solid #66A3FF";
                background = "#EAF2FF";
                padding = "3px 9px";
                break;
              default:
                color = "#D375AE";
                label = "Syariah";
                border = "1px solid #D375AE";
                background = "#FCDCFF";
                padding = "3px";
                break;
            }
            return (
              <span
                style={{
                  borderRadius: 8,
                  border,
                  background,
                  display: "inline-block",
                  width: 60,
                  height: 22,
                  textAlign: "center",
                  padding,
                  margin: "0 5px 0 0",
                  color,
                  fontFamily: "FuturaBkBT",
                  fontSize: "13px",
                  lineHeight: "16px",
                }}
              >
                {label}
              </span>
            );
          })}
        </div>
      ),
    },
    {
      title: "Status",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <span>{rowData.status === 0 ? "-" : "Tidak Aktif"}</span>
      ),
    },
  ];

  return (
    <div className={classes.cabang}>
      <Title label="Cabang">
        <Search
          options={[
            "ID Cabang",
            "Nama Cabang",
            "Provinsi",
            "Kota",
            "Telepon",
            "Tipe",
            "Status",
          ]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <div>
          <Table
            data={dataTable && dataTable[page - 1]}
            cols={tableConfig}
            page={page}
            setPage={setPage}
            totalPages={dataTable && dataTable.length}
            totalData={dataTable && dataTable.length}
          />
        </div>
      </div>
    </div>
  );
};

Cabang.propTypes = {};

Cabang.defaultProps = {};

export default Cabang;
