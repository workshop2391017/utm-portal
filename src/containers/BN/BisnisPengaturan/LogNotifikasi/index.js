// main
import React, { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core";

// libraries

// components
import Title from "../../../../components/BN/Title";
import Search from "../../../../components/BN/Search/SearchWithDropdown";
import Filter from "../../../../components/BN/Filter/GeneralFilter";
import Table from "../../../../components/BN/Table/GeneralTablePagination";
import GeneralButton from "../../../../components/BN/Button/GeneralButton";
import ButtonOutlined from "../../../../components/BN/Button/ButtonOutlined";

// components Modal
import Modal from "../../../../components/BN/Popups/EditBankManager";
import ModalBlastPromo from "../../../../components/BN/Popups/BlastPromo";
import ModalBlastPesan from "../../../../components/BN/Popups/BlastPesan";
import ModalBlastApp from "../../../../components/BN/Popups/BlastAppNew";

// assets
import { ReactComponent as Plus } from "../../../../assets/icons/BN/plus.svg";
import { ReactComponent as PlusBlue } from "../../../../assets/icons/BN/plus-blue.svg";

const LogNotifikasi = (props) => {
  //   const {} = props;
  const useStyles = makeStyles({
    LogNotifikasi: {},
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(1);
  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  const [modalLogNotifikasi, setModalLogNotifikasi] = useState(false);
  const [blastPromo, setBlastPromo] = useState(false);
  const [blastPesan, setBlastPesan] = useState(false);
  const [blastApp, setBlastApp] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);

  const [checkList, setCheckList] = useState([]);

  const handleOpenConfirm = () => {
    setBlastPromo(false);
    setBlastPesan(false);
    setBlastApp(false);
    setConfirmSuccess(true);
  };

  const tableConfig = [
    {
      title: "Tanggal",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.date}</span>,
    },
    {
      title: "CIF",
      headerAlign: "center",
      align: "center",
      render: (rowData) => <span>{rowData.cif}</span>,
    },
    {
      title: "Message",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.message}</span>,
    },
    {
      title: "Status",
      headerAlign: "left",
      align: "left",
      render: (rowData) => {
        if (rowData.status === 0) {
          return (
            <span
              style={{
                width: 72,
                height: 23,
                background: "#FFE4E4",
                fontSize: 13,
                border: "1px solid #FF6F6F",
                borderRadius: 8,
                color: "#FF6F6F",
                display: "inline-block",
                textAlign: "center",
              }}
            >
              Failed
            </span>
          );
        }
        return (
          <span
            style={{
              width: 72,
              height: 23,
              background: "#E7FFDC",
              fontSize: 13,
              border: "1px solid #75D37F",
              borderRadius: 8,
              color: "#75D37F",
              display: "inline-block",
              textAlign: "center",
            }}
          >
            Success
          </span>
        );
      },
    },
  ];

  // reset checklist data when change page
  useEffect(() => {
    setCheckList([]);
  }, [page]);

  return (
    <div className={classes.LogNotifikasi}>
      <Title label="Blast Notifikasi">
        <ButtonOutlined
          label="Blast App Baru"
          iconPosition="startIcon"
          buttonIcon={<PlusBlue />}
          width="170px"
          height="40px"
          color="#0061A7"
          style={{ marginRight: 20 }}
          onClick={() => setBlastApp(true)}
        />
        <ButtonOutlined
          label="Blast Pesan"
          iconPosition="startIcon"
          buttonIcon={<PlusBlue />}
          width="150px"
          height="40px"
          color="#0061A7"
          style={{
            marginRight: 20,
          }}
          onClick={() => setBlastPesan(true)}
        />
        <GeneralButton
          label="Blast Promo"
          iconPosition="startIcon"
          buttonIcon={<Plus />}
          width="160px"
          height="40px"
          style={{ marginRight: 20 }}
          onClick={() => setBlastPromo(true)}
        />
        <Search
          options={["CIF", "Message", "Status"]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="center"
          options={[
            {
              id: 1,
              type: "tabs",
              options: ["Push Notif", "SMS", "WA"],
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "Dari Tanggal",
            },
            {
              id: 3,
              type: "datePicker",
              placeholder: "Hingga Tanggal",
            },
          ]}
        />
        <div style={{ marginTop: 20 }}>
          <Table
            data={dataTable && dataTable[page - 1]}
            cols={tableConfig}
            page={page}
            setPage={setPage}
            totalPages={dataTable && dataTable.length}
            totalData={dataTable && dataTable.length}
          />
        </div>
      </div>
      <Modal
        label="Ubah Bank"
        isOpen={modalLogNotifikasi}
        handleClose={() => setModalLogNotifikasi(false)}
        // onContinue={handleOpenModal}
      />
      <ModalBlastPromo
        label="Blast Promo"
        isOpen={blastPromo}
        handleClose={() => setBlastPromo(false)}
        onContinue={handleOpenConfirm}
      />
      <ModalBlastPesan
        label="Blast Pesan"
        isOpen={blastPesan}
        handleClose={() => setBlastPesan(false)}
        onContinue={handleOpenConfirm}
      />
      <ModalBlastApp
        label="Blast App Baru"
        isOpen={blastApp}
        handleClose={() => setBlastApp(false)}
        onContinue={handleOpenConfirm}
      />
    </div>
  );
};

LogNotifikasi.propTypes = {};

LogNotifikasi.defaultProps = {};

export default LogNotifikasi;
