// main
import { Button, makeStyles } from "@material-ui/core";

// assets
import edit from "assets/icons/BN/edit-3.svg";
import AddComment from "components/BN/Popups/AddComment";
import EditModal from "components/BN/Popups/EditGeneralParameter";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Search from "components/BN/Search/SearchWithoutDropdown";
import TableICBB from "components/BN/TableIcBB";

// components
import Title from "components/BN/Title";
import Toast from "components/BN/Toats";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { checkOnline } from "stores/actions/errorGeneral";
import {
  handleGetGeneralParams,
  setEditgeneralParams,
  setGeneralParamsForm,
  setHandleClearError,
  submitRequestUpdateParameter,
} from "stores/actions/generalparam";
import { getLocalStorage } from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";
import DeletePopup from "components/BN/Popups/Delete";

const BillerKategori = (props) => {
  const useStyles = makeStyles({
    billerKategori: {},
    container: {
      padding: "20px 30px 30px",
    },
  });

  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    parameters: data,
    totalElements,
    totalPages,
    isLoading,
    generalParamsForm,
    isLoadingSubmit,
    error,
  } = useSelector(
    ({ generalParam }) => ({
      isLoading: generalParam?.isLoading,
      generalParamsForm: generalParam?.generalParamsForm,
      isLoadingSubmit: generalParam?.isLoadingSubmit,
      totalElements: generalParam?.totalElements,
      error: generalParam?.error,
      ...generalParam.data,
    }),
    shallowEqual
  );

  const [page, setPage] = useState(1);
  const [dataSearch, setDataSearch] = useState(null);
  const [editModal, setEditModal] = useState(false);
  const [commentModal, setCommentModal] = useState(false);
  const [editConfirmation, setEditConfirmation] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
  });

  const handleCollapseText = (text, textLength) => {
    const val = textLength || 50;
    if (text.length > val) {
      return `${text.substring(0, val)}...`;
    }
    return text;
  };

  const tableConfig = [
    {
      title: "Nomor",
      headerAlign: "left",
      key: "id",
      width: "60px",
      fontFamily: "FuturaBQ",
      fontSize: "13px",
      render: (a, index) =>
        page > 1 ? (page - 1) * 10 + (index + 1) : index + 1,
    },
    {
      title: "Module",
      headerAlign: "left",
      align: "left",
      key: "module",
      fontFamily: "FuturaMdBT",
      fontSize: "13px",
      render: (rowData) => (
        <span style={{ overflowWrap: "anywhere" }}>
          {handleCollapseText(rowData.module)}
        </span>
      ),
    },
    {
      title: "Value",
      headerAlign: "left",
      align: "left",
      width: "450px",
      fontFamily: "FuturaBQ",
      fontSize: "13px",
      render: (rowData) => <span>{handleCollapseText(rowData.value)}</span>,
    },
    {
      title: "Code",
      headerAlign: "left",
      align: "left",
      key: "name",
      fontFamily: "FuturaBQ",
      fontSize: "13px",
      width: 300,
      render: (rowData) => <span>{handleCollapseText(rowData.name, 25)}</span>,
    },
    {
      title: "Description",
      headerAlign: "left",
      align: "left",
      key: "description",
      fontFamily: "FuturaBQ",
      fontSize: "13px",
      render: (rowData) => (rowData.description ? rowData.description : "-"),
    },
    {
      title: "",
      headerAlign: "right",
      align: "right",
      width: 100,
      fontFamily: "FuturaMdBT",
      render: (rowData) => (
        <Button
          startIcon={<img alt="edit" src={edit} />}
          color="primary"
          style={{
            textTransform: "capitalize",
            fontFamily: "FuturaMdBT",
            fontSize: "13px",
          }}
          onClick={() => {
            dispatch(setEditgeneralParams(rowData));

            setEditModal(true);
          }}
        >
          Edit
        </Button>
      ),
    },
  ];

  const clickEditContinue = (data) => {
    dispatch(setGeneralParamsForm(data));
    setEditConfirmation(true);
    setEditModal(false);
  };

  const handleSuccess = () => {
    setSuccessConfirmModal({
      isOpen: true,
    });
  };

  const handleNext = () => {
    setCommentModal(false);
  };

  const clickCommentContinue = async (_, comment) => {
    dispatch(setEditgeneralParams({}));

    dispatch(checkOnline());

    const requestPayload = {
      requestComment: comment,
      ...generalParamsForm,
    };

    dispatch(
      submitRequestUpdateParameter(requestPayload, {
        handleSuccess,
        handleNext,
      })
    );
  };

  const debSerch = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(debSerch);

  // get dataTable
  useEffect(async () => {
    const requestPayload = {
      direction: "DESC",
      pageNumber: page - 1,
      pageSize: 10,
      sortBy: "id",
      module: debSerch || "",
    };

    if (page === 1) {
      dispatch(handleGetGeneralParams({ ...requestPayload }));
    } else if (debSerch !== prevSearch) {
      setPage(1);
    } else {
      dispatch(handleGetGeneralParams({ ...requestPayload }));
    }
  }, [page, debSerch]);

  return (
    <div className={classes.billerKategori}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Title label="General Parameter">
        <Search
          options={["No.", "Module", "Value", "Kode", "Deskripsi"]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="module"
          placement="left"
        />
      </Title>
      <div className={classes.container}>
        <TableICBB
          headerContent={tableConfig}
          dataContent={data ?? []}
          page={page}
          setPage={setPage}
          isLoading={isLoading}
          totalData={totalPages}
          totalElement={totalElements}
          noDataMessage="No Data"
        />
      </div>
      <DeletePopup
        isOpen={editConfirmation}
        handleClose={() => setEditConfirmation(false)}
        onContinue={clickCommentContinue}
        loading={isLoadingSubmit}
        message="Are You Sure To Edit Data?"
      />
      <EditModal
        isOpen={editModal}
        handleClose={() => {
          setEditModal(false);
          setCommentModal(false);
          dispatch(setEditgeneralParams({}));
        }}
        onContinue={clickEditContinue}
      />
      {/* <AddComment
        formLabel="Comment Change :"
        isOpen={commentModal}
        handleClose={() => {
          setCommentModal(false);
          dispatch(setEditgeneralParams({}));
        }}
        // onContinue={clickCommentContinue}
        onContinue={() => setEditConfirmation(true)}
        loading={isLoadingSubmit}
      /> */}
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        titlefontSize={28}
        handleClose={() => {
          setEditConfirmation(false);
          setSuccessConfirmModal({
            isOpen: false,
          });
        }}
      />
    </div>
  );
};

BillerKategori.propTypes = {};

BillerKategori.defaultProps = {};

export default BillerKategori;
