// main
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core";

// redux
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
  deleteUserAdmin,
  handleManagementUser,
  setErrorLdapClear,
  setErrorUserAlreadyExist,
  setHandleClearError,
  setUserData,
} from "stores/actions/managementuser";

// components
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Button from "components/BN/Button/GeneralButton";
import TambahPengaturanUser from "components/BN/Popups/TambahPengaturanUser";

import TableICBB from "components/BN/TableIcBB";

// assets
import { ReactComponent as SvgPlus } from "assets/icons/BN/plus-white.svg";
import Toast from "components/BN/Toats";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import DeletePopup from "components/BN/Popups/Delete";
import Badge from "components/BN/Badge";
import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { validateTask } from "stores/actions/validateTaskPortal";
import { dataUserDummy, dataDetailUser } from "./index.dummy";

const tableConfig = ({ onDelete, onEdit }) => [
  {
    title: "ID",
    headerAlign: "left",
    align: "left",
    width: "50px",
    key: "id",
  },
  {
    title: "Fullname",
    headerAlign: "left",
    align: "left",
    key: "fullname",
  },
  {
    title: "Phone Number",
    headerAlign: "left",
    align: "left",
    key: "phonenumber",
  },
  {
    title: "Email",
    headerAlign: "left",
    align: "left",
    width: "200px",
    key: "email",
  },
  {
    title: "Unit",
    headerAlign: "left",
    align: "left",
    width: "100px",
    key: "unit",
  },
  {
    title: "Role",
    headerAlign: "left",
    align: "left",
    width: "90px",
    key: "role",
    render: (rowData) => (
      <div style={{ textTransform: "capitalize" }}>
        {rowData?.role?.replaceAll("_", ", ")?.toLowerCase()}
      </div>
    ),
  },
  {
    title: "Workflow",
    headerAlign: "left",
    align: "left",
    width: "80px",
    key: "workflow",
  },
  {
    title: "Status",
    headerAlign: "left",
    align: "left",
    width: "80px",
    key: "status",
    render: (rowData) => (
      <Badge
        type={rowData.status == "Active" ? "green" : "orange"}
        label={rowData.status}
        styleBadge={{ width: "80px" }}
      />
    ),
  },
  {
    title: "",
    headerAlign: "right",
    width: "90px",
    fontFamily: "FuturaMdBT",
    render: (rowData) => (
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <GeneralMenu
          options={["edit", "delete", "inactive"]}
          onEdit={() => onEdit(rowData)}
          onDelete={() => onDelete(rowData)}
        />
      </div>
    ),
  },
];

const BillerManager = (props) => {
  const useStyles = makeStyles({
    billerManager: {},
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    isLoading,
    error,
    userAdminPaging: { totalPage, listPortalUser, totalElement },
    userData,
    userAdminPaging,
    isUserAlreadyExist,
  } = useSelector(({ managementuser }) => managementuser, shallowEqual);

  const [addUserModal, setAddUserModal] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [page, setPage] = useState(1);

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: "",
  });
  const [deleteConf, setDeleteConf] = useState(false);
  const [rowData, setRowData] = useState({});
  const [openModalAlreadyExist, setOpenModalAlreadyExist] = useState(false);

  const [isDelete, setIsDelete] = useState(undefined);

  useEffect(() => {
    if (isUserAlreadyExist) setOpenModalAlreadyExist(isUserAlreadyExist);
  }, [isUserAlreadyExist]);

  const { isLoadingSubmit } = useSelector(
    ({ managementuser }) => managementuser
  );

  const clickPush = () => {
    setAddUserModal(true);
  };

  const searchBounce = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(searchBounce);

  useEffect(async () => {
    const payloadRequest = {
      direction: "ASC",
      limit: 10,
      page: page - 1,
      searchBy: null,
      shortBy: null,
      searchValue: searchBounce || null,
    };

    if (page === 1) {
      dispatch(handleManagementUser({ ...payloadRequest }));
    } else if (searchBounce !== prevSearch) {
      setPage(1);
    } else {
      dispatch(handleManagementUser({ ...payloadRequest }));
    }
  }, [searchBounce, page]);

  const onDelete = (row) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PORTAL_USER,
          id: row.idUser,
          code: null,
          name: null,
          validate: true,
        },
        {
          async onContinue() {
            setIsDelete(true);
            setRowData(row);
            setDeleteConf(true);
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            // handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const onEdit = (row) => {
    dispatch(setUserData(row));
    setAddUserModal(true);
  };

  const handleSuccess = () => {
    setSuccessConfirmModal({
      isOpen: true,
      message: "Data Deleted Successfully",
      title: "Deleted Successfully",
    });
  };

  const handleNext = () => setDeleteConf(false);

  const hanldeDeleteUser = () => {
    dispatch(
      deleteUserAdmin(
        {
          ...rowData,
          branch: rowData?.branchId,
          role: rowData?.roleId,
          password: null,
          phoneNumber: null,
          requestComment: null,
          ipAddress: null,
        },
        {
          id: userData?.idUser,
          name: null,
          code: null,
        },
        {
          handleNext,
          handleSuccess,
        }
      )
    );
  };

  return (
    <div className={classes.billerManager}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />

      <SuccessConfirmation
        titlefontSize={28}
        message={isDelete ? "Deleted Successfully" : undefined}
        isOpen={successConfirmModal?.isOpen}
        handleClose={() => {
          setSuccessConfirmModal({ isOpen: false, message: "" });
          setIsDelete(false);
        }}
      />

      <TambahPengaturanUser
        isOpen={addUserModal}
        handleClose={() => {
          dispatch(setErrorUserAlreadyExist(false));
          dispatch(setErrorLdapClear());
          setAddUserModal(false);
        }}
        title="Add User"
        onContinue={(e) => {
          dispatch(setErrorLdapClear());
          if (e) {
            setSuccessConfirmModal(true);
            setAddUserModal(false);
          } else {
            setSuccessConfirmModal(false);
          }
        }}
      />

      <DeletePopup
        isOpen={deleteConf}
        handleClose={() => setDeleteConf(false)}
        onContinue={hanldeDeleteUser}
        loading={isLoadingSubmit}
        title="Confirmation"
        message="Are You Sure to Delete the Data?"
      />

      <Title label="User Settings">
        <Search
          options={[
            "Nama Biller",
            "Kode Payee",
            "Kategori",
            "Payment Handler",
            "Sumber Dana",
            "Status",
          ]}
          placeholder="Name, Username, or NIP"
          placement="bottom"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          style={{ width: 286, height: 40 }}
        />

        <Button
          label="New User"
          iconPosition="startIcon"
          buttonIcon={<SvgPlus width={16.67} height={16.67} />}
          width="182px"
          height="44px"
          style={{
            marginLeft: 30,
            width: 182,
            height: 44,
            fontFamily: "FuturaMdBT",
            fontSize: 15,
            fontWeight: "bold",
          }}
          onClick={clickPush}
        />
      </Title>
      <div className={classes.container}>
        <TableICBB
          headerContent={tableConfig({ onDelete, onEdit })}
          dataContent={dataDetailUser}
          page={page}
          setPage={setPage}
          isLoading={isLoading}
          totalData={totalPage}
          totalElement={totalElement}
        />
      </div>
    </div>
  );
};

BillerManager.propTypes = {};

BillerManager.defaultProps = {};

export default BillerManager;
