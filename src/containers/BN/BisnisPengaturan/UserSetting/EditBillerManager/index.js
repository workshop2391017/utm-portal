// main
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles, Collapse } from "@material-ui/core";

// components
import Select from "components/BN/Select/SelectGroup";
import RadioGroup from "components/BN/Radio/RadioGroup";
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";
import TextField from "components/BN/TextField/AntdTextField";
import TagsGroup from "components/BN/Tags/TagsGroup";
import GeneralButton from "components/BN/Button/GeneralButton";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import Table from "components/BN/Table/TableScroll";
import Menu from "components/BN/Menus/MenuTableEditBillerManager";
import Tabs from "components/BN/Tabs/ButtonTabs";
import AddComment from "components/BN/Popups/AddComment";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Switch from "components/BN/Switch/GeneralSwitch";

const EditBillerManager = (props) => {
  const useStyles = makeStyles({
    edit: {},
    title: {
      width: "100%",
      height: 63,
      backgroundColor: "#fff",
      boxShadow: "inset 0px -1px 0px #E6EAF3",
      lineHeight: "63px",
      fontSize: 20,
      paddingLeft: 30,
      fontWeight: 700,
      position: "relative",
      fontFamily: "Futura",
    },
    paper: {
      width: "100%",
      minHeight: 500,
      border: "1px solid #BCC8E7",
      backgroundColor: "#fff",
      borderRadius: 8,
      padding: "20px 30px 203px",
      position: "relative",
    },
    subtitle: {
      width: "100%",
      color: "#AEB3C6",
      paddingBottom: 4,
      borderBottom: "1px solid #AEB3C6",
      fontFamily: "Futura",
      fontSize: 12,
      fontWeight: 700,
      marginBottom: 22,
    },
    input: {
      minHeight: 63,
      marginBottom: 24,
    },
    lastField: {
      border: "1px solid #BCC8E7",
      borderRadius: 8,
      width: "100%",
      minHeight: 420,
      padding: "30px 20px",
      display: "flex",
      position: "relative",
    },
    lastFieldButton: {
      position: "absolute",
      right: 20,
      bottom: 32,
    },
    button: {
      position: "absolute",
      bottom: 30,
    },
  });
  const classes = useStyles();
  const history = useHistory();

  // Detail Biller State
  const [category, setCategory] = useState("Donasi");
  const [paymentHandler, setPaymentHandler] = useState("Open");
  const [sumberDana, setSumberDana] = useState(["Credit Card"]);
  const [paymentType, setPaymentType] = useState("VA");
  const [namaBiller, setNamaBiller] = useState(null);
  const [kodePayee, setKodePayee] = useState(null);
  const [lengthMin, setLengthMin] = useState(null);
  const [lengthMax, setLengthMax] = useState(null);
  const [kodePrefix, setKodePrefix] = useState(null);
  const [kodePrefixTags, setKodePrefixTags] = useState([]);

  // Last Field State
  const [lineNumber, setLineNumber] = useState(null);
  const [namaFieldEn, setNamaFieldEn] = useState(null);
  const [placeholderEn, setPlaceholderEn] = useState(null);
  const [fieldType, setFieldType] = useState(null);
  const [namaFieldId, setNamaFieldId] = useState(null);
  const [placeholderId, setPlaceholderId] = useState(null);
  const [fieldkey, setFieldkey] = useState(null);
  const [dataField, setDataField] = useState(null);

  // Denomination State
  const [useDenom, setUserDenom] = useState(true);
  const [valueDenom, setValueDenom] = useState(null);
  const [deskripsiIdDenom, setDeskripsiIdDenom] = useState(null);
  const [deskripsiEnDenom, setDeskripsiEnDenom] = useState(null);
  const [dataDenomTable, setDataDenomTable] = useState([]);

  // List Field State
  const [listFieldTabs, setListFieldTabs] = useState(0);
  const [dataListField, setDataListField] = useState([]);

  const [commentModal, setCommentModal] = useState(false);
  const [successModal, setSuccessModal] = useState(false);

  const addKodePrefix = (e) => {
    e.preventDefault();
    if (kodePrefix) {
      setKodePrefixTags([
        ...kodePrefixTags,
        {
          key: kodePrefixTags[kodePrefixTags.length - 1].key + 1,
          label: kodePrefix,
        },
      ]);
      setKodePrefix(null);
    }
  };

  const handleClear = () => {
    setLineNumber(null);
    setNamaFieldEn(null);
    setPlaceholderEn(null);
    setFieldType(null);
    setNamaFieldId(null);
    setPlaceholderId(null);
    setFieldkey(null);
  };

  const handleSaveField = () => {
    setDataField({
      lineNumber,
      namaFieldEn,
      placeholderEn,
      fieldType,
      namaFieldId,
      placeholderId,
      fieldkey,
    });
  };

  const commentContinue = () => {
    setCommentModal(false);
    setSuccessModal(true);
  };

  const tableConfigDenom = [
    {
      title: "Value",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.value}</span>,
    },
    {
      title: "Deskripsi ID",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.descriptionId}</span>,
    },
    {
      title: "Deskripsi EN",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.descriptionEn}</span>,
    },
    {
      title: "",
      headerAlign: "right",
      align: "right",
      render: (rowData) => <Menu />,
    },
  ];
  const tableConfigListField = [
    {
      title: "Input Type",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.inputType}</span>,
    },
    {
      title: "Line Number",
      headerAlign: "center",
      align: "center",
      render: (rowData) => <span>{rowData.lineNumber}</span>,
    },
    {
      title: "Nama Field (ID)",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.namaFieldId}</span>,
    },
    {
      title: "Nama Field (EN)",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.namaFieldEn}</span>,
    },
    {
      title: "Placeholder (ID)",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.placeholderId}</span>,
    },
    {
      title: "Placeholder (EN)",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.placeholderEn}</span>,
    },
    {
      title: "Fieldkey",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.fieldkey}</span>,
    },
    {
      title: "Field Type",
      headerAlign: "left",
      align: "left",
      render: (rowData) => <span>{rowData.fieldType}</span>,
    },
    {
      title: "",
      headerAlign: "right",
      align: "right",
      render: (rowData) => <Menu status={0} />,
    },
  ];

  return (
    <div className={classes.edit}>
      <div className={classes.title}>Ubah Biller</div>
      <div style={{ padding: "38px 30px 66px" }}>
        <div className={classes.paper}>
          <div className={classes.subtitle}>Detail Biller</div>
          <div style={{ display: "flex", marginBottom: 44 }}>
            <div style={{ width: "50%", paddingRight: 30 }}>
              <div className={classes.input}>
                <p style={{ marginBottom: 3 }}>Pilih Kategori :</p>
                <Select
                  value={category}
                  onChange={(value) => setCategory(value)}
                  placeholder="Kategori"
                  options={[
                    {
                      name: "BILLPAY_INTERNET_TV",
                      options: ["Bizznet", "Indihome"],
                    },
                    {
                      name: "BILLPAY_DONASI",
                      options: ["Kitabisa1", "Kitabisa2", "Kitabisa3"],
                    },
                    {
                      name: "BILLPAY_LOAN",
                    },
                  ]}
                  style={{ width: "100%" }}
                />
              </div>
              <div className={classes.input}>
                <p style={{ marginBottom: 7 }}>Payment Handler :</p>
                <RadioGroup
                  value={paymentHandler}
                  onChange={(e) => setPaymentHandler(e.target.value)}
                  options={["Open", "Close"]}
                />
              </div>
              <div className={classes.input}>
                <p style={{ marginBottom: 7 }}>Sumber Dana :</p>
                <CheckboxGroup
                  value={sumberDana}
                  onChange={(value) => setSumberDana(value)}
                  options={["Credit Card", "Savings", "Current"]}
                />
              </div>
              <div className={classes.input}>
                <p style={{ marginBottom: 7 }}>Payment Type :</p>
                <RadioGroup
                  value={paymentType}
                  onChange={(e) => setPaymentType(e.target.value)}
                  options={["VA", "Payment"]}
                />
              </div>
            </div>
            <div style={{ width: "50%", paddingLeft: 30 }}>
              <div className={classes.input}>
                <p style={{ marginBottom: 3 }}>Nama Biller :</p>
                <TextField
                  value={namaBiller}
                  onChange={(e) => setNamaBiller(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
              <div className={classes.input}>
                <p style={{ marginBottom: 3 }}>Kode Payee :</p>
                <TextField
                  value={kodePayee}
                  onChange={(e) => setKodePayee(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="200192812"
                />
              </div>
              <div style={{ display: "flex" }}>
                <div
                  className={classes.input}
                  style={{ width: "50%", paddingRight: 15 }}
                >
                  <p style={{ marginBottom: 3 }}>Length Min :</p>
                  <TextField
                    value={lengthMin}
                    onChange={(e) => setLengthMin(e.target.value)}
                    style={{ width: "100%" }}
                    placeholder="30 Char"
                  />
                </div>
                <div
                  className={classes.input}
                  style={{ width: "50%", paddingLeft: 15 }}
                >
                  <p style={{ marginBottom: 3 }}>Length Max :</p>
                  <TextField
                    value={lengthMax}
                    onChange={(e) => setLengthMax(e.target.value)}
                    style={{ width: "100%" }}
                    placeholder="60 Char"
                  />
                </div>
              </div>
              <div className={classes.input} style={{ marginBottom: 0 }}>
                <p style={{ marginBottom: 3 }}>Kode Prefix :</p>
                <form onSubmit={addKodePrefix}>
                  <TextField
                    value={kodePrefix}
                    onChange={(e) => setKodePrefix(e.target.value)}
                    style={{ width: "100%" }}
                    placeholder="Tambahkan Kode Prefix...."
                  />
                </form>
              </div>
              <TagsGroup
                tags={kodePrefixTags}
                setTags={setKodePrefixTags}
                scroll
              />
            </div>
          </div>
          <div className={classes.subtitle}>Denomination</div>
          <div style={{ display: "flex" }}>
            <div style={{ width: "50%", paddingRight: 20 }}>
              <div style={{ marginBottom: 20 }}>
                <span>Gunakan Denomination ?</span>
                <span style={{ float: "right" }}>
                  <Switch
                    checked={useDenom}
                    onChange={(e) => setUserDenom(e.target.checked)}
                  />
                </span>
              </div>
              <Collapse in={useDenom}>
                <div className={classes.input} style={{ marginBottom: 10 }}>
                  <p style={{ marginBottom: 3 }}>Value :</p>
                  <TextField
                    value={valueDenom}
                    onChange={(e) => setValueDenom(e.target.value)}
                    style={{ width: "100%" }}
                    placeholder="EntertainEster"
                  />
                </div>
                <div className={classes.input} style={{ marginBottom: 10 }}>
                  <p style={{ marginBottom: 3 }}>Deskripsi ID :</p>
                  <TextField
                    value={deskripsiIdDenom}
                    onChange={(e) => setDeskripsiIdDenom(e.target.value)}
                    style={{ width: "100%" }}
                    placeholder="EntertainEster"
                  />
                </div>
                <div className={classes.input} style={{ marginBottom: 10 }}>
                  <p style={{ marginBottom: 3 }}>Deskripsi EN :</p>
                  <TextField
                    value={deskripsiEnDenom}
                    onChange={(e) => setDeskripsiEnDenom(e.target.value)}
                    style={{ width: "100%" }}
                    placeholder="EntertainEster"
                  />
                </div>
              </Collapse>
            </div>
            <div style={{ width: "50%", paddingTop: 36, marginBottom: 20 }}>
              <Collapse in={useDenom}>
                <Table cols={tableConfigDenom} data={dataDenomTable} />
              </Collapse>
            </div>
          </div>
          <div className={classes.subtitle}>List Field</div>
          <div>
            <Tabs
              value={listFieldTabs}
              onChange={(event, newValue) => setListFieldTabs(newValue)}
              tabs={["Input", "Confirmation", "Receipt"]}
            />
          </div>
          <div style={{ margin: "20px 0" }}>
            <Table cols={tableConfigListField} data={dataListField} />
          </div>
          <div className={classes.lastField}>
            <div style={{ width: "50%", paddingRight: 30 }}>
              <div className={classes.input} style={{ marginBottom: 20 }}>
                <p style={{ marginBottom: 3 }}>Line Number :</p>
                <TextField
                  value={lineNumber}
                  onChange={(e) => setLineNumber(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
              <div className={classes.input} style={{ marginBottom: 20 }}>
                <p style={{ marginBottom: 3 }}>Nama Field (EN) :</p>
                <TextField
                  value={namaFieldEn}
                  onChange={(e) => setNamaFieldEn(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
              <div className={classes.input} style={{ marginBottom: 20 }}>
                <p style={{ marginBottom: 3 }}>Placeholder (EN) :</p>
                <TextField
                  value={placeholderEn}
                  onChange={(e) => setPlaceholderEn(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
              <div className={classes.input} style={{ marginBottom: 0 }}>
                <p style={{ marginBottom: 3 }}>Field Type :</p>
                <TextField
                  value={fieldType}
                  onChange={(e) => setFieldType(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
            </div>
            <div style={{ width: "50%", paddingLeft: 30 }}>
              <div className={classes.input} style={{ marginBottom: 20 }}>
                <p style={{ marginBottom: 3 }}>Nama Field (ID) :</p>
                <TextField
                  value={namaFieldId}
                  onChange={(e) => setNamaFieldId(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
              <div className={classes.input} style={{ marginBottom: 20 }}>
                <p style={{ marginBottom: 3 }}>Placeholder (ID) :</p>
                <TextField
                  value={placeholderId}
                  onChange={(e) => setPlaceholderId(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
              <div className={classes.input} style={{ marginBottom: 0 }}>
                <p style={{ marginBottom: 3 }}>Fieldkey :</p>
                <TextField
                  value={fieldkey}
                  onChange={(e) => setFieldkey(e.target.value)}
                  style={{ width: "100%" }}
                  placeholder="EntertainEster"
                />
              </div>
            </div>
            <div className={classes.lastFieldButton}>
              <ButtonOutlined
                label="Clear"
                width="77px"
                height="33px"
                style={{ marginRight: 20 }}
                onClick={handleClear}
              />
              <GeneralButton
                label="Simpan Field"
                width="132px"
                height="33px"
                onClick={handleSaveField}
              />
            </div>
          </div>
          <div className={classes.button} style={{ left: 30 }}>
            <ButtonOutlined
              label="Batal"
              width="77px"
              height="40px"
              onClick={() => history.goBack()}
            />
          </div>
          <div className={classes.button} style={{ right: 30 }}>
            <GeneralButton
              label="Simpan"
              width="92px"
              height="40px"
              onClick={() => setCommentModal(true)}
            />
          </div>
        </div>
      </div>
      <AddComment
        isOpen={commentModal}
        handleClose={() => setCommentModal(false)}
        onContinue={commentContinue}
      />
      <SuccessConfirmation
        isOpen={successModal}
        handleClose={() => setSuccessModal(false)}
      />
    </div>
  );
};

EditBillerManager.propTypes = {};

EditBillerManager.defaultProps = {};

export default EditBillerManager;
