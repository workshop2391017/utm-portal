import { makeStyles } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import Colors from "helpers/colors";
import React from "react";
import { ReactComponent as AddIcon } from "assets/icons/BN/plus-white.svg";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
  container: {
    width: "100%",
    height: "500px",
    padding: 20,
    backgroundColor: Colors.white,
    borderRadius: 10,

    "& .title": {
      fontFamily: "futuraHvBT",
      fontSize: 20,
      color: Colors.dark.hard,
    },
    "& .message": {
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      textAlign: "center",
    },
    "& .submessage": {
      color: Colors.dark.medium,
      fontFamily: "FuturaBkBT",
      fontSize: 13,
      marginBottom: 30,
    },
  },
  titleContainer: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    height: 400,
  },
});

const NoDataPersetujuanMatrixAdminBank = ({ onAdd, disabled, label }) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className="title">{label}</div>
      <div className={classes.titleContainer}>
        <div className="message">No Approval List</div>
        <div className="submessage">Add list to set matrix approval</div>
        <GeneralButton
          label="Add List"
          width={178}
          iconPosition="startIcon"
          buttonIcon={<AddIcon />}
          onClick={onAdd}
          disabled={disabled}
        />
      </div>
    </div>
  );
};

export default NoDataPersetujuanMatrixAdminBank;
