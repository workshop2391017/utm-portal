import { Button, makeStyles } from "@material-ui/core";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import MenuListWithoutCollapse from "components/BN/MenuList/MenuListWithoutCollapse";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import { pathnameCONFIG } from "configuration";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right.svg";
import Colors from "helpers/colors";
import {
  ADD_MATRIX_APPROVAL,
  EDIT_MATRIX_APPROVAL,
  handleApprovalMatrixAllMenu,
  handleApprovalMatrixMenu,
  handleApprovalMatrixSquence,
  matrixApprovalDelete,
  setAllLayanan,
  setClear,
  setMatrixApprovalActionInsert,
  setMatrixDetailList,
  setSelectedKeys,
  setSelectedMenu,
} from "stores/actions/matrixApproval";
import Toast from "components/BN/Toats";
import { setClearError } from "stores/actions/pengaturanHakAkses";
import editIcon from "../../../../assets/icons/BN/edit-blue.svg";
import trashIcon from "../../../../assets/icons/BN/trash-blue.svg";
import PopupJenisRekening from "./popupJenisLayanan";
import NoDataPersetujuanMatrixAdminBank from "./noData";

const Title = styled.div`
  color: #374062;
  font-family: "FuturaHvBT";
  font-size: ${(props) => (props.sidebar ? "15px" : "20px")};
  font-weight: 400;
`;

const useStyles = makeStyles({
  page: {
    padding: "20px",
    backgroundColor: "#F4F7FB",
  },
  container: {
    marginBottom: "46px",
  },
  row: {
    display: "flex",
    flexDirection: "row",
  },
  ItemSide: {
    width: "282px",
    marginRight: "20px",
  },
  itemRight: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardTop: {
    width: "100%",
    height: "154px",
    backgroundColor: "#fff",
    marginBottom: "20px",
    borderRadius: 10,
  },
  cardBottom: {
    height: "auto",
    width: "100%",
    padding: "20px",
    backgroundColor: "#fff",
    position: "relative",
    borderRadius: 10,
  },
  badge: {
    border: "1px solid #B3C1E7",
    height: "auto",
    width: "100%",
    borderRadius: "10px",
  },
  containerCardTop: {
    margin: "20px",
  },
  button: {
    marginTop: "58px",
    display: "flex",
    justifyContent: "space-between",
  },
  detailCard: {
    backgroundColor: "#fff",
    borderRadius: 10,
    height: 154,
    width: "100%",
    padding: "24px 20px",
    position: "relative",
    marginBottom: 20,
    "& .title": {
      fontSize: 20,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.hard,
      marginBottom: 10,
    },
    "& .subtitle": {
      fontSize: 13,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.medium,
      marginBottom: 10,
    },
    "& .lihatsemua": {
      display: "flex",
      alignItems: "center",
      fontSize: 13,
      fontFamily: "FuturaMdBT",
      color: Colors.primary.hard,
      fontWeight: 700,
      "&:hover": {
        cursor: "pointer",
      },
    },
  },
  menuSelected: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    marginBottom: 20,
  },
});

const dataHeader = [
  {
    title: "Seq",
    width: 100,
    key: "seq",
    render: (e, i) => i + 1,
  },
  {
    title: "User",
    key: "pengguna",
    width: 200,
  },
  {
    title: "Group",
    key: "kelompok",
    render: (e) => (
      <div style={{ textTransform: "capitalize" }}>{e.kelompok}</div>
    ),
  },
  {
    title: "Target Group",
    key: "targetKelompok",
    render: (e) => e?.targetKelompokName || "-",
  },
];

const SemuaHeaderComponent = ({ classes, isShow }) => {
  const [openModal, setOpenModal] = useState(false);
  const { matrixSquence, selectedMenu, isLoading, allLayananMenu } =
    useSelector((e) => e.matrixApproval);

  return isShow ? (
    <React.Fragment>
      <PopupJenisRekening
        open={openModal}
        handleCloseModal={() => setOpenModal(false)}
      />
      <div className={classes.detailCard}>
        <div className="title">{selectedMenu.label}</div>
        <div className="subtitle">Type of menu set :</div>
        <div className="value">{allLayananMenu?.length} Menu</div>
        <div className="lihatsemua">
          <Button
            style={{ all: "unset", position: "relative" }}
            onClick={() => setOpenModal(true)}
          >
            View All Menu Types
          </Button>
          <ChevronRight />
        </div>
        {/* <img
          src={''}
          alt="pattern"
          style={{ position: "absolute", right: 0, top: 0 }}
        /> */}
      </div>
    </React.Fragment>
  ) : null;
};

const PersetujuanMatrixAdminBank = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [successConf, setSuccessConf] = useState(false);
  const [cancelPopup, setCancelPopup] = useState(false);
  const [search, setSearch] = useState("");
  const [menuOpt, setMenuOpt] = useState([]);

  const {
    menuList,
    matrixSquence,
    selectedMenu,
    isLoading,
    isSuccessDelete,
    isLoadingDelete,
    isLoadingSquence,
    allLayananMenu,
    error,
    selectedKeys,
  } = useSelector((e) => e.matrixApproval);

  const ifEditWithChild = allLayananMenu?.filter((e) => e.checked)?.length > 0;

  const cekSemua = () => selectedMenu?.label?.includes("All");

  const find = ({ children = [], ...object }, search) => {
    let result;
    if (object?.label === search) return object;

    if (children)
      return (
        // eslint-disable-next-line no-return-assign
        children.some((o) => (result = find(o, search))) && {
          ...object,
          children: [result],
        }
      );

    return null;
  };

  useEffect(() => {
    dispatch(setMatrixDetailList([]));
    const payload = {
      group: "ADMIN",
      menuTypeId: null,
      searchBy: null,
      isApprovalMatrixAdmin: false,
      masterMenuIdList: null,
      parent_id: null,
      isApprovalMatrixMenu: true,
      isAllServiceMenu: true,
    };

    dispatch(handleApprovalMatrixMenu(payload));
  }, []);

  useEffect(() => {
    if (selectedMenu?.parent_id && cekSemua()) {
      const paylaod = {
        group: "ADMIN",
        menuTypeId: null,
        searchBy: null,
        isApprovalMatrixAdmin: false,
        masterMenuIdList: null,
        parent_id: selectedMenu?.parent_id,
        isAllServiceMenu: true,
      };
      dispatch(handleApprovalMatrixAllMenu(paylaod));
    } else dispatch(setAllLayanan([]));
  }, [selectedMenu, cekSemua()]);

  useEffect(() => {
    if (selectedMenu.id) {
      const paylaod = {
        id: selectedMenu.id,
      };
      dispatch(handleApprovalMatrixSquence(paylaod));
    }
  }, [selectedMenu.id]);

  useEffect(() => {
    setMenuOpt(menuList);
  }, [menuList]);

  const filter = (array, text) => {
    const getChildren = (result, object) => {
      if (object?.label?.toLowerCase()?.includes(text?.toLowerCase())) {
        result.push(object);
        return result;
      }
      if (Array.isArray(object.children)) {
        const children = object.children.reduce(getChildren, []);
        if (children.length) result.push({ ...object, children });
      }
      return result;
    };

    return array.reduce(getChildren, []);
  };

  useEffect(() => {
    const res = filter(menuList, search);
    setMenuOpt(res);
  }, [search]);

  const handleSelectedMenu = (e) => {
    let selMenu = {};
    let parentKey = "";
    menuOpt.forEach((fe) => {
      if (fe.children.find((f) => f.key === e.key)) {
        parentKey = fe?.key;
        selMenu = fe.children.find((f) => f.key === e.key);
      }
    });

    dispatch(setSelectedMenu(selMenu));
    dispatch(setSelectedKeys([parentKey, selMenu.key]));
  };

  useEffect(() => {
    if (menuOpt[0]?.children[0]) {
      if (!history.location.state?.prevPath?.includes("nasabah")) {
        handleSelectedMenu(menuOpt[0]?.children[0]);
        dispatch(
          setSelectedKeys([menuOpt[0]?.key, menuOpt[0]?.children[0]?.key])
        );
      }
    }
  }, [menuOpt]);

  const handleDelete = async () => {
    dispatch(matrixApprovalDelete({ menuType: "CUSTOMER" }, ifEditWithChild));
  };

  useEffect(() => {
    if (isSuccessDelete) {
      setSuccessConf(true);
      setCancelPopup(false);
    }
  }, [isSuccessDelete]);

  return (
    <div className={classes.page}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setClearError())}
      />
      <div className={classes.container}>
        <Title>Customer Matrix Approval</Title>
      </div>

      <div className={classes.row}>
        <div className={classes.ItemSide}>
          <MenuListWithoutCollapse
            selectedKeys={selectedKeys}
            loading={isLoading}
            height={500}
            headerTitle="Menu Type "
            menuOptions={menuOpt}
            search={search}
            setSearch={(e) => setSearch(e)}
            onClick={handleSelectedMenu}
          />
        </div>
        <div className={classes.itemRight}>
          {!Object.keys(matrixSquence ?? {}).length ? (
            <NoDataPersetujuanMatrixAdminBank
              label={selectedMenu?.label}
              onAdd={() => {
                dispatch(setMatrixApprovalActionInsert(ADD_MATRIX_APPROVAL));
                history.push(pathnameCONFIG.PERSETUJUAN_MATRIX.NASABAH.INSERT);
              }}
              disabled={!selectedMenu?.id || isLoading || isLoadingSquence}
            />
          ) : (
            <Fragment>
              <SemuaHeaderComponent isShow={cekSemua()} classes={classes} />

              <div className={classes.cardBottom}>
                <div className={classes.menuSelected}>
                  {!cekSemua() ? selectedMenu?.label : null}
                </div>
                <div className={classes.badge}>
                  <div
                    style={{
                      margin: "23px 20px",
                    }}
                  >
                    <Title>
                      {(matrixSquence?.squence ?? []).length} Number of
                      Approvals
                    </Title>
                  </div>

                  <TableMenuBaru
                    headerContent={dataHeader}
                    dataContent={matrixSquence?.squence ?? []}
                    isLoading={isLoadingSquence}
                  />
                </div>
                <div className={classes.button}>
                  <ButtonOutlined
                    label="Delete"
                    iconPosition="startIcon"
                    buttonIcon={<img src={trashIcon} alt="hapus" />}
                    onClick={() => {
                      setCancelPopup(true);
                    }}
                  />
                  <GeneralButton
                    style={{
                      width: "158px",
                    }}
                    iconPosition="startIcon"
                    buttonIcon={<img src={editIcon} alt="edit" />}
                    onClick={() => {
                      dispatch(
                        setMatrixApprovalActionInsert(EDIT_MATRIX_APPROVAL)
                      );
                      history.push(
                        pathnameCONFIG.PERSETUJUAN_MATRIX.NASABAH.INSERT
                      );
                    }}
                    label="Edit"
                  />
                </div>
              </div>
            </Fragment>
          )}
        </div>
      </div>

      <DeletePopup
        isOpen={cancelPopup}
        handleClose={() => setCancelPopup(false)}
        onContinue={handleDelete}
        message="Are You Sure To Delete the Data?"
        submessage="You can't undo this action"
        loading={isLoadingDelete}
      />
      <SuccessConfirmation
        message="Deleted Successfully"
        isOpen={successConf}
        handleClose={() => {
          setSuccessConf(false);
          setCancelPopup(false);
          dispatch(setClear());
        }}
      />
    </div>
  );
};

export default PersetujuanMatrixAdminBank;
