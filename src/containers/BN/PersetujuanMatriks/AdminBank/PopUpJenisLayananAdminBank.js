import { Backdrop, Fade, Modal } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import BasicPopUp from "components/SC/BasicPopUp";
import React, { useEffect, useState } from "react";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import ListCheckBox from "components/BN/List/ListCheckBox";
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import Badge from "components/BN/Badge";
import NoteAlert from "components/BN/Alert/NoteAlert";
import GeneralALert from "components/BN/Alert/GeneralAlert";
import { ReactComponent as InfoAlert } from "assets/icons/BN/megaphone.svg";
import ScrollCustom from "components/BN/ScrollCustom";
import { useDispatch, useSelector } from "react-redux";
import { setAllLayanan } from "stores/actions/matrixApproval";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  modaltitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 20,
    fontWeight: 700,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  headerModal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  content: {},
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    // paddingTop: 10,
  },
  paper: {
    width: 548,
    height: 449,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    height: 88,
    marginLeft: 20,
    marginRight: 20,
    alignItems: "center",
  },
  checkbox: {
    display: "flex",
    justifyContent: "space-between",
    padding: "8px",
    marginLeft: 15,
  },
  allCheckBox: {
    marginTop: 37,
    marginBottom: 8,
  },
  alert: {
    marginTop: 31,
    marginBottom: 19,
  },
}));

const PopupJenisLayananAdminBank = ({ handleCloseModal, open }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { allLayananMenu: allMenu, selectedMenu } = useSelector(
    (e) => e.matrixApproval
  );

  const allLayananMenu = allMenu || [];
  const handleChange = (i) => {
    const arr = [...allLayananMenu];
    arr[i] = {
      ...arr[i],
      checked: !arr[i].checked,
    };

    dispatch(setAllLayanan(arr));
  };

  const handleChangeAll = () => {
    const arr = [...allLayananMenu];

    const filterCheck = arr.findIndex((elm) => elm.checked);

    arr
      .filter((f) => f.status !== "WAITING_FOR_APPROVAL")
      .map((elm) => {
        elm.checked = !(filterCheck > -1);
        return elm;
      });

    dispatch(setAllLayanan(arr));
  };

  const checkAll = () => {
    const arr = [...allLayananMenu];

    return arr.length === arr.filter((e) => e.checked).length;
  };

  const status = (status) => {
    const obj = {
      message: "",
      type: "",
    };

    switch (status) {
      case "WAITING_FOR_APPROVAL":
        obj.message = "Waiting Approval";
        obj.type = "gray";
        break;
      case "DONE":
        obj.message = "Done";
        obj.type = "blue";
        break;
      default:
        break;
    }

    return { ...obj };
  };

  return (
    <Modal
      className={classes.modal}
      open={open}
      onClose={handleCloseModal}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 900,
      }}
    >
      <Fade in={open}>
        <div className={classes.paper}>
          <div className={classes.headerModal}>
            <div className={classes.modaltitle}>Menu Types</div>
            <div className={classes.xContainer}>
              <XIcon className="close" onClick={() => handleCloseModal(true)} />
            </div>
          </div>
          <div className={classes.content}>
            <div className={classes.allCheckBox}>
              <CheckboxSingle
                label={selectedMenu?.label}
                onChange={handleChangeAll}
                checked={checkAll()}
                indeterminate={
                  allLayananMenu.filter((a) => a.checked).length && !checkAll()
                }
              />
            </div>
            <ScrollCustom height={220}>
              {(allLayananMenu ?? []).map((elm, index) => (
                <div className={classes.checkbox} key={index}>
                  <CheckboxSingle
                    label={elm.menuNameEng}
                    disabled={elm.status === "WAITING_FOR_APPROVAL"}
                    onChange={() => handleChange(index)}
                    checked={elm.checked}
                  />
                  {elm.status ? (
                    <Badge
                      label={status(elm?.status)?.message}
                      outline
                      type={status(elm?.status)?.type}
                    />
                  ) : null}
                </div>
              ))}
            </ScrollCustom>
          </div>
          {/* <div className={classes.alert}>
            <NoteAlert
              text="Semua limit yang sudah diatur akan otomatis terubah apabila diatur kembali disini."
              icon={<InfoAlert />}
            />
          </div> */}
          <div className={classes.buttonGroup}>
            <ButtonOutlined label="Cancel" onClick={handleCloseModal} />
            <GeneralButton label="Save" onClick={handleCloseModal} />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default PopupJenisLayananAdminBank;
