import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import AntdTextField from "components/BN/TextField/AntdTextField";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import illustrationX from "assets/images/BN/illustrationred.png";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_MATRIX_APPROVAL,
  EDIT_MATRIX_APPROVAL,
  matrixApprovalSave,
  matrixApprovalTargetGrp,
  setClear,
  setClearError,
} from "stores/actions/matrixApproval";
import Colors from "helpers/colors";
import useDebounce from "utils/helpers/useDebounce";
import Toast from "components/BN/Toats";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import SelectGroup from "components/BN/Select/SelectWithSearch";
import iconTrash from "assets/icons/BN/trash-2.svg";
import { pathnameCONFIG } from "configuration";

const Title = styled.div`
  color: #374062;
  font-size: 20px;
  font-weight: 400;
  font-family: "FuturaHvBT";
`;

const useStyles = makeStyles({
  page: {
    padding: "20px",
    backgroundColor: "#F4F7FB",
    position: "relative",
    height: "100%",
  },
  container: {
    marginTop: "13px",
    marginBottom: "33px",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  cardTop: {
    borderRadius: "10px",
    height: "74px",
    backgroundColor: "#fff",
    padding: "25px 20px",
  },
  cardBottom: {
    marginTop: "20px",
    borderRadius: "10px",
    backgroundColor: "#fff",
    minHeight: "541px",
    padding: "40px 20px",
  },
  badge: {
    border: "solid 1px #B3C1E7",
    borderRadius: "10px",
    overflow: "hidden",
    "& .card": {
      margin: "20px",
    },
    "& .card-title": {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      marginBottom: "5px",
    },
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    padding: "22px",
    backgroundColor: "#fff",
  },
  errorSeqNum: {
    color: Colors.warning.hard,
    margin: "20px 20px 0 20px",
  },
});

const dataHeader = ({
  group,
  handleDelete,
  handleChange,
  targetGroup,
  selectedMenu,
}) => [
  {
    title: "Seq",
    width: 100,
    key: "seq",
    // render: (c, i) => i + 1,
  },
  {
    title: "User",
    width: 250,
    key: "pengguna",
    render: (rowData, i) => (
      <AntdTextField
        style={{ width: 80 }}
        width={80}
        value={rowData?.pengguna}
        onChange={handleChange("pengguna", i)}
      />
    ),
  },
  {
    title: "Group",
    key: "kelompok",
    render: (rowData, i) => (
      <SelectGroup
        placeholder="Select Group"
        value={rowData?.kelompok}
        options={
          ["Registration", "Change Authorization"].includes(
            selectedMenu?.label?.trim()
          )
            ? group?.filter(({ value }) => value === "GROUP_SPECIFIC")
            : group
        }
        onSelect={handleChange("kelompok", i)}
      />
    ),
  },
  {
    title: "Target Group",
    key: "targetKelompok",
    render: (rowData, i) => (
      <SelectGroup
        onSelect={handleChange("targetKelompok", i)}
        value={rowData?.targetKelompok || null}
        disabled={rowData?.kelompok !== "GROUP_SPECIFIC"}
        placeholder="Select Target Group"
        options={(targetGroup?.portalGroupList ?? []).map((elm) => ({
          value: elm?.id,
          label: elm?.name,
          ...elm,
        }))}
      />
    ),
  },
  {
    title: "",
    width: 150,
    render: (rowData, index) => (
      <button
        style={{
          border: "none",
          backgroundColor: "#fff",
          display: "flex",
          width: "100%",
          cursor: "pointer",
        }}
        onClick={() => handleDelete(index)}
      >
        <img src={iconTrash} alt="trash" />
      </button>
    ),
  },
];

const EditAdminBank = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [successConf, setSuccessConf] = useState(false);
  const [cancelPopup, setCancelPopup] = useState(false);
  const [noOfSquence, setNoOfSquence] = useState(1);
  const [exceedsUsers, setExceedsUsers] = useState(false);
  const [squenceList, setSquenceList] = useState([
    {
      seq: 1,
      pengguna: 1,
      kelompok: null,
      targetKelompok: "",
    },
  ]);
  const {
    group,
    targetGroup,
    selectedMenu,
    actionAdmin,
    isLoadingSave,
    isSuccessSave,
    matrixSquence,
    error,
    allLayananMenu,
  } = useSelector((e) => e.matrixApproval);
  const clickKembali = () =>
    history.push(pathnameCONFIG.PERSETUJUAN_MATRIX.ADMIN_BANK.LIST, {
      prevPath: history.location.pathname,
    });

  const ifEditWithChild = allLayananMenu.filter((e) => e.checked)?.length > 0;

  useEffect(() => {
    if (!selectedMenu?.id) clickKembali();
  }, [selectedMenu]);

  useEffect(() => {
    dispatch(
      matrixApprovalTargetGrp({
        officeType: null,
      })
    );
  }, []);

  const seq = useDebounce(noOfSquence, 1100);

  useEffect(() => {
    if (seq) {
      const currArr = [...squenceList];
      const newArr = [];

      if (seq < currArr.length) {
        setSquenceList(currArr.slice(0, seq));
      } else {
        for (let i = 0; i < seq - currArr.length; i++) {
          newArr.push({
            // eslint-disable-next-line no-plusplus
            seq: currArr.length + i + 1,
            pengguna: 1,
            kelompok: null,
            targetKelompok: "",
          });
        }

        setSquenceList([...currArr, ...newArr]);
      }
    }
  }, [seq]);

  const handleAddNew = () => {
    const arr = [...squenceList];
    arr.push({
      seq: arr.length + 1,
      pengguna: 1,
      kelompok: "",
      targetKelompok: "",
    });
    setNoOfSquence((a) => a + 1);
    setSquenceList(arr);
  };

  const handleDelete = (index) => {
    const arr = [...squenceList];

    arr.splice(index, 1);
    setNoOfSquence((e) => e - 1);
    setSquenceList(arr.map((elm, i) => ({ ...elm, seq: i + 1 })));
  };

  const handleChange = (name, i) => (e) => {
    const value = e.target ? e.target.value : e;
    const arr = [...squenceList];

    arr[i] = {
      ...arr[i],
      [name]: value,
      ...(name === "kelompok" &&
        value !== "GROUP_SPECIFIC" && {
          targetKelompok: null,
          portalGroupId: null,
        }),
    };

    setSquenceList(arr);
  };

  useEffect(() => {
    if (matrixSquence && actionAdmin === EDIT_MATRIX_APPROVAL) {
      setNoOfSquence(matrixSquence?.squence?.length);
      setSquenceList(
        (matrixSquence?.squence ?? []).map((elm, i) => ({ ...elm, seq: i + 1 }))
      );
    }
  }, [matrixSquence]);

  const handleSave = () => {
    const noOfSquence = squenceList?.length ?? 1;
    dispatch(
      matrixApprovalSave(
        { squenceList, noOfSquence, menuType: "ADMIN" },
        ifEditWithChild
      )
    );
  };

  useEffect(() => {
    if (isSuccessSave) {
      setSuccessConf(true);
      setCancelPopup(false);
    }
  }, [isSuccessSave]);

  const handleDisable = () =>
    squenceList.findIndex(
      (e) =>
        !e.seq ||
        !e.pengguna ||
        !e.kelompok ||
        (e.kelompok === "GROUP_SPECIFIC"
          ? !e.targetKelompok && !e.portalGroupId
          : false)
    ) >= 0 || !squenceList.length;

  return (
    <div>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setClearError())}
      />
      <div className={classes.page}>
        <div className={classes.container}>
          <button
            onClick={clickKembali}
            style={{
              border: "none",
              backgroundColor: "#F4F7FB",
            }}
          >
            <img
              src={arrowLeft}
              style={{
                marginTop: "-37px",
              }}
              alt="Kembali"
            />
          </button>
          <Title>{actionAdmin} Bank Admin Matrix Approval</Title>
        </div>
        <div className={classes.column}>
          <div className={classes.cardTop}>
            <Title>{selectedMenu?.label}</Title>
          </div>
          <div className={classes.cardBottom}>
            <div className={classes.badge}>
              <div className="card">
                <div className="card-title">No of approval :</div>
                <AntdTextField
                  value={noOfSquence}
                  type="number"
                  onChange={(e) => setNoOfSquence(Number(e.target.value))}
                  style={{
                    width: "134px",
                  }}
                />
              </div>

              <TableMenuBaru
                headerContent={dataHeader({
                  group,
                  handleDelete,
                  handleChange,
                  targetGroup,
                  selectedMenu,
                })}
                dataContent={squenceList ?? []}
              />

              <GeneralButton
                style={{
                  margin: "20px",
                  width: "113px",
                  fontFamily: "FuturaMdBT",
                  fontWeight: 700,
                }}
                onClick={handleAddNew}
                label="Add New"
              />
            </div>
          </div>
        </div>

        <DeletePopup
          isOpen={cancelPopup}
          handleClose={() => setCancelPopup(false)}
          onContinue={() => handleSave()}
          message={
            actionAdmin === ADD_MATRIX_APPROVAL
              ? "Are You Sure To Add Data?"
              : "Are You Sure To Edit Your Data?"
          }
          submessage="You can't undo this action"
          loading={isLoadingSave}
        />
        <SuccessConfirmation
          titlefontSize={28}
          isOpen={successConf}
          handleClose={() => {
            history.push(pathnameCONFIG.PERSETUJUAN_MATRIX.ADMIN_BANK.LIST, {
              prevPath: history.location.pathname,
            });
            setSuccessConf(false);
            setCancelPopup(false);
            dispatch(setClear());
          }}
        />
        <SuccessConfirmation
          height={510}
          title=""
          message="Number of Approvals Exceeds Total Users"
          submessage={
            <div style={{ textAlign: "center" }}>
              The number of approvals entered cannot be more than the total
              number of users of your company&apos;s approvers.
              <br />
              <br />
              The number of approvers you have is 8 users.
            </div>
          }
          isOpen={exceedsUsers}
          img={illustrationX}
          handleClose={() => {
            setExceedsUsers(false);
          }}
        />
      </div>
      <div className={classes.button}>
        <ButtonOutlined
          label="Cancel"
          onClick={() => history.goBack()}
          disabled={isLoadingSave}
        />
        <GeneralButton
          style={{
            width: "150px",
            fontFamily: "FuturaMdBT",
            fontWeight: 700,
          }}
          onClick={() => setCancelPopup(true)}
          label="Save"
          disabled={isLoadingSave || handleDisable()}
        />
      </div>
    </div>
  );
};

export default EditAdminBank;
