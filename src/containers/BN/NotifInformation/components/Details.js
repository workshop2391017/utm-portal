// Details page
import React, { useEffect, useState } from "react";
import { Button, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import TextField from "components/BN/TextField/AntdTextField";

// redux
import { useDispatch, useSelector } from "react-redux";
import {
  handleGetNotifInformationDetail,
  setData,
} from "stores/actions/nofitInformation";

// asset
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import moment from "moment";
import NIDetailTemplate from "./DetailsTemplate";

const useStyles = makeStyles({
  container: {
    margin: "20px",
    background: "#FFFFFF",
    borderRadius: "10px",
    padding: "20px",
  },
  paperWrapper: {
    margin: "20px",
    background: "#FFFFFF",
    borderRadius: "10px",
    padding: "20px",
    display: "flex",
    flexDirection: "row",
    gap: "24px",
  },
  cardHeader: {
    display: "flex",
    justifyContent: "space-between",
    height: "60px",
    alignItems: "center",
  },
  span: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    color: "#374062",
    fontSize: 13,
    letterSpacing: "0.01em",
  },
  title: {
    fontSize: "20px",
    fontFamily: "FuturaHvBT",
    color: "#2b2f3c",
    lineHeight: "24px",
    fontWeight: 400,
    letterSpacing: "0.03em",
  },
  head: {
    padding: "13px 20px",
  },
  actionText: {
    fontFamily: "FuturaHvBT",
    fontSize: "20px",
    color: "#374062",
    letterSpacing: "0.03em",
  },
  contentWrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    gap: "10px",
  },
  paper: {
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    borderRadius: 10,
    padding: 20,
    margin: 20,
  },
  cardWrapper: {
    display: "flex",
    flexDirection: "column",
    gap: "20px",
    width: "100%",
    border: "1px solid #EAF2FF",
    borderRadius: "10px",
    height: "100%",
  },
  headerWrapper: {
    display: "flex",
    alignItems: "center",
    height: "37px",
    background: "#0061A7",
    borderRadius: "10px 10px 0px 0px",
    fontFamily: "FuturaMdBT",
    fontWeight: "400",
    fontSize: "17px",
    lineHeight: "20px",
    color: "white",
    paddingLeft: "20px",
  },
  wrapperBadge: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  detailTitle: {
    fontFamily: "FuturaBkBT",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
    letterSpacing: "0.03em",
  },
  detailValue: {
    fontFamily: "FuturaMdBT",
    fontWeight: "400",
    fontSize: "15px",
    lineHeight: "24px",
    color: "#374062",
    letterSpacing: "0.01em",
  },
  detailWrapper: {
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    padding: "0px 40px 20px 40px",
    height: "100%",
  },
  fontWrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
});

const tableConfig = [
  {
    title: "Activity Log",
    headerAlign: "left",
    key: "approverName",
  },
  {
    title: "",
    headerAlign: "left",
    key: "dateExecute",
  },
  {
    title: "",
    headerAlign: "left",
    align: "left",
    fontFamily: "FuturaBkBT",
    width: 100,
    render: (rowData) => {
      if (rowData.action === "APPROVED") {
        return <Badge label="Approved" outline type="green" />;
      }
      return <Badge label="Rejected" outline type="red" />;
    },
  },
];

const dataActivityLog = [
  {
    user: "Malvin Virdaus",
    activityName: "Edit Approval Matrix",
    activityId: 158552,
    activityDate: 1664330365000,
    status: 0,
  },
  {
    user: "Nasywa Rana",
    activityName: "Edit Approval Matrix",
    activityId: 158552,
    activityDate: 1664330365000,
    status: 0,
  },
  {
    user: "Nasywa Rana",
    activityName: "Edit Approval Matrix",
    activityId: 158552,
    activityDate: 1664330365000,
    status: 0,
  },
  {
    user: "Nasywa Rana",
    activityName: "Edit Approval Matrix",
    activityId: 158552,
    activityDate: 1664330365000,
    status: 0,
  },
];

export const NorifInformationContentPaper = ({ title, children }) => {
  const classes = useStyles();

  return (
    <div className={classes.cardWrapper}>
      <div className={classes.headerWrapper}>{title}</div>
      <div className={classes.detailWrapper}>
        <div className={classes.fontWrapper}>{children}</div>
      </div>
    </div>
  );
};

const Details = ({ setActivePage }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { notificationPortalId, detail } = useSelector(
    (e) => e.notifInformation
  );

  const [comment, setComment] = useState(null);

  const fetchingNotif = () => {
    dispatch(handleGetNotifInformationDetail({ notificationPortalId }));
  };

  useEffect(() => {
    if (notificationPortalId) fetchingNotif();
  }, [notificationPortalId]);

  return (
    <React.Fragment>
      {/* Title & Back Button */}
      <div className={classes.head}>
        <Button
          startIcon={<img src={arrowLeft} alt="Kembali" />}
          onClick={() => {
            setActivePage(0);
            dispatch(setData(null));
          }}
          style={{}}
        />
        <div className={classes.title}>Notif Information</div>
      </div>

      {/* Header Detail */}
      <div
        className={classes.container}
        style={{ padding: "20px 0px 20px 20px" }}
      >
        <div className={classes.cardHeader}>
          <div className={classes.contentWrapper}>
            <span className={classes.actionText}>{detail?.activityName}</span>
            <div className={classes.wrapperBadge}>
              <Badge
                type={
                  detail?.status === "APPROVED"
                    ? "green"
                    : detail?.status === "REJECTED"
                    ? "red"
                    : "blue"
                }
                label={detail?.status}
              />
            </div>
          </div>
          {/* <img src={background} alt="pic" width="189.77px" /> */}
        </div>
      </div>

      {/* Activity Log */}
      <div className={classes.container}>
        <TableICBB
          headerContent={tableConfig}
          dataContent={detail?.activityLogs ?? []}
          rowColor="#EAF2FF"
          stripe
        />
      </div>

      {/* New / Old Data */}
      <div className={classes.paperWrapper}>
        <NIDetailTemplate data={detail} classes={classes} />
      </div>

      {/* Comment */}
      <div className={classes.paper}>
        <p style={{ marginBottom: 5 }}>Comment :</p>
        <TextField
          type="textArea"
          value={comment}
          onChange={(e) => setComment(e.target.value)}
          placeholder="Invalid title"
          style={{
            width: "100%",
            minHeight: 150,
            background: "#F4F7FB",
            color: "#BCC8E7",
          }}
        />
      </div>
    </React.Fragment>
  );
};

Details.propTypes = {};

export default Details;
