import React from "react";

import { makeStyles, Grid } from "@material-ui/core";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function InternationalBank({ data }) {
  const classes = useStyles();

  const options = [
    {
      title: "Bank Code :",
      value: data?.bankCode ?? "-",
    },
    {
      title: "Bank Name :",
      value: data?.bankName ?? "-",
    },
    {
      title: "Bank Alias Name :",
      value: data?.bankShortName ?? "-",
    },
    {
      title: "Country :",
      value: data?.country ?? "-",
    },
    {
      title: "City :",
      value: data?.city ?? "-",
    },
    {
      title: "Currency :",
      value: data?.currencyCode ?? "-",
    },
    {
      title: "Swift Code :",
      value: data?.swiftCode ?? "-",
    },
    {
      title: "Status :",
      value: data?.deleted ? "Inactive" : "Active" || "-",
    },
  ];

  const GridItem = ({ props }) => {
    const { title, value } = props;

    return (
      <Grid item>
        <Grid container direction="column">
          <Grid item className={classes.title}>
            {title}
          </Grid>
          <Grid item className={classes.value}>
            {value}
          </Grid>
        </Grid>
      </Grid>
    );
  };

  return (
    <Grid container direction="column" spacing={2}>
      {options.map((el, i) => (
        <GridItem props={el} key={i} />
      ))}
    </Grid>
  );
}
