import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function HandlingOfficer({ data }) {
  const classes = useStyles();

  const options = [
    {
      title: "Officer ID:",
      value: data?.officerId ?? "-",
    },
    {
      title: "Username :",
      value: data?.username ?? "-",
    },
    {
      title: "Branch :",
      value: data?.location ?? "-",
    },
    {
      title: "Email :",
      value: data?.email ?? "-",
    },
    {
      title: "Mobile Phone Number :",
      value: data?.phoneNumber ?? "-",
    },
  ];

  const GridItem = ({ props }) => {
    const { title, value } = props;

    return (
      <Grid item>
        <Grid container direction="column">
          <Grid item className={classes.title}>
            {title}
          </Grid>
          <Grid item className={classes.value}>
            {value}
          </Grid>
        </Grid>
      </Grid>
    );
  };

  return (
    <Grid container direction="column" spacing={2}>
      {options.map((el, i) => (
        <GridItem props={el} key={i} />
      ))}
    </Grid>
  );
}
