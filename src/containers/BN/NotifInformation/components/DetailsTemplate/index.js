import { Grid } from "@material-ui/core";
import AccountTypeDetail from "containers/BN/AuditTrail/AdminBank/components/TemplateData/Detail/AccountType";
import Colors from "helpers/colors";
import React from "react";
import { gridSize } from "utils/helpers";
import { NorifInformationContentPaper } from "../Details";
// import AccountTypeDetail from "./AccountType";
import ApprovalMatrix from "./ApprovalMatrix";
import ChargePackage from "./ChargePackage";
import { getTypeNotifInformation } from "./constType";
import DetailChargeList from "./DetailChargeList";
import DomesticBank from "./DomesticBank";
import GeneralParameter from "./GeneralParameter";
import HandlingOfficer from "./HandlingOfficer";
import HostErrorMapping from "./HostErrorMapping";
import InternationalBank from "./InternationalBank";

import NIMatrixNonFinancial from "./matrixNonFinancial";
import MenuPackage from "./MenuPackage";
import NIPrivilegeSettings from "./PrivilegeSettings";
import SegmentationBranch from "./SegmentationBranch";
import SegmentationDetail from "./SegmentationDetail";
import Service from "./Service";
import ServiceChargelist from "./ServiceChargelist";
import SpecialMenu from "./SpecialMenu";
import NITieringDetail from "./TieringDetail";
import NIUserSettings from "./UserSettings";

const NoDataTemplate = ({ subtitle }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      minHeight: "90%",
      flexDirection: "column",
      width: "50%",
    }}
  >
    <h3
      style={{
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        fontSize: 20,
        color: Colors.dark.medium,
      }}
    >
      No Data
    </h3>
    <p
      style={{
        fontFamily: "FuturaBkBT",
        fontWeight: 400,
        fontSize: 13,
        color: Colors.dark.medium,
      }}
    >
      {subtitle}
    </p>
  </div>
);

const mappingMenu = (type, props) => {
  switch (type) {
    case "CORPORATE_ACCOUNT_GROUPING_PORTAL":
      return <NIMatrixNonFinancial {...props} />;
    case "APPROVAL_MATRIX_DETAIL":
      return <ApprovalMatrix {...props} />;
    case "PRIVILEGE_SETTINGS_DETAIL":
      return <NIPrivilegeSettings {...props} />;
    case "USER_DETAIL":
      return <NIUserSettings {...props} />;
    case "TIERING_DETAIL":
      return <NITieringDetail {...props} />;
    case "PARAMETER_SETTINGS_DETAIL":
      return <GeneralParameter {...props} />;
    case "HOST_ERROR_MAPPING":
      return <HostErrorMapping {...props} />;
    case "DOMESTIC_BANK_DETAIL":
      return <DomesticBank {...props} />;
    case "INTERNATIONAL_BANK_DETAIL":
      return <InternationalBank {...props} />;
    case "HANDLING_OFFICER_DETAIL":
      return <HandlingOfficer {...props} />;
    case "SERVICE_DETAIL":
      return <Service {...props} />;
    case "SEGMENTATION_DETAIL":
      return <SegmentationDetail {...props} />;
    case "ACCOUNT_TYPE_DETAIL":
      return <AccountTypeDetail {...props} />;
    case "CHARGE_LIST_DETAIL":
      return <DetailChargeList {...props} />;
    case "CHARGE_PACKAGE_DETAIL":
      return <ChargePackage {...props} />;
    case "MENU_PACKAGE_DETAIL":
      return <MenuPackage {...props} />;
    case "SEGMENTATION_BRANCH_DETAIL":
      return <SegmentationBranch {...props} />;
    case "SPECIAL_MENU_DETAIL":
      return <SpecialMenu {...props} />;
    default:
      return null;
  }
};

const NIDetailTemplate = ({ data, classes }) => {
  const type = data ? getTypeNotifInformation(data?.activityName) : null;
  const size = gridSize(type);

  if (!type) return null;

  return (
    <React.Fragment>
      <Grid container spacing={2} style={{ minHeight: "auto" }}>
        <Grid item sm={size}>
          <NorifInformationContentPaper title="Old Data">
            {data?.oldData ? (
              mappingMenu(type, {
                title: "Old Data",
                data: JSON.parse(data?.oldData),
              })
            ) : (
              <NoDataTemplate subtitle=" Not displaying old data" />
            )}
          </NorifInformationContentPaper>
        </Grid>
        <Grid sm={size}>
          <NorifInformationContentPaper title="New Data">
            {data?.newData ? (
              mappingMenu(type, {
                title: "New Data",
                data: JSON.parse(data?.newData),
              })
            ) : (
              <NoDataTemplate subtitle="Not displaying new data" />
            )}
          </NorifInformationContentPaper>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default NIDetailTemplate;
