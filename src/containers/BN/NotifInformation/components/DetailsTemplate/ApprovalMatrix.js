import React, { Fragment } from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  header: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "15px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  thead: {
    background: "#F4F7FB",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#7B87AF",
  },
  trow: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  borderBottom: {
    borderBottom: "1px solid #F4F7FB",
  },
});

export default function ApprovalMatrix({ data }) {
  const classes = useStyles();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} classes={{ root: classes.header }}>
        {
          data?.workflowApprovalMatrixPortalDetailDtoList[0]
            ?.workflowGroupPortalDtoList?.length
        }{" "}
        Number of Approvals
      </Grid>
      <Grid item xs={12}>
        <Grid container classes={{ root: classes.thead }} spacing={2}>
          <Grid item xs={1}>
            Seq
          </Grid>
          <Grid item xs={1}>
            User
          </Grid>
          <Grid item xs={5}>
            Group
          </Grid>
          <Grid item xs={5}>
            Target Group
          </Grid>
        </Grid>
      </Grid>
      {data?.workflowApprovalMatrixPortalDetailDtoList?.map((parent, i) => (
        <Fragment key={i}>
          {parent?.workflowGroupPortalDtoList?.map((child, i) => (
            <Grid item xs={12} key={i}>
              <Grid
                container
                classes={{
                  root:
                    i !== parent?.workflowGroupPortalDtoList?.length - 1
                      ? `${classes.trow} ${classes.borderBottom}`
                      : classes.trow,
                }}
                spacing={2}
              >
                <Grid item xs={1}>
                  {child?.totalSequence ?? "-"}
                </Grid>
                <Grid item xs={1}>
                  {child?.totalApproval ?? "-"}
                </Grid>
                <Grid item xs={5}>
                  {child?.groupType ?? "-"}
                </Grid>
                <Grid item xs={5}>
                  {child?.portalGroupName ?? "-"}
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Fragment>
      ))}
    </Grid>
  );
}
