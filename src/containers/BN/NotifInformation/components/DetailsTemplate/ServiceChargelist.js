import React, { Fragment } from "react";

import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

const useStyles = makeStyles({
  tableHeader: {
    background: "#EAF2FF",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#374062",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  value: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "15px",
    color: "#374062",
  },
});

export default function ServiceChargelist({ data }) {
  const classes = useStyles();

  return (
    <Grid container direction="column">
      {/* -----Table Header----- */}
      <Grid item style={{ marginTop: 20 }}>
        <Grid
          container
          className={classes.tableHeader}
          spacing={2}
          style={{ paddingLeft: 20 }}
        >
          <Grid item xs={6}>
            Service Name
          </Grid>
          <Grid item xs={6}>
            Charge List Name
          </Grid>
        </Grid>
      </Grid>

      {/* -----Table Row----- */}
      <Grid item>
        {data?.map((el, i) => (
          <Grid
            container
            style={{
              paddingTop: i !== 0 ? 12 : undefined,
              paddingLeft: 20,
              borderBottom: "1px solid #EAF2FF",
            }}
            spacing={2}
            key={i}
          >
            <Grid item xs={6} className={classes.title}>
              {el?.service?.name && el?.service?.name !== ""
                ? el?.service?.name
                : el?.serviceId?.name && el?.serviceId?.name !== ""
                ? el?.serviceId?.name
                : "-"}
            </Grid>
            <Grid item xs={6}>
              <Grid container spacing={1} alignItems="center">
                {el?.chargesList?.map((el) => (
                  <Fragment key={el?.chargeName}>
                    <Grid item>
                      <BlueSphere />
                    </Grid>
                    <Grid item className={classes.value}>
                      {el?.chargeName && el?.chargeName !== ""
                        ? el?.chargeName
                        : "-"}
                    </Grid>
                  </Fragment>
                ))}
              </Grid>
            </Grid>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
}
