import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { handleConvertRole } from "utils/helpers";

const useStyles = makeStyles({
  column: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function HostErrorMapping({ data }) {
  const classes = useStyles();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Error Code:</span>
        <span className={classes.value}>{data?.code ?? "-"}</span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Source System :</span>
        <span className={classes.value}>{data?.sourceSystem ?? "-"}</span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Message In English :</span>
        <span className={classes.value}>{data?.engMessage ?? "-"}</span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Message in Indonesian :</span>
        <span className={classes.value}>{data?.idnMessage ?? "-"}</span>
      </Grid>
    </Grid>
  );
}
