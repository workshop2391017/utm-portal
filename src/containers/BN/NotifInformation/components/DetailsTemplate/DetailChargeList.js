import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Grid } from "@material-ui/core";

import { handleConvertRole } from "utils/helpers";

const useStyles = makeStyles({
  column: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontweight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#7B87AF",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "24px",
    letterSpacing: "0.01em",
    color: "#374062",
  },
});

export default function DetailChargeList({ data }) {
  const classes = useStyles();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Charge List :</span>
        <span className={classes.value}>
          {data?.length > 0
            ? data[0].name ?? "-"
            : data?.chargesList?.name ?? "-"}
        </span>
      </Grid>
      <Grid item xs={12} className={classes.column}>
        <span className={classes.title}>Flag Charge List :</span>
        <span className={classes.value}>
          {data?.length > 0
            ? data[0].type === "TRF_FEE"
              ? "Transfer Fee"
              : "Admin Fee"
            : data?.chargesList
            ? data?.chargesList?.type === "TRF_FEE"
              ? "Transfer Fee"
              : "Admin Fee"
            : "-"}
        </span>
      </Grid>
    </Grid>
  );
}
