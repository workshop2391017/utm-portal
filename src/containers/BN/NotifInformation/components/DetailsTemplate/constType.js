export const getTypeNotifInformation = (activityName) => {
  //   if (activityName.includes("Status User Setting Corporate"))
  //     return "STATUS_USER_CORPORATE_DETAIL";
  //   if (activityName.includes("User Setting Corporate"))
  //     return "USER_CORPORATE_DETAIL";
  if (activityName.includes("User Setting")) return "USER_DETAIL";
  //   if (activityName.includes("Level")) return "LEVEL_DETAIL";
  //   if (activityName.includes("Service Chargelist"))
  //     return "SERVICE_CHARGE_LIST_DETAIL";
  //   if (activityName.includes("Limit Package")) return "LIMIT_PACKAGE_DETAIL";
  if (activityName.includes("General Parameter"))
    return "PARAMETER_SETTINGS_DETAIL";
  //   if (activityName.includes("Parameter Advance"))
  //     return "PARAMETER_ADVANCE_DETAIL";
  //   if (activityName.includes("Parameter")) return "GENERAL_PARAMETER_DETAIL";
  if (activityName.includes("bank domestic")) return "DOMESTIC_BANK_DETAIL";
  if (activityName.includes("international bank"))
    return "INTERNATIONAL_BANK_DETAIL";
  //   if (activityName.includes("Account Limit Config"))
  //     return "CORPORATE_ACCOUNT_LIMIT_CONFIG";
  //   if (activityName.includes("Segmentation Setting"))
  //     return "CORPORATE_SEGMENTATION";
  if (activityName.includes("Privilege Setting"))
    return "PRIVILEGE_SETTINGS_DETAIL";
  //   if (activityName.includes("Product Detail")) return "PRODUCT_DETAIL";
  //   if (activityName.includes("Account Product")) return "ACCOUNT_PRODUCT_DETAIL";
  if (activityName.includes("product type detail"))
    return "ACCOUNT_TYPE_DETAIL";
  //   if (activityName.includes("Account Configuration"))
  //     return "ACCOUNT_CONFIGURATION_DETAIL";
  //   if (activityName.includes("Transaction Limit"))
  //     return "LIMIT_TRANSACTION_DETAIL";
  if (activityName.includes("error")) return "HOST_ERROR_MAPPING";
  if (activityName.includes("charges list")) return "CHARGE_LIST_DETAIL";
  if (activityName.includes("charge package")) return "CHARGE_PACKAGE_DETAIL";
  if (activityName.includes("service global")) return "SERVICE_DETAIL";
  if (activityName.includes("segment branch"))
    return "SEGMENTATION_BRANCH_DETAIL";
  if (activityName.includes("add segmentation")) return "SEGMENTATION_DETAIL";
  //   if (activityName.includes("Company Limit")) return "COMPANY_LIMIT_DETAIL";
  //   if (activityName.includes("Company Charge")) return "COMPANY_CHARGE_DETAIL";
  //   if (activityName.includes("Company")) return "CORPORATE_MANAGEMENT_DETAIL";
  if (activityName.includes("approval matrix portal"))
    return "APPROVAL_MATRIX_DETAIL";
  if (activityName.includes("menu package")) return "MENU_PACKAGE_DETAIL";
  if (activityName.includes("tiering")) return "TIERING_DETAIL";
  if (activityName.includes("special menu")) return "SPECIAL_MENU_DETAIL";
  if (activityName.includes("handling officer"))
    return "HANDLING_OFFICER_DETAIL";

  if (activityName.includes("corporate account group portal"))
    return "CORPORATE_ACCOUNT_GROUPING_PORTAL";

  return "";
};
