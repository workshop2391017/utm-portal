// Landing page
import { makeStyles } from "@material-ui/core";
import moment from "moment";
import React, { useEffect, useState } from "react";

// components
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import Search from "components/BN/Search/SearchWithoutDropdown";
import TableICBB from "components/BN/TableIcBB";
import Title from "components/BN/Title";
import Toast from "components/BN/Toats";
import Badge from "components/BN/Badge";
import { useDispatch, useSelector } from "react-redux";
import {
  handleGetNotifInformation,
  setNotifId,
} from "stores/actions/nofitInformation";

// const data = [
//   {
//     fullName: "ist.rian.superadminmaker",
//     activityName: "Edit Approval Matrix",
//     activityId: 158552,
//     activityDate: 1664330365000,
//     seen: true,
//     status: 0,
//     userActivityDetail: {
//       activityDate: 1664330365000,
//       roleName: "ADMIN_MAKER",
//       fullName: "ist.rian.superadminmaker",
//       branchName: "papa muda contoh",
//       action: "null",
//       groupName: "Group Superadmin Maker",
//       oldData: null,
//       newData: null,
//     },
//   },
//   {
//     fullName: "ist.rian.superadminmaker",
//     activityName: "Add General Parameter",
//     activityId: 158552,
//     activityDate: 1664330365000,
//     seen: false,
//     status: 1,
//     userActivityDetail: {
//       activityDate: 1664330365000,
//       roleName: "ADMIN_MAKER",
//       fullName: "ist.rian.superadminmaker",
//       branchName: "papa muda contoh",
//       action: "null",
//       groupName: "Group Superadmin Maker",
//       oldData: null,
//       newData: null,
//     },
//   },
// ];

const useStyles = makeStyles({
  container: {
    padding: "20px 30px 30px",
  },
  buttonGroup: {
    display: "flex",
    "& .MuiButton-outlinedPrimary.Mui-disabled": {
      border: "1px solid #BCC8E7",
      color: "#BCC8E7",
      backgroundColor: "#fff",
    },
    "& .MuiButton-containedPrimary.Mui-disabled": {
      backgroundColor: "#BCC8E7",
      color: "#fff",
    },
  },
  buttonTolak: {
    fontFamily: "FuturaMdBT",
    fontSize: 9,
    borderRadius: "6px !important",
    border: "1px solid #0061A7",
    color: "#0061A7",
    marginRight: "25px",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
  },
  buttonSetujui: {
    fontFamily: "FuturaMdBT",
    fontSize: 9,
    borderRadius: "6px !important",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    textTransform: "none",
  },
});

const Landing = ({ setActivePage }) => {
  const classes = useStyles();

  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);

  const dispatch = useDispatch();

  const { isLoading, data } = useSelector((e) => e.notifInformation);

  const tableConfig = [
    {
      title: "Date & Time",
      headerAlign: "left",
      align: "left",
      render: (rowData) => (
        <span>
          {moment(rowData.createdDate).format("DD-MM-YYYY | HH:mm:ss")}
        </span>
      ),
    },
    {
      title: "Activity",
      headerAlign: "left",
      key: "activityName",
    },
    {
      title: "Seen By User",
      headerAlign: "left",
      key: "seen",
      render: (rowData) => (rowData.seen ? "Yes" : "No"),
    },
    {
      title: "Status",
      headerAlign: "left",
      align: "left",
      fontFamily: "FuturaBkBT",
      width: 100,
      render: (rowData) => {
        if (rowData.activityStatus === "APPROVED") {
          return <Badge label="Approved" outline type="green" />;
        }

        if (rowData.activityStatus === "REJECTED") {
          return <Badge label="Rejected" outline type="red" />;
        }
        return <Badge label="Waiting" outline type="blue" />;
      },
    },
    {
      title: "",
      textAlign: "center",
      key: "btnapp",
      width: 100,
      render: (rowData) => (
        <div>
          <GeneralButton
            label="View Details"
            width="76px"
            height="23px"
            style={{ marginRight: 11, fontSize: 9, padding: 0 }}
            onClick={() => {
              setActivePage(1);
              dispatch(setNotifId(rowData?.id));
            }}
          />
        </div>
      ),
    },
  ];

  const fetchingNotif = (payload) => {
    dispatch(handleGetNotifInformation(payload));
  };

  useEffect(() => {
    const payload = {
      pageNumber: page - 1,
      pageSize: 10,
    };
    fetchingNotif(payload);
  }, [page]);

  // notifInformation

  return (
    <div>
      <Toast
        open={false}
        message={null}
        // handleClose={() => }
      />

      <Title label="Notif Information">
        <Search
          options={["Date & Time", "Activity", "Seen By User", "Status"]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Rejected", "Approved"],
            },
          ]}
        />
        <div style={{ marginTop: 20 }}>
          <TableICBB
            headerContent={tableConfig}
            dataContent={data?.notificationList ?? []}
            page={page}
            setPage={setPage}
            isLoading={isLoading}
            totalElement={data?.totalElements}
            totalData={data?.totalPages}
          />
        </div>
      </div>
    </div>
  );
};

Landing.propTypes = {};

Landing.defaultProps = {};

export default Landing;
