// NotifInformation page
import React, { useEffect, useState } from "react";

// redux
import { useSelector } from "react-redux";

// components
import Landing from "./components/Landing";
import Details from "./components/Details";

function NotifInformation() {
  const [activePage, setActivePage] = useState(0);

  // const { detail } = useSelector((state) => state.notifInformation);

  // useEffect(() => {
  //   if (detail) {
  //     setActivePage(1);
  //   }
  // }, [detail]);

  return (
    <div>
      {activePage === 0 && <Landing setActivePage={setActivePage} />}
      {activePage === 1 && <Details setActivePage={setActivePage} />}
    </div>
  );
}

export default NotifInformation;
