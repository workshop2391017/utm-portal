import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import Title from "components/BN/Title";
import {
  Grid,
  Paper,
  Typography,
  CardContent,
  Avatar,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { pathnameCONFIG } from "configuration";
import { useHistory } from "react-router-dom";

// components
import { ReactComponent as EditIcon } from "assets/icons/BN/pen.svg";
import GeneralButton from "components/BN/Button/GeneralButton";
import ScrollCustom from "components/BN/ScrollCustom";
import Search from "../../../components/BN/Search/SearchWithoutDropdown";
import plus from "../../../assets/icons/BN/plus-blue.svg";

const useStyles = makeStyles({
  container1: {
    // backgroundColor: "red",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: 10,
    marginRight: 30,
  },
  container2: {
    // backgroundColor: "red",
    backgroundColor: "#fff",
    borderRadius: 10,
    marginRight: 30,
  },
  preferensi: {
    backgroundColor: "#F4F7FB",
    height: "100%",
  },
  title: {
    fontSize: 15,
    fontFamily: "FuturaMdBT",
    color: "#fff",
  },
  card: {
    width: "100%",
    backgroundColor: "#0061A7",
    height: 40,
    border: "none",
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingLeft: 15,
    paddingTop: 8,
    paddingBottom: 8,
  },
  card2: {
    // backgroundColor: "yellow",
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
  },
  card3: {
    height: 60,
    display: "flex",
    justifyContent: "center",
    borderTop: "1px solid #E8EEFF",
  },
  card4: {
    // backgroundColor: "yellow",
    display: "flex",
    justifyContent: "space-between",
    borderBottom: "1px solid #BCC8E7",
    width: "320px",
  },
  subtitle: {
    fontSize: 13,
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    lineHeight: "16px",
    color: "#374062",
    letterSpacing: "0.03em",
  },
});

const ListJenisPromo = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [popUpDownload, setPopUpDownload] = useState(false);
  const [add, setAdd] = useState(false);
  const [subKategori, setSubKategori] = useState(true);

  return (
    <div className={classes.preferensi}>
      <Title label="List Jenis Promo" />

      <div
        style={{
          display: "flex",
          marginLeft: 23,
          marginRight: 37,
          width: "100%",
        }}
      >
        <Grid xs={3} className={classes.container1}>
          <Paper className={classes.card}>
            <Typography className={classes.title}>
              Kategori Preferensi
            </Typography>
          </Paper>
          <div
            style={{
              width: "100%",
              padding: 15,
              borderBottom: "1px solid #E8EEFF",
            }}
          >
            <Search
              style={{ width: "100%" }}
              options={[
                "ID Cabang",
                "Jenis Kantor",
                "nama Outlet",
                "Provinsi",
                "Kota",
                "Telepone",
                "Status",
              ]}
            />
          </div>
          <ScrollCustom style={{ width: "100%" }} height={400}>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
            <CardContent className={classes.card2}>
              <Typography className={classes.subtitle}>
                Transaksi Admin
              </Typography>
            </CardContent>
          </ScrollCustom>
        </Grid>
        <Grid xs={9} className={classes.container2}>
          <Paper className={classes.card}>
            <Typography className={classes.title}>
              Subkategori Preferensi
            </Typography>
          </Paper>
          <div
            style={{
              padding: 15,
              borderBottom: "1px solid #E8EEFF",
              display: "flex",
              gap: 50,
            }}
          >
            <Search
              style={{ width: "100%" }}
              options={[
                "ID Cabang",
                "Jenis Kantor",
                "nama Outlet",
                "Provinsi",
                "Kota",
                "Telepone",
                "Status",
              ]}
            />
            <GeneralButton
              label="Edit"
              iconPosition="startIcon"
              buttonIcon={<EditIcon />}
              width="94px"
            />
          </div>
          <div style={{ width: "100%", display: "flex", padding: "10px" }}>
            <ScrollCustom
              style={{ width: "100%", display: "flex" }}
              height={400}
            >
              <div style={{ flex: "50%" }}>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
              </div>
              <div style={{ flex: "50%" }}>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi AdminNNN
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
                <CardContent className={classes.card4}>
                  <Typography className={classes.subtitle}>
                    Transaksi Admin
                  </Typography>
                </CardContent>
              </div>
            </ScrollCustom>
          </div>
          <div
            style={{
              // backgroundColor: "pink",
              marginTop: "16px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontSize: 13,
              fontWeight: 700,
              height: "70px",
              marginLeft: "7px",
              // backgroundColor: "brown",
              fontFamily: "FuturaMdBT",
              letterSpacing: "0.03em",
              color: "#0061A7",
              cursor: "pointer",
            }}
          >
            <Avatar
              src={plus}
              style={{
                width: "17px",
                height: "17px",
              }}
            />
            <Typography
              style={{
                color: "#0061A7",
                display: "flex",

                fontSize: 13,
                fontWeight: 700,
                marginLeft: "7px",
                letterSpacing: "0.03em",
                fontFamily: "FuturaMdBT",
                // backgroundColor: "brown",
              }}
              onClick={() => {
                console.warn("berhasil");
                history.push(pathnameCONFIG.JENIS_PROMO.ADD_SUB_CATEGORY);
              }}
            >
              Subkategori Preferensi
            </Typography>
          </div>
        </Grid>
      </div>
    </div>
  );
};

ListJenisPromo.propTypes = {};

export default ListJenisPromo;
