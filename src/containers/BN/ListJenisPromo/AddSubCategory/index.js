import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

// components
import Title from "components/BN/Title";
import AntdTextField from "components/BN/TextField/AntdTextField";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as DeleteIcon } from "assets/icons/BN/trash-2.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirm from "components/BN/Popups/BerhasilRekeningKelompok";

const useStyles = makeStyles(() => ({
  page: {
    padding: "20px",
  },
  container: {
    width: "100%",
    height: "600px",
    marginTop: "20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",

    "& .form": {
      width: "700px",
      height: "auto",
      backgroundColor: "#FFFFFF",
      borderRadius: "20px",
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      padding: "30px",
      display: "flex",
      flexDirection: "column",
      "& #span1": {
        fontFamily: "FuturaMdBT",
        fontSize: "16px",
        letterSpacing: "0.03em",
        color: "#374062",
        fontWeight: 400,
        lineHeight: "19px",
      },
      "& #span2": {
        fontFamily: "FuturaBkBT",
        fontSize: "13px",
        fontWeight: 400,
        letterSpacing: "0.01em",
        color: "#374062",
        lineHeight: "18px",
      },
      "& #span3": {
        fontFamily: "FuturaMdBT",
        fontSize: "15px",
        letterSpacing: "0.01em",
        color: "#374062",
        fontWeight: 400,
        lineHeight: "19px",
      },
    },
  },
  footer: {
    width: "100%",
    height: "88px",
    display: "flex",
    justifyContent: "space-between",
    background: "#FFFFFF",
    alignItems: "center",
    padding: "22px 40px",
  },
}));

const data = [
  {
    id: 1,
    input: "hello",
  },
  {
    id: 2,
    input: "hello",
  },
  {
    id: 3,
    input: "hello",
  },
];

const AddSubCategory = () => {
  const classes = useStyles();

  const [dataMap, setDataMap] = useState([]);
  const [succes, setSuccess] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [confirmationDelete, setConfirmationDelete] = useState(false);
  const [dataDelete, setDelete] = useState(0);
  useEffect(() => {
    setDataMap(data);
  }, [data]);

  const handleAdd = () => {
    const data = [...dataMap];
    data.push({ id: data.length + 1, input: "" });
    setDataMap(data);
  };
  const handleDelete = () => {
    const data = [...dataMap];
    data.splice(dataDelete, 1);
    setDataMap(data);
  };
  console.warn("dataMap", dataMap);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100%",
      }}
    >
      <SuccessConfirm
        message="Saved Successfully"
        isOpen={succes}
        title="Please Wait For Approval"
        handleClose={() => setSuccess(false)}
      />
      <DeletePopup
        handleClose={() => setConfirmation(false)}
        isOpen={confirmation}
        message="
Are You Sure To Add Data?"
        onContinue={() => {
          setSuccess(true);
          setConfirmation(false);
        }}
      />
      <DeletePopup
        handleClose={() => setConfirmation(false)}
        isOpen={confirmationDelete}
        message="
Are You Sure To Delete Data?"
        onContinue={() => {
          handleDelete();
          setSuccess(true);
          setConfirmationDelete(false);
        }}
      />
      <div className={classes.page}>
        <Title label="Add Sub Category" />
        <div className={classes.container}>
          <div className="form">
            <span id="span1">Add Category Form</span>
            <span style={{ marginTop: "12px" }} id="span2">
              Please fill in the data below to add a category
            </span>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: "14px",
                gap: 4,
              }}
            >
              <span id="span2">Category : </span>
              <span id="span3">Daily Needs</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: "10px",
              }}
            >
              <span>Subcategory :</span>
              <div
                style={{
                  marginTop: "4px",
                  gap: 30,
                  padding: 4,
                  width: "670px",
                  display: "flex",
                  flexWrap: "wrap",
                  alignItems: "center",
                }}
              >
                {dataMap.map((item, index) => (
                  <div
                    style={{ display: "flex", alignItems: "center" }}
                    key={index}
                  >
                    <AntdTextField
                      style={{
                        marginRight: "4px",
                        width: "260px",
                      }}
                      placeholder="Enter Subcategory"
                    />
                    <button
                      onClick={() => {
                        setDelete(index);
                        setConfirmationDelete(true);
                      }}
                      style={{
                        cursor: "pointer",
                        background: "transparent",
                        border: "1px solid #D14848",
                        borderRadius: "10px",
                        padding: "8px 2px 2px 2px",
                        width: "50px",
                        height: "40px",
                      }}
                    >
                      <DeleteIcon />
                    </button>
                  </div>
                ))}

                <div>
                  {" "}
                  <GeneralButton
                    onClick={handleAdd}
                    width="50px"
                    height="40px"
                    style={{
                      fontSize: "25px",
                      borderRadius: "10px",
                    }}
                    label="+"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={classes.footer}>
        <ButtonOutlined label="Cancel" width="93px" height="44px" />
        <GeneralButton
          onClick={() => {
            setConfirmation(true);
          }}
          label="Save"
          width="78px"
          height="44px"
        />
      </div>
    </div>
  );
};

export default AddSubCategory;
