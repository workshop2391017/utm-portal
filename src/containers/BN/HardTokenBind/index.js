import React, { useState, useEffect } from "react";
import { Button, makeStyles } from "@material-ui/core";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { setHandleClearError } from "stores/actions/managementuser";
import TableICBB from "components/BN/TableIcBB";
import Toast from "components/BN/Toats";
import Title from "components/BN/Title";
import edit from "assets/icons/BN/edit-2.svg";
import EditHardToken from "components/BN/Popups/EditHardToken";
import {
  getCorporateList,
  getCorporateUserList,
  setCorporateUserList,
} from "stores/actions/managementHardToken";
import SelectGroup from "components/BN/Select/SelectGroup";
import GeneralButton from "components/BN/Button/GeneralButton";
import { useHistory } from "react-router-dom";
import arrowLeft from "assets/icons/BN/arrow-left.svg";

const HardTokenBind = () => {
  const dispatch = useDispatch();
  const router = useHistory();
  const classes = useStyles();
  const [selectedCorporateId, setSelectedCorporateId] = useState(null);
  const [selectedUserToBind, setSelectedUserToBind] = useState({});
  const [showEditModal, setShowEditModal] = useState(false);

  const { isLoading, error, corporateList, corporateUserList } = useSelector(
    ({ managementHardToken }) => managementHardToken,
    shallowEqual
  );

  useEffect(() => {
    dispatch(getCorporateList({}));
    return () => {
      dispatch(setCorporateUserList([]));
    };
  }, []);

  /* ---------------------------- HANDLER FUNCTION ---------------------------- */
  const handleShowBindModal = (row) => {
    setSelectedUserToBind(row);
    setShowEditModal(true);
  };

  const handleSearchUser = () => {
    if (!selectedCorporateId) return;
    dispatch(getCorporateUserList(selectedCorporateId));
  };

  /* ---------------------------------- VIEWS --------------------------------- */
  return (
    <div className={classes.billerManager}>
      <Button
        startIcon={<img src={arrowLeft} alt="Back" />}
        onClick={() => router.push("/hard-token")}
        style={{
          fontFamily: "FuturaMdBT",
          fontWeight: 700,
          fontSize: 13,
          color: "#0061A7",
          textTransform: "capitalize",
          margin: 30,
          marginBottom: 0,
        }}
      >
        Back
      </Button>
      <Title label="Bind Hard Token"></Title>
      <div className={classes.card}>
        <div
          style={{
            display: "flex",
          }}
        >
          <SelectGroup
            options={corporateList
              .sort((a, b) =>
                a.corporateName > b.corporateName
                  ? 1
                  : b.corporateName > a.corporateName
                  ? -1
                  : 0
              )
              .map((company) => ({
                ...company,
                name: company.corporateName,
              }))}
            value={selectedCorporateId}
            placeholder="Select Company"
            style={{ width: 335 }}
            onChange={(e) => setSelectedCorporateId(e)}
          />
        </div>
        <GeneralButton
          label="Search"
          disabled={!selectedCorporateId}
          onClick={handleSearchUser}
        />
      </div>

      <div className={classes.container}>
        <TableICBB
          headerContent={tableConfig({ onEdit: handleShowBindModal })}
          dataContent={corporateUserList.map((user) => user?.userProfile)}
          isLoading={isLoading}
          totalElement={false}
        />
      </div>

      <EditHardToken
        title="Edit Hard Token"
        isOpen={showEditModal}
        data={selectedUserToBind}
        corporateId={selectedCorporateId}
        handleClose={() => setShowEditModal(false)}
        onContinue={() => setSuccessConfirmModal(true)}
      />

      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
    </div>
  );
};

HardTokenBind.propTypes = {};
HardTokenBind.defaultProps = {};
export default HardTokenBind;

/* ------------------------------ TABLE CONFIG ------------------------------ */
const tableConfig = ({ onEdit }) => [
  {
    title: "Username",
    headerAlign: "left",
    align: "left",
    key: "username",
  },
  {
    title: "Name",
    headerAlign: "left",
    align: "left",
    key: "nameAlias",
  },
  {
    title: "Phone Number",
    headerAlign: "left",
    align: "left",
    key: "phoneNumber",
  },
  {
    title: "Email",
    headerAlign: "left",
    align: "left",
    key: "email",
  },
  {
    title: "",
    headerAlign: "right",
    width: "50px",
    fontFamily: "FuturaMdBT",
    render: (rowData) => (
      <Button
        startIcon={<img src={edit} alt="Menu" />}
        color="primary"
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={() => onEdit(rowData)}
      />
    ),
  },
];

/* --------------------------------- STYLES --------------------------------- */
const useStyles = makeStyles({
  billerManager: {},
  container: {
    padding: "20px 30px",
  },
  card: {
    padding: "15px 20px",
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    marginLeft: 30,
    marginRight: 30,
    borderRadius: 10,
  },
  menuButton: {
    minWidth: 40,
    padding: 0,
    "& .MuiButton-label": {
      padding: 0,
      "& .MuiButton-startIcon": {
        margin: 0,
      },
    },
  },
});
