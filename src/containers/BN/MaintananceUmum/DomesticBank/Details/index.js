import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Button, makeStyles } from "@material-ui/core";
import {
  addDataBankDomestic,
  getValidateCreate,
  setTypeDetail,
  setTypeSuccess,
  setValidate,
} from "stores/actions/domesticBank";
import {
  parseAlphaNumeric,
  parseNumber,
  parseTextNumFirstSpace,
  parseTextNumFirstSpecialChar,
  validateMinMax,
} from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import { pathnameCONFIG } from "configuration";

// component
import DeleteConfirmation from "components/BN/Popups/Delete";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import RadioGroup from "components/BN/Radio/RadioGroup";
import TextField from "components/BN/TextField/AntdTextField";
import Title from "components/BN/Title";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import FormField from "components/BN/Form/InputGroupValidation";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import PopUpComment from "./PopUpComment";

const useStyles = makeStyles({
  domesticbankDetail: {},
  container: {
    margin: "20px 30px",
    backgroundColor: "#fff",
    borderRadius: "10px",
    padding: 40,
    marginBottom: 52,
  },
  formcontent: {
    minHeight: 500,
  },
  inputGroup: {
    display: "flex",
    alignItems: "flex-star",
    "& .checkboxgroup": {
      width: 120,
      marginRight: 40,
      marginTop: 16,
      fontFamily: "FuturaBkBT",
      fontSize: 15,
      display: "flex",
      "& .span": {
        marginLeft: 10,
      },
    },
  },
});

const DomesticBankDetails = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const validates = false;
  const options = [
    { label: "Jalin", value: "JALIN", disabled: false },
    { label: "Alto", value: "ALT", disabled: false },
    { label: "Artajasa", value: "ARTAJASA", disabled: false },
    { label: "Prima", value: "PRIMA", disabled: false },
  ];
  const { dataDetail, isOpen, isLoading, isLoadingExcute, validate } =
    useSelector((state) => state.domesticBank);
  const [optionsPrimary, setOptionsPrimary] = useState(options);
  const [optionsSecondary, setOptionsSecondary] = useState(options);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [codeValidateTask, setCodeValidationTask] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [comment, setComment] = useState("");
  const [formData, setFormData] = useState({
    bankName: null,
    bankAlias: null,
    onlineSwitchingType: null,
    // onlineSwitchingTypeSecondary: null,
    onlineTransfer: false,
    rtgsTransfer: false,
    sknTransfer: false,
    bifastTransfer: false,
    onlineBankCode: null,
    rtgsCode: null,
    sknBankCode: null,
    bifastBankCode: null,
    swiftCode: null,
    // ? take out? swiftCode: "",
  });
  const validateBankAlias = useDebounce(formData.bankAlias, 1000);
  const validateOnline = useDebounce(formData.onlineBankCode, 1000);
  const validateBiFast = useDebounce(formData.bifastBankCode, 1000);
  const validateRGTS = useDebounce(formData.rtgsCode, 1000);
  const validateSKN = useDebounce(formData.sknBankCode, 1000);
  const validateSwiftCode = useDebounce(formData.swiftCode, 1000);

  const valBankName = !validateMinMax(formData.bankName, 2, 30);
  const valBankAlias = !validateMinMax(formData.bankAlias, 2, 30);
  const valCodeRgts = !validateMinMax(formData.rtgsCode, 2, 10);
  const valCodeSkn = !validateMinMax(formData.sknBankCode, 2, 10);
  const valCodeBifast = !validateMinMax(formData.bifastBankCode, 2, 10);
  const valCodeOnline = !validateMinMax(formData.onlineBankCode, 2, 3);
  const valCodeSwift = !validateMinMax(formData.swiftCode, 2, 10);

  const vCheckBoxInputs = () => {
    let checkBox = 0;
    let checkInput = 0;

    const biFastInput =
      formData.bifastBankCode !== null && formData.bifastBankCode !== "";
    const onlineTransferInput =
      formData.onlineBankCode !== null && formData.onlineBankCode !== "";
    const rtgsInput = formData.rtgsCode !== null && formData.rtgsCode !== "";
    const sknInput =
      formData.sknBankCode !== null && formData.sknBankCode !== "";

    if (formData.bifastTransfer) checkBox += 1;
    if (formData.onlineTransfer) checkBox += 1;
    if (formData.rtgsTransfer) checkBox += 1;
    if (formData.sknTransfer) checkBox += 1;

    if (biFastInput) checkInput += 1;
    if (onlineTransferInput) checkInput += 1;
    if (rtgsInput) checkInput += 1;
    if (sknInput) checkInput += 1;

    const checked =
      checkBox === checkInput && checkBox !== 0 && checkInput !== 0;
    return !checked;
  };
  const handleBack = () => {
    dispatch(setTypeDetail(null));
    history.push(pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK);
  };

  // const handleValidateSameValueOnline = () => {
  //   let error = false;
  //   if (
  //     formData?.onlineBankCode?.length > 0 &&
  //     formData?.onlineBankCode?.length !== undefined
  //   ) {
  //     if (
  //       formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd() ===
  //         formData.sknBankCode?.replace(/\s\s+/g, " ").trimEnd() ||
  //       formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd() ===
  //         formData.rtgsCode?.replace(/\s\s+/g, " ").trimEnd() ||
  //       formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd() ===
  //         formData.bifastBankCode?.replace(/\s\s+/g, " ").trimEnd() ||
  //       formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd() ===
  //         formData.swiftCode?.replace(/\s\s+/g, " ").trimEnd()
  //     )
  //       error = true;
  //   }
  //   return error;
  // };

  const handleValidateBankAlias = () => {
    const payload = {
      bankAlias:
        validateBankAlias !== null
          ? validateBankAlias?.replace(/\s\s+/g, " ").trim()
          : "",
      isInternational: false,
      oldBankName: dataDetail?.bankShortName,
    };
    if (
      dataDetail?.bankShortName !== formData.bankAlias &&
      formData.bankAlias !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.bankShortName === formData.bankAlias ||
      formData.bankAlias === ""
    ) {
      dispatch(setValidate([]));
    }
  };
  const handleValidateRGTS = () => {
    const payload = {
      isInternational: false,
      rtgsCode:
        validateRGTS !== null
          ? validateRGTS?.replace(/\s\s+/g, " ").trimEnd()
          : "",
      oldBankName: dataDetail?.bankShortName,
    };
    if (
      dataDetail?.rtgsCode !== formData.rtgsCode &&
      formData.rtgsCode !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.rtgsCode === formData.rtgsCode ||
      formData.rtgsCode === ""
    ) {
      dispatch(setValidate([]));
    }
  };
  const handleValidateSKN = () => {
    const payload = {
      isInternational: false,
      sknCode:
        validateSKN !== null
          ? validateSKN?.replace(/\s\s+/g, " ").trimEnd()
          : "",
      oldBankName: dataDetail?.bankShortName,
    };
    if (
      dataDetail?.sknBankCode !== formData.sknBankCode &&
      formData.sknBankCode !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.sknBankCode ===
        formData.sknBankCode?.replace(/\s\s+/g, " ").trimEnd() ||
      formData.sknBankCode === ""
    ) {
      dispatch(setValidate([]));
    }
  };
  const handleValidateBiFast = () => {
    const payload = {
      bifastCode:
        validateBiFast !== null
          ? validateBiFast?.replace(/\s\s+/g, " ").trimEnd()
          : "",
      isInternational: false,
      oldBankName: dataDetail?.bankShortName,
    };
    if (
      dataDetail?.bifastBankCode !== formData.bifastBankCode &&
      formData.bifastBankCode !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.bifastBankCode === formData.bifastBankCode ||
      formData.bifastBankCode === ""
    ) {
      dispatch(setValidate([]));
    }
  };
  const handleValidateSwift = () => {
    const payload = {
      swiftCode:
        validateSwiftCode !== null
          ? validateSwiftCode?.replace(/\s\s+/g, " ").trimEnd()
          : "",
      isInternational: false,
      oldBankName: dataDetail?.bankShortName,
    };
    if (
      dataDetail?.swiftCode !== formData.swiftCode &&
      formData.swiftCode !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.swiftCode === formData.swiftCode ||
      formData.swiftCode === ""
    ) {
      dispatch(setValidate([]));
    }
  };
  const handleValidateOnline = () => {
    const payload = {
      isInternational: false,
      bankCode:
        validateOnline !== null
          ? validateOnline?.replace(/\s\s+/g, " ").trimEnd()
          : "",
      oldBankName: dataDetail?.bankShortName,
    };
    if (
      dataDetail?.onlineBankCode !== formData.onlineBankCode &&
      formData.onlineBankCode !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.onlineBankCode === formData.onlineBankCode ||
      formData.onlineBankCode
    ) {
      dispatch(setValidate([]));
    }
  };

  const handleDisabledOnline = () => {
    let error = false;
    if (
      validate.includes("swift") ||
      (validate.includes(
        formData.sknBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("SKN", 1)) ||
      (validate.includes(
        formData.bifastBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("BI-Fast", 1)) ||
      (validate.includes(formData.rtgsCode?.replace(/\s\s+/g, " ").trimEnd()) &&
        validate.includes("RTGS", 1))
    )
      error = true;
    return error;
  };
  const handleDisabledSkn = () => {
    let error = false;
    if (
      (validate.includes("code") &&
        validate.includes(
          formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd()
        ) &&
        !validate.includes("SKN")) ||
      // formData?.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd() !==
      //   formData?.sknBankCode?.replace(/\s\s+/g, " ").trimEnd())
      (validate.includes(
        formData.bifastBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("BI-Fast", 1)) ||
      (validate.includes(formData.rtgsCode?.replace(/\s\s+/g, " ").trimEnd()) &&
        validate.includes("RTGS", 1)) ||
      (validate.includes(
        formData.swiftCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("swift"))
    )
      error = true;
    return error;
  };

  const handleDisabledRGTS = () => {
    let error = false;
    if (
      (validate.includes("code") &&
        validate.includes(
          formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd()
        ) &&
        !validate.includes("RTGS")) ||
      (validate.includes(
        formData.bifastBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("BI-Fast", 1)) ||
      (validate.includes(
        formData.sknBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("SKN", 1)) ||
      (validate.includes(
        formData.swiftCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("swift"))
    )
      error = true;
    return error;
  };

  const handleDisabledBiFast = () => {
    let error = false;
    if (
      (validate.includes("code") &&
        validate.includes(
          formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd()
        ) &&
        !validate.includes("BI-Fast")) ||
      (validate.includes(formData.rtgsCode?.replace(/\s\s+/g, " ").trimEnd()) &&
        validate.includes("RTGS", 1)) ||
      (validate.includes(
        formData.sknBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("SKN", 1)) ||
      (validate.includes(
        formData.swiftCode?.replace(/\s\s+/g, " ").trimEnd()
      ) &&
        validate.includes("swift"))
    )
      error = true;
    return error;
  };
  const handleDisabledSwift = () => {
    let error = false;
    if (
      (validate.includes("code") &&
        !validate.includes("swift") &&
        validate.includes(
          formData.sknBankCode?.replace(/\s\s+/g, " ").trimEnd()
        )) ||
      validate.includes(
        formData.bifastBankCode?.replace(/\s\s+/g, " ").trimEnd()
      ) ||
      validate.includes(formData.rtgsCode?.replace(/\s\s+/g, " ").trimEnd()) ||
      validate.includes(
        formData.onlineBankCode?.replace(/\s\s+/g, " ").trimEnd()
      )
    )
      error = true;
    return error;
  };

  const handleNext = () => {
    setOpenModalConfirmation(false);
  };
  const addBankDomestic = () => {
    let payload = {
      bankName: formData.bankName,
      bankAlias: formData.bankAlias,
      onlineSwitchingType: formData.onlineSwitchingType,
      // onlineSwitchingTypeSecondary: formData.onlineSwitchingTypeSecondary,
      onlineTransfer: formData.onlineTransfer,
      sknTransfer: formData.sknTransfer,
      rtgsTransfer: formData.rtgsTransfer,
      biFastTransfer: formData.bifastTransfer,
      onlineBankCode: formData.onlineBankCode,
      sknBankCode: formData.sknBankCode,
      rtgsCode: formData.rtgsCode,
      biFastBankCode: formData.bifastBankCode,
      swiftCode: formData.swiftCode,
      isEdit: false,
      // ? take out? swiftCode: formData.swiftCode,
    };
    if (dataDetail !== null) {
      payload = {
        ...payload,
        oldOnlineBankCode: dataDetail?.onlineBankCode,
        isEdit: true,
      };
      dispatch(addDataBankDomestic(payload, codeValidateTask));
    } else {
      payload = { ...payload, requestComment: comment };
      dispatch(addDataBankDomestic(payload, codeValidateTask, handleNext));
    }
  };
  useEffect(() => {
    if (dataDetail !== null) {
      setFormData({
        ...dataDetail,
        onlineSwitchingType:
          dataDetail?.onlineSwitchingType === "AMP" ||
          dataDetail?.onlineSwitchingType === "JALIN"
            ? "JALIN"
            : dataDetail?.onlineSwitchingType === "SAT" ||
              dataDetail?.onlineSwitchingType === "ARTAJASA"
            ? "ARTAJASA"
            : dataDetail?.onlineSwitchingType === "BCA" ||
              dataDetail?.onlineSwitchingType === "PRIMA"
            ? "PRIMA"
            : dataDetail?.onlineSwitchingType === "ALT"
            ? "ALT"
            : null,
        oldOnlineBankCode: dataDetail?.onlineBankCode,
        // onlineSwitchingTypePrimary: dataDetail?.onlineSwitchingType,
        bankAlias: dataDetail?.bankShortName,
        onlineTransfer:
          dataDetail?.onlineBankCode == null ||
          dataDetail?.onlineBankCode === ""
            ? validates
            : !validates,
        sknTransfer:
          dataDetail?.sknBankCode === null || dataDetail?.sknBankCode === ""
            ? validates
            : !validates,
        rtgsTransfer:
          dataDetail?.rtgsCode === null || dataDetail?.rtgsCode === ""
            ? validates
            : !validates,
        bifastTransfer:
          dataDetail?.bifastBankCode === null ||
          dataDetail?.bifastBankCode === ""
            ? validates
            : !validates,
        // swiftTransfer:
        //   dataDetail?.swiftCode === null || dataDetail?.swiftCode === ""
        //     ? validates
        //     : !validates,
      });
    }
    return () => {
      dispatch(setValidate([]));
    };
  }, [dataDetail]);

  // useEffect(() => {
  //   const data = optionsPrimary.filter(
  //     (elm) => elm.value === formData.onlineSwitchingTypePrimary
  //   );
  //   const result = optionsSecondary.map((item) => ({
  //     ...item,
  //     disabled: item?.label === data[0]?.label ? !item.disabled : false,
  //   }));
  //   setOptionsSecondary(result);
  // }, [formData.onlineSwitchingTypePrimary]);

  // useEffect(() => {
  //   const data = optionsSecondary.filter(
  //     (elm) => elm.value === formData.onlineSwitchingTypeSecondary
  //   );
  //   const result = optionsPrimary.map((item) => ({
  //     ...item,
  //     disabled: item?.label === data[0]?.label ? !item.disabled : false,
  //   }));
  //   setOptionsPrimary(result);
  // }, [formData.onlineSwitchingTypeSecondary]);

  useEffect(() => {
    if (validateBankAlias !== null) handleValidateBankAlias();
  }, [validateBankAlias]);

  useEffect(() => {
    if (validateOnline !== null && formData.onlineTransfer) {
      handleValidateOnline();
    }
  }, [validateOnline, formData.onlineTransfer]);

  useEffect(() => {
    if (
      validateBiFast !== null &&
      formData.bifastTransfer &&
      formData.onlineTransfer
    ) {
      handleValidateBiFast();
    }
  }, [validateBiFast, formData.bifastTransfer]);

  useEffect(() => {
    if (
      validateRGTS !== null &&
      formData.rtgsTransfer &&
      formData.onlineTransfer
    ) {
      handleValidateRGTS();
    }
  }, [validateRGTS, formData.rtgsTransfer]);

  useEffect(() => {
    if (validateSwiftCode !== null) {
      handleValidateSwift();
    }
  }, [validateSwiftCode]);

  useEffect(() => {
    if (
      validateSKN !== null &&
      formData.sknTransfer &&
      formData.onlineTransfer
    ) {
      handleValidateSKN();
    }
  }, [validateSKN, formData.sknTransfer]);

  useEffect(() => {
    const data = [];
    if (formData.onlineBankCode !== "" && formData.onlineBankCode !== null) {
      data.splice(0, 0, `${formData.onlineBankCode};`);
    } else {
      data.splice(0, 0, `;`);
    }
    if (formData.sknBankCode !== "" && formData.sknBankCode !== null) {
      data.splice(1, 0, `${formData.sknBankCode};`);
    } else {
      data.splice(1, 0, `;`);
    }
    if (formData.rtgsCode !== "" && formData.rtgsCode !== null) {
      data.splice(2, 0, `${formData.rtgsCode};`);
    } else {
      data.splice(2, 0, `;`);
    }
    if (formData.bifastBankCode !== "" && formData.bifastBankCode !== null) {
      data.splice(3, 0, `${formData.bifastBankCode}`);
    } else {
      data.splice(3);
    }
    setCodeValidationTask(data.join(""));
  }, [formData]);

  return (
    <div className={classes.domesticbankDetail}>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Back"
          />
        }
        onClick={handleBack}
      />
      <Title label={dataDetail !== null ? "Edit Bank" : "Add Bank"} />
      <div className={classes.container}>
        <div className={classes.formcontent}>
          <div style={{ display: "flex", width: "100%" }}>
            <div style={{ marginBottom: 25, width: "100%", paddingRight: 15 }}>
              <div style={{ marginBottom: 25 }}>
                <FormField
                  errorMessage="Min 2 and Max 30 Character!"
                  error={valBankName}
                  label="Bank Name :"
                >
                  <TextField
                    value={formData.bankName}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        bankName: e.target.value,
                      })
                    }
                    placeholder="Bank Mandiri"
                    style={{ width: "100%" }}
                    maxLength="30"
                  />
                </FormField>
              </div>

              <div style={{ marginBottom: 10 }}>
                <span
                  style={{
                    fontFamily: "FuturaBkBT",
                    fontWeight: 500,
                    fonySize: 13,
                    color: "#374062",
                    marginBotttom: 10,
                  }}
                >
                  Transfer Type
                </span>

                <div className={classes.inputGroup}>
                  <div className="checkboxgroup">
                    <CheckboxSingle
                      disabled={
                        validate.includes("alias") || handleDisabledOnline()
                      }
                      name="onlineTransfer"
                      checked={
                        formData.onlineTransfer &&
                        !validate.includes("alias") &&
                        !handleDisabledOnline()
                      }
                      onChange={() =>
                        setFormData({
                          ...formData,
                          onlineTransfer: !formData.onlineTransfer,
                        })
                      }
                    />
                    <span className="span">Online</span>
                  </div>
                  <div
                    style={{
                      marginBottom: 24,
                      width: "100%",
                    }}
                  >
                    <FormField
                      label="Online Code :"
                      errorMessage={
                        validate.includes("code")
                          ? "Code already exists"
                          : "Must 3 Character"
                      }
                      error={
                        valCodeOnline ||
                        (validate.includes("code", 1) &&
                          validate.includes(
                            formData.onlineBankCode?.trimEnd()
                          ) &&
                          !validate.includes("BI-Fast", 1) &&
                          !validate.includes("SKN", 1) &&
                          !validate.includes("RTGS", 1) &&
                          !validate.includes("swift", 1))
                      }
                    >
                      <TextField
                        disabled={
                          !formData.onlineTransfer ||
                          validate.includes("alias") ||
                          handleDisabledOnline()
                        }
                        value={formData.onlineBankCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            onlineBankCode: parseNumber(e.target.value.trim()),
                          })
                        }
                        placeholder="Code Online"
                        style={{ width: "100%" }}
                        maxLength="3"
                      />
                    </FormField>
                  </div>
                </div>

                <div className={classes.inputGroup}>
                  <div className="checkboxgroup">
                    <CheckboxSingle
                      disabled={
                        !formData.onlineTransfer ||
                        validate.includes("alias") ||
                        handleDisabledSkn()
                      }
                      name="sknTransfer"
                      checked={
                        formData.sknTransfer &&
                        formData.onlineTransfer &&
                        !validate.includes("alias") &&
                        !handleDisabledSkn()
                      }
                      onChange={() =>
                        setFormData({
                          ...formData,
                          sknTransfer: !formData.sknTransfer,
                        })
                      }
                    />
                    <span className="span">SKN</span>
                  </div>
                  <div
                    style={{
                      marginBottom: 24,
                      width: "100%",
                    }}
                  >
                    <FormField
                      label="SKN/Clearing code :"
                      errorMessage={
                        validate.includes("code")
                          ? "Code already exists"
                          : "Min 2 and Max 10 Character!"
                      }
                      error={
                        valCodeSkn ||
                        (validate.includes("SKN", 1) &&
                          validate.includes(
                            formData.sknBankCode
                              ?.replace(/\s\s+/g, " ")
                              .trimEnd()
                          ))
                      }
                    >
                      <TextField
                        disabled={
                          !formData.sknTransfer ||
                          !formData.onlineTransfer ||
                          validate.includes("alias") ||
                          handleDisabledSkn()
                        }
                        value={formData.sknBankCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            sknBankCode: parseAlphaNumeric(e.target.value),
                          })
                        }
                        placeholder="SKN/Clearing code"
                        style={{ width: "100%" }}
                        maxLength="10"
                      />
                    </FormField>
                  </div>
                </div>

                <div className={classes.inputGroup}>
                  <div className="checkboxgroup">
                    <CheckboxSingle
                      disabled={
                        !formData.onlineTransfer ||
                        validate.includes("alias") ||
                        handleDisabledRGTS()
                      }
                      name="rtgsTransfer"
                      checked={
                        formData.rtgsTransfer &&
                        formData.onlineTransfer &&
                        !validate.includes("alias") &&
                        !handleDisabledRGTS()
                      }
                      onChange={() =>
                        setFormData({
                          ...formData,
                          rtgsTransfer: !formData.rtgsTransfer,
                        })
                      }
                    />
                    <span className="span">RTGS</span>
                  </div>
                  <div
                    style={{
                      marginBottom: 24,
                      width: "100%",
                    }}
                  >
                    <FormField
                      label="RTGS code :"
                      errorMessage={
                        validate.includes("code")
                          ? "Code already exists"
                          : "Min 2 and Max 10 Character!"
                      }
                      error={
                        valCodeRgts ||
                        (validate.includes("RTGS", 1) &&
                          validate.includes(
                            formData.rtgsCode?.replace(/\s\s+/g, " ")
                          ))
                      }
                    >
                      <TextField
                        disabled={
                          !formData.rtgsTransfer ||
                          !formData.onlineTransfer ||
                          validate.includes("alias") ||
                          handleDisabledRGTS()
                        }
                        value={formData.rtgsCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            rtgsCode: parseAlphaNumeric(e.target.value),
                          })
                        }
                        placeholder="RTGS code"
                        style={{ width: "100%" }}
                        maxLength="10"
                      />
                    </FormField>
                  </div>
                </div>

                <div className={classes.inputGroup}>
                  <div className="checkboxgroup">
                    <CheckboxSingle
                      disabled={
                        !formData.onlineTransfer ||
                        validate.includes("alias") ||
                        handleDisabledBiFast()
                      }
                      // name="biFastTransfe"
                      checked={
                        formData.bifastTransfer &&
                        formData.onlineTransfer &&
                        !validate.includes("alias") &&
                        !handleDisabledBiFast()
                      }
                      onChange={() =>
                        setFormData({
                          ...formData,
                          bifastTransfer: !formData.bifastTransfer,
                        })
                      }
                    />
                    <span className="span">BI-Fast</span>
                  </div>
                  <div
                    style={{
                      marginBottom: 24,
                      width: "100%",
                    }}
                  >
                    <FormField
                      label="BI-Fast Code :"
                      errorMessage={
                        validate.includes("code")
                          ? "Code already exists"
                          : "Min 2 and Max 10 Character!"
                      }
                      error={
                        valCodeBifast ||
                        (validate.includes("BI-Fast", 1) &&
                          validate.includes(
                            formData.bifastBankCode
                              ?.replace(/\s\s+/g, " ")
                              .trimEnd()
                          ))
                      }
                    >
                      <TextField
                        disabled={
                          !formData.bifastTransfer ||
                          !formData.onlineTransfer ||
                          validate.includes("alias") ||
                          handleDisabledBiFast()
                        }
                        value={formData.bifastBankCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            bifastBankCode: parseAlphaNumeric(e.target.value),
                          })
                        }
                        placeholder="BI-Fast code"
                        style={{ width: "100%" }}
                        maxLength="10"
                      />
                    </FormField>
                  </div>
                </div>
              </div>
            </div>

            <div style={{ width: "100%", paddingLeft: 15 }}>
              <div style={{ marginBottom: 25 }}>
                <FormField
                  errorMessage={
                    validate.includes("alias")
                      ? "Name already exists"
                      : "Min 2 and Max 30 Character!"
                  }
                  label="Bank Alias Name :"
                  error={valBankAlias || validate.includes("alias")}
                >
                  <TextField
                    disabled={validate.includes("code")}
                    value={formData.bankAlias}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        bankAlias: e.target.value,
                      })
                    }
                    placeholder="Bank Mandiri"
                    style={{ width: "100%" }}
                    maxLength="30"
                  />
                </FormField>
              </div>
              <div
                style={{
                  marginBottom: 25,
                  width: "100%",
                }}
              >
                <FormField
                  label="BIC :"
                  errorMessage={
                    validate.includes("swift")
                      ? "Code already exists"
                      : "Min 2 and Max 10 Character!"
                  }
                  error={
                    // handleValidateSameValueSwift() ||
                    valCodeSwift ||
                    (validate.includes("swift") &&
                      validate.includes(
                        formData.swiftCode?.replace(/\s\s+/g, " ").trimEnd()
                      ))
                  }
                >
                  <TextField
                    disabled={
                      handleDisabledSwift() || validate.includes("alias")
                    }
                    value={formData.swiftCode}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        swiftCode: parseAlphaNumeric(e.target.value),
                      })
                    }
                    placeholder="BIC"
                    style={{ width: "100%" }}
                    maxLength="10"
                  />
                </FormField>
              </div>
              <div>
                <span
                  style={{
                    fontFamily: "FuturaBkBT",
                    fontWeight: 500,
                    fonySize: 13,
                    color: "#374062",
                    marginBotttom: 10,
                  }}
                >
                  Online Switching:
                </span>

                <div style={{ width: 324 }}>
                  <RadioGroup
                    value={formData.onlineSwitchingType}
                    options={optionsPrimary}
                    onChange={(e) => {
                      setFormData({
                        ...formData,
                        onlineSwitchingType: e.target.value,
                      });
                    }}
                  />
                </div>
              </div>
              {/* <div style={{ marginTop: "8px" }}>
                <span
                  style={{
                    fontFamily: "FuturaBkBT",
                    fontWeight: 500,
                    fonySize: 13,
                    color: "#374062",
                    marginBotttom: 10,
                  }}
                >
                  Type of Secondary Online Switching:
                </span>

                <div style={{ width: 324 }}>
                  <RadioGroup
                    value={formData.onlineSwitchingTypeSecondary}
                    options={optionsSecondary}
                    onChange={(e) => {
                      setFormData({
                        ...formData,
                        onlineSwitchingTypeSecondary: e.target.value,
                      });
                    }}
                  />
                </div>
              </div> */}
            </div>
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <ButtonOutlined
              label="Cancel"
              width="86px"
              style={{ marginRight: 30 }}
              color="#0061A7"
              onClick={handleBack}
            />
          </div>
          <div>
            <GeneralButton
              onClick={() => {
                setOpenModalConfirmation(true);
              }}
              disabled={
                formData.onlineSwitchingType === null ||
                formData.bankName === null ||
                formData.bankAlias === null ||
                formData?.swiftCode === null ||
                formData.onlineSwitchingType === "" ||
                formData.bankName === "" ||
                formData.bankAlias === "" ||
                formData?.swiftCode === "" ||
                valBankName ||
                valBankAlias ||
                valCodeSwift ||
                valCodeBifast ||
                valCodeOnline ||
                valCodeSkn ||
                valCodeRgts ||
                vCheckBoxInputs() ||
                validate.includes("code") ||
                validate.includes("alias")
              }
              label="Save"
            />
          </div>
        </div>
      </div>

      {/* <PopUpComment
        open={openModal}
        handleClose={() => {
          setOpenModal(false);
          setTimeout(() => setComment(""), 500);
        }}
        value={comment}
        onChange={(e) => setComment(e.target.value)}
        onContinue={() => {
          setOpenModal(false);
          setOpenModalConfirmation(true);
        }}
      /> */}

      <DeleteConfirmation
        title="Confirmation"
        message={
          dataDetail !== null
            ? "Are You Sure To Edit Data?"
            : "Are You Sure To Add Data?"
        }
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalConfirmation(false);
          setComment("");
        }}
        onContinue={() => {
          addBankDomestic();
        }}
      />

      <SuccessConfirmation
        isOpen={isOpen}
        message="Data Saved Successfully"
        handleClose={() => {
          dispatch(setTypeSuccess(false));
          setOpenModalConfirmation(false);
          dispatch(setTypeDetail(null));

          history.push(pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK);
        }}
      />
    </div>
  );
};

DomesticBankDetails.defaultProps = {};

export default DomesticBankDetails;
