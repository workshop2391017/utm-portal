import React, { useEffect, useState } from "react";
import { Avatar, Grid, makeStyles, Paper, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import useDebounce from "utils/helpers/useDebounce";
import {
  changeStatusDataBankDomesticDetail,
  deleteDataBankDomesticDetail,
  dowloadCsvBankDomestic,
  getDataBankDomestic,
  setTypeClearError,
  setTypeDetail,
  setTypeSuccess,
} from "stores/actions/domesticBank";
import DeleteConfirmation from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import Toast from "components/BN/Toats";
import Badge from "components/BN/Badge";
import Button from "components/BN/Button/GeneralButton";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import ModalHostErrormapping from "components/BN/Popups/HostErrormapping";
import TableICBB from "components/BN/TableIcBB";
import Title from "components/BN/Title";
import Unduh from "assets/icons/BN/unduh.svg";
import plus from "assets/icons/BN/plus-white.svg";
import reload from "assets/icons/BN/reload.png";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { validateTask } from "stores/actions/validateTaskPortal";
import usePrevious from "utils/helpers/usePrevious";

const DomesticBank = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { data, isLoading, error, isLoadingExcute, isOpen } = useSelector(
    (state) => state.domesticBank
  );
  const [openModal, setOpenModal] = useState(false);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [openModalReloadActive, setOpenModalReloadActive] = useState(false);
  const [dataRow, setDataRow] = useState(null);
  const [dataSearch, setDataSearch] = useState("");
  const [page, setPage] = useState(1);

  const result = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(result);
  const onDeleteItem = () => {
    const payload = {
      onlineBankCode: dataRow?.onlineBankCode,
      isDeleted: true,
    };

    dispatch(deleteDataBankDomesticDetail(payload));
  };
  const onReloadItem = () => {
    const payload = {
      onlineBankCode: dataRow?.onlineBankCode,
    };
    dispatch(changeStatusDataBankDomesticDetail(payload));
  };

  const dataHeader = [
    {
      title: "Bank Code",
      key: "onlineBankCode",
      width: 90,
    },
    {
      title: "Bank Name",
      key: "bankName",
      width: 190,
    },
    {
      title: "Bank Alias Name",
      key: "bankShortName",
      width: 190,
    },
    {
      title: "Transfer Type",
      width: 250,
      render: (rowData) => (
        <div style={{ display: "flex", position: "relative" }}>
          {rowData.onlineBankCode && (
            <Badge
              label="Online"
              type="blue"
              styleBadge={{ marginRight: "5px" }}
            />
          )}

          {rowData.sknBankCode && (
            <Badge
              label="SKN"
              type="orange"
              styleBadge={{ marginRight: "5px" }}
            />
          )}

          {rowData.bifastBankCode && (
            <Badge
              label="RTGS"
              type="red"
              styleBadge={{ marginRight: "5px" }}
            />
          )}

          {rowData.bifastBankCode && (
            <Badge
              label="BI FAST"
              type="purple"
              styleBadge={{ marginRight: "5px" }}
            />
          )}
        </div>
      ),
    },
    {
      title: "Online Switching",
      key: "onlineSwitchingType",
      render: (rowData) =>
        rowData?.onlineSwitchingType === "ALT"
          ? "Alto"
          : rowData?.onlineSwitchingType === "AMP" ||
            rowData?.onlineSwitchingType === "JALIN"
          ? "Jalin"
          : rowData?.onlineSwitchingType === "SAT" ||
            rowData?.onlineSwitchingType === "ARTAJASA"
          ? "Artajasa"
          : rowData?.onlineSwitchingType === "BCA" ||
            rowData?.onlineSwitchingType === "PRIMA"
          ? "Prima"
          : "-",
    },
    // {
    //   title: "Secondary Switching",
    //   key: "onlineSwitchingTypeSecondary",
    // },
    {
      title: "Status",
      width: 60,
      render: (rowData) =>
        rowData.isDeleted ? (
          <span
            style={{
              fontSize: "13px",
              color: "#374062",
              fontFamily: "FuturaBQ",
            }}
          >
            Inactive
          </span>
        ) : (
          <span>Active</span>
        ),
    },

    {
      title: "",
      width: "70px",
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "center",
              // backgroundColor: "red",
            }}
          >
            {rowData.isDeleted ? (
              <Grid>
                <Avatar
                  onClick={() => {
                    dispatch(
                      validateTask(
                        {
                          menuName: validateTaskConstant.BANK_DOMESTIC,
                          code: rowData?.onlineBankCode,
                          id: null,
                          name: rowData?.bankName,
                          validate: true,
                        },
                        {
                          onContinue() {
                            setDataRow(rowData);
                            setOpenModalReloadActive(true);
                          },
                          onError() {
                            // handle callback error
                            // popups callback error sudah dihandle
                            // ex. clear form, redirect, etc
                          },
                        },
                        {
                          redirect: false,
                        }
                      )
                    );
                  }}
                  src={reload}
                  style={{
                    width: "21px",
                    height: "21px",
                    display: "flex",
                    justifyContent: "center",
                    cursor: "pointer",
                    // backgroundColor: "red",
                  }}
                />
              </Grid>
            ) : (
              <GeneralMenu
                rowData={rowData}
                options={["edit", "delete"]}
                labels={{ delete: "Inactive" }}
                onDelete={() => {
                  dispatch(
                    validateTask(
                      {
                        menuName: validateTaskConstant.BANK_DOMESTIC,
                        code: rowData?.onlineBankCode,
                        id: null,
                        name: rowData?.bankName,
                        validate: true,
                      },
                      {
                        onContinue() {
                          setDataRow(rowData);

                          setOpenModalConfirmation(true);
                        },
                        onError() {
                          // handle callback error
                          // popups callback error sudah dihandle
                          // ex. clear form, redirect, etc
                        },
                      },
                      {
                        redirect: false,
                      }
                    )
                  );
                }}
                onEdit={() => {
                  dispatch(
                    validateTask(
                      {
                        menuName: validateTaskConstant.BANK_DOMESTIC,
                        code: rowData?.onlineBankCode,
                        id: null,
                        name: rowData?.bankName,
                        validate: true,
                      },
                      {
                        onContinue() {
                          dispatch(setTypeDetail(rowData));

                          history.push(
                            pathnameCONFIG.GENERAL_MAINTENANCE
                              .DOMESTIC_BANK_DETAIL
                          );
                        },
                        onError() {
                          // handle callback error
                          // popups callback error sudah dihandle
                          // ex. clear form, redirect, etc
                        },
                      },
                      {
                        redirect: false,
                      }
                    )
                  );
                }}
              />
            )}
          </div>
        </React.Fragment>
      ),
    },
  ];

  const useStyles = makeStyles({
    domesticbank: {},
    container: {
      padding: "20px 30px",
    },
    buttonUnduh: {
      cursor: "pointer",
      marginLeft: "20px",
      width: "94px",
      height: "44px",
      borderRadius: "10px",
      border: "1px solid #0061A7",
      padding: "14px 22px",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      "& .row": {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        "& .title": {
          fontSize: "15px",
          fontWeight: 700,
          fontFamily: "FuturaMdBT",
          color: "#0061A7",
        },
      },
    },
  });

  const classes = useStyles();

  const handleAddNew = () => {
    dispatch(setTypeDetail(null));
    history.push(pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK_DETAIL);
  };

  const onLoad = () => {
    const payload = {
      bankName: result.toLowerCase(),
      pageNumber: parseInt(page) - 1,
      pageSize: 10,
      isInternational: false,
    };

    if (page === 1) {
      dispatch(getDataBankDomestic(payload));
    } else if (result !== prevSearch) {
      setPage(1);
    } else {
      dispatch(getDataBankDomestic(payload));
    }
  };

  useEffect(() => {
    if (page === 1) {
      onLoad();
    } else if (result !== prevSearch) {
      setPage(1);
    } else {
      onLoad();
    }
  }, [result, page]);
  // useEffect(() => {
  //   onLoad();
  // }, [result, page]);

  return (
    <div className={classes.domesticbank}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />
      <SuccessConfirmation
        isOpen={isOpen}
        handleClose={() => {
          dispatch(setTypeSuccess(false));
          setOpenModalConfirmation(false);
          setOpenModalReloadActive(false);
        }}
      />

      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure Active the Data?"
        isOpen={openModalReloadActive}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalReloadActive(false);
        }}
        onContinue={() => {
          onReloadItem();
        }}
      />

      <DeleteConfirmation
        message="Are You Sure to Inactive the  Data?"
        title="Confirmation"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onDeleteItem();
        }}
      />
      <ModalHostErrormapping
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />
      <Title label="Domestic Bank">
        {/* ini pengaturan user */}
        <SearchWithDropdown
          style={{
            width: "200px",
          }}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Bank Code, Bank Name, Bank Alias Name"
          placement="bottom"
        />
        <Paper
          className={classes.buttonUnduh}
          elevation={0}
          onClick={() => {
            dispatch(dowloadCsvBankDomestic());
          }}
        >
          <div className="row">
            <img
              src={Unduh}
              alt="Download"
              width={15}
              height={15}
              style={{
                marginRight: "6px",
              }}
            />
            <Typography className="title">CSV</Typography>
          </div>
        </Paper>

        <Button
          label="Add Bank"
          iconPosition="startIcon"
          buttonIcon={<img src={plus} alt="plus" />}
          width="200px"
          height="40px"
          style={{ marginLeft: 30 }}
          onClick={handleAddNew}
        />
      </Title>
      <div className={classes.container}>
        <TableICBB
          isLoading={isLoading}
          headerContent={dataHeader}
          dataContent={data?.content}
          page={page}
          setPage={setPage}
          totalElement={data?.totalElements}
          totalData={data?.totalPages}
        />
      </div>
    </div>
  );
};

DomesticBank.propTypes = {};

DomesticBank.defaultProps = {};

export default DomesticBank;
