import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

// MUI
import { Box, makeStyles, Button, Grid } from "@material-ui/core";

// antd
import { Typography } from "antd";

// components
import Badge from "components/BN/Badge";
import Title from "components/BN/Title";
import Collapse from "components/BN/Collapse";

// assets
import arrowLeft from "assets/icons/BN/buttonkembali.svg";

const InstitusiVaDetail = () => {
  const history = useHistory();

  const [open, setOpen] = useState(false);

  const handleBack = () => history.goBack();

  const { dataDetail } = useSelector((state) => state.viaInstitution);
  console.warn("dataDetail:", dataDetail);
  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    rowcontainer: {
      display: "flex",
      flexDirection: "row",
      //   justifyContent: "space-between",
      marginBottom: "30px",
      //   backgroundColor: "red",
    },
    fieldTitle: {
      fontSize: 13,
      fontWeight: 400,
      fontFamily: "FuturaHvBT",
      color: "#7B87AF",
      marginBottom: "5px",
    },
    subFieldTitle: {
      fontSize: 14,
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      color: "#374062",
    },
    mainContainer: {
      margin: "63px 115px",
      //   margin: "50px",
      borderRadius: "20px",
      overflow: "hidden",
    },
    mainContent: {
      backgroundColor: "#fff",
      padding: "50px",
      "& .title": {
        fontSize: "20px",
        color: "#374062",
        fontFamily: "FuturaMdBT",
        fontWeight: 400,
        marginBottom: "30px",
      },
    },
    titleCode: {
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#7B87AF",
      fontWeight: 400,
    },
    gridTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    gridValue: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    gridValueBold: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: "15px",
      letterSpacing: "0.01em",
      color: "#374062",
    },
    card: {
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      padding: "15px 20px",
      gap: "10px",
      width: "350px",
      height: "140px",
      background: "#FFFFFF",
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
    },
  });

  const classes = useStyles();

  const badgeRender = (badge) => {
    if (badge === "NONBILLING") {
      return (
        <Badge
          type="green"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }

    if (badge === "BILLING") {
      return (
        <Badge
          type="blue"
          styleBadge={{
            width: "90px",
          }}
          label={badge}
        />
      );
    }
  };

  const FieldVirtualAccount = ({ name, val, badge, code }) => (
    <div style={{ flex: 1 }}>
      <div className={classes.fieldTitle}>{name ? <div>{name}</div> : ""}</div>
      <div
        // weight={500}
        size="15px"
        width="350px"
        className={classes.subFieldTitle}
      >
        {code}-{val}
      </div>
      <div>{badgeRender(badge)}</div>
    </div>
  );

  const GridItem = ({ title, value, size = 12 }) => (
    <Grid item xs={size}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title}
        </Grid>
        <Grid item className={classes.gridValue}>
          {value}
        </Grid>
      </Grid>
    </Grid>
  );

  const GridItemParameterSettings = ({
    title,
    value,
    valueBold,
    parentSize = 6,
  }) => (
    <Grid item xs={parentSize !== 12 ? 6 : 12}>
      <Grid container direction="column" spacing={1}>
        <Grid item className={classes.gridTitle}>
          {title ?? <br />}
        </Grid>
        {valueBold && (
          <Grid item className={classes.gridValueBold}>
            {valueBold}
          </Grid>
        )}
        <Grid item className={classes.gridValue}>
          {value !== "BILLING" && value !== "NONBILLING"
            ? value
            : badgeRender(value)}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={handleBack}
      />
      <Title label="Virtual Account Details" />
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">Virtual Account</span>
        </div>
        <div className={classes.mainContent}>
          <Collapse label="Company Information" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Company Name :"
                value={
                  dataDetail?.companyName && dataDetail?.companyName !== ""
                    ? dataDetail?.companyName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Email :"
                value={
                  dataDetail?.email && dataDetail?.email !== ""
                    ? dataDetail?.email
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Address :"
                value={
                  dataDetail?.address && dataDetail?.address !== ""
                    ? dataDetail?.address
                    : "-"
                }
                size={12}
              />
              <GridItem
                title="Regency/City :"
                value={
                  dataDetail?.city && dataDetail?.city !== ""
                    ? dataDetail?.city
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Province :"
                value={
                  dataDetail?.province && dataDetail?.province !== ""
                    ? dataDetail?.province
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Postal Code :"
                value={
                  dataDetail?.postalCode && dataDetail?.postalCode !== ""
                    ? dataDetail?.postalCode
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Business Fields :"
                value={
                  dataDetail?.fields && dataDetail?.fields !== ""
                    ? dataDetail?.fields
                    : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Owner Information" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItem
                title="Owner Name :"
                value={
                  dataDetail?.ownerName && dataDetail?.ownerName !== ""
                    ? dataDetail?.ownerName
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="NPWP :"
                value={
                  dataDetail?.npwp && dataDetail?.npwp !== ""
                    ? dataDetail?.npwp
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Position :"
                value={
                  dataDetail?.position && dataDetail?.position !== ""
                    ? dataDetail?.position
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Section :"
                value={
                  dataDetail?.part && dataDetail?.part !== ""
                    ? dataDetail?.part
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Home Phone Number (Operational) :"
                value={
                  dataDetail?.homePhoneNo && dataDetail?.homePhoneNo !== ""
                    ? dataDetail?.homePhoneNo
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Mobile Phone Number :"
                value={
                  dataDetail?.mobilePhoneNo && dataDetail?.mobilePhoneNo !== ""
                    ? dataDetail?.mobilePhoneNo
                    : "-"
                }
                size={6}
              />
              <GridItem
                title="Fax Number :"
                value={
                  dataDetail?.faxNo && dataDetail?.faxNo !== ""
                    ? dataDetail?.faxNo
                    : "-"
                }
                size={6}
              />
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Parameter Settings" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <Grid container spacing={3}>
              <GridItemParameterSettings
                title="Selected Account :"
                value={
                  dataDetail?.accountNumber && dataDetail?.accountNumber !== ""
                    ? dataDetail?.accountNumber
                    : "-"
                }
                valueBold={
                  dataDetail?.productName && dataDetail?.productName !== ""
                    ? dataDetail?.productName
                    : "-"
                }
                parentSize={12}
              />
              <GridItemParameterSettings
                title="VA Number Length :"
                value={
                  dataDetail?.vaNoLength && dataDetail?.vaNoLength !== ""
                    ? `${dataDetail?.vaNoLength} Characters`
                    : "-"
                }
              />
              <GridItemParameterSettings
                title="Customer ID Length :"
                value={
                  dataDetail?.customerIdLength &&
                  dataDetail?.customerIdLength !== ""
                    ? `${dataDetail?.customerIdLength} Characters`
                    : "-"
                }
              />
              {dataDetail?.paymentBill?.map((el, i) => (
                <GridItemParameterSettings
                  key={i}
                  title={i === 0 ? "Payment Code :" : undefined}
                  value={
                    el?.billingType && el?.billingType !== ""
                      ? el?.billingType
                      : "-"
                  }
                  valueBold={
                    el?.billingCode &&
                    el?.billingName &&
                    el?.billingCode !== "" &&
                    el?.billingName !== ""
                      ? `${el?.billingCode} - ${el?.billingName}`
                      : "-"
                  }
                />
              ))}
            </Grid>

            <div style={{ marginBottom: "30px" }} />
          </Collapse>

          <Collapse label="Branches" defaultOpen>
            <div style={{ marginBottom: "30px" }} />

            <div className={classes.card}>
              <h6 className={classes.gridValueBold}>
                {dataDetail?.branch?.name && dataDetail?.branch?.name !== ""
                  ? dataDetail?.branch?.name
                  : "-"}
              </h6>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span className={classes.gridTitle}>
                  {dataDetail?.branch?.phone && dataDetail?.branch?.phone !== ""
                    ? `Telepon : ${dataDetail?.branch?.phone}`
                    : "Phone : -"}
                </span>
                <span className={classes.gridTitle}>
                  {dataDetail?.branch?.fax && dataDetail?.branch?.fax !== ""
                    ? `Fax : ${dataDetail?.branch?.fax}`
                    : "Fax : -"}
                </span>
              </div>
              <span className={classes.gridTitle}>
                {dataDetail?.branch?.address &&
                dataDetail?.branch?.address !== ""
                  ? dataDetail?.branch?.address
                  : "-"}
              </span>
            </div>
            <div style={{ marginBottom: "30px" }} />
          </Collapse>
          <Box sx={{ margin: "10px auto" }} />
        </div>
      </div>
    </div>
  );
};

InstitusiVaDetail.propTypes = {};

InstitusiVaDetail.defaultProps = {};

export default InstitusiVaDetail;
