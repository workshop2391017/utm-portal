import { makeStyles } from "@material-ui/core";
import TagsGroup from "components/BN/Tags/TagsGroup";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// components
import { pathnameCONFIG } from "configuration";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import useDebounce from "utils/helpers/useDebounce";
import { has } from "lodash";
import Toast from "components/BN/Toats";
import DeletePopup from "../../../../components/BN/Popups/Delete";
import DeleteConfirmation from "../../../../components/BN/Popups/DeleteConfirmation";
import ModalMerchant from "../../../../components/BN/Popups/EditMerchant";
import SuccessConfirmation from "../../../../components/BN/Popups/SuccessConfirmation";
import Button from "../../../../components/BN/Button/GeneralButton";
import Menu from "../../../../components/BN/Menus/TabMenuHostErrorMapping";
import TableICBB from "../../../../components/BN/TableIcBB";
import Search from "../../../../components/BN/Search/SearchWithoutDropdown";
import GeneralMenu from "../../../../components/BN/Menus/GeneralMenu";

// assets
import plus from "../../../../assets/icons/BN/plus-white.svg";

// component
import Title from "../../../../components/BN/Title";

import {
  getDataInstitutionVIa,
  getDataInstitutionVIaDetail,
  setTypeClearError,
} from "../../../../stores/actions/viaInstitution";

const dataDummyTableCheckingAccount = [
  {
    companyId: "120021991928",
    companyName: "PT. Sampoerna Tbk",
    requestDate: "12/10/2021",
    username: "Rahardian Maulana",
    accountNumber: "102901212102",
  },
  {
    companyId: "120021991928",
    companyName: "PT. Sampoerna Tbk",
    requestDate: "12/10/2021",
    username: "Rahardian Maulana",
    accountNumber: "102901212102",
  },
  {
    companyId: "120021991928",
    companyName: "PT. Sampoerna Tbk",
    requestDate: "12/10/2021",
    username: "Rahardian Maulana",
    accountNumber: "102901212102",
  },
  {
    companyId: "120021991928",
    companyName: "PT. Sampoerna Tbk",
    requestDate: "12/10/2021",
    username: "Rahardian Maulana",
    accountNumber: "0120121021029",
  },
  {
    companyId: "120021991928",
    companyName: "PT. Sampoerna Tbk",
    requestDate: "12/10/2021",
    username: "Rahardian Maulana",
    accountNumber: "102901212102",
  },
];

const dataHeader = ({ history, dispatch }) => [
  {
    title: "Company ID",
    // headerAlign: "left",
    // align: "left",
    key: "companyId",
    // render: (rowData) => <span>{rowData.id}</span>,
  },
  {
    title: "Company Name",
    key: "companyName",
    // render: (rowData) => <span>{rowData.name}</span>,
  },
  {
    title: "Date",
    key: "date",
    render: (rowData) => {
      const tempDate = rowData?.date?.split(" ");
      const date = `${tempDate[2]} ${tempDate[1]} ${
        tempDate[tempDate.length - 1]
      } ${tempDate[3]}`;

      return <div>{moment(date).format("DD-MM-YYYY | HH:mm:ss")}</div>;
    },
  },
  {
    title: "Virtual Account Name",
    key: "virtualAccountName",
  },
  {
    title: "Virtual Account Number",
    key: "virtualAccountNumber",
  },

  {
    title: "",
    render: (rowData) => (
      <React.Fragment>
        <div
          style={{
            display: "flex",
            width: "100%",
            justifyContent: "flex-end",
            // backgroundColor: "red",
          }}
        >
          <button
            style={{
              border: "none",
              backgroundColor: "#0061A7",
              color: "#fff",
              borderRadius: "6px",
              padding: "6px 10px",
              cursor: "pointer",
            }}
            onClick={() => {
              const payload = {
                companyName: rowData?.companyName,
                corporateVaId: rowData?.corporateProfileId,
                registrationNumber: rowData?.registrationNumber,
              };

              dispatch(getDataInstitutionVIaDetail(payload));

              history.push(
                pathnameCONFIG.GENERAL_MAINTENANCE.INSTITUSI_VA_DETAIL
              );
            }}
          >
            View Details
          </button>
        </div>
      </React.Fragment>
    ),
  },
];

const InstitusiVa = () => {
  const history = useHistory();
  const [dataSearch, setDataSearch] = useState("");
  const [page, setPage] = useState(1);
  const hasil = useDebounce(dataSearch || null, 1500);
  const dispatch = useDispatch();

  const { data, isLoading, error } = useSelector(
    (state) => state.viaInstitution
  );

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();

  const clickPush = () => {
    document.scrollingElement.scrollTop = 0;
  };

  useEffect(() => {
    const payload = {
      corporateId: hasil,
      corporateName: hasil,
      pageNumber: page - 1,
      pageSize: 10,
    };

    dispatch(getDataInstitutionVIa(payload));
  }, [hasil, page]);

  return (
    <div className={classes.institusiva}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />
      <Title label="VA Institution">
        <Search
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Company ID, Company Name"
          placement="bottom"
        />
      </Title>
      <div className={classes.container}>
        <TableICBB
          headerContent={dataHeader({ history, dispatch })}
          dataContent={data?.vaInstitutionDtoList}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalElement={data?.totalElements}
          totalData={data?.totalPages}
        />
      </div>
      \
    </div>
  );
};

InstitusiVa.propTypes = {};

InstitusiVa.defaultProps = {};

export default InstitusiVa;
