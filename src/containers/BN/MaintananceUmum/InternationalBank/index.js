import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { Avatar, Grid, Typography, makeStyles } from "@material-ui/core";

import { pathnameCONFIG } from "configuration";

import {
  DeleteBankInternationalBank,
  getAllDataInternationalBank,
  ReloadActiveInternationalBank,
  setTypeClearError,
  setTypeDetailEdit,
  setTypeIsOpen,
} from "stores/actions/internationalBank";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { validateTask } from "stores/actions/validateTaskPortal";

import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";

import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import InternationalBankDetail from "components/BN/Popups/InternationalBankDetail";
import Toast from "components/BN/Toats";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeleteConfirmation from "components/BN/Popups/Delete";
import Badge from "components/BN/Badge";
import Button from "components/BN/Button/GeneralButton";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import TableICBB from "components/BN/TableIcBB";
import Title from "components/BN/Title";

import { ReactComponent as Plus } from "assets/icons/BN/plus-white.svg";
import reload from "assets/icons/BN/reload.png";

const buttonStyle = {
  marginLeft: 9,
  fontSize: "15px",
  fontFamily: "FuturaMdBT",
  fontWeight: 700,
  color: "white",
  borderRadius: 10,
};
const inactiveColor = { color: "#D14848" };
const activeColor = { color: "#0061A7" };

const useStyles = makeStyles((theme) => ({
  page: {
    backgroundColor: "#F4F7FB",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    padding: "0 20px",
  },
  typography: {
    ...theme.typography.subtitle2,
    letterSpacing: "0.03em",
  },
}));

const InternationalBank = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();

  const { data, isLoading, error, isOpen } = useSelector(
    (state) => state.internationalBank
  );

  const [openModal, setOpenModal] = useState(false);
  const [openModalReloadActive, setOpenModalReloadActive] = useState(false);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [page, setPage] = useState(1);
  const [dataDetail, setDataDetail] = useState({});
  const [oneItem, setOneItem] = useState(null);

  const [dataSearch, setDataSearch] = useState("");
  const resultSearch = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(resultSearch);

  const onReloadItem = () => {
    const payload = {
      onlineBankCode: oneItem?.onlineBankCode,
      isInternational: true,
    };

    dispatch(ReloadActiveInternationalBank(payload));
  };

  const onDeleteItem = () => {
    const payload = {
      onlineBankCode: oneItem?.onlineBankCode,
      isInternational: true,
      isDeleted: true,
    };
    dispatch(DeleteBankInternationalBank(payload));
  };

  const dataHeader = [
    {
      title: "BIC",
      render: (rowData) => (
        <span className={classes.typography}>{rowData.swiftCode}</span>
      ),
    },
    {
      title: "Bank Name",
      render: (rowData) => (
        <span className={classes.typography}>{rowData.bankName}</span>
      ),
    },
    {
      title: "Bank Alias Name",
      render: (rowData) => (
        <span className={classes.typography}>{rowData.bankShortName}</span>
      ),
    },
    {
      title: "Country",
      render: (rowData) => (
        <span className={classes.typography}>{rowData.country}</span>
      ),
    },
    {
      title: "Currency",
      render: (rowData) => (
        <span className={classes.typography}>{rowData.currencyCode}</span>
      ),
    },
    {
      title: "Status",
      render: (rowData) =>
        rowData.isDeleted ? (
          <span className={classes.typography} style={inactiveColor}>
            Inactive
          </span>
        ) : (
          <span className={classes.typography} style={activeColor}>
            Active
          </span>
        ),
    },

    {
      title: "",
      width: "70px",
      render: (rowData) => (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            width: "100%",
          }}
        >
          {rowData.isDeleted ? (
            <Avatar
              onClick={() =>
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.INTERNATIONAL_BANK,
                      code: rowData?.onlineBankCode,
                      id: null,
                      name: rowData?.bankName,
                      validate: true,
                    },
                    {
                      onContinue() {
                        setOneItem(rowData);
                        setOpenModalReloadActive(true);
                      },
                      onError() {
                        // handle callback error
                        // popups callback error sudah dihandle
                        // ex. clear form, redirect, etc
                      },
                    },
                    {
                      redirect: false,
                    }
                  )
                )
              }
              src={reload}
              style={{
                width: "21px",
                height: "21px",
                display: "flex",
                justifyContent: "center",
                cursor: "pointer",
              }}
            />
          ) : (
            <GeneralMenu
              openModal={openModal}
              close={() => setOpenModal(false)}
              rowData={rowData}
              options={["edit", "inactive", "detail"]}
              onDelete={() => {
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.INTERNATIONAL_BANK,
                      code: rowData?.onlineBankCode,
                      id: null,
                      name: rowData?.bankName,
                      validate: true,
                    },
                    {
                      onContinue() {
                        setOneItem(rowData);
                        setOpenModalConfirmation(true);
                      },
                      onError() {
                        // handle callback error
                        // popups callback error sudah dihandle
                        // ex. clear form, redirect, etc
                      },
                    },
                    {
                      redirect: false,
                    }
                  )
                );
              }}
              onEdit={() => {
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.INTERNATIONAL_BANK,
                      code: rowData?.onlineBankCode,
                      id: null,
                      name: rowData?.bankName,
                      validate: true,
                    },
                    {
                      onContinue() {
                        dispatch(setTypeDetailEdit(rowData));
                        history.push(
                          pathnameCONFIG.GENERAL_MAINTENANCE
                            .INTENRATIONAL_BANK_ADD
                        );
                      },
                      onError() {
                        // handle callback error
                        // popups callback error sudah dihandle
                        // ex. clear form, redirect, etc
                      },
                    },
                    {
                      redirect: false,
                    }
                  )
                );
              }}
              onDetails={() => {
                setDataDetail(rowData);
                setOpenModal(true);
              }}
            />
          )}
        </div>
      ),
    },
  ];

  const handleAddNew = () => {
    history.push(pathnameCONFIG.GENERAL_MAINTENANCE.INTENRATIONAL_BANK_ADD);
  };

  useEffect(() => {
    const payload = {
      bankName: resultSearch,
      isInternational: true,
      pageNumber: parseInt(page) - 1,
      pageSize: 10,
    };

    if (page === 1) {
      dispatch(getAllDataInternationalBank(payload));
    } else if (resultSearch !== prevSearch) {
      setPage(1);
    } else {
      dispatch(getAllDataInternationalBank(payload));
    }
  }, [resultSearch, page]);

  return (
    <Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />
      <Title label="International Bank">
        <SearchWithDropdown
          style={{ width: 200 }}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="BIC, Bank Name, Bank Alias Name"
          placement="bottom"
        />
        <Button
          label="Add Bank"
          iconPosition="startIcon"
          buttonIcon={<Plus />}
          width={168}
          height={44}
          style={buttonStyle}
          onClick={handleAddNew}
        />
      </Title>
      <div className={classes.page}>
        <div className={classes.container}>
          <TableICBB
            headerContent={dataHeader}
            dataContent={data?.content}
            isLoading={isLoading}
            page={page}
            setPage={setPage}
            totalElement={data?.totalElements}
            totalData={data?.totalPages}
          />
        </div>
        <InternationalBankDetail
          isOpen={openModal}
          handleClose={() => setOpenModal(false)}
          // onContinue,
          title="International Bank Details"
          rowData={dataDetail}
        />

        <SuccessConfirmation
          isOpen={isOpen}
          handleClose={() => {
            dispatch(setTypeIsOpen(false));
            setOpenModalConfirmation(false);
            setOpenModalReloadActive(false);
          }}
        />
        <DeleteConfirmation
          title="Confirmation"
          message="Are You Sure Active the Data?"
          isOpen={openModalReloadActive}
          loading={isLoading}
          handleClose={() => {
            setOpenModalReloadActive(false);
          }}
          onContinue={onReloadItem}
        />

        <DeleteConfirmation
          message="Are You Sure You to Inactive the Data?"
          isOpen={openModalConfirmation}
          handleClose={() => {
            setOpenModalConfirmation(false);
          }}
          onContinue={onDeleteItem}
        />
      </div>
    </Fragment>
  );
};

InternationalBank.propTypes = {};

InternationalBank.defaultProps = {};

export default InternationalBank;
