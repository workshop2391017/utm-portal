import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, Button, CircularProgress } from "@material-ui/core";
import { useHistory } from "react-router-dom";

// component
import {
  addDataBankDomestic,
  getDataBankDomesticDetail,
} from "stores/actions/domesticBank";
import Title from "../../../../../components/BN/Title";
import ButtonOutlined from "../../../../../components/BN/Button/ButtonOutlined";
import GeneralButton from "../../../../../components/BN/Button/GeneralButton";
import TextField from "../../../../../components/BN/TextField/AntdTextField";
import CheckboxSingle from "../../../../../components/SC/Checkbox/CheckboxSingle";
import RadioGroup from "../../../../../components/BN/Radio/RadioGroup";
import SuccessConfirmation from "../../../../../components/BN/Popups/SuccessConfirmation";
import SelectGroup from "../../../../../components/BN/Select/SelectWithSearch";
// assets
import plus from "../../../../../assets/icons/BN/plus-white.svg";
import arrowLeft from "../../../../../assets/icons/BN/buttonkembali.svg";

const InternationalBankUbah = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { dataDetail, isLoading } = useSelector((state) => state.domesticBank);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [dataDetailState, setDataDetailState] = useState({});
  const [isLoadingState, setIsLoadingState] = useState(1);

  const [openModal, setOpenModal] = useState(false);
  const [selectedIds, setSelectedIds] = useState([]);

  const [formData, setFormData] = useState({
    bankName: "",
    biFastTransfer: false,
    biFastBankCode: "",
    onlineBankCode: "",
    onlineSwitchingType: "",
    onlineTransfer: false,
    rtgsCode: "",
    rtgsTransfer: false,
    sknBankCode: "",
    sknTransfer: false,
    swiftCode: "",
  });

  const useStyles = makeStyles({
    domesticbankDetail: {},
    container: {
      margin: "20px 30px",
      backgroundColor: "#fff",
      borderRadius: "10px",
      padding: 40,
      marginBottom: 52,
    },
    formcontent: {
      minHeight: 500,
    },
    inputGroup: {
      display: "flex",
      alignItems: "flex-star",
      "& .checkboxgroup": {
        width: 120,
        marginRight: 40,
        marginTop: 16,
        fontFamily: "FuturaBkBT",
        fontSize: 15,
        display: "flex",
        "& .span": {
          marginLeft: 10,
        },
      },
    },
  });

  const classes = useStyles();

  useEffect(() => {
    setFormData({
      bankName: dataDetail?.bankName,
      biFastTransfer: dataDetail?.transferBiFastAllowed,
      biFastBankCode: dataDetail?.biFastCode,
      onlineBankCode: dataDetail?.bankCode,
      onlineSwitchingType: dataDetail?.bankNetwork,
      onlineTransfer: dataDetail.transferOnlineAllowed,
      rtgsCode: dataDetail?.rtgsCode,
      rtgsTransfer: dataDetail?.transferRtgsAllowed,
      sknBankCode: dataDetail?.sknCode,
      sknTransfer: dataDetail?.transferSknAllowed,
      swiftCode: dataDetail?.swiftCode,
    });
    setIsLoadingState(isLoading);
  }, [dataDetail, isLoading]);

  const clickPush = () => {
    document.scrollingElement.scrollTop = 0;
  };

  const handleBack = () => history.goBack();

  const addBankDomestic = () => {
    console.warn("payload:", formData);
    const hasil = addDataBankDomestic(formData)(dispatch);
    hasil
      .then((res) => {
        if (res === true) {
          setOpenSuccess(true);
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };

  // console.warn("dataDetail:", dataDetail);
  // console.warn("isLoading:", isLoading);
  // console.warn("dataStateDetail:", dataDetailState);
  // console.warn("formData:", formData);

  return (
    <div className={classes.domesticbankDetail}>
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={handleBack}
      />
      <Title label="Ubah Bank" />
      {isLoadingState === 1 ? (
        <CircularProgress
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "30%",
            marginLeft: "50%",
          }}
        />
      ) : (
        <div className={classes.container}>
          <div className={classes.formcontent}>
            <div style={{ display: "flex", width: "100%" }}>
              <div
                style={{ marginBottom: 25, width: "100%", paddingRight: 15 }}
              >
                <div style={{ marginBottom: 25 }}>
                  <p style={{ marginBottom: 2 }}>Nama Bank :</p>
                  <TextField
                    value={formData?.bankName}
                    onChange={(e) =>
                      setFormData({ ...formData, bankName: e.target.value })
                    }
                    placeholder="Bank Mandiri"
                    style={{ width: "100%" }}
                  />
                </div>

                <div style={{ marginBottom: 10 }}>
                  <span
                    style={{
                      fontFamily: "Futura",
                      fintWeight: 500,
                      fonySize: 13,
                      color: "#374062",
                    }}
                  >
                    Tipe Bank
                  </span>

                  <div className={classes.inputGroup}>
                    <div className="checkboxgroup">
                      <CheckboxSingle
                        name="onlineTransfer"
                        checked={formData.onlineTransfer}
                        onChange={() =>
                          setFormData({
                            ...formData,
                            onlineTransfer: !formData.onlineTransfer,
                          })
                        }
                      />
                      <span className="span">Online</span>
                    </div>
                    <div
                      style={{
                        marginBottom: 24,
                        width: "100%",
                      }}
                    >
                      <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                      <TextField
                        value={formData.onlineBankCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            onlineBankCode: e.target.value,
                          })
                        }
                        placeholder="Bank Mandiri"
                        style={{ width: "100%" }}
                      />
                    </div>
                  </div>

                  <div className={classes.inputGroup}>
                    <div className="checkboxgroup">
                      <CheckboxSingle
                        name="sknTransfer"
                        checked={formData.sknTransfer}
                        onChange={() =>
                          setFormData({
                            ...formData,
                            sknTransfer: !formData.sknTransfer,
                          })
                        }
                      />
                      <span className="span">SKN</span>
                    </div>
                    <div
                      style={{
                        marginBottom: 24,
                        width: "100%",
                      }}
                    >
                      <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                      <TextField
                        value={formData.sknBankCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            sknBankCode: e.target.value,
                          })
                        }
                        placeholder="Bank Mandiri"
                        style={{ width: "100%" }}
                      />
                    </div>
                  </div>

                  <div className={classes.inputGroup}>
                    <div className="checkboxgroup">
                      <CheckboxSingle
                        name="rtgsTransfer"
                        checked={formData.rtgsTransfer}
                        onChange={() =>
                          setFormData({
                            ...formData,
                            rtgsTransfer: !formData.rtgsTransfer,
                          })
                        }
                      />
                      <span className="span">RTGS</span>
                    </div>
                    <div
                      style={{
                        marginBottom: 24,
                        width: "100%",
                      }}
                    >
                      <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                      <TextField
                        value={formData.rtgsCode}
                        onChange={(e) =>
                          setFormData({ ...formData, rtgsCode: e.target.value })
                        }
                        placeholder="Bank Mandiri"
                        style={{ width: "100%" }}
                      />
                    </div>
                  </div>

                  <div className={classes.inputGroup}>
                    <div className="checkboxgroup">
                      <CheckboxSingle
                        name="biFastTransfe"
                        checked={formData.biFastTransfer}
                        onChange={() =>
                          setFormData({
                            ...formData,
                            biFastTransfer: !formData.biFastTransfer,
                          })
                        }
                      />
                      <span className="span">BI-Fast</span>
                    </div>
                    <div
                      style={{
                        marginBottom: 24,
                        width: "100%",
                      }}
                    >
                      <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                      <TextField
                        value={formData.biFastBankCode}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            biFastBankCode: e.target.value,
                          })
                        }
                        placeholder="Bank Mandiri"
                        style={{ width: "100%" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ width: "100%", paddingLeft: 15 }}>
                <div style={{ marginBottom: 25 }}>
                  <p style={{ marginBottom: 2 }}>Negara :</p>
                  <SelectGroup
                    placeholder="pilih provinsi"
                    options={[
                      {
                        label: "Pilih Negara",
                        value: "Malasiya",
                      },
                    ]}
                    // value="ACEH"
                    value="Malasiya"
                    name="provinsi"
                    style={{
                      width: "100%",
                      height: "40px",
                      marginRight: "50px",
                    }}
                    onSelect={(e) => {
                      console.warn("e:", e);
                    }}
                  />
                </div>
                <div style={{ marginBottom: 25 }}>
                  <p style={{ marginBottom: 2 }}>Kota :</p>
                  <SelectGroup
                    placeholder="pilih Kota"
                    options={[
                      {
                        label: "Pilih Negara",
                        value: "Johor",
                      },
                    ]}
                    // value="ACEH"
                    value="Johor"
                    name="provinsi"
                    style={{
                      width: "100%",
                      height: "40px",
                      marginRight: "50px",
                    }}
                    onSelect={(e) => {
                      console.warn("e:", e);
                    }}
                  />
                </div>
                <div style={{ marginBottom: 25 }}>
                  <p style={{ marginBottom: 2 }}>Kode Swift :</p>
                  <TextField
                    value={formData.swiftCode}
                    onChange={(e) =>
                      setFormData({ ...formData, swiftCode: e.target.value })
                    }
                    placeholder="2543"
                    style={{ width: "100%" }}
                  />
                </div>

                <div>
                  <span
                    style={{
                      fontFamily: "Futura",
                      fintWeight: 500,
                      fonySize: 13,
                      color: "#374062",
                      marginBotttom: 10,
                    }}
                  >
                    Jenis Online Switching
                  </span>

                  <div style={{ width: 324 }}>
                    <RadioGroup
                      value={formData.onlineSwitchingType}
                      options={[
                        { label: "Jalin", value: "JALIN" },
                        { label: "Alto", value: "ALTO" },
                        { label: "Artajasa", value: "AMP" },
                        { label: "Prima", value: "  PRIMA" },
                      ]}
                      onChange={(e) =>
                        setFormData({
                          ...formData,
                          onlineSwitchingType: e.target.value,
                        })
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>
              <ButtonOutlined
                label="Batal"
                width="86px"
                // height="40px"
                style={{ marginRight: 30 }}
                color="#0061A7"
                //   onClick={() => setDeleteModal(true)}
                //   disabled={isLoadingSubmit}
              />
            </div>
            <div>
              <GeneralButton
                // onClick={() => setOpenSuccess(true)}
                onClick={addBankDomestic}
                label={
                  // isLoadingSubmit ? (
                  //   <CircularProgress size={20} color="primary" />
                  // ) : (
                  //   "Setujui"
                  // )

                  "Simpan"
                }
                // width="157px"
                // height="40px"
                //   onClick={() => handleApprove(SETUJUI_PENGGUNA)}
                //   disabled={isLoadingSubmit}
              />
            </div>
          </div>
        </div>
      )}

      <SuccessConfirmation
        isOpen={openSuccess}
        handleClose={() => setOpenSuccess(false)}
      />
    </div>
  );
};

InternationalBankUbah.propTypes = {};

InternationalBankUbah.defaultProps = {};

export default InternationalBankUbah;
