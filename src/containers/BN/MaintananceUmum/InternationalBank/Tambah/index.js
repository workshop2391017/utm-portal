import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { makeStyles, Button, Typography } from "@material-ui/core";

import {
  AddSaveInternationalBank,
  getAllDataCurrencyInternationalBank,
  getValidateCreate,
  setTypeDetailEdit,
  setTypeIsOpen,
  setValidate,
} from "stores/actions/internationalBank";

import { parseTextNumFirstSpecialChar, validateMinMax } from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";

import FormField from "components/BN/Form/InputGroupValidation";
import DeleteConfirmation from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Footer from "components/BN/Footer";
import AntdTextField from "components/BN/TextField/AntdTextField";
import SelectGroup from "components/BN/Select/SelectWithSearch";

const useStyles = makeStyles((theme) => ({
  backButton: {
    ...theme.typography.backButton,
  },
  page: {
    backgroundColor: "#F4F7FB",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    "& .card": {
      backgroundColor: "white",
      display: "flex",
      margin: "0 20px",
      marginTop: 33,
      padding: 40,
      gap: 30,
      borderRadius: 10,
      "& .leftCard": {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        gap: 20,
      },
      "& .rightCard": {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        gap: 20,
        "& .bank-alias-bic--container": {
          display: "flex",
          gap: 20,
        },
      },
    },
  },
}));

const InternationalBankTambah = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { dataCurrencyList, dataDetail, isOpen, isLoading, validate } =
    useSelector(({ internationalBank }) => internationalBank);

  const [codeValidateTask, setCodeValidationTask] = useState("");
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [dropDownCurrency, setDropDownCurrency] = useState([]);
  const [formData, setFormData] = useState({
    bankName: "",
    bankAlias: "",
    country: "",
    city: "",
    currency: "",
    swiftCode: "",
  });

  const valSwiftCode = !validateMinMax(formData.swiftCode, 2, 10);
  const valBankName = !validateMinMax(formData.bankName, 2, 30);
  const valBankAlias = !validateMinMax(formData.bankAlias, 2, 30);
  const valCountry = !validateMinMax(formData.country, 3, 100);
  const valCity = !validateMinMax(formData.city, 3, 100);

  const validateBankAlias = useDebounce(formData?.bankAlias, 1000);
  const validateSwiftCode = useDebounce(formData?.swiftCode, 1000);

  const handleValidateBankAlias = () => {
    const payload = {
      bankAlias: validateBankAlias !== null ? validateBankAlias.trim() : "",
      bankName: "",
      swiftCode: "",
      isInternational: true,
    };
    if (
      dataDetail?.bankShortName !== formData.bankAlias &&
      formData.bankAlias !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.bankShortName === formData.bankAlias ||
      formData.bankAlias === ""
    ) {
      dispatch(setValidate([]));
    }
  };

  const handleValidateSwiftCode = () => {
    const payload = {
      bankAlias: "",
      bankName: "",
      swiftCode: validateSwiftCode !== null ? validateSwiftCode : "",
      isInternational: true,
    };
    if (
      dataDetail?.swiftCode !== formData.swiftCode &&
      formData.swiftCode !== ""
    ) {
      dispatch(getValidateCreate(payload));
    }
    if (
      dataDetail?.swiftCode === formData.swiftCode ||
      formData.swiftCode === ""
    ) {
      dispatch(setValidate([]));
    }
  };

  const handleDisabledSave = () => {
    const data = { ...formData };
    const error = Object.keys(data).some((elm) => !data[elm]);

    return error;
  };

  useEffect(() => {
    if (validateSwiftCode !== null) {
      handleValidateSwiftCode();
    }
  }, [validateSwiftCode]);

  useEffect(() => {
    if (validateBankAlias !== null) {
      handleValidateBankAlias();
    }
  }, [validateBankAlias]);

  useEffect(() => {
    const payload = { currencyList: null };
    dispatch(getAllDataCurrencyInternationalBank(payload));

    return () => {
      dispatch(setTypeDetailEdit(null));
    };
  }, []);

  useEffect(() => {
    const resultMapp = dataCurrencyList.map((item) => ({
      ...item,
      label: `${item.currencyCode} - ${item.nameEng}`,
      value: item.currencyCode,
    }));

    setDropDownCurrency(resultMapp);
  }, [dataCurrencyList]);

  const handleBack = () => {
    dispatch(setTypeDetailEdit(null));
    history.goBack();
  };

  useEffect(() => {
    if (dataDetail !== null) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        bankName: dataDetail?.bankName,
        bankAlias: dataDetail?.bankShortName,
        country: dataDetail?.country,
        city: dataDetail?.city,
        currency: dataDetail?.currencyCode,
        swiftCode: dataDetail?.swiftCode,
      }));
    }
  }, [dataDetail]);

  useEffect(() => {
    const data = [];
    if (formData.onlineBankCode !== "" && formData.bankCode !== null) {
      data.splice(0, 0, `${formData.bankCode};`);
    } else {
      data.splice(0, 0, `;`);
    }
    if (formData.swiftCode !== "" && formData.swiftCode !== null) {
      data.splice(1, 0, `${formData.swiftCode}`);
    } else {
      data.splice(1);
    }

    setCodeValidationTask(data.join(""));
  }, [formData]);

  const handleNext = () => {
    setOpenModalConfirmation(false);
  };

  const onSaveHandler = () => {
    if (dataDetail !== null) {
      const payload = {
        onlineBankCode: "-",
        bankName: formData.bankName,
        bankAlias: formData.bankAlias,
        city: formData.city,
        country: formData.country,
        currencyCode: formData.currency,
        swiftCode: formData.swiftCode,
        isInternational: true,
        isEdit: true,
        oldOnlineBankCode: dataDetail?.onlineBankCode,
      };

      dispatch(AddSaveInternationalBank(payload, codeValidateTask));
    } else {
      const payload = {
        onlineBankCode: "-",
        bankName: formData.bankName,
        bankAlias: formData.bankAlias,
        city: formData.city,
        country: formData.country,
        currencyCode: formData.currency,
        swiftCode: formData.swiftCode,
        isInternational: true,
        oldOnlineBankCode: dataDetail?.onlineBankCode,
      };

      dispatch(AddSaveInternationalBank(payload, codeValidateTask, handleNext));
    }
    setOpenModalConfirmation(false);
  };

  return (
    <div className={classes.page}>
      <div style={{ padding: "8px 20px 0 20px" }}>
        <Button
          disableElevation
          component="button"
          size="small"
          startIcon={<ArrowLeft />}
          className={classes.backButton}
          onClick={handleBack}
        >
          Back
        </Button>
        <Typography variant="h2">{dataDetail ? "Edit" : "Add"} Bank</Typography>
      </div>
      <div className="card">
        <div className="leftCard">
          <FormField
            errorMessage="Min 2 and Max 30 Characters!"
            error={valBankName}
            label="Bank Name :"
          >
            <AntdTextField
              style={{ width: "100%" }}
              disabled={validate.includes("alias") || validate.includes("code")}
              placeholder="Bank Name"
              value={formData.bankName}
              maxLength="30"
              onChange={(e) =>
                setFormData((prevFormData) => ({
                  ...prevFormData,
                  bankName: parseTextNumFirstSpecialChar(
                    e.target.value,
                    /[^A-Za-z0-9-:,'(.)& ]*$/g
                  ),
                }))
              }
            />
          </FormField>
          <FormField
            errorMessage="Min 3 and Max 100 Characters!"
            error={valCountry}
            label="Country :"
          >
            <AntdTextField
              disabled={validate.includes("alias") || validate.includes("code")}
              style={{ width: "100%" }}
              placeholder="Country"
              maxLength="100"
              value={formData.country}
              onChange={(e) =>
                setFormData((prevFormData) => ({
                  ...prevFormData,
                  country: parseTextNumFirstSpecialChar(
                    e.target.value,
                    /[^A-Za-z0-9-:,'(.)& ]*$/g
                  ),
                }))
              }
            />
          </FormField>
          <FormField label="Currency :">
            <SelectGroup
              disabled={validate.includes("alias") || validate.includes("code")}
              placeholder="Select Currency :"
              options={dropDownCurrency}
              value={formData.currency}
              style={{
                width: "100%",
                height: "40px",
                marginRight: "50px",
              }}
              onSelect={(e) =>
                setFormData((prevFormData) => ({
                  ...prevFormData,
                  currency: e,
                }))
              }
            />
          </FormField>
        </div>
        <div className="rightCard">
          <div className="bank-alias-bic--container">
            <FormField
              errorMessage={
                validate.includes("alias")
                  ? "Name already exists"
                  : "Min 2 and Max 30 Characters!"
              }
              error={valBankAlias || validate.includes("alias")}
              label="Bank Alias Name :"
              style={{ width: "80%" }}
            >
              <AntdTextField
                style={{ width: "100%" }}
                disabled={validate.includes("code")}
                placeholder="Bank Alias Name"
                maxLength="30"
                value={formData.bankAlias}
                onChange={(e) =>
                  setFormData((prevFormData) => ({
                    ...prevFormData,
                    bankAlias: parseTextNumFirstSpecialChar(
                      e.target.value,
                      /[^A-Za-z0-9-:,'(.)& ]*$/g
                    ),
                  }))
                }
              />
            </FormField>
            <FormField
              errorMessage={
                validate.includes("code")
                  ? "Code already exists"
                  : "Min 2 and Max 10 Characters!"
              }
              error={
                valSwiftCode ||
                (validate.includes("code") &&
                  validate.includes(formData.swiftCode))
              }
              label="BIC :"
            >
              <AntdTextField
                disabled={
                  validate.includes("alias") ||
                  (validate.includes("code") &&
                    validate.includes(formData.bankCode))
                }
                style={{ width: 153 }}
                maxLength="10"
                placeholder="0004"
                value={formData.swiftCode}
                onChange={(e) =>
                  setFormData({
                    ...formData,
                    swiftCode: e.target.value.replace(/[^0-9a-zA-Z ]/g, ""),
                  })
                }
              />
            </FormField>
          </div>
          <FormField
            errorMessage="Min 3 and Max 100 Characters!"
            error={valCity}
            label="City :"
          >
            <AntdTextField
              disabled={validate.includes("alias") || validate.includes("code")}
              style={{
                width: "100%",
              }}
              placeholder="City"
              value={formData.city}
              maxLength="100"
              onChange={(e) =>
                setFormData({
                  ...formData,
                  city: parseTextNumFirstSpecialChar(
                    e.target.value,
                    /[^A-Za-z0-9-:,'(.)& ]*$/g
                  ),
                })
              }
            />
          </FormField>
        </div>
      </div>
      <Footer
        disabled={
          handleDisabledSave() ||
          valSwiftCode ||
          valBankName ||
          valBankAlias ||
          valCountry ||
          valCity
        }
        onCancel={() => {
          dispatch(setTypeDetailEdit(null));
          history.goBack();
        }}
        onContinue={() => setOpenModalConfirmation(true)}
        disableElevation
      />
      <SuccessConfirmation
        isOpen={isOpen}
        handleClose={() => {
          dispatch(setTypeIsOpen(false));
          dispatch(setTypeDetailEdit(null));
          setOpenModalConfirmation(false);
          history.goBack();
        }}
      />

      <DeleteConfirmation
        title="Confirmation"
        message={
          dataDetail
            ? "Are You Sure To Edit Data?"
            : "Are You Sure To Add Data?"
        }
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoading}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={onSaveHandler}
      />
    </div>
  );
};

export default InternationalBankTambah;
