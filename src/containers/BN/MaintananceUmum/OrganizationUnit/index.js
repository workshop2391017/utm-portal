import { makeStyles, Button as ButtonChakra } from "@material-ui/core";
import TagsGroup from "components/BN/Tags/TagsGroup";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

// component
import Title from "../../../../components/BN/Title";
import Button from "../../../../components/BN/Button/GeneralButton";
import Menu from "../../../../components/BN/Menus/TabMenuHostErrorMapping";
import TableICBB from "../../../../components/BN/TableIcBB";
import Search from "../../../../components/BN/Search/SearchWithoutDropdown";
import Filter from "../../../../components/BN/Filter/GeneralFilter";
import GeneralMenu from "../../../../components/BN/Menus/GeneralMenu";
import ButtonIcon from "../../../../components/SC/IconButton";
import OrganizationUnitDetail from "./Detail";

// assets
import plus from "../../../../assets/icons/BN/plus-white.svg";
import arrowRight from "../../../../assets/icons/BN/arrow-right.svg";
import edit from "../../../../assets/icons/BN/edit-2.svg";

const dataDummyTableCheckingAccount = [
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
  {
    code: "12321321",
    name: "Organization 1",
    parent: "Orgnization 2",
    email: "majujaya@email.com",
    kota: "Yogyakarta",
    level: "Cabang",
  },
];

const dataHeader = ({ history, setOpenModal }) => [
  {
    title: "Code",
    // headerAlign: "left",
    // align: "left",
    key: "code",
    // render: (rowData) => <span>{rowData.id}</span>,
  },
  {
    title: "Name",
    key: "name",
    // render: (rowData) => <span>{rowData.name}</span>,
  },
  {
    title: "Parent",
    key: "parent",
  },
  {
    title: "Email",
    key: "email",
  },
  {
    title: "Kota",
    key: "kota",
  },
  {
    title: "Level",
    key: "level",
  },
  // {
  //   label: "Debit",
  //   // render: (rowData) => <span>{rowData.name}</span>,
  // },
  // {
  //   label: "Kredit",
  //   // render: (rowData) => <span>{rowData.name}</span>,
  // },
  // {
  //   label: "Saldo",
  //   key: "date",
  //   // render: (rowData) => <span>{rowData.date}</span>,
  // },
  {
    title: "",
    render: (rowData) => (
      <React.Fragment>
        <div
          style={{
            display: "flex",
            width: "100%",
            justifyContent: "flex-end",
            // backgroundColor: "red",
          }}
        >
          <ButtonChakra
            style={{
              textTransform: "capitalize",
              color: "#0061A7",
              fontFamily: "FuturaMdBT",
              fontWeight: "700",
            }}
            onClick={() =>
              history.push("/portal/maintenance-umum/organization-unit/add")
            }
          >
            <span style={{ marginRight: "10px" }}>Edit</span>{" "}
            <img src={edit} alt="" />
          </ButtonChakra>
          <ButtonChakra
            onClick={() => setOpenModal(true)}
            style={{
              textTransform: "capitalize",
              color: "#0061A7",
              fontFamily: "FuturaMdBT",
              fontWeight: "700",
            }}
          >
            <span style={{ marginRight: "10px" }}>Detail</span>{" "}
            <img src={arrowRight} alt="" />
          </ButtonChakra>
        </div>
      </React.Fragment>
    ),
  },
];

const OrganizationUnit = () => {
  const history = useHistory();

  const [openModal, setOpenModal] = useState(false);
  const [dataSearch, setDataSearch] = useState([]);
  const [dataFilter, setDataFilter] = useState(null);

  const useStyles = makeStyles({
    institusiva: {},
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();

  const clickPush = () => {
    document.scrollingElement.scrollTop = 0;
  };

  // useEffect(() => {
  //   setSelectedIds(
  //     dataDummyTableCheckingAccount
  //       .filter((item) => item.checked)
  //       .map((item) => item.id)
  //   );
  // }, [dataDummyTableCheckingAccount]);

  return (
    <div className={classes.institusiva}>
      <Title label="Organization Unit">
        {/* ini pengaturan user */}
        <Button
          label="Organization Unit"
          iconPosition="startIcon"
          buttonIcon={<img src={plus} alt="plus" />}
          width="200px"
          height="40px"
          style={{ marginLeft: 30 }}
          //   onClick={handleAddNew}
        />
      </Title>
      <div className={classes.container}>
        <div style={{ marginBottom: "20px" }}>
          <Filter
            dataFilter={dataFilter}
            setDataFilter={setDataFilter}
            align="left"
            withTitle={false}
            options={[
              {
                id: 1,
                type: "input",
                placeholder: "Organization Code",
              },
              {
                id: 2,
                type: "input",
                placeholder: "OrganizationName",
              },
            ]}
          />
        </div>
        {/* <Badge type="blue" label="Online" /> */}
        <TableICBB
          totalData={100}
          headerContent={dataHeader({ history, setOpenModal })}
          dataContent={dataDummyTableCheckingAccount}
        />

        <OrganizationUnitDetail
          isOpen={openModal}
          handleClose={() => setOpenModal(false)}
        />
      </div>
    </div>
  );
};

OrganizationUnit.propTypes = {};

OrganizationUnit.defaultProps = {};

export default OrganizationUnit;
