import React, { useState } from "react";

import { Fade, makeStyles, Modal, Backdrop } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

// component
import Title from "../../../../../components/BN/Title";
import ButtonOutlined from "../../../../../components/BN/Button/ButtonOutlined";
import GeneralButton from "../../../../../components/BN/Button/GeneralButton";
import OrganizationUnit from "..";

const OrganizationUnitDetail = ({ isOpen, handleClose }) => {
  const history = useHistory();
  const [state, setState] = useState(false);

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
    },
    flexbody: {
      display: "flex",
      justifyContent: "space-between",
      marginTop: 20,
    },
    content: {
      flex: 1,
    },
    modaltitle: {
      fontFamily: "FuturaHvBT",
      fontSize: 32,
      fontWeight: 400,
      textAlign: "center",
      marginBottom: 40,
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      width: 750,
      minHeight: 490,
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      //   padding: "28px 30px",
      alignItems: "center",
      borderRadius: 8,
      position: "relative",
    },
    title: {
      fontSize: 13,
      fontFamily: "FuturaMdBT",
      fontWeight: 600,
    },
    value: {
      fontFamily: "FuturaMdBT",
      fontSize: 15,
      marginTop: 10,
      fontWeight: 400,
    },
    footeraction: {
      display: "flex",
      justifyContent: "space-between",
      marginTop: 50,
    },
  });

  const classes = useStyles();

  return (
    <div>
      <Modal
        open={isOpen}
        className={classes.modal}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.container}>
              <div className={classes.modaltitle}>Organization Unit</div>

              <div className={classes.flexbody}>
                <div className={classes.content}>
                  <span className={classes.title}>Organization Code</span>
                  <p className={classes.value}>1231231231</p>
                </div>
                <div className={classes.content}>
                  <span className={classes.title}>Organization Name</span>
                  <p className={classes.value}>Organization 1</p>
                </div>
              </div>
              <div className={classes.flexbody}>
                <div className={classes.content}>
                  <span className={classes.title}>
                    Organization Description
                  </span>
                  <p className={classes.value}> - </p>
                </div>
                <div className={classes.content}>
                  <span className={classes.title}>Organization Parent</span>
                  <p className={classes.value}>Orfanization 22</p>
                </div>
              </div>
              <div className={classes.flexbody}>
                <div className={classes.content}>
                  <span className={classes.title}>Email</span>
                  <p className={classes.value}>organization1@email.com</p>
                </div>
                <div className={classes.content}>
                  <span className={classes.title}>Kota</span>
                  <p className={classes.value}>Yogyakarta</p>
                </div>
              </div>

              {/* <div style={{ marginTop: 20 }}> */}
              <div className={classes.content} style={{ marginTop: 20 }}>
                <span className={classes.title}>Alamat 1</span>
                <p className={classes.value}>
                  jl.kaliurang km 13.5 no 14, RT 12, RW 15, Krawitan,
                  Umbulmartani, Ngemplak, Kab.Sleman, Special Region of
                  Yogyakarta 55584
                </p>
              </div>
              <div className={classes.content} style={{ marginTop: 20 }}>
                <span className={classes.title}>Alamat 2</span>
                <p className={classes.value}>
                  jl.kaliurang km 13.5 no 14, RT 12, RW 15, Krawitan,
                  Umbulmartani, Ngemplak, Kab.Sleman, Special Region of
                  Yogyakarta 55584
                </p>
              </div>
              <div className={classes.content} style={{ marginTop: 20 }}>
                <span className={classes.title}>Level</span>
                <p className={classes.value}>Cabang</p>
              </div>
              <div className={classes.footeraction}>
                <div>
                  <GeneralButton
                    label="Tambah"
                    width="145px"
                    onClick={() => {}}
                  />
                </div>
                <div>
                  {" "}
                  <ButtonOutlined
                    label="Edit"
                    width="135px"
                    onClick={() =>
                      history.push(
                        "/portal/maintenance-umum/organization-unit/add"
                      )
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

OrganizationUnitDetail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};
OrganizationUnitDetail.defaultProps = {};

export default OrganizationUnitDetail;
