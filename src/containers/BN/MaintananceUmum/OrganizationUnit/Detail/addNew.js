import { Card } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

// component
import Title from "../../../../../components/BN/Title";
import ButtonOutlined from "../../../../../components/BN/Button/ButtonOutlined";
import GeneralButton from "../../../../../components/BN/Button/GeneralButton";
import SuccessConfirmation from "../../../../../components/BN/Popups/SuccessConfirmation";

import TextField from "../../../../../components/BN/TextField/AntdTextField";
import SelectGroup from "../../../../../components/BN/Select/SelectGroup";

const AddNewOrganizationUnit = () => {
  const history = useHistory();
  const [formData, setFormData] = useState({});
  const [openSuccess, setOpenSuccess] = useState(false);

  const useStyles = makeStyles({
    addneworganization: {},
    container: {
      padding: "20px 30px",
    },
    card: {
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      padding: 40,
      borderRadius: 20,
      marginLeft: 180,
      marginRight: 180,
    },
    inputflextree: {
      display: "flex",
      justifyContent: "space-between",
      backgroundColor: "#fff",
    },
    footeraction: {
      display: "flex",
      justifyContent: "space-between",
      height: "83px",
      alignItems: "center",
      paddingLeft: 20,
      paddingRight: 20,
      backgroundColor: "#fff",
    },
  });

  const classes = useStyles();
  return (
    <div>
      <Title label="Tambah Organization Unit" />
      <div className={classes.container}>
        <Card className={classes.card}>
          <div className={classes.inputflextree}>
            <div style={{ flex: 1, marginRight: "20px" }}>
              <span>Organization Unit</span>
              <div>
                <TextField placeholder="123123" style={{ width: "100%" }} />
              </div>
            </div>
            <div style={{ flex: 1, marginRight: "20px" }}>
              <span>Organization Name</span>
              <div>
                <TextField
                  placeholder="Bank KC Yogyakarta"
                  style={{ width: "100%" }}
                />
              </div>
            </div>
            <div style={{ flex: 1 }}>
              <span>Organization Desc</span>
              <div>
                <TextField placeholder="Deskripsi" style={{ width: "100%" }} />
              </div>
            </div>
          </div>
          <div className={classes.inputflextree} style={{ marginTop: 20 }}>
            <div style={{ flex: 1, marginRight: "20px" }}>
              <span>Organization Parent</span>
              <div>
                <SelectGroup
                  style={{ width: "100%" }}
                  placeholder="Pilih Parent"
                  options={[{ name: "parent", id: 1 }]}
                />
              </div>
            </div>
            <div style={{ flex: 1, marginRight: "20px" }}>
              <span>Email</span>
              <div>
                <TextField
                  placeholder="Bank KC Yogyakarta"
                  style={{ width: "100%" }}
                />
              </div>
            </div>
            <div style={{ flex: 1 }}>
              <span>Kota</span>
              <div>
                <SelectGroup
                  style={{ width: "100%" }}
                  placeholder="Pilih Kota"
                  options={[{ name: "Kota", id: 1 }]}
                />
              </div>
            </div>
          </div>
          <div style={{ marginTop: 22 }}>
            <span>Alamat 1</span>
            <div>
              <TextField
                placeholder="Alamat Organization 1"
                style={{ width: "100%" }}
              />
            </div>
          </div>
          <div style={{ marginTop: 22 }}>
            <span>Alamat 2</span>
            <div>
              <TextField
                placeholder="Alamat Organization 2"
                style={{ width: "100%" }}
              />
            </div>
          </div>
          <div style={{ marginTop: 22 }}>
            <span>Alamat 3</span>
            <div>
              <TextField
                placeholder="Alamat Organization 3"
                style={{ width: "100%" }}
              />
            </div>
          </div>
          <div style={{ marginTop: 22 }}>
            <span>Level</span>
            <div>
              <SelectGroup
                placeholder="Pilih Level"
                options={[{ name: "Level 1", id: 1 }]}
              />
            </div>
          </div>
        </Card>
      </div>
      <div className={classes.footeraction}>
        <div>
          <ButtonOutlined
            label="Batal"
            width="135px"
            onClick={() => history.goBack()}
          />
        </div>
        <div>
          <GeneralButton
            label="Tambah"
            width="145px"
            onClick={() => setOpenSuccess(true)}
          />
        </div>
      </div>

      <SuccessConfirmation
        isOpen={openSuccess}
        message="Tambah Organization Unit Berhasil"
        submessage="Anda telah berhasil menambah organization unit"
        handleClose={() => setOpenSuccess(false)}
      />
    </div>
  );
};

export default AddNewOrganizationUnit;
