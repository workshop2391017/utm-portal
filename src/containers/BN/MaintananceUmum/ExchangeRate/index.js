import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";

// component
import Title from "../../../../components/BN/Title";
import Search from "../../../../components/BN/Search/SearchWithoutDropdown";
import Button from "../../../../components/BN/Button/GeneralButton";
import Chart from "../../../../components/BN/Charts/Chart";
import Tabs from "../../../../components/BN/Tabs/AntdTabs";
import TableICBB from "../../../../components/BN/TableIcBB";

// assets
import plus from "../../../../assets/icons/BN/plus-white.svg";
import arrowDown from "../../../../assets/icons/BN/arrow-square-down.svg";
import arrowUp from "../../../../assets/icons/BN/arrow-square-up.svg";

const ExchangeRate = () => {
  const [state, setState] = useState({});
  const [dataSearch, setDataSearch] = useState([]);
  const [dataDummyTableCheckingAccount, setDataDummyTableCheckingAccount] =
    useState([]);
  const useStyles = makeStyles({
    exchangerate: {},
    container: {
      padding: "20px 30px",
    },
    containerChart: {
      backgroundColor: "#fff",
      marginBottom: "36px",
    },
    chart: {
      marginLeft: "63px",
      marginRight: "14px",
    },
  });

  const classes = useStyles();

  useEffect(() => {
    const dummy = [];
    for (let i = 0; i < 10; i++) {
      dummy.push({
        matauang: "Australian Dolar",
        kode: "AUD",
        beli: "3368",
        jual: "3342",
        tgl: 0,
        waktu: "12:00",
      });
    }
    setDataDummyTableCheckingAccount(dummy);
  }, []);

  const dataHeader = [
    {
      title: "Matauang",
      // headerAlign: "left",
      // align: "left",
      key: "matauang",
      render: (rowData) => <span>🇦🇺 {rowData.matauang}</span>,
    },
    {
      title: "Kode",
      key: "kode",
      // render: (rowData) => <span>{rowData.name}</span>,
    },
    {
      title: "Beli",
      key: "beli",
      render: (rowData) => (
        <div>
          {rowData.beli}
          <img
            src={arrowDown}
            alt="down arrow"
            style={{
              width: "20px",
              height: "20px",
              borderRadius: "50px",
              marginLeft: "5px",
            }}
          />
        </div>
      ),
    },
    {
      title: "Jual",
      key: "jual",
      render: (rowData) => (
        <div>
          {rowData.jual}
          <img
            src={arrowUp}
            alt="down arrow"
            style={{
              width: "20px",
              height: "20px",
              borderRadius: "50px",
              marginLeft: "5px",
            }}
          />
        </div>
      ),
    },
    {
      title: "Tgl",
      key: "tgl",
    },
    {
      title: "Waktu",
      key: "waktu",
    },
  ];

  return (
    <div className={classes.exchangerate}>
      <Title label="Exchange Rate" />
      <div className={classes.container}>
        <div className={classes.containerChart}>
          <div
            style={{
              fontFamily: "FuturaHvBT",
              fontweight: "900",
              fontSize: "16px",
              paddingTop: "12px",
              paddingLeft: "12px",
              paddingBottom: "1px",
            }}
          >
            Nilai Tukar
          </div>
          <div className={classes.chart}>
            <Chart
              chartTabs={
                <Tabs
                  // onChange={setActiveChartTabs}
                  tabs={["USD", "AUD", "SGD"]}
                  style={{ marginBottom: -20 }}
                />
              }
              series={[
                {
                  name: "series-1",
                  data: [300000, 400000, 45000, 50000, 49000, 60000],
                },
                {
                  name: "series-2",
                  data: [200000, 300000, 40000, 40000, 50000, 55000],
                },
              ]}
              categories={[
                "Senin\n05/07/2021",
                "Selasa\n05/07/2021",
                "Rabu\n05/07/2021",
                "Kamis\n05/07/2021",
                "Jum'at\n05/07/2021",
                "Sabtu\n05/07/2021",
              ]}
              // isLoading={isLoadingChart}
              // onChangeTabsChart={setActiveChartTabs}
              activeChartTabs="1"
              // dateRange={series.dateRange}
              // data={dataChartTabs}
              // dataTooltip={dataChartTooltip}
              // categories={dataChartCategories}
              // loading={isLoadingChart}
              // tabsValue={chartTabs}
              // onChangeTabsValue={(key) => setChartTabs(key)}
              // maxRange={dataChartRange}
            />
          </div>
        </div>
        <TableICBB
          tableRounded={false}
          headerContent={dataHeader}
          dataContent={dataDummyTableCheckingAccount}
          totalData={20}
        />
      </div>
    </div>
  );
};

ExchangeRate.propTypes = {};

ExchangeRate.defaultProps = {};

export default ExchangeRate;
