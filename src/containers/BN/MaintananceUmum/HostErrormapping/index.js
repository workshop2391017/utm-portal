import { makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  DeleteDataHostErrorMapping,
  getDataHostErrorMapping,
  setTypeClearError,
  setTypeOpenDelete,
  setTypeOpenModal,
} from "stores/actions/hostErrorMapping";

// assets
import plus from "assets/icons/BN/plus-white.svg";
import Button from "components/BN/Button/GeneralButton";
import ModalHostErrormapping from "components/BN/Popups/HostErrormapping";
import TableICBB from "components/BN/TableIcBB";

// component
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import DeleteConfirmation from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";
import Toast from "components/BN/Toats";
import useDebounce from "utils/helpers/useDebounce";
import { validateMinMax } from "utils/helpers";
import usePrevious from "utils/helpers/usePrevious";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";

const HostErrormapping = () => {
  const dispatch = useDispatch();
  const {
    data,
    isLoading,
    error,
    isOpen,
    openModal: openModalStore,
    isLoadingExcute,
    isOpenDelete,
  } = useSelector((state) => state.hostErrorMapping);

  const [dataRow, setDataRow] = useState(null);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);

  const [openModal, setOpenModal] = useState(false);

  const [page, setPage] = useState(1);
  const [dataSearch, setDataSearch] = useState(null);
  const searchBounce = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(searchBounce);

  const onDeleteHandler = () => {
    dispatch(DeleteDataHostErrorMapping(dataRow));
  };

  const useStyles = makeStyles({
    hosterrormapping: {},
    container: {
      padding: "20px 30px",
    },
  });

  const dataHeader = () => [
    {
      title: "Error Code",

      key: "code",
    },
    {
      title: "Source System",
      key: "sourceSystem",
    },
    {
      title: "Message in English",
      key: "engMessage",
    },
    {
      title: "Message in Indonesian",
      key: "idnMessage",
    },
    {
      title: "",
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <GeneralMenu
              openModal={openModal}
              close={() => setOpenModal(false)}
              rowData={rowData}
              options={["edit", "delete"]}
              onDelete={() => {
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.HOST_ERROR_MAPPING,
                      code: null,
                      id: rowData?.id,
                      name: null,
                      validate: true,
                    },
                    {
                      onContinue() {
                        setDataRow(rowData);
                        setOpenModalConfirmation(true);
                      },
                      onError() {
                        // handle callback error
                        // popups callback error sudah dihandle
                        // ex. clear form, redirect, etc
                      },
                    },
                    {
                      redirect: false,
                    }
                  )
                );
              }}
              onEdit={() => {
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.HOST_ERROR_MAPPING,
                      code: null,
                      id: rowData?.id,
                      name: null,
                      validate: true,
                    },
                    {
                      onContinue() {
                        setDataRow(rowData);
                        dispatch(setTypeOpenModal(true));
                      },
                      onError() {
                        // handle callback error
                        // popups callback error sudah dihandle
                        // ex. clear form, redirect, etc
                      },
                    },
                    {
                      redirect: false,
                    }
                  )
                );
              }}
            />
          </div>
        </React.Fragment>
      ),
    },
  ];

  const classes = useStyles();

  useEffect(() => {
    const payload = {
      direction: "ASC",
      pageNumber: parseInt(page) - 1,
      pageSize: 10,
      searchBy: "",
      code: searchBounce || null,
    };

    if (page === 1) {
      dispatch(getDataHostErrorMapping(payload));
    } else if (searchBounce !== prevSearch) {
      setPage(1);
    } else {
      dispatch(getDataHostErrorMapping(payload));
    }
  }, [searchBounce, page]);

  return (
    <div className={classes.hosterrormapping}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />
      <SuccessConfirmation
        isOpen={isOpenDelete}
        handleClose={() => {
          dispatch(setTypeOpenDelete(false));
          setOpenModalConfirmation(false);
        }}
      />

      <ModalHostErrormapping
        rowData={dataRow === null ? null : dataRow}
        title={dataRow === null ? "Create Error Message" : "Edit Error Message"}
        isOpen={openModalStore}
        setDataRow={setDataRow}
        handleClose={() => {
          dispatch(setTypeOpenModal(false));
          setDataRow(null);
        }}
      />

      <DeleteConfirmation
        title="Confirmation"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onDeleteHandler();
        }}
      />

      <Title label="Host Error Mapping">
        <Search
          placeholder="Error Code, Message"
          placement="bottom"
          style={{ width: "200px" }}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
        <Button
          label="New Error Messages"
          iconPosition="startIcon"
          buttonIcon={<img src={plus} alt="plus" />}
          width="215px"
          height="44px"
          fontFamily="FuturaMdBT"
          fontSize="15px"
          style={{ marginLeft: 30 }}
          onClick={() => {
            dispatch(setTypeOpenModal(true));
            setDataRow(null);
          }}
        />
      </Title>
      <div className={classes.container}>
        <TableICBB
          headerContent={dataHeader()}
          dataContent={data?.content ?? []}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
        />
      </div>
    </div>
  );
};

HostErrormapping.propTypes = {};

HostErrormapping.defaultProps = {};

export default HostErrormapping;
