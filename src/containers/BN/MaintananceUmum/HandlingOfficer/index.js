import { makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// assets
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Toast from "components/BN/Toats";
import {
  deleteDataHandlingOfficer,
  getDataHandlingOfficer,
  setHandleClearErrorOfficer,
  setTypeOpenModal,
  setTypePopUpConfirm,
} from "stores/actions/handlingOfficer";
import useDebounce from "utils/helpers/useDebounce";

import DeleteConfirmation from "components/BN/Popups/Delete";
import plus from "assets/icons/BN/plus-white.svg";
import Button from "components/BN/Button/GeneralButton";
import HandlingOfficerModal from "components/BN/Popups/HandlingOfficerModal";
import Search from "components/BN/Search/SearchWithoutDropdown";
import TableICBB from "components/BN/TableIcBB";

// component
import Title from "components/BN/Title";
import usePrevious from "utils/helpers/usePrevious";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { useValidateLms } from "utils/helpers/validateLms";
import { confValidateMenuName } from "stores/actions/validatedatalms";

const HandlingOfficer = () => {
  const [openModal, setOpenModal] = useState(false);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [dataSearch, setDataSearch] = useState("");
  const result = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(result);
  const [page, setPage] = useState(1);
  const [dataRow, setDataRow] = useState(null);

  const dispatch = useDispatch();
  const {
    data,
    isLoading,
    error,
    isOpenModal,
    isLoadingExcute,
    openModal: openModalStore,
    modalConfirm,
  } = useSelector((state) => state.handlingOfficer);

  const useStyles = makeStyles({
    handlingofficer: {},
    container: {
      padding: "20px 30px",
    },
  });

  const classes = useStyles();

  const onDeleteHandler = () => {
    const payload = {
      id: dataRow?.id,
      email: dataRow?.email,
      location: dataRow?.location,
      officerId: dataRow?.officerId,
      phoneNumber: dataRow?.phoneNumber,
      username: dataRow?.username,
      isDeleted: true,
    };
    dispatch(deleteDataHandlingOfficer(payload));
  };

  const dataHeader = ({ onEdit, onDelete }) => [
    {
      title: "Officer ID",

      key: "officerId",
    },
    {
      title: "Officer Name",
      key: "username",
    },
    {
      title: "Branch",
      key: "location",
    },
    {
      title: "E-mail",
      key: "email",
    },
    {
      title: "Mobile phone number",
      key: "phoneNumber",
    },

    {
      title: "",
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <GeneralMenu
              openModal={openModal}
              close={() => setOpenModal(false)}
              rowData={rowData}
              options={["edit", "delete"]}
              onDelete={() => {
                onDelete(rowData);
              }}
              onEdit={() => onEdit(rowData)}
            />
          </div>
        </React.Fragment>
      ),
    },
  ];

  useEffect(() => {
    const payload = {
      search: result,
      pageNumber: parseInt(page - 1),
      pageSize: 10,
    };

    if (page === 1) {
      dispatch(getDataHandlingOfficer(payload));
    } else if (result !== prevSearch) {
      setPage(1);
    } else {
      dispatch(getDataHandlingOfficer(payload));
    }
  }, [result, page]);

  const onEdit = (rowData) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.HANDLING_OFFICER,
          id: rowData?.id,
          name: null,
          code: null,
          validate: true,
        },
        {
          async onContinue() {
            setDataRow(rowData);

            dispatch(setTypeOpenModal(true));
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const onDelete = (rowData) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.HANDLING_OFFICER,
          id: rowData?.id,
          name: null,
          code: null,
          validate: true,
        },
        {
          async onContinue() {
            setDataRow(rowData);
            setOpenModalConfirmation(true);
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  return (
    <React.Fragment>
      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure to Delete the Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onDeleteHandler();
        }}
      />
      <SuccessConfirmation
        isOpen={modalConfirm}
        handleClose={() => {
          dispatch(setTypePopUpConfirm(false));

          setOpenModalConfirmation(false);
        }}
      />
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearErrorOfficer())}
      />
      <div className={classes.handlingofficer}>
        <HandlingOfficerModal
          rowData={dataRow}
          title={!dataRow?.id ? "Add Officer" : "Edit Officer"}
          isOpen={openModalStore}
          handleClose={() => {
            dispatch(setTypeOpenModal(false));
            setDataRow(null);
          }}
        />

        <Title label="Handling Officer">
          <Search
            placeholder="Officer ID, Officer Name"
            placement="bottom"
            style={{
              width: "286px",
            }}
            dataSearch={dataSearch}
            setDataSearch={setDataSearch}
          />
          <Button
            label="Add Officer"
            iconPosition="startIcon"
            buttonIcon={<img src={plus} alt="plus" />}
            width="200px"
            height="40px"
            style={{ marginLeft: 30 }}
            onClick={() => {
              setDataRow(null);
              dispatch(setTypeOpenModal(true));
            }}
          />
        </Title>
        <div className={classes.container}>
          <TableICBB
            headerContent={dataHeader({ onEdit, onDelete })}
            dataContent={data?.handlingOfficerPagination}
            isLoading={isLoading}
            totalElement={data?.totalElements}
            totalData={data?.totalPages}
            page={page}
            setPage={setPage}
          />
        </div>
      </div>
    </React.Fragment>
  );
};

HandlingOfficer.propTypes = {};

HandlingOfficer.defaultProps = {};

export default HandlingOfficer;
