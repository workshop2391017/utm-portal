import { Button, Paper, Typography, makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import { ReactComponent as DeviceIcon } from "assets/icons/BN/device-icon.svg";
import Title from "components/BN/Title";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { serviceConsolidationCompanyLoginDetail } from "utils/api";
import { handleConsolidationCompanyLoginDetail } from "stores/actions/consolidationReport";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "left",
    alignItems: "left",
    padding: "20px 20px 20px 50px",
    width: "100%",
  },
  backButton: {
    ...theme.typography.backButton,
    width: "20px",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "left",
    padding: "20px 15px 15px 15px",
    gap: "25px",
    borderRadius: "10px",
  },
  content: {
    display: "flex",
    justifyContent: "space-between",
    padding: "30px 100px 30px 100px",
  },
  contentLeft: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    gap: "20px",
    justifyContent: "center",
    padding: "50px 20px 50px 20px",
  },
  column: {
    display: "flex",
    justifyContent: "space-between",
  },
  key: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "17px",
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "17px",
  },
}));

const CompanyLoginDetail = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { authorization, companyLoginDetail, companyLoginId, fullName } =
    useSelector((e) => e?.consolidationReport);
  const handleClickBack = () => {
    history.goBack();
    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };
  useEffect(() => {
    if (!companyLoginId) history.goBack();
    if (companyLoginId) {
      const payload = {
        auditLoginDataId: companyLoginId,
      };
      dispatch(handleConsolidationCompanyLoginDetail(payload));
    }
  }, [companyLoginId]);

  let username = {};
  if (companyLoginDetail)
    username = JSON.parse(companyLoginDetail?.additionalData);

  return (
    <div className={classes.wrapper}>
      <Button
        size="small"
        startIcon={<ArrowLeft />}
        onClick={handleClickBack}
        className={classes.backButton}
      >
        Back
      </Button>
      <Title label="Detail Company Login" paddingLeft={0} />
      <Paper className={classes.container}>
        <div className={classes.content}>
          <div className={classes.contentLeft}>
            <div className={classes.column}>
              <Typography className={classes.key}>Username</Typography>
              <Typography className={classes.value}>{fullName}</Typography>
            </div>
            <div className={classes.column}>
              <Typography className={classes.key}>User ID</Typography>
              <Typography className={classes.value}>
                {username?.userId}
              </Typography>
            </div>
            {authorization === "MULTIPLE" ? (
              <div className={classes.column}>
                <Typography className={classes.key}>Group</Typography>
                <Typography className={classes.value}>
                  {username?.group}
                </Typography>
              </div>
            ) : null}
            {companyLoginDetail?.phoneBrand ? (
              <React.Fragment>
                <div className={classes.column}>
                  <Typography className={classes.key}>Device</Typography>
                  <Typography className={classes.value}>
                    {companyLoginDetail?.phoneBrand}
                  </Typography>
                </div>
                <div className={classes.column}>
                  <Typography className={classes.key}>OS</Typography>
                  <Typography className={classes.value}>
                    {companyLoginDetail?.os}
                  </Typography>
                </div>
                <div className={classes.column}>
                  <Typography className={classes.key}>App Version</Typography>
                  <Typography className={classes.value}>
                    {companyLoginDetail?.appVersion}
                  </Typography>
                </div>{" "}
              </React.Fragment>
            ) : null}
            <div className={classes.column}>
              <Typography className={classes.key}>Timestamp</Typography>
              <Typography className={classes.value}>
                {moment(companyLoginDetail?.activityDate).format(
                  "DD/MM/YYYY | HH:mm"
                ) ?? "-"}
              </Typography>
            </div>
            {companyLoginDetail?.ipAddress ? (
              <div className={classes.column}>
                <Typography className={classes.key}>IP Address</Typography>
                <Typography className={classes.value}>
                  {companyLoginDetail?.ipAddress}
                </Typography>
              </div>
            ) : null}
          </div>
          <div>
            <DeviceIcon />
          </div>
        </div>
      </Paper>
    </div>
  );
};

export default CompanyLoginDetail;
