import GeneralButton from "components/BN/Button/GeneralButton";
import PopupFilter from "containers/BN/ConsolidationReport/Detail/PopupFilter";
import TableICBB from "components/BN/TableIcBB";
import Colors from "helpers/colors";
import React from "react";
import Search from "components/BN/Search/SearchWithoutDropdown";

import { ReactComponent as SVGArrow } from "assets/icons/BN/arrow-right.svg";
import { ReactComponent as SvgDownload } from "assets/icons/BN/download-white.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import Badge from "components/BN/Badge";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import { ReactComponent as SVGDownload } from "assets/icons/BN/download.svg";
import { ReactComponent as SVGFile } from "assets/icons/BN/file-text.svg";
import AntdTextField from "components/BN/TextField/AntdTextField";

const dataHeader = ({ history }) => [
  {
    title: "Update Date",
    key: "date",
  },
  {
    title: "Nama File",
    key: "file",
    width: 312,
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          color: Colors.primary.hard,
        }}
      >
        <SVGFile path={Colors.primary.hard} style={{ marginRight: 14 }} />
        {rowData.file}
      </div>
    ),
  },
  {
    title: "File Size",
    key: "size",
  },
  {
    title: "",
    key: "status",
    render: (rowData) => (
      <div style={{ display: "flex", color: Colors.primary.hard }}>
        <SVGDownload style={{ marginRight: 6 }} />
        Download File
      </div>
    ),
  },
];

const dataDummyTableCheckingAccount = [
  {
    id: 1,
    date: "14 Des 2021",
    file: "Q3 - 2021 Quarterly Financial Statement.pdf",
    size: "3148 KB",
  },

  {
    id: 1,
    date: "14 Des 2021",
    file: "Q3 - 2021 Quarterly Financial Statement.pdf",
    size: "3148 KB",
  },
  {
    id: 1,
    date: "14 Des 2021",
    file: "Q3 - 2021 Quarterly Financial Statement.pdf",
    size: "3148 KB",
  },
  {
    id: 1,
    date: "14 Des 2021",
    file: "Q3 - 2021 Quarterly Financial Statement.pdf",
    size: "3148 KB",
  },
  {
    id: 1,
    date: "14 Des 2021",
    file: "Q3 - 2021 Quarterly Financial Statement.pdf",
    size: "3148 KB",
  },
];

const useStyles = makeStyles({
  container: {
    padding: "20px",
  },
  paperpt: {
    backgroundColor: "#fff",
    width: "100%",
    height: "544px",
    borderRadius: 10,
    border: "1px solid #fff",
  },
  corporateName: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  title: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
    paddingTop: 13,
    paddingBlock: 8,
  },
  sideText: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.gray.medium,
  },
  textGroup: {
    padding: 20,
  },
  contentContainer: {
    borderRadius: "10px",
    minHeight: 300,
    backgroundColor: "white",
    padding: "25px 13px 30px 11px",
  },
  filterGroup: {
    display: "flex",
    padding: "16px 14px 23px 13px",
  },
  dateSearch: {
    width: "167px !important",
  },
});

const CompanyConsolisationDetail = () => {
  const history = useHistory();
  const classes = useStyles();

  return (
    <div>
      <div className={classes.filterGroup}>
        <div style={{ width: "100%", display: "flex" }}>
          <AntdTextField
            type="date"
            placeholder="Mulai Dari"
            style={{ width: 167, marginRight: 10 }}
          />
          <AntdTextField
            type="date"
            placeholder="Berakhir Pada"
            style={{ width: 167, marginRight: 10 }}
          />

          <Search
            style={{ marginRight: 20, width: "100% !important" }}
            options={["User ID"]}
            //   dataSearch={dataSearch}
            //   setDataSearch={setDataSearch}
          />
        </div>

        <GeneralButton width="111px" label="Terapkan" />
      </div>
      <TableICBB
        headerContent={dataHeader({ history })}
        dataContent={dataDummyTableCheckingAccount}
        page={1}
        totalData={100}
        rowColor={Colors.gray.ultrasoft}
      />
    </div>
  );
};

export default CompanyConsolisationDetail;
