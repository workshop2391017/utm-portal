// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import Colors from "helpers/colors";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import { ReactComponent as FilePDF } from "assets/icons/BN/file-pdf.svg";
import { ReactComponent as FileXLXS } from "assets/icons/BN/file-excel.svg";
import RadioSingle from "components/BN/Radio/RadioSingle";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paperContainer: {
    width: 480,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    padding: "20px 0",
  },
  paper: {
    padding: "0px 20px 0px 30px",
  },
  aturFilter: {
    fontSize: 24,
    textAlign: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    marginBottom: 51,
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 36,
    fontWeight: 900,
    color: Colors.dark.hard,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  filterInput: {
    display: "flex",
  },
  input: {
    textAlign: "left",
  },
  buttonGroup: {
    height: 84,
    display: "flex",
    justifyContent: "space-between",
    padding: "22px 20px",
    marginTop: 30,
  },
  fileTitle: {
    marginBottom: 33,
    fontFamily: "FuturaBkBT",
    fontSize: 17,
    color: Colors.dark.hard,
  },
}));

const DownloadPopup = ({
  isOpen,
  handleClose,
  title,
  options,
  setDataFilter,
  onDownload,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);
  const [downloadType, setDownloadType] = useState("");

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paperContainer}>
            <div className={classes.paper}>
              <div>
                <div className={classes.aturFilter}>Download File</div>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <RadioSingle
                      label="PDF"
                      checked={downloadType === "pdf"}
                      handleChange={() => setDownloadType("pdf")}
                    />
                    <FilePDF />
                  </div>

                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      marginTop: 10,
                    }}
                  >
                    <RadioSingle
                      label="XLSX"
                      checked={downloadType === "xlsx"}
                      handleChange={() => setDownloadType("xlsx")}
                    />
                    <FileXLXS />
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.buttonGroup}>
              <ButtonOutlined label="Cancel" width={82} onClick={handleClose} />

              <GeneralButton
                label="Download"
                width={93}
                onClick={() => onDownload(downloadType)}
                disabled={!downloadType}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

DownloadPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  options: PropTypes.array,
  setDataFilter: PropTypes.func,
};

DownloadPopup.defaultProps = {
  title: "Berhasil",
  options: [],
  setDataFilter: () => {},
};

export default DownloadPopup;
