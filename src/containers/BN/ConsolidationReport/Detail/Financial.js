import GeneralButton from "components/BN/Button/GeneralButton";
import PopupFilter from "containers/BN/ConsolidationReport/Detail/PopupFilter";
import TableICBB from "components/BN/TableIcBB";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import { ReactComponent as SvgDownload } from "assets/icons/BN/download-white.svg";
import { Box, makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  downloadPagesConf,
  handleConsolidationFinancial,
  handleConsolidationReportDownload,
  handleTransactionGroup,
  handleSoftGet,
  setTransactionTypeConf,
} from "stores/actions/consolidationReport";
import {
  formatAmount,
  searchDate,
  searchDateEnd,
  searchDateStart,
} from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import { ReactComponent as ArrowRight } from "assets/icons/BN/arrow-right.svg";
import moment from "moment";
import DownloadPopup from "./popupDownload";

import UserInformationPopups from "./popupsUserInfo";

const headerMultiple = ({ setOpenDetail }) => [
  {
    title: "Date & Time",
    key: "dateTime",
  },
  {
    title: "User Information",
    key: "userInformation",
    render: (e) => (
      <div>
        <div>{e.fullName}</div>
        <Box
          style={{
            color: "#0061A7",
            fontWeight: 700,
            fontSize: 11,
            fontFamily: "FuturaMdBT",
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
          }}
          onClick={() => setOpenDetail({ isOpen: true, data: e })}
        >
          Lihat Detail <ArrowRight width={18} />
        </Box>
      </div>
    ),
  },
  {
    title: "Source of Funds",
    key: "sumberDana",
    render: (rowData) => (
      <div>
        <div style={{ fontFamily: "FuturaMdBT" }}>
          {rowData?.sofProductName}
        </div>
        <div style={{ fontFamily: "FuturaBkBT" }}>{rowData?.sof}</div>
      </div>
    ),
  },
  {
    title: "Transaction Type",
    key: "jenisTransaksi",
    render: (rowData) => (
      <div>
        <div style={{ fontFamily: "FuturaMdBT" }}>
          {rowData?.transactionCategory}
        </div>
        <div style={{ fontFamily: "FuturaBkBT" }}>{rowData?.transactionId}</div>
      </div>
    ),
  },
  {
    title: "Transaction Amount",
    key: "transactionAmount",
    render: (e) => `IDR ${formatAmount(e?.transactionAmount)}`,
  },
];

const headerSingle = () => [
  {
    title: "Date & Time",
    key: "dateTime",
  },
  {
    title: "User Information",
    key: "userInformation",
    render: (e) => (
      <div>
        <div>{e.fullName}</div>
        <div>{e.username}</div>
      </div>
    ),
  },
  {
    title: "Source of Funds",
    key: "sumberDana",
    render: (rowData) => (
      <div>
        <div style={{ fontFamily: "FuturaMdBT" }}>
          {rowData?.sofProductName}
        </div>
        <div style={{ fontFamily: "FuturaBkBT" }}>{rowData?.sof}</div>
      </div>
    ),
  },
  {
    title: "Transaction Type",
    key: "jenisTransaksi",
    render: (rowData) => (
      <div>
        <div style={{ fontFamily: "FuturaMdBT" }}>
          {rowData?.transactionCategory}
        </div>
        <div style={{ fontFamily: "FuturaBkBT" }}>{rowData?.transactionId}</div>
      </div>
    ),
  },
  {
    title: "Transaction Amount",
    key: "transactionAmount",
    render: (e) => `IDR ${formatAmount(e?.transactionAmount)}`,
  },
];

const useStyles = makeStyles({
  container: {
    padding: "20px",
  },
  paperpt: {
    backgroundColor: "#fff",
    width: "100%",
    height: "544px",
    borderRadius: 10,
    border: "1px solid #fff",
  },
  corporateName: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  title: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
    paddingTop: 13,
    paddingBlock: 8,
  },
  sideText: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.gray.medium,
  },
  textGroup: {
    padding: 20,
  },
  contentContainer: {
    borderRadius: "10px",
    minHeight: 300,
    backgroundColor: "white",
    padding: "25px 13px 30px 11px",
  },
  filterGroup: {
    display: "flex",
    padding: "16px 14px 23px 13px",
  },
  prefix: {
    backgroundColor: Colors.primary.hard,
    width: 110,
    height: 40,
    marginLeft: -20,
    marginTop: -1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
    fontFamily: "FuturaMdBT",
  },
});

const FinancialReportDetail = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [openDownload, setOpenDownload] = useState(false);
  const [dataFilter, setDataFilter] = useState(null);
  const [searchVal, setSearchVal] = useState("");
  const [openDetail, setOpenDetail] = useState({ isOpen: false, data: null });

  const {
    corporateId,
    workflowGroup,
    isLoading,
    financial,
    authorization,
    softData,
    transactionGroup,
  } = useSelector((e) => e?.consolidationReport);

  const dataHeader = authorization === "SINGLE" ? headerSingle : headerMultiple;

  const popupOption =
    authorization === "SINGLE"
      ? [
          {
            id: 1,
            type: "rangeDate",
            title: ["Start Date", "End Date"],
            placeholder: ["DD/MM/YYYY", "DD/MM/YYYY"],
          },
          {
            id: 2,
            type: "dropdown",
            options: [
              softData?.corporateAccountList?.map((e) => ({
                label: `${e?.productName}-${e?.accountNumber}`,
                value: e?.accountNumber,
              })) ?? [],
            ],
            title: "Source of Funds :",
            placeholder: "Source of Funds",
          },
          {
            id: 3,
            type: "dropdown",
            options: [
              transactionGroup?.transactionStatusDtoList?.map((e) => ({
                label: e?.transactionGroupName,
                value: e?.transactionGroupEnum,
              })) ?? [],
            ],
            title: "Transaction Type :",
            placeholder: "Transaction Type",
          },
        ]
      : authorization === "MULTIPLE"
      ? [
          {
            id: 1,
            type: "rangeDate",
            title: ["Start Date", "End Date"],
            placeholder: ["DD/MM/YYYY", "DD/MM/YYYY"],
          },
          {
            id: 2,
            type: "dropdown",
            options: [
              softData?.corporateAccountList?.map((e) => ({
                label: `${e?.productName}-${e?.accountNumber}`,
                value: e?.accountNumber,
              })) ?? [],
            ],
            title: "Source of Funds :",
            placeholder: "Source of Funds",
          },
          {
            id: 3,
            type: "dropdown2Row",
            options: [
              workflowGroup?.getGroupDtoList?.map((e) => ({
                label: e.name,
                value: e?.workflowGroupId,
              })) ?? [],
              transactionGroup?.transactionStatusDtoList?.map((e) => ({
                label: e?.transactionGroupName,
                value: e?.transactionGroupEnum,
              })) ?? [],
            ],
            title: ["User Group", "Transaction Type"],
            placeholder: ["User Group", "Transaction  Type"],
          },
        ]
      : null;

  const searchDeb = useDebounce(searchVal, 1300);

  useEffect(() => {
    if (corporateId) {
      const payload = {
        corporateProfileId: corporateId,
        startDate: dataFilter?.startDate0 ? dataFilter?.startDate0 : new Date(),
        endDate: dataFilter?.endDate0 ? dataFilter?.endDate0 : new Date(),
        groupId: dataFilter?.dropdown12,
        page: page - 1,
        searchValue: searchDeb,
        size: 10,
        sof: dataFilter?.dropdown1,
        transactionGroup:
          authorization === "MULTIPLE"
            ? dataFilter?.dropdown22
            : dataFilter?.dropdown2,
      };

      dispatch(handleConsolidationFinancial(payload));
      dispatch(handleTransactionGroup({}));
    }
  }, [corporateId, page, dataFilter, searchDeb]);

  const handleDownload = (type) =>
    dispatch(
      handleConsolidationReportDownload(
        {
          financialReportList: financial?.financialReportDtoList ?? [],
          dateFrom: dataFilter?.startDate0
            ? dataFilter?.startDate0
            : new Date(),
          dateTo: dataFilter?.endDate0 ? dataFilter?.endDate0 : new Date(),
          format: type,
          otorisasi: authorization,
        },
        { pages: downloadPagesConf.FINANCIAL_REPORT }
      )
    );

  useEffect(() => {
    if (authorization && corporateId) {
      const payload = {
        accountGroupId: null,
        corporateProfileId: corporateId,
        isMultiple: authorization === "MULTIPLE",
      };
      dispatch(handleSoftGet(payload));
    }
  }, []);

  return (
    <div>
      <UserInformationPopups
        isOpen={openDetail.isOpen}
        data={openDetail.data}
        handleClose={() => setOpenDetail({ isOpen: false, data: null })}
      />
      <DownloadPopup
        isOpen={openDownload}
        handleClose={() => setOpenDownload(false)}
        onDownload={handleDownload}
      />
      <div className={classes.filterGroup}>
        <div style={{ width: "100%", paddingRight: 20 }}>
          <SearchWithDropdown
            dataSearch={searchVal}
            setDataSearch={(event) => setSearchVal(event)}
            placeholder="User ID, Full Name"
          />
        </div>

        <div style={{ width: 250, display: "flex" }}>
          <PopupFilter setDataFilter={setDataFilter} options={popupOption} />

          <GeneralButton
            width="120px"
            label="Download"
            buttonIcon={<SvgDownload />}
            iconPosition="start"
            onClick={() => setOpenDownload(true)}
          />
        </div>
      </div>
      <TableICBB
        headerContent={dataHeader({ history, setOpenDetail })}
        dataContent={financial?.financialReportDtoList ?? []}
        page={page}
        setPage={setPage}
        totalData={100}
        isLoading={isLoading}
        noDataMessage="No Consolidation Report Data"
        bordered
      />
    </div>
  );
};

export default FinancialReportDetail;
