// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import TableICBB from "components/BN/TableIcBB";
import { handleFinancialDetail } from "stores/actions/consolidationReport";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { mapRole } from "./NonFinancial";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paperContainer: {
    width: 480,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    padding: "20px 0",
  },
  paper: {
    padding: "0px 20px 0px 30px",
  },
  aturFilter: {
    fontSize: 24,
    textAlign: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    marginBottom: 51,
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 28,
    fontWeight: 400,
    color: Colors.dark.hard,
    textAlign: "center",
    marginBottom: 10,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  filterInput: {
    display: "flex",
  },
  input: {
    textAlign: "left",
  },
  buttonGroup: {
    height: 84,
    display: "flex",
    justifyContent: "space-between",
    padding: "22px 20px",
    marginTop: 30,
  },
  fileTitle: {
    marginBottom: 33,
    fontFamily: "FuturaBkBT",
    fontSize: 17,
    color: Colors.dark.hard,
  },
}));

const UserInformationPopups = ({ isOpen, handleClose, data }) => {
  const classes = useStyles();
  const { financialDetail, isDetailLoading } = useSelector(
    (e) => e?.consolidationReport
  );

  const dispatch = useDispatch();

  const dataHeader = [
    {
      title: "Informasi User",
      key: "dateTime",
      render: (e) => (
        <div>
          <div style={{ fontFamily: "FuturaMdBT" }}>
            {moment(e?.createDateApproval).format("DD MMMM YYYY HH:mm:ss")}
          </div>
          <div style={{ fontFamily: "FuturaHvBT" }}>{e?.name}</div>
          <div style={{ fontFamily: "FuturaMdBT" }}>{e?.userId}</div>
        </div>
      ),
    },
    {
      title: "Group",
      key: "dateTime",
      render: (e) => e?.groupDto?.name,
    },
    {
      title: "Role",
      key: "role",
      render: (e) => mapRole(e?.role),
    },
  ];

  useEffect(() => {
    if (data?.transactionGroupId && data?.transactionId) {
      const payload = {
        transactionGroupId: data.transactionGroupId,
        transactionId: data.transactionId,
      };
      dispatch(handleFinancialDetail(payload));
    }
  }, [data]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paperContainer}>
            <div className={classes.paper}>
              <div className={classes.xContainer}>
                <XIcon className="close" onClick={handleClose} />
              </div>
              <div>
                <div className={classes.title}>User Information</div>
              </div>
              <TableICBB
                headerContent={dataHeader}
                dataContent={financialDetail?.detailFinanceReportDtoList}
                totalData={100}
                isLoading={isDetailLoading}
                bordered
                scroll={financialDetail?.detailFinanceReportDtoList?.length > 4}
                scrollHeight={400}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

UserInformationPopups.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default UserInformationPopups;
