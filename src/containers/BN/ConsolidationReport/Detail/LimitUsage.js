import GeneralButton from "components/BN/Button/GeneralButton";
import PopupFilter from "containers/BN/ConsolidationReport/Detail/PopupFilter";
import TableICBB from "components/BN/TableIcBB";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";

import { ReactComponent as SVGArrow } from "assets/icons/BN/arrow-right.svg";
import { ReactComponent as SvgDownload } from "assets/icons/BN/download-white.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import Badge from "components/BN/Badge";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  downloadPagesConf,
  handleConsolidationReportDownload,
  handleLimitUsage,
} from "stores/actions/consolidationReport";
import { formatAmount, searchDateEnd, searchDateStart } from "utils/helpers";
import SelectGroup from "components/BN/Select/SelectGroup";
import DownloadPopup from "./popupDownload";

const dataHeader = ({ history }) => [
  {
    title: "Matriks",
    key: "matrix",
  },
  {
    title: "Max. Transaction/day",
    key: "globalLimitTransactionPerDay",
    render: (row) => (
      <div>
        <div>{row?.globalLimitTransactionPerDay} Kali</div>
        <div>
          <span
            style={{ fontFamily: "FuturaMdBT", fontSize: 13, color: "#7B87AF" }}
          >
            Tersisa
          </span>
          : {row?.globalLimitTransactionPerDay - row?.limitTransactionPerDay}{" "}
          Kali
        </div>
      </div>
    ),
  },
  {
    title: "Max. Amount/day",
    key: "globalAmountTransactionPerDay",
    render: (row) => (
      <div>
        <div>Rp. {formatAmount(row?.globalAmountTransactionPerDay)} </div>
        <div>
          <span
            style={{ fontFamily: "FuturaMdBT", fontSize: 13, color: "#7B87AF" }}
          >
            Tersisa
          </span>
          : Rp.
          {formatAmount(
            row?.globalAmountTransactionPerDay - row?.amountTransactionPerDay
          )}
        </div>
      </div>
    ),
  },
  {
    title: "Min. Trx",
    key: "minAmountTransaction",
    render: (e) => `Rp. ${formatAmount(e.minAmountTransaction)}`,
  },
  {
    title: "Max. Trx",
    key: "",
    render: (e) => `Rp. ${formatAmount(e.maxAmountTransaction)}`,
  },
];

const useStyles = makeStyles({
  container: {
    padding: "20px",
  },
  paperpt: {
    backgroundColor: "#fff",
    width: "100%",
    height: "544px",
    borderRadius: 10,
    border: "1px solid #fff",
  },
  corporateName: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  title: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
    paddingTop: 13,
    paddingBlock: 8,
  },
  sideText: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.gray.medium,
  },
  textGroup: {
    padding: 20,
  },
  contentContainer: {
    borderRadius: "10px",
    minHeight: 300,
    backgroundColor: "white",
    padding: "25px 13px 30px 11px",
  },
  filterGroup: {
    display: "flex",
    padding: "16px 14px 23px 13px",
  },
});

const ReportDetail = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [dataFilter, setDataFilter] = useState(null);
  const { corporateId, limitUsage, isLoading, authorization } = useSelector(
    (e) => e?.consolidationReport
  );

  const [openDownload, setOpenDownload] = useState(false);

  useEffect(() => {
    const payload = {
      corporateProfileId: corporateId,
      page: 1,
      size: 10,
    };
    dispatch(handleLimitUsage(payload));
  }, []);

  const handleDownload = (type) =>
    dispatch(
      handleConsolidationReportDownload(
        {
          // additionalSearch: null,
          corporateProfileId: corporateId,
          format: type,
          otorisasi: authorization,
        },
        { pages: downloadPagesConf.LIMITUSAGE_REPORT }
      )
    );

  return (
    <div>
      <DownloadPopup
        isOpen={openDownload}
        handleClose={() => setOpenDownload(false)}
        onDownload={handleDownload}
      />
      <div className={classes.filterGroup}>
        <div style={{ width: "100%", paddingRight: 20 }}>
          <SelectGroup
            placeholder="Pilih Jenis Transaksi"
            style={{
              width: "335px",
              height: "40px",
              marginRight: "50px",
            }}
          />
        </div>

        <GeneralButton
          width="120px"
          label="Download"
          buttonIcon={<SvgDownload />}
          iconPosition="start"
          onClick={() => setOpenDownload(true)}
        />
      </div>
      <div
        style={{
          border: "1px solid #B3CAF43B",
          borderRadius: 10,
        }}
      >
        <div style={{ padding: "15px 20px" }}>
          <div>
            <span
              style={{
                fontFamily: "FuturaHvBT",
                color: "#374062",
                fontSize: 15,
              }}
            >
              Accounting Group
            </span>
          </div>
          <div style={{ marginTop: 10 }}>
            <span
              style={{
                fontFamily: "FuturaMdBT",
                color: "#7B87AF",
                fontSize: 13,
              }}
            >
              Sesama Transfer
            </span>
          </div>
        </div>

        <TableICBB
          tableRounded={false}
          headerContent={dataHeader({ history })}
          dataContent={limitUsage?.limitUsageList}
          page={page}
          setPage={setPage}
          totalData={limitUsage?.totalPages}
          totalElement={limitUsage?.totalElements}
          isLoading={isLoading}
          noDataMessage="No Consolidation Report Data"
          bordered
        />
      </div>
    </div>
  );
};

export default ReportDetail;
