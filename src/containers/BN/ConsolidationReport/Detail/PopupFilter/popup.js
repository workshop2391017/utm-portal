// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";

import Colors from "helpers/colors";
import AntdTextField from "components/BN/TextField/AntdTextField";
import Tabs from "components/BN/Tabs/GeneralTabs";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import AntdSelect from "components/BN/Select/SelectGroup";
import { useDispatch, useSelector } from "react-redux";
import { handleSoftGet } from "stores/actions/consolidationReport";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import moment from "moment/moment";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: "auto",
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 30px 0 30px",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  aturFilter: {
    fontSize: 28,
    fontFamily: "FuturaHvBT",
    color: Colors.dark.hard,
    marginBottom: 41,
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 36,
    fontWeight: 900,
    color: Colors.dark.hard,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  filterInput: {
    display: "flex",
    justifyContent: "space-between",
    gap: 10,
    width: "100%",
  },
  input: {
    textAlign: "left",
    width: "100%",
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    padding: "22px 0",
    marginTop: 20,
  },
  dropdown2Row: {
    display: "flex",
    justifyContent: "space-between",
    gap: "10px",
  },
}));

const FilterPopup = ({
  isOpen,
  handleClose,
  title,
  options,
  setDataFilter,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [filter, setFilter] = useState(null);
  const handleSave = () => {
    setDataFilter(filter);
    handleClose();
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        disableEnforceFocus
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div>
              <div className={classes.aturFilter}>Set Filter</div>
              {(options ?? []).map((elm, i) => (
                <React.Fragment>
                  {elm.type === "rangeDate" ? (
                    <div className={classes.filterInput} key={i}>
                      <div className={classes.input}>
                        <div>{elm?.title?.[0] ?? ""}</div>
                        <AntdTextField
                          style={{ width: "200px" }}
                          type="date"
                          placeholder={elm?.placeholder?.[0] ?? ""}
                          value={filter?.[`startDate${i}`]}
                          onChange={(e) =>
                            setFilter({
                              ...filter,
                              [`startDate${i}`]: e,
                            })
                          }
                          disabledDate={(current) =>
                            current > moment().endOf("day")
                          }
                        />
                      </div>
                      <div className={classes.input}>
                        <div>{elm?.title?.[1] ?? ""}</div>
                        <AntdTextField
                          style={{ width: "200px" }}
                          type="date"
                          placeholder={elm?.placeholder?.[1] ?? ""}
                          value={filter?.[`endDate${i}`]}
                          disabled={!filter?.startDate0}
                          onChange={(e) =>
                            setFilter({
                              ...filter,
                              [`endDate${i}`]: e,
                            })
                          }
                          disabledDate={(current) =>
                            current <
                              moment(
                                moment(filter.startDate0).format("YYYY-MM-DD")
                              ) || current > moment().endOf("day")
                          }
                        />
                      </div>
                    </div>
                  ) : elm.type === "tabs" ? (
                    <div className={classes.input}>
                      <div style={{ marginTop: "31px" }}>
                        {elm?.title ?? ""}
                      </div>
                      <Tabs
                        onChange={(event, newValue) => {
                          setFilter({
                            ...filter,
                            [`tabs${i}`]: newValue,
                          });
                        }}
                        value={filter?.[`tabs${i}`]}
                        tabWidth={110}
                        tabs={elm.options}
                        style={{ height: "30px", width: "100%" }}
                      />
                    </div>
                  ) : elm.type === "dropdown" ? (
                    <div>
                      <div className={classes.input} style={{ marginTop: 31 }}>
                        <div>{elm?.title ?? ""}</div>
                        <SelectWithSearch
                          placeholder={elm.placeholder}
                          value={filter?.[`dropdown${i}`]}
                          style={{ width: "100%" }}
                          onChange={(e) =>
                            setFilter({
                              ...filter,
                              [`dropdown${i}`]: e,
                            })
                          }
                          options={elm.options}
                        />
                      </div>
                    </div>
                  ) : elm.type === "dropdown2Row" ? (
                    <div className={classes.dropdown2Row}>
                      <div className={classes.input} style={{ marginTop: 31 }}>
                        <div>{elm?.title?.[0] ?? ""}</div>
                        <SelectWithSearch
                          placeholder={elm?.placeholder?.[0] ?? ""}
                          value={filter?.[`dropdown1${i}`]}
                          style={{ width: "100%" }}
                          onChange={(e) =>
                            setFilter({
                              ...filter,
                              [`dropdown1${i}`]: e,
                            })
                          }
                          options={elm.options[0]}
                        />
                      </div>
                      <div className={classes.input} style={{ marginTop: 31 }}>
                        <div>{elm?.title?.[1] ?? ""}</div>
                        <SelectWithSearch
                          placeholder={elm?.placeholder?.[1] ?? ""}
                          value={filter?.[`dropdown2${i}`]}
                          style={{ width: "100%" }}
                          onChange={(e) =>
                            setFilter({
                              ...filter,
                              [`dropdown2${i}`]: e,
                            })
                          }
                          options={elm.options[1]}
                        />
                      </div>
                    </div>
                  ) : null}
                </React.Fragment>
              ))}
            </div>

            <div className={classes.buttonGroup}>
              <ButtonOutlined
                label="Cancel"
                width={157.5}
                style={{ marginRight: 11 }}
                onClick={handleClose}
              />

              <GeneralButton label="Save" width={157.5} onClick={handleSave} />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

FilterPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  options: PropTypes.array,
  setDataFilter: PropTypes.func,
};

FilterPopup.defaultProps = {
  title: "Berhasil",
  options: [],
  setDataFilter: () => {},
};

export default FilterPopup;
