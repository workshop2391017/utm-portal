import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import React, { useState } from "react";

// assets
import { ReactComponent as SvgFilter } from "assets/icons/BN/filter.svg";
import { useDispatch, useSelector } from "react-redux";
import { setSofData } from "stores/actions/consolidationReport";

import FilterPopup from "./popup";

const PopupFilter = ({ options, setDataFilter }) => {
  const dispatch = useDispatch();
  const [openPopup, setOpenPopup] = useState(false);

  return (
    <div>
      <ButtonOutlined
        width="111px"
        label="Filter"
        buttonIcon={<SvgFilter />}
        iconPosition="start"
        style={{ marginRight: 20 }}
        onClick={() => setOpenPopup(true)}
      />
      <FilterPopup
        isOpen={openPopup}
        handleClose={() => {
          dispatch(setSofData([]));
          setOpenPopup(false);
        }}
        options={options}
        setDataFilter={setDataFilter}
      />
    </div>
  );
};

export default PopupFilter;
