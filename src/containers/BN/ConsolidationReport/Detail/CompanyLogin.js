import GeneralButton from "components/BN/Button/GeneralButton";
import PopupFilter from "containers/BN/ConsolidationReport/Detail/PopupFilter";
import TableICBB from "components/BN/TableIcBB";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import { ReactComponent as SvgDownload } from "assets/icons/BN/download-white.svg";
import Badge from "components/BN/Badge";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  downloadPagesConf,
  handleConsolidationCompanyLogin,
  handleConsolidationCompanyLoginDetail,
  handleConsolidationReportDownload,
  handleSoftGet,
  setCompanyLoginId,
  setFullName,
  setTransactionTypeConf,
} from "stores/actions/consolidationReport";
import { searchDate, searchDateEnd, searchDateStart } from "utils/helpers";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import useDebounce from "utils/helpers/useDebounce";
import moment from "moment";
import { pathnameCONFIG } from "configuration";
import DownloadPopup from "./popupDownload";
import { mapRole } from "./NonFinancial";

const headerMultiple = ({ history, dispatch }) => [
  {
    title: "Date",
    render: (row) => (
      <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
        {moment(row?.dateTimeLogin).format("YYYY-MM-DD")}
      </div>
    ),
  },
  {
    title: "User Information",
    render: (row) => (
      <div>
        <div style={{ color: "#374062" }}>{row?.fullName}</div>
        <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
          {row?.username}
        </div>
      </div>
    ),
  },
  {
    title: "Group",
    key: "userGroup",
  },
  {
    title: "Role",
    key: "role",
    width: 250,
    render: (rowData) => mapRole(rowData.role),
  },
  {
    title: "Login",
    render: (rowData) => rowData?.loginTime?.slice(0, -3) ?? "-",
  },
  {
    title: "",
    textAlign: "center",
    width: 100,
    render: (rowData) => (
      <div>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{ marginRight: 11, fontSize: 9, padding: 0 }}
          onClick={() => {
            dispatch(setCompanyLoginId(rowData?.loginId));
            dispatch(setFullName(rowData?.fullName));
            history.push(
              pathnameCONFIG.CONSOLIDATION_REPORT.COMPANY_LOGIN_DETAIL
            );
          }}
        />
      </div>
    ),
  },
];

const headerSingle = ({ history, dispatch }) => [
  {
    title: "Date",
    render: (row) => (
      <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
        {moment(row?.dateTimeLogin).format("YYYY-MM-DD")}
      </div>
    ),
  },
  {
    title: "User Information",
    render: (row) => (
      <div>
        <div style={{ color: "#374062" }}>{row?.fullName}</div>
        <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
          {row?.username}
        </div>
      </div>
    ),
  },
  {
    title: "Login",
    render: (rowData) => rowData?.loginTime?.slice(0, -3) ?? "-",
  },
  {
    title: "",
    textAlign: "center",
    width: 110,
    render: (rowData) => (
      <div>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{ marginRight: 11, fontSize: 9, padding: 0 }}
          onClick={() => {
            dispatch(setCompanyLoginId(rowData?.loginId));
            dispatch(setFullName(rowData?.fullName));
            history.push(
              pathnameCONFIG.CONSOLIDATION_REPORT.COMPANY_LOGIN_DETAIL
            );
          }}
        />
      </div>
    ),
  },
];

const useStyles = makeStyles({
  container: {
    padding: "20px",
  },
  paperpt: {
    backgroundColor: "#fff",
    width: "100%",
    height: "544px",
    borderRadius: 10,
    border: "1px solid #fff",
  },
  corporateName: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  title: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
    paddingTop: 13,
    paddingBlock: 8,
  },
  sideText: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.gray.medium,
  },
  textGroup: {
    padding: 20,
  },
  contentContainer: {
    borderRadius: "10px",
    minHeight: 300,
    backgroundColor: "white",
    padding: "25px 13px 30px 11px",
  },
  filterGroup: {
    display: "flex",
    padding: "16px 14px 23px 13px",
  },
  prefix: {
    backgroundColor: Colors.primary.hard,
    width: 110,
    height: 40,
    marginLeft: -20,
    marginTop: -1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
    fontFamily: "FuturaMdBT",
  },
});

const CompanyLoginReportDetail = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openDownload, setOpenDownload] = useState(false);
  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);
  const [searchVal, setSearchVal] = useState("");

  const { corporateId, workflowGroup, companyLogin, isLoading, authorization } =
    useSelector((e) => e?.consolidationReport);

  const roleOption = ["Approver", "Maker", "Releaser"];

  const dataHeader = authorization === "SINGLE" ? headerSingle : headerMultiple;

  const searchDeb = useDebounce(searchVal, 1300);

  useEffect(() => {
    if (corporateId) {
      const payload = {
        corporateProfileId: corporateId,
        startDate: dataFilter?.startDate0 ? dataFilter?.startDate0 : new Date(),
        endDate: dataFilter?.endDate0 ? dataFilter?.endDate0 : new Date(),
        groupId: dataFilter?.dropdown11,
        page: page - 1,
        searchValue: searchDeb,
        size: 10,
        transactionGroup: setTransactionTypeConf[dataFilter?.tabs1],
        role: dataFilter?.dropdown21,
      };

      dispatch(handleConsolidationCompanyLogin(payload));
    }
  }, [corporateId, page, dataFilter, searchDeb]);

  const handleDownload = (type) =>
    dispatch(
      handleConsolidationReportDownload(
        {
          companyLoginReportList: companyLogin?.reportCompanyLoginDtoList ?? [],
          dateFrom: dataFilter?.startDate0
            ? dataFilter?.startDate0
            : new Date(),
          dateTo: dataFilter?.endDate0 ? dataFilter?.endDate0 : new Date(),
          format: type,
          otorisasi: authorization,
        },
        { pages: downloadPagesConf.COMPANY_LOGIN }
      )
    );

  const popupOption =
    authorization === "SINGLE"
      ? [
          {
            id: 1,
            type: "rangeDate",
            title: ["Start Date", "End Date"],
            placeholder: ["DD/MM/YYYY", "DD/MM/YYYY"],
          },
        ]
      : authorization === "MULTIPLE"
      ? [
          {
            id: 1,
            type: "rangeDate",
            title: ["Mulai Dari", "Berakhir Pada"],
            placeholder: ["Mulai Dari", "Berakhir Pada"],
          },
          {
            id: 2,
            type: "dropdown2Row",
            options: [
              workflowGroup?.getGroupDtoList?.map((e) => ({
                label: e?.name,
                value: e?.workflowGroupId,
              })) ?? [],
              roleOption.map((e) => ({ label: e, value: e })) ?? [],
            ],
            title: ["User  Group", "Role"],
            placeholder: ["User  Group", "Role"],
          },
        ]
      : null;

  return (
    <div>
      <DownloadPopup
        isOpen={openDownload}
        handleClose={() => setOpenDownload(false)}
        onDownload={handleDownload}
      />
      <div className={classes.filterGroup}>
        <div style={{ width: "100%", paddingRight: 20 }}>
          <SearchWithDropdown
            dataSearch={searchVal}
            setDataSearch={(event) => setSearchVal(event)}
            placeholder="Full Name"
          />
        </div>

        <div style={{ width: 250, display: "flex" }}>
          <PopupFilter setDataFilter={setDataFilter} options={popupOption} />

          <GeneralButton
            width="120px"
            label="Download"
            buttonIcon={<SvgDownload />}
            iconPosition="start"
            onClick={() => setOpenDownload(true)}
          />
        </div>
      </div>
      <TableICBB
        headerContent={dataHeader({ history, dispatch })}
        dataContent={companyLogin?.reportCompanyLoginDtoList ?? []}
        page={page}
        setPage={setPage}
        totalData={companyLogin?.totalPages}
        totalElement={companyLogin?.numberOfElements}
        isLoading={isLoading}
        noDataMessage="No Consolidation Report Data"
        bordered
      />
    </div>
  );
};

export default CompanyLoginReportDetail;
