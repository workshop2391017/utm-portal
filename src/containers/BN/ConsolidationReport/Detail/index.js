import { Button, makeStyles } from "@material-ui/core";
import MenuList from "components/BN/MenuList";
import Title from "components/BN/Title";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  handleConsolidationDetail,
  handleWorkflowGroup,
  setError,
} from "stores/actions/consolidationReport";
import Toast from "components/BN/Toats";
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import NonFinancial from "./NonFinancial";
import FinancialReportDetail from "./Financial";
import CompanyLoginReportDetail from "./CompanyLogin";
import LimitUsage from "./LimitUsage";
import CompanyConsolisationDetail from "./CompanyCondolistaion";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "20px",
  },
  titleContainer: {
    display: "flex",
    flexDirection: "column",
    padding: "10px 10px 0px 30px",
  },
  paperpt: {
    backgroundColor: "#fff",
    width: "100%",
    height: "544px",
    borderRadius: 10,
    border: "1px solid #fff",
  },
  corporateName: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  title: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
    paddingTop: 13,
    paddingBlock: 8,
  },
  sideText: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.gray.medium,
  },
  textGroup: {
    padding: 20,
  },
  contentContainer: {
    borderRadius: "10px",
    minHeight: 300,
    backgroundColor: "white",
    padding: "25px 13px 30px 11px",
  },
  filterGroup: {
    display: "flex",
    padding: "16px 14px 23px 13px",
  },
  backButton: {
    ...theme.typography.backButton,
    width: "20px",
    paddingLeft: 0,
  },
}));

const menuOptions = [
  {
    key: "sub1",
    name: "Non Financial",
  },
  {
    key: "sub2",
    name: "Financial",
  },
  {
    key: "sub3",
    name: "Company Login",
  },
  {
    key: "sub4",
    name: "Limit Usage",
  },
];

const Details = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [menuSelected, setMenuSelected] = useState("Non Financial");
  const [keySelected, setKeySelected] = useState("sub1");
  const { corporateId, detail, error, authorization } = useSelector(
    (e) => e?.consolidationReport
  );

  const classes = useStyles();

  const handleRenderSidePages = () => {
    switch (menuSelected) {
      case "Non Financial":
        return <NonFinancial />;
      case "Financial":
        return <FinancialReportDetail />;
      case "Company Login":
        return <CompanyLoginReportDetail />;
      case "Limit Usage":
        return <LimitUsage />;
      case "Company Consolidation":
        return <CompanyConsolisationDetail />;

      default:
        return "None";
    }
  };

  useEffect(() => {
    if (!corporateId) history.goBack();

    if (corporateId) {
      const payload = {
        corporateProfileId: corporateId,
      };
      dispatch(handleConsolidationDetail(payload));
    }
  }, [corporateId]);

  useEffect(() => {
    if (corporateId) {
      const payload = {
        corporateProfileId: corporateId,
      };
      dispatch(handleWorkflowGroup(payload));
    }
  }, [corporateId]);

  const handleClickBack = () => {
    history.goBack();
  };

  return (
    <div>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setError({ isError: false, message: "" }))}
      />
      <div className={classes.titleContainer}>
        <Button
          size="small"
          startIcon={<ArrowLeft />}
          onClick={handleClickBack}
          className={classes.backButton}
        >
          Back
        </Button>
        <Title label="Report" paddingLeft={0} />
      </div>

      <div className={classes.container}>
        <div className={classes.container}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ width: 260, height: 544 }}>
              <div className={classes.paperpt}>
                <div className={classes.textGroup}>
                  <h1 className={classes.corporateName}>{detail?.name}</h1>
                  <h1 className={classes.title}>{detail?.accountNumber}</h1>
                  <h1 className={classes.sideText}>
                    {authorization} Authorization
                  </h1>
                  <h1 className={classes.sideText}>{detail?.contact}</h1>
                  <h1 className={classes.sideText}>{detail?.address}</h1>
                </div>

                <MenuList
                  menuOptions={menuOptions}
                  onTitleClick={(e, key) => {
                    setMenuSelected(e?.name);
                    setKeySelected(key);
                  }}
                  mode="vertical"
                  selectedKeys={keySelected}
                />
              </div>
            </div>
            <div style={{ width: "100%", paddingLeft: 20 }}>
              <div className={classes.contentContainer}>
                {handleRenderSidePages()}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;
