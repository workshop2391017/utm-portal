import GeneralButton from "components/BN/Button/GeneralButton";
import PopupFilter from "containers/BN/ConsolidationReport/Detail/PopupFilter";
import TableICBB from "components/BN/TableIcBB";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import { ReactComponent as SvgDownload } from "assets/icons/BN/download-white.svg";
import Badge from "components/BN/Badge";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import SearchWithoutDropdown from "components/BN/Search/SearchWithoutDropdown";
import { useDispatch, useSelector } from "react-redux";
import {
  downloadPagesConf,
  handleConsolidationNonFinancial,
  handleConsolidationReportDownload,
  handleSoftGet,
  setTransactionTypeConf,
} from "stores/actions/consolidationReport";
import useDebounce from "utils/helpers/useDebounce";
import { searchDate, searchDateEnd, searchDateStart } from "utils/helpers";
import moment from "moment";
import DownloadPopup from "./popupDownload";

export const mapRole = (type) => {
  const roleColor = {
    RELEASER: "orange",
    MAKER: "blue",
    APPROVER: "green",
    ADMIN_USER_RELEASER: "orange",
    ADMIN_USER_MAKER: "blue",
    ADMIN_USER_APPROVER: "green",
  };
  if (
    type === "ADMIN_USER_RELEASER" ||
    type === "ADMIN_USER_MAKER" ||
    type === "ADMIN_USER_APPROVER"
  )
    return (
      <div style={{ display: "flex" }}>
        <Badge
          styleBadge={{ marginRight: 5 }}
          label={
            <span style={{ textTransform: "capitalize" }}>
              {type
                ?.split("_")
                .filter((e) => e !== "USER")
                .join(" ")
                .toLowerCase()}
            </span>
          }
          type={roleColor[type]}
          outline
        />
      </div>
    );
  return (
    <div style={{ display: "flex" }}>
      {type
        ?.split("_")
        .filter((e) => e !== "USER")
        .map((e) => (
          <Badge
            styleBadge={{ marginRight: 5 }}
            label={
              <span style={{ textTransform: "capitalize" }}>
                {e?.toLowerCase()}
              </span>
            }
            type={roleColor[e]}
            outline
          />
        ))}
    </div>
  );
};

const headerMultiple = () => [
  {
    title: "User Information",
    key: "dateTime",
    render: (row) => (
      <div>
        <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
          {row?.dateTime}
        </div>
        <div style={{ color: "#374062" }}>{row?.fullName}</div>
        <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
          {row?.userId}
        </div>
      </div>
    ),
  },
  {
    title: "User Group",
    key: "userGroupName",
    width: 100,
  },
  {
    title: "Role",
    key: "role",
    render: (rowData) => mapRole(rowData.role),
  },
  {
    title: "Activity",
    key: "activity",
  },
];

const headerSingle = () => [
  {
    title: "User Information",
    key: "dateTime",
    render: (row) => (
      <div>
        <div style={{ fontFamily: "FuturaBkBT", color: "#374062" }}>
          {row?.dateTime}
        </div>
      </div>
    ),
  },
  {
    title: "User ID",
    key: "username",
    width: 100,
  },
  {
    title: "Username",
    key: "fullName",
  },
  {
    title: "Activity",
    key: "activity",
  },
];

const useStyles = makeStyles({
  container: {
    padding: "20px",
  },
  paperpt: {
    backgroundColor: "#fff",
    width: "100%",
    height: "544px",
    borderRadius: 10,
    border: "1px solid #fff",
  },
  corporateName: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  title: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
    paddingTop: 13,
    paddingBlock: 8,
  },
  sideText: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.gray.medium,
  },
  textGroup: {
    padding: 20,
  },
  contentContainer: {
    borderRadius: "10px",
    minHeight: 300,
    backgroundColor: "white",
    padding: "25px 13px 30px 11px",
  },
  filterGroup: {
    display: "flex",
    padding: "16px 14px 23px 13px",
  },
  prefix: {
    backgroundColor: Colors.primary.hard,
    width: 110,
    height: 40,
    marginLeft: -20,
    marginTop: -1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
    fontFamily: "FuturaMdBT",
  },
});

const ReportDetail = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openDownload, setOpenDownload] = useState(false);
  const [page, setPage] = useState(1);
  const [dataFilter, setDataFilter] = useState(null);
  const [searchVal, setSearchVal] = useState("");

  const { corporateId, nonFinancial, isLoading, workflowGroup, authorization } =
    useSelector((e) => e?.consolidationReport);

  const searchDeb = useDebounce(searchVal, 1300);

  const dataHeader = authorization === "SINGLE" ? headerSingle : headerMultiple;

  useEffect(() => {
    if (corporateId) {
      const payload = {
        corporateProfileId: corporateId,
        page: page - 1,
        size: 10,
        groupId: dataFilter?.dropdown1,
        startDate: dataFilter?.startDate0 ? dataFilter?.startDate0 : new Date(),
        endDate: dataFilter?.endDate0 ? dataFilter?.endDate0 : new Date(),
        activityCategory: null,
        // activityName: searchDeb,
        searchValue: searchDeb,
        transactionGroup: null,
      };

      dispatch(handleConsolidationNonFinancial(payload));
    }
  }, [corporateId, page, dataFilter, searchDeb]);

  const handleDownload = (type) =>
    dispatch(
      handleConsolidationReportDownload(
        {
          otorisasi: authorization,
          dateFrom: dataFilter?.startDate0
            ? dataFilter?.startDate0
            : new Date(),
          dateTo: dataFilter?.endDate0 ? dataFilter?.endDate0 : new Date(),
          format: type,
          nonFinancialReportList: nonFinancial?.auditDataList ?? [],
        },
        { pages: downloadPagesConf.NONFINANCIAL_REPORT }
      )
    );

  const popupOption =
    authorization === "SINGLE"
      ? [
          {
            id: 1,
            type: "rangeDate",
            title: ["Start Date", "End Date"],
            placeholder: ["DD/MM/YYYY", "DD/MM/YYYY"],
          },
        ]
      : authorization === "MULTIPLE"
      ? [
          {
            id: 1,
            type: "rangeDate",
            title: ["Start Date", "End Date"],
            placeholder: ["DD/MM/YYYY", "DD/MM/YYYY"],
          },
          {
            id: 2,
            type: "dropdown",
            options:
              workflowGroup?.getGroupDtoList?.map((e) => ({
                label: e.name,
                value: e?.workflowGroupId,
              })) ?? [],
            title: "User Group",
            placeholder: "User Group",
          },
        ]
      : null;

  return (
    <div>
      <DownloadPopup
        isOpen={openDownload}
        handleClose={() => setOpenDownload(false)}
        onDownload={handleDownload}
      />
      <div className={classes.filterGroup}>
        <div style={{ width: "100%", paddingRight: 20 }}>
          <SearchWithoutDropdown
            style={{ marginRight: 20, width: "100% !important" }}
            placeholder="User ID, Full Name"
            dataSearch={searchVal}
            setDataSearch={(event) => setSearchVal(event)}
          />
        </div>

        <div style={{ width: 250, display: "flex" }}>
          <PopupFilter setDataFilter={setDataFilter} options={popupOption} />

          <GeneralButton
            width="120px"
            label="Download"
            buttonIcon={<SvgDownload />}
            iconPosition="start"
            onClick={() => setOpenDownload(true)}
          />
        </div>
      </div>
      <TableICBB
        bordered
        headerContent={dataHeader({ history })}
        dataContent={nonFinancial?.auditDataList ?? []}
        page={page}
        setPage={setPage}
        totalData={nonFinancial?.totalPages}
        totalElement={nonFinancial?.totalElements}
        noDataMessage="No Consolidation Report Data"
        isLoading={isLoading}
      />
    </div>
  );
};

export default ReportDetail;
