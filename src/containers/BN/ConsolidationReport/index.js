import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { makeStyles, Typography, Grid } from "@material-ui/core";

import Title from "components/BN/Title";
import TableICBB from "components/BN/TableIcBB";
import Badge from "components/BN/Badge";
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";

import Colors from "helpers/colors";
import {
  capitalizeFirstLetterFromUppercase,
  setLocalStorage,
} from "utils/helpers";

import { pathnameCONFIG } from "configuration";

import {
  handleConsolidation,
  setCorporateid,
  setAuthorization,
} from "stores/actions/consolidationReport";

const useStyles = makeStyles({
  container: {
    padding: "20px",
  },
});

const dataHeader = ({ history, dispatch }) => [
  {
    title: "Date & Time",
    render: (rowData) => (
      <Grid container wrap="nowrap">
        <Grid item zeroMinWidth>
          <Typography variant="subtitle1" noWrap>
            12-06-2021 | 12:22:42
          </Typography>
        </Grid>
      </Grid>
    ),
  },
  {
    title: "Company ID",
    render: (rowData) => (
      <Grid container wrap="nowrap">
        <Grid item zeroMinWidth>
          <Typography variant="subtitle1" noWrap>
            {rowData?.corporateId}
          </Typography>
        </Grid>
      </Grid>
    ),
  },
  {
    title: "Company Name",
    render: (rowData) => (
      <Grid container wrap="nowrap">
        <Grid item zeroMinWidth>
          <Typography variant="subtitle1" noWrap>
            {rowData?.corporateName}
          </Typography>
        </Grid>
      </Grid>
    ),
  },
  {
    title: "Phone Number",
    render: (rowData) => (
      <Grid container wrap="nowrap">
        <Grid item zeroMinWidth>
          <Typography variant="subtitle1" noWrap>
            {rowData?.corporateContact}
          </Typography>
        </Grid>
      </Grid>
    ),
  },
  {
    title: "Address",
    render: (rowData) => (
      <Grid container wrap="nowrap">
        <Grid item zeroMinWidth>
          <Typography variant="subtitle1" noWrap>
            {rowData?.corporateAddress}
          </Typography>
        </Grid>
      </Grid>
    ),
  },

  {
    title: "Business Category",
    render: (rowData) => {
      const badgeColor = {
        SINGLE: "green",
        MULTIPLE: "blue",
      };
      return (
        <Grid container style={{ paddingLeft: 24 }}>
          <Grid
            item
            style={{
              display: "flex",
              justifyContent: "center",
              width: 60,
            }}
          >
            <Badge
              label={
                <span>
                  {capitalizeFirstLetterFromUppercase(rowData?.otorisasi)}
                </span>
              }
              type={badgeColor[rowData?.otorisasi]}
              outline
            />
          </Grid>
        </Grid>
      );
    },
  },
  {
    title: "",
    width: 80,
    render: (rowData) => (
      <GeneralButton
        label="View Details"
        width="76px"
        height="23px"
        style={{ marginRight: 11, fontSize: 9, padding: 0 }}
        onClick={() => {
          dispatch(setCorporateid(rowData?.id));
          dispatch(setAuthorization(rowData?.otorisasi));
          history.push(pathnameCONFIG.CONSOLIDATION_REPORT.DETAIL);
        }}
      />
    ),
  },
];

const options = [
  {
    id: 1,
    type: "input",
    placeholder: "Search...",
    tooltipPlaceholder: "Company ID, Company Name",
    tooltipPlacement: "bottom",
  },
  {
    id: 2,
    type: "tabs",
    options: ["All", "Single", "Multiple"],
  },
];

const SegmentationReport = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { data, isLoading } = useSelector(
    ({ consolidationReport }) => consolidationReport
  );

  const [dataFilter, setDataFilter] = useState(null);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const authorization = [null, "SINGLE", "MULTIPLE"];
    const payload = {
      pageNumber: page - 1,
      pageSize: 10,
      searchValue: dataFilter?.date?.dateValue1,
      authorizationType: authorization[dataFilter?.tabs?.tabValue2],
    };
    dispatch(handleConsolidation(payload));
    console.log(data);
  }, [dataFilter, page]);

  console.log("dataFilter =>", dataFilter);
  return (
    <div style={{ backgroundColor: Colors.gray.ultrasoft }}>
      <Title label="Consolidation Report" />
      <div className={classes.container}>
        <div style={{ marginBottom: 20 }}>
          <Filter
            dataFilter={dataFilter}
            setDataFilter={setDataFilter}
            align="left"
            title="Show :"
            options={options}
          />
        </div>

        <TableICBB
          headerContent={dataHeader({ history, dispatch })}
          dataContent={data?.corporateProfileList ?? []}
          page={page}
          setPage={setPage}
          totalData={data?.totalPages}
          totalElement={data?.totalElements}
          isLoading={isLoading}
        />
      </div>
    </div>
  );
};

export default SegmentationReport;
