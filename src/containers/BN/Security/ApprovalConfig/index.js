// main
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Button, CircularProgress } from "@material-ui/core";
import Checkbox from "components/SC/Checkbox/CheckboxSingle";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import illustrationX from "assets/images/BN/illustrationred.png";

// helpers
import {
  getLocalStorage,
  handleConvertRole,
  searchDate,
  searchDateEnd,
  searchDateStart,
  setLocalStorage,
} from "utils/helpers";

// redux
import {
  handleConfigData,
  setConfigId,
  SETUJUI_PENGGUNA,
  TOLAK_PENGGUNA,
  submitApprovalConfig,
  setHandleClearError,
} from "stores/actions/approvalConfig";
import { pathnameCONFIG } from "configuration";
import { checkOnline } from "stores/actions/errorGeneral";

// components
import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Filter from "components/BN/Filter/GeneralFilter";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeletePopup from "components/BN/Popups/Delete";
import AddComment from "components/BN/Popups/AddComment";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import TableICBB from "components/BN/TableIcBB";
import Toast from "components/BN/Toats";
import Badge from "components/BN/Badge";
import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";

const ApprovalConfig = (props) => {
  // const {} = props;
  const useStyles = makeStyles({
    approvalConfig: {},
    container: {
      padding: "20px 30px 30px",
    },
    buttonGroup: {
      display: "flex",
      "& .MuiButton-outlinedPrimary.Mui-disabled": {
        border: "1px solid #BCC8E7",
        color: "#BCC8E7",
        backgroundColor: "#fff",
      },
      "& .MuiButton-containedPrimary.Mui-disabled": {
        backgroundColor: "#BCC8E7",
        color: "#fff",
      },
    },
    buttonTolak: {
      fontFamily: "Futura",
      borderRadius: "6px !important",
      border: "1px solid #0061A7",
      color: "#0061A7",
      marginRight: "25px",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
    buttonSetujui: {
      fontFamily: "FuturaMdBT",
      fontSize: 9,
      borderRadius: "6px !important",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
      textTransform: "none",
    },
  });
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { data, totalPage, totalElement, isLoading, error, isLoadingSubmit } =
    useSelector(({ approvalConfig }) => ({
      ...approvalConfig,
      data: approvalConfig.data?.listApproval ?? [],
      totalPage: approvalConfig?.data?.totalPage ?? 0,
      totalElement: approvalConfig?.data?.totalElement ?? 0,
    }));

  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);

  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(1);

  const [checkList, setCheckList] = useState([]);
  const [disabledList, setDisabledList] = useState(0);

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    type: null,
    message: "",
  });
  // const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [commentModal, setCommentModal] = useState(false);

  const handleCheckbox = (item) => {
    if (!checkList.includes(item)) {
      setCheckList([...checkList, item]);
    } else {
      const arr = checkList.filter((n) => n !== item);
      setCheckList(arr);
    }
  };

  // useEffect(async () => {
  // const params = await JSON.parse(getLocalStorage("detailParams"));
  // if (params.page) setPage(params.page);
  // }, [localStorage]);

  const clickDetail = (rowData) => {
    dispatch(setConfigId([rowData.id]));
    const lsParams = {
      config_id: rowData.id,
      page,
      isAllowedTransaction: rowData?.isAllowedTransaction,
    };
    setLocalStorage("detailParams", JSON.stringify(lsParams));
    history.push(pathnameCONFIG.APPROVAL.DETAIL_APPROVAL_CONFIG);
  };

  const handleClickAll = (e) => {
    if (e.target.checked) {
      const arr = data
        .filter(
          (item) => item?.status === 0 && item.isAllowedTransaction === true
        )
        .map((item) => item.id);
      const dataTemp = [...checkList, ...arr];

      setCheckList(dataTemp);
    } else {
      setCheckList([]);
    }
  };

  // const dataLength =
  //   (data ?? [])?.length - disabledList === 0
  //     ? checkList.length + 1
  //     : (data ?? [])?.length - disabledList;

  const isRowCheck = () => {
    let check = false;
    const dataFilter = data?.filter(
      ({ status, isAllowedTransaction }) =>
        status === 0 && isAllowedTransaction === true
    );

    const dataCheck = dataFilter.filter((elm) =>
      checkList.find((element) => elm.id === element)
    );
    const dataCount =
      dataCheck.length === 0 ? dataFilter.length + 1 : dataCheck.length;
    if (dataCount === dataFilter.length) check = true;
    return check;
  };

  const tableConfig = [
    {
      title: "Username",
      key: "username",
      render: (rowData) => rowData?.username ?? "-",
    },
    {
      title: "NIP",
      key: "nip",
      render: (rowData) => rowData?.nip ?? "-",
    },
    {
      title: "Role",
      key: "role",
      render: (rowData) => handleConvertRole(rowData?.role ?? "-"),
    },
    {
      title: "Date & Time",
      key: "date",
      render: (rowData) =>
        moment(rowData.approvalDate).format("DD-MM-YYYY HH:mm:ss"),
    },
    {
      title: "Activity",
      key: "activityName",
    },
    {
      title: "Status",
      key: "status",
      render: (rowData) => {
        if (rowData.status === 0) {
          return (
            <Badge
              label={rowData?.approvedStatusSequence}
              outline
              type="blue"
            />
          );
        }
        if (rowData.status === 1) {
          return (
            <Badge
              label={rowData?.approvedStatusSequence}
              outline
              type="green"
            />
          );
        }
        return (
          <Badge label={rowData?.approvedStatusSequence} outline type="red" />
        );
      },
    },
    {
      title: "",
      headerAlign: "right",
      align: "right",
      width: "80px",
      render: (rowData) => (
        <div style={{ display: "flex" }}>
          <GeneralButton
            style={{
              padding: 0,
              fontSize: 9,
              width: 76,
              height: 23,
            }}
            onClick={() => clickDetail(rowData)}
            label="View Details"
          />
        </div>
      ),
    },
  ];

  const hanldeLoadData = async (dataSearch) => {
    await dispatch(checkOnline(dispatch));

    const statusFilter = dataFilter?.tabs?.tabValue3;

    const status =
      statusFilter === 0
        ? null
        : statusFilter === 1
        ? 0
        : statusFilter === 2
        ? 2
        : statusFilter === 3
        ? 1
        : null;

    const requestPayload = {
      activity: null,
      direction: "DESC",
      endDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      limit: 10,
      searchValue: dataSearch || null,
      page: page - 1,
      startDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      isConfig: true,
      status,
    };

    dispatch(handleConfigData(requestPayload));
  };

  const searchDeb = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(searchDeb);

  // Get dataTable
  useEffect(() => {
    if (page === 1) {
      hanldeLoadData(searchDeb);
    } else if (searchDeb !== prevSearch) {
      setPage(1);
    } else {
      hanldeLoadData(searchDeb);
    }
  }, [page, dataFilter, searchDeb]);

  // Get number of disabled list
  useEffect(() => {
    if (data) {
      const arr = data.filter(
        (item) => item.status !== 0 || !item?.isAllowedTransaction
      );
      setDisabledList(arr.length);
    }
  }, [data, page]);

  // Reset checklist data when change page
  // useEffect(() => {
  //   setCheckList([]);
  // }, [page]);

  // Multiple approve
  const handleAction = async ({ comment, type }) => {
    await dispatch(checkOnline(dispatch));

    const submit = await submitApprovalConfig({
      checkList,
      data,
      status: type,
      comment,
    })(dispatch);

    if (submit) {
      setCommentModal({ isOpen: false, type: null });
      if (type === SETUJUI_PENGGUNA) {
        setSuccessConfirmModal({
          isOpen: true,
          type: 1,
          message: `[${checkList.length}] Activity Successfully Approved`,
        });
        setCommentModal(false);
      } else if (type === TOLAK_PENGGUNA) {
        // setFailedConfirmModal(true);
        setSuccessConfirmModal({
          isOpen: true,
          type: 2,
          message: `[${checkList.length}] Activity Successfully Rejected`,
        });
        setCommentModal(false);
      }
      hanldeLoadData();
    }
  };

  return (
    <div className={classes.approvalConfig}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Title label="Parameter Approval">
        <Search
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Username, Activity"
          placement="left"
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />
        <div style={{ marginTop: 20 }}>
          <TableICBB
            selected
            headerContent={tableConfig}
            dataContent={data}
            page={page}
            setPage={setPage}
            isLoading={isLoading}
            totalElement={totalElement}
            totalData={totalPage}
            selectedValue={checkList}
            extraComponents={
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  paddingRight: "40px",
                }}
              >
                <ButtonOutlined
                  disabled={isLoadingSubmit}
                  label={
                    isLoadingSubmit ? (
                      <CircularProgress size={20} color="primary" />
                    ) : checkList.length < 2 ? (
                      "Reject "
                    ) : (
                      "Reject All"
                    )
                  }
                  width="135px"
                  style={{ marginRight: 11 }}
                  onClick={() => setDeleteModal(true)}
                />
                <GeneralButton
                  loading={isLoadingSubmit}
                  label={
                    isLoadingSubmit ? (
                      <CircularProgress size={20} color="primary" />
                    ) : checkList.length < 2 ? (
                      "Approve "
                    ) : (
                      "Approve All"
                    )
                  }
                  width={134}
                  onClick={() => handleAction({ type: SETUJUI_PENGGUNA })}
                />
              </div>
            }
          />
        </div>
      </div>
      <SuccessConfirmation
        isOpen={successConfirmModal?.isOpen}
        handleClose={() => {
          setSuccessConfirmModal({ isOpen: false, type: null, message: "" });
          setCheckList([]);
          hanldeLoadData();
        }}
        title="Success"
        img={successConfirmModal?.type === 2 ? illustrationX : null}
        message={successConfirmModal?.message}
        submessage="Some activity may need approval"
      />
      <DeletePopup
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={() => {
          setDeleteModal(false);
          setCommentModal(true);
        }}
        title="Confirmation"
      />
      <AddComment
        isOpen={commentModal}
        handleClose={() => setCommentModal(false)}
        onContinue={(onContinue, comment) => {
          if (onContinue) handleAction({ comment, type: TOLAK_PENGGUNA });
        }}
        title="Rejection Comment"
      />
      {/* <FailedConfirmation
        isOpen={successConfirmModal?.isOpen}
        handleClose={() => setFailedConfirmModal(false)}
        title="Success"
        
      /> */}
    </div>
  );
};

ApprovalConfig.propTypes = {};

ApprovalConfig.defaultProps = {};

export default ApprovalConfig;
