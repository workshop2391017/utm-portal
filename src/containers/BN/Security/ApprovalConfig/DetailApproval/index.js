// main
// libraries
import { Button, CircularProgress, makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import ApprovalHeader from "components/BN/ApprovalHeader";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import WarningAlert from "components/BN/Popups/WarningAlert";
import TextField from "components/BN/TextField/AntdTextField";
import illustrationX from "assets/images/BN/illustrationred.png";

// components
import Title from "components/BN/Title";
import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

// Redux
import {
  handleApprovalConfigDetail,
  setClearError,
  setHandleClearError,
  SETUJUI_PENGGUNA,
  submitApprovalConfig,
  TOLAK_PENGGUNA,
} from "stores/actions/approvalConfig";
import { checkOnline } from "stores/actions/errorGeneral";

// helpers
import { getLocalStorage } from "utils/helpers";

const DataParameter = (props) => {
  const { label, classes, approvalDetail } = props;
  return (
    <Grid item xs={6}>
      <Grid item xs={12}>
        <h1 className={classes.title}>{label}</h1>
      </Grid>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={6}>
          <div style={{ display: "flex", marginBottom: 6 }}>
            <div>Module</div> <br />
          </div>
          <div style={{ display: "flex", marginBottom: 7 }}>
            <div
              style={{
                marginTop: -5,
                fontWeight: "bold",
                overflowWrap: "break-word",
                inlineSize: "90%",
                fontFamily: "FuturaHvBT",
              }}
            >
              {approvalDetail?.module ?? "-"}
            </div>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div style={{ display: "flex", marginBottom: 6 }}>
            <div>Description</div> <br />
          </div>
          <div style={{ display: "flex", marginBottom: 7 }}>
            <div
              style={{
                marginTop: -5,
                fontWeight: "bold",
                overflowWrap: "break-word",
                inlineSize: "90%",
                fontFamily: "FuturaHvBT",
              }}
            >
              {approvalDetail?.description ?? "-"}
            </div>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div style={{ display: "flex", marginBottom: 6 }}>
            <div>Value</div> <br />
          </div>
          <div style={{ display: "flex", marginBottom: 7 }}>
            <div
              style={{
                marginTop: -5,
                fontWeight: "bold",
                overflowWrap: "break-word",
                inlineSize: "90%",
                fontFamily: "FuturaHvBT",
              }}
            >
              {approvalDetail?.value ?? "-"}
            </div>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div style={{ display: "flex", marginBottom: 6 }}>
            <div>Encryption</div> <br />
          </div>
          <div style={{ display: "flex", marginBottom: 7 }}>
            <div
              style={{
                marginTop: -5,
                fontWeight: "bold",
                overflowWrap: "break-word",
                inlineSize: "90%",
                fontFamily: "FuturaHvBT",
              }}
            >
              {approvalDetail?.isEncrypted ? "Yes" : "No"}
            </div>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div style={{ display: "flex", marginBottom: 6 }}>
            <div>Code</div> <br />
          </div>
          <div style={{ display: "flex", marginBottom: 7 }}>
            <div
              style={{
                marginTop: -5,
                fontWeight: "bold",
                overflowWrap: "break-word",
                inlineSize: "90%",
                fontFamily: "FuturaHvBT",
              }}
            >
              {approvalDetail?.name ?? "-"}
            </div>
          </div>
        </Grid>
      </Grid>
    </Grid>
  );
};

const DetailApproval = (props) => {
  const useStyles = makeStyles({
    detailApproval: {},
    container: {
      padding: 30,
    },
    paper: {
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      borderRadius: 10,
      minHeight: 270,
      padding: 20,
      position: "relative",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontSize: 17,
      fontWeight: 400,
      marginBottom: 15,
    },
  });

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const params = JSON.parse(getLocalStorage("detailParams"));
  const { approvalDetail, isLoadingSubmit, errorApprove, isLoading, error } =
    useSelector(({ approvalConfig }) => ({
      ...approvalConfig,
    }));

  const [comment, setComment] = useState(null);
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
  });
  const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [modalConf, setModalConf] = useState(false);
  const [errorAlert, setErrorAlert] = useState(false);

  const clickBatal = () => {
    history.push(pathnameCONFIG.APPROVAL.CONFIG_APPROVAL);
  };

  useEffect(async () => {
    await dispatch(checkOnline());

    if (!params.config_id)
      history.push(pathnameCONFIG.APPROVAL.CONFIG_APPROVAL);
    if (params.config_id) {
      handleApprovalConfigDetail([params?.config_id])(dispatch);
    }
  }, [localStorage, params?.config_id]);

  useEffect(() => {
    if (errorApprove?.isError) {
      setErrorAlert(true);
      setModalConf(false);
    }
  }, [errorApprove]);

  useEffect(() => {
    if (approvalDetail?.approvalComment) {
      setComment(approvalDetail?.approvalComment);
    } else setComment("");
  }, [approvalDetail]);

  const handleApprove = async (status) => {
    await dispatch(checkOnline(dispatch));

    const submited = await submitApprovalConfig({
      checkList: [approvalDetail?.id],
      comment,
      status,
      data: [approvalDetail],
      typeApproval: approvalDetail?.typeApproval,
    })(dispatch);

    if (submited) {
      if (status === SETUJUI_PENGGUNA) {
        setSuccessConfirmModal({
          isOpen: true,
          submessage: !submited?.data?.isToReleaserList[0]
            ? "Please wait for approval"
            : undefined,
          message: "Activity Successfully Approved",
        });
        setModalConf({ isOpen: false, message: "", action: "" });
      } else if (status === TOLAK_PENGGUNA) {
        setModalConf({ isOpen: false, message: "", action: "" });
        setSuccessConfirmModal({
          isOpen: true,
          message: "Activity Successfully Rejected",
          isReject: true,
        });
      }
    } else {
      setFailedConfirmModal(true);
    }
  };

  return (
    <div className={classes.detailApproval}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={clickBatal}
      />
      <Title label="Parameter Approval Details" />

      <div className={classes.container}>
        <ApprovalHeader
          label={`${approvalDetail?.activityName?.split(" ")?.[0]} Data`}
          status={approvalDetail?.approvedStatusSequence}
        />

        <div style={{ display: "flex" }}>
          <div style={{ width: "100%", paddingRight: 15 }}>
            <div className={classes.paper}>
              <div>
                {isLoading ? (
                  <div
                    style={{ width: "100%", minHeight: 496, display: "flex" }}
                  >
                    <div style={{ margin: "auto" }}>
                      <CircularProgress color="primary" size={40} />
                    </div>
                  </div>
                ) : (
                  <Grid
                    container
                    rowSpacing={1}
                    columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                  >
                    <DataParameter
                      label="Old Data"
                      classes={classes}
                      approvalDetail={
                        approvalDetail?.oldData?.systemParameterList
                      }
                    />
                    <DataParameter
                      label="New Data"
                      classes={classes}
                      approvalDetail={
                        approvalDetail?.newData?.systemParameterList
                      }
                    />
                  </Grid>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className={classes.paper}
          style={{ margin: "30px 0", minHeight: 190 }}
        >
          <p style={{ marginBottom: 5 }}>Comment :</p>
          <TextField
            disabled={approvalDetail?.status !== 0}
            type="textArea"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            placeholder="Comments"
            style={{ width: "100%", minHeight: 150 }}
          />
        </div>
      </div>
      <SuccessConfirmation
        isOpen={successConfirmModal?.isOpen}
        submessage={successConfirmModal?.submessage}
        message={successConfirmModal?.message}
        img={successConfirmModal?.isReject ? illustrationX : null}
        handleClose={() => {
          setSuccessConfirmModal({ isOpen: false });
          history.push(pathnameCONFIG.APPROVAL.CONFIG_APPROVAL);
        }}
        title="Success"
      />
      <DeletePopup
        isOpen={modalConf?.isOpen}
        handleClose={() =>
          setModalConf({ isOpen: false, message: "", action: "" })
        }
        loading={isLoadingSubmit}
        message={modalConf.message}
        onContinue={() => {
          if (modalConf?.action === SETUJUI_PENGGUNA) {
            handleApprove(SETUJUI_PENGGUNA);
          }
          if (modalConf?.action === TOLAK_PENGGUNA) {
            handleApprove(TOLAK_PENGGUNA);
          }
        }}
      />
      <FailedConfirmation
        isOpen={failedConfirmModal}
        handleClose={() => setFailedConfirmModal(false)}
        title="Success"
      />
      <WarningAlert
        isOpen={errorAlert}
        message="You Can't approve this data"
        handleClose={() => {
          dispatch(setClearError());
          setErrorAlert(false);
        }}
      />
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "white",
          padding: "22px 40px",
        }}
      >
        <ButtonOutlined
          label="Reject"
          width="86px"
          style={{ marginRight: 30 }}
          color="#0061A7"
          onClick={() =>
            setModalConf({
              isOpen: true,
              message: "Are You Sure To Reject The Activity?",
              action: TOLAK_PENGGUNA,
            })
          }
          disabled={
            isLoadingSubmit ||
            approvalDetail?.status === 1 ||
            approvalDetail?.status === 2 ||
            !params?.isAllowedTransaction
          }
        />
        <GeneralButton
          label={
            isLoadingSubmit ? (
              <CircularProgress size={20} color="primary" />
            ) : (
              "Approve"
            )
          }
          onClick={() =>
            setModalConf({
              isOpen: true,
              message: "Are You Sure To Approve the Activity?",
              action: SETUJUI_PENGGUNA,
            })
          }
          disabled={
            isLoadingSubmit ||
            approvalDetail?.status === 1 ||
            approvalDetail?.status === 2 ||
            !params?.isAllowedTransaction
          }
        />
      </div>
    </div>
  );
};

DetailApproval.propTypes = {};

DetailApproval.defaultProps = {};

export default DetailApproval;
