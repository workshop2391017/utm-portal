// main
// libraries
import { Button, CircularProgress, makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Toast from "components/BN/Toats";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { checkOnline } from "stores/actions/errorGeneral";

// redux
import {
  handleApprovalDetail,
  setHandleClearError,
  SETUJUI_PENGGUNA,
  submitApprovalUser,
  TOLAK_PENGGUNA,
} from "stores/actions/userApproval";

// helpers
import { getLocalStorage, handleConvertRole } from "utils/helpers";

// components
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import TextField from "components/BN/TextField/AntdTextField";
import Title from "components/BN/Title";
import illustrationX from "assets/images/BN/illustrationred.png";
import ApprovalHeader from "components/BN/ApprovalHeader";
import { pathnameCONFIG } from "configuration";

const DetailApproval = (props) => {
  const useStyles = makeStyles({
    container: {
      padding: 30,
    },
    paper: {
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      borderRadius: 10,
      minHeight: 270,
      padding: 20,
    },
    title: {
      fontFamily: "FuturaHvBT",
      fontSize: 17,
      fontWeight: "bold",
      marginBottom: 15,
    },
    valueData: {
      fontFamily: "FuturaHvBT",
      fontWeight: 900,
      fontSize: 13,
    },
    buttonGroup: {
      display: "flex",
      justifyContent: "space-between",
      width: "100%",
      backgroundColor: "white",
      padding: "22px 40px",
    },
  });
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [confirmation, setConfirmation] = useState({
    isOpen: false,
    message: "",
    submessage: "",
    title: "",
    action: null,
  });
  const par = getLocalStorage("detailParams");
  const params = par ? JSON.parse(par) : null;

  const { isLoadingSubmit, isLoading, usersId, approvalDetail, error } =
    useSelector(({ userApproval }) => userApproval);

  const approvalData = approvalDetail[0];

  const [comment, setComment] = useState(null);

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: "",
  });
  const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [commentModal, setCommentModal] = useState(false);

  const clickBatal = () => {
    history.push(pathnameCONFIG.APPROVAL.USER_APPROVAL);
    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  useEffect(async () => {
    await dispatch(checkOnline(dispatch));

    if (!params.id) history.goBack();
    if (params.id) handleApprovalDetail([params?.id])(dispatch);
  }, [localStorage, usersId, params?.id]);

  const handleApprove = async (status) => {
    await dispatch(checkOnline(dispatch));

    const requestPayload = {
      approvalComment: comment,
      approvalStatus: status,
      idApproval: approvalDetail[0]?.id,
      typeApproval: "PORTAL_USER",
    };

    const submited = await submitApprovalUser(requestPayload)(dispatch);

    if (submited) {
      if (status === SETUJUI_PENGGUNA) {
        setSuccessConfirmModal({
          isOpen: true,
          message: "Activity Successfully Approved",
          submessage: !submited?.data?.isToReleaserList[0]
            ? "Please wait for approval"
            : undefined,
          action: "approve",
        });
        setCommentModal(false);
      } else if (status === TOLAK_PENGGUNA) {
        setSuccessConfirmModal({
          isOpen: true,
          message: "Activity Successfully Rejected",
          action: "reject",
        });
        setCommentModal(false);
      }
    }
  };

  useEffect(() => {
    if (approvalDetail[0]?.approvalComment) {
      setComment(approvalDetail[0]?.approvalComment);
    } else setComment("");
  }, [approvalDetail]);

  return (
    <div>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={clickBatal}
      />

      <Title label="User Approval Details" />
      <div className={classes.container}>
        <ApprovalHeader
          label={`${approvalDetail?.[0]?.activityName?.split(" ")?.[0]} Data`}
          status={approvalDetail?.[0]?.approvedStatusSequence}
        />

        <div style={{ display: "flex" }}>
          <div style={{ width: "100%", paddingRight: 15 }}>
            <div className={classes.paper}>
              {isLoading ? (
                <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
                  <div style={{ margin: "auto" }}>
                    <CircularProgress color="primary" size={40} />
                  </div>
                </div>
              ) : (
                <Grid container spacing={6}>
                  <Grid item xs={3}>
                    <h1 className={classes.title}>Old Data</h1>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Username :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.username ?? "-"}
                      </div>
                    </div>
                    {/* // batas Username */}
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Name :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.fullName ?? "-"}
                      </div>
                    </div>
                    {/* page break here woy */}
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>NIP :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.nip ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Group :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.portalGroupId?.name ?? "-"}
                      </div>
                    </div>
                  </Grid>

                  <Grid item xs={3} style={{ marginTop: 40 }}>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Email :</div>
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.email ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Role :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {handleConvertRole(approvalData?.oldData?.idRole?.name)}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Office :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.idBranch?.name ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Ip Address :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.oldData?.ipAddress ?? "-"}
                      </div>
                    </div>
                  </Grid>
                  <Grid item xs={3}>
                    <h1 className={classes.title}>New Data</h1>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Username :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.username ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Name :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.fullName ?? "-"}
                      </div>
                    </div>
                    {/* page break here woy */}
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>NIP :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.nip ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Group :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.portalGroupId?.name ?? "-"}
                      </div>
                    </div>
                  </Grid>
                  <Grid item xs={3} style={{ marginTop: 40 }}>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Email :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.email ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Role :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {handleConvertRole(approvalData?.newData?.idRole?.name)}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Office :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.idBranch?.name ?? "-"}
                      </div>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ width: 96 }}>Ip Address :</div> <br />
                    </div>
                    <div style={{ display: "flex", marginBottom: 15 }}>
                      <div className={classes.valueData}>
                        {approvalData?.newData?.ipAddress ?? "-"}
                      </div>
                    </div>
                  </Grid>
                </Grid>
              )}
            </div>
          </div>
        </div>
        <div
          className={classes.paper}
          style={{ margin: "30px 0", minHeight: 190 }}
        >
          <p style={{ marginBottom: 5 }}>Comment :</p>
          <TextField
            disabled={approvalDetail[0]?.status !== 0}
            type="textArea"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            placeholder="Comments"
            style={{ width: "100%", minHeight: 150 }}
          />
        </div>
      </div>
      <SuccessConfirmation
        isOpen={successConfirmModal?.isOpen}
        message={successConfirmModal?.message}
        handleClose={() => {
          setSuccessConfirmModal({ isOpen: false });
          history.push(pathnameCONFIG.APPROVAL.USER_APPROVAL);
        }}
        title="Success"
        submessage={successConfirmModal?.submessage}
        img={successConfirmModal.action === "reject" ? illustrationX : null}
      />
      <DeletePopup
        isOpen={confirmation.isOpen}
        message={confirmation.message}
        handleClose={() =>
          setConfirmation({
            isOpen: false,
            message: "",
            submessage: "",
            title: "",
            action: "",
          })
        }
        onContinue={() => {
          if (confirmation.action === "approve") {
            handleApprove(SETUJUI_PENGGUNA);
          } else if (confirmation.action === "reject") {
            handleApprove(TOLAK_PENGGUNA);
          }
          setConfirmation({
            isOpen: false,
            message: "",
            submessage: "",
            title: "",
            action: "",
          });
        }}
        title="Confirmation"
      />
      <FailedConfirmation
        isOpen={failedConfirmModal}
        handleClose={() => setFailedConfirmModal(false)}
        title="Gagal"
      />
      <div className={classes.buttonGroup}>
        <ButtonOutlined
          label="Reject"
          width="86px"
          style={{ marginRight: 30 }}
          color="#0061A7"
          onClick={() =>
            setConfirmation({
              isOpen: true,
              message: "Are You Sure You Reject the Activity?",
              action: "reject",
            })
          }
          disabled={
            isLoadingSubmit ||
            approvalDetail[0]?.status !== 0 ||
            !params?.isAllowedTransaction
          }
        />
        <GeneralButton
          label={
            isLoadingSubmit ? (
              <CircularProgress size={20} color="primary" />
            ) : (
              "Approve"
            )
          }
          onClick={() => {
            setConfirmation({
              isOpen: true,
              message: "Are You Sure You Approve the Activity?",
              action: "approve",
            });
          }}
          disabled={
            isLoadingSubmit ||
            approvalDetail[0]?.status !== 0 ||
            !params?.isAllowedTransaction
          }
        />
      </div>
    </div>
  );
};

DetailApproval.propTypes = {};

DetailApproval.defaultProps = {};

export default DetailApproval;
