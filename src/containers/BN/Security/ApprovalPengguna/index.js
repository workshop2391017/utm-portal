// main
// libraries
import { Button, CircularProgress, makeStyles } from "@material-ui/core";
import Checkbox from "components/SC/Checkbox/CheckboxSingle";
import { pathnameCONFIG } from "configuration";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import illustrationX from "assets/images/BN/illustrationred.png";

// redux
import {
  handleUserApproval,
  setClearError,
  setHandleClearError,
  SETUJUI_PENGGUNA,
  setUsersId,
  submitMultipleApprovalUser,
  TOLAK_PENGGUNA,
} from "stores/actions/userApproval";

// components
import {
  getLocalStorage,
  handleConvertRole,
  searchDate,
  searchDateEnd,
  searchDateStart,
  setLocalStorage,
} from "utils/helpers";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import AddComment from "components/BN/Popups/AddComment";
import DeletePopup from "components/BN/Popups/Delete";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import WarningAlert from "components/BN/Popups/WarningAlert";
import Search from "components/BN/Search/SearchWithoutDropdown";
import TableICBB from "components/BN/TableIcBB";
import Title from "components/BN/Title";
import { checkOnline } from "stores/actions/errorGeneral";
import Toast from "components/BN/Toats";
import AntdTextField from "components/BN/TextField/AntdTextField";
import Badge from "components/BN/Badge";
import useDebounce from "utils/helpers/useDebounce";
import usePrevious from "utils/helpers/usePrevious";

const ApprovalPengguna = (props) => {
  const useStyles = makeStyles({
    approvalPengguna: {},
    container: {
      padding: "20px 30px 30px",
    },
    buttonGroup: {
      display: "flex",

      "& .MuiButton-outlinedPrimary.Mui-disabled": {
        border: "1px solid #BCC8E7",
        color: "#BCC8E7",
        backgroundColor: "#fff",
      },
      "& .MuiButton-containedPrimary.Mui-disabled": {
        backgroundColor: "#BCC8E7",
        color: "#fff",
      },
    },
    buttonTolak: {
      fontFamily: "FuturaMdBT",
      fontSize: 9,
      borderRadius: "6px !important",
      border: "1px solid #0061A7",
      color: "#0061A7",
      marginRight: "25px",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
    buttonSetujui: {
      fontFamily: "FuturaMdBT",
      fontSize: 9,
      borderRadius: "6px !important",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
      textTransform: "none",
    },
  });

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const {
    data: { listApproval: data },
    isLoading,
    totalElement,
    totalPage,
    isLoadingSubmit,
    errorApprove,
    error,
    isExecute,
  } = useSelector(({ userApproval }) => ({
    ...userApproval,
    totalElement: userApproval?.data?.totalElement ?? 0,
    totalPage: userApproval?.data?.totalPage ?? 0,
  }));

  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);

  const [page, setPage] = useState(1);

  const [checkList, setCheckList] = useState([]);
  const [disabledList, setDisabledList] = useState(0);

  const [successConfirmModal, setSuccessConfirmModal] = useState(false);
  const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [commentModal, setCommentModal] = useState({
    type: null,
    isOpen: false,
  });
  const [errorAlert, setErrorAlert] = useState(false);

  const handleCheckbox = (item) => {
    if (!checkList.includes(item)) {
      setCheckList([...checkList, item]);
    } else {
      const arr = checkList.filter((n) => n !== item);
      setCheckList(arr);
    }
  };

  const clickDetail = (rowData) => {
    dispatch(setUsersId([rowData.id]));
    const lsParams = {
      id: rowData.id,
      page,
      isAllowedTransaction: rowData?.isAllowedTransaction,
    };
    setLocalStorage("detailParams", JSON.stringify(lsParams));
    history.push(pathnameCONFIG.APPROVAL.DETAIL_APPROVAL_USER);
  };

  const handleClickAll = (e) => {
    if (e.target.checked) {
      const arr = data
        .filter(
          (item) => item?.status === 0 && item.isAllowedTransaction === true
        )
        .map((item) => item.id);
      const dataTemp = [...checkList, ...arr];

      setCheckList(dataTemp);
    } else {
      setCheckList([]);
    }
  };

  // const dataLength =
  //   (data ?? [])?.length - disabledList === 0
  //     ? checkList.length + 1
  //     : (data ?? [])?.length - disabledList;

  const isRowCheck = () => {
    let check = false;
    const dataFilter = data?.filter(
      ({ status, isAllowedTransaction }) =>
        status === 0 && isAllowedTransaction === true
    );

    const dataCheck = dataFilter.filter((elm) =>
      checkList.find((element) => elm.id === element)
    );

    const dataCount =
      dataCheck.length === 0 ? dataFilter.length + 1 : dataCheck.length;
    if (dataCount === dataFilter.length) check = true;
    return check;
  };

  const tableConfig = [
    {
      title: "Username",
      key: "username",
      render: (rowData) => rowData?.username ?? "-",
    },
    {
      title: "NIP",
      key: "nip",
      render: (rowData) => rowData?.nip ?? "-",
    },
    {
      title: "Role",
      key: "role",
      render: (rowData) => handleConvertRole(rowData?.role ?? "-"),
    },
    {
      title: "Date & Time",
      key: "date",
      render: (rowData) =>
        moment(rowData.approvalDate).format("DD-MM-YYYY HH:mm:ss"),
    },
    {
      title: "Activity",
      key: "activityName",
    },
    {
      title: "Status",
      key: "status",
      render: (rowData) => {
        if (rowData.status === 0) {
          return (
            <Badge
              label={rowData?.approvedStatusSequence}
              outline
              type="blue"
            />
          );
        }
        if (rowData.status === 1) {
          return (
            <Badge
              label={rowData?.approvedStatusSequence}
              outline
              type="green"
            />
          );
        }
        return (
          <Badge label={rowData?.approvedStatusSequence} outline type="red" />
        );
      },
    },
    {
      title: "",
      headerAlign: "right",
      align: "right",
      width: "80px",
      render: (rowData) => (
        <div style={{ display: "flex" }}>
          <GeneralButton
            style={{
              padding: 0,
              fontSize: 9,
              width: 76,
              height: 23,
            }}
            onClick={() => clickDetail(rowData)}
            label="View Details"
          />
        </div>
      ),
    },
  ];

  // get Paging
  useEffect(() => {
    const getParams = async () => {
      let params = await getLocalStorage("detailParams");
      if (params) params = JSON.parse(params);
      if (params?.page) setPage(params.page);
    };
    getParams();
  }, []);

  const searchDeb = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(searchDeb);

  const hanldeLoadData = async (search) => {
    await dispatch(checkOnline(dispatch));

    const statusFilter = dataFilter?.tabs?.tabValue3;
    const status =
      statusFilter === 0
        ? null
        : statusFilter === 1
        ? 0
        : statusFilter === 2
        ? 2
        : statusFilter === 3
        ? 1
        : null;

    const requestPayload = {
      activity: null,
      direction: "DESC",
      endDate: dataFilter?.date?.dateValue2
        ? searchDateStart(dataFilter?.date?.dateValue2)
        : null,
      limit: 10,
      searchValue: dataSearch || null,
      page: page - 1,
      startDate: dataFilter?.date?.dateValue1
        ? searchDateEnd(dataFilter?.date?.dateValue1)
        : null,
      isConfig: false,
      status,
      typeApproval: "PORTAL_USER",
    };

    if (page === 1) {
      dispatch(handleUserApproval(requestPayload));
    } else if (searchDeb !== prevSearch) {
      setPage(1);
    } else {
      dispatch(handleUserApproval(requestPayload));
    }
  };

  // get dataTable
  useEffect(() => {
    hanldeLoadData();
  }, [page, dataFilter, dataSearch]);

  const handleAction = async ({ comment, type }) => {
    await dispatch(checkOnline(dispatch));
    const submitted = await submitMultipleApprovalUser({
      checkList,
      data,
      status: type,
      comment,
    })(dispatch);

    if (submitted) {
      setCommentModal({ isOpen: false, type: null });
      if (type === SETUJUI_PENGGUNA) {
        setSuccessConfirmModal(true);
        setCommentModal(false);
      } else if (type === TOLAK_PENGGUNA) {
        setFailedConfirmModal(true);
        setCommentModal(false);
      }
      hanldeLoadData();
    }
  };

  // Get number of disabled list
  useEffect(() => {
    if ((data ?? []).length > 0) {
      const arr = data.filter(
        (item) => item.status !== 0 || !item?.isAllowedTransaction
      );
      setDisabledList(arr.length);
    }
  }, [data, page]);

  // Reset checklist data when change page
  // useEffect(() => {
  //   setCheckList([]);
  // }, [page]);

  return (
    <div className={classes.approvalPengguna}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />

      <Title label="User Approval">
        <Search
          options={[
            "ID Pengguna CST",
            "NIP",
            "Tgl & Wkt Submission",
            "Activity",
            "Komentar",
            "Status",
          ]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Username, Activity"
          placement="left"
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />
        <div style={{ marginTop: 20 }}>
          <TableICBB
            headerContent={tableConfig}
            dataContent={data ?? []}
            page={page}
            setPage={setPage}
            isLoading={isLoading}
            selectedValue={checkList}
            totalElement={totalElement}
            totalData={totalPage}
            extraComponents={
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  paddingRight: "40px",
                }}
              >
                <ButtonOutlined
                  disabled={isLoadingSubmit}
                  label={
                    isLoadingSubmit ? (
                      <CircularProgress size={20} color="primary" />
                    ) : checkList.length < 2 ? (
                      "Reject "
                    ) : (
                      "Reject All"
                    )
                  }
                  width="114px"
                  style={{ marginRight: 11 }}
                  onClick={() => setDeleteModal(true)}
                />
                <GeneralButton
                  loading={isLoadingSubmit}
                  label={
                    isLoadingSubmit ? (
                      <CircularProgress size={20} color="primary" />
                    ) : checkList.length < 2 ? (
                      "Approve "
                    ) : (
                      "Approve All"
                    )
                  }
                  width={134}
                  onClick={() => handleAction({ type: SETUJUI_PENGGUNA })}
                />
              </div>
            }
          />
        </div>
      </div>
      <SuccessConfirmation
        isOpen={successConfirmModal}
        handleClose={() => {
          setSuccessConfirmModal(false);
          setCheckList([]);
          hanldeLoadData();
        }}
        title="Success"
        message={`[${checkList.length}] Activity Successfully Approved`}
        submessage="Some activity may need approval"
      />
      <DeletePopup
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={() => {
          setCommentModal({ isOpen: true, type: TOLAK_PENGGUNA });
          setDeleteModal(false);
        }}
        title="Konfirmasi"
      />
      <AddComment
        isOpen={commentModal?.isOpen}
        handleClose={() => setCommentModal({ isOpen: false, type: null })}
        loading={isLoadingSubmit}
        onContinue={(onContinue, comment) => {
          if (onContinue) handleAction({ comment, type: TOLAK_PENGGUNA });
        }}
        title="Komentar Approval Pengguna"
      />

      <SuccessConfirmation
        img={illustrationX}
        isOpen={failedConfirmModal}
        handleClose={() => {
          setFailedConfirmModal(false);
          setCheckList([]);
          hanldeLoadData();
        }}
        title="Success"
        message={`[${checkList.length}] Activity Successfully Rejected`}
      />
      <WarningAlert
        isOpen={errorAlert}
        handleClose={() => {
          dispatch(setClearError());
          setErrorAlert(false);
        }}
      />
    </div>
  );
};

ApprovalPengguna.propTypes = {};

ApprovalPengguna.defaultProps = {};

export default ApprovalPengguna;
