// main
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import { CircularProgress, makeStyles } from "@material-ui/core";
import { pathnameCONFIG } from "configuration";

// components
import GeneralButton from "components/BN/Button/GeneralButton";
import Filter from "components/BN/Filter/GeneralFilter";
import Search from "components/BN/Search/SearchWithoutDropdown";
import TableICBB from "components/BN/TableIcBB";
import Title from "components/BN/Title";
import Badge from "components/BN/Badge";
import { useDispatch, useSelector } from "react-redux";
import Toast from "components/BN/Toats";
import {
  setClearError,
  handlePrivilegeApproval,
  setDataIdBulk,
  handleBulkApprove,
  setClear,
} from "stores/actions/privilegeApproval";
import moment from "moment";
import {
  handleConvertRole,
  searchDateEnd,
  searchDateStart,
  setLocalStorage,
  validateMinMax,
} from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import illustrationX from "assets/images/BN/illustrationred.png";

import PopUpComment from "./PopUpComment";

const useStyles = makeStyles({
  approvalConfig: {},
  container: {
    padding: "20px 30px 30px",
  },
  buttonGroup: {
    display: "flex",
    "& .MuiButton-outlinedPrimary.Mui-disabled": {
      border: "1px solid #BCC8E7",
      color: "#BCC8E7",
      backgroundColor: "#fff",
    },
    "& .MuiButton-containedPrimary.Mui-disabled": {
      backgroundColor: "#BCC8E7",
      color: "#fff",
    },
  },
  buttonTolak: {
    fontFamily: "Futura",
    borderRadius: "6px !important",
    border: "1px solid #0061A7",
    color: "#0061A7",
    marginRight: "25px",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
  },
  buttonSetujui: {
    fontFamily: "Futura",
    borderRadius: "6px !important",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    padding: 0,
    textTransform: "none",
  },
});

const tableConfig = ({ classes, clickDetail }) => [
  {
    title: "Username",
    key: "username",
    render: (rowData) => rowData?.username ?? "-",
  },
  {
    title: "NIP",
    key: "nip",
    render: (rowData) => rowData?.nip ?? "-",
  },
  {
    title: "Role",
    key: "role",
    render: (rowData) => handleConvertRole(rowData?.role ?? "-"),
  },
  {
    title: "Date & Time",
    key: "date",
    render: (rowData) =>
      moment(rowData.approvalDate).format("DD-MM-YYYY HH:mm:ss"),
  },
  {
    title: "Activity",
    key: "activityName",
  },

  // {
  //   title: "Comment",
  //   key: "approvalComment",
  // },
  {
    title: "Status",
    key: "status",
    render: (rowData) => {
      if (rowData.status === 0) {
        return (
          <Badge label={rowData?.approvedStatusSequence} outline type="blue" />
        );
      }
      if (rowData.status === 1) {
        return (
          <Badge label={rowData?.approvedStatusSequence} outline type="green" />
        );
      }
      return (
        <Badge label={rowData?.approvedStatusSequence} outline type="red" />
      );
    },
  },
  {
    title: "",
    headerAlign: "right",
    align: "right",
    width: "80px",
    render: (rowData) => (
      <div style={{ display: "flex" }}>
        <GeneralButton
          style={{
            padding: 0,
            fontSize: 9,
            width: 76,
            height: 23,
          }}
          onClick={() => clickDetail(rowData)}
          label="View Details"
        />
      </div>
    ),
  },
];

const PersetujuanHakAkses = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    error,
    data,
    isLoading,
    isSuccessApprove,
    isLoadingApprove,
    isSuccessReject,
  } = useSelector((e) => e.privilrgeApproval);
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: "",
  });

  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  const [conf, setConf] = useState({
    isOpen: false,
    message: "",
    action: null,
  });

  const [comment, setComment] = useState("");
  const [openComment, setOpenComment] = useState({
    isOpen: false,
    type: 0,
  });
  const [page, setPage] = useState(1);
  const [selectedIds, setSelectedIds] = useState([]);
  const [dataContent, setDataContent] = useState([]);
  const disabledCondition = false;

  useEffect(() => {
    if (isSuccessApprove?.isSuccess) {
      setSuccessConfirmModal({
        isOpen: true,
        message: `[${selectedIds.length}] Activity Successfully Approved`,
        submessage: "Some activity may need approval",
        action: "approve",
      });
    }

    if (isSuccessReject) {
      setSuccessConfirmModal({
        isOpen: true,
        message: `[${selectedIds.length}] Activity Successfully Rejected`,
        action: "reject",
      });
    }

    setConf({
      isOpen: false,
      message: "",
      action: null,
    });
  }, [isSuccessApprove, isSuccessReject]);

  useEffect(() => {
    if (data?.listApproval?.length > 0) {
      const dataFilter = data?.listApproval?.filter((el) =>
        selectedIds.find((element) => element === el.id)
      );

      const dataSelected = dataFilter?.map((item) => ({
        idApproval: item?.id,
      }));
      dispatch(setDataIdBulk(dataSelected));
    }
  }, [data, selectedIds]);

  useEffect(() => {
    if (data?.listApproval?.length > 0) {
      const dataMap = data?.listApproval.map((item) => ({
        ...item,

        disabled:
          item?.approvedStatusSequence === "APPROVED" ||
          item.isAllowedTransaction === false
            ? true
            : disabledCondition,
      }));
      setDataContent(dataMap);
    } else {
      setDataContent([]);
    }
  }, [data]);

  const searchDeb = useDebounce(dataSearch, 1300);
  const prevSearch = useDebounce(searchDeb);

  const onLoad = () => {
    const statusFilter = dataFilter?.tabs?.tabValue3;
    const statusOpt = [null, 0, 2, 1];

    const payload = {
      activity: null,
      direction: "DESC",
      endDate: dataFilter?.date?.dateValue2
        ? searchDateStart(dataFilter?.date?.dateValue2)
        : null,
      limit: 10,
      nip: null,
      page: page - 1,
      startDate: dataFilter?.date?.dateValue1
        ? searchDateEnd(dataFilter?.date?.dateValue1)
        : null,
      isConfig: false,
      status: statusOpt[statusFilter],
      searchValue: searchDeb,
      typeApproval: "PORTAL_GROUP",
    };
    dispatch(handlePrivilegeApproval(payload));
  };
  useEffect(() => {
    if (page === 1) {
      onLoad();
    } else if (searchDeb !== prevSearch) {
      setPage(1);
    } else {
      onLoad();
    }
  }, [dataFilter, searchDeb, page]);

  const clickDetail = (rowData) => {
    const lsParams = {
      id: rowData.id,
      page,
      isAllowedTransaction: rowData?.isAllowedTransaction,
    };
    setLocalStorage("dPApprovalPriivilege", JSON.stringify(lsParams));
    history.push(pathnameCONFIG.APPROVAL.HAK_AKSES_DETAIL);
  };

  return (
    <div className={classes.approvalConfig}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setClearError())}
      />
      <Title label="Privilage Approval">
        <Search
          placeholder="Username, Activity"
          placement="bottom"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={setDataFilter}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />
        <div style={{ marginTop: 20 }}>
          <TableICBB
            selecable={false}
            handleSelect={setSelectedIds}
            handleSelectAll={setSelectedIds}
            selectedValue={selectedIds}
            headerContent={tableConfig({ classes, clickDetail })}
            dataContent={dataContent ?? []}
            page={page}
            setPage={setPage}
            totalData={data?.totalPage}
            totalElement={data?.totalElement}
            isLoading={isLoading}
            extraComponents={
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  gap: 20,
                }}
              >
                <ButtonOutlined
                  disabled={isLoading}
                  onClick={() => {
                    setOpenComment({ isOpen: true, type: 2 });
                  }}
                  style={{ marginLeft: 220 }}
                  label={
                    isLoading ? (
                      <CircularProgress size={20} color="primary" />
                    ) : selectedIds.length < 2 ? (
                      "Reject "
                    ) : (
                      "Reject All"
                    )
                  }
                />
                <GeneralButton
                  disabled={isLoading}
                  onClick={() => {
                    setOpenComment({ isOpen: true, type: 1 });
                  }}
                  style={{ marginRight: 30 }}
                  label={
                    isLoading ? (
                      <CircularProgress size={20} color="primary" />
                    ) : selectedIds.length < 2 ? (
                      "Approve "
                    ) : (
                      "Approve All"
                    )
                  }
                  width="135px"
                />
              </div>
            }
          />
        </div>
      </div>
      <PopUpComment
        value={comment}
        onChange={(e) => setComment(e.target.value)}
        open={openComment?.isOpen}
        handleClose={() => {
          setOpenComment({ isOpen: false, type: "" });
          setComment("");
        }}
        disabled={!validateMinMax(comment, 1, 200)}
        onContinue={() => {
          if (openComment?.type === 1) {
            return setConf({
              isOpen: true,
              message: "Are You Sure To Approve The Activity?",
              action: 1,
            });
          }
          if (openComment?.type === 2) {
            setConf({
              isOpen: true,
              message: "Are You Sure To Reject The Activity?",
              action: 2,
            });
          }
        }}
      />
      <DeletePopup
        isOpen={conf?.isOpen}
        handleClose={() =>
          setConf({ isOpen: false, message: "", action: null })
        }
        onContinue={() => {
          if (conf.action === 1) {
            return dispatch(
              handleBulkApprove({
                action: conf.action,
                comment,
              })
            );
          }
          if (conf.action === 2) {
            dispatch(
              handleBulkApprove({
                action: conf.action,
                comment,
              })
            );
          }
        }}
        title="Confirmation"
        message={conf.message}
        submessage="You Can't undo this action"
        loading={isLoadingApprove}
      />
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        handleClose={() => {
          setOpenComment({ isOpen: false, type: "" });
          dispatch(setClear());
          setSelectedIds([]);
          onLoad();
          setSuccessConfirmModal({
            isOpen: false,
            message: "",
            submessage: "",
          });
          setConf({
            isOpen: false,
            message: "",
            action: null,
          });
          setComment("");
        }}
        title="Success"
        submessage={successConfirmModal?.submessage}
        img={successConfirmModal.action === "reject" && illustrationX}
        message={successConfirmModal.message}
      />
    </div>
  );
};

PersetujuanHakAkses.propTypes = {};

PersetujuanHakAkses.defaultProps = {};

export default PersetujuanHakAkses;
