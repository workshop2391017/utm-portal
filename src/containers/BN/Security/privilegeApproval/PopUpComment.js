import { Backdrop, Fade, Modal, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import AntdTextField from "components/BN/TextField/AntdTextField";
import React from "react";

const useStyles = makeStyles({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: "477px",
    borderRadius: "8px",
    height: "350px",
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px",
    "& .title": {
      fontSize: "20px",
      fontFamily: "FuturaHvBT",
      color: "#374062",
      fontWeight: 400,
    },
    "& .card": {
      marginTop: "40px",
      "& .label": {
        fontSize: "13px",
        color: "#374062",
        fontWeight: 400,
        fontFamily: "FuturaBkBT",
      },
    },
    "& .buttonGroup": {
      marginTop: "80px",
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
  },

  page: {},
});

const PopUpComment = ({
  open,
  handleClose,
  onChange,
  onContinue,
  value,
  disabled = false,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.page}>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Typography className="title">Change Comment</Typography>

            <div className="card">
              <Typography className="label">Add Comment</Typography>
              <AntdTextField
                value={value}
                type="textArea"
                onChange={onChange}
                placeholder="Comment"
                style={{
                  height: "88px",
                }}
              />
            </div>

            <div className="buttonGroup">
              <div>
                <ButtonOutlined
                  label="Cancel"
                  width="86px"
                  style={{ marginRight: 30 }}
                  color="#0061A7"
                  onClick={handleClose}
                />
              </div>
              <div>
                <GeneralButton
                  disabled={disabled}
                  onClick={onContinue}
                  label="Save"
                />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default PopUpComment;
