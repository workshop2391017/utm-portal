// libraries
import { Button, Grid, makeStyles } from "@material-ui/core";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import WarningAlert from "components/BN/Popups/WarningAlert";
import TextField from "components/BN/TextField/AntdTextField";
import Colors from "helpers/colors";

// components
import Title from "components/BN/Title";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import illustrationX from "assets/images/BN/illustrationred.png";

// Redux
import {
  setClearError,
  handlePrivilegeApprovalDetail,
  handleApprove,
  setClear,
} from "stores/actions/privilegeApproval";
import FormSectionTitle from "components/BN/Form/formSectionTitle";
import Toast from "components/BN/Toats";
import { getLocalStorage } from "utils/helpers";
import ApprovalHeader from "components/BN/ApprovalHeader";
import { pathnameCONFIG } from "configuration";

const useStyles = makeStyles({
  detailApproval: {},
  container: {
    padding: 30,
  },
  paper: {
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 20,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    marginBottom: 15,
  },
  content: {
    height: 269,
    width: "100%",
    border: "1px solid #B3C1E7",
    borderRadius: 10,
    "& .content-title": {
      fontSize: 15,
      fontFamily: "FuturaHvBT",
      color: "#374062",
      padding: "23px 20px",
    },
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "20px 40px",
    backgroundColor: "white",
  },
  papercomment: {
    border: "1px solid #B3C1E7",
    height: 190,
    marginTop: 20,
  },
  detailCard: {
    backgroundColor: "#fff",
    borderRadius: 10,
    height: 154,
    width: "100%",
    position: "relative",
    "& .title": {
      fontSize: 20,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.hard,
      marginBottom: 10,
    },
    "& .subtitle": {
      fontSize: 13,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.medium,
      marginBottom: 10,
    },
    "& .lihatsemua": {
      display: "flex",
      alignItems: "center",
      fontSize: 13,
      fontFamily: "FuturaMdBT",
      color: Colors.primary.hard,
      fontWeight: 700,
      "&:hover": {
        cursor: "pointer",
      },
    },
    "& .value": {
      marginBottom: 5,
    },
  },
  inputGorup: {
    marginTop: 20,
    "& .label": {
      fontFamily: "FuturaBkBT",
      color: Colors.dark.medium,
      fontWeight: 400,
    },
    "& .valuelabel": {
      fontFamily: "FuturaMdBT",
      fontSize: 14,
    },
  },
  bulletsGroup: {
    display: "flex",
    alignItems: "center",
    marginTop: 14,

    "& .bullets": {
      width: 7,
      height: 7,
      borderRadius: 50,
      marginRight: 10,
      backgroundColor: "#0061A7",
    },
    "& .bullet-value": {
      fontFamily: "FuturaMdBT",
      fontSize: 15,
    },
  },
});

const PersetujuanHakAksesDetail = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const {
    detailData,
    error,
    isSuccessApprove,
    isSuccessReject,
    isLoadingApprove,
  } = useSelector((e) => e.privilrgeApproval);
  const params = JSON.parse(getLocalStorage("dPApprovalPriivilege"));

  const newData = detailData?.[0]?.newData;
  const oldData = detailData?.[0]?.oldData;
  const oldOfficeType =
    oldData?.portalGroupDetailDto?.[0]?.portalGroup?.officeType ||
    oldData?.[0]?.portalGroup?.officeType;
  const oldGroup =
    oldData?.portalGroupDetailDto?.[0]?.portalGroup?.name ||
    oldData?.[0]?.portalGroup?.name;
  const oldMasterMenu =
    oldData?.portalGroupDetailDto?.[0]?.masterMenu ||
    oldData?.[0]?.masterMenu ||
    [];

  const newOfficeType = newData?.[0]?.portalGroup?.officeType;
  const newGroup = newData?.[0]?.portalGroup?.name;
  const newMasterMenu = newData?.[0]?.masterMenu ?? [];

  const [comment, setComment] = useState(null);
  const [errorAlert, setErrorAlert] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: "",
  });

  const [deleteModal, setDeleteModal] = useState({
    isOpen: false,
    message: "",
  });

  useEffect(() => {
    if (!params.id) history.push(pathnameCONFIG.APPROVAL.HAK_AKSES);
    if (params.id) {
      const payload = {
        approvalIds: [params?.id],
      };
      dispatch(handlePrivilegeApprovalDetail(payload));
    }
  }, [params?.id]);

  useEffect(() => {
    if (detailData[0]?.requestComment) {
      setComment(detailData[0]?.requestComment);
    }
  }, [detailData]);

  const submit = (status) => {
    dispatch(
      handleApprove({ data: detailData, comment, approvalStatus: status })
    );
  };

  useEffect(() => {
    if (isSuccessApprove?.isSuccess) {
      setSuccessConfirmModal({
        isOpen: true,
        message: "Activity Successfully Approved",
        submessage: !isSuccessApprove?.releaser[0]
          ? "Please wait for approval"
          : undefined,
        action: "approve",
      });
    }

    if (isSuccessReject) {
      setSuccessConfirmModal({
        isOpen: true,
        message: "Activity Successfully Rejected",
        action: "reject",
      });
    }

    setDeleteModal({ isOpen: false, message: "" });
  }, [isSuccessApprove, isSuccessReject]);

  useEffect(() => {
    if (detailData[0]?.approvalComment) {
      setComment(detailData[0]?.approvalComment);
    } else setComment("");
  }, [detailData]);

  return (
    <div className={classes.detailApproval}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setClearError())}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={() => history.push(pathnameCONFIG.APPROVAL.HAK_AKSES)}
      />
      <Title label="Privilege Approval Details" />

      <div className={classes.container}>
        <ApprovalHeader
          label={`${detailData?.[0]?.activityName?.split(" ")?.[0]} Data`}
          status={detailData?.[0]?.approvedStatusSequence}
        />

        <div className={classes.paper}>
          {oldMasterMenu?.length ? (
            <React.Fragment>
              <FormSectionTitle
                title="Old Data"
                color={Colors.dark.hard}
                borderColor={Colors.dark.hard}
              />

              <Grid container spacing={6}>
                <Grid item xs={6} className={classes.inputGorup}>
                  <div className="label">Office :</div>
                  <div className="value">
                    {oldOfficeType === "KP"
                      ? "Head Office"
                      : oldOfficeType === "KC"
                      ? "Branch Office"
                      : "-"}
                  </div>
                </Grid>
                <Grid item xs={6} className={classes.inputGorup}>
                  <div className="label">Group Name :</div>
                  <div className="value">{oldGroup}</div>
                </Grid>
              </Grid>
              <Grid container spacing={6}>
                {oldMasterMenu?.map((p, i) => (
                  <Grid
                    item
                    xs={12}
                    className={classes.inputGorup}
                    style={{ padding: "0 25px" }}
                  >
                    {i === 0 && <div className="label">Group Features :</div>}
                    <div className="valuelabel">{p?.name_en}</div>
                    <Grid
                      container
                      spacing={12}
                      style={{
                        marginLeft: 20,
                        ...(oldMasterMenu.length > 1 &&
                          oldMasterMenu.length < i - 2 && {
                            borderBottom: "1px solid #EAF2FF",
                          }),
                        paddingBottom: 24,
                        marginBottom: 20,
                      }}
                    >
                      {p?.menuAccessChild?.map((c, ic) => (
                        <Grid item xs={6}>
                          <div className={classes.bulletsGroup}>
                            <div className="bullets" />
                            <div className="bullet-value">{c?.name_en}</div>
                          </div>
                        </Grid>
                      ))}
                    </Grid>
                  </Grid>
                ))}
              </Grid>
            </React.Fragment>
          ) : null}

          {newMasterMenu?.length ? (
            <React.Fragment>
              <FormSectionTitle
                title="New Data"
                color={Colors.dark.hard}
                borderColor={Colors.dark.hard}
              />

              <Grid container spacing={6}>
                <Grid item xs={6} className={classes.inputGorup}>
                  <div className="label">Office :</div>
                  <div className="value">
                    {newOfficeType === "KP"
                      ? "Head Office"
                      : newOfficeType === "KC"
                      ? "Branch Office"
                      : "-"}
                  </div>
                </Grid>
                <Grid item xs={6} className={classes.inputGorup}>
                  <div className="label">Group Name :</div>
                  <div className="value">{newGroup}</div>
                </Grid>
              </Grid>
              <Grid container spacing={6}>
                {newMasterMenu?.map((p, i) => (
                  <Grid
                    item
                    xs={12}
                    className={classes.inputGorup}
                    style={{ padding: "0 25px" }}
                  >
                    {i === 0 && <div className="label">Group Features :</div>}
                    <div className="valuelabel">{p?.name_en}</div>
                    <Grid
                      container
                      spacing={12}
                      style={{
                        marginLeft: 20,
                        ...(oldMasterMenu.length > 1 &&
                          oldMasterMenu.length < i - 2 && {
                            borderBottom: "1px solid #EAF2FF",
                          }),
                        paddingBottom: 24,
                        marginBottom: 20,
                      }}
                    >
                      {p?.menuAccessChild?.map((c, ic) => (
                        <Grid item xs={6}>
                          <div className={classes.bulletsGroup}>
                            <div className="bullets" />
                            <div className="bullet-value">{c?.name_en}</div>
                          </div>
                        </Grid>
                      ))}
                    </Grid>
                  </Grid>
                ))}
              </Grid>
            </React.Fragment>
          ) : null}
        </div>
        <div className={`${classes.paper} ${classes.papercomment}`}>
          <p style={{ marginBottom: 5 }}>Comment :</p>
          <TextField
            disabled={detailData[0]?.status !== 0}
            type="textArea"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            placeholder="Comments"
            style={{ width: "100%", height: 128 }}
          />
        </div>
      </div>

      <div className={classes.buttonGroup}>
        <ButtonOutlined
          label="Reject"
          width="86px"
          style={{ marginRight: 30 }}
          color="#0061A7"
          onClick={() =>
            setDeleteModal({
              isOpen: true,
              message: "Are You Sure To Reject The Activity?",
              action: "reject",
            })
          }
          disabled={
            detailData[0]?.status !== 0 || !params?.isAllowedTransaction
          }
        />
        <GeneralButton
          label="Approve"
          onClick={() =>
            setDeleteModal({
              isOpen: true,
              message: "Are You Sure To Approve the Activity?",
              action: "approve",
            })
          }
          disabled={
            detailData[0]?.status !== 0 || !params?.isAllowedTransaction
          }
        />
      </div>
      <SuccessConfirmation
        isOpen={successConfirmModal.isOpen}
        handleClose={() => {
          dispatch(setClear());
          setSuccessConfirmModal({
            isOpen: false,
            message: "",
            submessage: "",
          });
          history.push(pathnameCONFIG.APPROVAL.HAK_AKSES);
        }}
        title="Success"
        submessage={successConfirmModal?.submessage}
        img={successConfirmModal.action === "reject" && illustrationX}
        message={successConfirmModal.message}
      />
      <DeletePopup
        isOpen={deleteModal.isOpen}
        message={deleteModal.message}
        submessage="You can't undo this action"
        handleClose={() => setDeleteModal({ isOpen: false, message: "" })}
        loading={isLoadingApprove}
        onContinue={() => {
          if (deleteModal.action === "approve") submit(1);
          if (deleteModal.action === "reject") submit(2);
        }}
        title="Confirmation"
      />

      <WarningAlert
        isOpen={errorAlert}
        message="You can't approve this data"
        handleClose={() => {
          dispatch(setClearError());
          setErrorAlert(false);
        }}
      />
    </div>
  );
};

PersetujuanHakAksesDetail.propTypes = {};

PersetujuanHakAksesDetail.defaultProps = {};

export default PersetujuanHakAksesDetail;
