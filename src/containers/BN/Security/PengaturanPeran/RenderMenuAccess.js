import React, { useEffect } from "react";
import Collapse from "components/BN/Collapse";
import { makeStyles } from "@material-ui/core";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";

const useStyles = makeStyles({
  accessTitle: {
    fontSize: 13,
    color: "#7B87AF",
    paddingBottom: 2,
    margin: "10px 0 16px",
    fontFamily: "FuturaMdBT",
  },
  collapse: {
    backgroundColor: "white !important",
    padding: "0 !important",
    height: "30px !important",
    borderRadius: 0,
  },
});

export const getIdsLIne = (data, ids) => {
  data.forEach((elm) => {
    if (elm.privilegeChild) {
      getIdsLIne(elm.privilegeChild, ids);
    }
    ids.push(elm.privilegeId);
  });
};

export const getNames = (data, names) => {
  data.forEach((elm) => {
    if (elm.privilegeChild) {
      getNames(elm.privilegeChild, names);
    }
    names.push(elm.privilegeMenu);
  });
};

const RenderMenuAccess = ({
  menuAccess: access,
  peran,
  checkList,
  handleCheckbox,
}) => {
  const classes = useStyles();

  const find = ({ privilegeChild = [], ...object }, privilegeMenu) => {
    let result;
    if (object.privilegeMenu === privilegeMenu) return object;

    if (privilegeChild)
      return (
        // eslint-disable-next-line no-return-assign
        privilegeChild.some((o) => (result = find(o, privilegeMenu))) && {
          ...object,
          privilegeChild: [result],
        }
      );

    return null;
  };

  const handleChange = (menu) => {
    const treeLineData = find(
      { privilegeChild: access ?? [] },
      menu?.privilegeMenu
    );

    const ids = [];
    const currentIdClick = menu?.privilegeId;
    getIdsLIne(treeLineData?.privilegeChild, ids);

    handleCheckbox(ids, currentIdClick);
  };

  const handleRenderMenuAccess = (menuAccess, childIdx = 0, name = []) =>
    (menuAccess ?? []).map((menu, i) => (
      <div
        key={i}
        style={{
          marginTop: 10,
          marginLeft: childIdx * 10,
          paddingBottom: i === (access ?? []).length - 1 ? 10 : 0,
        }}
      >
        <div>
          <Collapse
            className={classes.collapse}
            style={{
              borderBottom: !childIdx ? "1px solid #7B87AF" : "none",
            }}
            defaultOpen
            label={
              menu.privilegeUiType === "MENU" ? (
                <div className={classes.accessTitle}>{menu.privilegeMenu}</div>
              ) : (
                menu.privilegeMenu
              )
            }
          >
            {menu.privilegeChild &&
            menu.privilegeChild.find(
              ({ privilegeUiType }) => privilegeUiType === "MENU"
            ) ? (
              handleRenderMenuAccess(menu.privilegeChild, childIdx + 1, name)
            ) : (
              <div>
                {menu?.privilegeChild ? (
                  (menu?.privilegeChild ?? []).map((actMenu, actIdx) => (
                    <div key={actIdx}>
                      <CheckboxSingle
                        disabled={!peran}
                        checked={checkList?.includes(actMenu?.privilegeId)}
                        onChange={() => handleChange(actMenu)}
                        label={actMenu?.privilegeMenu?.split(" ")[0]}
                      />
                    </div>
                  ))
                ) : (
                  <CheckboxSingle
                    disabled={!peran}
                    checked={checkList?.includes(menu?.privilegeId)}
                    onChange={() => handleChange(menu)}
                    label={menu?.privilegeMenu?.split(" ")[0]}
                  />
                )}
              </div>
            )}
          </Collapse>
        </div>
      </div>
    ));

  return <div>{handleRenderMenuAccess(access)}</div>;
};

export default RenderMenuAccess;
