// main
import { CircularProgress, makeStyles } from "@material-ui/core";
// assets
import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import AddComment from "components/BN/Popups/AddComment";
// components
import DeletePopup from "components/BN/Popups/Delete";
// components Modal
import Modal from "components/BN/Popups/EditRole";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";
import Toast from "components/BN/Toats";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import React, { useEffect, useState } from "react";
// libraries
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { checkOnline } from "stores/actions/errorGeneral";
// Redux
import {
  handleGetAllPrivilege,
  handleGetAllRole,
  handleRequestDeleteRole,
  handleRequestSaveRole,
  setClearDuplicate,
  setHandleClearError,
} from "stores/actions/managementRole";

import useDebounce from "utils/helpers/useDebounce";
import ListPeran from "./ListPeran";
import RenderMenuAccess, { getIdsLIne } from "./RenderMenuAccess";

const useStyles = makeStyles({
  pengaturanPeran: {},
  container: {
    padding: "20px 30px",
  },
  paper: {
    backgroundColor: "#fff",
    width: "100%",
    height: "auto",
    borderRadius: 8,
    border: "1px solid #D3DCF4",
    maxHeight: 584,
  },
  paperPeran: {
    backgroundColor: "#fff",
    width: "100%",
    height: "auto",
    borderRadius: 8,
    border: "1px solid #D3DCF4",
    maxHeight: 583,
  },
  title: {
    fontWeight: 900,
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    margin: "22px 20px 20px",
  },
  peranContainer: {
    height: 40,
    padding: "3px 20px",
    position: "relative",
    cursor: "pointer",
    transition: "0.2s",
    "&:hover": {
      backgroundColor: "#F4F7FB",
    },
  },
  peranContainerActive: {
    height: 40,
    padding: "3px 20px",
    position: "relative",
    cursor: "pointer",
    backgroundColor: "#0061A7",
    "& p": {
      color: "#fff",
    },
  },
  peranRole: {
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    fontWeight: 400,
    margin: 0,
  },
  peranCount: {
    fontSize: 9,
    color: "#7B87AF",
    margin: 0,
    fontFamily: "FuturaMdBT",
  },
  edit: {
    position: "absolute",
    right: 0,
    top: "50%",
    transform: "translateY(-50%)",
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 0,
    paddingRight: 0,
  },
  access: {
    border: "1px solid #BCC8E7",
    height: 411,
    margin: "0 20px",
    padding: "0 10px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      width: "26px",
      // borderRadius: "50px",
      height: "100px",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#fff",
      width: "10px",
      // borderRadius: "50px",

      borderRight: "10px solid #fff",
      borderLeft: "10px solid #fff",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#0061A7",
      // borderRadius: "50px",
      height: "30px",
      width: "10px",
      borderRadius: "10px",
      borderRight: "11px solid #fff",
      borderLeft: "11px solid #fff",
    },
    "&::-webkit-scrollbar-track-piece:start": {
      background: "#F4F7FB",
      borderRight: "9px solid #fff",
      borderLeft: "10px solid #fff",
      marginTop: "51px",
    },
    "&::-webkit-scrollbar-track-piece:end": {
      background: "#F4F7FB",
      borderBottom: "261px",
      borderRight: "9px solid #fff",
      borderLeft: "10px solid #fff",
    },
  },
  accessTitle: {
    fontSize: 13,
    color: "#7B87AF",
    paddingBottom: 2,
    margin: "10px 0 16px",
    fontFamily: "FuturaMdBT",
  },
  check: {
    fontSize: 15,
    color: "#374062",
    "& .ant-checkbox": {
      width: 20,
      height: 20,
      borderRadius: 3,
      "& .ant-checkbox-inner": {
        width: 20,
        height: 20,
        borderRadius: 3,
        border: "1px solid #BCC8E7",
        "&::after": {
          width: 6.5,
          height: 11.17,
          top: "42%",
        },
      },
    },
    "& .ant-checkbox-checked": {
      "& .ant-checkbox-inner": {
        border: "1px solid #0061A7 !important",
      },
    },
    "& .ant-checkbox-indeterminate": {
      "& .ant-checkbox-inner::after": {
        width: "10px !important",
        height: "10px !important",
        top: "50% !important",
      },
    },
  },

  collapse: {
    backgroundColor: "white !important",
    padding: "0 !important",
    height: "30px !important",
    borderRadius: 0,
  },
});
const PengaturanPeran = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    privilege,
    isLoadingPrivilege,
    isLoadingDetail,
    detailRole,
    isLoadingSubmit,
    error,
  } = useSelector(
    ({ managementRole }) => ({
      ...managementRole,
    }),
    shallowEqual
  );
  const [dataSearch, setDataSearch] = useState(null);
  const [peran, setPeran] = useState(null);
  const [modal, setModal] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState({
    isOpen: false,
    message: null,
  });
  const [checkListTemp, setCheckListTemp] = useState([]);
  const [privilegeLength, setPrivilegeLength] = useState(0);
  const [checkListIds, setCheckListIds] = useState([]);
  const [openComment, setOpenComment] = useState(false);
  const [deleteConfirmation, setDeleteConfirmation] = useState(false);
  const [deleteData, setDeleteData] = useState(null);

  const handleCheckbox = (ids, currentIdClick) => {
    if (!checkListIds.find((a) => a === currentIdClick)) {
      setCheckListIds([...new Set([...checkListIds, ...ids])]);
    } else {
      const idFilter = checkListIds.filter((x) => x !== currentIdClick);
      setCheckListIds(idFilter);
    }
  };

  const handleClickAll = (e) => {
    const ids = [];
    getIdsLIne(privilege?.privilegeList ?? [], ids);

    if (e.target.checked) {
      setCheckListIds([...new Set(ids)]);
    } else setCheckListIds([]);
  };

  useEffect(() => {
    const ids = [];
    getIdsLIne(privilege?.privilegeList ?? [], ids);
    setPrivilegeLength(ids.length);
  }, [privilege?.privilegeList]);

  const compareChecklist = (arr1, arr2) =>
    arr1.sort().join(",") === arr2.sort().join(",");

  // Get role details and menus
  useEffect(() => {
    if (Object.keys(detailRole).length > 0) {
      const ids = [];

      getIdsLIne(detailRole?.privilegeList, ids);

      setCheckListIds(ids);
      setCheckListTemp(ids);
    }
  }, [detailRole]);

  useEffect(async () => {
    setCheckListIds([]);

    await dispatch(checkOnline(dispatch));
    if (!dataSearch) {
      handleGetAllRole({
        roleName: null,
      })(dispatch);
    }
  }, []);

  const dataSearchDebounce = useDebounce(dataSearch, 1300);

  useEffect(() => {
    handleGetAllRole({
      roleName: dataSearchDebounce,
    })(dispatch);
  }, [dataSearchDebounce]);

  useEffect(async () => {
    await dispatch(checkOnline(dispatch));
    handleGetAllPrivilege()(dispatch);
  }, []);

  const handleUpdateRole = async (comment) => {
    await dispatch(checkOnline(dispatch));
    const submited = await handleRequestSaveRole(
      { ...detailRole, requestComment: comment },
      checkListIds
    )(dispatch);

    if (submited) {
      setConfirmSuccess({ isOpen: true, message: "Perubahan Disimpan" });
      setOpenComment(false);
    }
  };

  const handleAddNewRole = async (onContinue, newData) => {
    await dispatch(checkOnline(dispatch));
    const { inputRole, checkListIds } = newData;

    if (onContinue) {
      const requestPayload = {
        privilegeId: checkListIds,
        roleName: inputRole,
      };
      const submit = await handleRequestSaveRole(
        { ...requestPayload, requestComment: null },
        checkListIds
      )(dispatch);

      if (submit) {
        setConfirmSuccess({
          isOpen: true,
          message: "Penambahan Data Disimpan",
        });

        // close modal
        setOpenComment(false);
        setModal(false);

        // clear state
        setCheckListIds([]);
        setCheckListTemp([]);
        setPeran(null);
      }
    }
  };

  const handleDeleteRole = async () => {
    await dispatch(checkOnline(dispatch));
    const submit = await handleRequestDeleteRole(deleteData)(dispatch);
    if (submit) {
      setDeleteConfirmation(false);
      setDeleteData(null);
      setConfirmSuccess({
        isOpen: true,
        message: "Berhasil Menghapus Data",
      });
    }
  };

  return (
    <div className={classes.pengaturanPeran}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Title label="Pengaturan Peran">
        <Search
          options={["Peran", "Akses"]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
          placeholder="Cari Peran"
          style={{ width: 256 }}
        />

        <GeneralButton
          label="Tambah Peran"
          iconPosition="startIcon"
          buttonIcon={<Plus />}
          width="175px"
          height="40px"
          style={{ marginLeft: 30 }}
          onClick={() => setModal(true)}
        />
      </Title>

      <div className={classes.container}>
        <div
          style={{
            display: "flex",
            minHeight: 200,
          }}
        >
          <div style={{ width: 270, marginRight: 20 }}>
            <ListPeran
              setPeran={setPeran}
              setModal={setModal}
              setDeleteConfirmation={setDeleteConfirmation}
              setDeleteData={setDeleteData}
              peran={peran}
            />
          </div>
          <div style={{ flex: "auto" }}>
            <div className={classes.paper}>
              <h1 className={classes.title}>Menu Akses</h1>

              {isLoadingPrivilege || isLoadingDetail ? (
                <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
                  <div style={{ margin: "auto" }}>
                    <CircularProgress color="primary" size={40} />
                  </div>
                </div>
              ) : (
                <div className={classes.access}>
                  <div style={{ marginTop: 10 }}>
                    <CheckboxSingle
                      disabled={peran === null}
                      indeterminate={
                        checkListIds.length > 0 &&
                        checkListIds.length < privilegeLength
                      }
                      checked={
                        privilegeLength > 0 &&
                        checkListIds.length === privilegeLength
                      }
                      onChange={handleClickAll}
                      label="Izinkan Semua Akses"
                    />
                  </div>
                  <RenderMenuAccess
                    menuAccess={privilege?.privilegeList ?? []}
                    peran={peran}
                    checkList={checkListIds}
                    handleCheckbox={handleCheckbox}
                  />
                </div>
              )}
              <div style={{ padding: "39px 20px 20px" }}>
                <React.Fragment>
                  <ButtonOutlined
                    label="Batal"
                    width="75px"
                    height="40px"
                    disabled={
                      peran === null
                        ? true
                        : !!compareChecklist(checkListIds, checkListTemp)
                    }
                  />
                  <GeneralButton
                    label="Simpan"
                    width="90px"
                    height="40px"
                    style={{ float: "right" }}
                    onClick={() => setOpenComment(true)}
                    disabled={
                      peran === null
                        ? true
                        : !!compareChecklist(checkListIds, checkListTemp)
                    }
                  />
                </React.Fragment>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        label="Tambah Peran"
        isOpen={modal}
        handleClose={() => {
          setModal(false);
          dispatch(setClearDuplicate());
        }}
        onContinue={handleAddNewRole}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess?.isOpen}
        message={confirmSuccess?.message}
        handleClose={() => setConfirmSuccess({ isOpen: false, message: false })}
      />
      <AddComment
        isOpen={openComment}
        handleClose={() => setOpenComment(false)}
        onContinue={(onContinue, comment) => {
          if (onContinue) {
            handleUpdateRole(comment);
          }
        }}
        title="Rejection Comment"
        loading={isLoadingSubmit}
      />
      <DeletePopup
        isOpen={deleteConfirmation}
        handleClose={() => setDeleteConfirmation(false)}
        onContinue={handleDeleteRole}
        title="Confirmation"
        message="Are you Sure to Delete Data?"
        loading={isLoadingSubmit}
      />
    </div>
  );
};

PengaturanPeran.propTypes = {};

PengaturanPeran.defaultProps = {};

export default PengaturanPeran;
