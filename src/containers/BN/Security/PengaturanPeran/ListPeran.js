import React from "react";
import { CircularProgress, Paper } from "@material-ui/core";
import ScrollCustom from "components/BN/ScrollCustom";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { checkOnline } from "stores/actions/errorGeneral";
import {
  handleDuplicate,
  handleGetRoleDetail,
} from "stores/actions/managementRole";
import Menu from "components/BN/Menus/MenuPengaturanPeran";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  pengaturanPeran: {},
  container: {
    padding: "20px 30px",
  },
  paper: {
    backgroundColor: "#fff",
    width: "100%",
    height: "auto",
    borderRadius: 8,
    border: "1px solid #D3DCF4",
    maxHeight: 584,
  },
  paperPeran: {
    backgroundColor: "#fff",
    width: "100%",
    height: "auto",
    borderRadius: 8,
    border: "1px solid #D3DCF4",
    maxHeight: 583,
  },
  title: {
    fontWeight: 900,
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    margin: "22px 20px 20px",
  },
  peranContainer: {
    height: 40,
    padding: "3px 20px",
    position: "relative",
    cursor: "pointer",
    transition: "0.2s",
    "&:hover": {
      backgroundColor: "#F4F7FB",
    },
  },
  peranContainerActive: {
    height: 40,
    padding: "3px 20px",
    position: "relative",
    cursor: "pointer",
    backgroundColor: "#0061A7",
    "& p": {
      color: "#fff",
    },
  },
  peranRole: {
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    fontWeight: 400,
    margin: 0,
  },
  peranCount: {
    fontSize: 9,
    color: "#7B87AF",
    margin: 0,
    fontFamily: "FuturaMdBT",
  },
  edit: {
    position: "absolute",
    right: 0,
    top: "50%",
    transform: "translateY(-50%)",
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 0,
    paddingRight: 0,
  },
  access: {
    border: "1px solid #BCC8E7",
    height: 411,
    margin: "0 20px",
    padding: "0 10px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      width: "26px",
      // borderRadius: "50px",
      height: "100px",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#fff",
      width: "10px",
      // borderRadius: "50px",

      borderRight: "10px solid #fff",
      borderLeft: "10px solid #fff",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#0061A7",
      // borderRadius: "50px",
      height: "30px",
      width: "10px",
      borderRadius: "10px",
      borderRight: "11px solid #fff",
      borderLeft: "11px solid #fff",
    },
    "&::-webkit-scrollbar-track-piece:start": {
      background: "#F4F7FB",
      borderRight: "9px solid #fff",
      borderLeft: "10px solid #fff",
      marginTop: "51px",
    },
    "&::-webkit-scrollbar-track-piece:end": {
      background: "#F4F7FB",
      borderBottom: "261px",
      borderRight: "9px solid #fff",
      borderLeft: "10px solid #fff",
    },
  },
  accessTitle: {
    fontSize: 13,
    color: "#7B87AF",
    paddingBottom: 2,
    margin: "10px 0 16px",
    fontFamily: "FuturaMdBT",
  },
  check: {
    fontSize: 15,
    color: "#374062",
    "& .ant-checkbox": {
      width: 20,
      height: 20,
      borderRadius: 3,
      "& .ant-checkbox-inner": {
        width: 20,
        height: 20,
        borderRadius: 3,
        border: "1px solid #BCC8E7",
        "&::after": {
          width: 6.5,
          height: 11.17,
          top: "42%",
        },
      },
    },
    "& .ant-checkbox-checked": {
      "& .ant-checkbox-inner": {
        border: "1px solid #0061A7 !important",
      },
    },
    "& .ant-checkbox-indeterminate": {
      "& .ant-checkbox-inner::after": {
        width: "10px !important",
        height: "10px !important",
        top: "50% !important",
      },
    },
  },

  collapse: {
    backgroundColor: "white !important",
    padding: "0 !important",
    height: "30px !important",
    borderRadius: 0,
  },
});
const ListPeran = ({
  setPeran,
  setModal,
  setDeleteConfirmation,
  setDeleteData,
  peran,
}) => {
  const { data, isLoading } = useSelector(
    ({ managementRole }) => ({
      ...managementRole,
    }),
    shallowEqual
  );

  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <div className={classes.paperPeran}>
      <h1 className={classes.title}>Peran</h1>
      <ScrollCustom height={517}>
        {isLoading ? (
          <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
            <div style={{ margin: "auto" }}>
              <CircularProgress color="primary" size={40} />
            </div>
          </div>
        ) : (
          <div style={{ paddingBottom: 50 }}>
            {(data ?? []).map((item, i) => (
              <div
                className={
                  peran === item.roleName
                    ? classes.peranContainerActive
                    : classes.peranContainer
                }
                style={{ marginTop: i === 0 ? 0 : 10 }}
              >
                <Paper
                  style={{
                    boxShadow: "none",
                    backgroundColor: "transparent",
                  }}
                  onClick={async () => {
                    setPeran(item?.roleName);
                    await dispatch(checkOnline(dispatch));
                    handleGetRoleDetail({ roleId: item.roleId })(dispatch);
                  }}
                >
                  <p className={classes.peranRole}>{item.roleName}</p>
                  <p className={classes.peranCount}>{item.userTotal} Anggota</p>
                </Paper>

                <div className={classes.edit}>
                  <Menu
                    handleDeleteRole={() => {
                      setDeleteConfirmation(true);
                      setDeleteData(item);
                    }}
                    handleDuplicate={async () => {
                      await dispatch(checkOnline(dispatch));
                      setModal(true);
                      handleDuplicate(item, { roleId: item.roleId })(dispatch);
                    }}
                    handleEdit={() => {
                      setPeran(item?.roleName);
                      handleGetRoleDetail({ roleId: item.roleId })(dispatch);
                    }}
                    color={peran === item.roleName ? "white" : "blue"}
                  />
                </div>
              </div>
            ))}
          </div>
        )}
      </ScrollCustom>
    </div>
  );
};

export default ListPeran;
