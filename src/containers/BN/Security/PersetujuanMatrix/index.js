// main
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Button, CircularProgress } from "@material-ui/core";
import Checkbox from "components/SC/Checkbox/CheckboxSingle";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

// helpers
import {
  handleConvertRole,
  searchDate,
  searchDateEnd,
  searchDateStart,
  setLocalStorage,
} from "utils/helpers";

// redux
import { setHandleClearError } from "stores/actions/approvalConfig";
import { pathnameCONFIG } from "configuration";

// components
import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Filter from "components/BN/Filter/GeneralFilter";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeletePopup from "components/BN/Popups/Delete";
import AddComment from "components/BN/Popups/AddComment";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import TableICBB from "components/BN/TableIcBB";
import Toast from "components/BN/Toats";
import Badge from "components/BN/Badge";
import {
  handleGetPersetujuanApprovalMatrix,
  handleApprovePersetujuanApprovalMatrixBulkExecute,
  setTypeBulkReffNumber,
} from "stores/actions/approvalPersetujuanMatrix";
import useDebounce from "utils/helpers/useDebounce";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import illustrationX from "assets/images/BN/illustrationred.png";
import { setClearDetail } from "stores/actions/managementRole";
import PopUpComment from "./PopUpComment";

const useStyles = makeStyles({
  approvalConfig: {},
  container: {
    padding: "20px 30px 30px",
  },
  buttonGroup: {
    display: "flex",
    "& .MuiButton-outlinedPrimary.Mui-disabled": {
      border: "1px solid #BCC8E7",
      color: "#BCC8E7",
      backgroundColor: "#fff",
    },
    "& .MuiButton-containedPrimary.Mui-disabled": {
      backgroundColor: "#BCC8E7",
      color: "#fff",
    },
  },
  buttonTolak: {
    fontFamily: "Futura",
    borderRadius: "6px !important",
    border: "1px solid #0061A7",
    color: "#0061A7",
    marginRight: "25px",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
  },
  buttonSetujui: {
    width: 76,
    padding: "6px 10px",
    fontFamily: "Futura",
    borderRadius: "6px !important",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    textTransform: "none",
    fontSize: 9,
  },
});

const PersetujuanMatrix = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [page, setPage] = useState(1);
  const [comment, setComment] = useState("");
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: "",
    subMessage: "",
  });
  const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [commentModal, setCommentModal] = useState(false);
  const [conf, setConf] = useState({
    isOpen: false,
    message: "",
    action: null,
  });

  const {
    data,
    error,
    isLoading,
    isSuccessApprove,
    isSuccessReject,
    isLoadingExecute,
  } = useSelector((e) => e?.approvalPersetujuanMatrix);

  const clickDetail = (rowData) => {
    const lsParams = {
      reffNo: rowData.reffNo,
      page,
    };
    setLocalStorage("persetujuanMatrixApproval", JSON.stringify(lsParams));
    history.push(pathnameCONFIG.APPROVAL.MATRIX_DETAIL);
  };

  const tableConfig = [
    {
      title: "Username",
      key: "username",
      render: (rowData) => rowData?.createdByName ?? "-",
    },
    {
      title: "NIP",
      key: "nip",
      render: (rowData) => rowData?.createdByNip ?? "-",
    },
    {
      title: "Role",
      key: "role",
      render: (rowData) => handleConvertRole(rowData?.roleMaker ?? "-"),
    },
    {
      title: "Date & Time",
      key: "date",
      render: (rowData) =>
        moment(rowData.approvalDate).format("DD-MM-YYYY HH:mm:ss"),
    },
    {
      title: "Activity",
      key: "activityName",
    },
    {
      title: "Status",
      key: "status",
      render: (rowData) => {
        const status = rowData.status?.toLowerCase();
        if (status?.includes("waiting")) {
          return <Badge label={rowData?.status} outline type="blue" />;
        }
        if (status?.includes("approved")) {
          return <Badge label={rowData?.status} outline type="green" />;
        }
        return <Badge label={rowData?.status} outline type="red" />;
      },
    },
    {
      title: "",
      headerAlign: "right",
      align: "right",
      width: "80px",
      render: (rowData) => (
        <div style={{ display: "flex" }}>
          <GeneralButton
            style={{
              padding: 0,
              fontSize: 9,
              width: 76,
              height: 23,
            }}
            onClick={() => clickDetail(rowData)}
            label="View Details"
          />
        </div>
      ),
    },
  ];

  const searchDeb = useDebounce(dataSearch, 1200);
  const prevSearch = useDebounce(searchDeb, 1300);

  const handleApprove = (action) => {
    const payload = {
      action,
      workFlowMenu: "APPROVAL_MATRIX_PORTAL",
      modifiedReason: comment,
    };
    dispatch(handleApprovePersetujuanApprovalMatrixBulkExecute(payload));
  };

  const onLoad = () => {
    const status = dataFilter?.tabs?.tabValue3 || 0;
    const statusOpt = [null, "WAITING", "REJECTED", "APPROVED"];

    const payload = {
      fromDate: dataFilter?.date?.dateValue1
        ? searchDateStart(dataFilter?.date?.dateValue1)
        : null,
      menuName: "APPROVAL_MATRIX_PORTAL",
      page: page - 1,
      size: 10,
      toDate: dataFilter?.date?.dateValue2
        ? searchDateEnd(dataFilter?.date?.dateValue2)
        : null,
      workFlowStatus: statusOpt[status],
      searchValue: searchDeb || null,
      id: null,
      isMatrixApproval: true,
    };
    dispatch(handleGetPersetujuanApprovalMatrix(payload));
  };

  useEffect(() => {
    if (page === 1) {
      onLoad();
    } else if (searchDeb !== prevSearch) {
      setPage(1);
    } else {
      onLoad();
    }
  }, [searchDeb, dataFilter, page]);

  useEffect(() => {
    if (data?.workflowApprovalDtoList?.length > 0 && selectedIds?.length > 0) {
      const dataFilter = data?.workflowApprovalDtoList?.filter((el) =>
        selectedIds?.find((element) => element === el.id)
      );

      const dataSelected = dataFilter?.map((item) => ({
        referenceNumber: item?.reffNo,
      }));

      dispatch(setTypeBulkReffNumber(dataSelected));
    }
  }, [selectedIds, data]);

  useEffect(() => {
    if (isSuccessApprove?.isSuccess) {
      setSuccessConfirmModal({
        isOpen: true,
        message: `[${selectedIds.length}] Activity Successfully Approved`,
        subMessage: isSuccessApprove?.releaser
          ? "Please wait for approval"
          : undefined,
      });
    }

    if (isSuccessReject) {
      setSuccessConfirmModal({
        isOpen: true,
        message: `[${selectedIds.length}] Activity Successfully Rejected`,
      });
    }
    setConf({ isOpen: false, message: "" });
  }, [isSuccessApprove, isSuccessReject]);

  return (
    <div className={classes.approvalConfig}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />

      <Title label="Matrix Approval">
        <Search
          options={[
            "ID Pengguna CST",
            "Tanggal & Waktu",
            "Activity",
            "Komentar",
            "Status",
          ]}
          placeholder="Username, Activity"
          placement="left"
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />
      </Title>
      <div className={classes.container}>
        <Filter
          dataFilter={dataFilter}
          setDataFilter={(e) => {
            setPage(1);
            setDataFilter(e);
          }}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
          ]}
        />

        <div style={{ marginTop: 20 }}>
          <TableICBB
            selecable={false}
            handleSelect={setSelectedIds}
            handleSelectAll={setSelectedIds}
            headerContent={tableConfig}
            dataContent={(data?.workflowApprovalDtoList ?? []).map((elm) => ({
              ...elm,
              disabled:
                elm?.isAllowTransaction === false || elm?.status === "Approved",
              additionalData: JSON.parse(elm?.additionalData),
            }))}
            page={page}
            setPage={setPage}
            isLoading={isLoading}
            totalElement={data?.totalElements}
            totalData={data?.totalPages}
            selectedValue={selectedIds}
            extraComponents={
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  gap: 20,
                }}
              >
                <ButtonOutlined
                  onClick={() => {
                    setConf({
                      isOpen: false,
                      message: "Are You Sure To Reject The Activity?",
                      action: 6,
                    });
                    setCommentModal(true);
                  }}
                  style={{ marginLeft: 220 }}
                  label={
                    isLoadingExecute ? (
                      <CircularProgress size={20} color="primary" />
                    ) : selectedIds.length < 2 ? (
                      "Reject "
                    ) : (
                      "Reject All"
                    )
                  }
                />
                <GeneralButton
                  onClick={() => {
                    setConf({
                      isOpen: false,
                      message: "Are You Sure To Approve the Activity?",
                      action: 5,
                    });
                    setCommentModal(true);
                  }}
                  style={{ marginRight: 30 }}
                  label={
                    isLoadingExecute ? (
                      <CircularProgress size={20} color="primary" />
                    ) : selectedIds.length < 2 ? (
                      "Approve "
                    ) : (
                      "Approve All"
                    )
                  }
                  width="135px"
                />
              </div>
            }
          />
        </div>
      </div>

      <SuccessConfirmation
        img={isSuccessReject && illustrationX}
        isOpen={successConfirmModal?.isOpen}
        title="Success"
        handleClose={() => {
          setSuccessConfirmModal({
            isOpen: false,
            message: "",
            subMessage: "",
          });
          dispatch(setClearDetail());
          setCommentModal(false);
          history.push(pathnameCONFIG.APPROVAL.MATRIX);
          setConf({ isOpen: false, message: "", action: 0 });
          setSelectedIds([]);
          onLoad();
          setComment("");
        }}
        message={successConfirmModal?.message}
        submessage="Some activity may need approval"
      />
      <DeletePopup
        loading={isLoadingExecute}
        isOpen={conf.isOpen}
        handleClose={() => setConf({ isOpen: false, message: "", action: 0 })}
        onContinue={() => {
          if (conf.action === 5) {
            handleApprove(conf.action);
          }
          if (conf.action === 6) {
            handleApprove(conf.action);
          }
        }}
        message={conf.message}
      />

      <PopUpComment
        onChange={(e) => setComment(e.target.value)}
        value={comment}
        open={commentModal}
        onContinue={() =>
          setConf({
            ...conf,
            isOpen: true,
          })
        }
        handleClose={() => {
          setCommentModal(false);
          setComment("");
        }}
      />

      <FailedConfirmation
        isOpen={failedConfirmModal}
        handleClose={() => setFailedConfirmModal(false)}
        title="Gagal"
      />
    </div>
  );
};

PersetujuanMatrix.propTypes = {};

PersetujuanMatrix.defaultProps = {};

export default PersetujuanMatrix;
