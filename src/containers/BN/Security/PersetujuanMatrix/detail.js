// libraries
import { Button, makeStyles } from "@material-ui/core";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import WarningAlert from "components/BN/Popups/WarningAlert";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import TextField from "components/BN/TextField/AntdTextField";
import Colors from "helpers/colors";
import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right.svg";

// components
import Title from "components/BN/Title";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import illustrationX from "assets/images/BN/illustrationred.png";
import {
  handleApprovePersetujuanApprovalMatrixExecute,
  handleGetPersetujuanApprovalMatrixDetail,
  setClear,
  handleApprovalMatrixAllMenu,
} from "stores/actions/approvalPersetujuanMatrix";
import { getLocalStorage } from "utils/helpers";
import ApprovalHeader from "components/BN/ApprovalHeader";

// Redux
import { setClearError } from "stores/actions/approvalConfig";
import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import PopupJenisRekening from "./popupJenisLayanan";

const useStyles = makeStyles({
  detailApproval: {},
  container: {
    padding: 30,
  },
  paper: {
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 20,
    height: "auto",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 400,
    marginBottom: 15,
    display: "flex",
    justifyContent: "space-between",
  },
  content: {
    minHeight: 269,
    width: "100%",
    border: "1px solid #B3C1E7",
    borderRadius: 10,
    "& .content-title": {
      fontSize: 15,
      fontFamily: "FuturaHvBT",
      color: "#374062",
      padding: "23px 20px",
    },
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "20px 40px",
    backgroundColor: "white",
  },
  papercomment: {
    border: "1px solid #B3C1E7",
    height: 190,
    marginTop: 20,
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 20,
  },
  detailCard: {
    backgroundColor: "#fff",
    borderRadius: 10,
    height: 154,
    width: "100%",
    position: "relative",
    "& .title": {
      fontSize: 20,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.hard,
      marginBottom: 10,
      display: "flex",
      justifyContent: "space-between",
    },
    "& .subtitle": {
      fontSize: 13,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.medium,
      marginBottom: 10,
    },
    "& .lihatsemua": {
      display: "flex",
      alignItems: "center",
      fontSize: 13,
      fontFamily: "FuturaMdBT",
      color: Colors.primary.hard,
      fontWeight: 700,
      "&:hover": {
        cursor: "pointer",
      },
    },
    "& .value": {
      marginBottom: 5,
    },
  },
});

const headerContent = [
  {
    title: "Seq",
    key: "seq",
    width: 100,
    render: (e, i) => i + 1,
  },
  {
    title: "User",
    key: "totalApproval",
    width: 150,
  },
  {
    title: "Group",
    key: "groupType",
  },
  {
    title: "Target Group",
    key: "targetKelompok",
    render: (e, i) => e?.portalGroupName || "-",
  },
];

const SemuaHeaderComponent = ({ classes, isShow, menuList }) => {
  const [openModal, setOpenModal] = useState(false);
  const { detail } = useSelector((e) => e?.approvalPersetujuanMatrix);

  return isShow ? (
    <React.Fragment>
      <PopupJenisRekening
        open={openModal}
        handleCloseModal={() => setOpenModal(false)}
        menuList={menuList[0]}
      />
      <div className={classes.detailCard}>
        <div className="title">{detail?.metadata?.menuName}</div>
        <div className="subtitle">Type of service set :</div>
        <div className="value">
          {menuList && (menuList[0]?.menuAccessChild ?? [])?.length} Service
        </div>
        <div className="lihatsemua">
          <Button
            disabled={!menuList}
            style={{ all: "unset", position: "relative" }}
            onClick={() => setOpenModal(true)}
          >
            View All Service Types
          </Button>
          <ChevronRight />
        </div>
      </div>
    </React.Fragment>
  ) : null;
};

const PersetujuanMatrixDetail = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    detail,
    isSuccessApprove,
    isSuccessReject,
    isLoading,
    isLoadingExecute,
    error,
  } = useSelector((e) => e?.approvalPersetujuanMatrix);

  const wfSquence =
    detail?.metadata?.workflowApprovalMatrixPortalDetailDtoList?.[0]
      ?.workflowGroupPortalDtoList ?? [];

  const [comment, setComment] = useState(null);
  const [successConfirmModal, setSuccessConfirmModal] = useState(false);
  const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [conf, setConf] = useState(false);
  const [errorAlert, setErrorAlert] = useState(false);

  const cekSemua = () => detail?.metadata?.menuName?.includes("All");

  useEffect(() => {
    const params = JSON.parse(getLocalStorage("persetujuanMatrixApproval"));
    if (!params.reffNo) history.push(pathnameCONFIG.APPROVAL.MATRIX);
    if (params.reffNo) {
      const payload = {
        reffNo: params.reffNo,
      };
      dispatch(handleGetPersetujuanApprovalMatrixDetail(payload));
    }
  }, []);

  const handleApprove = (action) => {
    const params = JSON.parse(getLocalStorage("persetujuanMatrixApproval"));

    const payload = {
      action,
      referenceNumber: params.reffNo,
      workFlowMenu: "APPROVAL_MATRIX_PORTAL",
      modifiedReason: comment,
    };
    dispatch(handleApprovePersetujuanApprovalMatrixExecute(payload));
  };

  useEffect(() => {
    if (isSuccessApprove?.isSuccess) {
      setSuccessConfirmModal({
        isOpen: true,
        message: "Activity Successfully Approved",
        submessage: isSuccessApprove?.releaser
          ? "Please wait for approval"
          : undefined,
      });
    }

    if (isSuccessReject) {
      setSuccessConfirmModal({
        isOpen: true,
        message: "Activity Successfully Rejected",
      });
    }
    setConf({ isOpen: false, message: "" });
  }, [isSuccessApprove, isSuccessReject]);

  const menuList = (detail?.additionalData ?? []).map((elm) => ({
    ...elm,
    menuAccessChild: elm.menuAccessChild.filter(
      (elm) => !elm.name_en.includes("All")
    ),
  }));

  useEffect(() => {
    if (detail?.userDataApprovalWorkflowDto?.comment) {
      setComment(detail?.userDataApprovalWorkflowDto?.comment);
    } else setComment("");
  }, [detail]);

  return (
    <div className={classes.detailApproval}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setClear())}
      />
      <Button
        startIcon={
          <img
            src={arrowLeft}
            style={{ paddingLeft: "23px", paddingTop: "10px" }}
            alt="Kembali"
          />
        }
        onClick={() => history.goBack()}
      />
      <Title label="Matrix Approval Details" />

      <div className={classes.container}>
        <ApprovalHeader
          label={
            <div style={{ textTransform: "capitalize" }}>
              {detail?.userDataApprovalWorkflowDto?.activityName
                ?.split(" ")[0]
                ?.toLowerCase()}{" "}
              Data
            </div>
          }
          status={detail?.userDataApprovalWorkflowDto?.statusName}
        />
        <div className={classes.paper}>
          <SemuaHeaderComponent
            isShow={cekSemua()}
            classes={classes}
            menuList={menuList}
          />
          {!cekSemua() && (
            <div className={classes.title}>{detail?.metadata?.menuName} </div>
          )}
          <div className={classes.content}>
            <div className="content-title">
              {wfSquence?.length} Number of Approvals
            </div>
            <TableMenuBaru
              headerContent={headerContent}
              dataContent={wfSquence}
              isLoading={isLoading}
            />
          </div>
        </div>
        <div className={classes.papercomment}>
          <p style={{ marginBottom: 5 }}>Comment :</p>
          <TextField
            disabled={
              !detail?.userDataApprovalWorkflowDto?.statusName?.includes(
                "WAITING"
              )
            }
            type="textArea"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            placeholder="Comments"
            style={{ width: "100%", height: 128 }}
          />
        </div>
      </div>
      <div className={classes.buttonGroup}>
        <ButtonOutlined
          label="Reject"
          disabled={
            !detail?.userDataApprovalWorkflowDto?.statusName
              ?.toLowerCase()
              ?.includes("waiting")
          }
          width="86px"
          style={{ marginRight: 30 }}
          color="#0061A7"
          onClick={() =>
            setConf({
              isOpen: true,
              message: "Are You Sure To Reject The Activity?",
              action: 6,
            })
          }
        />
        <GeneralButton
          label="Approve"
          disabled={
            !detail?.userDataApprovalWorkflowDto?.statusName
              ?.toLowerCase()
              ?.includes("waiting")
          }
          onClick={() =>
            setConf({
              isOpen: true,
              message: "Are You Sure To Approve the Activity?",
              action: 5,
            })
          }
        />
      </div>
      <SuccessConfirmation
        img={isSuccessReject && illustrationX}
        isOpen={successConfirmModal?.isOpen}
        title="Success"
        handleClose={() => {
          setSuccessConfirmModal({ isOpen: false, message: "" });
          dispatch(setClear());
          history.push(pathnameCONFIG.APPROVAL.MATRIX);
        }}
        message={successConfirmModal?.message}
      />

      <DeletePopup
        loading={isLoadingExecute}
        isOpen={conf.isOpen}
        handleClose={() => setConf({ isOpen: false, message: "" })}
        onContinue={() => {
          if (conf.action === 5) {
            handleApprove(conf.action);
          }
          if (conf.action === 6) {
            handleApprove(conf.action);
          }
        }}
        message={conf.message}
      />
      <FailedConfirmation
        isOpen={failedConfirmModal}
        handleClose={() => setFailedConfirmModal(false)}
        title="Gagal"
      />
      <WarningAlert
        isOpen={errorAlert}
        message="Anda Tidak Bisa Melakukan Approval"
        handleClose={() => {
          dispatch(setClearError());
          setErrorAlert(false);
        }}
      />
    </div>
  );
};

PersetujuanMatrixDetail.propTypes = {};

PersetujuanMatrixDetail.defaultProps = {};

export default PersetujuanMatrixDetail;
