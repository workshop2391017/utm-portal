import { Backdrop, CircularProgress, Fade, Modal } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import ScrollCustom from "components/BN/ScrollCustom";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { handleApprovalMatrixAllMenu } from "stores/actions/approvalPersetujuanMatrix";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  modaltitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 20,
    fontWeight: 700,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  headerModal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  content: {},
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
  },
  paper: {
    width: 452,
    height: 449,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },

  checkbox: {
    display: "flex",
    justifyContent: "space-between",
    padding: "11px 0",
    marginLeft: 15,
    fontSize: 15,
    fontFamily: "FuturaBkBT",
  },
  allCheckBox: {
    marginTop: 37,
    marginBottom: 8,
    fontFamily: "FuturaHvBT",
    fontSize: 15,
  },
}));

const PopupJenisRekening = ({ handleCloseModal, open, parentId, menuList }) => {
  const classes = useStyles();

  const { isLoadingAllMenu, detail } = useSelector(
    (e) => e?.approvalPersetujuanMatrix
  );

  return (
    <Modal
      className={classes.modal}
      open={open}
      onClose={handleCloseModal}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 900,
      }}
    >
      <Fade in={open}>
        <div className={classes.paper}>
          <div className={classes.headerModal}>
            <div className={classes.modaltitle}>Menu Type</div>
            <div className={classes.xContainer}>
              <XIcon className="close" onClick={handleCloseModal} />
            </div>
          </div>
          <div className={classes.content}>
            <div className={classes.allCheckBox}>{menuList?.name_en}</div>
            {isLoadingAllMenu ? (
              <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
                <div style={{ margin: "auto" }}>
                  <CircularProgress color="primary" size={40} />
                </div>
              </div>
            ) : (
              <ScrollCustom height={280}>
                {(menuList?.menuAccessChild ?? []).map((elm, index) => (
                  <div className={classes.checkbox} key={index}>
                    {elm.name_en}
                  </div>
                ))}
              </ScrollCustom>
            )}
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default PopupJenisRekening;
