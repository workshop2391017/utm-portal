// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";

// libraries

// components
import Title from "../../../../components/BN/Title";
import ButtonOutlined from "../../../../components/BN/Button/ButtonOutlined";
import GeneralButton from "../../../../components/BN/Button/GeneralButton";
import TextField from "../../../../components/BN/TextField/AntdTextField";
import TagsGroup from "../../../../components/BN/Tags/TagsGroup";
import SuccessConfirmation from "../../../../components/BN/Popups/SuccessConfirmation";
import DeleteConfirmation from "../../../../components/BN/Popups/DeleteConfirmation";

const ApprovalPengguna = (props) => {
  // const {} = props;
  const useStyles = makeStyles({
    blacklistSandi: {},
    container: {
      padding: "33px 30px",
      display: "flex",
    },
    paper: {
      width: "100%",
      minHeight: 500,
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      borderRadius: 8,
      padding: "20px 20px 80px",
      position: "relative",
    },
    title: {
      fontFamily: "FuturaHvBT",
      fontSize: 15,
      fontWeight: 900,
    },
    subtitle: {
      fontFamily: "Futura",
      fontSize: 12,
      paddingBottom: 4,
      color: "#AEB3C6",
      borderBottom: "1px solid #AEB3C6",
    },
    tags: {
      width: "100%",
    },
    button: {
      position: "absolute",
      bottom: 20,
    },
  });
  const classes = useStyles();

  const [mpin, setMpin] = useState(null);
  const [sandi, setSandi] = useState(null);
  const [mpinTags, setMpinTags] = useState([]);
  const [sandiTags, setSandiTags] = useState([]);

  const [successModal, setSuccessModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);

  const clickAddMpin = (e) => {
    e.preventDefault();
    if (mpin) {
      setMpinTags([
        ...mpinTags,
        {
          key: mpinTags[mpinTags.length - 1].key + 1,
          label: mpin,
        },
      ]);
    }
  };
  const clickAddSandi = (e) => {
    e.preventDefault();
    if (sandi) {
      setSandiTags([
        ...sandiTags,
        {
          key: sandiTags[sandiTags.length - 1].key + 1,
          label: sandi,
        },
      ]);
    }
  };

  return (
    <div className={classes.blacklistSandi}>
      <Title label="MPIN & Sandi Terlarang" />
      <div className={classes.container}>
        <div style={{ width: "50%", paddingRight: 10 }}>
          <div className={classes.paper}>
            <h1 className={classes.title}>MPIN Terlarang</h1>
            <div style={{ margin: "23px 0" }}>
              <p style={{ marginBottom: 3 }}>Masukan MPIN :</p>
              <div style={{ display: "flex" }}>
                <div style={{ flex: "auto" }}>
                  <TextField
                    value={mpin}
                    onChange={(e) => setMpin(e.target.value)}
                    placeholder="123456"
                    style={{ width: "100%" }}
                  />
                </div>
                <div style={{ marginLeft: 14 }}>
                  <GeneralButton
                    label="Tambahkan"
                    width="125px"
                    onClick={clickAddMpin}
                  />
                </div>
              </div>
            </div>
            <div className={classes.subtitle}>Daftar MPIN terlarang</div>
            <div className={classes.tags}>
              <TagsGroup
                tags={mpinTags}
                setTags={setMpinTags}
                chipWidth={92}
                onClick={() => setDeleteModal(true)}
              />
            </div>
            <div className={classes.button} style={{ left: 20 }}>
              <ButtonOutlined label="Batal" width="77px" />
            </div>
            <div className={classes.button} style={{ right: 20 }}>
              <GeneralButton
                label="Simpan"
                width="93px"
                onClick={() => setSuccessModal(true)}
              />
            </div>
          </div>
        </div>
        <div style={{ width: "50%", paddingLeft: 10 }}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Sandi Terlarang</h1>
            <div style={{ margin: "23px 0" }}>
              <p style={{ marginBottom: 3 }}>Masukan Sandi Terlarang :</p>
              <div style={{ display: "flex" }}>
                <div style={{ flex: "auto" }}>
                  <TextField
                    value={sandi}
                    onChange={(e) => setSandi(e.target.value)}
                    placeholder="password123"
                    style={{ width: "100%" }}
                  />
                </div>
                <div style={{ marginLeft: 14 }}>
                  <GeneralButton
                    label="Tambahkan"
                    width="125px"
                    onClick={clickAddSandi}
                  />
                </div>
              </div>
            </div>
            <div className={classes.subtitle}>Daftar Sandi terlarang</div>
            <div className={classes.tags}>
              <TagsGroup
                tags={sandiTags}
                setTags={setSandiTags}
                chipWidth={92}
                onClick={() => setDeleteModal(true)}
              />
            </div>
            <div className={classes.button} style={{ left: 20 }}>
              <ButtonOutlined label="Batal" width="77px" />
            </div>
            <div className={classes.button} style={{ right: 20 }}>
              <GeneralButton
                label="Simpan"
                width="93px"
                onClick={() => setSuccessModal(true)}
              />
            </div>
          </div>
        </div>
      </div>
      <SuccessConfirmation
        isOpen={successModal}
        handleClose={() => setSuccessModal(false)}
      />
      <DeleteConfirmation
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
      />
    </div>
  );
};

ApprovalPengguna.propTypes = {};

ApprovalPengguna.defaultProps = {};

export default ApprovalPengguna;
