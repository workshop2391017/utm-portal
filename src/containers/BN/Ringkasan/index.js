// main
import React, {
  useState,
  useEffect,
  useContext,
  useCallback,
  useMemo,
} from "react";

import { makeStyles } from "@material-ui/core";

// import Loader
// redux
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import {
  handleCompireTransaction,
  handleGetActivityDetails,
  handleGetTransactionChart,
  handleGetTransactionHistoryPage,
} from "stores/actions/dashboard";
import { setClearDetailData } from "stores/actions/dashboard/activityDetail";
import { checkOnline } from "stores/actions/errorGeneral";

// helpers
import {
  backendDate,
  generalDateFormat,
  getLocalStorage,
  rangeDate,
  searchDate,
} from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";

import Toast from "components/BN/Toats";
import { setHandleClearError as handleClearErrorTransaction } from "stores/actions/dashboard/compireTransaction";
import { setHandleClearError as handleClearErrorChart } from "stores/actions/dashboard/transactionChart";
import { setHandleClearError as handleClearErrorHistory } from "stores/actions/dashboard/transactionHistoryPage";
import formatNumber from "helpers/formatNumber";
import GeneralButton from "components/BN/Button/GeneralButton";

import NoAccess from "components/BN/NoAccess";
import FailedConfirmation from "components/BN/Popups/FailedConfirmationData";
import usePrevious from "utils/helpers/usePrevious";

import { RootContext } from "router";
import HeaderContext from "helpers/context/header";

// libraries

// components
import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Filter from "components/BN/Filter/GeneralFilter";
import Card from "components/BN/Cards/DashboardCard";
import Chart from "components/BN/Charts/Chart";
import ActivityDetails from "components/BN/Popups/ActivityDetail";
import TableICBB from "components/BN/TableIcBB";
import Badge from "components/BN/Badge";
import WarningAlert from "components/BN/Popups/WarningAlert";

// assets

import warning from "assets/images/BN/illustrationyellow.png";
import { mappActivity, searchActivity } from "utils/helpers/dashboardActivity";

// Jss
const useStyles = makeStyles((theme) => ({
  center: {
    margin: "94px auto 0",
    width: "fit-content",
    textAlign: "center",
  },
  account: {
    marginBottom: 20,
  },
  desc: {
    fontSize: 24,
  },
  container: {
    padding: "20px 30px",
  },
  card: {
    display: "flex",
    justifyContent: "space-between",
    margin: "30px 0 20px",
    gap: "10px",
  },
  welcomeCard: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: "10px",
    width: "100%",
    height: 552,
  },
  chart: {
    minHeight: 419,
    backgroundColor: "#fff",
    borderRadius: 8,
    padding: "18px 15px 15px",
    marginBottom: 20,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 16,
    fontWeight: 900,
  },
  rooting: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  titlee: {
    flexGrow: 1,
    display: "none",
    text: "bold",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "gray",
  },
  appbar: {
    backgroundColor: "#F4F7FB",
    height: "60px",
    // blend: 'pass through',
    // opacity: '0,2',
    // textSizeAdjust: '50px',
  },
  inputRooting: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(5)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "16ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

/* --------------------------------- START --------------------------------- */
const Ringkasan = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    data,
    isLoading,
    error: errorHistory,
  } = useSelector((e) => e?.transactionHistoryPage);
  const ACTIVITY_DETAILS = useSelector((e) => e?.activityDetail);
  const {
    isLoadingChart,
    dataChart: chartData,
    error: errorChart,
  } = useSelector(({ transactionChart }) => ({
    ...transactionChart,
  }));

  const {
    dataCompire,
    isLoadingCompire,
    error: errorCompire,
  } = useSelector((e) => e?.compireTransaction);

  // const { userLogin } = useContext(RootContext);
  const { isRefreshed } = useContext(HeaderContext);

  const userLoginStorage = getLocalStorage("portalUserLogin");
  const userLogin = userLoginStorage ? JSON.parse(userLoginStorage) : {};

  const [activeChartTabs, setActiveChartTabs] = useState("1");
  const [errorAlert, setErrorAlert] = useState(false);

  const [page, setPage] = useState(1);
  const [noDataModal, setNoDataModal] = useState(false);
  const [detailModal, setDetailModal] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState({
    tabs: {
      tabValue1: 0,
    },
    date: {
      dateValue2: {
        startDate: null,
        endDate: null,
      },
      dateValue3: {
        startDate: null,
        endDate: null,
      },
    },
  });

  const tableConfig = ({ setDetailModal }) => [
    {
      title: "Transaction ID",
      headerAlign: "left",
      key: "transactionId",
      align: "left",
    },
    {
      title: "Date & Time",
      headerAlign: "left",
      align: "left",

      render: (rowData) => (
        <span>
          {moment(rowData.transactionDate).format("DD-MM-YYYY | HH:mm:ss")}
        </span>
      ),
    },
    {
      // PINDAH DANA - oa
      // TRANSFER Sesama Bank - on
      // TRANSFER BANK LAIN - off
      // TRANSFER_RTGS
      // TRANSFET_SKN
      title: "Activity",
      headerAlign: "left",
      align: "left",
      render: (row) => mappActivity(row?.transactionCategory),
      // render: (rowData) => {
      //   if (rowData.transactionCategory?.toLowerCase().includes("transfer")) {
      //     if (rowData.transactionCategory?.toLowerCase().includes("oa")) {
      //       return <span>Own Account Transfer</span>;
      //     }
      //     if (rowData.transactionCategory?.toLowerCase().includes("on")) {
      //       return <span>Sesama Transfer</span>;
      //     }
      //     if (rowData.transactionCategory?.toLowerCase().includes("off")) {
      //       return <span>TRANSFER BANK LAIN</span>;
      //     }
      //     return (
      //       <span>{rowData.transactionCategory?.split("_", "").join(" ")}</span>
      //     );
      //   }
      //   return <span>{rowData.transactionCategory?.split("_").join(" ")}</span>;
      // },
    },

    {
      title: "Customer ID",
      headerAlign: "left",
      align: "left",
      key: "userProfileId",
    },
    // {
    //   title: "Application Version",
    //   headerAlign: "left",
    //   align: "left",
    //   render: (rowData) => <span>{rowData?.appVersion ?? "-"}</span>,
    // },
    {
      title: "Status",
      headerAlign: "center",
      align: "center",
      render: (rowData) => (
        <Badge
          label={
            rowData.status === 1
              ? "Error"
              : rowData.status === 0
              ? "Transaction"
              : "Anomaly"
          }
          type={
            rowData.status === 1
              ? "red"
              : rowData.status === 0
              ? "green"
              : "orange"
          }
          styleBadge={{ marginRight: "5px" }}
        />
      ),
    },
    {
      title: "",
      textAlign: "center",
      width: 150,
      render: (rowData) => (
        <div>
          <GeneralButton
            label="View Details"
            width="76px"
            height="23px"
            style={{ marginRight: 11, fontSize: 9, padding: 0 }}
            onClick={() => setDetailModal(rowData)}
          />
        </div>
      ),
    },
  ];

  const handleDateFilter = useCallback(([type, defaultDate], searchType) => {
    const searchFunc = searchType === "searchDate" ? backendDate : searchDate;
    const dateKey = dataFilter?.date;

    switch (type) {
      case "fromDateBefore":
        return dateKey.dateValue2?.startDate
          ? searchFunc(dateKey.dateValue2?.startDate)
          : defaultDate || null;
      case "toDateBefore":
        return dateKey.dateValue2?.endDate
          ? searchFunc(dateKey.dateValue2?.endDate)
          : defaultDate || null;
      case "fromDateNow":
        return dateKey.dateValue3?.startDate
          ? searchFunc(dateKey.dateValue3?.startDate)
          : defaultDate || null;
      case "toDateNow":
        return dateKey.dateValue3?.endDate
          ? searchFunc(dateKey.dateValue3?.endDate)
          : defaultDate || null;
      default:
        return null;
    }
  });

  const getRangeDate = (start = 1, numOfRange) => {
    const dates = [];

    for (let i = start; i <= numOfRange; i++) {
      const date = moment();
      date.subtract(i, "day").format("DD-MM-YYYY");
      dates.push(moment(date).format("YYYY-MM-DD"));
    }

    return {
      from: dates[dates.length - 1], // terkecil
      to: dates[0], // terbesar
    };
  };

  const handleTabsFilters = (section) => {
    const MINGGUAN = "Mingguan";
    const BULANAN = "Bulanan";
    const TAHUNAN = "Tahunan";

    const dateKey = dataFilter?.date;

    const date1 = rangeDate(
      dateKey.dateValue2?.startDate,
      dateKey.dateValue2?.endDate
    );
    const date2 = rangeDate(
      dateKey.dateValue3?.startDate,
      dateKey.dateValue3?.endDate
    );

    let fromDateBefore = null;
    let toDateBefore = null;

    let fromDateNow = null;
    let toDateNow = null;

    // tabs filter chart default
    let type = MINGGUAN;

    const curr = getRangeDate(0, 6);
    const before = getRangeDate(7, 13);

    // when the search date, more than 30 days, set to monthly
    if (date1 >= 365 || date2 >= 365) {
      type = TAHUNAN;
    } else if (date1 >= 30 || date2 >= 30) {
      type = BULANAN;
    } else {
      // tabs filter
      switch (dataFilter?.tabs?.tabValue1) {
        case 0:
          type = MINGGUAN;

          // illustration with rangeDate 1. [start, end],  2[start, end]

          fromDateBefore = before.from;
          toDateBefore = before.to;
          fromDateNow = curr.from;
          toDateNow = curr.to;

          break;
        case 1:
          type = BULANAN;

          // illustration with rangeDate 1. [start, end],  2[start, end]

          fromDateBefore = moment()
            .startOf("months")
            .subtract(1, "months")
            .format("YYYY-MM-DD");
          toDateBefore = moment()
            .endOf("months")
            .subtract(1, "months")
            .format("YYYY-MM-DD");

          fromDateNow = moment().startOf("months").format("YYYY-MM-DD");
          toDateNow = moment().endOf("months").format("YYYY-MM-DD");
          break;
        case 2:
          type = TAHUNAN;

          // illustration with rangeDate 1. [start, end],  2.[start, end]

          fromDateBefore = moment()
            .startOf("years")
            .subtract(1, "years")
            .format("YYYY-MM-DD");
          toDateBefore = moment()
            .endOf("years")
            .subtract(1, "years")
            .format("YYYY-MM-DD");

          fromDateNow = moment().startOf("years").format("YYYY-MM-DD");
          toDateNow = moment().endOf("years").format("YYYY-MM-DD");
          break;
        default:
          type = MINGGUAN;
          break;
      }
    }

    // get calculation [key] based on / [week, month, yaers ]/ transaction by the type
    let key = "dayTransaction";
    let index = 1;

    switch (type) {
      case MINGGUAN:
        key = "dayTransaction";
        index = 1;
        break;
      case BULANAN:
        key = "weekTransaction";
        index = 2;
        break;
      case TAHUNAN:
        key = "monthTransaction";
        index = 0;
        break;
      default:
        key = "dayTransaction";
        index = 1;
    }

    // get transaction [key] by the active tabs
    const transaksi =
      activeChartTabs === "1"
        ? ["transactionSuccessNow", "transactionSuccessBefore"]
        : ["transactionErrorNow", "transactionErrorBefore"];

    const usedCategory =
      chartData?.[transaksi[0]]?.[index]?.[key]?.categories?.length >
      chartData?.[transaksi[1]]?.[index]?.[key]?.categories?.length
        ? chartData?.[transaksi[0]]?.[index]?.[key]?.categories ?? []
        : chartData?.[transaksi[1]]?.[index]?.[key]?.categories ?? [];

    let categories = (
      chartData?.[transaksi[0]]?.[index]?.[key]?.categories ?? []
    ).map((elm, index) => `Day ${index + 1}`);

    // get categories title by the type

    if (type === BULANAN) {
      const dataFillOne = usedCategory;

      categories = dataFillOne.map((elm, index) => [
        `Week ${index + 1}`,
        `${moment(moment(elm, "YYYY/MM/DD")).format("MMM YYYY")}`,
      ]);
    } else if (type === TAHUNAN) {
      const dataFillOne = usedCategory;

      // categories = ["week1 Agu 2021", "week2", "week3", "week4", "week5"];
      categories = dataFillOne.map((elm, index) => [
        moment(moment(elm, "YYYY/MM/DD")).format("MMM"),
        moment(moment(elm, "YYYY/MM/DD")).format("YYYY"),
      ]);
    } else if (type === MINGGUAN) {
      // get the most existing days from existing data

      const dataFillOne = usedCategory;

      categories = (dataFillOne ?? []).map((elm, index) => `Day ${index + 1}`);
    }

    if (section === "chart") {
      return {
        transaksi,
        index,
        key,
        categories,
      };
    }
    return {
      fromDateBefore,
      toDateBefore,
      fromDateNow,
      toDateNow,
    };
  };

  const searchHistory = useDebounce(dataSearch, 1300);
  const prevSearch = usePrevious(searchHistory);

  const handleGetHistoryPages = async (searchHistory) => {
    const { fromDateBefore, toDateBefore, fromDateNow, toDateNow } =
      handleTabsFilters();

    const historyPageParams = {
      direction: "DESC",
      fromDateBefore:
        fromDateBefore || handleDateFilter(["fromDateBefore"], "searchDate"),
      toDateBefore:
        toDateBefore ||
        handleDateFilter(["toDateBefore"], "searchDate") ||
        fromDateBefore ||
        handleDateFilter(["fromDateBefore"], "searchDate"),
      fromDateNow:
        fromDateNow || handleDateFilter(["fromDateNow"], "searchDate"),
      toDateNow:
        toDateNow ||
        handleDateFilter(["toDateNow"], "searchDate") ||
        fromDateBefore ||
        handleDateFilter(["fromDateBefore"], "searchDate"),
      pageNumber: page - 1,
      pageSize: 10,
      transactionCategory: null,
      searchValue: null,
      // searchValueList: !searchHistory ? null : searchActivity(searchHistory),
      searchValueList: !searchHistory
        ? null
        : searchActivity(searchHistory).length > 0
        ? searchActivity(searchHistory)
        : [searchHistory],
      status: Number(activeChartTabs) - 1, // 0 transaksi, 1 error
    };

    if (page === 1) {
      dispatch(handleGetTransactionHistoryPage(historyPageParams));
    } else if (searchHistory !== prevSearch) {
      setPage(1);
    } else {
      dispatch(handleGetTransactionHistoryPage(historyPageParams));
    }

    // dispatch(handleGetTransactionHistoryPage(historyPageParams));
  };

  useEffect(() => {
    if (
      userLogin?.portalGroupDetailDtoList &&
      userLogin?.portalGroupDetailDtoList[0]?.masterMenu.findIndex(
        (value) => value.id === 69
      ) !== -1
    ) {
      handleGetHistoryPages(searchHistory);
    }
  }, [page, dataFilter, activeChartTabs, searchHistory]);

  // get detail activity
  useEffect(async () => {
    if (
      userLogin?.portalGroupDetailDtoList &&
      userLogin?.portalGroupDetailDtoList[0]?.masterMenu.findIndex(
        (value) => value.id === 69
      ) !== -1
    ) {
      if (Object.keys(detailModal).length > 0) {
        dispatch(checkOnline());
        dispatch(
          handleGetActivityDetails(
            {
              activityDate: moment(detailModal?.transactionDate).format(
                "YYYY-MM-DD"
              ),
              transactionId: detailModal?.transactionId,
              userProfileId: detailModal?.userProfileId,
              activityName: mappActivity(detailModal?.transactionCategory),
            },
            detailModal
          )
        );
      }
    }
  }, [detailModal]);

  // change chartTabs to 1 when dataFilter change
  useEffect(() => {
    if (dataFilter) {
      setActiveChartTabs("1");
    }
  }, [dataFilter]);

  const searchDatevalidate = (dateKey) => {
    const date1 = rangeDate(
      dateKey.dateValue2?.startDate,
      dateKey.dateValue2?.endDate
    );
    const date2 = rangeDate(
      dateKey.dateValue3?.startDate,
      dateKey.dateValue3?.endDate
    );

    if (date2 - date1 > 5) {
      setErrorAlert(true);
      return false;
    }

    return true;
  };

  useEffect(async () => {
    if (
      userLogin?.portalGroupDetailDtoList &&
      userLogin?.portalGroupDetailDtoList[0]?.masterMenu.findIndex(
        (value) => value.id === 69
      ) !== -1
    ) {
      const { fromDateBefore, toDateBefore, fromDateNow, toDateNow } =
        handleTabsFilters();

      if (data?.totalElements === 0) {
        setNoDataModal(true);
      }

      const requestPayload = {
        fromDateBefore:
          fromDateBefore || handleDateFilter(["fromDateBefore"], "searchDate"),
        toDateBefore:
          toDateBefore || handleDateFilter(["toDateBefore"], "searchDate"),
        fromDateNow:
          fromDateNow ||
          handleDateFilter(["fromDateNow"], "searchDate") ||
          fromDateBefore ||
          handleDateFilter(["fromDateBefore"], "searchDate"),
        toDateNow:
          toDateNow ||
          handleDateFilter(["toDateNow"], "searchDate") ||
          toDateBefore ||
          handleDateFilter(["toDateBefore"], "searchDate"),
      };

      dispatch(checkOnline());
      dispatch(handleGetTransactionChart(requestPayload));
    }
  }, [dataFilter]);

  useEffect(async () => {
    if (
      userLogin?.portalGroupDetailDtoList &&
      userLogin?.portalGroupDetailDtoList[0]?.masterMenu.findIndex(
        (value) => value.id === 69
      ) !== -1
    ) {
      const { fromDateBefore, toDateBefore, fromDateNow, toDateNow } =
        handleTabsFilters();

      const requestPayload = {
        fromDateBefore:
          fromDateBefore || handleDateFilter(["fromDateBefore"], "searchDate"),
        toDateBefore:
          toDateBefore || handleDateFilter(["toDateBefore"], "searchDate"),
        fromDateNow:
          fromDateNow ||
          handleDateFilter(["fromDateNow"], "searchDate") ||
          fromDateBefore ||
          handleDateFilter(["fromDateBefore"], "searchDate"),
        toDateNow:
          toDateNow ||
          handleDateFilter(["toDateNow"], "searchDate") ||
          toDateBefore ||
          handleDateFilter(["toDateBefore"], "searchDate"),
      };
      dispatch(checkOnline());
      dispatch(handleCompireTransaction(requestPayload));
    }
  }, [dataFilter]);

  const getChartNameFormat = (data) =>
    (data ?? []).map((elm, index) => {
      const date = elm.split(", ");
      if (date.length > 1) {
        return date
          .map((range) => generalDateFormat(range, "DD/MM/YYYY"))
          .join(" - ");
      }
      return generalDateFormat(elm, "DD/MM/YYYY");
    });

  const series = useMemo(() => {
    const { transaksi, index, key, categories } = handleTabsFilters("chart");

    return {
      series: [
        {
          // chart x axis tooltip label => zformatter
          name: getChartNameFormat(
            chartData?.[transaksi[0]]?.[index]?.[key]?.categories ?? []
          ),
          // chart data
          data: chartData?.[transaksi[0]]?.[index]?.[key]?.data?.data ?? [],
        },
        {
          name: getChartNameFormat(
            chartData?.[transaksi[1]]?.[index]?.[key]?.categories ?? []
          ),
          // chart data
          data: chartData?.[transaksi[1]]?.[index]?.[key]?.data?.data ?? [],
        },
      ],
      categories,
    };
  }, [chartData, activeChartTabs]);

  return (
    <div className={classes.ringkasan}>
      <Toast
        open={
          errorCompire?.isError || errorChart?.isError || errorHistory?.isError
        }
        message={
          errorCompire?.message ||
          errorChart?.message ||
          errorHistory?.message ||
          null
        }
        handleClose={() => {
          dispatch(handleClearErrorTransaction());
          dispatch(handleClearErrorChart());
          dispatch(handleClearErrorHistory());
        }}
      />

      <Title label="Dashboard" />
      {userLogin?.portalGroupDetailDtoList &&
      userLogin?.portalGroupDetailDtoList[0]?.masterMenu.findIndex(
        (value) => value.id === 69
      ) === -1 ? (
        <NoAccess />
      ) : (
        <div className={classes.container}>
          <Filter
            page="dashboard"
            withTitle
            isDashboard
            title="Show:"
            dataFilter={dataFilter}
            setDataFilter={(search) => {
              if (searchDatevalidate(search?.date)) {
                setDataFilter(search);
              }
            }}
            align="left"
            options={[
              {
                id: 1,
                type: "tabs",
                options: ["Weekly", "Monthly", "Annual"],
              },
              {
                id: 2,
                type: "rangePicker",
                placeholder: ["Start Date", "End Date"],
              },
              {
                id: 3,
                type: "rangePicker",
                placeholder: ["Start Date", "End Date"],
              },
            ]}
          />
          <div className={classes.card}>
            <Card
              color="blue"
              title="Total Transaction"
              number1={dataCompire?.totalAktivitas?.amountNow ?? 0}
              number2={dataCompire?.totalAktivitas?.amountBefore ?? 0}
              percentage={dataCompire?.totalAktivitas?.percentage ?? 0}
              status={dataCompire?.totalAktivitas?.status}
              loading={isLoadingCompire}
            />
            <Card
              color="red"
              title="Failed Transactions"
              number1={dataCompire?.transaksiGagal?.amountNow ?? 0}
              number2={dataCompire?.transaksiGagal?.amountBefore ?? 0}
              percentage={dataCompire?.transaksiGagal?.percentage ?? 0}
              status={dataCompire?.transaksiGagal?.status ?? 0}
              loading={isLoadingCompire}
            />
            <Card
              color="green"
              title="Successful Transactions"
              number1={dataCompire?.transaksiSukses?.amountNow ?? 0}
              number2={dataCompire?.transaksiSukses?.amountBefore ?? 0}
              percentage={dataCompire?.transaksiSukses?.percentage ?? 0}
              status={dataCompire?.transaksiSukses?.status ?? 0}
              loading={isLoadingCompire}
            />
            {/* <Card
              color="purple"
              title="Revenue Projection"
              number1={
                dataCompire &&
                `Rp. ${formatNumber(
                  dataCompire?.proyeksiPendapatan?.amountNow ?? 0
                )}`
              }
              number2={
                dataCompire &&
                `Rp. ${formatNumber(
                  dataCompire?.proyeksiPendapatan?.amountBefore ?? 0
                )}`
              }
              percentage={dataCompire?.proyeksiPendapatan?.percentage ?? 0}
              status={dataCompire?.proyeksiPendapatan?.status}
              loading={isLoadingCompire}
              showInfo
            /> */}
          </div>
          <div className={classes.chart}>
            <h1 className={classes.title}>Transaction</h1>
            <Chart
              series={series.series}
              categories={series.categories}
              isLoading={isLoadingChart}
              onChangeTabsChart={setActiveChartTabs}
              activeChartTabs={activeChartTabs}
              tabsValue={activeChartTabs}
            />
          </div>
          <div className={classes.search}>
            <Search
              dataSearch={dataSearch}
              placeholder="Transaksi ID, Activity"
              setDataSearch={setDataSearch}
              placement="bottom"
            />
          </div>
          <div>
            <TableICBB
              headerContent={tableConfig({ setDetailModal })}
              dataContent={data?.content ?? []}
              page={page}
              setPage={setPage}
              isLoading={isLoading}
              // onClickRow={setDetailModal}
              totalData={data?.totalPages - 1}
              totalElement={data?.totalElements}
            />
          </div>
        </div>
      )}

      <ActivityDetails
        activityDetail={ACTIVITY_DETAILS}
        isOpen={detailModal !== false}
        handleClose={() => {
          setDetailModal(false);
          dispatch(setClearDetailData());
        }}
      />
      <FailedConfirmation
        message="Invalid date selection"
        submessage="The maximum date difference is five days"
        isOpen={errorAlert}
        handleClose={() => setErrorAlert(false)}
        warning
        img={warning}
        title="Warning"
      />
    </div>
  );
};

Ringkasan.propTypes = {};

Ringkasan.defaultProps = {};

export default Ringkasan;
