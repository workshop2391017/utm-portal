import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import AntdTextField from "components/BN/TextField/AntdTextField";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setAddOneData } from "stores/actions/responseSetting";
import { Grid } from "@material-ui/core";
import DeletePopup from "components/BN/Popups/Delete";

const useStyles = makeStyles({
  page: {
    padding: "20px",
    height: "100%",
    backgroundColor: "#F4F7FB",
    position: "relative",
  },
  card: {
    boxSizing: "border-box",
    height: "625px",
    width: "600px",
    borderRadius: "20px",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    backgroundColor: "#fff",
    padding: "30px",
    margin: "auto",
    marginTop: "10%",
  },
});

const Title = styled.div`
  color: #2b2f3c;
  font-size: 20px;
  font-family: FuturaHvBT;
  font-weight: 900;
  margin-bottom: 10px;
`;

const SubTitle = styled.div`
  color: #2b2f3c;
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 16px;
  margin-bottom: 10px;
`;

const Label = styled.div`
  color: #374062;
  font-weight: 500;
  font-family: FuturaBQ;
  font-size: 13px;
  margin-bottom: 5px;
`;

const SubContent = styled.div`
  color: #374062;
  font-weight: 400;
  font-family: FuturaBkBT;
  font-size: 13px;
  margin-bottom: 26px;
`;
const TambahResponseSetting = (props) => {
  const classes = useStyles();
  const [formData, setFormData] = useState({
    judul: "",
    descIndonesia: "",
    descEnglish: "",
  });
  const [openSuccess, setOpenSuccess] = useState(false);
  const [openConfirmation, setOpenConfirmation] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();

  const { editData } = useSelector((state) => state.responseSetting);

  // useEffect(() => {
  //   if (editData) {
  //     setFormData({
  //       judul: editData.title,
  //       desc: editData.description,

  //     });
  //   }
  // }, []);

  return (
    <Grid className={classes.page}>
      <SuccessConfirmation
        isOpen={openSuccess}
        // title="Perubahan Berhasil"
        // message="Perubahan Data Berhasil Disimpan"
        // submessage="Silakan menunggu persetujuan"
        handleClose={() => {
          setOpenSuccess(false);
          setOpenConfirmation(false);
        }}
      />
      <DeletePopup
        isOpen={openConfirmation}
        // title="Perubahan Berhasil"
        message="Are You Sure To Add Data?"
        onContinue={() => setOpenSuccess(true)}
        // submessage="Silakan menunggu persetujuan"
        handleClose={() => setOpenConfirmation(false)}
      />

      {editData ? <Title>Edit Response</Title> : <Title>Add Response</Title>}

      <Grid className={classes.card}>
        {editData ? (
          <SubTitle>Form Edit Response</SubTitle>
        ) : (
          <SubTitle>Form Add Response</SubTitle>
        )}

        <SubContent>Please fill in the data below to add a response</SubContent>
        <div>
          <Label>Response Title:</Label>
          <AntdTextField
            style={{
              width: "100%",
            }}
            value={formData.judul}
            onChange={(e) =>
              setFormData({ ...formData, judul: e.target.value })
            }
            placeholder="Enter the response title"
          />
        </div>
        <div
          style={{
            marginTop: "10px",
          }}
        >
          <Label>Indonesian Response Content:</Label>
          <AntdTextField
            style={{
              width: "100%",
              height: "174px",
            }}
            type="textArea"
            value={formData.descIndonesia}
            onChange={(e) =>
              setFormData({ ...formData, descIndonesia: e.target.value })
            }
            placeholder="Enter the Indonesian response content"
          />
        </div>
        <div
          style={{
            marginTop: "10px",
          }}
        >
          <Label>English Response Content:</Label>
          <AntdTextField
            style={{
              width: "100%",
              height: "174px",
            }}
            type="textArea"
            value={formData.descEnglish}
            onChange={(e) =>
              setFormData({ ...formData, descEnglish: e.target.value })
            }
            placeholder="Enter the Indonesian response content"
          />
        </div>
      </Grid>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          position: "absolute",
          bottom: 0,
          left: "1px",
          right: "2px",
          padding: "20px",

          backgroundColor: "#fff",
        }}
      >
        <ButtonOutlined
          label="Cancel"
          width="86px"
          color="#0061A7"
          onClick={() => {
            dispatch(setAddOneData(null));
            history.goBack();
          }}
        />

        <GeneralButton
          onClick={() => setOpenConfirmation(true)}
          label="Save"
          width="100px"
          height="43px"
        />
      </div>
    </Grid>
  );
};

TambahResponseSetting.propTypes = {};

export default TambahResponseSetting;
