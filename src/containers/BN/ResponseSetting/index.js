import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import GeneralButton from "components/BN/Button/GeneralButton";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import TableCard from "components/BN/TableIcBB/TableCard";
import { dataDummyResponseSetting } from "services/mockup_data/BN/dataResponseSetting";
import DeletePopup from "components/BN/Popups/Delete";

import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import { useDispatch } from "react-redux";
import { setAddOneData } from "stores/actions/responseSetting";
import AddBlueUpdate from "../../../assets/icons/BN/addWhiteUpdate.svg";

const useStyles = makeStyles({
  page: {
    height: "100%",
    backgroundColor: "#F4F7FB",
    padding: "20px",
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 40,
  },
  card: {
    display: "flex",
    justifyContent: "space-between",
    height: "72px",
    width: "100%",
    padding: "16px 20px",
    backgroundColor: "#fff",
    borderRadius: "10px",
    marginTop: "26px",
    marginBottom: "20px",
  },
  cardContent: {
    height: "240px",
    width: "320px",
    backgroundColor: "#fff",
    borderRadius: "10px",
    // marginRight: "28px",
    marginBottom: "18px",
  },
  header: {
    padding: "12px 15px",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    height: "42px",
    borderBottom: "1px solid #E8EEFF",
  },
  childHeaderRight: {
    display: "flex",
    alignItems: "center",
    gap: 10,
  },
});

const Title = styled.div`
  color: #2b2f3c;
  font-size: 20px;
  font-family: FuturaHvBT;
  font-weight: 900;
`;

const ResponseSetting = (props) => {
  const classes = useStyles();
  const [searchValue, setSearchValue] = useState("");
  const history = useHistory();
  const [page, setPage] = useState(1);
  const [modalYakin, setModalYakin] = useState(false);
  const [successConfirm, setSuccessConfirm] = useState(false);
  const dispatch = useDispatch();
  return (
    <div className={classes.page}>
      <div className={classes.container}>
        <Title>Contact Support Response </Title>
        <div className={classes?.childHeaderRight}>
          <SearchWithDropdown
            placeholder="Title"
            dataSearch={searchValue}
            style={{
              width: "200px",
            }}
            setDataSearch={setSearchValue}
          />
          <GeneralButton
            iconPosition="start"
            buttonIcon={
              <img
                src={AddBlueUpdate}
                alt="addBlueUpdate"
                width="12px"
                height="12px"
              />
            }
            label="Response"
            style={{
              width: "133px",
              height: "44px",
              fontSize: "15px",
              fontWeight: 700,
              fontFamily: "FuturaMdBT",
              color: "#fff",
              padding: "10px 20px",
            }}
            onClick={() =>
              history.push(pathnameCONFIG.UTILITAS.RESPONSE_SETTING_ADD)
            }
          />
        </div>
      </div>

      <TableCard
        iconGlobe
        background={false}
        data={dataDummyResponseSetting}
        onDelete={() => {
          console.warn("testing");
          setModalYakin(true);
        }}
        onEdit={(item) => {
          console.warn("item:", item);

          dispatch(setAddOneData(item));

          history.push(pathnameCONFIG.UTILITAS.RESPONSE_SETTING_ADD);
        }}
        totalData={10}
        page={page}
        setPage={() => console.warn("test")}
        totalElement={120}
      />
      <SuccessConfirmation
        isOpen={successConfirm}
        message="Response Dihapus"
        submessage="Anda telah berhasil menghapus response,
      Sedang menunggu persetujuan"
        handleClose={() => setSuccessConfirm(false)}
      />

      <DeletePopup
        isOpen={modalYakin}
        handleClose={() => setModalYakin(false)}
        onContinue={() => {
          setModalYakin(false);
          setSuccessConfirm(true);
        }}
        title="Konfirmasi"
        message="Hapus Response ?"
        submessage="Apakah Anda yakin ingin manghapus
        response ?"
      />
    </div>
  );
};

ResponseSetting.propTypes = {};

export default ResponseSetting;
