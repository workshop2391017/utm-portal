/* eslint-disable jsx-a11y/control-has-associated-label */
// main
import React, { useState, useEffect } from "react";

// libraries
import { makeStyles, Grid, CircularProgress } from "@material-ui/core";

// redux
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
  handleSubmit,
  setErrorAlreadyLogin,
  setIsBlockirAccount,
  setError,
  setIpChecking,
  handleKeycloakSubmit,
} from "stores/actions/login";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import IpLock from "assets/icons/BN/ip-lock.svg";

import { useHistory } from "react-router-dom";

import { Form } from "antd";
import { removeAllLocalStorage } from "utils/helpers";
import { encryptPassword } from "utils/encrypt";
import BlockedPopup from "components/BN/Popups/BlockedPopup";

// components
import GeneralButton from "../../../components/BN/Button/GeneralButton";
import TextField from "../../../components/BN/TextField/AntdTextField";
import WrongPassword from "../../../components/BN/Popups/WrongPassword";
import Simple from "../../../components/BN/Popups/Simple";

// assets
import bannerImage from "../../../assets/images/BN/bannerPortal.jpg";
import user from "../../../assets/icons/BN/user.svg";
import lock from "../../../assets/icons/BN/lock.svg";
import RawLogo from "../../../assets/images/BN/rawlabs-logo-yellow.png";

import keycloak from "utils/helpers/keycloak";
// import { useKeycloak } from "@react-keycloak/web";
// import Keycloak from "keycloak-js";

import UserService from "services/UserService";
import axios from "axios";
import { setLocalStorage } from "utils/helpers";
import { pathnameCONFIG } from "configuration";

import { dataResponDummy } from "./dummyNewLogin";

const LoginScreen = () => {
  // const [count, setCount] = useState(0);
  // const { keycloak } = useKeycloak();
  // const keycloakToken = UserService.getToken();
  // const isKeycloakLogin = UserService.isLoggedIn();
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    isLoading,
    errorAlreadyLogin,
    isBlockirAccount,
    error,
    isIpChecking,
  } = useSelector(({ login }) => ({
    isLoading: login.isLoading,
    errorAlreadyLogin: login.errorAlreadyLogin,
    isBlockirAccount: login.isBlockirAccount,
    error: login.error,
    isIpChecking: login.isIpChecking,
  }));
  const [username, setUsername] = useState("");
  const [usernameError1, setUsernameError1] = useState(false);
  const [usernameError2, setUsernameError2] = useState(false);
  const [password, setPassword] = useState("");
  const [passwordError1, setPasswordError1] = useState(false);
  const [passwordError2, setPasswordError2] = useState(false);
  const [passwordError3, setPasswordError3] = useState(false);
  const [passwordError4, setPasswordError4] = useState(false);

  // console.log("autentikasi token ", keycloakToken);
  // console.log("autentikasi login ", isKeycloakLogin);

  const useStyles = makeStyles({
    container: {
      backgroundColor: "#fff",
      display: "flex",
      flexDirection: "column",
      flexGrow: 1,
    },
    loginTitle: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontStyle: "normal",
      fontSize: "28px",
      color: "#374062",
      lineHeight: "59.68px",
      height: 60,
      width: 167,
      margin: "159px 0 30px",
    },
    loginSubtitle: {
      fontFamily: "FuturaBQ",
      lineHeight: "15.23px",
      fontSize: "13px",
      height: 16,
      color: "#BCC8E7",
      margin: "10px 10px",
      textAlign: "center",
    },
    inputTitle: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: "13px",
      color: "#374062",
      // lineHeight: "15.23px",
      // marginTop: 29,
      width: "320px",
      transition: "0.3s",
    },
    banner: {
      width: "100%",
      height: "100%",
      objectFit: "fill",
      // display: "block",
    },
    paper: {
      height: 140,
      width: 100,
    },
    Item: {
      marginLeft: "90px",
      color: "#FFCCC1",
    },
    error: {
      fontSize: 13,
      color: "#FF6F6F",
      position: "absolute",
      left: 0,
      transition: "0.3s ease-in-out",
    },
    error2: {
      marginTop: 10,
      fontSize: 13,
      color: "#FF6F6F",
      position: "absolute",
      left: 0,
      transition: "0.3s ease-in-out",
    },
    form: {
      position: "relative",
      margin: 0,
      marginBottom: 20,
      "& .ant-input::placeholder": {
        fontSize: 15,
        fontFamily: "FuturaBkBT",
        color: "#BCC8E7",
      },
      "& .ant-form-item-control-input .inputUsername": {
        "&:hover": {
          boxShadow: `0 0 0 2px rgb(${
            usernameError1 || usernameError2 ? "255 111 111" : "87 168 233"
          } / 20%)`,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: `0 0 0 2px rgb(${
            usernameError1 || usernameError2 ? "255 111 111" : "87 168 233"
          } / 20%)`,
        },
        "& .ant-input": {
          boxShadow: "none",
          fontSize: 15,
          color: "#0061A7",
        },
        "&:focus": {
          boxShadow: `0 0 0 2px rgb(${
            usernameError1 || usernameError2 ? "255 111 111" : "87 168 233"
          } / 20%) !important`,
        },
      },
      "& .ant-form-item-control-input .inputPassword": {
        "&:hover": {
          fontFamily: "FuturaBkBT",
          fontSize: "15px",
          boxShadow: `0 0 0 2px rgb(${
            passwordError1 || passwordError2 || passwordError3 || passwordError4
              ? "255 111 111"
              : "87 168 233"
          } / 20%)`,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: `0 0 0 2px rgb(${
            passwordError1 || passwordError2 || passwordError3 || passwordError4
              ? "255 111 111"
              : "87 168 233"
          } / 20%)`,
        },
        "& .ant-input": {
          boxShadow: "none",
          fontSize: 15,
          color: "#0061A7",
        },
        "&:focus": {
          boxShadow: `0 0 0 2px rgb(${
            passwordError1 || passwordError2 || passwordError3 || passwordError4
              ? "255 111 111"
              : "87 168 233"
          } / 20%) !important`,
        },
      },
    },
  });
  const classes = useStyles();
  // ------ Example useDispatch and useSelecter --------
  // const dispatch = useDispatch();

  // const { counter } = useSelector(
  //   ({ exampleReducer }) => ({
  //     counter: exampleReducer.counter,
  //   }),
  //   shallowEqual
  // );

  // useEffect(() => {
  //   // Update the document title using the browser API
  //   document.title = `You clicked ${counter} times`;
  // }, [counter]);

  const handleCloseModalAlreadyLogin = () => {
    dispatch(setErrorAlreadyLogin({ isError: false, message: "" }));
  };

  const login = (e) => {
    e.preventDefault();
    setUsernameError1(false);
    setPasswordError1(false);
    setUsernameError2(false);
    setPasswordError2(false);
    setPasswordError3(false);
    setPasswordError4(false);
    const body = {
      username,
      password: encryptPassword(password),
    };

    if (username && password) {
      dispatch(handleSubmit(body, history));
      // axios.post(`${process.env.REACT_APP_API_BN}/login${process.env.REACT_APP_DEPLOY_BN==='BN' ? 'ldap' : ''}`, body, headers)
      // .then(res => {
      // sessionStorage.setItem('user', JSON.stringify({...res.data, token: res.headers.access_token}));

      // })
      // .catch(err => {
      //   if (err.response) {
      //     const error = err.response.data.errorCode;
      // if (error === '0033') {
      //   setUsernameError1(true);
      //   setPasswordError1(false);
      //   setUsernameError2(false);
      //   setPasswordError2(false);
      //   setPasswordError3(false);
      //   setPasswordError4(false);
      // } else if (error === '0034') {
      //   setUsernameError1(false);
      //   setPasswordError1(true);
      //   setUsernameError2(false);
      //   setPasswordError2(false);
      //   setPasswordError3(false);
      //   setPasswordError4(false);
      // } else if (error === '0039') {
      //   setAlreadyLoginLabel(`${username} sedang login. Silahkan masukkan username lainnya.`);
      //   setAlreadyLoginModal(true);
      //   setUsernameError1(false);
      //   setPasswordError1(false);
      //   setUsernameError2(false);
      //   setPasswordError2(false);
      //   setPasswordError3(false);
      //   setPasswordError4(false);
      // } else if (error === '0058') {
      //   setAlreadyLoginLabel(err.response.data.idnMessage);
      //   setAlreadyLoginModal(true);
      //   setUsernameError1(false);
      //   setPasswordError1(false);
      //   setUsernameError2(false);
      //   setPasswordError2(false);
      //   setPasswordError3(false);
      //   setPasswordError4(false);
      // } else if (error === '0025') {
      //   setModalIsOpen(true);
      //   setUsernameError1(false);
      //   setPasswordError1(false);
      //   setUsernameError2(false);
      //   setPasswordError2(false);
      //   setPasswordError3(false);
      //   setPasswordError4(false);
      // } else alert(err);
      //   };
      //   setLoading(false);
      // });
    } else if (username && !password) {
      setUsernameError2(false);
      setPasswordError2(true);
      setUsernameError1(false);
      setPasswordError1(false);
      setPasswordError3(false);
      setPasswordError4(false);
    } else if (!username && password) {
      setUsernameError2(true);
      setPasswordError2(false);
      setUsernameError1(false);
      setPasswordError1(false);
      setPasswordError3(false);
      setPasswordError4(false);
    } else {
      setUsernameError2(true);
      setPasswordError2(true);
      setUsernameError1(false);
      setPasswordError1(false);
      setPasswordError3(false);
      setPasswordError4(false);
    }
  };

  const onTypeUsername = (e) => {
    if (e.target.value) {
      setUsernameError1(false);
      setUsernameError2(false);
    } else {
      setUsernameError1(false);
      setUsernameError2(true);
    }
    dispatch(setError({ isError: false, message: "" }));
    setUsername(e.target.value);
  };

  const onTypePassword = (e) => {
    // const paswd =
    //   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (e.target.value) {
      // if (e.target.value.length < 8) {
      setPasswordError1(false);
      setPasswordError2(false);
      //     setPasswordError3(true);
      //     setPasswordError4(false);
      //   }
      //   else if (!e.target.value.match(paswd)) {
      //     setPasswordError1(false);
      //     setPasswordError2(false);
      //     setPasswordError3(false);
      //     setPasswordError4(true);
      //   } else {
      //     setPasswordError1(false);
      //     setPasswordError2(false);
      //     setPasswordError3(false);
      //     setPasswordError4(false);
      //   }
    } else {
      setPasswordError1(false);
      setPasswordError2(true);
      // setPasswordError3(false);
      // setPasswordError4(false);
    }
    dispatch(setError({ isError: false, message: "" }));
    setPassword(e.target.value);
  };

  // const handleWrongPassword = () => {
  //   setCount(0);
  //   localStorage.removeItem('loginCount');
  //   setModalIsOpen(false);
  // };
  // useEffect(() => {
  //   if(count) {
  //     localStorage.setItem('loginCount', count);
  //   };
  //   if(localStorage.getItem('loginCount')==3) {
  //     setModalIsOpen(true);
  //   };
  // }, [count]);

  useEffect(() => {
    // keycloak
    //   .init({ onLoad: "login-required" })
    //   .then((authenticated) => {
    //     if (authenticated) {
    //       console.log("user is authenticated : ", authenticated);
    //       // window.location.href = "http://localhost:3000/portal";
    //       alert("user is authenticated");
    //     } else {
    //       console.log("user is not authenticated : ", authenticated);
    //       // alert("user is not authenticated");
    //     }
    //   })
    //   .catch((error) => {
    //     console.log("error keycloack : ", error);
    //   });
    // checkSso();
    // const isLoggedin = UserService.isLoggedIn();
    // console.log("keycloak is loggin : ", isLoggedin);
    // console.log("keycloak token : ", UserService.getToken());
    // const body = {
    //   username: "raw.dwi",
    //   password: encryptPassword("admin"),
    // };
    // if (isLoggedin) {
    //   dispatch(handleSubmit(body, history));
    // }
    UserService.initKeycloak(doAfterLogin);
    removeAllLocalStorage();
    if (sessionStorage.getItem("user")) {
      console.log("dashboard masuk sini ");
      window.location.replace("/portal/dashboard");
    }
  }, []);

  const doAfterLogin = (data) => {
    console.log("callback keycloak : ", data);
    const postData = {};

    // Custom headers for the request
    const headers = {
      "Content-Type": "application/json",
      Authorization: `${data.tokenKeycloak}`, // Replace with your actual token
    };

    axios
      .post(
        "https://be.arccelera.cloud/api/portalservice/logininfo",
        postData,
        {
          headers: headers,
        }
      )
      .then((response) => {
        // console.log("Response Data:", response.data);
        // console.log("Response Headers:", response.headers);
        const dataUserResp = {
          ...dataResponDummy,
        };
        console.log("dataUserResp : ", dataUserResp);
        setLocalStorage("portalAccessToken", response.headers.access_token);
        setLocalStorage("portalUserLogin", JSON.stringify(dataUserResp));
        history.push(pathnameCONFIG.DASHBOARD);
        return;
      })
      .catch((error) => {
        console.error("Error:", error);
        return;
      });
  };

  // useEffect(() => {
  //   if (isKeycloakLogin) {
  //     console.log("ini hanif true");
  //     const body = {
  //       username: "raw.dwi",
  //       password: encryptPassword("admin"),
  //     };
  //     dispatch(handleKeycloakSubmit(body, history));
  //   }
  // }, [isKeycloakLogin, keycloakToken]);

  // const checkSso = async () => {
  //   try {
  //     const authenticated = await keycloak.init();
  //     console.log(
  //       `user is ${authenticated ? "authenticated" : "not authenticated"}`
  //     );
  //   } catch (e) {
  //     console.log("error sso : ", e);
  //   }
  // };

  // fomlogin
  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CircularProgress size={60} />
    </div>
    // <div className={classes.container}>
    //   <SuccessConfirmation
    //     isOpen={isIpChecking}
    //     handleClose={() => dispatch(setIpChecking(false))}
    //     message="IP Checking Active"
    //     title=""
    //     submessage="You cannot login. Please use the registered IP"
    //     img={IpLock}
    //     height="390px"
    //     closeModal={false}
    //   />
    //   <Grid>
    //     <div style={{ width: "50%", height: "100vh", float: "right" }}>
    //       <img className={classes.banner} alt="" src={bannerImage} />
    //     </div>
    //     <div style={{ float: "left" }}>
    //       <div
    //         style={{
    //           width: "120x",
    //           height: "80px",
    //           position: "absolute",
    //           top: "8%",
    //           left: "4%",
    //         }}
    //       >
    //         <img
    //           style={{ width: "100%", height: "100%", objectFit: "contain" }}
    //           src={RawLogo}
    //           alt=""
    //         />
    //       </div>
    //       <Form
    //         initialValues={{
    //           ip: "",
    //           username: "",
    //           password: "",
    //         }}
    //         autoComplete="off"
    //         style={{
    //           marginLeft: 90,
    //         }}
    //       >
    //         <div className={classes.loginTitle}>Login</div>
    //         <div
    //           className={classes.inputTitle}
    //           style={{
    //             color: usernameError1 || usernameError2 ? "#FF6F6F" : "#374062",
    //           }}
    //         >
    //           Username :
    //         </div>
    //         <Form.Item className={classes.form}>
    //           <TextField
    //             type="input"
    //             style={{
    //               margin: "5px 0",
    //               width: "360px",
    //               borderColor:
    //                 usernameError1 || usernameError2 ? "#FF6F6F" : "#BCC8E7",
    //               position: "relative",
    //               zIndex: 1,
    //             }}
    //             prefix={
    //               <img
    //                 src={user}
    //                 alt="user"
    //                 style={{ width: "12px", height: "13.5px" }}
    //               />
    //             }
    //             className="inputUsername"
    //             placeholder="Username"
    //             value={username}
    //             onChange={onTypeUsername}
    //           />
    //           <p
    //             className={classes.error}
    //             style={{
    //               opacity: usernameError1 ? 1 : 0,
    //               bottom: usernameError1 ? -18 : 5,
    //             }}
    //           >
    //             Your Username is Wrong
    //           </p>
    //           <p
    //             className={classes.error}
    //             style={{
    //               opacity: usernameError2 ? 1 : 0,
    //               bottom: usernameError2 ? -18 : 5,
    //             }}
    //           >
    //             Please enter username
    //           </p>
    //         </Form.Item>
    //         <div
    //           className={classes.inputTitle}
    //           style={{
    //             color:
    //               passwordError1 ||
    //               passwordError2 ||
    //               passwordError3 ||
    //               passwordError4
    //                 ? "#FF6F6F"
    //                 : "#374062",
    //           }}
    //         >
    //           Password :
    //         </div>
    //         <Form.Item className={classes.form} style={{ marginBottom: 46 }}>
    //           <TextField
    //             type="password-with-eye"
    //             style={{
    //               // margin: "5px 0",
    //               width: "360px",
    //               borderColor:
    //                 passwordError1 ||
    //                 passwordError2 ||
    //                 passwordError3 ||
    //                 passwordError4
    //                   ? "#FF6F6F"
    //                   : "#BCC8E7",
    //               position: "relative",
    //               zIndex: 1,
    //             }}
    //             prefix={
    //               <img
    //                 src={lock}
    //                 alt="lock"
    //                 style={{ width: "13.5px", height: "15px" }}
    //               />
    //             }
    //             className="inputPassword"
    //             placeholder="Password"
    //             value={password}
    //             onChange={onTypePassword}
    //           />
    //           <p
    //             className={classes.error}
    //             style={{
    //               opacity: passwordError1 ? 1 : 0,
    //               bottom: passwordError1 ? -18 : 0,
    //             }}
    //           >
    //             Your Password is Wrong
    //           </p>
    //           <p
    //             className={classes.error}
    //             style={{
    //               opacity: passwordError2 ? 1 : 0,
    //               bottom: passwordError2 ? -18 : 0,
    //             }}
    //           >
    //             Please Enter password
    //           </p>
    //           <p
    //             className={classes.error}
    //             style={{
    //               opacity: passwordError3 ? 1 : 0,
    //               bottom: passwordError3 ? -18 : 0,
    //             }}
    //           >
    //             Enter a password of at least 8 characters
    //           </p>
    //           {/* <p
    //             className={classes.error2}
    //             style={{
    //               opacity: passwordError4 ? 1 : 0,
    //               bottom: passwordError4 ? -33 : 0,
    //             }}
    //           >
    //             Password harus memiliki setidaknya satu Lowercase, Uppercase,
    //             Number, dan Special Character
    //           </p> */}
    //           <p
    //             className={classes.error}
    //             style={{
    //               opacity: error.isError ? 1 : 0,
    //               bottom: error.isError ? -18 : 0,
    //             }}
    //           >
    //             {error.message}
    //           </p>
    //         </Form.Item>

    //         <div className={classes.loginSubtitle}>
    //           *Please login using username and password
    //         </div>

    //         <Form.Item>
    //           <button onClick={login} style={{ display: "none" }} />
    //           <GeneralButton
    //             label={
    //               isLoading ? (
    //                 <CircularProgress size={20} color="primary" />
    //               ) : (
    //                 "Login"
    //               )
    //             }
    //             width="360px"
    //             height="44px"
    //             variant="contained"
    //             onClick={login}
    //             disabled={!!isLoading}
    //           />
    //         </Form.Item>
    //       </Form>
    //     </div>
    //   </Grid>
    //   <BlockedPopup
    //     isOpen={isBlockirAccount}
    //     handleClose={() => dispatch(setIsBlockirAccount(false))}
    //   />
    //   <Simple
    //     isOpen={errorAlreadyLogin.isError}
    //     label={errorAlreadyLogin.message}
    //     handleClose={handleCloseModalAlreadyLogin}
    //   />
    // </div>
  );
};

LoginScreen.propTypes = {};

LoginScreen.defaultProps = {};

export default LoginScreen;
