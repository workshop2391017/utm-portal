import React, { useEffect, useState } from "react";
import { makeStyles, Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";

// components
import Title from "components/BN/Title";
import TableICBB from "components/BN/TableIcBB";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import GeneralButton from "components/BN/Button/GeneralButton";
import DeleteConfirmation from "components/BN/Popups/Delete";

import { ReactComponent as SvgPlus } from "assets/icons/BN/plus-white.svg";
import { ReactComponent as DeleteIcon } from "assets/icons/BN/trash-21.svg";
import TambahKategori from "./TambahKategoriPreferensi";
import {
  getPromoCategory,
  handlePromoDeleteCategory,
  handlePromoSaveCategory,
  setCategoriesField,
  setError,
  setPopUp,
} from "../../../stores/actions/promo";
import useDebounce from "../../../utils/helpers/useDebounce";
import usePrevious from "../../../utils/helpers/usePrevious";
import SuccessConfirmation from "../../../components/BN/Popups/SuccessConfirmation";
import { validateTask } from "../../../stores/actions/validateTaskPortal";

const dataHeader = ({ setAdd, setDeletePromo, dispatch }) => [
  {
    title: "Category ID",
    key: "code",
    width: 100,
  },
  {
    title: "Category Name",
    key: "name",
    width: 1000,
  },
  {
    width: 30,
    render: (rowData) => (
      <React.Fragment>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <button
            onClick={() => {
              dispatch(
                setCategoriesField({
                  id: rowData?.id,
                  code: rowData?.code,
                  name: rowData?.name,
                })
              );
              setDeletePromo(true);
            }}
            style={{
              outline: "none",
              border: "none",
              display: "flex",
              width: "72px",
              justifyContent: "space-between",
              background: "transparent",
            }}
          >
            <DeleteIcon />
          </button>
        </div>
      </React.Fragment>
    ),
  },
];
const useStyles = makeStyles(() => ({
  page: { padding: "20px" },
}));
const PromoCategory = () => {
  const classes = useStyles();
  const [isDelete, setIsDelete] = useState(false);
  const [search, setSearch] = useState("");
  const [pages, setPage] = useState(0);
  const [addConfirmation, setAddConfirmation] = useState(false);
  const [category, setCategory] = useState({
    id: 0,
    name: "",
  });
  const [add, setAdd] = useState(false);
  const prevPage = usePrevious(pages);
  const [deletePromo, setDeletePromo] = useState(false);
  const [success, setSuccess] = useState(false);
  const dispatch = useDispatch();
  const { page, categories, isLoading, popUp, dataFilter, isCategoriesField } =
    useSelector((state) => state.promo);
  const dataSearchDeb = useDebounce(search, 1300);

  const getPromo = () => {
    dispatch(
      getPromoCategory({
        pageSize: 10,
        pageNumber: pages === 0 ? pages : pages - 1,
        search,
      })
    );
  };
  const deletePromoCategory = () => {
    dispatch(
      validateTask(
        {
          id: category?.id,
          menuName: "173",
          code: category?.code,
          name: category?.name,
        },
        {
          onContinue() {
            dispatch(
              handlePromoDeleteCategory({
                id: category.id,
                code: category.code,
                name: category.name,
                isDeleted: true,
              })
            );
          },
          onError() {},
        },
        {
          redirect: false,
        }
      )
    );
  };

  useEffect(() => {
    setIsDelete(popUp);
  }, [popUp]);

  const addPromoCategory = () => {
    dispatch(
      validateTask(
        {
          code: category?.code,
          name: category?.name,
          menuName: "173",
        },
        {
          onContinue() {
            dispatch(
              handlePromoSaveCategory({
                code: category.code,
                name: category.name,
                isDeleted: false,
              })
            );
            setSuccess(true);
          },
          onError() {},
        },
        {
          redirect: false,
        }
      )
    );
  };

  useEffect(() => {
    if (pages === 0 || prevPage !== pages) {
      getPromo();
    } else {
      setPage(0);
    }
  }, [dataSearchDeb, dataFilter, pages]);

  useEffect(() => {
    setCategory({
      code: isCategoriesField?.code,
      name: isCategoriesField?.name,
      id: isCategoriesField?.id,
    });
  }, [isCategoriesField]);

  useEffect(() => {
    getPromo();
  }, []);

  return (
    <div className={classes.page}>
      <SuccessConfirmation
        title="Please Wait For Approval"
        message="Saved Successfully"
        isOpen={success}
        handleClose={() => {
          setSuccess(false);
        }}
      />
      <SuccessConfirmation
        title="Please Wait For Approval"
        message="Deleted Successfully"
        isOpen={isDelete}
        handleClose={() => {
          setIsDelete(false);
          dispatch(setPopUp(false));
        }}
      />
      <DeleteConfirmation
        isOpen={addConfirmation}
        message="Are You Sure To Add Data?"
        submessage="You cannot undo this action"
        handleClose={() => setAddConfirmation(false)}
        onContinue={() => {
          addPromoCategory();
          setAddConfirmation(false);
        }}
      />
      <DeleteConfirmation
        isOpen={deletePromo}
        message="Are You Sure To Delete the Data?"
        submessage="You cannot undo this action"
        handleClose={() => setDeletePromo(false)}
        onContinue={() => {
          deletePromoCategory();
          setDeletePromo(false);
        }}
      />
      <TambahKategori
        title1="Add Promo Category"
        title2="Category Name"
        placeholder="Enter category name"
        isOpen={add}
        onContinue={() => {
          setAddConfirmation(true);
          setAdd(false);
        }}
        handleClose={() => {
          setAdd(false);
        }}
      />

      <Title paddingLeft={0} label="Promo Category">
        <div style={{ marginRight: "12px" }}>
          <SearchWithDropdown
            setDataSearch={setSearch}
            dataSearch={search}
            placeholder="Category ID, Category Name"
          />
        </div>
        <div>
          <GeneralButton
            width="192px"
            height="44px"
            label="Promo Category"
            onClick={() => setAdd(true)}
            buttonIcon={<SvgPlus />}
            iconPosition="startIcon"
          />
        </div>
      </Title>

      <div style={{ marginTop: "36px" }}>
        <TableICBB
          isLoading={isLoading}
          headerContent={dataHeader({ setAdd, setDeletePromo, dispatch })}
          setPage={setPage}
          page={pages}
          dataContent={categories}
          totalData={page?.totalPages}
          totalElement={page?.totalElements}
        />
      </div>
    </div>
  );
};

export default PromoCategory;
