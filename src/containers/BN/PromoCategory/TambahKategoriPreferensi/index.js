// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ButtonOutlined from "../../../../components/BN/Button/ButtonOutlined";
// components
import GeneralButton from "../../../../components/BN/Button/GeneralButton";
import TextField from "../../../../components/BN/TextField/AntdTextField";
import { setCategoriesField } from "../../../../stores/actions/promo";
import {
  parseNumber,
  parseTextNumFirstSpace,
  validateMinMax,
} from "../../../../utils/helpers";
import FormField from "../../../../components/BN/Form/InputGroupValidation";
import { useValidateLms } from "../../../../utils/helpers/validateLms";
import {
  confValidateMenuName,
  handlevalidationDataLms,
} from "../../../../stores/actions/validatedatalms";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 350,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: "normal",
    marginBottom: 20,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    marginTop: 71,
  },
}));

const TambahKategoriPreferensi = ({
  isOpen,
  handleClose,
  onContinue,
  title1,
  title2,
  placeholder,
  setOpenModalSucces,
  rowData,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [formData, setFormData] = useState({
    name: "",
    code: null,
  });

  const { isAlreadyExist } = useSelector((e) => e.validatedatalms);

  useValidateLms({
    name: formData?.name,
    code: formData?.code,
    nameCode: "47",
    codeCode: "47",
  });

  const vNameChar = !validateMinMax(formData.name, 2, 50);
  const vCodeChar = !validateMinMax(formData.code, 2, 8);

  const onSave = () => {
    dispatch(
      setCategoriesField({
        code: formData.code,
        name: formData.name,
      })
    );
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title1}</h1>
            <div
              style={{ display: "flex", flexDirection: "column", gap: "20px" }}
            >
              <div>
                <FormField
                  label="Category ID:"
                  error={isAlreadyExist.code || vCodeChar}
                  errorMessage={
                    isAlreadyExist.code
                      ? "Category ID is already exists"
                      : "Min 2 and Max 8 character"
                  }
                >
                  <TextField
                    // closeIcon={edit !== false ? true : null}
                    onClear={() => {
                      setFormData({ code: null });
                    }}
                    style={{ width: "100%" }}
                    type="text"
                    placeholder="Input Category ID"
                    value={formData?.code}
                    onChange={(e) => {
                      setFormData({
                        ...formData,
                        code: parseNumber(e.target.value),
                      });
                    }}
                  />
                </FormField>
              </div>
              <div>
                <FormField
                  label="Cateogry Name:"
                  error={vNameChar || isAlreadyExist.name}
                  errorMessage={
                    isAlreadyExist.name
                      ? "Category Name is already exists"
                      : "Min 2 and Max 50 character"
                  }
                >
                  <TextField
                    // closeIcon={edit !== false ? true : null}
                    onClear={() => {
                      setFormData({ name: "" });
                    }}
                    style={{ width: "100%" }}
                    placeholder="Input Category Name"
                    value={formData?.name}
                    onChange={(e) => {
                      setFormData({
                        ...formData,
                        name: e.target.value,
                      });
                    }}
                  />
                </FormField>
              </div>
            </div>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Cancel"
                width="157.5px"
                height="44px"
                color="#0061A7"
                onClick={() => {
                  handleClose();
                  setFormData({
                    code: null,
                    name: "",
                  });
                }}
                style={{ display: "flex", alignItems: "center" }}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                style={{ display: "flex", alignItems: "center" }}
                label="Save"
                width="157.5px"
                height="44px"
                onClick={() => {
                  onSave();
                  onContinue();
                  setFormData({
                    code: null,
                    name: "",
                  });
                }}
                disabled={
                  formData.name === undefined ||
                  !formData.name.replace(/\s/g, "").length ||
                  formData.code === undefined ||
                  formData.code === null ||
                  vNameChar ||
                  vCodeChar ||
                  isAlreadyExist.name ||
                  isAlreadyExist.code
                }
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

TambahKategoriPreferensi.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title1: PropTypes.string,
  title2: PropTypes.string,
};

TambahKategoriPreferensi.defaultProps = {
  title1: "Tambah Kategori",
  title2: "Nama Kategori",
};

export default TambahKategoriPreferensi;
