import React from "react";
import DetailTemplateApprovalWorkflow from "./DetailTemplate";

const DetailApprovalWorkflowMenuGlobal = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject"));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve"));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="title"
      renderDataUser={() => <div>a</div>}
      renderDetailData={() => <div>a</div>}
      setComment={(e) => {}}
      comment=""
      onReject={() => {}}
      onApprove={() => {}}
      isLoadingExecute={false/true}
      isLoading={false/true}
      isSuccess={false/true}
    />
  );
};

export default DetailApprovalWorkflowMenuGlobal;
