import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useMemo, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
});

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  const mapping = useMemo(() => {
    const obj = {
      SA: {
        name: "Saving Account",
        child: [],
      },
      CA: {
        name: "Current Account",
        child: [],
      },
      LN: {
        name: "Loan Account",
        child: [],
      },
      CD: {
        name: "Deposit Account",
        child: [],
      },
    };

    detailData?.forEach((elm) => {
      const type = elm?.accountType;
      obj[type]?.child?.push(elm);
    });

    return obj;
  }, [detailData]);

  return (
    <Grid container spacing={3}>
      <Grid container spacing={3} style={{ padding: 15 }}>
        {Object.keys(mapping).map((key) =>
          mapping[key]?.child?.length ? (
            <Grid item xs={12}>
              <div className={classes.detailTitle}>{mapping[key]?.name}</div>
              <Grid container spacing={3} style={{ paddingLeft: 20 }}>
                {mapping[key]?.child?.map((elm) => (
                  <Grid item xs={6}>
                    <div style={{ display: "inline-block" }}>
                      <div className={classes.listGroup}>
                        <div className={classes.bullets} />
                        <div>{elm?.productName}</div>
                      </div>
                    </div>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          ) : null
        )}
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkProdukRekening = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comment, setComments] = useState("");
  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Account Product Change Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkProdukRekening;
