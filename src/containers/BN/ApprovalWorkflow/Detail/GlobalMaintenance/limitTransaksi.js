import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import { formatAmountDot } from "utils/helpers";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
});

const dataHeader = () => [
  {
    title: "Currency Matrix",
    key: "currMtx",
  },
  {
    title: "Max Transaction / Day",
    key: "maxTrxDay",
  },
  {
    title: "Minimum Transaction",
    key: "minTrx",
  },
  {
    title: "Maximum Transaction",
    key: "maxTrx",
  },
  {
    title: "Max Nominal / Day",
    key: "maxNominalDay",
  },
];

const DetailData = ({ detailData }) => {
  const [dataDetailState, setDataDetailState] = useState([]);

  useEffect(() => {
    const restMappResult = detailData?.serviceDtoList?.map((item) => ({
      ...item,
      name: item?.serviceName,
      child: item?.serviceCurrencyMatrixDtoList?.map((val) => ({
        ...val,
        corporateId: 1,
        currMtx: val?.currencyMatrixName,
        maxTrxDay: val?.maxTransactionDay,
        minTrx: `${val?.currencyCode}. ${formatAmountDot(
          val?.minTransactionAmount
        )}`,
        maxTrx: `${val?.currencyCode}. ${formatAmountDot(
          val?.maxTransactionAmount
        )}`,
        maxNominalDay: `${val?.currencyCode}. ${formatAmountDot(
          val?.maxTransactionDayAmount
        )}`,
      })),
    }));
    setDataDetailState(restMappResult);
  }, [detailData]);

  return (
    <TableICBBCollapse
      headerContent={dataHeader()}
      dataContent={dataDetailState}
      // page={page}
      // setPage={setPage}
      // totalData={totalPages}
      // isLoading={isLoading}
      selecable={false} // hide selecable
      collapse={false} // hide icon collapse
      defaultOpenCollapseBreakdown // open collapse default
      handleSelect={(e) => console.warn("EE =>", e)}
      handleSelectAll={(e) => console.warn("HandleSelectAll =>", e)}
      selectedValue={[]}
    />
  );
};

const DetailApprovalWorkLimtTransaksi = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comment, setComments] = useState("");
  const detailData = dataDetail?.metadata;
  const additionalData = dataDetail?.additionalData;
  console.warn("detailData:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Transaction Limit Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={additionalData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkLimtTransaksi;
