import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Grid, makeStyles } from "@material-ui/core";

import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";

import Colors from "helpers/colors";

import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";

import Badge from "components/BN/Badge";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  marginLeft: {
    marginLeft: "16px",
  },
});

const DataUser = () => {
  const classes = useStyles();
  const { dataDetail } = useSelector((state) => state?.aproverWorkFlow);

  const onStatus = (status) => {
    if (status === "WAITING") {
      return (
        <Badge
          type="blue"
          label="Waiting"
          styleBadge={{ display: "inline-block" }}
        />
      );
    }

    if (status === "APPROVED") {
      return (
        <Badge
          type="blue"
          label="Approved"
          styleBadge={{ display: "inline-block" }}
        />
      );
    }

    if (status === "REJECTED") {
      return (
        <Badge
          type="blue"
          label="Rejected"
          styleBadge={{ display: "inline-block" }}
        />
      );
    }
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>
          {dataDetail?.userDataApprovalWorkflowDto?.createdUserName}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>
          {dataDetail?.userDataApprovalWorkflowDto?.userPortalId}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>
          {" "}
          {dataDetail?.userDataApprovalWorkflowDto?.createdDate}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>
          {" "}
          {dataDetail?.userDataApprovalWorkflowDto?.branchName}{" "}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>
          {" "}
          {dataDetail?.userDataApprovalWorkflowDto?.activityName}{" "}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>

        <div className={classes.valueDataUser}>
          {onStatus(dataDetail?.userDataApprovalWorkflowDto?.statusName)}
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  const classes = useStyles();
  const { dataDetail } = useSelector((state) => state?.aproverWorkFlow);

  // const [dataDetailParse, setDataDetailParse] = useState(
  //   JSON.parse(dataDetail?.metadata)
  // );

  console.warn("dataDetail:", dataDetail);

  return (
    <Grid container spacing={4}>
      <Grid item xs={4}>
        <Grid container direction="column">
          <Grid item classes={{ root: classes.labelDetail }}>
            Transaction Category :
          </Grid>
          <Grid item classes={{ root: classes.valueDetail }}>
            {dataDetail?.metadata?.service?.transactionCategory?.name}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={4}>
        <Grid container direction="column">
          <Grid item classes={{ root: classes.labelDetail }}>
            Service Name :
          </Grid>
          <Grid item classes={{ root: classes.valueDetail }}>
            {dataDetail?.metadata?.service?.name}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={4}>
        <Grid container direction="column">
          <Grid item classes={{ root: classes.labelDetail }}>
            Currency Matrix :
          </Grid>
          <Grid item>
            <Grid container direction="column" spacing={1}>
              {dataDetail?.metadata?.currencyMatrixList?.map((item, i) => (
                <Grid item classes={{ root: classes.marginLeft }} key={i}>
                  <Grid container spacing={1}>
                    <Grid item>
                      <BlueSphere />
                    </Grid>
                    <Grid item>{item.name}</Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkService = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comment, setComments] = useState("");
  const detailData = dataDetail?.metadata;

  console.warn("detailDataSevice:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Detail Service"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => setComments(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkService;
