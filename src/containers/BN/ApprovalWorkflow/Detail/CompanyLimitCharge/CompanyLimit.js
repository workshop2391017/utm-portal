import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import formatNumber from "helpers/formatNumber";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  const [dataTable, setDataTable] = useState([]);

  useEffect(() => {
    const dataGlobalLimitResultMap = detailData?.globalLimits?.map((item) => ({
      name: item?.serviceCurrencyMatrix?.service?.name,
      child: [
        {
          currMtx: item?.serviceCurrencyMatrix?.currencyMatrix?.name,
          maxTrxDay: item?.maxTransaction,
          currencyCode: item?.currencyCode,
          minTrx: `Rp. ${formatNumber(item?.minAmountTransaction)}`,
          maxTrx: `Rp. ${formatNumber(item?.maxAmountTransaction)}`,
          maxNominalDay: `Rp. ${formatNumber(item?.maxAmountTransactionOfDay)}`,
        },
      ],
    }));

    setDataTable(dataGlobalLimitResultMap);
  }, [detailData]);

  const dataHeader = () => [
    {
      title: "Currency Matrix",
      key: "currMtx",
    },
    {
      title: "Number of Transaction",
      key: "maxTrxDay",
    },
    {
      title: "Currency",
      key: "currencyCode",
    },
    {
      title: "Maximum Transaction",
      key: "maxTrx",
    },
  ];

  const dataDummyTableCheckingAccount = [
    {
      id: 1,
      name: "Transfer Funds",
      child: [
        {
          corporateId: 1,
          currMtx: "Local - Local",
          maxTrxDay: "10",
          minTrx: "Rp 10.000",
          maxTrx: "Rp 10.000.000",
          maxNominalDay: "Rp 1.000.000.000",
        },
        {
          corporateId: 1,
          currMtx: "Local - Forex",
          maxTrxDay: "10",
          minTrx: "Rp 10.000",
          maxTrx: "Rp 10.000.000",
          maxNominalDay: "Rp 1.000.000.000",
        },
        {
          corporateId: 1,
          currMtx: "Local - Forex Same",
          maxTrxDay: "10",
          minTrx: "Rp 10.000",
          maxTrx: "Rp 10.000.000",
          maxNominalDay: "Rp 1.000.000.000",
        },
      ],
    },

    {
      id: 2,
      name: "Fellow Bank",
      child: [
        {
          corporateId: 1,
          currMtx: "Local - Local",
          maxTrxDay: "10",
          minTrx: "Rp 10.000",
          maxTrx: "Rp 10.000.000",
          maxNominalDay: "Rp 1.000.000.000",
        },
        {
          corporateId: 1,
          currMtx: "Local - Forex",
          maxTrxDay: "10",
          minTrx: "Rp 10.000",
          maxTrx: "Rp 10.000.000",
          maxNominalDay: "Rp 1.000.000.000",
        },
        {
          corporateId: 1,
          currMtx: "Local - Forex Same",
          maxTrxDay: "10",
          minTrx: "Rp 10.000",
          maxTrx: "Rp 10.000.000",
          maxNominalDay: "Rp 1.000.000.000",
        },
      ],
    },
  ];

  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          gap: 16,
          marginBottom: 20,
        }}
      >
        <div className={classes.detailTittle}>
          {detailData?.companyLimits?.corporateName}
        </div>
        <div style={{ display: "flex", gap: 16 }}>
          <div className={classes.titleSubtitle}>
            {detailData?.corporateId ?? "-"}
          </div>
          <div
            style={{
              border: "1px solid #0061A7",
              width: "51px",
              height: "26px",
              borderRadius: "5px",
              color: "#0061A7",
              backgroundColor: "#EAF2FF",
              textAlign: "center",
            }}
          >
            {detailData?.segmentation ?? "-"}
          </div>
        </div>
      </div>
      <TableICBBCollapse
        headerContent={dataHeader()}
        dataContent={dataTable}
        // page={page}
        // setPage={setPage}
        // totalData={totalPages}
        // isLoading={isLoading}
        selecable={false} // hide selecable
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown // open collapse default
        handleSelect={(e) => console.warn("EE =>", e)}
        handleSelectAll={(e) => console.warn("HandleSelectAll =>", e)}
        selectedValue={[]}
      />
    </div>
  );
};

const DetailApprovalWorkflowCompanyLimit = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const [comment, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject"));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve"));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Company Limit Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowCompanyLimit;
