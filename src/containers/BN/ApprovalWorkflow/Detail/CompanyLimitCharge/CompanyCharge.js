import React, { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

import { Grid, Typography, makeStyles } from "@material-ui/core";

import Badge from "components/BN/Badge";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import { ReactComponent as CalendarBlue } from "assets/icons/BN/calendarblue.svg";
import { ReactComponent as IconDoc } from "assets/icons/BN/icondoc.svg";

import { formatAmountDot } from "utils/helpers";

import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import { getDataDownloadFile } from "stores/actions/companyCharge";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles((theme) => ({
  corporateId: {
    marginRight: 15,
    fontWeight: 400,
    fontSize: 15,
    fontFamily: "FuturaBQ",
    letterSpacing: "0.03em",
    color: "#7B87AF",
  },
  badge: {
    ...theme.typography.subtitle1,
    gap: 12.5,
    padding: 5,
    borderRadius: 5,
    border: "none",
    letterSpacing: "0.03em",
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F4F7FB",
    color: "#0061A7",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  chargeList: {
    ...theme.typography.h6,
    letterSpacing: "0.01em",
    marginLeft: 20,
    marginBottom: 8,
  },
}));

const dataHeader = [
  {
    title: "Charge Type",
    render: (rowData) => (
      <Typography variant="subtitle1">
        {rowData?.serviceCharge?.charges?.name ?? "-"}
      </Typography>
    ),
  },
  {
    title: "Currency",
    render: (rowData) => (
      <Typography variant="subtitle1">{rowData?.currencyId}</Typography>
    ),
  },
  {
    title: "Fixed Amount",
    render: (rowData) => (
      <Typography variant="subtitle1">
        {rowData?.fee ? formatAmountDot(rowData?.fee) : "-"}
      </Typography>
    ),
  },
  {
    title: "Tiering/Slab",
    render: (rowData) => (
      <Typography variant="subtitle1">
        {rowData?.tiering?.name ?? "-"}
      </Typography>
    ),
  },
];

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const dataListPeriodical = dataDetail?.metadata?.serviceChargePackage?.filter(
    (elm) => elm.service?.name === "Monthly" || elm.service?.name === "Annual"
  );
  const dataListTransactional =
    dataDetail?.metadata?.serviceChargePackage?.filter(
      (elm) => elm.service?.name !== "Monthly" && elm.service?.name !== "Annual"
    );

  const groupDataListPeriodical = dataListPeriodical?.reduce((groups, item) => {
    const service = groups[item.service?.name] || [];
    service.push(item);
    groups[item.service.name] = service;
    return groups;
  }, {});
  const groupDataListTransactional = dataListTransactional?.reduce(
    (groups, item) => {
      const service = groups[item.service?.name] || [];
      service.push(item);
      groups[item.service.name] = service;
      return groups;
    },
    {}
  );

  const resultListPeriodical = [];
  const resultListTransactional = [];

  if (Object.keys(groupDataListPeriodical ?? {})?.length > 0) {
    Object.keys(groupDataListPeriodical).forEach((key) => {
      resultListPeriodical.push({
        name: key,
        child: groupDataListPeriodical[key],
      });
    });
  }
  if (Object.keys(groupDataListTransactional ?? {})?.length > 0) {
    Object.keys(groupDataListTransactional).forEach((key) => {
      resultListTransactional.push({
        name: key,
        child: groupDataListTransactional[key],
      });
    });
  }

  const formatFrom = "YYYY-DD-MM HH:mm:ss";
  const formatTo = "DD/MM/YYYY";

  const startDate = moment(
    dataDetail?.metadata?.companyCharge?.starEffectiveDate
  ).format(formatTo);

  const endDate = moment(
    dataDetail?.metadata?.companyCharge?.endEffectiveDate
  ).format(formatTo);
  const date = `${startDate} - ${endDate}`;
  const undefinedDate =
    startDate === "Invalid date" || endDate === "Invalid date";

  const file = dataDetail?.metadata?.companyCharge?.file;
  const fileName = file?.split("private/kyc/file/")[1];

  const documentDownloadHandler = () => {
    dispatch(getDataDownloadFile(file, fileName));
  };
  return (
    <Fragment>
      <Typography variant="h2" style={{ marginBottom: 15 }}>
        {dataDetail?.metadata?.corporateName}
      </Typography>
      <Grid container alignItems="center" style={{ marginBottom: 15 }}>
        <span className={classes.corporateId}>
          {dataDetail?.metadata?.corporateId ?? "-"}
        </span>
        {dataDetail?.metadata?.segmentation && (
          <Badge
            outline
            type="primary"
            label={dataDetail?.metadata?.segmentation}
          />
        )}
      </Grid>
      <Grid container style={{ marginBottom: 28, gap: 15 }}>
        {!undefinedDate && (
          <div className={classes.badge}>
            <CalendarBlue />
            <span>{date ?? ""}</span>
          </div>
        )}
        {file && (
          <button
            type="button"
            className={classes.badge}
            onClick={documentDownloadHandler}
            style={{ cursor: "pointer" }}
          >
            <IconDoc />
            <span>{fileName}</span>
          </button>
        )}
      </Grid>
      <div className={classes.chargeList}>Periodical Charge List</div>
      <TableICBBCollapse
        headerContent={dataHeader}
        dataContent={resultListPeriodical ?? []}
        selecable={false} // hide selecable
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown // open collapse default
      />

      <div className={classes.chargeList}>Transactional Charge List</div>
      <TableICBBCollapse
        headerContent={dataHeader}
        dataContent={resultListTransactional ?? []}
        selecable={false} // hide selecable
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown // open collapse default
      />
    </Fragment>
  );
};

const DetailApprovalWorkflowCompanyCharge = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Company Charge Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowCompanyCharge;
