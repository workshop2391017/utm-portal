/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import { Grid, makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";
import purify from "dompurify";

import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    display: "flex",
    flexDirection: "column",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  listWrapper: {
    paddingLeft: 20,
  },
});

export const promoMediaOptions = [
  { key: "isSendToPush", value: "Appplication" },
  { key: "isSendToSMS", value: "SMS" },
  { key: "isSendToEmail", value: "Email" },
  { key: "isSendToWhatsapp", value: "WhatsApp" },
];

const dummyDetail = {
  questionId: "<p>How to make a fund transfer</p>",
  questionEn: "<p>Bagaimana cara melakukan transfer pindah dana</p>",
  answerId:
    "<ol><li>Masuk ke aplikasi mobile banking atau internet banking Anda.</li><li>Pilih opsi &quot;Transfer&quot; atau &quot;Pindahkan Dana&quot;.</li><li>Pilih akun sumber dan tujuan.</li><li>Masukkan rincian penerima, jumlah, dan informasi lain yang diminta.</li><li>Verifikasi transaksi dan ikuti petunjuk selanjutnya untuk menyelesaikan proses.</li></ol>",
  answerIEn:
    "<ol> <li>Login to your mobile banking or internet banking application.</li> <li>Select the &quot;Transfer&quot; or &quot;Move Funds&quot; option.</li> <li>Select the source and destination account.</li> <li>Enter beneficiary details, amount, and other requested information.</li> <li>Verify the transaction and follow the next prompts to complete the process.</li></ol>",
};

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>User :</div>
          <div className={classes.valueDetail}>
            {detailData?.category === 1
              ? "Admin Bank"
              : detailData?.category === 0
              ? "Customer"
              : "-"}
          </div>
        </Grid>
      </Grid>
      <Grid container spacing={3} style={{ marginTop: 20 }}>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>FAQ Question (ID) :</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(detailData?.questionIdn),
                }}
              />
            </span>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>FAQ Question (ENG) :</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(detailData?.questionEng),
                }}
              />
            </span>
          </div>
        </Grid>
      </Grid>
      <Grid container spacing={3} style={{ marginTop: 20 }}>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>FAQ Answer (ID) :</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(detailData?.answerIdn),
                }}
              />
            </span>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>FAQ Answer (ENG) :</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(detailData?.answerEng),
                }}
              />
            </span>
          </div>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};
const DetailFaq = () => {
  const [comment, setComments] = useState("");
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Details FAQ"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailFaq;
