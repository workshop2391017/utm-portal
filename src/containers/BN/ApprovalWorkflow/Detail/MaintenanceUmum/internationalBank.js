import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const DetailData = ({ detailData }) => {
  console.warn("detailData:", detailData);
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Bank code :</div>
        <div className={classes.valueDetail}>{detailData?.bankCode || "-"}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Bank Name :</div>
        <div className={classes.valueDetail} style={{ wordWrap: "break-word" }}>
          {detailData?.bankName || "-"}
        </div>
      </Grid>

      <Grid item xs={4}>
        <div className={classes.labelDetail}>Bank Alias Name :</div>
        <div className={classes.valueDetail} style={{ wordWrap: "break-word" }}>
          {detailData?.bankShortName || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Country :</div>
        <div className={classes.valueDetail} style={{ wordWrap: "break-word" }}>
          {detailData?.country || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>City :</div>
        <div className={classes.valueDetail} style={{ wordWrap: "break-word" }}>
          {detailData?.city || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Currency :</div>
        <div className={classes.valueDetail}>
          {detailData?.currencyCode || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>BIC :</div>
        <div className={classes.valueDetail}>
          {detailData?.swiftCode || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Status :</div>
        <div className={classes.valueDetail}>
          {detailData?.deleted ? "Inactive" : "Active" || "-"}
        </div>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowInternationalBank = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="International Bank Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      comment={comment}
      setComment={(e) => {
        setComment(e.target.value);
      }}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowInternationalBank;
