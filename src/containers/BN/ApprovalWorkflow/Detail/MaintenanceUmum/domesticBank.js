import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Bank Name :</div>
        <div className={classes.valueDetail} style={{ wordWrap: "break-word" }}>
          {detailData?.bankName}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Bank Alias Name :</div>
        <div className={classes.valueDetail} style={{ wordWrap: "break-word" }}>
          {detailData?.bankShortName}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Online Switching :</div>
        <div className={classes.valueDetail}>
          {detailData?.bankNetwork === "ALT"
            ? "Alto"
            : detailData?.bankNetwork === "AMP" ||
              detailData?.bankNetwork === "JALIN"
            ? "Jalin"
            : detailData?.bankNetwork === "SAT" ||
              detailData?.bankNetwork === "ARTAJASA"
            ? "Artajasa"
            : detailData?.bankNetwork === "BCA" ||
              detailData?.bankNetwork === "PRIMA"
            ? "Prima"
            : "-"}
        </div>
      </Grid>

      <Grid item xs={4}>
        <div className={classes.labelDetail}>BIC :</div>
        <div className={classes.valueDetail}>
          {detailData?.swiftCode || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Status:</div>
        <div className={classes.valueDetail}>
          {detailData?.deleted ? "Inactive" : "Active" || "-"}
        </div>
      </Grid>
      <Grid item xs={4} />

      <Grid item xs={8}>
        <div className={classes.labelDetail}>Transfer Type :</div>
        <div className={classes.valueDetail}>
          <div style={{ display: "flex", gap: 100 }}>
            {detailData?.transferOnlineAllowed && (
              <div style={{ display: "flex", alignItems: "center" }}>
                <Badge
                  label="Online"
                  type="blue"
                  styleBadge={{ marginRight: 18 }}
                  outline
                />
                <span style={{ marginLeft: "-13px" }}>
                  {detailData?.bankCode}
                </span>
              </div>
            )}

            {detailData?.transferSknAllowed && (
              <div style={{ display: "flex", alignItems: "center" }}>
                <Badge
                  label="SKN"
                  type="orange"
                  styleBadge={{ marginRight: 18 }}
                  outline
                />
                <span style={{ marginLeft: "-13px" }}>
                  {detailData?.sknCode}
                </span>
              </div>
            )}
            {detailData?.transferRtgsAllowed && (
              <div style={{ display: "flex", alignItems: "center" }}>
                <Badge
                  label="RTGS"
                  type="red"
                  styleBadge={{ marginRight: 18 }}
                  outline
                />
                <span style={{ marginLeft: "-13px" }}>
                  {detailData?.rtgsCode}
                </span>
              </div>
            )}

            {detailData?.transferBiFastAllowed && (
              <div style={{ display: "flex", alignItems: "center" }}>
                <Badge
                  label="BI Fast"
                  type="purple"
                  styleBadge={{ marginRight: 18 }}
                  outline
                />
                <span style={{ marginLeft: "-13px" }}>
                  {detailData?.biFastCode}
                </span>
              </div>
            )}
          </div>
        </div>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowDomesticBank = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comment, setComment] = useState("");

  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Domestic Bank Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowDomesticBank;
