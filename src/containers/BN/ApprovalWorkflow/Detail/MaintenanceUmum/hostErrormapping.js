import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  console.warn("detailData:", detailData);
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Error Code:</div>
        <div className={classes.valueDetail}>{detailData?.code}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Source System :</div>
        <div className={classes.valueDetail}>{detailData?.sourceSystem}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Message In English :</div>
        <div className={classes.valueDetail}>{detailData?.engMessage}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Message in Indonesian :</div>
        <div className={classes.valueDetail}>{detailData?.idnMessage}</div>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowhostErrormapping = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const [comment, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Host Error Mapping Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowhostErrormapping;
