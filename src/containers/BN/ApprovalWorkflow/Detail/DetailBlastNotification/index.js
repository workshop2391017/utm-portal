/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import { Grid, makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";

import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    display: "flex",
    flexDirection: "column",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  listWrapper: {
    paddingLeft: 20,
  },
});

export const promoMediaOptions = [
  { key: "isSendToPush", value: "Appplication" },
  { key: "isSendToSMS", value: "SMS" },
  { key: "isSendToEmail", value: "Email" },
  { key: "isSendToWhatsapp", value: "WhatsApp" },
];

const DetailData = ({ detailData }) => {
  const classes = useStyles();
  const [promoMedia, setPromoMedia] = useState([]);
  useEffect(() => {
    if (detailData) {
      const media = [];
      promoMediaOptions.map((itemPromo) => {
        Object.entries(detailData).map(([key, value]) => {
          if (key === itemPromo.key && value) {
            media.push(itemPromo.value);
          }
        });
      });
      setPromoMedia(media);
    }
  }, [detailData]);

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promotion Title :</div>
          <div className={classes.valueDetail}>{detailData?.title}</div>
        </Grid>
      </Grid>
      <Grid container spacing={3} style={{ marginTop: 20 }}>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>Blast Date:</div>
          <div className={classes.valueDetail}>
            <Grid container spacing={2} className={classes.listWrapper}>
              {detailData?.scheduleDate.map((item) => (
                <Grid item xs={6}>
                  <Grid container spacing={1} alignItems="center">
                    <Grid item>
                      <div className={classes.bullets} />
                    </Grid>
                    <Grid item>{item}</Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>Promotional Media:</div>
          <div className={classes.valueDetail}>
            <Grid container spacing={2} className={classes.listWrapper}>
              {promoMedia.map((item) => (
                <Grid item xs={6}>
                  <Grid container spacing={1} alignItems="center">
                    <Grid item>
                      <div className={classes.bullets} />
                    </Grid>
                    <Grid item>{item}</Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </div>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};
const DetailBlastNotification = (props) => {
  const [comment, setComments] = useState("");
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Blast Promo Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailBlastNotification;
