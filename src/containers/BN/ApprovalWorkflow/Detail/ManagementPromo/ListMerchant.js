import React, { useState } from "react";
import PropTypes from "prop-types";
import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import TableICBB from "components/BN/TableIcBB";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    display: "flex",
    flexDirection: "column",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
});
const dataDummy = [
  {
    id: "197329",
    typeMerchant: "E-commerce",
    nameMerchant: "OpenStore",
    city: "Cimahi",
  },
  {
    id: "197329",
    typeMerchant: "E-commerce",
    nameMerchant: "OpenStore",
    city: "Cimahi",
  },
  {
    id: "197329",
    typeMerchant: "E-commerce",
    nameMerchant: "OpenStore",
    city: "Cimahi",
  },
  {
    id: "197329",
    typeMerchant: "E-commerce",
    nameMerchant: "OpenStore",
    city: "Cimahi",
  },
  {
    id: "197329",
    typeMerchant: "E-commerce",
    nameMerchant: "OpenStore",
    city: "Cimahi",
  },
];
const dataHeader = () => [
  {
    title: "Merchant ID",
    key: "id",
    render: (rowData) => (
      <span
        style={{
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          color: "#44495B",
          fontSize: 13,
          letterSpacing: "0.01em",
        }}
      >
        {rowData.id}
      </span>
    ),
  },
  {
    title: "Merchant Type",
    key: "typeMerchant",
    render: (rowData) => (
      <span
        style={{
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          color: "#44495B",
          fontSize: 13,
          letterSpacing: "0.01em",
        }}
      >
        {rowData.typeMerchant}
      </span>
    ),
  },
  {
    title: "Merchant Name",
    key: "nameMerchant",
    render: (rowData) => (
      <span
        style={{
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          color: "#44495B",
          fontSize: 13,
          letterSpacing: "0.01em",
        }}
      >
        {rowData.nameMerchant}
      </span>
    ),
  },
  {
    title: "City",
    key: "city",
    render: (rowData) => (
      <span
        style={{
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          color: "#44495B",
          fontSize: 13,
          letterSpacing: "0.01em",
        }}
      >
        {rowData.city}
      </span>
    ),
  },
];
const DetailData = ({ detailData }) => {
  console.warn("detailData:", detailData);
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TableICBB
            scroll
            scrollHeight={250}
            dataContent={dataDummy ?? []}
            headerContent={dataHeader() ?? []}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};
const DetailApprovalWorkflowListMerchant = (props) => {
  const [comment, setComments] = useState("");
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;

  console.warn("detailData:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Merchant List Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowListMerchant;
