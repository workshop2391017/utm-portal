/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import purify from "dompurify";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import moment from "moment";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    display: "flex",
    flexDirection: "column",
    wordWrap: "break-word",
  },
  valueDetailFile: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.primary.hard,
    display: "flex",
    flexDirection: "column",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
});

function RenderImage({ base64 }) {
  function goToImage() {
    const image = new Image();
    image.src = base64.image || base64.fileName;
    // ? minio udah bisa diconsume belum?
    // ? caranya gini?
    // console.log("+++ base64", base64);
    const w = window.open("");
    w.document.write(image.outerHTML);
  }
  return (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a
      href={
        base64.fileName.includes("digitalbanking/public/promo/")
          ? base64.fileName
          : "#"
      }
      target="_blank"
      rel="noreferrer"
      onClick={(e) => {
        e.preventDefault();
        goToImage();
      }}
    >
      {base64.fileName.includes("digitalbanking/public/promo/")
        ? base64.fileName.split("digitalbanking/public/promo/")[1]
        : base64.fileName}
    </a>
  );
}

function renderRecommend(arr) {
  const result = [];
  arr.map((item) => {
    if (item.value) {
      result.push(item.lable);
    }
  });
  return result;
}

const DetailData = ({ detailData }) => {
  // console.warn("detailData:", detailData);
  const classes = useStyles();
  const [mappingValue, setMappingValue] = useState(null);
  useEffect(() => {
    if (detailData) {
      const dataSet = {
        promoSubject: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.promotionSubject
          : detailData?.metadata?.promoSubject,
        promoCode: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.promotionCode
          : detailData?.metadata?.promoCode,
        promoLink: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.promotionLink
          : detailData?.metadata?.promoLink,
        base64PromoImage: detailData?.metadata?.isDeleted
          ? {
              image: null,
              fileName: detailData?.additionalData?.promotionImageLink,
            }
          : detailData?.metadata?.base64PromoImage,
        base64PromoImage2: detailData?.metadata?.isDeleted
          ? {
              image: null,
              fileName: detailData?.additionalData?.promotionImageLink2,
            }
          : detailData?.metadata?.base64PromoImage2,
        base64PromoImage3: detailData?.metadata?.isDeleted
          ? {
              image: null,
              fileName: detailData?.additionalData?.promotionImageLink3,
            }
          : detailData?.metadata?.base64PromoImage3,
        base64PromoImage4: detailData?.metadata?.isDeleted
          ? {
              fimage: null,
              fileName: detailData?.additionalData?.promotionImageLink4,
            }
          : detailData?.metadata?.base64PromoImage4,
        startDate: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.startDate
          : detailData?.metadata?.startDate,
        endDate: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.endDate
          : detailData?.metadata?.endDate,
        isBeforeLogin: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.isBeforeLogin
          : detailData?.metadata?.isBeforeLogin,
        isAfterLogin: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.isAfterLogin
          : detailData?.metadata?.isAfterLogin,
        tncId: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.termsAndConditions
          : detailData?.metadata?.tncId,
        tncEn: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.termsAndConditionsEn
          : detailData?.metadata?.tncEn,
        descriptionId: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.description
          : detailData?.metadata?.descriptionId,
        descriptionEn: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.descriptionEn
          : detailData?.metadata?.descriptionEn,
        segmentation: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.segmentation
          : detailData?.metadata?.segmentation,
        promoCategory: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.promotionCategory
          : detailData?.metadata?.promoCategory,
        location: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.location
          : detailData?.metadata?.location,
        merchant: detailData?.metadata?.isDeleted
          ? detailData?.additionalData?.merchant
          : detailData?.metadata?.merchant,
      };
      setMappingValue(dataSet);
    }
  }, [detailData]);

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Name:</div>
          <div className={classes.valueDetail}>
            {mappingValue?.promoSubject}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Code :</div>
          <div className={classes.valueDetail}>{mappingValue?.promoCode}</div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Links:</div>
          <div className={classes.valueDetail}>{mappingValue?.promoLink}</div>
        </Grid>
      </Grid>
      <Grid container style={{ marginTop: "32px" }} spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Image (676 x 364) :</div>
          <div className={classes.valueDetailFile}>
            {mappingValue?.base64PromoImage ? (
              <RenderImage base64={mappingValue?.base64PromoImage} />
            ) : (
              "-"
            )}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Image (636 x 136) :</div>
          <div className={classes.valueDetailFile}>
            {mappingValue?.base64PromoImage2 ? (
              <RenderImage base64={mappingValue?.base64PromoImage2} />
            ) : (
              "-"
            )}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Image (90 x 112) :</div>
          <div className={classes.valueDetailFile}>
            {mappingValue?.base64PromoImage3 ? (
              <RenderImage base64={mappingValue?.base64PromoImage3} />
            ) : (
              "-"
            )}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Promo Image (676 x 364) :</div>
          <div className={classes.valueDetailFile}>
            {mappingValue?.base64PromoImage4 ? (
              <RenderImage base64={mappingValue?.base64PromoImage4} />
            ) : (
              "-"
            )}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Start Date:</div>
          <div className={classes.valueDetail}>
            {mappingValue?.startDate
              ? moment(mappingValue?.startDate).format("DD/MM/YYYY HH:mm:ss")
              : ""}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Expiration Date :</div>
          <div className={classes.valueDetail}>
            {mappingValue?.endDate
              ? moment(mappingValue?.endDate).format("DD/MM/YYYY HH:mm:ss")
              : ""}
          </div>
        </Grid>
      </Grid>
      <Grid container style={{ marginTop: "32px" }} spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Recommendation :</div>
          <div className={classes.valueDetail}>
            {renderRecommend([
              {
                lable: "Before Login",
                value: mappingValue?.isBeforeLogin,
              },
              {
                lable: "After Login",
                value: mappingValue?.isAfterLogin,
              },
            ]).map((item, index) => {
              switch (index) {
                case 1:
                  return ` & ${item}`;
                default:
                  return item;
              }
            })}
          </div>
        </Grid>
      </Grid>
      <Grid container style={{ marginTop: "32px" }} spacing={3}>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>T&C ID :</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(mappingValue?.tncId || ""),
                }}
              />
            </span>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>T&C EN:</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(mappingValue?.tncEn || ""),
                }}
              />
            </span>
          </div>
        </Grid>
      </Grid>
      <Grid container style={{ marginTop: "32px" }} spacing={3}>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>Promo Description ID :</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(mappingValue?.descriptionId || ""),
                }}
              />
            </span>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.labelDetail}>Promo Description EN:</div>
          <div className={classes.valueDetail}>
            <span>
              <div
                dangerouslySetInnerHTML={{
                  __html: purify.sanitize(mappingValue?.descriptionEn || ""),
                }}
              />
            </span>
          </div>
        </Grid>
      </Grid>
      <Grid container style={{ marginTop: "32px" }} spacing={3}>
        <Grid
          style={{ display: "flex", flexDirection: "column", gap: "20px" }}
          item
          xs={6}
        >
          <div>
            <div className={classes.labelDetail}>Segmentation :</div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "auto",
              }}
            >
              <div style={{ display: "flex", flexDirection: "column" }}>
                {mappingValue?.segmentation?.map((e, i) => (
                  <div className={classes.listGroup} key={i}>
                    <div className={classes.bullets} />
                    {e.name ?? "-"}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Grid>
        <Grid
          style={{ display: "flex", flexDirection: "column", gap: "20px" }}
          item
          xs={6}
        >
          <div>
            <div className={classes.labelDetail}>Promo Category :</div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "auto",
              }}
            >
              <div style={{ display: "flex", flexDirection: "column" }}>
                {mappingValue?.promoCategory?.map((e, i) => (
                  <div className={classes.listGroup} key={i}>
                    <div className={classes.bullets} />
                    {detailData?.metadata?.isDeleted
                      ? e?.promoCategory?.name ?? "-"
                      : e?.name ?? "-"}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Grid>
        <Grid
          style={{ display: "flex", flexDirection: "column", gap: "20px" }}
          item
          xs={6}
        >
          <div>
            <div className={classes.labelDetail}>Locations :</div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "auto",
              }}
            >
              <div style={{ display: "flex", flexDirection: "column" }}>
                {mappingValue?.location?.length > 0 ? (
                  <React.Fragment>
                    {mappingValue?.location?.map((e, i) => (
                      <div className={classes.listGroup} key={i}>
                        <div className={classes.bullets} />
                        {e ?? "-"}
                      </div>
                    ))}
                  </React.Fragment>
                ) : (
                  "-"
                )}
              </div>
            </div>
          </div>
        </Grid>
        <Grid
          style={{ display: "flex", flexDirection: "column", gap: "20px" }}
          item
          xs={6}
        >
          <div>
            <div className={classes.labelDetail}>Merchants :</div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "auto",
              }}
            >
              <div style={{ display: "flex", flexDirection: "column" }}>
                {mappingValue?.merchant?.map((e, i) => (
                  <div className={classes.listGroup} key={i}>
                    <div className={classes.bullets} />
                    {e.name ?? "-"}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};
const DetailApprovalWorkflowListPromo = (props) => {
  const [comment, setComments] = useState("");
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Promo List Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={dataDetail} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowListPromo;
