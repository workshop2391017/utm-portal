import React, { useState } from "react";
import PropTypes from "prop-types";
import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    display: "flex",
    flexDirection: "column",
  },
  listGroup: {
    display: "flex",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
});

const DetailData = ({ detailData }) => {
  console.warn("detailData:", detailData);
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Category :</div>
          <div className={classes.valueDetail}>
            {detailData?.chargesList?.name}
          </div>
        </Grid>
        <Grid item xs={4}>
          <Grid style={{ display: "flex", gap: 90 }} item xs={6}>
            <div>
              <div className={classes.labelDetail}>Segmentation :</div>
              <div
                style={{
                  display: "flex",
                  gap: 50,
                  width: "auto",
                }}
              >
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    Platinum
                  </div>
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    Gold
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    Platinum
                  </div>
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    Gold
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    Platinum
                  </div>
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    Gold
                  </div>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};
const DetailApprovalWorkflowJenisPromo = (props) => {
  const [comment, setComments] = useState("");
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;

  console.warn("detailData:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Promo List Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowJenisPromo;
