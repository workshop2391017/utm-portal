import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
});

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Charge List :</div>
        <div className={classes.valueDetail}>
          {detailData?.chargesList?.name}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Flag Charge List :</div>
        <div className={classes.valueDetail}>
          {detailData?.chargesList?.type === "TRF_FEE"
            ? "Transfer Fee"
            : "Admin Fee"}
        </div>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowChargeList = () => {
  const [comment, setComments] = useState("");
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Charge List Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowChargeList;
