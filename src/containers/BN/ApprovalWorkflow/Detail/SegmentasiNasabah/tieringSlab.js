import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Grid } from "@material-ui/core";
import _ from "lodash";

import { formatAmountDot } from "utils/helpers/";

import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";

import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";
import { useStyles } from "./styleTieringSlab";

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData, additionalData }) => {
  const classes = useStyles();

  console.warn("detailData:", detailData);
  console.warn("additionalData:", additionalData);

  const [dataTable, setDataTable] = useState({});
  const [typeOfTier, setTypeOfTier] = useState(null);

  let dataHeader;
  const dataHeaderPicker = () => {
    dataHeader = [
      {
        title: "Number Of Transaction",
        key: "numOfTrx",
      },
      {
        title: "Transaction Amount",
        key: "trxAmount",
      },
      {
        title: "Charge / Transaction (Rp)",
        key: "chargeTrx",
      },
    ];
    if (typeOfTier === "Transaction Amount") {
      dataHeader = dataHeader.filter((el, i) => i !== 0);
    }
    if (typeOfTier === "Number of Transaction") {
      dataHeader = dataHeader.filter((el, i) => i !== 1);
    }
    return dataHeader;
  };

  useEffect(() => {
    const resultMapping = additionalData?.tieringAmountList?.map((item) => ({
      ...item,
      numOfTrx:
        item.numberOfTransaction === undefined
          ? "-"
          : `${
              item?.isGreaterNumberOfTransaction &&
              item?.isEqualNumberOfTransaction
                ? ">= "
                : item?.isGreaterNumberOfTransaction &&
                  !item?.isEqualNumberOfTransaction
                ? "> "
                : !item?.isGreaterNumberOfTransaction &&
                  item?.isEqualNumberOfTransaction
                ? "<= "
                : !item?.isGreaterNumberOfTransaction &&
                  !item?.isEqualNumberOfTransaction
                ? "< "
                : null
            }${formatAmountDot(parseInt(item?.numberOfTransaction))}`,
      trxAmount:
        item.transactionAmount === undefined
          ? "-"
          : `${
              item?.isGreaterTransactionAmount && item?.isEqualTransactionAmount
                ? ">= "
                : item?.isGreaterTransactionAmount &&
                  !item?.isEqualTransactionAmount
                ? "> "
                : !item?.isGreaterTransactionAmount &&
                  item?.isEqualTransactionAmount
                ? "<= "
                : !item?.isGreaterTransactionAmount &&
                  !item?.isEqualTransactionAmount
                ? "< "
                : null
            }${formatAmountDot(parseInt(item?.transactionAmount))}`,
      chargeTrx:
        item?.charge === 0 ? "0" : formatAmountDot(parseInt(item?.charge)),
    }));
    console.warn("ini resultMapping :", resultMapping);
    if (!resultMapping) {
      setTypeOfTier(null);
    } else if (
      resultMapping[0]?.trxAmount !== "-" &&
      resultMapping[0]?.numOfTrx !== "-"
    ) {
      setTypeOfTier("All Types of Tiers");
    } else if (resultMapping[0]?.trxAmount === "-") {
      setTypeOfTier("Number of Transaction");
    } else if (resultMapping[0]?.numOfTrx === "-") {
      setTypeOfTier("Transaction Amount");
    }

    const dataResult = {
      ...additionalData,
      tieringAmountList: resultMapping,
    };

    setDataTable(dataResult);
  }, [additionalData]);

  console.warn("dataTable:", dataTable);

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 20 }}>
        <div className={classes.detailTittle}>{dataTable?.tieringName}</div>
        <div className={classes.titleSubtitle}>{dataTable?.tieringCode}</div>
      </div>
      {typeOfTier !== null && <h1 className={classes.styleH1}>{typeOfTier}</h1>}
      <TableICBB
        headerContent={dataHeaderPicker()}
        dataContent={dataTable?.tieringAmountList ?? []}
      />
    </div>
  );
};

const DetailApprovalWorkflowTieringSlab = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const additionalData = dataDetail?.additionalData;
  const [comment, setComment] = useState("");

  console.warn("detailData:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Tiering/Slab Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData detailData={detailData} additionalData={additionalData} />
      }
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowTieringSlab;
