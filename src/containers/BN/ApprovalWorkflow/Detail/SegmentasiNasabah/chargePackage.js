import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import formatNumber from "helpers/formatNumber";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    marginLeft: 15,
  },
  tableTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
    fontWeight: 900,
    margin: "10px 10px 10px 20px",
  },
});

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  const dataHeader = () => [
    {
      title: "Charge Type",
      key: "chargeName",
    },
    {
      title: "Currency",
      key: "currencyId",
    },
    {
      title: "Fixed Amount",
      key: "fee",
      render: (e) =>
        e.fee >= 0 && e.fee !== null && e.tieringName === null
          ? formatNumber(e.fee)
          : "-",
    },
    {
      title: "Tiering/Slab",
      key: "tieringName",
      render: (e) => e.tieringName || "-",
    },
  ];

  // grouping
  const handleGrouping = (payload) => {
    const arr = [];

    payload.forEach((elm) => {
      const fIdx = arr.findIndex((e) => e.serviceId === elm.serviceId);

      if (fIdx >= 0) {
        arr[fIdx].child.push(elm);
      } else {
        const assign = { ...elm, child: [] };
        assign.child.push(elm);
        arr.push(assign);
      }
    });

    return arr;
  };

  const periodical = handleGrouping(
    (
      detailData?.serviceChargePackageMeta?.filter((e) =>
        e?.chargeGroup?.includes("TRANSACTIONAL GROUP")
      ) || []
    ).map((elm) => ({
      ...elm,
      name: elm?.serviceName,
    }))
  );

  const transactional = handleGrouping(
    (
      detailData?.serviceChargePackageMeta?.filter((e) =>
        e?.chargeGroup?.includes("PERIODICAL GROUP")
      ) || []
    ).map((elm) => ({
      ...elm,
      name: elm?.serviceName,
    }))
  );

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 20 }}>
        <div className={classes.detailTittle}>
          {detailData?.chargePackage?.name}
        </div>
        <div className={classes.titleSubtitle}>
          {detailData?.chargePackage?.code}
        </div>
      </div>
      <div className={classes.tableTitle}>Periodical Charge List</div>
      <TableICBBCollapse
        headerContent={dataHeader()}
        dataContent={transactional}
        selecable={false}
        collapse={false}
        defaultOpenCollapseBreakdown
      />

      <div className={classes.tableTitle}>Transactional Charge List</div>
      <TableICBBCollapse
        headerContent={dataHeader()}
        dataContent={periodical}
        selecable={false}
        collapse={false}
        defaultOpenCollapseBreakdown
      />
    </div>
  );
};

const DetailApprovalWorkflowChargePackage = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const detailData = dataDetail?.metadata;
  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Charge Package Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowChargePackage;
