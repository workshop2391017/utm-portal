import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import formatNumber from "helpers/formatNumber";
import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    overflowWrap: "anywhere",
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.medium,
    margin: "20px 0",
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  subTitle: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.medium,
    fontSize: 13,
    marginBottom: 10,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ additionalData }) => {
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.subTitle}>Type :</div>
          <div className={classes.valueDetail}>{additionalData?.type}</div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.subTitle}>Segmentation :</div>
          <div className={classes.valueDetail}>
            {additionalData?.segmentation?.name}
          </div>
        </Grid>
      </Grid>
      <div className={classes.detailTitle}>Parameter</div>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.subTitle}>Average balance :</div>
          <div className={classes.valueDetail}>
            {formatNumber(additionalData?.averageBalance)}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.subTitle}>Age :</div>
          <div className={classes.valueDetail}>
            {additionalData?.age1} - {additionalData?.age2} years old
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.subTitle}>Position :</div>
          <div className={classes.valueDetail}>{additionalData?.division}</div>
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} className={classes.border}>
          <div className={classes.subTitle}>Business fields :</div>
          <Grid container spacing={3} style={{ marginTop: 9 }}>
            {additionalData?.businessFieldList?.map((item) => (
              <Grid
                item
                xs={6}
                style={{ margin: 0, marginBottom: 18, padding: 0 }}
              >
                <div style={{ display: "inline-block" }}>
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    {item?.name}
                  </div>
                </div>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

const DetailApprovalWorkflowTypeParameter = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComments] = useState("");

  const detailData = dataDetail?.metadata;
  const additionalData = dataDetail?.additionalData;

  console.warn("detailData:", detailData);
  console.warn("additionalData:", additionalData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Parameter Type Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData detailData={detailData} additionalData={additionalData} />
      }
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowTypeParameter;
