import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import ScrollCustom from "components/BN/ScrollCustom";
import Colors from "helpers/colors";
import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    overflowWrap: "anywhere",
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  console.warn("detailData:", detailData);

  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDetail}> Segmentation Name : </div>
        <div className={classes.valueDetail}>
          {detailData?.name ? detailData?.name : "-"}
        </div>
      </Grid>

      <Grid item xs={4}>
        <div className={classes.labelDetail}>Segmentation Description :</div>
        <div className={classes.valueDetail}>
          {detailData?.description ? detailData?.description : "-"}
        </div>
      </Grid>

      <Grid item xs={4}>
        <div className={classes.labelDetail} />
        <div className={classes.valueDetail} />
      </Grid>

      <Grid item xs={4}>
        <div className={classes.labelDetail}>Account Type :</div>
        <div className={classes.valueDetail}>
          {detailData?.jenisProductId?.name
            ? detailData?.jenisProductId?.name
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Limit Package :</div>
        <div className={classes.valueDetail}>
          {detailData?.limitPackageId?.name
            ? detailData?.limitPackageId?.name
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Menu Package :</div>
        <div className={classes.valueDetail}>
          {detailData?.menuPackageId?.name
            ? detailData?.menuPackageId?.name
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Charge Packages :</div>
        <div className={classes.valueDetail}>
          {detailData?.chargePackageId?.name
            ? detailData?.chargePackageId?.name
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Special Menu :</div>
        <div className={classes.valueDetail}>
          {detailData?.menuKhususId?.name
            ? detailData?.menuKhususId?.name
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Branch Segment :</div>
        <div className={classes.valueDetail}>
          {detailData?.segmentBranchId?.name
            ? detailData?.segmentBranchId?.name
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Advanced Parameters :</div>

        <ScrollCustom
          height="300px"
          width="200px"
          scroll={detailData?.saveParameterAdvance?.length > 10}
        >
          {detailData?.saveParameterAdvance?.length > 0 ? (
            <React.Fragment>
              {detailData?.saveParameterAdvance
                ?.filter((e) => !e?.isDeleted)
                .map((el, i) => (
                  <div key={i} className={classes.valueDetail}>
                    {el?.accountNumber}
                  </div>
                ))}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className={classes.valueDetail}>-</div>
            </React.Fragment>
          )}
        </ScrollCustom>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowDaftarSegmentasi = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const [comment, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Segmentation List Change Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowDaftarSegmentasi;
