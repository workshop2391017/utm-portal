import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import { el } from "date-fns/locale";
import Colors from "helpers/colors";
import React, { useEffect, useMemo, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    marginLeft: 15,
  },
});

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();

  // const mapping = useMemo(() => {
  //   const obj = {};

  //   dataDetail?.additionalData?.accountProductList?.forEach((elm) => {
  //     const type = elm?.accountType;
  //     obj[type] = { name: type === "SA" ? "Tabungan" : "Giro", child: [] };
  //     elm?.accountProducts?.forEach((child) => obj[type].child.push(child));
  //   });

  //   return obj;
  // }, [dataDetail]);

  const mapping = useMemo(() => {
    const obj = {};

    dataDetail?.metadata?.forEach((elm) => {
      const type = elm?.accountProduct?.accountType;
      obj[type] = {
        name:
          type === "SA"
            ? "Savings"
            : type === "CA"
            ? "Current Account"
            : type === "LN"
            ? "Loan Pinjaman"
            : type === "CD"
            ? "Deposito"
            : null,
        child: [],
      };
      dataDetail?.metadata?.forEach((child) => {
        if (type === child?.accountProduct?.accountType) {
          obj[type]?.child?.push(child?.accountProduct);
        }
      });
    });

    return obj;
  }, [dataDetail]);

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 20 }}>
        <div className={classes.detailTittle}>
          {dataDetail?.additionalData?.productTypeName}
        </div>
        <div className={classes.titleSubtitle}>
          {dataDetail?.additionalData?.productTypeCode}
        </div>
      </div>
      <Grid container spacing={3}>
        {Object.keys(mapping)
          .reverse()
          .map((key) => (
            <Grid item xs={12}>
              <div className={classes.detailTitle}>{mapping[key]?.name}</div>
              <Grid container spacing={3}>
                {mapping[key]?.child?.map((elm) => (
                  <Grid item xs={6}>
                    <div style={{ display: "inline-block" }}>
                      <div className={classes.listGroup}>
                        <div className={classes.bullets} />
                        <div>{`${elm?.productName} - ${
                          elm?.productCode ?? ""
                        }`}</div>
                      </div>
                    </div>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

const DetailApprovalWorkflowJenisRekeningProduk = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Product Account Type Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowJenisRekeningProduk;
