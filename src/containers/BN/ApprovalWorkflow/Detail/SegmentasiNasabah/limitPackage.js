import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";

import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import formatNumber from "helpers/formatNumber";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    marginLeft: 15,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const getGroupingData = (dataCompare, compareKey, dataArray) =>
  dataArray.filter((item) => dataCompare === item[compareKey]);

const DetailData = ({ detailData, additionalData }) => {
  const classes = useStyles();

  const [dataTable, setDataTable] = useState([]);

  useEffect(() => {
    const res = additionalData?.globalLimit?.map((item) => ({
      name: item.serviceName,
      child: getGroupingData(
        item.serviceName,
        "serviceName",
        additionalData?.globalLimit
      )?.map((val) => ({
        currMtx: val?.serviceCurrencyMatrixName,
        maxTrxDay: val?.maxTransaction,
        minTrx: `${formatNumber(val?.minAmountTransaction)}`,

        maxTrx: `${formatNumber(val?.maxAmountTransaction)}`,
        maxNominalDay: `${formatNumber(val?.maxAmountTransactionOfDay)}`,
        currency: `${val.currencyCode}`,
      })),
    }));
    setDataTable(res);
  }, [additionalData]);

  const dataHeader = () => [
    {
      title: "Currency Matrix",
      key: "currMtx",
    },
    {
      title: "Currency",
      key: "currency",
    },
    {
      title: "Number of Transaction",
      key: "maxTrxDay",
    },
    {
      title: "Minimum Transaction Amount",
      key: "minTrx",
      width: 250,
    },
    {
      title: "Maximum Nominal Transaction",
      key: "maxTrx",
      width: 250,
    },
    {
      title: "Maximum Nominal / Day",
      key: "maxNominalDay",
      width: 250,
    },
  ];

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 20 }}>
        <div className={classes.detailTittle}>
          {additionalData?.limitPackage?.name}
        </div>
        <div className={classes.titleSubtitle}>
          {additionalData?.limitPackage?.code}
        </div>
      </div>
      <TableICBBCollapse
        headerContent={dataHeader()}
        dataContent={dataTable}
        // page={page}
        // setPage={setPage}
        // totalData={totalPages}
        // isLoading={isLoading}
        selecable={false} // hide selecable
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown // open collapse default
        handleSelect={(e) => console.warn("EE =>", e)}
        handleSelectAll={(e) => console.warn("HandleSelectAll =>", e)}
        selectedValue={[]}
      />
    </div>
  );
};

const DetailApprovalWorkflowLimitPackage = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const detailData = dataDetail?.metadata;
  const additionalData = dataDetail?.additionalData;

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Package Limit Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData detailData={detailData} additionalData={additionalData} />
      }
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowLimitPackage;
