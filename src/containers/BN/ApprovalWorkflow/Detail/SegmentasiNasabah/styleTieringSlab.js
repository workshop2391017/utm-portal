import { makeStyles } from "@material-ui/core";

import Colors from "helpers/colors";

export const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    marginLeft: 15,
  },
  styleH1: {
    width: "auto",
    height: "20px",
    marginBottom: "20px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "17px",
    lineHeight: "20px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
});
