import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  DetailDataTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    marginLeft: 15,
  },
  border: {
    padding: 0,
    margin: 0,
    boxShadow: "0px 2px 0px #EAF2FF",
    marginBottom: 20,
    paddingBottom: 20,
  },
});

const DetailData = ({ additionalData }) => {
  const classes = useStyles();

  console.warn("additionalData:", additionalData);
  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 20 }}>
        <div className={classes.DetailDataTitle}>
          {additionalData?.segmentBranch?.name}
        </div>
        <div className={classes.titleSubtitle}>
          {additionalData?.segmentBranch?.code}
        </div>
      </div>

      <Grid container spacing={3}>
        {additionalData?.branch?.map((item) => (
          <Grid item xs={12} className={classes.border}>
            <div className={classes.detailTitle}>{item?.name}</div>
            <Grid container spacing={3} style={{ marginTop: 9 }}>
              {item?.child?.map((val) => (
                <Grid
                  item
                  xs={6}
                  style={{ margin: 0, marginBottom: 18, padding: 0 }}
                >
                  <div style={{ display: "inline-block" }}>
                    <div className={classes.listGroup}>
                      <div className={classes.bullets} />
                      {val?.name}
                    </div>
                  </div>
                </Grid>
              ))}
            </Grid>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

const DetailApprovalWorkflowSegmenCabang = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const additionalData = dataDetail?.additionalData;
  const [comment, setComment] = useState("");

  console.warn("detailData:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Branch Segment Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData additionalData={additionalData} />}
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowSegmenCabang;
