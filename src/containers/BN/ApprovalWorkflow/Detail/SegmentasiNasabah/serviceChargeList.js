import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  listGroup: {
    display: "flex",
    // justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
    marginLeft: 20,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  subTitle: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.medium,
    fontSize: 13,
    marginBottom: 10,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.subTitle}>List of Charge List :</div>
      <Grid container spacing={3}>
        {(detailData?.serviceCharge ?? []).map((elm) => (
          <Grid item xs={4}>
            <div className={classes.detailTitle}>{elm?.serviceId?.name}</div>

            <div style={{ display: "inline-block" }}>
              {elm?.chargesList?.map((cl) => (
                <div className={classes.listGroup}>
                  <div className={classes.bullets} />
                  {cl.chargeName}
                </div>
              ))}
            </div>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

const DetailApprovalWorkflowServiceChargeList = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const [comment, setComment] = useState("");

  console.warn("detailData:", detailData);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Service Charge List Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowServiceChargeList;
