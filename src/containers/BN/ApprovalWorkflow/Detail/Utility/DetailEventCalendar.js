import { makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";
import moment from "moment";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
  },
  tableTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
    fontWeight: 900,
    margin: "10px 10px 10px 20px",
  },
  wrapper: {
    display: "flex",
    gap: 50,
    width: "100%",
  },
  textWrap: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
    width: "33%",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: 13,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 15,
    color: Colors.dark.hard,
  },
  container: {
    display: "flex",
    gap: 20,
    flexDirection: "column",
  },
});

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Event Date :</p>
          <p className={classes.value}>
            {dataDetail?.metadata?.isRegularEvent ? (
              <React.Fragment>
                {moment(dataDetail?.metadata?.startTime).format("DD/MM/YYYY")} -{" "}
                {moment(dataDetail?.metadata?.endTime).format("DD/MM/YYYY")}
              </React.Fragment>
            ) : (
              <React.Fragment>
                {moment(dataDetail?.metadata?.eventDate).format("DD/MM/YYYY")}
              </React.Fragment>
            )}
          </p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Event Name :</p>
          <p className={classes.value}>{dataDetail?.metadata?.name}</p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Category :</p>
          <p className={classes.value}>{dataDetail?.metadata?.type}</p>
        </div>
      </div>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Regular Events:</p>
          <p className={classes.value}>
            {dataDetail?.metadata?.isRegularEvent ? "Yes" : "No"}
          </p>
        </div>
      </div>
    </div>
  );
};

const EventCalendar = () => {
  const dispatch = useDispatch();
  const [comment, setComment] = useState("");
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Event Calendar Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default EventCalendar;
