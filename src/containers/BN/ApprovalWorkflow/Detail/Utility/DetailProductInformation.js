/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { makeStyles } from "@material-ui/core";

import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";

import Colors from "helpers/colors";
import { formatAmountDot } from "utils/helpers";

import PreviewImage from "components/BN/Popups/PreviewImage";
import blueSphere from "assets/icons/BN/blue-sphere.svg";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";
import { Currency } from "../../../../../helpers/currency";

const useStyles = makeStyles({
  list: {
    "& li:not(:last-child)": { marginBottom: 8 },
    listStyleImage: `url(${blueSphere})`,
  },
  listItem: {
    paddingLeft: 8,
    fontSize: 15,
  },
  wrapper: {
    display: "flex",
    gap: 50,
    width: "100%",
  },
  textWrap: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
    width: "33%",
  },
  textWrap50: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
    width: "50%",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: 13,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 15,
    color: Colors.dark.hard,
  },
  container: {
    display: "flex",
    gap: 20,
    flexDirection: "column",
  },
  link: {
    color: "#0061A7",
    cursor: "pointer",
  },
});

const getSavingType = (type) => {
  switch (type) {
    case 0:
      return "Transactional";
    case 3:
      return "Investment";
    case 2:
      return "Futures";
    case 1:
      return "Cardless";
    case 4:
      return "Program (Investment)";

    default:
      return "-";
  }
};

const getAccountType = (type) => {
  switch (type) {
    case "CA":
      return "Current Account";
    case "SA":
      return "Saving Account";
    case "LN":
      return "Loan Account";
    case "CD":
      return "Deposit Account";

    default:
      return "-";
  }
};

const getDepositType = (type) => {
  switch (type) {
    case 1:
      return "eDeposito";
    case 3:
      return "Deposito Valas";
    case 2:
      return "Deposito";

    default:
      return "-";
  }
};

const getYear = (date) => date.split("-")[0];

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();
  const [modal, setModal] = useState(false);

  const metadata = dataDetail?.metadata;

  const allowedFor = [
    {
      value: metadata?.openAccountAllowed,
      label: "Open Second Account",
    },
    {
      value: metadata?.sofEdeposito,
      label: "SOF e-Deposito",
    },
    {
      value: metadata?.isRegisterAllowed,
      label: "Registration",
    },
    {
      value: metadata?.isValasAllowed,
      label: "Valas",
    },
    {
      value: metadata?.sofAllowed,
      label: "SOF",
    },
  ];

  return (
    <div className={classes.container}>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Product :</p>
          <p className={classes.value}>{metadata?.product ?? "-"}</p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Product Code :</p>
          <p className={classes.value}>{metadata?.productCode ?? "-"}</p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Product Name :</p>
          <p className={classes.value}>{metadata?.productName ?? "-"}</p>
        </div>
      </div>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Account Type :</p>
          <p className={classes.value}>
            {getAccountType(metadata?.accountType)}
          </p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Savings Type :</p>
          <p className={classes.value}>{getSavingType(metadata?.savingType)}</p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Allowed For :</p>
          {allowedFor.every(({ value }) => !value) ? (
            "-"
          ) : (
            <ul className={classes.list}>
              {allowedFor.map(
                ({ value, label }) =>
                  value && <li className={classes.listItem}>{label}</li>
              )}
            </ul>
          )}
        </div>
      </div>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Deposit Type :</p>
          <p className={classes.value}>
            {getDepositType(metadata?.depositType)}
          </p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Maximum Age :</p>
          <p className={classes.value}>
            {formatAmountDot(metadata?.maximumAge) || "-"}
            {/* {(metadata?.createdDate && getYear(metadata?.createdDate)) || "-"} */}
          </p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Product Image :</p>
          <p
            className={`${classes.value} ${classes.link}`}
            onClick={() => setModal(true)}
          >
            {`${metadata?.imageName}${
              metadata?.extensionImage ? metadata?.extensionImage : ""
            }`}
          </p>
        </div>
      </div>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Select Currency:</p>
          <p className={classes.value}>
            {Currency(metadata?.currencyCode) ?? "IDR"}
          </p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Initial Deposit :</p>
          <p className={classes.value}>
            {`${
              metadata?.currencyCode ? metadata?.currencyCode : "IDR"
            } ${formatAmountDot(metadata?.minInitialDeposit)}` || "-"}
          </p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Minimum Balance:</p>
          <p className={classes.value}>
            {`${
              metadata?.currencyCode ? metadata?.currencyCode : "IDR"
            } ${formatAmountDot(metadata?.minBalance)}` || "-"}
          </p>
        </div>
      </div>
      <div className={classes.textWrap}>
        <p className={classes.title}>Hold Amount :</p>
        <p className={classes.value}>
          {`${
            metadata?.currencyCode ? metadata?.currencyCode : "IDR"
          } ${formatAmountDot(metadata?.holdAmount)}` || "-"}
        </p>
      </div>
      <div className={classes.wrapper}>
        <div className={classes.textWrap50}>
          <p className={classes.title}>Product Benefits ID :</p>
          <div
            className={classes.value}
            dangerouslySetInnerHTML={{ __html: metadata?.productBenefitIdn }}
          />
        </div>
        <div className={classes.textWrap50}>
          <p className={classes.title}>Product Benefits EN :</p>
          <div
            className={classes.value}
            dangerouslySetInnerHTML={{ __html: metadata?.productBenefitEng }}
          />
        </div>
      </div>
      <div className={classes.wrapper}>
        <div className={classes.textWrap50}>
          <p className={classes.title}>Product Description ID :</p>
          <div
            className={classes.value}
            dangerouslySetInnerHTML={{ __html: metadata?.productInfoIdn }}
          />
        </div>
        <div className={classes.textWrap50}>
          <p className={classes.title}>Product Description EN :</p>
          <div
            className={classes.value}
            dangerouslySetInnerHTML={{ __html: metadata?.productInfoEng }}
          />
        </div>
      </div>

      <PreviewImage
        label={`${metadata?.imageName}${
          metadata?.extensionImage ? metadata?.extensionImage : ""
        }`}
        title={`${metadata?.imageName}${
          metadata?.extensionImage ? metadata?.extensionImage : ""
        }`}
        isOpen={modal}
        image={`data:image/${metadata?.extensionImage};base64, ${metadata?.base64ProductImage}`}
        handleClose={() => setModal(false)}
      />
    </div>
  );
};

const DetailProductInformation = () => {
  const dispatch = useDispatch();
  const { dataDetail, isLoading } = useSelector(
    ({ aproverWorkFlow }) => aproverWorkFlow
  );

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };

  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Product Information Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={isLoading}
      isSuccess={false}
    />
  );
};

export default DetailProductInformation;
