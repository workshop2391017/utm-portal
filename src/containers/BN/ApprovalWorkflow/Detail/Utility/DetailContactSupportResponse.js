import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import { formatAmountDot } from "utils/helpers";
import React, { useEffect } from "react";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
  },
  tableTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
    fontWeight: 900,
    margin: "10px 10px 10px 20px",
  },
});

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();

  const dataListPeriodical = dataDetail?.metadata?.serviceChargePackage?.filter(
    (elm) => elm.service?.name === "Monthly" || elm.service?.name === "Annual"
  );
  const dataListTransactional =
    dataDetail?.metadata?.serviceChargePackage?.filter(
      (elm) => elm.service?.name !== "Monthly" && elm.service?.name !== "Annual"
    );

  const groupDataListPeriodical = dataListPeriodical?.reduce((groups, item) => {
    const service = groups[item.service?.name] || [];
    service.push(item);
    groups[item.service.name] = service;
    return groups;
  }, {});
  const groupDataListTransactional = dataListTransactional?.reduce(
    (groups, item) => {
      const service = groups[item.service?.name] || [];
      service.push(item);
      groups[item.service.name] = service;
      return groups;
    },
    {}
  );

  const resultListPeriodical = [];
  const resultListTransactional = [];
  if (Object.keys(groupDataListPeriodical ?? {})?.length > 0) {
    Object.keys(groupDataListPeriodical).forEach((key) => {
      resultListPeriodical.push({
        name: key,
        child: groupDataListPeriodical[key],
      });
    });
  }
  if (Object.keys(groupDataListTransactional ?? {})?.length > 0) {
    Object.keys(groupDataListTransactional).forEach((key) => {
      resultListTransactional.push({
        name: key,
        child: groupDataListTransactional[key],
      });
    });
  }

  return (
    <div>
      <div
        style={{
          display: "flex",
          gap: 50,
          width: "100%",
        }}
      >
        <div
          style={{
            width: "34%",
            display: "flex",
            flexDirection: "column",
            gap: "5px",
          }}
        >
          <p
            style={{
              fontFamily: "FuturaBkBT",
              fontWeight: 400,
              fontSize: 13,
              color: Colors.dark.medium,
            }}
          >
            Response Title:
          </p>
          <p
            style={{
              fontFamily: "FuturaMdBT",
              fontWeight: 400,
              fontSize: 15,
              color: Colors.dark.hard,
            }}
          >
            Greet Response
          </p>
        </div>
        <div
          style={{
            width: "66%",
            display: "flex",
            flexDirection: "column",
            gap: "5px",
          }}
        >
          <p
            style={{
              fontFamily: "FuturaBkBT",
              fontWeight: 400,
              fontSize: 13,
              color: Colors.dark.medium,
            }}
          >
            Response Title:
          </p>
          <p
            style={{
              fontFamily: "FuturaMdBT",
              fontWeight: 400,
              fontSize: 15,
              color: Colors.dark.hard,
            }}
          >
            Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet
            consectetur elite integer in lectus. Sodales sed sem faucibus et
            velit volutpat egestas. Nulla duis dignissim ut commodo amet nunc.
            Suspendisse non turpis nisl vulputate suspendisse pellentesque
            adipiscing ac orci. Netus mi maecenas eget sit ligula purus magna.
            Bibendum nam vulputate a fames lectus orci. Scelerisque sit arcu.
          </p>
        </div>
      </div>
    </div>
  );
};

const DetailContactSupportResponse = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject"));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve"));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Contact Support Response Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => {}}
      comment=""
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailContactSupportResponse;
