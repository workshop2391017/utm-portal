/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Typography, makeStyles } from "@material-ui/core";

import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";

import Colors from "helpers/colors";

import { capitalizeFirstLetterFromUppercase } from "utils/helpers";
import Badge from "components/BN/Badge";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "grid",
    gridTemplateColumns: "repeat(3, 1fr)",
    rowGap: "20px",
  },
  value: { fontFamily: "FuturaMdBT", fontSize: 16 },
}));

const renderRole = (role) => {
  if (role === "ADMIN_MAKER_APPROVER") {
    return "Maker, Approver";
  }
  if (role === "ADMIN_MAKER") {
    return "Maker";
  }
  return "Approver";
};
const DetailData = ({ dataDetail }) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div>
        <Typography variant="subtitle2">Name :</Typography>
        <span className={classes.value}>
          {dataDetail?.additionalData?.name || "-"}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">Username :</Typography>
        <span className={classes.value}>
          {dataDetail?.additionalData?.username || "-"}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">NIP :</Typography>
        <span className={classes.value}>
          {dataDetail?.additionalData?.nip || "-"}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">E-mail :</Typography>
        <span className={classes.value}>
          {dataDetail?.additionalData?.email || "-"}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">Role :</Typography>
        <span className={classes.value}>
          {renderRole(dataDetail?.additionalData?.role)}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">Group :</Typography>
        <span className={classes.value}>
          {dataDetail?.additionalData?.portalGroup || "-"}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">Branch :</Typography>
        <span className={classes.value}>
          {capitalizeFirstLetterFromUppercase(
            dataDetail?.additionalData?.branch
          )}
        </span>
      </div>

      <div>
        <Typography variant="subtitle2">Login Status :</Typography>
        <Badge
          outline
          label={dataDetail?.additionalData?.isIpCheck ? "Active" : "Inactive"}
          type={dataDetail?.additionalData?.isIpCheck ? "green" : "red"}
          styleBadge={{
            display: "inline-block",
          }}
        />
      </div>

      <div>
        <Typography variant="subtitle2">Status User :</Typography>
        <Badge
          outline
          label={dataDetail?.metaData?.status === 0 ? "Inactive" : "Active"}
          type={dataDetail?.metaData?.status === 0 ? "red" : "green"}
          styleBadge={{
            display: "inline-block",
          }}
        />
      </div>
    </div>
  );
};

const DetailUserSetting = () => {
  const dispatch = useDispatch();
  const { dataDetail, isLoading } = useSelector(
    ({ aproverWorkFlow }) => aproverWorkFlow
  );

  const [comment, setComment] = useState("");

  const additionalData = dataDetail?.additionalData;
  const metaData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };

  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="User Setting Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData dataDetail={{ additionalData, metaData }} />
      }
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={isLoading}
      isSuccess={false}
    />
  );
};

export default DetailUserSetting;
