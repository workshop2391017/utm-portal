import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import { formatAmountDot } from "utils/helpers";
import React, { useEffect, useState } from "react";
import TableICBBCollapse from "components/BN/TableIcBB/collapse";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import purify from "dompurify";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
  },
  tableTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
    fontWeight: 900,
    margin: "10px 10px 10px 20px",
  },
  wrapper: {
    display: "flex",
    gap: 50,
    width: "100%",
  },
  textWrap: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
    width: "33%",
  },
  textWrap50: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
    width: "50%",
  },
  textWrap100: {
    display: "flex",
    flexDirection: "column",
    gap: "5px",
    width: "100%",
  },
  title: {
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: 13,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 15,
    color: Colors.dark.hard,
  },
  container: {
    display: "flex",
    gap: 20,
    flexDirection: "column",
  },
});

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();

  console.log("dataDetail =>", dataDetail);

  return (
    <div className={classes.container}>
      <div className={classes.wrapper}>
        <div className={classes.textWrap}>
          <p className={classes.title}>Category :</p>
          <p className={classes.value}>{dataDetail?.metadata?.category}</p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Code :</p>
          <p className={classes.value}>{dataDetail?.metadata?.code}</p>
        </div>
        <div className={classes.textWrap}>
          <p className={classes.title}>Feature :</p>
          <p className={classes.value}>{dataDetail?.metadata?.name}</p>
        </div>
      </div>
      <div className={classes.wrapper}>
        {/* <div className={classes.textWrap50}>
          <p className={classes.title}>Indonesian Text :</p>
          <p className={classes.value}>
            <div
              dangerouslySetInnerHTML={{
                __html: purify.sanitize(dataDetail?.metadata?.valueID || ""),
              }}
            />
          </p>
        </div> */}
        <div className={classes.textWrap100}>
          <p className={classes.title}>Text Editor :</p>
          <p className={classes.value}>
            <div
              dangerouslySetInnerHTML={{
                __html: purify.sanitize(dataDetail?.metadata?.value || ""),
              }}
            />
          </p>
        </div>
      </div>
    </div>
  );
};

const EventCalendar = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Content Management Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default EventCalendar;
