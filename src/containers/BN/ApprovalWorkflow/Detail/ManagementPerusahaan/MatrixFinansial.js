import { Button, Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import ScrollCustom from "components/BN/ScrollCustom";
import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right.svg";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
  getAllService,
} from "stores/actions/aproverWorkFlow";
import formatNumber from "helpers/formatNumber";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";
import PopupJenisRekening from "./components/popupJenisLayanan";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  companyName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 20,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  category: {
    display: "flex",
    alignItems: "center",
    "& div:nth-child(1)": {
      fontFamily: "FuturaMdBT",
      fontSize: 12,
      color: Colors.dark.hard,
    },
    "& div:nth-child(2)": {
      fontFamily: "FuturaMdBT",
      fontSize: 12,
      color: Colors.gray.medium,
    },
  },
  pembayaran: {
    fontFamily: "FuturaHvBT",
    fontSize: 17,
    color: Colors.dark.hard,
    marginTop: 20,
    marginBottom: 5,
  },
  currency: {
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
  },
  content: {
    marginTop: 20,
    border: "1px solid #B3C1E7",
    borderRadius: 10,
    "& .content-title": {
      margin: "10px 20px 20px 20px",
      "& div:nth-child(1)": {
        fontFamily: "FuturaHvBT",
        fontSize: 15,
        color: Colors.dark.hard,
      },
      "& div:nth-child(2)": {
        marginTop: 10,
        fontFamily: "FuturaMdBT",
        fontSize: 13,
        color: Colors.dark.medium,
        "& span": {
          color: Colors.dark.hard,
        },
      },
    },
  },
  rightSection: {
    "& .jenis-layanan": {
      fontFamily: "FuturaHvBT",
      fontSize: 13,
      color: Colors.dark.medium,
    },
    textAlign: "right",
    "& .lihatsemua": {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      fontSize: 13,
      fontFamily: "FuturaMdBT",
      color: Colors.primary.hard,
      fontWeight: 700,
      "&:hover": {
        cursor: "pointer",
      },
    },
  },
});

const headerContent = [
  {
    title: "Seq",
    key: "sequenceNumber",
    width: 100,
  },
  {
    title: "User",
    key: "totalApproval",
    width: 100,
  },
  {
    title: "Approval Level",
    key: "workflowLimitSchemeName",
  },
  {
    title: "Group",
    key: "groupType",
  },
  {
    title: "Group Target",
    key: "workflowGroupName",
    render: (row) => row?.workflowGroupName ?? "-",
  },
];

const DetailData = ({ dataDetail, allServicesMatrix }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);

  const isAllMenus = dataDetail?.metadata?.approvalMatrix?.find((a) =>
    a.menuName?.includes("All")
  );

  useEffect(() => {
    if (
      dataDetail?.metadata?.parentMenuId &&
      dataDetail?.additionalData?.corporateProfileId
    ) {
      dispatch(
        getAllService({
          group: "USER",
          isApprovalMatrixAdmin: false,
          parent_id: dataDetail?.metadata?.parentMenuId,
          isAllServiceMenu: true,
          corporateProfileId: dataDetail?.additionalData?.corporateProfileId,
        })
      );
    }
  }, [dataDetail]);

  return (
    <div>
      <PopupJenisRekening
        services={allServicesMatrix?.workflowApprovalMatrixAllServiceDtos ?? []}
        open={openModal}
        handleClose={() => setOpenModal(false)}
      />

      <div className={classes.companyName}>
        {dataDetail?.additionalData?.detailCorporate?.corporateName}
      </div>
      <div className={classes.category}>
        <div>
          {dataDetail?.additionalData?.detailCorporate?.corporateAuthorization}{" "}
          Authorization
        </div>
      </div>
      <div className={classes.pembayaran}>
        {isAllMenus?.menuName ||
          dataDetail?.metadata?.approvalMatrix?.[0]?.menuName}
      </div>
      <div className={classes.currency}>
        {isAllMenus?.currencyCode ||
          dataDetail?.metadata?.currencyCodeTo ||
          dataDetail?.metadata?.approvalMatrix?.[0]?.currencyCode}{" "}
        -{" "}
        {isAllMenus?.currencyName ||
          dataDetail?.metadata?.currencyCodeToName ||
          dataDetail?.metadata?.approvalMatrix?.[0]?.currencyName}
      </div>

      {isAllMenus && (
        <div className={classes.rightSection}>
          <div className="jenis-layanan">Type of Service Regulated:</div>
          <div>
            {allServicesMatrix?.workflowApprovalMatrixAllServiceDtos?.length}{" "}
            Services
          </div>
          <div>
            <div className="lihatsemua">
              <Button
                style={{ all: "unset", position: "relative" }}
                onClick={() => {
                  setOpenModal(true);
                }}
              >
                View All Service Types
              </Button>
              <ChevronRight />
            </div>
          </div>
        </div>
      )}
      <ScrollCustom
        width="100%"
        height={dataDetail?.metadata?.approvalMatrix?.length > 3 ? 550 : "auto"}
      >
        {dataDetail?.metadata?.approvalMatrix?.map((ev) => (
          <div className={classes.content}>
            <div className="content-title">
              <div>0 - {formatNumber(ev?.maximumLimit)}</div>
              <div>
                Number of Approvals : <span>{ev?.totalSequence}</span>
              </div>
            </div>
            <TableMenuBaru
              headerContent={headerContent}
              dataContent={ev?.detail ?? []}
            />
          </div>
        ))}
      </ScrollCustom>
    </div>
  );
};

const DetailApprovalWorkflowMatrixFinansial = () => {
  const dispatch = useDispatch();
  const { dataDetail, allServicesMatrix } = useSelector(
    (state) => state.aproverWorkFlow
  );
  const [comment, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Financial Matrix Settings Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData
          dataDetail={dataDetail}
          allServicesMatrix={allServicesMatrix}
        />
      }
      setComment={(e) => setComments(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowMatrixFinansial;
