import { Grid, makeStyles } from "@material-ui/core";

import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";
import styled from "styled-components";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import elips from "../../../../../assets/images/BN/elips.png";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;
const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: FuturaBQ;
  font-weight: 500;
`;

const TitleContent = styled.div`
  color: #374062;
  font-family: FuturaMdBT;
  font-size: 15px;
  font-weight: 400;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  card: {
    display: "flex",
    marginTop: "20px",
  },
  cardItem: {
    width: "100%",
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Acitivy :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = () => {
  const classes = useStyles();
  return (
    <div>
      <Title>PT. Jaya Sentosa</Title>
      <div
        style={{
          display: "flex",
        }}
      >
        <Lable primary>Individual </Lable> |
        <Lable>Multiple - Without Sys Admin</Lable>
      </div>
      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>Group Name :</Label>
          <TitleContent>Maker 1</TitleContent>
        </div>
        <div className={classes.cardItem}>
          <Label>Division :</Label>
          <TitleContent>Admin</TitleContent>
        </div>
        <div className={classes.cardItem}>
          <Label>Member :</Label>
          <TitleContent>Member 1</TitleContent>
        </div>
      </div>

      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>Transaction Limit :</Label>
          <TitleContent>Rp. 100.000</TitleContent>
        </div>
        <div className={classes.cardItem}>
          <Label>Currency :</Label>
          <TitleContent>IDR</TitleContent>
        </div>
        <div className={classes.cardItem}>
          {/* <Label></Label>
          <TitleContent>Anggota 1</TitleContent> */}
        </div>
      </div>

      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>Access :</Label>
          <TitleContent>Transaction Type</TitleContent>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginTop: "8px",
            }}
          >
            <img
              style={{
                width: "7px",
                height: "7px",
                borderRadius: "3px",
                color: "#0061A7",
                marginRight: "10px",
              }}
              src={elips}
              alt="linkaran"
            />

            <TitleContent>Transfer</TitleContent>
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginTop: "8px",
            }}
          >
            <img
              style={{
                width: "7px",
                height: "7px",
                borderRadius: "3px",
                color: "#0061A7",
                marginRight: "10px",
              }}
              src={elips}
              alt="linkaran"
            />

            <TitleContent>Payment</TitleContent>
          </div>
        </div>
        <div className={classes.cardItem}>
          <TitleContent>Saving Account</TitleContent>
        </div>
        <div className={classes.cardItem}>
          {/* <Label></Label>
          <TitleContent>Anggota 1</TitleContent> */}
        </div>
      </div>
    </div>
  );
};

const DetailApprovalWorkflowDPengaturanAlurKerja = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comments, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comments));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comments));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Workflow Settings Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData />}
      setComment={(e) => setComments(e.target.value)}
      comment={comments}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowDPengaturanAlurKerja;
