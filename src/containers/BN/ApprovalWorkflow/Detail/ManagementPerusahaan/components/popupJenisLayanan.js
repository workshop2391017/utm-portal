import React from "react";

import {
  Modal,
  Backdrop,
  Fade,
  Paper,
  Grid,
  Typography,
  IconButton,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ScrollCustom from "components/BN/ScrollCustom";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 452,
    minHeight: 300,
    borderRadius: 20,
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    background: "white",
  },
}));

export default function RegulatedServicesPopup({
  services,
  handleClose,
  open,
}) {
  const classes = useStyles();

  return (
    <Modal
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Paper className={classes.modal}>
          <Grid
            container
            direction="column"
            style={{ padding: 20, height: "100%" }}
          >
            <Grid
              container
              item
              justifyContent="space-between"
              alignItems="center"
              style={{ marginBottom: 20 }}
            >
              <Grid item xs="auto">
                <Typography variant="h2">Service Type</Typography>
              </Grid>
              <Grid item xs="auto">
                <IconButton onClick={handleClose}>
                  <XIcon />
                </IconButton>
              </Grid>
            </Grid>

            <ScrollCustom height={330}>
              <Grid container direction="column">
                {services?.map((e, i) => (
                  <Grid
                    key={i}
                    item
                    xs="auto"
                    style={{
                      marginBottom: 16,
                      paddingLeft: !e?.menuNameEng?.includes("All")
                        ? 16
                        : undefined,
                    }}
                  >
                    <Typography
                      variant={
                        !e?.menuNameEng?.includes("All") ? "subtitle2" : "h6"
                      }
                    >
                      {e?.menuNameEng}
                    </Typography>
                  </Grid>
                ))}
              </Grid>
            </ScrollCustom>
          </Grid>
        </Paper>
      </Fade>
    </Modal>
  );
}
