import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import { ReactComponent as Star } from "assets/icons/BN/star.svg";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";
import TableIcbb from "../../../../../components/BN/TableIcBB";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const dataHeader = [
  {
    title: "Account Number",
    key: "accountNumber",
  },
  {
    title: "Product name",
    key: "productName",
  },
];

const formatTable = [
  {
    title: "Account number",
    headerAlign: "left",
    align: "left",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          width: "171px",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <p
          style={{
            fontFamily: "FuturaBkBT",
            fontWeight: 400,
            fontSize: "13px",
            lineHeight: "16px",
          }}
        >
          {rowData.accountNumber}
        </p>
        {rowData.isPrimary && (
          <Star viewBox="0 0 20 20" height="24" width="24" />
        )}
      </div>
    ),
  },
  {
    title: "Product Name",
    headerAlign: "left",
    align: "left",
    render: (rowData) => (
      <p
        style={{
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          fontSize: "13px",
          lineHeight: "16px",
        }}
      >
        {rowData.productName}
      </p>
    ),
  },
];

const DetailData = ({ detailData, detailAdditionalData }) => {
  const classes = useStyles();

  return (
    <div>
      <Title>{detailAdditionalData?.corporateName}</Title>
      <div
        style={{
          display: "flex",
        }}
      >
        <Lable>
          {detailAdditionalData?.corporateAuthorization} Authorization
        </Lable>
      </div>
      <div
        style={{
          marginTop: "20px",
          marginBottom: "20px",
        }}
      >
        <Title subtitle>{detailData?.accountGroupName}</Title>
      </div>
      <TableIcbb
        headerContent={formatTable}
        dataContent={detailData?.accounts ?? []}
      />
    </div>
  );
};

const DetailApprovalWorkflowPengelompokanRekening = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const detailData = dataDetail?.metadata;
  const detailAdditionalData = dataDetail?.additionalData;

  const [comment, setComment] = React.useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Account Grouping Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData
          detailData={detailData}
          detailAdditionalData={detailAdditionalData}
        />
      }
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowPengelompokanRekening;
