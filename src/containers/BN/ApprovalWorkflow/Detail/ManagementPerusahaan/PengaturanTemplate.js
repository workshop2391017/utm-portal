import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";
import styled from "styled-components";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: "FuturaBQ";
  font-weight: 500;
  margin-bottom: 5px;
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const Title = styled.div`
  color: #374062;
  font-family: "FuturaMdBT";
  font-weight: 400;
  font-size: ${(props) => (props.title ? "16px" : "15px")};
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  card: {
    display: "flex",
    marginBottom: "20px",
  },
  cardItem: {
    width: "100%",
  },
  containerCard: {},
  cardTitle: {
    marginTop: "20px",
    marginBottom: "20px",
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Melakukan Perubahan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = () => {
  const classes = useStyles();
  return (
    <div className={classes.containerCard}>
      <Title>PT. Jaya Sentosa</Title>
      <div
        style={{
          display: "flex",
        }}
      >
        <Lable primary>Individual </Lable> |
        <Lable>Multiple - Without Sys Admin</Lable>
      </div>
      <div
        style={{
          marginTop: "20px",
          marginBottom: "20px",
        }}
      >
        <Title>Role : Maker</Title>
      </div>

      <div className={classes.cardTitle}>
        <Title title>Business Owner Profile</Title>
      </div>

      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>Full Name :</Label>
          <Title>Primanita</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>Gender :</Label>
          <Title>Perempuan</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>Nationality :</Label>
          <Title>WNI</Title>
        </div>
      </div>

      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>Date of birth :</Label>
          <Title>27 / 12 / 1985</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>Place of birth :</Label>
          <Title>Yogyakarta</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>KTP / KITAS Number :</Label>
          <Title>12343434355</Title>
        </div>
      </div>

      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>Position :</Label>
          <Title>Manager</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>Home Phone Number :</Label>
          <Title>022 - 823 8475</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>Mobile Phone Number :</Label>
          <Title>+62 - 823 8475 3435</Title>
        </div>
      </div>

      <div className={classes.card}>
        <div className={classes.cardItem}>
          <Label>E-Mail :</Label>
          <Title>primanita@email.com</Title>
        </div>
        <div className={classes.cardItem}>
          <Label>Complete Address :</Label>
          <Title>
            Wirosaban Baru, Jl. Pangeran Wirosobo No.54, RT.53/RW.14, Sorosutan,
            Umbulharjo, Yogyakarta City, Special Region of Yogyakarta 55162
          </Title>
        </div>
        <div className={classes.cardItem}>
          {/* <Label>No Telepon Genggam :</Label>
        <Title>+62 - 823 8475 3435</Title> */}
        </div>
      </div>
    </div>
  );
};

const DetailApprovalWorkflowPengaturanTemplate = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comments, setComments] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comments));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comments));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Template Settings Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData />}
      setComment={(e) => setComments(e.target.value)}
      comment={comments}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowPengaturanTemplate;
