import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { formatAmountDot, formatPhoneStripNoSpace } from "utils/helpers";
import { useDispatch, useSelector } from "react-redux";
import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const Subtitle = styled.div`
  color: #0061a7;
  font-size: 15px;
  font-family: FuturaHvBT;
  font-weight: 400;
  margin-bottom: 10px;
`;
const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: FuturaBQ;
  font-weight: 500;
`;
const TextContent = styled.div`
  color: #374062;
  font-size: 15px;
  font-weight: 400;
  font-family: FuturaMdBT;
`;

const TitleContent = styled.div`
  color: #374062;
  font-size: 14px;
  font-family: FuturaHvBT;
  font-weight: 400;
  margin-bottom: 10px;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  card: {
    display: "flex",
    justifyContent: "space-between",
    paddingRight: "20px",
    marginBottom: "20px",
  },
  cardItem: {
    paddingTop: "10px",
  },
  cardContent: {
    paddingLeft: "20px",
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ detailData }) => {
  const classes = useStyles();
  const { dataDetail } = useSelector((state) => state?.aproverWorkFlow);
  const [dataDetailState, setDataDetailState] = useState([]);

  // const conversiName = (firstInput, data) => {
  //   const resulFilter = data?.filter(
  //     (item) => item?.name.toLowerCase() === firstInput.toLowerCase()
  //   )};

  const toTitleCase = (str) =>
    str.replace(
      /\w\S*/g,
      (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    );

  useEffect(() => {
    const resultMapping = detailData?.accountLimits?.map((item) => ({
      ...item,
      transactionCategory: toTitleCase(
        item?.transactionCategory?.split("_").join(" ")
      ),
      maxAmountTransaction: `IDR ${formatAmountDot(
        item?.maxAmountTransaction?.toString()
      )}`,
    }));

    setDataDetailState(resultMapping);
  }, [detailData]);

  return (
    <div>
      <Title>{dataDetail?.additionalData?.corporateName}</Title>
      <div
        style={{
          display: "flex",
          marginBottom: "20px",
        }}
      >
        <Lable>
          {dataDetail?.additionalData?.corporateAuthorization} Authorization
        </Lable>
      </div>
      <div>
        <div className={classes.labelDataUser}>Account Number :</div>
        <TextContent>
          {formatPhoneStripNoSpace(dataDetail?.metadata?.accountNumber) || ""}
        </TextContent>
      </div>

      {dataDetailState?.length > 0 && (
        <React.Fragment>
          <div className={classes.card}>
            <div className={classes.cardItem}>
              <Subtitle>Payment</Subtitle>
              <div className={classes.cardContent}>
                <Label>
                  {dataDetailState[0]?.transactionCategory && "Payment Limits:"}
                </Label>
                <TextContent>
                  {dataDetailState[0]?.maxAmountTransaction}
                </TextContent>
              </div>
            </div>
            <div className={classes.cardItem}>
              <Subtitle>Purchase</Subtitle>
              <div className={classes.cardContent}>
                <Label>
                  {dataDetailState[1]?.transactionCategory && "Purchase Limit:"}
                </Label>
                <TextContent>
                  {dataDetailState[1]?.maxAmountTransaction}
                </TextContent>
              </div>
            </div>
            <div className={classes.cardItem}>
              <Subtitle>Payroll</Subtitle>
              <div className={classes.cardContent}>
                <Label>
                  {dataDetailState[7]?.transactionCategory && "Payroll Limits:"}
                </Label>
                <TextContent>
                  {dataDetailState[7]?.maxAmountTransaction}
                </TextContent>
              </div>
            </div>
          </div>
          <div className={classes.card}>
            <div className={classes.cardItem}>
              <Subtitle>Transfer</Subtitle>
              <div className={classes.cardContent}>
                <Label>
                  {dataDetailState[4]?.transactionCategory && "Bank Transfer:"}
                </Label>
                <TextContent>
                  {dataDetailState[4]?.maxAmountTransaction}
                </TextContent>
              </div>
            </div>
            <div className={classes.cardItem}>
              <Subtitle>Domestic Transfer</Subtitle>
              <div className={classes.cardContent}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: "10px",
                  }}
                >
                  <div
                    style={{
                      marginRight: "10px",
                    }}
                  >
                    <Label>
                      {dataDetailState[5]?.transactionCategory &&
                        "Online Limits:"}
                    </Label>
                    <TextContent>
                      {dataDetailState[5]?.maxAmountTransaction}
                    </TextContent>
                  </div>

                  <div>
                    <Label>
                      {dataDetailState[3]?.transactionCategory && "SKN Limits:"}
                    </Label>
                    <TextContent>
                      {dataDetailState[3]?.maxAmountTransaction}
                    </TextContent>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: "10px",
                  }}
                >
                  <div
                    style={{
                      marginRight: "10px",
                    }}
                  >
                    <Label>
                      {dataDetailState[2]?.transactionCategory &&
                        "RTGS Limits:"}
                    </Label>
                    <TextContent>
                      {dataDetailState[2]?.maxAmountTransaction}
                    </TextContent>
                  </div>
                  <div>
                    <Label>
                      {dataDetailState[6]?.transactionCategory &&
                        "BI-FAST Limits:"}
                    </Label>
                    <TextContent>
                      {dataDetailState[6]?.maxAmountTransaction}
                    </TextContent>
                  </div>
                </div>
              </div>
            </div>
            <div> </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

const DetailApprovalWorkflowKonfigurasiRekening = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);
  const [comment, setComments] = useState("");

  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Account Configuration Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowKonfigurasiRekening;
