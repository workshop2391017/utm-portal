import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import Colors from "helpers/colors";
import { formatPhoneNumber } from "utils/helpers";

import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles((theme) => ({
  mediumTypography: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 16,
    letterSpacing: "0.01em",
    color: Colors.dark.hard,
    overflowWrap: "anywhere",
  },
}));

const LabelAndValue = ({
  lastRow,
  label1,
  label2,
  label3,
  value1,
  value2,
  value3,
}) => {
  const classes = useStyles();

  return (
    <Grid
      container
      item
      xs="auto"
      style={{ marginBottom: lastRow ? undefined : 20 }}
    >
      <Grid container direction="column" item xs={4}>
        <Grid item xs="auto">
          <Typography variant="subtitle1" style={{ color: Colors.dark.medium }}>
            {label1}
          </Typography>
          <span className={classes.mediumTypography} style={{ fontSize: 15 }}>
            {value1}
          </span>
        </Grid>
      </Grid>
      <Grid container direction="column" item xs={4} style={{ paddingLeft: 8 }}>
        <Grid item xs="auto">
          <Typography variant="subtitle1" style={{ color: Colors.dark.medium }}>
            {label2}
          </Typography>
          <span className={classes.mediumTypography} style={{ fontSize: 15 }}>
            {value2}
          </span>
        </Grid>
      </Grid>
      {label3 && value3 ? (
        <Grid
          container
          direction="column"
          item
          xs={4}
          style={{ paddingLeft: 16 }}
        >
          <Grid item xs="auto">
            <Typography
              variant="subtitle1"
              style={{ color: Colors.dark.medium }}
            >
              {label3}
            </Typography>
            <span className={classes.mediumTypography} style={{ fontSize: 15 }}>
              {value3}
            </span>
          </Grid>
        </Grid>
      ) : null}
    </Grid>
  );
};

const DetailData = () => {
  const classes = useStyles();

  const { dataDetail } = useSelector((state) => state?.aproverWorkFlow);

  return (
    <Grid container direction="column">
      {/* ----- Business Field Profile Block-----*/}
      <Grid
        item
        xs="auto"
        className={classes.mediumTypography}
        style={{ marginBottom: 20 }}
      >
        <span>Business Field Profile</span>
      </Grid>

      <LabelAndValue
        label1="Corporate ID :"
        label2="Business Name :"
        label3="Business Fields :"
        value1={dataDetail?.metadata?.corporateId ?? "-"}
        value2={dataDetail?.metadata?.corporateName ?? "-"}
        value3={dataDetail?.metadata?.corporateFieldName ?? "-"}
      />
      <LabelAndValue
        label1="Complete Address :"
        label2="Province :"
        label3="City/District :"
        value1={dataDetail?.metadata?.corporateAddress ?? "-"}
        value2={dataDetail?.metadata?.province ?? "-"}
        value3={dataDetail?.metadata?.district ?? "-"}
      />
      <LabelAndValue
        label1="District :"
        label2="Sub-district :"
        label3="Postcode :"
        value1={dataDetail?.metadata?.subDistrict ?? "-"}
        value2={dataDetail?.metadata?.village ?? "-"}
        value3={dataDetail?.metadata?.postalCode ?? "-"}
      />
      <LabelAndValue
        label1="Business Phone Number :"
        // label2="Amount of Soft Token Requirement :"
        value1={
          dataDetail?.metadata?.corporateContact
            ? formatPhoneNumber(
                dataDetail?.metadata?.corporateContact,
                18
              ).replace(/^0/g, "+62 - ")
            : "-"
        }
        // value2={dataDetail?.metadata?.numberOfToken ?? "-"}
      />

      {/* -----Person in Charge Block----- */}
      <Grid
        item
        xs="auto"
        className={classes.mediumTypography}
        style={{ marginBottom: 20 }}
      >
        <span>Person in Charge</span>
      </Grid>

      <LabelAndValue
        label1="User ID :"
        label2="Full Name :"
        label3="Position :"
        value1={dataDetail?.metadata?.username ?? "-"}
        value2={dataDetail?.metadata?.fullName ?? "-"}
        value3={dataDetail?.metadata?.jobPosition ?? "-"}
      />
      <LabelAndValue
        label1="Department :"
        label2="KTP / KITAS Number :"
        label3="NPWP Number :"
        value1={dataDetail?.metadata?.division ?? "-"}
        value2={dataDetail?.metadata?.identityNumber ?? "-"}
        value3={dataDetail?.metadata?.npwp ?? "-"}
      />
      <LabelAndValue
        lastRow
        label1="Home Phone Number :"
        label2="Mobile Phone Number :"
        label3="Email :"
        value1={
          dataDetail?.metadata?.phoneHome
            ? formatPhoneNumber(dataDetail?.metadata?.phoneHome, 18)
            : "-"
        }
        value2={
          dataDetail?.metadata?.phoneNumber
            ? formatPhoneNumber(dataDetail?.metadata?.phoneNumber, 18).replace(
                /^0/g,
                "+62 - "
              )
            : "-"
        }
        value3={dataDetail?.metadata?.email ?? "-"}
      />
    </Grid>
  );
};

const DetailApprovalWorkflowInformasiPerusahaan = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };

  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Company Information Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowInformasiPerusahaan;
