import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { useState } from "react";
import styled from "styled-components";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const Label = styled.div`
  color: #7b87af;
  font-size: 13px;
  font-family: FuturaBQ;
  font-weight: 500;
  margin-top: 20px;
  margin-bottom: 5px;
`;

const Subtitle = styled.div`
  color: #374062;
  font-size: 15px;
  font-family: FuturaMdBT;
  font-weight: 400;
  margin-bottom: 20px;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const DataUser = () => {
  const classes = useStyles();

  const { dataDetail } = useSelector((state) => state?.aproverWorkFlow);

  const onStatus = (status) => {
    if (status === "WAITING") {
      return (
        <Badge
          type="blue"
          label="Waiting"
          styleBadge={{ display: "inline-block" }}
        />
      );
    }

    if (status === "APPROVED") {
      return (
        <Badge
          type="blue"
          label="Approved"
          styleBadge={{ display: "inline-block" }}
        />
      );
    }

    if (status === "REJECTED") {
      return (
        <Badge
          type="blue"
          label="Rejected"
          styleBadge={{ display: "inline-block" }}
        />
      );
    }
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>
          {dataDetail?.userDataApprovalWorkflowDto?.createdUserName}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>
          {dataDetail?.userDataApprovalWorkflowDto?.userPortalId}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>
          {" "}
          {dataDetail?.userDataApprovalWorkflowDto?.createdDate}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>
          {" "}
          {dataDetail?.userDataApprovalWorkflowDto?.branchName}{" "}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>
          {" "}
          {dataDetail?.userDataApprovalWorkflowDto?.activityName}{" "}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>

        <div className={classes.valueDataUser}>
          {onStatus(dataDetail?.userDataApprovalWorkflowDto?.statusName)}
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = (props) => {
  // console.warn("dataProps:", props);

  const { addData, detailData } = props;

  // console.warn("result:", addData, detailData);

  return (
    <div>
      <Title>{addData?.corporateName}</Title>
      <div
        style={{
          display: "flex",
        }}
      >
        <Lable>{addData?.corporateAuthorization} Authorization</Lable>
      </div>
      <Label>Transfer Recipient :</Label>
      <Subtitle>
        {detailData?.isBeneficiaryGroup ? "Corporate" : "User"}
      </Subtitle>
    </div>
  );
};

const DetailApprovalWorkflowPengaturanPenerimaTf = () => {
  const { reffNo, dataDetail } = useSelector((state) => state?.aproverWorkFlow);

  // console.warn("dataDetail:", dataDetail);
  const dispatch = useDispatch();

  const [comment, setComment] = useState("");

  const detailData = dataDetail?.metadata;
  const additionalData = dataDetail?.additionalData;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Transfer Recipient Setting Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData detailData={detailData} addData={additionalData} />
      }
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowPengaturanPenerimaTf;
