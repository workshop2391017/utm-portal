import { Grid, Typography, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React from "react";
import styled from "styled-components";
import { ReactComponent as BlueSphere } from "assets/icons/BN/blue-sphere.svg";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import { formatPhoneNumber } from "utils/helpers";
import { metadata } from "react-cron-generator/dist/meta";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const Label2 = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaBkBT;
  font-weight: 400;
`;

const TextContent = styled.div`
  color: #374062;
  font-family: FuturaMdBT;
  font-weight: 400;
  font-size: 15px;
  letterofspacing: "0.01em";
`;

const useStyles = makeStyles({
  noWrapText: {
    color: "#374062",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 15,
    letterSpacing: "0.01em",
  },
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  card: {
    display: "flex",
    marginTop: "20px",
    gap: 247,
  },
  cardItem: {
    marginTop: "10px",
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Activity :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = (adminRole) => {
  const classes = useStyles();
  const { dataDetail } = useSelector((state) => state?.aproverWorkFlow);

  const handleRoleUser = (string) => {
    if (string) {
      const temp = string
        .split("_")
        .filter((word) => word !== "USER")
        .map((word) => `${word.charAt(0)}${word.slice(1).toLowerCase()}`);

      if (temp[0] === "Admin") {
        if (temp[1] === "Releaser") {
          temp[1] = "Approver";
        }
        return temp.join(" ");
      }

      return temp.join(", ");
    }
  };

  return (
    <div>
      <Title>{dataDetail?.additionalData?.corporateName}</Title>
      <div
        style={{
          display: "flex",
        }}
      >
        <Lable>
          {dataDetail?.additionalData?.corporateAuthorization} Authorization
        </Lable>
      </div>
      <Grid container style={{ marginTop: 10 }} spacing={3}>
        <Grid item xs={4}>
          <TextContent>
            {" "}
            Role:{" "}
            {handleRoleUser(
              dataDetail?.metadata?.roleCode ||
                dataDetail?.metadata?.detailUser?.roleCode
            )}
          </TextContent>
          {dataDetail?.metadata?.status === 4 ||
          dataDetail?.metadata?.detailUser?.status === 4 ? (
            <Badge
              type="red"
              label="Non Active"
              outline
              styleBadge={{
                textAlign: "center",
                float: "left",
                clear: "left",
              }}
            />
          ) : dataDetail?.metadata?.status === 0 ||
            dataDetail?.metadata?.detailUser?.status === 0 ||
            !dataDetail?.metadata?.status ||
            !dataDetail?.metadata?.detailUser?.status ? (
            <Badge
              outline
              type="green"
              label="Active"
              styleBadge={{
                textAlign: "center",
                float: "left",
                clear: "left",
              }}
            />
          ) : null}
        </Grid>
        {dataDetail?.metadata?.roleCode?.includes(
          "ADMIN"
        ) ? null : (dataDetail?.metadata?.roleCode?.split("_")[0] ||
            dataDetail?.metadata?.detailUser?.roleCode?.split("_")[0] !==
              "ADMIN") &&
          dataDetail?.additionalData?.corporateAuthorization?.toLowerCase() ===
            "multiple" ? (
          <React.Fragment>
            <Grid item xs={4}>
              <TextContent> Approval Level :</TextContent>
              <TextContent>
                {" "}
                {dataDetail?.metadata?.workflowLimitSchemeName ||
                  dataDetail?.metadata?.detailUser?.workflowLimitSchemeName ||
                  "-"}
              </TextContent>
            </Grid>
            <Grid item xs={4}>
              <TextContent> User Group:</TextContent>
              <TextContent>
                {" "}
                {dataDetail?.metadata?.workflowGroupName ||
                dataDetail?.metadata?.detailUser?.workflowGroupName ? (
                  <Badge
                    type="blue"
                    label={
                      dataDetail?.metadata?.workflowGroupName ||
                      dataDetail?.metadata?.detailUser?.workflowGroupName
                    }
                    styleBadge={{
                      float: "left",
                      clear: "left",
                      textAlign: "center",
                    }}
                  />
                ) : (
                  "-"
                )}
              </TextContent>
            </Grid>
          </React.Fragment>
        ) : null}
      </Grid>
      <div style={{ display: "flex", flexDirection: "column", marginTop: 10 }}>
        <TextContent>User Data</TextContent>
        <Grid container style={{ marginTop: 8 }} spacing={3}>
          <Grid item xs={4}>
            <Label2> Corporate ID :</Label2>
            <TextContent style={{ wordWrap: "break-word" }}>
              {" "}
              {dataDetail?.additionalData?.corporateId || "-"}
            </TextContent>
          </Grid>
          {dataDetail?.metadata?.roleCode?.includes("ADMIN") ? null : (
            <Grid item xs={4}>
              <Label2>User ID :</Label2>
              <TextContent>
                {" "}
                {dataDetail?.metadata?.userId ||
                  dataDetail?.metadata?.detailUser?.userId ||
                  "-"}
              </TextContent>
            </Grid>
          )}
          <Grid item xs={4}>
            <Label2> Full Name :</Label2>
            <TextContent style={{ wordWrap: "break-word" }}>
              {" "}
              {dataDetail?.metadata?.fullName ||
                dataDetail?.metadata?.detailUser?.fullName ||
                "-"}
            </TextContent>
          </Grid>
          <Grid item xs={4}>
            <Label2> KTP Number :</Label2>
            <TextContent>
              {" "}
              {dataDetail?.metadata?.identityNumber ||
                dataDetail?.metadata?.detailUser?.identityNumber ||
                "-"}
            </TextContent>
          </Grid>
          {dataDetail?.metadata?.roleCode?.includes("ADMIN") ? (
            <Grid item xs={4}>
              <Label2> Email :</Label2>
              <TextContent>
                {" "}
                {dataDetail?.metadata?.email ||
                  dataDetail?.metadata?.detailUser?.email ||
                  "-"}
              </TextContent>
            </Grid>
          ) : null}

          {dataDetail?.metadata?.roleCode?.includes("ADMIN") ? null : (
            <Grid item xs={4}>
              <Label2> Email :</Label2>

              <TextContent>
                {" "}
                {dataDetail?.metadata?.email ||
                  dataDetail?.metadata?.detailUser?.email ||
                  "-"}
              </TextContent>
            </Grid>
          )}
          <Grid item xs={4}>
            <Label2>Mobile Phone Number/HP :</Label2>
            <TextContent>
              {dataDetail?.metadata?.phoneNumber
                ? formatPhoneNumber(
                    dataDetail?.metadata?.phoneNumber ?? "",
                    18
                  ).replace(/^0/g, "+62 - ")
                : dataDetail?.metadata?.detailUser?.phoneNumber
                ? formatPhoneNumber(
                    dataDetail?.metadata?.detailUser?.phoneNumber ?? "",
                    18
                  ).replace(/^0/g, "+62 - ")
                : "-"}
            </TextContent>
          </Grid>
        </Grid>
      </div>
      {dataDetail?.metadata?.roleCode?.includes(
        "ADMIN"
      ) ? null : (dataDetail?.metadata?.roleCode?.split("_")[0] ||
          dataDetail?.metadata?.detailUser?.roleCode?.split("_")[0] !==
            "ADMIN") &&
        dataDetail?.additionalData?.corporateAuthorization?.toLowerCase() ===
          "multiple" ? (
        <div
          style={{ display: "flex", flexDirection: "column", marginTop: 20 }}
        >
          <TextContent>Notification Settings</TextContent>
          <Label2 style={{ marginTop: 20 }}>
            Notifications are active if :
          </Label2>
          {dataDetail?.metadata?.isGetTransactionProcessed ||
          dataDetail?.metadata?.detailUser?.isGetTransactionProcessed ? (
            <div style={{ marginTop: 4, paddingLeft: 16 }}>
              <div style={{ display: "flex", alignItems: "center", gap: 8 }}>
                <BlueSphere />
                <span style={{ fontFamily: "FuturaMdBT", color: "#374062" }}>
                  Transaction Processed
                </span>
              </div>
            </div>
          ) : (
            <span>-</span>
          )}
        </div>
      ) : null}
    </div>
  );
};

const DetailApprovalWorkflowPengaturanPengguna = () => {
  const dispatch = useDispatch();
  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComment] = React.useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="User Settings Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowPengaturanPengguna;
