import { Button, Grid, makeStyles } from "@material-ui/core";
import styled from "styled-components";
import Badge from "components/BN/Badge";
import TableMenuBaru from "components/BN/TableIcBB/TableMenuBaru";
import Colors from "helpers/colors";
import React, { useState } from "react";
import ScrollCustom from "components/BN/ScrollCustom";
import FormSectionTitle from "components/BN/Form/formSectionTitle";
import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right.svg";

import { useDispatch, useSelector } from "react-redux";
import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import { formatAmountDot } from "utils/helpers";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";
import PopupJenisRekening from "./components/popupJenisLayanan";

const Lable = styled.div`
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    marginTop: 20,
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  companyName: {
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: 20,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  category: {
    display: "flex",
    alignItems: "center",
    "& div:nth-child(1)": {
      fontFamily: "FuturaMdBT",
      fontSize: 12,
      color: Colors.dark.hard,
    },
    "& div:nth-child(2)": {
      fontFamily: "FuturaMdBT",
      fontSize: 12,
      color: Colors.gray.medium,
    },
  },
  pembayaran: {
    fontFamily: "FuturaHvBT",
    fontSize: 17,
    color: Colors.dark.hard,
    marginTop: 20,
    marginBottom: 5,
  },
  currency: {
    fontFamily: "FuturaMdBT",
    fontSize: 12,
    color: Colors.dark.hard,
  },
  content: {
    marginTop: 20,
    border: "1px solid #B3C1E7",
    borderRadius: 10,
    "& .content-title": {
      margin: "10px 20px 20px 20px",
      "& div:nth-child(1)": {
        fontFamily: "FuturaHvBT",
        fontSize: 15,
        color: Colors.dark.hard,
      },
      "& div:nth-child(2)": {
        marginTop: 10,
        fontFamily: "FuturaMdBT",
        fontSize: 13,
        color: Colors.dark.medium,
        "& span": {
          color: Colors.dark.hard,
        },
      },
    },
  },
  rightSection: {
    "& .jenis-layanan": {
      fontFamily: "FuturaHvBT",
      fontSize: 13,
      color: Colors.dark.medium,
    },
    textAlign: "right",
    "& .lihatsemua": {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      fontSize: 13,
      fontFamily: "FuturaMdBT",
      color: Colors.primary.hard,
      fontWeight: 700,
      "&:hover": {
        cursor: "pointer",
      },
    },
  },
  listGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    fontSize: 15,
  },
  bullets: {
    width: 7,
    height: 7,
    backgroundColor: "#0061A7",
    borderRadius: 10,
    marginRight: 10,
  },
  fiturPenggunaContainer: {
    marginTop: 20,
    borderBottom: "1px solid #EAF2FF",
    height: "auto",
    position: "relative",
    paddingBottom: 24,
  },
  containerDetail: {
    margin: "20px 0",
  },
});

const dataHeaderRow = () => [
  {
    title: "Currency Matrix",
    render: (rowData) => (
      <div>{rowData?.serviceCurrencyMatrix?.currencyMatrix?.name}</div>
    ),
  },

  {
    title: "Maximum Nominal / Day",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          // borderBottom: "1px solid #DDEBF9",
        }}
      >
        {formatAmountDot(rowData?.maxAmountTransactionOfDay?.toString())}
        <div style={{ width: "100%", marginTop: "5px" }}>
          <span
            style={{
              fontFamily: "FuturaMdBT",
              color: "#7B87AF",
              fontSize: "13px",
              fontWeight: 400,
            }}
          >
            Number of Transaction
          </span>{" "}
          <span> {rowData?.maxTransaction ?? "-"}</span>
        </div>
      </div>
    ),
  },

  {
    title: "Maximum Nominal Transaction",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          // borderBottom: "1px solid #DDEBF9",
        }}
      >
        {formatAmountDot(rowData?.maxAmountTransaction.toString())}
      </div>
    ),
  },
  {
    title: "Minimum Nominal Transaction",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          // borderBottom: "1px solid #DDEBF9",
        }}
      >
        {formatAmountDot(rowData?.minAmountTransaction.toString())}
      </div>
    ),
  },
];

const DetailData = ({
  detailData,
  detailAdditionalData,
  limitData,
  menuData,
}) => {
  const classes = useStyles();

  const [openModal, setOpenModal] = useState(false);

  const modalCloseHandler = () => setOpenModal(false);

  return (
    <div>
      <PopupJenisRekening
        open={openModal}
        handleCloseModal={modalCloseHandler}
      />

      <div className={classes.companyName}>
        {detailAdditionalData?.corporateName}
      </div>
      <div className={classes.category}>
        <Lable style={{ color: "#bcc8e7" }}>
          {detailAdditionalData?.corporateAuthorization} Authorization
        </Lable>
      </div>

      <div className={classes.containerDetail}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <div className={classes.labelDataUser}>Code :</div>
            <div className={classes.valueDataUser}>{detailData?.code}</div>
          </Grid>
          <Grid item xs={4}>
            <div className={classes.labelDataUser}>Group Name :</div>
            <div className={classes.valueDataUser}>{detailData?.name}</div>
          </Grid>
          <Grid item xs={4}>
            <div className={classes.labelDataUser}>Account Group :</div>
            <div className={classes.valueDataUser}>
              <Badge
                type="blue"
                label={detailAdditionalData?.accountGroupName}
                styleBadge={{ display: "inline-block" }}
              />
            </div>
          </Grid>
        </Grid>
      </div>

      <FormSectionTitle
        title="User Features"
        borderColor={Colors.dark.soft}
        color={Colors.dark.soft}
      />
      {menuData.map((data, i) => (
        <div
          key={`${data.parentName}${i}`}
          className={classes.fiturPenggunaContainer}
        >
          <div className={classes.detailTitle}>{data.parentName}</div>
          <Grid container spacing={3} style={{ marginLeft: 20 }}>
            {data?.child?.map((menuChild, i) => (
              <Grid
                key={`${menuChild}${i}`}
                item
                xs={6}
                style={{ padding: "10px 0 0 0" }}
              >
                <div style={{ display: "inline-block" }}>
                  <div className={classes.listGroup}>
                    <div className={classes.bullets} />
                    {menuChild}
                  </div>
                </div>
              </Grid>
            ))}
          </Grid>
        </div>
      ))}

      <FormSectionTitle
        title="Limit List"
        borderColor={Colors.dark.soft}
        color={Colors.dark.soft}
      />
      <ScrollCustom height={568}>
        {limitData?.map((data, i) => (
          <div key={`${data.serviceName}${i}`} className={classes.content}>
            <div className="content-title">
              <div>{data?.serviceName ?? "-"}</div>
            </div>
            <TableMenuBaru
              headerContent={dataHeaderRow()}
              dataContent={data?.child}
            />
          </div>
        ))}
      </ScrollCustom>
    </div>
  );
};

const getMenuList = (dataMenu) => {
  if (!dataMenu?.length) {
    return [];
  }
  const newDataMenu = [];
  dataMenu.forEach((item) => {
    const indexArray = newDataMenu.findIndex(
      (newMenu) => newMenu.parentName === item.parentName
    );
    if (indexArray === -1 && item.parentName !== item.menuName) {
      const newObject = { parentName: item.parentName, child: [] };
      newObject.child.push(item.menuName);
      newDataMenu.push(newObject);
    } else {
      newDataMenu[indexArray]?.child?.push(item.menuName);
    }
  });

  return newDataMenu;
};

const getLimitList = (dataLimit) => {
  if (!dataLimit?.length) {
    return [];
  }

  const newDataLimit = [];

  dataLimit.forEach((item) => {
    const indexArray = newDataLimit.findIndex(
      (newMenu) =>
        newMenu.serviceName === item?.serviceCurrencyMatrix?.service?.name
    );
    if (indexArray === -1) {
      const newObject = {
        serviceName: item?.serviceCurrencyMatrix?.service?.name,
        child: [],
      };
      newObject.child.push({ ...item });
      newDataLimit.push(newObject);
    } else {
      newDataLimit[indexArray]?.child?.push({ ...item });
    }
  });

  return newDataLimit;
};

const DetailApprovalWorkflowPengaturanKelompok = () => {
  const dispatch = useDispatch();

  const { dataDetail, isLoading } = useSelector(
    ({ aproverWorkFlow }) => aproverWorkFlow
  );
  const { metadata, additionalData } = dataDetail;
  const menuData = getMenuList(additionalData?.menuListDtos);
  const limitData =
    metadata?.activity !== "DELETE_WORKFLOW_GROUP"
      ? getLimitList(metadata?.corporateGlobalLimitDtosApprovalWorkflow)
      : getLimitList(metadata?.corporateGlobalLimitDtos);

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  const commentChangeHandler = (event) => setComment(event.target.value);

  return (
    <DetailTemplateApprovalWorkflow
      title="Group Setting Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={
        <DetailData
          detailData={metadata}
          detailAdditionalData={additionalData}
          menuData={menuData}
          limitData={limitData}
        />
      }
      comment={comment}
      setComment={commentChangeHandler}
      onApprove={onApprove}
      onReject={onReject}
      isLoading={isLoading}
      isLoadingExecute={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowPengaturanKelompok;
