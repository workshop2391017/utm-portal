import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";

import Badge from "components/BN/Badge";

import Colors from "helpers/colors";

import { formatAmountDot } from "utils/helpers";

import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";
import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const Title = styled.div`
  color: #2b2f3c;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: ${(props) => (props.subtitle ? "17px" : "20px")};
`;

const Lable = styled.div`
  color: ${(props) => (props.primary ? "#374062" : "#BCC8E7")};
  font-size: 12px;
  font-family: FuturaMdBT;
  font-weight: 400;
`;

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    wordWrap: "break-word",
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
  detailDataTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 16,
    color: Colors.dark.hard,
    margin: "20px 0",
  },
  levelTitle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
  },
});

const DataUser = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Username :</div>
        <div className={classes.valueDataUser}>Suseno Sulistyawan</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>ID :</div>
        <div className={classes.valueDataUser}>12345</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Date & Time :</div>
        <div className={classes.valueDataUser}>30 Jun 2021 | 18:42:32</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Branch :</div>
        <div className={classes.valueDataUser}>Yogyakarta</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Acitivy :</div>
        <div className={classes.valueDataUser}>Edit Data</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDataUser}>Status :</div>
        <div className={classes.valueDataUser}>
          <Badge
            type="blue"
            label="Waiting"
            styleBadge={{ display: "inline-block" }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();
  return (
    <div>
      <Title>{dataDetail?.additionalData?.corporateName}</Title>
      <div
        style={{
          display: "flex",
          marginBottom: 16,
        }}
      >
        <Lable>
          {dataDetail?.additionalData?.corporateAuthorization} Authorization
        </Lable>
      </div>
      <div className={classes.levelTitle} style={{ wordWrap: "break-word" }}>
        {dataDetail?.metadata?.name}
      </div>
      <div className={classes.detailDataTitle}>Authorization Limit Scheme</div>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Approval Level Name :</div>
          <div className={classes.valueDetail}>
            {dataDetail?.metadata?.name}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Currency :</div>
          <div className={classes.valueDetail}>
            {dataDetail?.metadata?.currency}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Maker Limit :</div>
          <div className={classes.valueDetail}>
            {formatAmountDot(dataDetail?.metadata?.limitMaker)}
          </div>
        </Grid>
      </Grid>
      <div className={classes.detailDataTitle}>Approval Limit</div>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>Single Approval Limit :</div>
          <div className={classes.valueDetail}>
            {formatAmountDot(dataDetail?.metadata?.limitSingleApproval)}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>
            Intra Group Approval Limit :
          </div>
          <div className={classes.valueDetail}>
            {formatAmountDot(dataDetail?.metadata?.limitIngroupApproval)}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.labelDetail}>
            Cross Group Approval Limit :
          </div>
          <div className={classes.valueDetail}>
            {formatAmountDot(dataDetail?.metadata?.limitOutsidegroupApproval)}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

const DetailApprovalWorkflowPengaturanLevel = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Level Setings Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowPengaturanLevel;
