import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Button, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import Colors from "helpers/colors";

import ScrollCustom from "components/BN/ScrollCustom";

import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right.svg";

import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";
import RegulatedServicesPopup from "./components/popupJenisLayanan";

const useStyles = makeStyles((theme) => ({
  withOrWithout: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 12,
    color: Colors.gray.medium,
  },
  viewButton: {
    ...theme.typography.backButton,
  },
  table: {
    height: 295,
    border: `1px solid ${Colors.gray.medium}`,
    borderRadius: 10,
    padding: "20px 0px",
  },
  tableHead: {
    height: 36,
    background: Colors.gray.ultrasoft,
    paddingLeft: 20,
  },
  tableRow: {
    height: 36,
    paddingLeft: 20,
    marginTop: 16,
    borderBottom: `1px solid ${Colors.neutral.gray.soft}`,
  },
}));

const DetailData = ({ dataDetail }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const renderGroupName = (string) =>
    string
      .split("_")
      .map((word) => `${word.charAt(0)}${word.slice(1).toLowerCase()}`)
      .join(" ");

  console.warn("ini dataDetail :", dataDetail);
  return (
    <Grid container direction="column">
      {/* -----Popups----- */}
      <RegulatedServicesPopup
        services={dataDetail?.metadata?.approvalMatrix}
        open={open}
        handleClose={() => setOpen(false)}
      />

      <Grid
        container
        item
        direction="column"
        xs="auto"
        style={{ marginBottom: 20 }}
      >
        <Grid item>
          <Typography variant="h2">
            {dataDetail?.additionalData?.detailCorporate?.corporateName}
          </Typography>
        </Grid>
        <Grid item>
          <span className={classes.withOrWithout}>
            {
              dataDetail?.additionalData?.detailCorporate
                ?.corporateAuthorization
            }{" "}
            Authorization
          </span>
        </Grid>
      </Grid>

      <Grid container item xs="auto" style={{ marginBottom: 20 }}>
        <Grid container item xs={6} direction="column">
          <Grid item>
            <Typography variant="h2">
              {dataDetail?.metadata?.approvalMatrix?.length === 1
                ? dataDetail?.metadata?.approvalMatrix[0]?.menuName
                : dataDetail?.metadata?.approvalMatrix?.find(({ menuName }) =>
                    menuName?.includes("All")
                  )?.menuName}
            </Typography>
          </Grid>
          {dataDetail?.metadata?.approvalMatrix?.some(({ menuName }) =>
            menuName?.includes("All")
          ) ? (
            <Grid item>
              <span
                className={classes.withOrWithout}
                style={{ color: Colors.dark.hard }}
              >
                {dataDetail?.metadata?.menuName}
              </span>
            </Grid>
          ) : null}
        </Grid>
        {dataDetail?.metadata?.approvalMatrix?.some(({ menuName }) =>
          menuName?.includes("All")
        ) ? (
          <Grid container item xs={6} alignItems="flex-end" direction="column">
            <Grid item>
              <Typography variant="body2" style={{ color: Colors.dark.medium }}>
                Type of Service Regulated:
              </Typography>
            </Grid>
            <Grid item>
              <Typography
                variant="subtitle1"
                style={{ color: Colors.dark.hard }}
              >
                {`${dataDetail?.metadata?.approvalMatrix?.length - 1} Services`}
              </Typography>
            </Grid>
            <Grid item>
              <Button
                component="button"
                size="small"
                endIcon={<ChevronRight />}
                className={classes.viewButton}
                onClick={() => setOpen(true)}
              >
                View All Service Types
              </Button>
            </Grid>
          </Grid>
        ) : null}
      </Grid>

      {/* -----Table----- */}
      <Grid container item direction="column" className={classes.table}>
        <Grid item xs="auto" style={{ paddingLeft: 20, marginBottom: 32 }}>
          <Typography variant="h6">
            {`${dataDetail?.metadata?.totalSequence} Number of Approvals`}
          </Typography>
        </Grid>

        {/* -----Table Head----- */}
        <Grid
          container
          alignItems="center"
          item
          xs="auto"
          className={classes.tableHead}
        >
          <Grid item xs={1}>
            <Typography variant="body2" style={{ color: Colors.dark.medium }}>
              Seq
            </Typography>
          </Grid>
          <Grid item xs={1}>
            <Typography variant="body2" style={{ color: Colors.dark.medium }}>
              User
            </Typography>
          </Grid>
          <Grid item xs={5}>
            <Typography variant="body2" style={{ color: Colors.dark.medium }}>
              Group
            </Typography>
          </Grid>
          <Grid item xs={5}>
            <Typography variant="body2" style={{ color: Colors.dark.medium }}>
              Group Target
            </Typography>
          </Grid>
        </Grid>

        {/* -----Table Row----- */}
        <ScrollCustom height={160}>
          {dataDetail?.metadata?.approvalMatrix[0]?.detail?.map((detail, i) => (
            <Grid
              key={i}
              container
              alignItems="center"
              item
              xs="auto"
              className={classes.tableRow}
            >
              <Grid item xs={1}>
                <Typography variant="subtitle1">
                  {detail?.sequenceNumber}
                </Typography>
              </Grid>
              <Grid item xs={1}>
                <Typography variant="subtitle1">
                  {detail?.totalApproval}
                </Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography variant="subtitle1">
                  {renderGroupName(detail?.groupType)}
                </Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography variant="subtitle1">
                  {detail?.workflowGroupName || "-"}
                </Typography>
              </Grid>
            </Grid>
          ))}
        </ScrollCustom>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowMatrixNonFinansial = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };
  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };
  return (
    <DetailTemplateApprovalWorkflow
      title="Non-Financial Matrix Settings Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData dataDetail={dataDetail} />}
      setComment={(e) => setComment(e.target.value)}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowMatrixNonFinansial;
