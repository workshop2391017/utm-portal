import React, { Fragment, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import { Button, CircularProgress, Grid, Typography } from "@material-ui/core";

import Badge from "components/BN/Badge";
import DeletePopup from "components/BN/Popups/Delete";
import FailedConfirmation from "components/BN/Popups/FailedConfirmation";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import TextField from "components/BN/TextField/AntdTextField";
import FormField from "components/BN/Form/InputGroupValidation";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import illustrationX from "assets/images/BN/illustrationred.png";

import {
  getDateApproval,
  getLocalStorage,
  validateMinMax,
} from "utils/helpers";

import Colors from "helpers/colors";

import {
  setTypeSuccesConfirmation,
  setSelectedApprove,
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
} from "stores/actions/aproverWorkFlow";

import { pathnameCONFIG } from "configuration";

import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  backButton: {
    ...theme.typography.backButton,
  },
  detailApprovaW: {
    paddingBottom: 40,
    backgroundColor: Colors.gray.ultrasoft,
  },
  container: {
    position: "relative",
    marginBottom: 80,
    margin: 30,
  },
  paper: {
    backgroundColor: "#fff",
    borderRadius: 10,
    minHeight: 200,
    padding: "18px 25px",
    position: "relative",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 17,
    fontWeight: "bold",
    marginBottom: 15,
  },
  buttongroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    height: 84,
    position: "fixed",
    bottom: 0,
    marginRight: 0,
    marginLeft: 200,
    paddingLeft: "50px",
    paddingRight: "50px",
    left: 0,
    right: 0,
  },
  fieldTitle: {
    fontSize: 13,
    fontWeight: 400,
    fontFamily: "FuturaBkBT",
    color: "#374062",
  },
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTittle: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    color: Colors.dark.hard,
    fontWeight: 900,
  },
  titleSubtitle: {
    color: Colors.dark.medium,
    fontFamily: "FuturaBkBT",
    fontSize: 13,
    marginLeft: 15,
  },
}));

const DetailTemplateApprovalWorkflow = ({
  title,
  renderDataUser,
  renderDetailData,
  onReject,
  onApprove,
  setComment,
  comment,
  isLoadingExecute,
  isLoading,
  isSuccess,
  userData,
}) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const {
    isOpen,
    isGetDetail,
    isReject,
    isLoading: isLoadingStore,
    isToReleaser,
    activeMenuSideBar,
  } = useSelector((state) => state.aproverWorkFlow);

  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: "",
  });
  const [failedConfirmModal, setFailedConfirmModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [aprroveModal, setApproveModal] = useState(false);

  const handleBack = () => {
    history.push(pathnameCONFIG.APPROVAL_WORKFLOW.BASE_URL);
    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  useEffect(() => {
    const reffNumber = getLocalStorage("reffNumber");
    const selectedApproverStorage = getLocalStorage("selectedApprover");
    const selectedApprover = selectedApproverStorage
      ? JSON.parse(selectedApproverStorage)
      : {};

    if (!isGetDetail) {
      if (!userData.createdUserName) {
        if (reffNumber) {
          const payload = {
            reffNo: reffNumber,
          };
          dispatch(setSelectedApprove(selectedApprover));
          dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));
        }
      }
    }
  }, []);

  useEffect(() => {
    if (isSuccess) {
      setSuccessConfirmModal({
        isOpen: true,
        message: "Activity Successfully Rejected",
      });
    }
  }, [isSuccess]);

  useEffect(() => {
    if (userData?.comment) {
      setComment({ target: { value: userData?.comment } });
    } else setComment({ target: { value: null } });
  }, [userData]);

  const getValueBadge = (status) => {
    if (status?.toLowerCase()?.includes("waiting")) {
      return { type: "blue", label: status };
    }

    if (status?.toLowerCase()?.includes("rejected")) {
      return { type: "red", label: status };
    }

    if (status?.toLowerCase()?.includes("approved")) {
      return { type: "green", label: status };
    }

    return { type: "", label: "" };
  };

  const statusType = getValueBadge(userData?.statusName);

  return (
    <div className={classes.detailApprovaW}>
      <Grid
        container
        direction="column"
        item
        style={{
          paddingLeft: 20,
          paddingTop: 8,
          marginBottom: 28,
        }}
      >
        <Grid item>
          <Button
            disableElevation
            component="button"
            size="small"
            startIcon={<ArrowLeft />}
            className={classes.backButton}
            onClick={handleBack}
          >
            Back
          </Button>
        </Grid>
        <Grid item>
          <Typography variant="h2">{title}</Typography>
        </Grid>
      </Grid>
      <div className={classes.container}>
        <div className={classes.paper} style={{ marginBottom: 20 }}>
          {isLoading ? (
            <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
              <div style={{ margin: "auto" }}>
                <CircularProgress color="primary" size={40} />
              </div>
            </div>
          ) : (
            <div
            // style={{
            //   position: "relative",
            //   height: "auto",
            //   minHeight: 181,
            // }}
            >
              <h1 className={classes.title}>User Data</h1>
              {/* <img
                src={''}
                alt="pattern"
                style={{ position: "absolute", right: 0, top: 0 }}
              /> */}

              <Grid
                container
                spacing={3}
                style={{ position: "absolute", zIndex: 2 }}
              >
                <Grid item xs={4}>
                  <div className={classes.labelDataUser}>Username :</div>
                  <div className={classes.valueDataUser}>
                    {userData?.createdUserName}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.labelDataUser}>NIP :</div>
                  <div className={classes.valueDataUser}>
                    {userData?.nipMaker}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.labelDataUser}>Date & Time :</div>
                  <div className={classes.valueDataUser}>
                    {getDateApproval(userData?.createdDate)}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.labelDataUser}>Office :</div>
                  <div className={classes.valueDataUser}>
                    {userData?.branchName}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.labelDataUser}>Activity :</div>
                  <div className={classes.valueDataUser}>
                    {userData?.activityName}
                  </div>
                </Grid>
                <Grid item xs={4}>
                  <div className={classes.labelDataUser}>Status :</div>
                  <div className={classes.valueDataUser}>
                    <Badge
                      type={statusType.type}
                      label={statusType.label}
                      styleBadge={{ display: "inline-block" }}
                      outline
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          )}
        </div>
        <div style={{ display: "flex" }}>
          <div style={{ width: "100%" }}>
            <div className={classes.paper}>
              {isLoading ? (
                <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
                  <div style={{ margin: "auto" }}>
                    <CircularProgress color="primary" size={40} />
                  </div>
                </div>
              ) : (
                <div>
                  <h1 className={classes.title}>Details</h1>

                  {renderDetailData}
                </div>
              )}
            </div>
          </div>
        </div>
        <div
          className={classes.paper}
          style={{ margin: "20px 0", minHeight: 190 }}
        >
          {/* <p style={{ marginBottom: 5 }}>Comment :</p> */}
          <FormField
            label="Comment :"
            error={!validateMinMax(comment, 1, 200)}
            errorMessage="Max 200 Character"
            margin={{ margin: "0" }}
          >
            <TextField
              type="textArea"
              value={comment}
              disabled={
                !userData?.statusName?.toLowerCase()?.includes("waiting")
              }
              onChange={setComment}
              placeholder="Comments"
              style={{ width: "100%", minHeight: 150 }}
            />
          </FormField>
        </div>
      </div>

      <div className={classes.buttongroup}>
        <ButtonOutlined
          label="Reject"
          width="86px"
          style={{ marginRight: 30 }}
          color="#0061A7"
          onClick={() => setDeleteModal(true)}
          disabled={
            isLoadingExecute ||
            !userData?.statusName?.toLowerCase()?.includes("waiting") ||
            isLoading ||
            !validateMinMax(comment, 1, 200) ||
            !userData?.isAllowTransaction
          }
        />
        <GeneralButton
          label={
            isLoadingExecute ? (
              <CircularProgress size={20} color="primary" />
            ) : (
              "Approve"
            )
          }
          onClick={() => setApproveModal(true)}
          disabled={
            isLoadingExecute ||
            !userData?.statusName?.toLowerCase()?.includes("waiting") ||
            isLoadingStore ||
            !validateMinMax(comment, 1, 200) ||
            !userData?.isAllowTransaction
          }
        />
      </div>
      <SuccessConfirmation
        img={isReject === 6 && illustrationX}
        isOpen={isOpen}
        handleClose={() => {
          setDeleteModal(false);
          setApproveModal(false);
          dispatch(setTypeSuccesConfirmation(false));
          history.push(pathnameCONFIG.APPROVAL_WORKFLOW.BASE_URL);
        }}
        title="Success"
        message={
          isReject === 6
            ? "Activity Successfully Rejected"
            : "Activity Successfully Approved"
        }
        submessage={!isToReleaser ? "Please wait for approval" : undefined}
      />
      <DeletePopup
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={onReject}
        message="Are You Sure To Reject the Activity?"
        loading={isLoadingStore}
      />

      <DeletePopup
        isOpen={aprroveModal}
        handleClose={() => setApproveModal(false)}
        onContinue={onApprove}
        message="Are You Sure To Approve the Activity?"
        loading={isLoadingStore}
      />

      <FailedConfirmation
        isOpen={failedConfirmModal}
        handleClose={() => setFailedConfirmModal(false)}
        title="Failed"
      />
    </div>
  );
};

DetailTemplateApprovalWorkflow.propTypes = {
  userData: PropTypes.object,
};

DetailTemplateApprovalWorkflow.defaultProps = {
  userData: {
    createdUserName: "",
    userPortalId: null,
    statusName: "",
    createdDate: "",
    branchName: "",
    activityName: "",
  },
};

export default DetailTemplateApprovalWorkflow;
