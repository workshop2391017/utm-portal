import { Grid, makeStyles } from "@material-ui/core";
import Badge from "components/BN/Badge";
import Colors from "helpers/colors";
import React, { Fragment, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  setTypeClearError,
  ExcuteDataApprovelWorkFlow,
} from "stores/actions/aproverWorkFlow";

import PaperTitle from "containers/BN/PaymentSetup/BillerManager/components/PaperTitle";
import { tableRow } from "containers/BN/PaymentSetup/BillerManager/Form/style";
import TableScroll from "components/BN/Table/TabbleScrollblue";
import GeneralTabs from "components/BN/Tabs/GeneralTabs";

import { ReactComponent as BlueDot } from "assets/icons/BN/blue-sphere.svg";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles((theme) => ({
  separator: {
    fontFamily: "FuturaMdBT",
    fontSize: "15px",
    color: Colors.dark.soft,
    borderBottom: `1px solid ${Colors.dark.soft}`,
    paddingBottom: "8px",
    marginBottom: "16px",
  },
  label: {
    ...theme.typography.subtitle1,
    color: Colors.dark.medium,
  },
  value: {
    fontFamily: "FuturaMdBT",
    fontSize: "15px",
    letterSpacing: "0.01em",
  },
}));

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  const [tab, setTab] = useState(0);
  const [templateList, setTemplateList] = useState([]);

  useEffect(() => {
    let list = [];
    switch (tab) {
      case 0:
        list = detailData?.billerTemplateList?.filter(
          ({ templateType }) => templateType === "I"
        );
        setTemplateList(list);
        break;
      case 1:
        list = detailData?.billerTemplateList?.filter(
          ({ templateType }) => templateType === "C"
        );
        setTemplateList(list);
        break;
      case 2:
        list = detailData?.billerTemplateList?.filter(
          ({ templateType }) => templateType === "R"
        );
        setTemplateList(list);
        break;
      case 3:
        list = detailData?.billerTemplateList?.filter(
          ({ templateType }) => templateType === "P"
        );
        setTemplateList(list);
        break;

      default:
        break;
    }
  }, [tab]);

  const dataHeader = [
    {
      title: "Value",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.denominationValue}</span>
      ),
    },
    {
      title: "Description ID",
      render: (rowData) => <span style={tableRow}>{rowData?.labelId}</span>,
    },
    {
      title: "Description EN",
      render: (rowData) => <span style={tableRow}>{rowData?.labelEn}</span>,
    },
    {
      title: "Amount",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.denominationCategory}</span>
      ),
    },
  ];

  const dataHeaderList = [
    {
      title: "Input Type",
      render: (rowData) => (
        <span style={tableRow}>
          {rowData?.inputType === "Y" ? "Receipt" : "-"}
        </span>
      ),
    },
    {
      title: "Line Number",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.lineNum || "-"}</span>
      ),
    },
    {
      title: "Min-Max Length",
      render: (rowData) => (
        <span style={tableRow}>
          {rowData?.minFieldLength && rowData?.maxFieldLength
            ? `${rowData?.minFieldLength}-${rowData?.maxFieldLength}`
            : "-"}
        </span>
      ),
    },
    {
      title: "Field Name ID",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.nameId || "-"}</span>
      ),
    },
    {
      title: "Field Name EN",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.nameEn || "-"}</span>
      ),
    },
    {
      title: "Placeholder ID",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.placeHolderId || "-"}</span>
      ),
    },
    {
      title: "Placeholder EN",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.placeHolderEn || "-"}</span>
      ),
    },
    {
      title: "Field Key",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.fieldKey || "-"}</span>
      ),
    },
    {
      title: "Field Type",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.fieldType || "-"}</span>
      ),
    },
  ];
  // console.log("detailData:", detailData);
  return (
    <div>
      <h6 className={classes.separator}>Biller Details</h6>
      <Grid container style={{ marginBottom: 16 }}>
        <Grid container direction="column" item xs={4}>
          <span className={classes.label}>Billers Category :</span>
          <span className={classes.value}>
            {detailData?.chooseCategoryData?.label || "-"}
          </span>
        </Grid>
        <Grid container direction="column" item xs={4}>
          <span className={classes.label}>Biller Name :</span>
          <span className={classes.value}>{detailData?.billerName || "-"}</span>
        </Grid>
        <Grid container direction="column" item xs={4}>
          <span className={classes.label}>Payee Code :</span>
          <span className={classes.value}>{detailData?.payeeCode || "-"}</span>
        </Grid>
      </Grid>
      <Grid container style={{ marginBottom: 8 }}>
        <Grid container direction="column" item xs={4}>
          <span className={classes.label}>Information :</span>
          <span className={classes.value}>-</span>
        </Grid>
        <Grid container direction="column" item xs={4}>
          <span className={classes.label}>Prefix Code :</span>
          <Grid container style={{ marginLeft: 20 }}>
            {detailData?.billerPrefixList?.map((item) => (
              <Grid
                key={`${item.billerCategory}${item.prefix}${item.id}`}
                item
                xs={4}
              >
                <BlueDot />
                <span className={classes.value} style={{ marginLeft: 8 }}>
                  {item.prefix}
                </span>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
      {detailData?.billerDenomList?.length > 0 && (
        <Fragment>
          <PaperTitle titleText="Denomination" />
          <br />
          <TableScroll
            cols={dataHeader}
            data={detailData.billerDenomList || []}
            bordered
            scroll
          />
        </Fragment>
      )}
      <br />
      <PaperTitle titleText="List Field" />
      <br />
      <GeneralTabs
        value={tab}
        tabs={["Input", "Confirmation", "Receipt", "Partial"]}
        onChange={(e, v) => setTab(v)}
      />
      <br />
      <TableScroll
        cols={dataHeaderList}
        data={
          templateList?.length > 0
            ? templateList
            : detailData?.billerTemplateList?.filter(
                ({ templateType }) => templateType === "I"
              )
        }
        bordered
        scroll
      />
    </div>
  );
};

const DetailApprovalWorkflowBillerManager = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const metadata = dataDetail?.metadata;

  const [comment, setComment] = useState("");

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };

  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Biller Manager details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={metadata} />}
      setComment={(e) => {
        setComment(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowBillerManager;
