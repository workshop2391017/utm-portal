import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Grid, makeStyles } from "@material-ui/core";

import Colors from "helpers/colors";

import { ExcuteDataApprovelWorkFlow } from "stores/actions/aproverWorkFlow";

import DetailTemplateApprovalWorkflow from "../DetailTemplate";

const useStyles = makeStyles({
  valueDataUser: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  labelDataUser: {
    fontFamily: "FuturaBkBT",
    color: Colors.dark.hard,
    fontSize: 13,
  },
  labelDetail: {
    color: Colors.dark.medium,
    fontSize: 13,
    fontFamily: "FuturaBQ",
  },
  valueDetail: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
  },
  detailTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    color: Colors.dark.hard,
    marginBottom: 5,
  },
});

const DetailData = ({ detailData }) => {
  const classes = useStyles();

  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Transaction Code :</div>
        <div className={classes.valueDetail}>{detailData?.tranCode || "-"}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Code :</div>
        <div className={classes.valueDetail}>{detailData?.code || "-"}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Category Name ID :</div>
        <div className={classes.valueDetail}>{detailData?.nameId || "-"}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Category Name EN :</div>
        <div className={classes.valueDetail}>{detailData?.nameEn || "-"}</div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Service Category :</div>
        <div className={classes.valueDetail}>
          {detailData?.serviceCategory === "1"
            ? "PURCHASE"
            : detailData?.serviceCategory === "2"
            ? "BILLPAYMENT"
            : detailData?.serviceCategory === "5"
            ? "BANCASSURANCE"
            : "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Description :</div>
        <div className={classes.valueDetail}>
          {detailData?.description || "-"}
        </div>
      </Grid>
      <Grid item xs={4}>
        <div className={classes.labelDetail}>Add Prefix Code :</div>
        <div className={classes.valueDetail}>
          {detailData?.usePrefix ? "Yes" : "No"}
        </div>
      </Grid>
    </Grid>
  );
};

const DetailApprovalWorkflowBillersCategory = () => {
  const dispatch = useDispatch();

  const { dataDetail } = useSelector((state) => state.aproverWorkFlow);

  const [comment, setComments] = useState("");

  const detailData = dataDetail?.metadata;

  const onReject = () => {
    dispatch(ExcuteDataApprovelWorkFlow("reject", comment));
  };

  const onApprove = () => {
    dispatch(ExcuteDataApprovelWorkFlow("approve", comment));
  };

  return (
    <DetailTemplateApprovalWorkflow
      title="Category Biller Details"
      userData={dataDetail?.userDataApprovalWorkflowDto}
      renderDetailData={<DetailData detailData={detailData} />}
      setComment={(e) => {
        setComments(e.target.value);
      }}
      comment={comment}
      onReject={onReject}
      onApprove={onApprove}
      isLoadingExecute={false}
      isLoading={false}
      isSuccess={false}
    />
  );
};

export default DetailApprovalWorkflowBillersCategory;
