import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React from "react";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeReffNumber,
  setTypeClearError,
} from "stores/actions/aproverWorkFlow";
import { useDispatch, useSelector } from "react-redux";
import ApprovalWfTableTemplate from "../tableTemplate";

// const dataDummyTableCheckingAccount = [
//   {
//     id: 1,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     // status: "1",
//     stat: 2,
//   },
//   {
//     id: 2,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     checked: true,
//     status: 1,
//     stat: 1,
//   },
//   {
//     id: 3,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     // checked: true,
//     status: 1,
//     stat: 0,
//   },
//   {
//     id: 4,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     stat: 2,
//   },
//   {
//     tglwaktu: "Bank",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     checked: true,
//     id: 5,
//     stat: 2,
//   },
//   {
//     id: 6,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     stat: 2,
//   },
// ];

const MenuKhusus = () => {
  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );
  const history = useHistory();
  const dispatch = useDispatch();
  // console.warn("data:", data);
  console.warn("dataFilterState:", dataFilter);

  // useEffect(() => {
  //   const { fromDate, toDate, workFlowStatus } = dataFilter;

  //   const payload = {
  //     fromDate,
  //     menuName: "CORPORATE_DETAIL_EDIT",
  //     page: 0,
  //     size: 10,
  //     toDate,
  //     workFlowStatus,
  //     id: null,
  //   };

  //   dispatch(getDataApprovelWorkFlowManagmentPerusahaan(payload));
  // }, [dataFilter]);

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };

    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));

    history.push(
      pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL.MENU_KHUSUS
    );
  };

  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />

      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={data?.pageNumber}
        totalPages={data?.totalPages}
        numberOfElements={data?.totalElements}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default MenuKhusus;
