import React from "react";
import ApprovalWfTableTemplate from "./tableTemplate";

const ApprovalWorkflowBlank = () => (
  <div>
    <ApprovalWfTableTemplate data={[]} />
  </div>
);

export default ApprovalWorkflowBlank;
