import { CircularProgress, makeStyles } from "@material-ui/core";
import { pathnameCONFIG } from "configuration";
import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  ExcuteDataBulkApprovelWorkFlow,
  getDataApprovelWorkFlow,
  setTypeActivatedMenu,
  setTypeBulkReffNumber,
  setTypeDataFilter,
  setTypeSuccesConfirmation,
} from "stores/actions/aproverWorkFlow";

import moment from "moment";
import { validateMinMax } from "utils/helpers";
import illustrationX from "assets/images/BN/illustrationred.png";
import Badge from "components/BN/Badge";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import TableICBB from "components/BN/TableIcBB";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import PopUpComment from "./PopUpComment";

const dataHeader = ({ onDetail, activeMenuSideBar }) => [
  {
    title: "Username",
    key: "username",
    width: 150,
    render: (rowData) => (
      <React.Fragment>
        <span>
          {rowData?.createdByName === "" || rowData?.createdByName === null
            ? "-"
            : rowData.createdByName}
        </span>
      </React.Fragment>
    ),
  },
  {
    title: "NIP",
    key: "nip",
    textAlign: "center",
    width: 100,
    render: (rowData) => (
      <React.Fragment>
        <span
          style={{
            textAlign: "center",
            width:
              rowData?.createdByNip === "" || rowData?.createdByNip === null
                ? "50%"
                : "auto",
          }}
        >
          {rowData?.createdByNip === "" || rowData?.createdByNip === null
            ? "-"
            : rowData.createdByNip}
        </span>
      </React.Fragment>
    ),
  },
  {
    title: "Role",
    width: 100,
    key: "role",
    render: (rowData) => (
      <React.Fragment>
        <span>
          {rowData?.roleMaker === "ADMIN_MAKER"
            ? "Maker"
            : rowData?.roleMaker === "ADMIN_APPROVER"
            ? "Approver"
            : rowData?.roleMaker === "ADMIN_MAKER_APPROVER"
            ? "Maker, Approver"
            : "-"}
        </span>
      </React.Fragment>
    ),
  },
  {
    title: "Date & Time",
    key: "date",
    width: 200,
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          gap: "5px",
          margin: "10px 0px 10px 0px",
        }}
      >
        <span>{moment(rowData.date).format("DD MMM YYYY")}</span>
        <span>{moment(rowData.date).format("HH:mm:ss")}</span>
      </div>
    ),
  },

  {
    title: "Activity",
    width: 200,
    // key: "activityName",
    render: (rowData) => (
      <span>
        {activeMenuSideBar.name === "Billers Category" &&
        rowData.activityName?.includes("Delete")
          ? "Inactive Data"
          : rowData.activityName}
        {/* {console.log("ini", rowData?.activityName, activeMenuSideBar)} */}
      </span>
    ),
  },
  {
    title: "Status",
    textAlign: "center",
    key: "status",
    width: 150,
    render: (rowData) => {
      const status = rowData?.status?.toLowerCase();
      return (
        <Badge
          type={
            status?.includes("rejected")
              ? "red"
              : status?.includes("approved")
              ? "green"
              : status?.includes("waiting")
              ? "blue"
              : ""
          }
          label={rowData.status}
          outline
        />
      );
    },
  },
  {
    title: "",
    textAlign: "center",
    key: "btnapp",
    width: 110,
    render: (rowData) => (
      <div>
        <GeneralButton
          label="View Details"
          width="76px"
          height="23px"
          style={{ marginRight: 11, fontSize: 9, padding: 0 }}
          onClick={() => onDetail(rowData?.reffNo)}
        />
      </div>
    ),
  },
];

const ApprovalWfTableTemplate = ({
  data,
  onDetail,
  isLoading,
  // page,
  totalPages,
  numberOfElements,
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [comment, setComment] = useState("");
  const [openComment, setOpenComment] = useState({
    isOpen: false,
    type: "",
  });
  const [rejectModal, setRejectModal] = useState(false);
  const [aprroveModal, setApproveModal] = useState(false);
  const {
    activeMenuSideBar,
    dataFilter,
    isReject,
    isOpen,
    isToReleaser,
    isLoadingExecute,
  } = useSelector((state) => state.aproverWorkFlow);

  const setPage = (page) => {
    dispatch(setTypeDataFilter({ ...dataFilter, pageSelected: page }));
  };

  const [dataContent, setDataContent] = useState([]);

  const disabledCondition = false;

  useEffect(() => {
    const dataMap = data?.map((item) => ({
      ...item,
      disabled:
        item?.isAllowTransaction === false ||
        item?.status === "Approved" ||
        item?.status === "Rejected" ||
        item?.statusName === "Approved" ||
        item?.statusName === "Rejected"
          ? true
          : disabledCondition,
    }));

    setDataContent(dataMap);
  }, [data]);

  const [selectedIds, setSelectedIds] = useState([]);

  const useStyles = makeStyles({
    buttonTolak: {
      fontFamily: "FuturaMdBT",
      borderRadius: "6px !important",
      border: "1px solid #0061A7",
      color: "#0061A7",
      marginRight: "25px",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
    buttonSetujui: {
      fontFamily: "FuturaMdBT",
      borderRadius: "6px !important",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
    buttonGroup: {
      display: "flex",

      "& .MuiButton-outlinedPrimary.Mui-disabled": {
        border: "1px solid #BCC8E7",
        color: "#BCC8E7",
        backgroundColor: "#fff",
      },
      "& .MuiButton-containedPrimary.Mui-disabled": {
        backgroundColor: "#BCC8E7",
        color: "#fff",
      },
    },
  });
  const classes = useStyles();

  useEffect(() => {
    const dataFilter = data?.filter((el) =>
      selectedIds.find((element) => element === el.id)
    );

    const dataSelected = dataFilter?.map((item) => ({
      referenceNumber: item?.reffNo,
    }));
    dispatch(setTypeBulkReffNumber(dataSelected));
  }, [selectedIds, data]);

  const onReject = () => {
    dispatch(ExcuteDataBulkApprovelWorkFlow("reject", comment));
    setRejectModal(false);
    setOpenComment({ isOpen: false, type: "" });
  };
  const onApprove = () => {
    dispatch(ExcuteDataBulkApprovelWorkFlow("approve", comment));
    setApproveModal(false);
    setOpenComment({ isOpen: false, type: "" });
  };

  return (
    <div className={classes.pengaturanTemplate}>
      <SuccessConfirmation
        img={isReject === 6 && illustrationX}
        isOpen={isOpen}
        handleClose={() => {
          setRejectModal(false);
          setApproveModal(false);
          dispatch(setTypeSuccesConfirmation(false));
          history.push(pathnameCONFIG.APPROVAL_WORKFLOW.BASE_URL);
          dispatch(getDataApprovelWorkFlow({ searchValue: "" }));
          setSelectedIds([]);
          setComment("");
        }}
        title="Success"
        message={
          isReject === 6
            ? `[${selectedIds.length}] Activity Successfully Rejected`
            : `[${selectedIds.length}] Activity Successfully Approved`
        }
        submessage="Some activity may need approval"
      />
      <DeletePopup
        isOpen={rejectModal}
        handleClose={() => setRejectModal(false)}
        onContinue={onReject}
        title="Confirmation"
        message="Are you sure to reject the activity?"
        submessage="You Can't undo this action"
        loading={isLoading}
      />

      <DeletePopup
        isOpen={aprroveModal}
        handleClose={() => setApproveModal(false)}
        onContinue={onApprove}
        title="Confirmation"
        message="Are you sure to approve the activity?"
        submessage="You Can't undo this action"
        loading={isLoading}
      />
      <div className={classes.container}>
        <TableICBB
          selecable={false}
          handleSelect={setSelectedIds}
          handleSelectAll={setSelectedIds}
          isLoading={isLoading}
          selectedValue={selectedIds}
          headerContent={dataHeader({ onDetail, activeMenuSideBar })}
          dataContent={dataContent}
          page={dataFilter.pageSelected}
          setPage={setPage}
          totalData={totalPages}
          totalElement={numberOfElements}
          // noDataMessage={
          //   <div>
          //     You Don&apos;t Have <br />
          //     Task List
          //   </div>
          // }
          extraComponents={
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <ButtonOutlined
                onClick={() => setOpenComment({ isOpen: true, type: "Reject" })}
                style={{ marginLeft: 220 }}
                label={
                  isLoadingExecute ? (
                    <CircularProgress size={20} color="primary" />
                  ) : selectedIds.length < 2 ? (
                    "Reject "
                  ) : (
                    "Reject All"
                  )
                }
                disabled={isLoadingExecute}
              />
              <GeneralButton
                onClick={() =>
                  setOpenComment({ isOpen: true, type: "Approve" })
                }
                style={{ marginRight: 30 }}
                label={
                  isLoadingExecute ? (
                    <CircularProgress size={20} color="primary" />
                  ) : selectedIds.length < 2 ? (
                    "Approve "
                  ) : (
                    "Approve All"
                  )
                }
                width="135px"
              />
            </div>
          }
        />
      </div>
      <PopUpComment
        value={comment}
        onChange={(e) => setComment(e.target.value)}
        open={openComment?.isOpen}
        handleClose={() => {
          setOpenComment(false);
          setComment("");
        }}
        disabled={!validateMinMax(comment, 1, 200)}
        onContinue={() => {
          if (openComment?.type === "Approve") {
            setApproveModal(true);
          } else {
            setRejectModal(true);
          }
        }}
      />
    </div>
  );
};

ApprovalWfTableTemplate.propTypes = {};

ApprovalWfTableTemplate.defaultProps = {};

export default ApprovalWfTableTemplate;
