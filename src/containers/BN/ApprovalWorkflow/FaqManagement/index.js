import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React from "react";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeReffNumber,
  setTypeClearError,
} from "stores/actions/aproverWorkFlow";
import { useDispatch, useSelector } from "react-redux";
import ApprovalWfTableTemplate from "../tableTemplate";

const FaqManagement = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };

    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));

    history.push(pathnameCONFIG.APPROVAL_WORKFLOW.FAQ.DETAIL);
  };

  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />

      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={data?.pageNumber}
        totalPages={data?.totalPages}
        numberOfElements={data?.totalElements}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default FaqManagement;
