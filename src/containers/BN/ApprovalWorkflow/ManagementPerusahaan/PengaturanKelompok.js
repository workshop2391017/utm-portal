import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeReffNumber,
  setTypeClearError,
} from "stores/actions/aproverWorkFlow";
import ApprovalWfTableTemplate from "../tableTemplate";

const PengaturanKelompok = () => {
  const history = useHistory();
  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );

  const dispatch = useDispatch();

  // useEffect(() => {
  //   const { fromDate, toDate, workFlowStatus } = dataFilter;

  //   const payload = {
  //     fromDate,
  //     menuName: "CORPORATE_ACCOUNT_GROUP",
  //     page: 0,
  //     size: 10,
  //     toDate,
  //     workFlowStatus,
  //     id: null,
  //   };

  //   dispatch(getDataApprovelWorkFlowManagmentPerusahaan(payload));
  // }, [dataFilter]);

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };

    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));

    history.push(
      pathnameCONFIG.APPROVAL_WORKFLOW.MANAGEMENT_PERUHSAAN_DETAIL
        .PENGATURAN_KELOMPOK
    );
  };
  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />

      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={data?.pageNumber}
        totalPages={data?.totalPages}
        numberOfElements={data?.totalElements}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default PengaturanKelompok;
