import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeClearError,
  setTypeEmpatyFilter,
  setTypeReffNumber,
} from "stores/actions/aproverWorkFlow";
import ApprovalWfTableTemplate from "../tableTemplate";

// const dataDummyTableCheckingAccount = [
//   {
//     id: 1,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     // status: "1",
//     stat: 2,
//   },
//   {
//     id: 2,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     checked: true,
//     status: 1,
//     stat: 1,
//   },
//   {
//     id: 3,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     // checked: true,
//     status: 1,
//     stat: 0,
//   },
//   {
//     id: 4,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     stat: 2,
//   },
//   {
//     tglwaktu: "Bank",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     checked: true,
//     id: 5,
//     stat: 2,
//   },
//   {
//     id: 6,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     stat: 2,
//   },
// ];

const KonfigurasiRekening = () => {
  const history = useHistory();
  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );

  const dispatch = useDispatch();

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };

    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));
    history.push(
      pathnameCONFIG.APPROVAL_WORKFLOW.MANAGEMENT_PERUHSAAN_DETAIL
        .KONFIGURASI_REKENING
    );
  };
  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />

      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={data?.pageNumber}
        totalPages={data?.totalPages}
        numberOfElements={data?.totalElements}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default KonfigurasiRekening;
