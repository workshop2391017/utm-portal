import React from "react";
import { configMenuNameAw } from "./menus";
import Service from "./GlobalMaintenance/service";
import LimitTransaksi from "./GlobalMaintenance/limitTransaksi";
import MenuGlobal from "./GlobalMaintenance/menuGlobal";
import ProdukRekening from "./GlobalMaintenance/produkRekening";
import DomesticBank from "./MaintenanceUmum/domesticBank";
import HandlingOfficer from "./MaintenanceUmum/handlingOfficer";
import HostErrorMapping from "./MaintenanceUmum/hostErrorMapping";
import InternationalBank from "./MaintenanceUmum/internationalBank";
import ChargeList from "./SegmentasiNasabah/chargeList";
import ChargePackage from "./SegmentasiNasabah/chargePackage";
import DaftarSegmentasi from "./SegmentasiNasabah/daftarSegmentasi";
import JenisRekeningProduk from "./SegmentasiNasabah/jenisRekeningProduk";
import LimitPackage from "./SegmentasiNasabah/limitPackage";
import MenuPackage from "./SegmentasiNasabah/menuPackage";
import SegmenCabang from "./SegmentasiNasabah/segmenCabang";
import ServiceChargeList from "./SegmentasiNasabah/serviceChargeList";
import TieringSlab from "./SegmentasiNasabah/tieringSlab";
import MenuKhusus from "./SegmentasiNasabah/menuKhusus";
import TypeParameter from "./SegmentasiNasabah/typeParameter";
import CompanyLimit from "./CompanyLimitCharge/CompanyLimit";
import CompanyCharge from "./CompanyLimitCharge/CompanyCharge";
import InformasiPerusahaan from "./ManagementPerusahaan/InformasiPerusahaan";
import PengelompokanRekening from "./ManagementPerusahaan/PengelompokanRekening";
import KonfigurasiRekening from "./ManagementPerusahaan/KonfigurasiRekening";
import PengaturanPenerimaTransfer from "./ManagementPerusahaan/PengaturanPenerimaTransfer";
import PengaturanSegmentasi from "./ManagementPerusahaan/PengaturanSegmentasi";
// import PengaturanAlurKerja from "./ManagementPerusahaan/PengaturanAlurKerja";
// import PengaturanTemplate from "./ManagementPerusahaan/PengaturanTemplate";
import PengaturanPengguna from "./ManagementPerusahaan/PengaturanPengguna";
import PengaturanKelompok from "./ManagementPerusahaan/PengaturanKelompok";
import MatrixFinansial from "./ManagementPerusahaan/MatrixFinansial";
import MatrixNonFinansial from "./ManagementPerusahaan/MatrixNonFinansial";
import PengaturanLevel from "./ManagementPerusahaan/PengaturanLevel";
import BillerManager from "./PaymentSetup/BillerManager";
import BillersCategory from "./PaymentSetup/BillersCategory";
import ApprovalWorkflowBlank from "./_blank";
import ListPromo from "./ManagementPromo/ListPromo";
import PromoCategoryWorkflow from "./ManagementPromo/PromoCategory";
import ListJenisPromo from "./ManagementPromo/ListJenisPromo";
import ListMerchant from "./ManagementPromo/ListMerchant";
import ContactSupportResponse from "./Utility/ContactSupportResponse";
import EventCalendar from "./Utility/EventDetail";
import ContentManagement from "./Utility/ContentManagement";
import ProductInformation from "./Utility/ProductInformation";
import BlastNotification from "./BlastNotification";
import UserSetting from "./Utility/UserSetting";
import FaqManagement from "./FaqManagement";

const RenderPagesAW = ({ activeMenu }) => {
  const getRenderPage = () => {
    switch (activeMenu) {
      case configMenuNameAw.UTILITAS.CONTACT_SUPPORT_RESPONSE.name:
        return <ContactSupportResponse />;
      case configMenuNameAw.UTILITAS.EVENT_CALENDAR.name:
        return <EventCalendar />;
      case configMenuNameAw.UTILITAS.CONTENT_MANAGEMENT.name:
        return <ContentManagement />;
      case configMenuNameAw.UTILITAS.PRODUCT_INFORMATION.name:
        return <ProductInformation />;

      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.INFORMASI_PERUSAHAAN.name:
        return <InformasiPerusahaan />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGELOMPOKAN_REKENING.name:
        return <PengelompokanRekening />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.KONFIGURASI_REKENING.name:
        return <KonfigurasiRekening />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_PENERIMA_TRANSFER
        .name:
        return <PengaturanPenerimaTransfer />;

      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_SEGMENTASI.name:
        return <PengaturanSegmentasi />;
      // case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_ALUR_KERJA.name:
      //   return <PengaturanAlurKerja />;
      // case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_TEMPLATE.name:
      //   return <PengaturanTemplate />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_PENGGUNA.name:
        return <PengaturanPengguna />;

      case configMenuNameAw.GLOBAL_MAINTENANCE.MENU_GLOBAL.name:
        return <MenuGlobal />;
      case configMenuNameAw.GLOBAL_MAINTENANCE.PRODUK_REKENING.name:
        return <ProdukRekening />;
      case configMenuNameAw.GLOBAL_MAINTENANCE.LIMIT_TRANSAKSI.name:
        return <LimitTransaksi />;
      case configMenuNameAw.GLOBAL_MAINTENANCE.SERVICE.name:
        return <Service />;

      case configMenuNameAw.MAINTENANCE_UMUM.DOMESTIC_BANK.name:
        return <DomesticBank />;
      case configMenuNameAw.MAINTENANCE_UMUM.HANDLING_OFFICER.name:
        return <HandlingOfficer />;
      case configMenuNameAw.MAINTENANCE_UMUM.HOST_ERROR_MAPPING.name:
        return <HostErrorMapping />;
      case configMenuNameAw.MAINTENANCE_UMUM.INTERNATIONAL_BANK.name:
        return <InternationalBank />;

      case configMenuNameAw.SEGMENTASI_NASABAH.CHARGE_LIST.name:
        return <ChargeList />;
      case configMenuNameAw.SEGMENTASI_NASABAH.CHARGE_PACKAGE.name:
        return <ChargePackage />;
      case configMenuNameAw.SEGMENTASI_NASABAH.DAFTAR_SEGMENTASI.name:
        return <DaftarSegmentasi />;
      case configMenuNameAw.SEGMENTASI_NASABAH.JENIS_REKENING_PRODUK.name:
        return <JenisRekeningProduk />;
      case configMenuNameAw.SEGMENTASI_NASABAH.LIMIT_PACKAGE.name:
        return <LimitPackage />;
      case configMenuNameAw.SEGMENTASI_NASABAH.MENU_PACKAGE.name:
        return <MenuPackage />;
      case configMenuNameAw.SEGMENTASI_NASABAH.SEGMEN_CABANG.name:
        return <SegmenCabang />;
      case configMenuNameAw.SEGMENTASI_NASABAH.SERVICE_CHARGE_LIST.name:
        return <ServiceChargeList />;
      case configMenuNameAw.SEGMENTASI_NASABAH.TIERING_SLAB.name:
        return <TieringSlab />;

      case configMenuNameAw.SEGMENTASI_NASABAH.MENU_KHUSUS.name:
        return <MenuKhusus />;
      case configMenuNameAw.SEGMENTASI_NASABAH.TYPE_PARAMETER.name:
        return <TypeParameter />;

      case configMenuNameAw.COMPANY_LIMIT_CHARGE.COMPANY_LIMIT.name:
        return <CompanyLimit />;
      case configMenuNameAw.COMPANY_LIMIT_CHARGE.COMPANY_CHARGE.name:
        return <CompanyCharge />;

      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_KELOMPOK.name:
        return <PengaturanKelompok />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.MATRIX_FINANSIAL.name:
        return <MatrixFinansial />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.MATRIX_NONFINANSIAL.name:
        return <MatrixNonFinansial />;
      case configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_LEVEL.name:
        return <PengaturanLevel />;
      case configMenuNameAw.MANAGAMENT_PROMO.LIST_PROMO.name:
        return <ListPromo />;
      case configMenuNameAw.MANAGAMENT_PROMO.PROMO_CATEGORY.name:
        return <PromoCategoryWorkflow />;
      case configMenuNameAw.MANAGAMENT_PROMO.LIST_JENIS_PROMO.name:
        return <ListJenisPromo />;
      case configMenuNameAw.MANAGAMENT_PROMO.LIST_MERCHANT.name:
        return <ListMerchant />;

      case configMenuNameAw.PAYMENT_SETUP.BILLER_MANAGER.name:
        return <BillerManager />;
      case configMenuNameAw.PAYMENT_SETUP.BILLER_CATEGORY.name:
        return <BillersCategory />;

      case configMenuNameAw.BLAST_NOTIFICATION.LIST_BLAST_NOTIFICATION.name:
        return <BlastNotification />;
      case configMenuNameAw.UTILITAS.USER_SETTINGS.name:
        return <UserSetting />;
      case configMenuNameAw.FAQ.FAQ_MANAGEMENT.name:
        return <FaqManagement />;
      default:
        return <ApprovalWorkflowBlank />;
    }
  };

  return <div>{getRenderPage()}</div>;
};

export default React.memo(RenderPagesAW);
