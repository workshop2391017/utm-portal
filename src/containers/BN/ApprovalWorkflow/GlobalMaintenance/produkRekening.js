import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeReffNumber,
  setTypeClearError,
} from "stores/actions/aproverWorkFlow";
import { useDispatch, useSelector } from "react-redux";
import ApprovalWfTableTemplate from "../tableTemplate";

// const dataDummyTableCheckingAccount = [
//   {
//     id: 1,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     // status: "1",
//     stat: 2,
//   },
//   {
//     id: 2,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     checked: true,
//     status: 1,
//     stat: 1,
//   },
//   {
//     id: 3,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     // checked: true,
//     status: 1,
//     stat: 0,
//   },
//   {
//     id: 4,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     stat: 2,
//   },
//   {
//     tglwaktu: "Bank",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     checked: true,
//     id: 5,
//     stat: 2,
//   },
//   {
//     id: 6,
//     tglwaktu: "30 Jun 2021 18:42:32",
//     idb: "1234",
//     namap: "Suseno Sulistyawan",
//     stat: 2,
//   },
// ];

const ProdukRekening = () => {
  const history = useHistory();

  const dispatch = useDispatch();

  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );

  console.warn("data:", data);

  console.warn("dataFilterState:", dataFilter);

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };
    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));
    history.push(
      pathnameCONFIG.APPROVAL_WORKFLOW.GLOBAL_MAINTENANCE_DETAIL.PRODUK_REKENING
    );
  };

  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />
      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={data?.pageNumber}
        totalPages={data?.totalPages}
        numberOfElements={data?.totalElements}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default ProdukRekening;
