import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeReffNumber,
  setTypeClearError,
} from "stores/actions/aproverWorkFlow";
import { useDispatch, useSelector } from "react-redux";
import ApprovalWfTableTemplate from "../tableTemplate";

// const dataDummyTableCheckingAccount = [
//   {
//     id: "16708",
//     date: "30 Jun 2021 18:42:32",
//     activityName: "Delete Data",
//     namap: "Suseno Sulistyawan",
//     status: "APPROVED",
//     stat: 2,
//   },
//   {
//     id: "16708",
//     date: "30 Jun 2021 18:42:32",
//     activityName: "Delete Data",
//     namap: "Suseno Sulistyawan",
//     status: "APPROVED",
//     stat: 2,
//   },
//   {
//     id: "16708",
//     date: "30 Jun 2021 18:42:32",
//     activityName: "Delete Data",
//     namap: "Suseno Sulistyawan",
//     status: "APPROVED",
//     stat: 2,
//   },
//   {
//     id: "16708",
//     date: "30 Jun 2021 18:42:32",
//     activityName: "Delete Data",
//     namap: "Suseno Sulistyawan",
//     status: "APPROVED",
//     stat: 2,
//   },
//   {
//     id: "16708",
//     date: "30 Jun 2021 18:42:32",
//     activityName: "Delete Data",
//     namap: "Suseno Sulistyawan",
//     status: "APPROVED",
//     stat: 2,
//   },
//   {
//     id: "16708",
//     date: "30 Jun 2021 18:42:32",
//     activityName: "Delete Data",
//     namap: "Suseno Sulistyawan",
//     status: "APPROVED",
//     stat: 2,
//   },
// ];

const BillerManager = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );

  console.warn("data:", data);

  console.warn("dataFilterState:", dataFilter);

  // useEffect(() => {
  //   const { fromDate, toDate, workFlowStatus } = dataFilter;

  //   const payload = {
  //     fromDate,
  //     menuName: "GLOBAL_LIMIT",
  //     page: 0,
  //     size: 10,
  //     toDate,
  //     workFlowStatus,
  //     id: null,
  //   };

  //   dispatch(getDataApprovelWorkFlowManagmentPerusahaan(payload));
  // }, [dataFilter]);

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };

    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));

    history.push(pathnameCONFIG.APPROVAL_WORKFLOW.PAYMENT_SETUP.BILLER_MANAGER);
  };

  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />

      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={1}
        totalPages={10}
        numberOfElements={6}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default BillerManager;
