import { pathnameCONFIG } from "configuration/index";

export const configMenuNameAw = {
  MANAJEMENT_PERUSAHAAN: {
    INFORMASI_PERUSAHAAN: {
      name: "Company Information",
      id: 1,
      workflowName: "CORPORATE_DETAIL_EDIT",
    },
    PENGELOMPOKAN_REKENING: {
      name: "Account Grouping",
      id: 2,
      workflowName: "CORPORATE_ACCOUNT_GROUP_PORTAL",
    },
    KONFIGURASI_REKENING: {
      name: "Account Configuration",
      id: 3,
      workflowName: "ACCOUNT_LIMIT_CONFIG_PORTAL",
    },
    PENGATURAN_PENERIMA_TRANSFER: {
      name: "Transfer Recipient Settings",
      id: 4,
      workflowName: "BENEFICIARY_CONFIG_ADD_PORTAL",
    },
    PENGATURAN_SEGMENTASI: {
      name: "Segmentation Settings",
      id: 5,
      workflowName: "CORPORATE_SEGMENTATION",
    },
    PENGATURAN_KELOMPOK: {
      name: "Group Settings",
      id: 6,
      workflowName: "WORKFLOW_GROUP_PORTAL",
    },
    PENGATURAN_PENGGUNA: {
      name: "User Settings",
      id: 7,
      workflowName: "USER_SETTING_PORTAL",
    },
    MATRIX_FINANSIAL: {
      name: "Financial Matrix",
      id: 8,
      workflowName: "APPROVAL_MATRIX_FINANCIAL",
    },
    MATRIX_NONFINANSIAL: {
      name: "Non-Financial Matrix",
      id: 9,
      workflowName: "APPROVAL_MATRIX_NON_FINANCIAL",
    },
    PENGATURAN_LEVEL: {
      name: "Level Settings",
      id: 10,
      workflowName: "WORKFLOW_LIMIT_SCHEME_PORTAL",
    },
  },
  MAINTENANCE_UMUM: {
    HOST_ERROR_MAPPING: {
      name: "Host Error Mapping",
      id: 1,
      workflowName: "HOST_ERROR_MAPPING",
    },
    DOMESTIC_BANK: {
      name: "Domestic Bank",
      id: 2,
      workflowName: "BANK_DOMESTIC",
    },
    INTERNATIONAL_BANK: {
      name: "International Bank",
      id: 3,
      workflowName: "INTERNATIONAL_BANK",
    },
    HANDLING_OFFICER: {
      name: "Handling Officer",
      id: 4,
      workflowName: "HANDLING_OFFICER",
    },
  },
  GLOBAL_MAINTENANCE: {
    MENU_GLOBAL: { name: "Global Menu", id: 1, workflowName: "MENU_GLOBAL" },
    PRODUK_REKENING: {
      name: "Account Products",
      id: 2,
      workflowName: "SAVE_ACCOUNT_PRODUCT",
    },
    LIMIT_TRANSAKSI: {
      name: "Transaction Limits",
      id: 3,
      workflowName: "GLOBAL_LIMIT",
    },
    SERVICE: { name: "Service", id: 4, workflowName: "SERVICE_GLOBAL_LIMIT" },
  },
  SEGMENTASI_NASABAH: {
    DAFTAR_SEGMENTASI: {
      name: "Segmentation List",
      id: 1,
      workflowName: "SEGMENTATION",
    },
    JENIS_REKENING_PRODUK: {
      name: "Product Account Type",
      id: 2,
      workflowName: "PRODUCT_TYPE_DETAIL",
    },
    LIMIT_PACKAGE: {
      name: "Limit Package",
      id: 3,
      workflowName: "LIMIT_PACKAGE",
    },
    CHARGE_LIST: {
      name: "Charge List",
      id: 4,
      workflowName: "CHARGES_LIST",
    },
    SERVICE_CHARGE_LIST: {
      name: "Service Charge List",
      id: 5,
      workflowName: "SERVICE_CHARGE_LIST",
    },
    CHARGE_PACKAGE: {
      name: "Charge Package",
      id: 6,
      workflowName: "CHARGE_PACKAGE",
    },
    MENU_PACKAGE: {
      name: "Menu Package",
      id: 7,
      workflowName: "MENU_PACKAGE",
    },
    TIERING_SLAB: { name: "Tiering/Slab", id: 8, workflowName: "TIERING" },
    SEGMEN_CABANG: {
      name: "Branch Segment",
      id: 9,
      workflowName: "SEGMENT_BRANCH",
    },
    MENU_KHUSUS: { name: "Special Menu", id: 10, workflowName: "SPECIAL_MENU" },
    TYPE_PARAMETER: {
      name: "Parameter Setting",
      id: 11,
      workflowName: "PARAMETER_SETTING",
    },
  },
  COMPANY_LIMIT_CHARGE: {
    COMPANY_LIMIT: {
      name: "Company Limit",
      id: 1,
      workflowName: "COMPANY_LIMIT",
    },
    COMPANY_CHARGE: {
      name: "Company Charge",
      id: 2,
      workflowName: "COMPANY_CHARGE",
    },
  },
  MANAGAMENT_PROMO: {
    LIST_PROMO: {
      name: "List Promo",
      id: 1,
      workflowName: "MANAGEMENT_PROMO",
    },
    PROMO_CATEGORY: {
      name: "Promo Category",
      id: 2,
      workflowName: "PROMO_CATEGORY",
    },
    LIST_JENIS_PROMO: {
      name: "List Jenis Promo",
      id: 3,
      workflowName: "LIST_JENIS_PROMO",
    },
    LIST_MERCHANT: {
      name: "List Merchant",
      id: 4,
      workflowName: "LIST_MERCHANT",
    },
  },
  BLAST_NOTIFICATION: {
    LIST_BLAST_NOTIFICATION: {
      name: "List Blast Promo",
      id: 1,
      workflowName: "BLAST_PROMO",
    },
  },
  PAYMENT_SETUP: {
    BILLER_MANAGER: {
      name: "Biller Manager",
      id: 1,
      workflowName: "BILLER_MANAGER",
    },
    BILLER_CATEGORY: {
      name: "Billers Category",
      id: 2,
      workflowName: "BILLER_CATEGORY",
    },
  },
  UTILITAS: {
    CONTACT_SUPPORT_RESPONSE: {
      name: "Contact Support Response",
      id: 1,
      workflowName: "CORPORATE_DETAIL_EDIT",
    },
    EVENT_CALENDAR: {
      name: "Event Calendar",
      id: 2,
      workflowName: "CALENDER_EVENT",
    },
    CONTENT_MANAGEMENT: {
      name: "Content Management",
      id: 3,
      workflowName: "CONTENT_MANAGEMENT",
    },
    PRODUCT_INFORMATION: {
      name: "Product Information",
      id: 4,
      workflowName: "INFORMATION_PRODUCT",
    },
    USER_SETTINGS: {
      name: "User Settings Utility",
      id: 5,
      workflowName: "PORTAL_USER_BLOCK_UNBLOCK",
    },
  },
  FAQ: {
    FAQ_MANAGEMENT: {
      name: "FAQ Management",
      id: 1,
      workflowName: "FAQ",
    },
  },
};

export const menuOptions = [
  {
    id: "sub1",
    name: "Utillity",
    idPortal: 367,
    submenu: [
      configMenuNameAw.UTILITAS.CONTACT_SUPPORT_RESPONSE,
      configMenuNameAw.UTILITAS.EVENT_CALENDAR,
      configMenuNameAw.UTILITAS.CONTENT_MANAGEMENT,
      configMenuNameAw.UTILITAS.PRODUCT_INFORMATION,
      configMenuNameAw.UTILITAS.USER_SETTINGS,
    ],
  },
  {
    id: "sub2",
    name: "Company Management",
    idPortal: 190,
    submenu: [
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.INFORMASI_PERUSAHAAN,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGELOMPOKAN_REKENING,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.KONFIGURASI_REKENING,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_PENERIMA_TRANSFER,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_SEGMENTASI,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_KELOMPOK,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_PENGGUNA,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.MATRIX_FINANSIAL,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.MATRIX_NONFINANSIAL,
      configMenuNameAw.MANAJEMENT_PERUSAHAAN.PENGATURAN_LEVEL,
    ],
  },
  {
    id: "sub3",
    name: "General Maintenance",
    idPortal: 194,
    submenu: [
      configMenuNameAw.MAINTENANCE_UMUM.HOST_ERROR_MAPPING,
      configMenuNameAw.MAINTENANCE_UMUM.DOMESTIC_BANK,
      configMenuNameAw.MAINTENANCE_UMUM.INTERNATIONAL_BANK,
      configMenuNameAw.MAINTENANCE_UMUM.HANDLING_OFFICER,
    ],
  },
  {
    id: "sub4",
    name: "Global Maintenance",
    idPortal: 192,
    submenu: [
      configMenuNameAw.GLOBAL_MAINTENANCE.MENU_GLOBAL,
      configMenuNameAw.GLOBAL_MAINTENANCE.PRODUK_REKENING,
      configMenuNameAw.GLOBAL_MAINTENANCE.LIMIT_TRANSAKSI,
      configMenuNameAw.GLOBAL_MAINTENANCE.SERVICE,
    ],
  },
  {
    id: "sub5",
    name: "Customer Segmentation",
    idPortal: 191,
    submenu: [
      configMenuNameAw.SEGMENTASI_NASABAH.DAFTAR_SEGMENTASI,
      configMenuNameAw.SEGMENTASI_NASABAH.JENIS_REKENING_PRODUK,
      configMenuNameAw.SEGMENTASI_NASABAH.LIMIT_PACKAGE,
      configMenuNameAw.SEGMENTASI_NASABAH.CHARGE_LIST,
      configMenuNameAw.SEGMENTASI_NASABAH.SERVICE_CHARGE_LIST,
      configMenuNameAw.SEGMENTASI_NASABAH.CHARGE_PACKAGE,
      configMenuNameAw.SEGMENTASI_NASABAH.MENU_PACKAGE,
      configMenuNameAw.SEGMENTASI_NASABAH.TIERING_SLAB,
      configMenuNameAw.SEGMENTASI_NASABAH.SEGMEN_CABANG,
      configMenuNameAw.SEGMENTASI_NASABAH.MENU_KHUSUS,
      configMenuNameAw.SEGMENTASI_NASABAH.TYPE_PARAMETER,
    ],
  },
  {
    id: "sub6",
    name: "Company Limits & Charges",
    idPortal: 193,
    submenu: [
      configMenuNameAw.COMPANY_LIMIT_CHARGE.COMPANY_LIMIT,
      configMenuNameAw.COMPANY_LIMIT_CHARGE.COMPANY_CHARGE,
    ],
  },
  {
    id: "sub7",
    name: "Management Promo",
    idPortal: 368,
    submenu: [
      configMenuNameAw.MANAGAMENT_PROMO.LIST_PROMO,
      // configMenuNameAw.MANAGAMENT_PROMO.LIST_JENIS_PROMO,
      configMenuNameAw.MANAGAMENT_PROMO.PROMO_CATEGORY,
      // configMenuNameAw.MANAGAMENT_PROMO.LIST_MERCHANT,
    ],
  },
  {
    id: "sub8",
    name: "Payment Setup",
    submenu: [
      configMenuNameAw.PAYMENT_SETUP.BILLER_MANAGER,
      configMenuNameAw.PAYMENT_SETUP.BILLER_CATEGORY,
    ],
  },
  {
    id: "sub9",
    name: "Blast Promo",
    idPortal: 389,
    submenu: [configMenuNameAw.BLAST_NOTIFICATION.LIST_BLAST_NOTIFICATION],
  },
  {
    id: "sub10",
    name: "FAQ Download",
    submenu: [configMenuNameAw.FAQ.FAQ_MANAGEMENT],
  },
];
