// main
import React, { useEffect, useState } from "react";
import { Card, makeStyles } from "@material-ui/core";

// components
import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import MenuList from "components/BN/MenuList";
import Filter from "components/BN/Filter/GeneralFilter";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import {
  setTypeDataFilter,
  setSelectedApprove,
  getDataApprovelWorkFlow,
  // setPageAction,
  setBarMenuApprovelWorkFlow,
  setTypeActivatedMenu,
  setData,
  // cleanFilter,
} from "stores/actions/aproverWorkFlow";
import {
  getLocalStorage,
  searchDateEnd,
  searchDateStart,
  setLocalStorage,
} from "utils/helpers";
import useDebounce from "utils/helpers/useDebounce";
import Colors from "helpers/colors";

// asset
import NoData from "assets/images/BN/illustrationred.png";

import RenderPagesAW from "./renderPages";
import { menuOptions } from "./menus";

const ApprovalWorkflow = (props) => {
  const useStyles = makeStyles((theme) => ({
    approvalWorkflow: {
      marginBottom: 50,
    },
    container: {
      paddingTop: 10,
    },
    textbackground: {
      background: "#fff",
      marginTop: "-3px",
      width: "100%",
      height: "100px",
      borderRadius: "0px 0px 20px 20px",
    },
    root: {
      width: "100%",
      maxWidth: 360,
      color: "#0061A7",
      backgroundColor: "primary",
    },
    nested: {
      color: "#0061A7",
      paddingLeft: theme.spacing(4),
    },
    headerdokumen: {
      fontWeight: 500,
      fontFamily: "FuturaHvBT",
      fontSize: 15,
      background: "white",
      color: Colors.dark.hard,
      paddingLeft: "20px",
      padding: "10px",
      width: "100%",
      float: "left",
      marginBottom: "20px",
      height: "40px",
      borderRadius: "20px 20px 0px 0px",
    },
    paper: {
      backgroundColor: "#fff",
      width: "730",
      height: "1075",
      borderRadius: 20,
      border: "1px solid #fff",
    },
    tableBlank: {
      width: "100%",
      height: 416,
      boxShadow: `0px 3px 10px rgba(188, 200, 231, 0.2)`,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
    },
    paperpt: {
      backgroundColor: "#fff",
      width: "100%",
      height: "554",
      paddingBottom: 30,
      borderRadius: 10,
      overflow: "hidden",
      border: "1px solid #fff",
      [theme.breakpoints.up("md")]: {
        width: 280,
      },
      [theme.breakpoints.up("lg")]: {
        width: 300,
      },
      [theme.breakpoints.up("xl")]: {
        width: 300,
      },
    },
    title: {
      fontWeight: 900,
      fontFamily: "FuturaHvBT",
      fontSize: 24,
      color: "#0061A7",
      margin: "28px 30px 20px",
    },
    edit: {
      position: "absolute",
      right: 0,
      top: "50%",
      transform: "translateY(-50%)",
      padding: 8,
    },
    access: {
      // border: "1px solid #770055",
      // ini border dalam
      minHeight: 400,
      maxHeight: 1447,
      margin: "0 20px",
      padding: "0 10px",
      overflow: "auto",
      "&::-webkit-scrollbar": {
        display: "none",
      },
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    check: {
      fontSize: 15,
      color: "#374062",
      "& .ant-checkbox": {
        width: 20,
        height: 20,
        borderRadius: 3,
        "& .ant-checkbox-inner": {
          width: 20,
          height: 20,
          borderRadius: 3,
          border: "1px solid #BCC8E7",
          "&::after": {
            width: 6.5,
            height: 11.17,
            top: "42%",
          },
        },
      },
      "& .ant-checkbox-checked": {
        "& .ant-checkbox-inner": {
          border: "1px solid #0061A7 !important",
        },
      },
      "& .ant-checkbox-indeterminate": {
        "& .ant-checkbox-inner::after": {
          width: "10px !important",
          height: "10px !important",
          top: "50% !important",
        },
      },
    },
  }));

  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    approveSelected,
    pageSelected,
    dataSidebar,
    activeMenuSideBar,
    dataFilter: dataFilterStore,
    isLoading,
  } = useSelector((state) => state.aproverWorkFlow);

  const [tableSearch, setTableSearch] = useState(null);
  const [dataSearch, setDataSearch] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [activeMenu, setActiveMenu] = useState("");
  const [dataFilter, setDataFilter] = useState(null);
  const [menuOpt, setMenuOptions] = useState();

  const filter = (array, text) => {
    const getSubmenu = (result, object) => {
      if (object?.name?.toLowerCase()?.includes(text?.toLowerCase())) {
        result.push(object);
        return result;
      }
      if (Array.isArray(object.submenu)) {
        const submenu = object.submenu.reduce(getSubmenu, []);
        if (submenu.length) result.push({ ...object, submenu });
      }
      return result;
    };

    return array.reduce(getSubmenu, []);
  };

  useEffect(() => {
    const dataPortalLogin = getLocalStorage("portalUserLogin")
      ? JSON.parse(getLocalStorage("portalUserLogin"))
      : {};
    const dataMenu = dataPortalLogin?.portalGroupDetailDtoList[0]?.masterMenu;
    const menuApproval = dataMenu?.find((e) => e.id === 120);
    const resultMap = [];
    menuOptions?.forEach((item) => {
      if (menuApproval?.menuAccessChild.length > 0) {
        const indexMenu = menuApproval?.menuAccessChild.findIndex(
          (menu) => menu.id === item.idPortal
        );
        // if (indexMenu !== -1) {
        resultMap.push({
          ...item,
          key: `sub-${item?.id}`,
          submenu: item?.submenu.map((val) => ({
            ...val,
            key: `submenu-${item?.id}-${val.id}`,
          })),
        });
        // }
      }
    });

    if (dataSearch) {
      const res = filter(resultMap, dataSearch);
      setMenuOptions(res);
    } else setMenuOptions(resultMap);
  }, [dataSearch]);

  const indikasiStatus = (params) => {
    switch (params?.tabs?.tabValue3) {
      case 1:
        return "WAITING";

      case 2:
        return "REJECTED";

      case 3:
        return "APPROVED";

      default:
        return null;
    }
  };

  const searchDebounce = useDebounce(tableSearch, 1300);

  useEffect(() => {
    if (approveSelected.name || searchDebounce) {
      dispatch(
        getDataApprovelWorkFlow({
          searchValue: searchDebounce,
        })
      );
    }
  }, [approveSelected, dataFilterStore, searchDebounce]);

  useEffect(() => {
    if (activeMenu) {
      setDataFilter(null);
    }
  }, [activeMenu]);

  useEffect(() => {
    if (dataFilter !== null) {
      const payload = {
        ...dataFilterStore,
        fromDate: dataFilter?.date?.dateValue1
          ? searchDateStart(dataFilter?.date?.dateValue1)
          : null,
        toDate: dataFilter?.date?.dateValue2
          ? searchDateEnd(dataFilter?.date?.dateValue2)
          : null,
        workFlowStatus: indikasiStatus(dataFilter),
        pageSelected: 1,
      };

      dispatch(setTypeDataFilter(payload));
    }
  }, [dataFilter]);

  return (
    <div className={classes.approvalWorkflow}>
      <Title label="Approval Workflow">
        <Search
          dataSearch={tableSearch}
          setDataSearch={setTableSearch}
          placeholder="NIP"
          placement="left"
        />
      </Title>
      <div
        style={{
          padding: 20,
        }}
      >
        <Filter
          style={{ marginBottom: "20px" }}
          dataFilter={dataFilter}
          setDataFilter={(e) => {
            setDataFilter(e);
          }}
          align="left"
          options={[
            {
              id: 1,
              type: "datePicker",
              placeholder: "Start Date",
            },
            {
              id: 2,
              type: "datePicker",
              placeholder: "End Date",
              disabled: true,
            },
            {
              id: 3,
              type: "tabs",
              options: ["All", "Waiting", "Rejected", "Approved"],
            },
            {
              id: 4,
              type: "button",
              options: ["Setuju", "Tolak"],
            },
          ]}
        />
        <div className={classes.container}>
          <div
            style={{
              display: "flex",
              gap: 20,
            }}
          >
            <div style={{ marginTop: 20, width: "auto" }}>
              <div className={classes.paperpt}>
                <div className={classes.headerdokumen}>Menu</div>
                <div
                  style={{
                    marginLeft: "10px",
                    marginBottom: "20px",
                    paddingRight: 12,
                  }}
                >
                  <Search
                    options={["Peran", "Akses"]}
                    dataSearch={dataSearch}
                    setDataSearch={setDataSearch}
                    style={{ width: "100%" }}
                    trigger="none"
                  />
                </div>

                {menuOpt?.length === 0 && (
                  <Card className={classes.tableBlank}>
                    <div>
                      <div className={classes.noDataImg}>
                        <img src={NoData} alt="no data" />
                      </div>
                      <div className={classes.noDataMessage}>No Data</div>
                    </div>
                  </Card>
                )}

                <MenuList
                  // handle one click
                  disabled={isLoading}
                  // withHeader="ASAASASASAS"
                  isApprovalWorkflow
                  selectedKeys={dataSidebar}
                  menuOptions={menuOpt}
                  openKeys={dataSidebar}
                  onTitleClick={(e, selected) => {
                    dispatch(setBarMenuApprovelWorkFlow(selected));
                  }}
                  onSubClick={(e, keySelected, _, keys) => {
                    setActiveMenu(e.name);
                    // console.log("menuSelected", e);
                    dispatch(setSelectedApprove(e));
                    setLocalStorage("selectedApprover", JSON.stringify(e));
                    dispatch(setTypeActivatedMenu(e));
                    dispatch(setBarMenuApprovelWorkFlow(keys));
                    dispatch(setData({}));
                    dispatch(setTypeDataFilter({}));
                  }}
                />
              </div>
            </div>
            <div
              className={classes.container}
              style={{
                width: "100%",
                overflowX: "hidden",
                height: "auto",
              }}
            >
              <RenderPagesAW activeMenu={activeMenuSideBar?.name} />
            </div>
          </div>
        </div>
      </div>

      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => {
          setConfirmSuccess(false);
        }}
      />
    </div>
  );
};

ApprovalWorkflow.propTypes = {};

ApprovalWorkflow.defaultProps = {};

export default ApprovalWorkflow;
