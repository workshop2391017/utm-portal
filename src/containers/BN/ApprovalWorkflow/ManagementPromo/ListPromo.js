import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import React from "react";
import { useHistory } from "react-router-dom";
import {
  getDataApprovelWorkFlowManagmentPerusahaanDetail,
  setTypeClearError,
  setTypeReffNumber,
} from "stores/actions/aproverWorkFlow";
import { useDispatch, useSelector } from "react-redux";
import ApprovalWfTableTemplate from "../tableTemplate";

const ListPromo = () => {
  const { data, isLoading, error, dataFilter } = useSelector(
    (state) => state.aproverWorkFlow
  );
  const history = useHistory();
  const dispatch = useDispatch();

  const onDetail = (params) => {
    const payload = {
      reffNo: params,
    };

    dispatch(setTypeReffNumber(params));
    dispatch(getDataApprovelWorkFlowManagmentPerusahaanDetail(payload));
    history.push(pathnameCONFIG.APPROVAL_WORKFLOW.MANAGEMENT_PROMO.PROMO);
  };

  return (
    <React.Fragment>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setTypeClearError())}
      />

      <ApprovalWfTableTemplate
        data={data?.workflowApprovalDtoList}
        isLoading={isLoading}
        page={data?.pageNumber}
        totalPages={data?.totalPages}
        numberOfElements={data?.totalElements}
        onDetail={(params) => onDetail(params)}
      />
    </React.Fragment>
  );
};

export default ListPromo;
