import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { makeStyles, Button } from "@material-ui/core";

import useInput from "hooks/use-input";

import ConfirmationPopup from "components/BN/Popups/Delete";
import SuccessPopup from "components/BN/Popups/SuccessConfirmation";
import Footer from "components/BN/Footer";

import { ReactComponent as TrashRed } from "assets/icons/BN/TrashRed.svg";

import timeFormatHandler from "helpers/formatTime/format-time";
import Colors from "helpers/colors";

import {
  actionCutOffTimeGetCurrencies,
  actionCutOffTimeSave,
  setSuccess,
} from "stores/actions/cutOffTime";

import Description from "../components/Description/Description";
import ServiceAndTransferTypes from "../components/ServiceAndTransferTypes/ServiceAndTransferTypes";
import FormHeader from "../components/FormHeader/FormHeader";
import CutOffTime from "../components/CutOffTime/CutOffTime";
import Header from "../components/Header/Header";
import RemittanceInputs from "../components/RemittanceInputs/RemittanceInputs";

export const useStyles = makeStyles((theme) => ({
  section: {
    background: "#F4F7FB",
  },
  main: {
    marginTop: 20,
    paddingBottom: 60,
    display: "flex",
    justifyContent: "center",
  },
  form: {
    width: 840,
    height: "auto",
    padding: "30px 40px",
    backgroundColor: "white",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    borderRadius: 20,
  },
  cutOffTime: {
    ...theme.typography.h1,
    display: "flex",
    alignItems: "center",
    padding: "0 15px",
    marginTop: 20,
    fontSize: 17,
    height: 40,
    color: "#374062",
    backgroundColor: "#F4F7FB",
  },
  clearButton: {
    ...theme.typography.actionButton,
    color: Colors.warning.hard,
    marginLeft: "auto",
  },
}));

const timeValidateHandler = (value) => {
  if (!value) {
    return false;
  }
  if (value.length === 5) {
    const hourIsValid = Number(value.split(":")[0]) <= 24;
    const minuteIsValid = Number(value.split(":")[1]) <= 59;
    return hourIsValid && minuteIsValid;
  }
  return false;
};

const conditionalTimeValidateHandler = (value) => {
  if (!value) {
    return true;
  }
  if (value.length === 5) {
    const hourIsValid = Number(value.split(":")[0]) <= 24;
    const minuteIsValid = Number(value.split(":")[1]) <= 59;
    return hourIsValid && minuteIsValid;
  }
  return false;
};

export default function PengaturanCutOffTimeForm(props) {
  const { onChangePage, onSwitchForm, activePage, date, onFilterReset } = props;

  const { pageNumber, rowData } = activePage;
  const { isSingle, serviceType } = rowData;

  const classes = useStyles();
  const dispatch = useDispatch();

  const { isSuccess, currencies, isLoading } = useSelector(
    ({ cutOffTime }) => cutOffTime
  );

  const [formDataRemittance, setFormDataRemittance] = useState([]);
  const [openConfirmation, setOpenConfirmation] = useState(false);
  const [mainSwitch, setMainSwitch] = useState(false);
  const [breakTimeToggle, setBreakTimeToggle] = useState(false);
  const [breakTimeToggleConditional, setBreakTimeToggleConditional] =
    useState(false);

  const {
    value: startTime,
    isValid: startTimeIsValid,
    isError: startTimeIsError,
    valueChangeHandler: startTimeChangeHandler,
    inputBlurHandler: startTimeBlurHandler,
    resetHandler: startTimeResetHandler,
  } = useInput(timeValidateHandler, timeFormatHandler);

  const {
    value: startBreakTime,
    isValid: startBreakTimeIsValid,
    isError: startBreakTimeIsError,
    valueChangeHandler: startBreakTimeChangeHandler,
    inputBlurHandler: startBreakTimeBlurHandler,
    resetHandler: startBreakTimeResetHandler,
  } = useInput(timeValidateHandler, timeFormatHandler);

  const {
    value: endTime,
    isValid: endTimeIsValid,
    isError: endTimeIsError,
    valueChangeHandler: endTimeChangeHandler,
    inputBlurHandler: endTimeBlurHandler,
    resetHandler: endTimeResetHandler,
  } = useInput(timeValidateHandler, timeFormatHandler);

  const {
    value: endBreakTime,
    isValid: endBreakTimeIsValid,
    isError: endBreakTimeIsError,
    valueChangeHandler: endBreakTimeChangeHandler,
    inputBlurHandler: endBreakTimeBlurHandler,
    resetHandler: endBreakTimeResetHandler,
  } = useInput(timeValidateHandler, timeFormatHandler);

  const {
    value: conditionalStartTime,
    isValid: conditionalStartTimeIsValid,
    isError: conditionalStartTimeIsError,
    valueChangeHandler: conditionalStartTimeChangeHandler,
    inputBlurHandler: conditionalStartTimeBlurHandler,
    resetHandler: conditionalStartTimeResetHandler,
  } = useInput(
    mainSwitch ? conditionalTimeValidateHandler : timeValidateHandler,
    timeFormatHandler
  );

  const {
    value: conditionalStartBreakTime,
    isValid: conditionalStartBreakTimeIsValid,
    isError: conditionalStartBreakTimeIsError,
    valueChangeHandler: conditionalStartBreakTimeChangeHandler,
    inputBlurHandler: conditionalStartBreakTimeBlurHandler,
    resetHandler: conditionalStartBreakTimeResetHandler,
  } = useInput(
    mainSwitch ? conditionalTimeValidateHandler : timeValidateHandler,
    timeFormatHandler
  );

  const {
    value: conditionalEndTime,
    isValid: conditionalEndTimeIsValid,
    isError: conditionalEndTimeIsError,
    valueChangeHandler: conditionalEndTimeChangeHandler,
    inputBlurHandler: conditionalEndTimeBlurHandler,
    resetHandler: conditionalEndTimeResetHandler,
  } = useInput(
    mainSwitch ? conditionalTimeValidateHandler : timeValidateHandler,
    timeFormatHandler
  );

  const {
    value: conditionalEndBreakTime,
    isValid: conditionalEndBreakTimeIsValid,
    isError: conditionalEndBreakTimeIsError,
    valueChangeHandler: conditionalEndBreakTimeChangeHandler,
    inputBlurHandler: conditionalEndBreakTimeBlurHandler,
    resetHandler: conditionalEndBreakTimeResetHandler,
  } = useInput(
    mainSwitch ? conditionalTimeValidateHandler : timeValidateHandler,
    timeFormatHandler
  );

  const { value: description, valueChangeHandler: descriptionChangeHandler } =
    useInput();

  useEffect(() => {
    console.log("ini formDataRemittance", formDataRemittance);
  }, [formDataRemittance]);

  useEffect(() => {
    // populate form logic
    if (serviceType && pageNumber === 3) {
      if (rowData.desc) {
        descriptionChangeHandler(rowData.desc);
      }
      if (rowData.startTime) {
        setMainSwitch(true);
        startTimeChangeHandler(rowData.startTime);
        endTimeChangeHandler(rowData.endTime);
      }
      if (rowData.startBreakTime) {
        setBreakTimeToggle(true);
        startBreakTimeChangeHandler(rowData.startBreakTime);
        endBreakTimeChangeHandler(rowData.endBreakTime);
      }
      if (rowData.conditionalStartTime) {
        conditionalStartTimeChangeHandler(rowData.conditionalStartTime);
        conditionalEndTimeChangeHandler(rowData.conditionalEndTime);
      }
      if (rowData.conditionalStartBreakTime) {
        setBreakTimeToggleConditional(true);
        conditionalStartBreakTimeChangeHandler(
          rowData.conditionalStartBreakTime
        );
        conditionalEndBreakTimeChangeHandler(rowData.conditionalEndBreakTime);
      }
    } else if (!serviceType && rowData) {
      dispatch(actionCutOffTimeGetCurrencies({}));

      const hasNormalCOT = rowData.some(({ startTime }) => startTime);

      if (hasNormalCOT) setMainSwitch(true);

      const newArray = rowData.map((item) => ({
        ...item,
        breakTimeToggle: (item.startBreakTime && true) || false,
        breakTimeToggleConditional:
          (item.conditionalStartBreakTime && true) || false,
        isConditionalCOTHidden: false,
        isNormalCOTHidden: false,
        conditionalCurrencyCode: null,
      }));
      setFormDataRemittance(newArray);
      onSwitchForm((prevState) => ({ ...prevState, fromRemittance: true }));
    }
  }, [rowData]);

  // * -----Start: form validation logic-----
  let formIsValid;

  if (mainSwitch && serviceType) {
    if (!breakTimeToggle && !breakTimeToggleConditional) {
      const inconsistency =
        (conditionalStartTimeIsValid &&
          conditionalStartTime === "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime !== "") ||
        (conditionalStartTimeIsValid &&
          conditionalStartTime !== "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime === "");

      formIsValid =
        startTimeIsValid &&
        endTimeIsValid &&
        conditionalStartTimeIsValid &&
        conditionalEndTimeIsValid &&
        !inconsistency;
    } else if (!breakTimeToggle && breakTimeToggleConditional) {
      const inconsistency =
        (conditionalStartTimeIsValid &&
          conditionalStartTime === "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime !== "") ||
        (conditionalStartTimeIsValid &&
          conditionalStartTime !== "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime === "") ||
        (conditionalStartBreakTimeIsValid &&
          conditionalStartBreakTime === "" &&
          conditionalEndBreakTimeIsValid &&
          conditionalEndBreakTime !== "") ||
        (conditionalStartBreakTimeIsValid &&
          conditionalStartBreakTime !== "" &&
          conditionalEndBreakTimeIsValid &&
          conditionalEndBreakTime === "") ||
        (conditionalStartBreakTimeIsValid &&
          conditionalStartBreakTime === "" &&
          conditionalEndBreakTimeIsValid &&
          conditionalEndBreakTime === "");

      formIsValid =
        startTimeIsValid &&
        endTimeIsValid &&
        conditionalStartTimeIsValid &&
        conditionalEndTimeIsValid &&
        conditionalStartBreakTimeIsValid &&
        conditionalEndBreakTimeIsValid &&
        !inconsistency;
    } else if (breakTimeToggle && !breakTimeToggleConditional) {
      const inconsistency =
        (conditionalStartTimeIsValid &&
          conditionalStartTime === "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime !== "") ||
        (conditionalStartTimeIsValid &&
          conditionalStartTime !== "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime === "");

      formIsValid =
        startTimeIsValid &&
        endTimeIsValid &&
        startBreakTimeIsValid &&
        endBreakTimeIsValid &&
        conditionalStartTimeIsValid &&
        conditionalEndTimeIsValid &&
        !inconsistency;
    } else if (breakTimeToggle && breakTimeToggleConditional) {
      const inconsistency =
        (conditionalStartTimeIsValid &&
          conditionalStartTime === "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime !== "") ||
        (conditionalStartTimeIsValid &&
          conditionalStartTime !== "" &&
          conditionalEndTimeIsValid &&
          conditionalEndTime === "") ||
        (conditionalStartBreakTimeIsValid &&
          conditionalStartBreakTime === "" &&
          conditionalEndBreakTimeIsValid &&
          conditionalEndBreakTime !== "") ||
        (conditionalStartBreakTimeIsValid &&
          conditionalStartBreakTime !== "" &&
          conditionalEndBreakTimeIsValid &&
          conditionalEndBreakTime === "") ||
        (conditionalStartBreakTimeIsValid &&
          conditionalStartBreakTime === "" &&
          conditionalEndBreakTimeIsValid &&
          conditionalEndBreakTime === "");

      formIsValid =
        startTimeIsValid &&
        endTimeIsValid &&
        startBreakTimeIsValid &&
        endBreakTimeIsValid &&
        conditionalStartTimeIsValid &&
        conditionalEndTimeIsValid &&
        conditionalStartBreakTimeIsValid &&
        conditionalEndBreakTimeIsValid &&
        !inconsistency;
    }
  }

  if (!mainSwitch && serviceType) {
    if (!breakTimeToggleConditional) {
      formIsValid = conditionalStartTimeIsValid && conditionalEndTimeIsValid;
    } else {
      formIsValid =
        conditionalStartTimeIsValid &&
        conditionalEndTimeIsValid &&
        conditionalStartBreakTimeIsValid &&
        conditionalEndBreakTimeIsValid;
    }
  }

  if (mainSwitch && !serviceType) {
    formIsValid =
      !formDataRemittance.some(
        (o) =>
          (!timeValidateHandler(o.startTime) ||
            !timeValidateHandler(o.endTime) ||
            ((!timeValidateHandler(o.startBreakTime) ||
              !timeValidateHandler(o.endBreakTime)) &&
              o.breakTimeToggle)) &&
          !o.isNormalCOTHidden
      ) &&
      !formDataRemittance.some((o, index) => {
        const inconsistency =
          (o.conditionalStartTime && !o.conditionalEndTime) ||
          (!o.conditionalStartTime && o.conditionalEndTime) ||
          (o.conditionalStartBreakTime && !o.conditionalEndBreakTime) ||
          (!o.conditionalStartBreakTime && o.conditionalEndBreakTime);

        const isBreaktimeToggled =
          ((!o.conditionalStartBreakTime && !o.conditionalEndBreakTime) ||
            (!o.conditionalStartTime && !o.conditionalEndTime)) &&
          o.breakTimeToggleConditional;

        return (
          (!conditionalTimeValidateHandler(o.conditionalStartTime) ||
            !conditionalTimeValidateHandler(o.conditionalEndTime) ||
            !conditionalTimeValidateHandler(o.conditionalStartBreakTime) ||
            !conditionalTimeValidateHandler(o.conditionalEndBreakTime) ||
            inconsistency ||
            isBreaktimeToggled) &&
          !o.isConditionalCOTHidden
        );
      });
  }

  if (!mainSwitch && !serviceType) {
    formIsValid = !formDataRemittance.some(
      (o) =>
        (!timeValidateHandler(o.conditionalStartTime) ||
          !timeValidateHandler(o.conditionalEndTime) ||
          ((!timeValidateHandler(o.conditionalStartBreakTime) ||
            !timeValidateHandler(o.conditionalEndBreakTime)) &&
            o.breakTimeToggleConditional)) &&
        !o.isConditionalCOTHidden
    );
  }
  // * -----End: form validation logic-----

  // * -----Start: handlers-----
  const remittanceFormHandler = (event, index, key) => {
    if (!key.includes("breakTimeToggle")) {
      setFormDataRemittance((prevData) => {
        const array = [...prevData];

        array[index] = {
          ...array[index],
          [key]: key.includes("Time")
            ? timeFormatHandler(event.target.value)
            : event,
        };

        return array;
      });
    }

    if (key === "breakTimeToggle") {
      setFormDataRemittance((prevData) => {
        const array = [...prevData];

        array[index] = {
          ...array[index],
          startBreakTime: !event.target.checked
            ? ""
            : array[index].startBreakTime,
          endBreakTime: !event.target.checked ? "" : array[index].endBreakTime,
          [key]: event.target.checked,
        };

        return array;
      });
    }

    if (key === "breakTimeToggleConditional") {
      setFormDataRemittance((prevData) => {
        const array = [...prevData];

        array[index] = {
          ...array[index],
          conditionalStartBreakTime: !event.target.checked
            ? ""
            : array[index].conditionalStartBreakTime,
          conditionalEndBreakTime: !event.target.checked
            ? ""
            : array[index].conditionalEndBreakTime,
          [key]: event.target.checked,
        };

        return array;
      });
    }
  };

  const addFieldHandler = (isConditional) => () => {
    setFormDataRemittance((prevData) => {
      const array = [...prevData];

      let isShowAll;

      if (isConditional) {
        isShowAll = array.every(
          ({ isConditionalCOTHidden }) => !isConditionalCOTHidden
        );
      } else {
        isShowAll = array.every(({ isNormalCOTHidden }) => !isNormalCOTHidden);
      }

      if (!isShowAll) {
        const hiddenFieldIndexFound = array.findIndex((o) =>
          isConditional ? o.isConditionalCOTHidden : o.isNormalCOTHidden
        );

        if (hiddenFieldIndexFound || hiddenFieldIndexFound.toString() === "0") {
          array[hiddenFieldIndexFound][
            `${isConditional ? "isConditionalCOTHidden" : "isNormalCOTHidden"}`
          ] = false;
        }

        return array;
      }

      const newField = {
        conditionalId: null,
        conditionalStartTime: null,
        conditionalStartBreakTime: null,
        conditionalEndTime: null,
        conditionalEndBreakTime: null,
        conditionalCurrencyCode: null,
        id: null,
        isSingle,
        isBulk: !isSingle,
        desc: null,
        serviceType: "Remittance",
        startTime: null,
        startBreakTime: null,
        endTime: null,
        normalCot: null,
        endBreakTime: null,
        cityName: null,
        cityCode: null,
        currencyCode: null,
        currencyDesc: null,
        conditionalcotDate: null,
        isNormalCOTHidden: isConditional,
        isConditionalCOTHidden: !isConditional,
        breakTimeToggle: false,
        breakTimeToggleConditional: false,
      };

      return array.concat(newField);
    });
  };

  const removeFieldHandler = (isConditional, index) => () => {
    setFormDataRemittance((prevData) => {
      const array = [...prevData];

      if (isConditional) {
        if (
          !array[index + 1] &&
          !array[index].startTime &&
          array[index].isNormalCOTHidden
        ) {
          return array.filter((_, i) => i !== index);
        }

        array[index] = {
          ...array[index],
          conditionalCurrencyCode: null,
          conditionalStartTime: null,
          conditionalStartBreakTime: null,
          conditionalEndTime: null,
          conditionalEndBreakTime: null,
          breakTimeToggleConditional: false,
          isConditionalCOTHidden: true,
        };

        return array;
      }

      if (
        !array[index + 1] &&
        !array[index].conditionalStartTime &&
        array[index].isConditionalCOTHidden
      ) {
        return array.filter((_, i) => i !== index);
      }

      array[index] = {
        ...array[index],
        currencyCode: null,
        startTime: null,
        startBreakTime: null,
        endTime: null,
        endBreakTime: null,
        breakTimeToggle: false,
        isNormalCOTHidden: true,
      };

      return array;
    });
  };

  const breakTimeToggleHandler = (event) => {
    if (!event.target.checked) {
      startBreakTimeResetHandler();
      endBreakTimeResetHandler();
    }
    setBreakTimeToggle(event.target.checked);
  };

  const mainSwitchHandler = (event) => {
    if (!event.target.checked && serviceType) {
      startTimeResetHandler();
      endTimeResetHandler();
      breakTimeToggleHandler(event);
    }
    if (!event.target.checked && !serviceType) {
      setFormDataRemittance((prevData) =>
        prevData.map((rowData) => ({
          ...rowData,
          startTime: "",
          endTime: "",
          startBreakTime: "",
          endBreakTime: "",
          breakTimeToggle: false,
        }))
      );
    }
    setMainSwitch(event.target.checked);
  };

  const breakTimeToggleConditionalHandler = (event) => {
    if (!event.target.checked) {
      conditionalStartBreakTimeResetHandler();
      conditionalEndBreakTimeResetHandler();
    }
    setBreakTimeToggleConditional(event.target.checked);
  };

  const clearHandler = () => {
    conditionalStartTimeResetHandler();
    conditionalEndTimeResetHandler();
    setBreakTimeToggleConditional(false);
    conditionalStartBreakTimeResetHandler();
    conditionalEndBreakTimeResetHandler();
  };

  const openConfirmationHandler = () => setOpenConfirmation(true);

  const closeConfirmationHandler = () => setOpenConfirmation(false);

  const closeSuccessHandler = () => {
    dispatch(setSuccess(false));
    onFilterReset();
    onChangePage(1);
  };

  const submitHandler = () => {
    closeConfirmationHandler();

    if (serviceType) {
      const payload = {
        conditionalCutOffTime: {
          cotDate: rowData.date,
          endBreakTime: `${rowData.date} ${conditionalEndBreakTime}:00`,
          endTime: `${rowData.date} ${conditionalEndTime}:00`,
          id: rowData.id,
          startBreakTime: `${rowData.date} ${conditionalStartBreakTime}:00`,
          startTime: `${rowData.date} ${conditionalStartTime}:00`,
        },
        cutOffTIme: {
          desc: description,
          endBreakTime: `${rowData.date} ${endBreakTime}:00`,
          endTime: `${rowData.date} ${endTime}:00`,
          id: rowData.id,
          isBulk: rowData.isBulk,
          isDeleted: false,
          isSingle: rowData.isSingle,
          serviceType,
          startBreakTime: `${rowData.date} ${startBreakTime}:00`,
          startTime: `${rowData.date} ${startTime}:00`,
        },
      };

      dispatch(actionCutOffTimeSave(payload));
    } else {
      // todo: remittance dihold dulu sampai BA confirm udah bisa dikerjain
    }
  };
  // * -----End: handlers-----

  const currencyOptions =
    currencies.length &&
    currencies.map(({ nameEng, currencyCode }, i) => ({
      value: currencyCode,
      label: nameEng,
      key: currencyCode,
    }));

  return (
    <section className={classes.section}>
      <Header
        header={pageNumber === 3 ? "Edit" : "Add"}
        onChangePage={onChangePage}
      />

      <main className={classes.main}>
        <form
          id="form-cut-off-time"
          onSubmit={(event) => {
            event.preventDefault();
            openConfirmationHandler();
          }}
          className={classes.form}
        >
          <FormHeader
            header={pageNumber === 3 ? "Edit" : "Add"}
            subheader={pageNumber === 3 ? "edit" : "add"}
          />

          <ServiceAndTransferTypes
            serviceType={serviceType ?? "Remittance"}
            transferType={
              serviceType
                ? isSingle
                  ? "Single"
                  : "Bulk Transfer"
                : rowData[0].isSingle
                ? "Single"
                : "Bulk Transfer"
            }
          />

          <Description
            value={description}
            onChange={descriptionChangeHandler}
          />

          <div className={classes.cutOffTime}>Normal Cut Off Time</div>

          {/* -----Normal Cut Off Time Inputs Block----- */}
          {serviceType && (
            <CutOffTime
              activePage={activePage}
              // start time
              startTime={startTime}
              startTimeIsValid={startTimeIsValid}
              startTimeIsError={startTimeIsError}
              onStartTimeChange={startTimeChangeHandler}
              onStartTimeBlur={startTimeBlurHandler}
              // start break time
              startBreakTime={startBreakTime}
              startBreakTimeIsValid={startBreakTimeIsValid}
              startBreakTimeIsError={startBreakTimeIsError}
              onStartBreakTimeChange={startBreakTimeChangeHandler}
              onStartBreakTimeBlur={startBreakTimeBlurHandler}
              // end time
              endTime={endTime}
              endTimeIsValid={endTimeIsValid}
              endTimeIsError={endTimeIsError}
              onEndTimeChange={endTimeChangeHandler}
              onEndTimeBlur={endTimeBlurHandler}
              // end break time
              endBreakTime={endBreakTime}
              endBreakTimeIsValid={endBreakTimeIsValid}
              endBreakTimeIsError={endBreakTimeIsError}
              onEndBreakTimeChange={endBreakTimeChangeHandler}
              onEndBreakTimeBlur={endBreakTimeBlurHandler}
              // toggles
              mainSwitch={mainSwitch}
              mainSwitchHandler={mainSwitchHandler}
              breakTimeToggle={breakTimeToggle}
              onBreakTimeToggleChange={breakTimeToggleHandler}
            />
          )}
          {/* Normal Cut Off Time Inputs Remittance Block */}
          {!serviceType && (
            <RemittanceInputs
              activePage={activePage}
              mainSwitch={mainSwitch}
              currencies={currencyOptions}
              mainSwitchHandler={mainSwitchHandler}
              data={formDataRemittance}
              onChangeData={remittanceFormHandler}
              onAddField={addFieldHandler}
              onRemoveField={removeFieldHandler}
            />
          )}

          <div className={classes.cutOffTime}>
            Conditional Cut Off Time
            <Button
              startIcon={<TrashRed />}
              onClick={clearHandler}
              className={classes.clearButton}
            >
              Clear
            </Button>
          </div>

          {/* -----Conditional Cut Off Time Inputs Block----- */}
          {serviceType && (
            <CutOffTime
              isConditional
              activePage={activePage}
              date={date}
              breakTimeToggle={breakTimeToggleConditional}
              onBreakTimeToggleChange={breakTimeToggleConditionalHandler}
              // start time
              startTime={conditionalStartTime}
              startTimeIsValid={conditionalStartTimeIsValid}
              startTimeIsError={conditionalStartTimeIsError}
              onStartTimeChange={conditionalStartTimeChangeHandler}
              onStartTimeBlur={conditionalStartTimeBlurHandler}
              // start break time
              startBreakTime={conditionalStartBreakTime}
              startBreakTimeIsValid={conditionalStartBreakTimeIsValid}
              startBreakTimeIsError={conditionalStartBreakTimeIsError}
              onStartBreakTimeChange={conditionalStartBreakTimeChangeHandler}
              onStartBreakTimeBlur={conditionalStartBreakTimeBlurHandler}
              // end time
              endTime={conditionalEndTime}
              endTimeIsValid={conditionalEndTimeIsValid}
              endTimeIsError={conditionalEndTimeIsError}
              onEndTimeChange={conditionalEndTimeChangeHandler}
              onEndTimeBlur={conditionalEndTimeBlurHandler}
              // end break time
              endBreakTime={conditionalEndBreakTime}
              endBreakTimeIsValid={conditionalEndBreakTimeIsValid}
              endBreakTimeIsError={conditionalEndBreakTimeIsError}
              onEndBreakTimeChange={conditionalEndBreakTimeChangeHandler}
              onEndBreakTimeBlur={conditionalEndBreakTimeBlurHandler}
            />
          )}
          {/* Conditional Cut Off Time Inputs Remittance Block */}
          {!serviceType && (
            <RemittanceInputs
              isConditional
              activePage={activePage}
              date={date}
              data={formDataRemittance}
              currencies={currencyOptions}
              onChangeData={remittanceFormHandler}
              onAddField={addFieldHandler}
              onRemoveField={removeFieldHandler}
            />
          )}
        </form>
      </main>

      <Footer
        disableElevation
        disabled={!formIsValid}
        onCancel={onChangePage(1)}
        type="submit"
        formId="form-cut-off-time"
      />

      {/* -----Popups----- */}
      <ConfirmationPopup
        isOpen={openConfirmation}
        handleClose={closeConfirmationHandler}
        onContinue={submitHandler}
        message={`Are You Sure To ${
          pageNumber === 3 ? "Edit" : "Add"
        } Your Data?`}
        submessage="You cannot undo this action"
      />
      <SuccessPopup isOpen={isSuccess} handleClose={closeSuccessHandler} />
    </section>
  );
}
