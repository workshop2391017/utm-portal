import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

import { Typography, makeStyles } from "@material-ui/core";

import { actionCutOffTimeGet } from "stores/actions/cutOffTime";

import Filter from "components/BN/Filter/GeneralFilter";

import Table from "./components/Table/Table";
import Form from "./Form/Form";
import templateData from "./components/Table/templateData";

const useStyles = makeStyles((theme) => ({
  section: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
    padding: "26px 20px 0 20px",
    backgroundColor: "#F4F7FB",
  },
  h2: {
    marginBottom: 26,
  },
  main: {
    display: "flex",
    flexDirection: "column",
    gap: 20,
    marginTop: 20,
  },
}));

const dateFormat = "DD/MM/YYYY";
const date = moment();
const dateNow = moment(date).format(dateFormat);

const options = [{ id: 1, type: "datePicker", placeholder: dateNow }];

const CutOffTime = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { data } = useSelector(({ cutOffTime }) => cutOffTime);

  const [tableData, setTableData] = useState({
    fromRemittance: false,
    data: [],
  });
  const [dataFilter, setDataFilter] = useState(null);
  const [activePage, setActivePage] = useState({
    pageNumber: 1,
    rowData: null,
  });

  useEffect(() => {
    if (Object.keys(data).length !== 0)
      setTableData({ fromRemittance: false, data });
  }, [data]);

  const filterHandler = (event) => {
    setTableData({ fromRemittance: false, data: [] });
    const date = moment(event.date.dateValue1).format(dateFormat);
    setDataFilter({ date });
    dispatch(actionCutOffTimeGet({ date }));
  };

  const changePageHandler = (pageNumber, rowData) => () => {
    setActivePage({ pageNumber, rowData });
  };

  const resetDataFilterHandler = () => setDataFilter(null);

  if (activePage.pageNumber === 1) {
    return (
      <section className={classes.section}>
        <Typography variant="h2" component="header" className={classes.h2}>
          Cut Off Time Settings
        </Typography>

        <main className={classes.main}>
          <Filter
            title="Date :"
            page="cut off time"
            options={options}
            dataFilter={dataFilter}
            setDataFilter={filterHandler}
          />
          <Table
            data={
              data.length !== 0 && Object.keys(data).length !== 0
                ? data
                : tableData.data.length !== 0
                ? tableData.data
                : dataFilter
                ? templateData
                : []
            }
            onChangePage={changePageHandler}
          />
        </main>
      </section>
    );
  }

  return (
    <Form
      activePage={activePage}
      onSwitchForm={setTableData}
      date={dataFilter.date}
      onChangePage={changePageHandler}
      onFilterReset={resetDataFilterHandler}
    />
  );
};

export default CutOffTime;
