import React, { Fragment } from "react";

import { makeStyles, Typography, TableRow, TableCell } from "@material-ui/core";

import InfoButton from "components/BN/Button/Info/Info";
import GeneralButton from "components/BN/Button/GeneralButton";
import ExpandButton from "components/BN/Button/Expand/Expand";

import { ReactComponent as EditIcon } from "assets/icons/BN/edit-icon-s3.svg";
import { ReactComponent as AddWhite } from "assets/icons/BN/add_white.svg";

import Colors from "helpers/colors";

const useStyles = makeStyles((theme) => ({
  collapse: {
    display: "flex",
    alignItems: "center",
    gap: 8,
    border: "none",
  },
  collapsible: {
    backgroundColor: Colors.gray.ultrasoft,
    overflow: "hidden",
  },
  alignCell: {
    verticalAlign: "top",
    border: "none",
  },
  alignCellItems: {
    display: "flex",
    flexDirection: "column",
    gap: 15,
  },
  tableData: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 13,
    letterSpacing: "0.03em",
  },
  borderNone: {
    border: "none",
  },
  colorRed: {
    color: Colors.warning.hard,
  },
  backgroundColor: {
    backgroundColor: Colors.gray.ultrasoft,
  },
}));

const actionButtonStyle = {
  fontFamily: "FuturaMdBT",
  fontWeight: 700,
  fontSize: 9,
  letterSpacing: "0.03em",
  width: 54,
  height: 24,
  padding: "6px 10px",
};

const Collapse = ({
  array,
  onActionClick,
  isSingleExpanded,
  isBulkExpanded,
  onSingleExpand,
  onBulkExpand,
}) => {
  const classes = useStyles();

  let remittanceSingle = [];
  let remittanceBulk = [];

  let remittanceSingleFirstId = null;
  let remittanceBulkFirstId = null;

  if (array.length !== 0) {
    remittanceSingle = array[2].child.filter(({ isSingle }) => isSingle);
    remittanceBulk = array[2].child.slice(remittanceSingle.length);

    remittanceSingleFirstId = remittanceSingle[0].id;
    remittanceBulkFirstId = remittanceBulk[0].id;
  }

  const expandBulkDisabled = remittanceBulk.length <= 1;
  const expandSingleDisabled = remittanceSingle.length <= 1;

  return (
    <Fragment>
      {array.map((parent) => (
        <Fragment key={parent.serviceType}>
          {/* -----Title Block----- */}
          <TableRow>
            <TableCell className={classes.collapse}>
              <Typography variant="subtitle1">{parent.serviceType}</Typography>
              <InfoButton />
            </TableCell>
          </TableRow>
          {/* -----Children Block----- */}
          {parent.child.map((child, index) => (
            <TableRow
              key={`${child.id}${child.conditionalId}${index}`}
              className={
                parent.serviceType === "Remittance"
                  ? classes.collapsible
                  : classes.backgroundColor
              }
              style={{
                display:
                  parent.serviceType === "Remittance"
                    ? child.isSingle
                      ? child.id !== remittanceSingleFirstId
                        ? isSingleExpanded
                          ? undefined
                          : "none"
                        : undefined
                      : child.id !== remittanceBulkFirstId
                      ? isBulkExpanded
                        ? undefined
                        : "none"
                      : undefined
                    : undefined,
              }}
            >
              {parent.serviceType !== "Remittance" ||
              child.id === remittanceSingleFirstId ||
              child.id === remittanceBulkFirstId ? (
                <TableCell
                  className={
                    parent.serviceType === "Remittance"
                      ? classes.collapse
                      : classes.alignCell
                  }
                >
                  <Typography variant="subtitle1">
                    {child.isSingle ? "Single" : "Bulk Transfer"}
                  </Typography>
                  {parent.serviceType === "Remittance" &&
                    (child.id === remittanceSingleFirstId ||
                      child.id === remittanceBulkFirstId) && (
                      <ExpandButton
                        props={{
                          onClick: child.isSingle
                            ? expandSingleDisabled
                              ? undefined
                              : onSingleExpand
                            : expandBulkDisabled
                            ? undefined
                            : onBulkExpand,
                          expand: child.isSingle
                            ? isSingleExpanded
                            : isBulkExpanded,
                          disabled: child.isSingle
                            ? expandSingleDisabled
                            : expandBulkDisabled,
                        }}
                      />
                    )}
                </TableCell>
              ) : (
                <TableCell className={classes.borderNone} />
              )}

              <TableCell
                className={`${classes.alignCell} ${classes.tableData}`}
              >
                <span>{child.currencyDesc || "-"}</span>
              </TableCell>

              {/* -----Normal Cut Off Time----- */}
              <TableCell className={classes.alignCell}>
                {child.startTime ? (
                  <div
                    className={`${classes.tableData} ${classes.alignCellItems}`}
                  >
                    <span>{`${child.startTime} - ${child.endTime} WIB`}</span>
                    <span>
                      {child.startBreakTime &&
                        `Break : ${child.startBreakTime} - ${child.endBreakTime} WIB`}
                      {!child.startBreakTime && `Break : -`}
                    </span>
                  </div>
                ) : (
                  <span className={classes.tableData}>-</span>
                )}
              </TableCell>

              {/* -----Conditional Cut Off Time----- */}
              <TableCell className={classes.alignCell}>
                {child.conditionalStartTime ? (
                  <div
                    className={`${classes.tableData} ${classes.colorRed} ${classes.alignCellItems}`}
                  >
                    <span>
                      {`${child.conditionalStartTime} - ${child.conditionalEndTime} WIB`}
                    </span>
                    <span>
                      {child.conditionalStartBreakTime &&
                        `Break : ${child.conditionalStartBreakTime} - ${child.conditionalEndBreakTime} WIB`}
                      {!child.conditionalStartBreakTime && `Break : -`}
                    </span>
                  </div>
                ) : (
                  <span className={`${classes.tableData} ${classes.colorRed}`}>
                    -
                  </span>
                )}
              </TableCell>

              {parent.serviceType !== "Remittance" ||
              child.id === remittanceSingleFirstId ||
              child.id === remittanceBulkFirstId ? (
                <TableCell className={classes.borderNone}>
                  <GeneralButton
                    label={!child.id && !child.conditionalId ? "Add" : "Edit"}
                    iconPosition="startIcon"
                    buttonIcon={
                      !child.id && !child.conditionalId ? (
                        <AddWhite />
                      ) : (
                        <EditIcon />
                      )
                    }
                    style={actionButtonStyle}
                    onClick={
                      !child.id && !child.conditionalId
                        ? onActionClick(
                            2,
                            parent.serviceType === "Remittance"
                              ? child.isSingle
                                ? remittanceSingle
                                : remittanceBulk
                              : child
                          )
                        : onActionClick(
                            3,
                            parent.serviceType === "Remittance"
                              ? child.isSingle
                                ? remittanceSingle
                                : remittanceBulk
                              : child
                          )
                    }
                  />
                </TableCell>
              ) : (
                <TableCell className={classes.borderNone} />
              )}
            </TableRow>
          ))}
        </Fragment>
      ))}
    </Fragment>
  );
};

export default Collapse;
