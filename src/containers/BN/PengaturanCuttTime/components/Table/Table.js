import React, { Fragment, useState } from "react";

import {
  makeStyles,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from "@material-ui/core";

import Colors from "helpers/colors";

import Collapse from "./Collapse";

const useStyles = makeStyles((theme) => ({
  table: {
    backgroundColor: "white",
  },
  tableHeadTitles: {
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: 13,
    color: "white",
  },
  thead: {
    backgroundColor: Colors.primary.hard,
    "& th:first-child": {
      borderRadius: "10px 0 0 0",
      borderRight: 0,
    },
    "& th:last-child": {
      borderRadius: "0 10px 0 0",
      borderLeft: 0,
    },
  },
  tableRowGap: {
    border: "none",
    backgroundColor: `${Colors.gray.ultrasoft} !important`,
  },
  noDataContainer: {
    position: "absolute",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: 363,
    borderRadius: 10,
    backgroundColor: "white",
  },
  noDataMessage: {
    ...theme.typography.noDataMessage,
    fontSize: 17,
  },
  noDataSubmessage: {
    ...theme.typography.noDataSubmessage,
  },
}));

const tableHeadTitles = [
  "Service Type",
  "Currency",
  "Normal Cut Off Time",
  "Conditional Cut Off Time",
];

const TableCutOffTime = ({ data, onChangePage }) => {
  const classes = useStyles();

  const [isSingleExpanded, setIsSingleExpanded] = useState(false);
  const [isBulkExpanded, setIsBulkExpanded] = useState(false);

  const handleExpand = (setState) => () => {
    setState((prevExpanded) => !prevExpanded);
  };

  const tableRowGap = (
    <TableRow>
      {tableHeadTitles.map((title) => (
        <TableCell key={title} size="small" className={classes.tableRowGap} />
      ))}
      <TableCell size="small" className={classes.tableRowGap} />
    </TableRow>
  );

  const isDateChosen = data.length !== 0;

  const tableData = isDateChosen ? (
    <Collapse
      array={data}
      onActionClick={onChangePage}
      isSingleExpanded={isSingleExpanded}
      isBulkExpanded={isBulkExpanded}
      onSingleExpand={handleExpand(setIsSingleExpanded)}
      onBulkExpand={handleExpand(setIsBulkExpanded)}
    />
  ) : (
    <div className={classes.noDataContainer}>
      <p className={classes.noDataMessage}>You Haven&apos;t Selected a Date</p>
      <p className={classes.noDataSubmessage}>Choose a date first</p>
    </div>
  );

  return (
    <Fragment>
      <Table aria-label="table" className={classes.table}>
        <TableHead className={classes.thead}>
          <TableRow>
            {tableHeadTitles.map((title) => (
              <TableCell key={title} className={classes.tableHeadTitles}>
                {title}
              </TableCell>
            ))}
            <TableCell />
          </TableRow>
        </TableHead>

        <TableBody style={{ position: "relative" }}>
          {tableRowGap}
          {tableData}
        </TableBody>
      </Table>
    </Fragment>
  );
};

export default TableCutOffTime;
