import React, { Fragment, memo } from "react";
import moment from "moment";

import { makeStyles, Typography } from "@material-ui/core";

import AntdTextField from "components/BN/TextField/AntdTextField";

import { ReactComponent as ClockGray } from "assets/icons/BN/ClockGray.svg";
import { ReactComponent as ClockBlue } from "assets/icons/BN/ClockBlue.svg";

import Colors from "helpers/colors";

import Switch from "../Switch/Switch";

const useStyles = makeStyles((theme) => ({
  date: {
    margin: "20px 0",
    padding: "0 15px",
    display: "flex",
    flexDirection: "column",
    gap: 5,
  },
  grid: {
    display: "grid",
    marginBottom: 20,
    padding: "0 15px",
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
    gap: 5,
    gridRow: "1 / 2",
  },
  grayLabel: {
    ...theme.typography.gray,
  },
  value: {
    ...theme.typography.h1,
    fontSize: 15,
    color: Colors.dark.hard,
  },
  colorRed: {
    color: Colors.warning.hard,
  },
  inputStyle: {
    width: "350px !important",
  },
}));

const maxLength = 5;

const dateFormat = "DD/MM/YYYY";
const date = moment();
const dateNow = moment(date).format(dateFormat);

const CutOffTime = (props) => {
  const {
    isConditional,
    activePage,
    date,
    // start time
    startTime,
    startTimeIsError,
    onStartTimeChange,
    onStartTimeBlur,
    // start breaktime
    startBreakTime,
    startBreakTimeIsError,
    onStartBreakTimeChange,
    onStartBreakTimeBlur,
    // end time
    endTime,
    endTimeIsError,
    onEndTimeChange,
    onEndTimeBlur,
    // end breaktime
    endBreakTime,
    endBreakTimeIsError,
    onEndBreakTimeChange,
    onEndBreakTimeBlur,
    // toggles
    mainSwitch,
    mainSwitchHandler,
    breakTimeToggle,
    onBreakTimeToggleChange,
  } = props;

  const classes = useStyles();

  const disabled = !mainSwitch && !isConditional;
  const breakTimeDisabled = isConditional
    ? !breakTimeToggle
    : !mainSwitch || !breakTimeToggle;

  const breakTimeEndAdornment = isConditional ? (
    breakTimeDisabled ? (
      <ClockGray />
    ) : (
      <ClockBlue />
    )
  ) : undefined;
  const endAdornment = isConditional ? <ClockBlue /> : undefined;

  return (
    <div>
      {isConditional && (
        <div className={classes.date}>
          <span className={classes.grayLabel}>Date :</span>
          <span className={classes.value}>{date || dateNow}</span>
        </div>
      )}

      {!isConditional && (
        <Switch
          label="Setting Normal Cut Off Time"
          checked={mainSwitch}
          onChange={mainSwitchHandler}
        />
      )}

      <div className={classes.grid}>
        <div className={classes.flexColumn}>
          <Typography variant="subtitle1" component="label">
            Start Time :
          </Typography>
          <div>
            <AntdTextField
              value={startTime}
              onChange={onStartTimeChange}
              className={classes.inputStyle}
              placeholder="12:00"
              suffix={endAdornment}
              disabled={disabled}
              onBlur={onStartTimeBlur}
              maxLength={maxLength}
            />
            {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )}
          </div>
        </div>
        <div className={classes.flexColumn}>
          <Typography variant="subtitle1" component="label">
            End Time :
          </Typography>
          <div>
            <AntdTextField
              value={endTime}
              onChange={onEndTimeChange}
              className={classes.inputStyle}
              placeholder="12:00"
              suffix={endAdornment}
              disabled={disabled}
              onBlur={onEndTimeBlur}
              maxLength={maxLength}
            />
            {endTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )}
          </div>
        </div>
      </div>

      <Switch
        label="Break Time"
        checked={breakTimeToggle}
        onChange={onBreakTimeToggleChange}
        disabled={disabled}
      />

      <div className={classes.grid}>
        <div className={classes.flexColumn}>
          <Typography variant="subtitle1" component="label">
            Start Time :
          </Typography>
          <div>
            <AntdTextField
              value={startBreakTime}
              onChange={onStartBreakTimeChange}
              className={classes.inputStyle}
              disabled={breakTimeDisabled}
              placeholder="12:00"
              suffix={breakTimeEndAdornment}
              onBlur={onStartBreakTimeBlur}
              maxLength={maxLength}
            />
            {startBreakTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )}
          </div>
        </div>
        <div className={classes.flexColumn}>
          <Typography variant="subtitle1" component="label">
            End Time :
          </Typography>
          <div>
            <AntdTextField
              value={endBreakTime}
              onChange={onEndBreakTimeChange}
              className={classes.inputStyle}
              disabled={breakTimeDisabled}
              placeholder="12:00"
              suffix={breakTimeEndAdornment}
              onBlur={onEndBreakTimeBlur}
              maxLength={maxLength}
            />
            {endBreakTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(CutOffTime);
