import React, { Fragment, memo } from "react";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  formHeader: {
    ...theme.typography.h2,
    fontSize: 17,
    letterSpacing: "0.01em",
    marginBottom: 10,
  },
  subheader: {
    ...theme.typography.subtitle1,
    marginBottom: 20,
  },
}));

const FormHeader = ({ header, subheader }) => {
  const classes = useStyles();

  return (
    <Fragment>
      <h6 className={classes.formHeader}>Form {header} Cut Off Time</h6>
      <div className={classes.subheader}>
        Please fill in the data below to {subheader} cut off time
      </div>
    </Fragment>
  );
};

export default memo(FormHeader);
