import React, { memo } from "react";

import { makeStyles, Typography, Button } from "@material-ui/core";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";

const useStyles = makeStyles((theme) => ({
  header: {
    padding: "4px 20px",
  },
  backButton: {
    ...theme.typography.backButton,
  },
}));

const Header = ({ onChangePage, header }) => {
  const classes = useStyles();

  return (
    <header className={classes.header}>
      <div>
        <Button
          startIcon={<ArrowLeft />}
          onClick={onChangePage(1)}
          className={classes.backButton}
        >
          Back
        </Button>
      </div>
      <Typography variant="h2">{header} Cut Off Time</Typography>
    </header>
  );
};

export default memo(Header);
