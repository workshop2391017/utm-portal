import React, { Fragment, memo, useEffect, useState } from "react";
import moment from "moment";

import {
  makeStyles,
  Typography,
  Switch as SwitchMUI,
  IconButton,
} from "@material-ui/core";

import AntdTextField from "components/BN/TextField/AntdTextField";
import AntdSelectWithCheck from "components/BN/Select/AntdSelectWithCheck";

import { ReactComponent as ClockGray } from "assets/icons/BN/ClockGray.svg";
import { ReactComponent as ClockBlue } from "assets/icons/BN/ClockBlue.svg";
import { ReactComponent as TrashRed } from "assets/icons/BN/TrashRed.svg";
import { ReactComponent as AddWhite } from "assets/icons/BN/AddWhite.svg";

import Colors from "helpers/colors";

import Switch from "../Switch/Switch";

const useStyles = makeStyles((theme) => ({
  addFieldButtonContainer: {
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: 15,
  },
  addFieldButton: {
    background: Colors.primary.hard,
    borderRadius: 10,
    width: 60,
    height: 40,
    "&:hover": {
      background: Colors.primary.hard,
      opacity: 0.9,
    },
    "&:active": {
      opacity: 1,
    },
  },
  mainSwitch: {
    display: "flex",
    justifyContent: "space-between",
    padding: "8px 15px",
    width: "213px",
  },
  mainSwitchLabel: {
    ...theme.typography.h6,
    letterSpacing: "0.01em",
  },
  switch: {
    "& .MuiSwitch-switchBase.Mui-disabled + .MuiSwitch-track": {
      cursor: "not-allowed",
    },
    "& .MuiSwitch-switchBase": {
      color: "#BCC8E7",
    },
    "& .MuiSwitch-track": {
      backgroundColor: "#E6EAF3",
    },
    "& .Mui-checked.MuiSwitch-switchBase": {
      color: "#0061A7",
    },
    "& .Mui-checked + .MuiSwitch-track": {
      backgroundColor: "#0061A7",
    },
    "& .MuiSwitch-thumb": {
      boxShadow: "none",
      border: "1px solid #FFFFFF",
    },
    "& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track": {
      opacity: "initial",
    },
  },
  iconButton: {
    width: 60,
    height: 40,
    borderRadius: 10,
    border: "1px solid #D14848",
  },
  date: {
    margin: "20px 0",
    padding: "0 15px",
    display: "flex",
    flexDirection: "column",
    gap: 5,
  },
  grayLabel: {
    ...theme.typography.gray,
  },
  value: {
    ...theme.typography.h1,
    fontSize: 15,
    color: Colors.dark.hard,
  },
  flex: {
    display: "flex",
    marginBottom: 20,
    gap: 10,
    padding: "0 15px",
    alignItems: "flex-end",
    position: "relative",
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
    gap: 5,
  },
  inputStyle: {
    width: "213px !important",
  },
  inputWrapper: {
    position: "relative",
  },
}));

const dateFormat = "DD/MM/YYYY";
const date = moment();
const dateNow = moment(date).format(dateFormat);

const RemittanceInputs = (props) => {
  const {
    isConditional,
    activePage,
    date,
    currencies,
    mainSwitch,
    mainSwitchHandler,
    onChangeData,
    onAddField,
    onRemoveField,
    data,
  } = props;

  const classes = useStyles();

  const [currencyOptions, setCurrencyOptions] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (currencies) setCurrencyOptions(currencies);
  }, [currencies]);

  useEffect(() => {
    if (currencyOptions.length !== 0) {
      setIsLoading(false);
    }
  }, [currencyOptions]);

  return (
    <div>
      {isConditional && (
        <div className={classes.date}>
          <span className={classes.grayLabel}>Date :</span>
          <span className={classes.value}>{date || dateNow}</span>
        </div>
      )}

      {!isConditional && (
        <Switch
          label="Setting Normal Cut Off Time"
          checked={mainSwitch}
          onChange={mainSwitchHandler}
        />
      )}

      {!isConditional &&
        data.map(
          (item, index) =>
            !item.isNormalCOTHidden && (
              <Fragment key={`${item.id}${item.conditionalId}${index}`}>
                <div className={classes.flex}>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      Currency :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdSelectWithCheck
                        style={{ width: "213px" }}
                        placeholder="Choose Currency"
                        value={item.currencyCode}
                        onChange={(event) =>
                          onChangeData(event, index, "currencyCode")
                        }
                        options={currencyOptions}
                        disabled={!mainSwitch || isLoading}
                      />
                      {/* {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      Start Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.startTime}
                        onChange={(event) =>
                          onChangeData(event, index, "startTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        disabled={!mainSwitch}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      End Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.endTime}
                        onChange={(event) =>
                          onChangeData(event, index, "endTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        disabled={!mainSwitch}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {endTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <IconButton
                    className={classes.iconButton}
                    onClick={onRemoveField(false, index)}
                  >
                    <TrashRed />
                  </IconButton>
                </div>
                <div
                  className={classes.flex}
                  style={{
                    borderBottom: "1px dashed #BCC8E7",
                    paddingBottom: 20,
                  }}
                >
                  <div className={classes.mainSwitch}>
                    <span
                      className={classes.mainSwitchLabel}
                      style={{
                        color: item.breakTimeToggle ? undefined : "#BCC8E7",
                      }}
                    >
                      Break Time
                    </span>
                    <SwitchMUI
                      size="small"
                      checked={item.breakTimeToggle}
                      onChange={(event) =>
                        onChangeData(event, index, "breakTimeToggle")
                      }
                      className={classes.switch}
                      disabled={!mainSwitch}
                    />
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      Start Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.startBreakTime}
                        onChange={(event) =>
                          onChangeData(event, index, "startBreakTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        suffix={
                          item.breakTimeToggle ? <ClockBlue /> : <ClockGray />
                        }
                        disabled={!item.breakTimeToggle || !mainSwitch}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      End Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.endBreakTime}
                        onChange={(event) =>
                          onChangeData(event, index, "endBreakTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        suffix={
                          item.breakTimeToggle ? <ClockBlue /> : <ClockGray />
                        }
                        disabled={!item.breakTimeToggle || !mainSwitch}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {endTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                </div>
                {(index ===
                  data.findIndex(({ isNormalCOTHidden }) => isNormalCOTHidden) -
                    1 ||
                  index === data.length - 1) && (
                  <div className={classes.addFieldButtonContainer}>
                    <IconButton
                      color="primary"
                      className={classes.addFieldButton}
                      onClick={onAddField(false)}
                      disabled={!mainSwitch}
                    >
                      <AddWhite />
                    </IconButton>
                  </div>
                )}
              </Fragment>
            )
        )}
      {!isConditional &&
        data.every(({ isNormalCOTHidden }) => isNormalCOTHidden) && (
          <div className={classes.addFieldButtonContainer}>
            <IconButton
              color="primary"
              className={classes.addFieldButton}
              onClick={onAddField(false)}
              disabled={!mainSwitch}
            >
              <AddWhite />
            </IconButton>
          </div>
        )}
      {isConditional &&
        data.map(
          (item, index) =>
            !item.isConditionalCOTHidden && (
              <Fragment key={`${item.id}${item.conditionalId}${index}`}>
                <div className={classes.flex}>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      Currency :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdSelectWithCheck
                        style={{ width: "213px" }}
                        placeholder="Choose Currency"
                        value={item.conditionalCurrencyCode}
                        onChange={(event) =>
                          onChangeData(event, index, "conditionalCurrencyCode")
                        }
                        options={currencyOptions}
                        disabled={isLoading}
                      />
                      {/* {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      Start Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.conditionalStartTime}
                        onChange={(event) =>
                          onChangeData(event, index, "conditionalStartTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        //   disabled={}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      End Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.conditionalEndTime}
                        onChange={(event) =>
                          onChangeData(event, index, "conditionalEndTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        //   disabled={}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {endTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <IconButton
                    className={classes.iconButton}
                    onClick={onRemoveField(true, index)}
                  >
                    <TrashRed />
                  </IconButton>
                </div>
                <div
                  className={classes.flex}
                  style={{
                    borderBottom: "1px dashed #BCC8E7",
                    paddingBottom: 20,
                  }}
                >
                  <div className={classes.mainSwitch}>
                    <span
                      className={classes.mainSwitchLabel}
                      style={{
                        color: item.breakTimeToggleConditional
                          ? undefined
                          : "#BCC8E7",
                      }}
                    >
                      Break Time
                    </span>
                    <SwitchMUI
                      size="small"
                      checked={item.breakTimeToggleConditional}
                      onChange={(event) =>
                        onChangeData(event, index, "breakTimeToggleConditional")
                      }
                      className={classes.switch}
                      // disabled={}
                    />
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      Start Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.conditionalStartBreakTime}
                        onChange={(event) =>
                          onChangeData(
                            event,
                            index,
                            "conditionalStartBreakTime"
                          )
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        suffix={
                          item.breakTimeToggleConditional ? (
                            <ClockBlue />
                          ) : (
                            <ClockGray />
                          )
                        }
                        disabled={!item.breakTimeToggleConditional}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {startTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                  <div className={classes.flexColumn}>
                    <Typography variant="subtitle1" component="label">
                      End Time :
                    </Typography>
                    <div className={classes.inputWrapper}>
                      <AntdTextField
                        value={item.conditionalEndBreakTime}
                        onChange={(event) =>
                          onChangeData(event, index, "conditionalEndBreakTime")
                        }
                        className={classes.inputStyle}
                        placeholder="12:00"
                        suffix={
                          item.breakTimeToggleConditional ? (
                            <ClockBlue />
                          ) : (
                            <ClockGray />
                          )
                        }
                        disabled={!item.breakTimeToggleConditional}
                        //   onBlur={}
                        maxLength={5}
                      />
                      {/* {endTimeIsError && (
              <p className={classes.colorRed}>Input is invalid.</p>
            )} */}
                    </div>
                  </div>
                </div>
                {(index ===
                  data.findIndex(
                    ({ isConditionalCOTHidden }) => isConditionalCOTHidden
                  ) -
                    1 ||
                  index === data.length - 1) && (
                  <div className={classes.addFieldButtonContainer}>
                    <IconButton
                      color="primary"
                      className={classes.addFieldButton}
                      onClick={onAddField(true)}
                    >
                      <AddWhite />
                    </IconButton>
                  </div>
                )}
              </Fragment>
            )
        )}
      {isConditional &&
        data.every(({ isConditionalCOTHidden }) => isConditionalCOTHidden) && (
          <div className={classes.addFieldButtonContainer}>
            <IconButton
              color="primary"
              className={classes.addFieldButton}
              onClick={onAddField(true)}
            >
              <AddWhite />
            </IconButton>
          </div>
        )}
    </div>
  );
};

export default memo(RemittanceInputs);
