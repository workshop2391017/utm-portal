import React, { memo, Fragment } from "react";

import { Typography } from "@material-ui/core";

import AntdTextField from "components/BN/TextField/AntdTextField";

const style = { width: 360, height: 77, marginTop: 5 };

const Description = ({ value, onChange }) => (
  <Fragment>
    <Typography variant="subtitle1" component="label">
      Description :
    </Typography>
    <div>
      <AntdTextField
        type="textArea"
        placeholder="Enter Description"
        value={value}
        onChange={onChange}
        style={style}
      />
    </div>
  </Fragment>
);

export default memo(Description);
