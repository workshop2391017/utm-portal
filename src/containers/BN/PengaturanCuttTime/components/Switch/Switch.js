import React, { memo } from "react";

import { makeStyles, Switch as SwitchMUI } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  mainSwitch: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "8px 15px",
    margin: "20px 0",
  },
  mainSwitchLabel: {
    ...theme.typography.h6,
    letterSpacing: "0.01em",
  },
  switch: {
    "& .MuiSwitch-switchBase.Mui-disabled + .MuiSwitch-track": {
      cursor: "not-allowed",
    },
    "& .MuiSwitch-switchBase": {
      color: "#BCC8E7",
    },
    "& .MuiSwitch-track": {
      backgroundColor: "#E6EAF3",
    },
    "& .Mui-checked.MuiSwitch-switchBase": {
      color: "#0061A7",
    },
    "& .Mui-checked + .MuiSwitch-track": {
      backgroundColor: "#0061A7",
    },
    "& .MuiSwitch-thumb": {
      boxShadow: "none",
      border: "1px solid #FFFFFF",
    },
    "& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track": {
      opacity: "initial",
    },
  },
}));

const Switch = ({ label, checked, onChange, disabled }) => {
  const classes = useStyles();

  return (
    <div className={classes.mainSwitch}>
      <span className={classes.mainSwitchLabel}>{label}</span>
      <SwitchMUI
        size="small"
        checked={checked}
        onChange={onChange}
        className={classes.switch}
        disabled={disabled}
      />
    </div>
  );
};

export default memo(Switch);
