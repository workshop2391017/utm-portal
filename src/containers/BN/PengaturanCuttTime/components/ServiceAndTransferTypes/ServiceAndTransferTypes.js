import React, { memo } from "react";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  grid: {
    display: "grid",
    marginBottom: 20,
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
    gap: 5,
    gridRow: "1 / 2",
  },
  grayLabel: {
    ...theme.typography.gray,
  },
  value: {
    ...theme.typography.h1,
    fontSize: 15,
    color: "#374062",
  },
}));

const ServiceAndTransferTypes = ({ serviceType, transferType }) => {
  const classes = useStyles();

  return (
    <div className={classes.grid}>
      <div className={classes.flexColumn}>
        <span className={classes.grayLabel}>Service Type :</span>
        <span className={classes.value}>{serviceType}</span>
      </div>
      <div className={classes.flexColumn}>
        <span className={classes.grayLabel}>Transfer Type :</span>
        <span className={classes.value}>{transferType}</span>
      </div>
    </div>
  );
};

export default memo(ServiceAndTransferTypes);
