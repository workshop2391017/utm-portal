// main
import React from "react";
import PropTypes, { bool } from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { DatePicker } from "antd";
import { ReactComponent as CheckClose } from "assets/icons/BN/close.svg";
// assets
import calendar from "assets/icons/BN/calendar.svg";

const GeneralDatePicker = (props) => {
  const {
    value,
    onChange,
    style,
    placeholder,
    width,
    notbg,
    format,
    isFormat,
    disabledDate,
    ...rest
  } = props;
  const useStyles = makeStyles((theme) => ({
    datePicker: {
      padding: "4px 0px 4px 11px",
      height: 40,
      width,
      [theme.breakpoints.up("md")]: {
        width: 165,
      },
      [theme.breakpoints.up("lg")]: {
        width: 190.5,
      },
      [theme.breakpoints.up("xl")]: {
        width: 190.5,
      },
      borderRadius: 10,
      border: "1px solid #0061A7",
      "&:hover": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #0061A7",
      },

      "& .ant-picker-input": {
        cursor: "pointer",
        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "& .ant-picker-suffix": {
          cursor: "pointer",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      "& .ant-picker-panel-container": {
        borderRadius: 12,
        padding: 20,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        "& .ant-picker-panel": {
          border: "none",
          "& .ant-picker-date-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& thead": {
                  "& tr th": {
                    color: "#374062",
                  },
                },
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                      },
                    },
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        lineHeight: "28px",
                        minWidth: 28,
                        height: 28,
                        color: "#F2F5FD !important",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-month-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-year-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-decade-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-footer": {
            border: "none",
            "& .ant-picker-today-btn": {
              color: "#06377B",
            },
          },
        },
      },
    },
  }));
  const classes = useStyles();

  return (
    <React.Fragment>
      <DatePicker
        defaultValue={value}
        disabledDate={disabledDate}
        onChange={onChange}
        placeholder={placeholder}
        className={classes.datePicker}
        style={style}
        format={format}
        clearIcon={
          value ? (
            <div
              style={{
                height: "40px",
                width: "34px",
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                alignItems: "center",
                backgroundColor: "#0061A7",
                borderTopRightRadius: "10px",
                borderBottomRightRadius: "10px",
                padding: "0px 0px 0px 0px",
                marginRight: "-3px",
              }}
            >
              <CheckClose />
            </div>
          ) : null
        }
        {...rest}
        suffixIcon={
          <div
            style={{
              height: "40px",
              width: "34px",
              display: "flex",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center",
              backgroundColor: "#0061A7",
              borderTopRightRadius: "10px",
              borderBottomRightRadius: "10px",
              padding: "0px 0px 0px 0px",
              // marginRight: "-12px",
            }}
          >
            <img
              src={calendar}
              alt="icon-suffix"
              style={{
                cursor: "pointer",
              }}
            />
          </div>
        }
        dropdownClassName={classes.dropdown}
      />
    </React.Fragment>
  );
};

GeneralDatePicker.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
  style: PropTypes.object,
  width: PropTypes.number,
  placeholder: PropTypes.string,
  isFormat: PropTypes.bool,
};

GeneralDatePicker.defaultProps = {
  value: null,
  onChange: () => {},
  style: null,
  width: 200,
  isFormat: bool,
  placeholder: "Placeholder",
};

export default GeneralDatePicker;
