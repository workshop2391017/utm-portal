/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from "react";
import { Avatar, makeStyles, Typography } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import SpecialCharge from "components/BN/Popups/SpecialCharge";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Toast from "components/BN/Toats";
import { pathnameCONFIG } from "configuration";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  getDataChargeCompanyDetail,
  getDataDownloadFile,
  setDetail,
  setHandleClearError,
  setPopUpSuccess,
  setSaveSpecialCharge,
} from "stores/actions/companyCharge";
import useDebounce from "utils/helpers/useDebounce";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { validateTask } from "stores/actions/validateTaskPortal";
import { formatAmount, formatAmountDot } from "utils/helpers";
import styled from "styled-components";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import calender from "assets/icons/BN/calendarblue.svg";
import { ReactComponent as IconDoc } from "assets/icons/BN/icondoc.svg";
import TableICBB from "components/BN/TableIcBB/collapse";

import Title from "components/BN/Title";

// assets
import iconSync from "assets/icons/BN/pena.svg";
import Badge from "components/BN/Badge";
import TableEdit from "../TableEdit";

const TitleFont = styled.div`
  font-size: 20px;
  color: #2b2f3c;
  font-weight: 900;
  font-family: FuturaHvBT;
  font-weight: 900;
  margin-top: 20px;
`;

const dataHeader = () => [
  {
    title: "Charge Type",
    key: "chargeType",
    render: (rowData) => <span>{rowData?.serviceCharge?.charges?.name}</span>,
  },
  {
    title: "Currency",
    key: "currencyId",
  },
  {
    title: "Fixed Amount",
    key: "fee",
    render: (rowData) => (
      <span>
        {rowData?.fee === 0
          ? rowData.fee
          : rowData.fee !== undefined
          ? formatAmountDot(rowData?.fee?.toString())
          : "-"}
      </span>
    ),
  },
  {
    title: "Tiering/Slab",
    key: "tiering",
    render: (rowData) => (
      <span>
        {rowData?.tiering?.name !== undefined ? rowData?.tiering?.name : `-`}
      </span>
    ),
  },
];
const useStyles = makeStyles({
  container: {
    padding: "20px",
    height: "100%",
    backgroundColor: "#F4F7FB",
    position: "relative",
  },
  card: {
    height: "177px",
    width: "100%",
    padding: "24px 20px",
    backgroundColor: "#fff",
    borderRadius: "10px",
    marginBottom: "22px",
  },
  title: {
    color: "#2B2F3C",
    fontSize: "20px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    marginBottom: "10px",
  },
  subtitle: {
    color: "#7B87AF",
    fontSize: "15px",
    fontFamily: "FuturaBQ",
    fontWeight: 400,
    marginRight: "25px",
    alignItems: "center",
  },
  label: {
    color: "#0061A7",
    fontSize: "13px",
    fontFamily: "FuturaBkBT",
    fontWeight: "normal",
  },
  containerCard: {
    height: "76px",
    display: "flex",
    justifyContent: "space-between",
  },
  tableSection: {
    marginTop: 20,
    backgroundColor: "white",
    borderRadius: "10px 10px 0px 0px",
    "& .title": {
      fontSize: 15,
      padding: "8px 20px",
      fontWeight: 900,
      fontFamily: "FuturaHvBT",
      color: "#374062",
      letterSpacing: "0.01em",
    },
  },
});

const CompanyChargeDetail = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [initialData, setInitialData] = useState("0");
  const [dataSearch, setDataSearch] = useState(null);
  const [dataListPeriodical, setDataListPeriodical] = useState([]);
  const [dataListTransactional, setDataListTransactional] = useState([]);
  const [specialCharge, setSpecialCharge] = useState(false);
  const [specialChargeImg, setSpecialChargeImg] = useState("");
  const [checkedTransaction, setCheckedTransaction] = useState([]);
  const [checkedPeriodical, setCheckedPeriodical] = useState([]);
  const {
    isLoading,
    error,
    dataSpecial,
    formDataPayload,
    detail,
    isOpen,
    data,
  } = useSelector((state) => state.companyCharge);

  const [dataDetail, setDataDetail] = useState({
    segmentation: "",
    corporateName: "",
    corporateId: "",
    corporateProfileid: "",
    startEffectiveDate: "",
    endEffectiveDate: "",
    fileName: "",
  });

  useEffect(() => {
    if (Object.values(data).length === 0) {
      history.push(pathnameCONFIG?.TRANSACTION_MAINTENANCE?.COMPANY_CHARGE);
    }
  }, [data]);

  useEffect(() => {
    if (Object.values(detail).length > 0) {
      setDataDetail({
        ...dataDetail,
        segmentation: detail?.segmentation?.name,
        corporateName: detail?.corporateName,
        id: detail?.companyCharge?.id ?? null,
        corporateProfileid: detail?.corporateProfileId,
        startEffectiveDate:
          detail?.startEffectiveDate !== null
            ? moment(new Date(detail.startEffectiveDate)).format("DD/MM/YYYY")
            : "-",
        endEffectiveDate:
          detail?.endEffectiveDate !== null
            ? moment(new Date(detail.endEffectiveDate)).format("DD/MM/YYYY")
            : "-",
        fileName:
          detail?.document?.split("/")[3] !== null ? detail?.document : "-",
      });
    }
    return () => {
      setCheckedPeriodical([]);
      setCheckedTransaction([]);
      dispatch(setSaveSpecialCharge({}));
    };
  }, [detail]);

  // useEffect(() => {
  //   if (Object.values(dataSpecial).length > 0) {
  //     setDataDetail({
  //       ...dataDetail,
  //       startEffectiveDate: dataSpecial?.starEffectiveDate,
  //       endEffectiveDate: dataSpecial?.endEffectiveDate,
  //       fileName: dataSpecial?.file,
  //     });
  //   }
  // }, [dataSpecial]);

  const clickKembali = () => {
    history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE);
    dispatch(setDetail({}));
    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  useEffect(() => {
    if (Object.values(data).length > 0) {
      dispatch(getDataChargeCompanyDetail({ ...formDataPayload, search: "" }));
    }
  }, [data]);
  const payloadSearch = useDebounce(dataSearch, 1500);

  useEffect(() => {
    if (payloadSearch !== null) {
      dispatch(
        getDataChargeCompanyDetail({
          ...formDataPayload,
          search: payloadSearch,
        })
      );
    }
  }, [payloadSearch]);

  useEffect(() => {
    if (Object.keys(detail).length !== 0) {
      const dataA = detail?.listPriode?.map((listPriod) => ({
        ...listPriod,
        id: listPriod?.serviceId,
        name: listPriod?.serviceName,
        child: listPriod?.child?.map((val) => ({
          ...val,
          currencyId: val?.currencyId,
          fee: val?.fee,
          tiering: { id: val?.tiering?.id, name: val?.tiering?.name },
        })),
      }));
      const dataB = detail?.listTransactions?.map((listTransaction) => ({
        ...listTransaction,
        id: listTransaction?.serviceId,
        name: listTransaction?.serviceName,
        child: listTransaction?.child?.map((val) => ({
          ...val,
          chargeType: val?.serviceCharge?.charges?.name,
          currencyId: val?.currencyId,
          fee: val?.fee,
          tiering: { id: val?.tiering?.id, name: val?.tiering?.name },
        })),
      }));

      // dataA.map((item) => listData.push(item));
      // dataB.map((item) => listData.push(item));
      setDataListPeriodical(dataA);
      setDataListTransactional(dataB);
    }
  }, [detail]);

  const renderTable = () => {
    const dataHasilMappingPeriodical = dataListPeriodical
      ?.filter((item) => checkedPeriodical.includes(item?.id))
      .map((elm) => ({
        ...elm,
        child: elm.child.map((cElm) => ({
          ...cElm,
          currencyId: cElm?.currencyId || "IDR",
          existingField:
            cElm.fee || cElm.fee === 0
              ? "fee"
              : cElm.tiering?.id
              ? "tiering"
              : null,
          radioFee: cElm.fee === 0 ? true : !!cElm.fee,

          radioTieringSlab: !!cElm.tiering?.id,
        })),
      }));

    const dataHasilMappingTransactional = dataListTransactional
      ?.filter((item) => checkedTransaction.includes(item?.id))
      .map((elm) => ({
        ...elm,
        child: elm.child.map((cElm) => ({
          ...cElm,
          existingField:
            cElm.fee || cElm.fee === 0
              ? "fee"
              : cElm.tiering?.id
              ? "tiering"
              : null,
          radioFee: cElm.fee === 0 ? true : !!cElm.fee,
          radioTieringSlab: !!cElm.tiering?.id,
        })),
      }));

    switch (initialData) {
      case "0":
        return (
          <div>
            <div className={classes.tableSection}>
              <div className="title">Periodical Charge List</div>
              <TableICBB
                checkListAll={false}
                selecable // hide selecable
                collapse
                headerContent={dataHeader()}
                dataContent={dataListPeriodical.map((item) => ({
                  ...item,
                  status:
                    Object.values(dataSpecial).length === 0 &&
                    Object.values(detail?.companyCharge ?? {}).length === 0,
                }))}
                // page={page}
                // setPage={setPage}
                // totalData={totalPages}
                isLoading={isLoading}
                handleSelect={(e) => {
                  setCheckedPeriodical(e);
                }}
                selectedValue={checkedPeriodical}
              />
            </div>
            <div className={classes.tableSection}>
              <div className="title"> Transactional Charge List</div>
              <TableICBB
                checkListAll={false}
                selecable // hide selecable
                collapse
                headerContent={dataHeader()}
                dataContent={dataListTransactional.map((item) => ({
                  ...item,
                  status:
                    Object.values(dataSpecial).length === 0 &&
                    Object.values(detail?.companyCharge ?? {}).length === 0,
                }))}
                // page={page}
                // setPage={setPage}
                // totalData={totalPages}
                isLoading={isLoading}
                handleSelect={(e) => {
                  setCheckedTransaction(e);
                }}
                selectedValue={checkedTransaction}
              />
            </div>
          </div>
        );

      case "1":
        return (
          <TableEdit
            dataSpecial={dataSpecial}
            selecable // hide selecable
            collapse // hide icon collapse
            defaultOpenCollapseBreakdown // open collapse default
            dataDetail={dataDetail}
            detail={detail}
            dataTablePeriodical={dataHasilMappingPeriodical}
            dataTableTransactional={dataHasilMappingTransactional}
            setOpenModalConfirmation={setOpenModalConfirmation}
            openModalConfirmation={openModalConfirmation}
          />
        );

      default:
        break;
    }
  };

  return (
    <div className={classes.container}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <SpecialCharge
        isOpen={specialCharge}
        handleClose={() => {
          setSpecialCharge(false);
        }}
        detail={detail}
        setSpecialChargeImg={setSpecialChargeImg}
      />
      <SuccessConfirmation
        title="Please Wait For Approval"
        message="Saved Successfully"
        isOpen={isOpen}
        handleClose={() => {
          dispatch(setPopUpSuccess(false));
          setOpenModalConfirmation(false);
          history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE);
        }}
      />

      <div className={classes.containerCard}>
        <div style={{ minWidth: "350px" }}>
          <button
            onClick={clickKembali}
            style={{
              marginBottom: -20,
              border: "none",
              backgroundColor: "#F4F7FB",
              cursor: "pointer",
            }}
          >
            <img
              src={arrowLeft}
              style={{
                marginTop: "4px",
              }}
              alt="Back"
            />
          </button>
          <TitleFont>Company Charge Details</TitleFont>
        </div>
        {initialData === "0" ? (
          <Title>
            <Search
              placement="bottom"
              dataSearch={dataSearch}
              setDataSearch={setDataSearch}
            />
            <GeneralButton
              disabled={
                (checkedPeriodical?.length === 0 &&
                  checkedTransaction?.length === 0) ||
                (Object.values(dataSpecial).length === 0 &&
                  Object.values(detail?.companyCharge ?? {}).length === 0)
              }
              iconPosition="startIcon"
              buttonIcon={
                <img
                  src={iconSync}
                  alt="iconsync"
                  style={{
                    width: 18,
                    height: 17,
                  }}
                />
              }
              style={{
                width: 284,
                height: 44,
                fontSize: 15,
                fontFamily: "FuturaMdBT",
                fontWeight: 700,
                paddingTop: 9.5,
                paddingBottom: 9.5,
                paddingRight: 20,
                paddingLeft: 20,
                marginLeft: 20,
                // background: "#0061A7",
              }}
              label="Edit Company Charge Type"
              onClick={() => {
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.COMPANY_CHARGE,
                      code: detail?.corporateProfileId,
                      id: detail?.companyCharge?.id ?? null,
                      name: detail?.corporateName,
                      validate: true,
                    },
                    {
                      onContinue() {
                        setInitialData("1");
                      },
                    },
                    {
                      redirect: false,
                    }
                  )
                );
              }}
            />
          </Title>
        ) : null}
      </div>

      <div className={classes.card}>
        <Typography className={classes.title}>
          {detail.corporateName}
        </Typography>
        <div
          style={{
            display: "flex",
          }}
        >
          <h3 className={classes.subtitle}>{detail?.corporateId ?? "-"}</h3>
          {detail?.segmentation?.name && (
            <Badge type="blue" label={detail?.segmentation?.name} />
          )}
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "26px",
            marginBottom: "10px",
          }}
        >
          <div
            style={{
              display: "flex",
            }}
          >
            <div
              style={{
                display: "flex",
                backgroundColor: "#F4F7FB",
                width: "250px",
                height: "30px",
                alignItems: "center",
                padding: "6px 7px",
                borderRadius: "5px",
              }}
            >
              <Avatar
                variant="square"
                src={calender}
                style={{
                  width: "15px",
                  height: "16px",
                  marginRight: "12px",
                }}
              />
              <p className={classes.label}>
                {Object.values(dataSpecial).length !== 0
                  ? `${moment(new Date(dataSpecial?.starEffectiveDate)).format(
                      "DD/MM/YYYY"
                    )} -
                   ${moment(new Date(dataSpecial.endEffectiveDate)).format(
                     "DD/MM/YYYY"
                   )}`
                  : detail.startEffectiveDate !== null
                  ? `${moment(
                      dataDetail?.startEffectiveDate,
                      "DD/MM/YYYY"
                    ).format("DD/MM/YYYY")} -
                   ${moment(dataDetail?.endEffectiveDate, "DD/MM/YYYY").format(
                     "DD/MM/YYYY"
                   )}`
                  : `-`}
              </p>
            </div>

            <div
              onClick={() => {
                if (Object.values(dataSpecial).length > 0) {
                  const url = window.URL.createObjectURL(specialChargeImg);
                  const link = document.createElement("a");
                  link.href = url;
                  link.setAttribute(
                    "download",
                    dataSpecial?.file?.split("/")[3]
                  );
                  document.body.appendChild(link);
                  link.click();
                } else {
                  dispatch(
                    getDataDownloadFile(
                      dataDetail?.fileName,
                      dataDetail?.fileName?.split("/")[3] ?? null
                    )
                  );
                }
              }}
              style={{
                cursor: "pointer",
                display: "flex",
                backgroundColor: "#F4F7FB",
                width: "350px",
                height: "30px",
                marginLeft: "12px",
                alignItems: "center",
                gap: 8,
                padding: "6px 7px",
                borderRadius: "5px",
              }}
            >
              <IconDoc />

              <p className={classes.label}>
                {dataSpecial?.file !== undefined
                  ? dataSpecial.file.split("/")[3]
                  : dataDetail?.fileName?.split("/")[3] ?? "-"}
              </p>
            </div>
          </div>

          <GeneralButton
            width="160px"
            style={{
              fontSize: "15px",
              fontFamily: "FuturaMdBT",
              fontWeight: 700,
              color: "#fff",
            }}
            onClick={() => setSpecialCharge(true)}
            label="Special Charge"
          />
        </div>
      </div>

      {renderTable()}
    </div>
  );
};

CompanyChargeDetail.propTypes = {};

CompanyChargeDetail.defaultProps = {};

export default CompanyChargeDetail;
