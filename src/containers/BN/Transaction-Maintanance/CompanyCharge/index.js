import { Avatar, makeStyles, Typography } from "@material-ui/core";

import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getDataChargeCompany,
  setFormDataPayload,
  setHandleClearError,
  setSaveSpecialCharge,
} from "stores/actions/companyCharge";
import moment from "moment";

import Toast from "components/BN/Toats";
import AntdTextField from "components/BN/TextField/AntdTextField";
import styled from "styled-components";
import TableICBB from "../../../../components/BN/TableIcBB";

// assets
import iconDoc from "../../../../assets/icons/BN/icondoc.svg";
// component
import Title from "../../../../components/BN/Title";
import { pathnameCONFIG } from "../../../../configuration";
import GeneralButton from "../../../../components/BN/Button/GeneralButton";

const Card = styled.div`
  height: 613px;
  width: 100%;
  background-color: #fff;
  border-radius: 10px;
  color: #aeb3c6;
  font-size: 17px;
  font-family: FuturaHvBT;
  font-weight: 400;
  line-height: 613px;
`;

const TextEmpaty = styled.div`
  width: 100%;
  margin: auto;
  text-align: center;
`;

const useStyles = makeStyles({
  companylimit: {
    backgroundColor: "#F4F7FB",
  },
  container: {
    padding: "20px 30px",
  },
  card: {
    width: "100%",
    height: 138,
    borderRadius: "10px",
    backgroundColor: "#FFFFFF",
    marginBottom: "20px",
    padding: "20px",
  },
  subCard: {
    display: "flex",
    justifyContent: "space-between",
  },
  title: {
    color: "#2B2F3C",
    fontSize: "15px",
    fontFamily: "FuturaHvBT",
    fontWeight: "bold",
    marginBottom: 14,
  },
  label: {
    fontSize: 13,
    color: "#374062",
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
  },
});
const CompanyCharge = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { isLoading, error, data } = useSelector(
    (state) => state.companyCharge
  );

  const classes = useStyles();
  const [initialData, setInitialData] = useState(true);
  const [formData, setFormData] = useState({
    idPerusahaan: "",
    namePerusahaan: "",
  });
  const onSearchData = () => {
    dispatch(setSaveSpecialCharge({}));
    const payload = {
      corporateId: formData.idPerusahaan,
      corporateName: formData.namePerusahaan,
    };
    dispatch(getDataChargeCompany(payload));
    setInitialData(false);
  };
  const handleSelectData = (rowData) => {
    dispatch(
      setFormDataPayload({
        corporateId: rowData?.corporateId,
        corporateName: rowData?.corporateName,
        corporateProfileId: rowData?.corporateProfileId,
        document: rowData?.file,
        endEffectiveDate: rowData?.endEffectiveDate,
        startEffectiveDate: rowData?.startEffectiveDate,
      })
    );
  };

  const DataEmpaty = () => (
    <React.Fragment>
      <Card>
        <TextEmpaty>Do a Search To View Data</TextEmpaty>
      </Card>
    </React.Fragment>
  );

  const dataHeader = () => [
    {
      title: "Company ID",
      // headerAlign: "left",
      // align: "left",
      // key: "idPerusahaan",
      render: (rowData) => <span>{rowData?.corporateId}</span>,
    },
    {
      title: "Company Name",
      // key: "corporateName",
      render: (rowData) => <span>{rowData?.corporateName}</span>,
    },

    {
      title: "Expired",
      // key: "endEffectiveDate",
      render: (rowData) => (
        <span>
          {rowData.endEffectiveDate !== null
            ? moment(new Date(parseInt(rowData?.endEffectiveDate))).format(
                "DD/MM/YYYY"
              )
            : "-"}
        </span>
      ),
    },

    {
      title: "Document",

      render: (rowData) => (
        <div
          style={{
            display: "flex",
          }}
        >
          <Avatar
            src={iconDoc}
            style={{
              width: "16px",
              height: "17px",
              marginRight: "12px",
            }}
          />
          {rowData?.file?.split("/")[3] || "-"}
        </div>
      ),
    },
    // {
    //   label: "Saldo",
    //   key: "date",
    //   // render: (rowData) => <span>{rowData.date}</span>,
    // },
    {
      title: "",
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "flex-end",

              // backgroundColor: "red",
            }}
          >
            <GeneralButton
              label="View Details"
              width="120px"
              height="32px"
              style={{ fontSize: "14px", display: "flex" }}
              onClick={() => {
                handleSelectData(rowData);
                history.push(
                  pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE_DETAIL
                );
              }}
            />
          </div>
        </React.Fragment>
      ),
    },
  ];
  return (
    <div className={classes.companylimit}>
      <React.Fragment>
        <Title label="Company Charge" />
        <Toast
          open={error?.isError}
          message={error?.message ?? null}
          handleClose={() => dispatch(setHandleClearError())}
        />
        <div className={classes.container}>
          <div className={classes.card}>
            <Typography className={classes.title}>Company Search</Typography>
            <div className={classes.subCard}>
              <div
                style={{
                  display: "flex",
                }}
              >
                <div
                  style={{
                    marginRight: "30px",
                  }}
                >
                  <p className={classes.label}>Company ID :</p>
                  <AntdTextField
                    value={formData.idPerusahaan}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        idPerusahaan: e.target.value.replace(
                          /[^0-9a-zA-Z ]/g,
                          ""
                        ),
                      })
                    }
                    placeholder="109303"
                    style={{
                      width: "199px",
                      height: "40px",
                      borderRadius: "10px",
                      border: "1px solid #BCC8E7",
                      fontSize: "16px",
                    }}
                  />
                </div>

                <div>
                  <p className={classes.label}>Company Name :</p>
                  <AntdTextField
                    value={formData.namePerusahaan}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        namePerusahaan: e.target.value.replace(
                          /[^0-9a-zA-Z ]/g,
                          ""
                        ),
                      })
                    }
                    placeholder="Bank Employee Cooperative"
                    style={{
                      width: "199px",
                      height: "40px",
                      borderRadius: "10px",
                      border: "1px solid #BCC8E7",
                      fontSize: "16px",
                    }}
                  />
                </div>
              </div>
              <div>
                <GeneralButton
                  height="44px"
                  width="94px"
                  label="Search"
                  onClick={onSearchData}
                />
              </div>
            </div>
          </div>
          {initialData ? (
            <DataEmpaty />
          ) : (
            <TableICBB
              scroll
              headerContent={dataHeader()}
              dataContent={data?.listCorporate ?? []}
              isLoading={isLoading}
              // page={1}
              // totalPages={10}
              // totalData={20}
            />
          )}
        </div>
      </React.Fragment>
    </div>
  );
};

CompanyCharge.propTypes = {};

CompanyCharge.defaultProps = {};

export default CompanyCharge;
