import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addDataCompanyCharge,
  getListCurrency,
  getListTiering,
} from "stores/actions/companyCharge";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import Gap from "components/BN/Gap";
import AntdTextField from "components/BN/TextField/AntdTextField";
import RadioOption from "components/BN/Radio/RadioSingle";
import TableICBB from "components/BN/TableIcBB/collapse";
import SelectGroup from "components/BN/Select/SelectWithSearch";
import DeleteConfirmation from "components/BN/Popups/Delete";
import { pathnameCONFIG } from "configuration";
import { NumberWithMaxLength, parseNum } from "utils/helpers";
import formatNumber from "helpers/formatNumber";
import moment from "moment";

const useStyles = makeStyles({
  tableSection: {
    marginBottom: 88,
    backgroundColor: "white",
    borderRadius: "10px 10px 0px 0px",
    "& .title": {
      fontSize: 15,
      padding: "8px 20px",
      fontWeight: 900,
      fontFamily: "FuturaHvBT",
      color: "#374062",
      letterSpacing: "0.01em",
    },
  },
});

const TableEdit = ({
  detail,
  dataTablePeriodical,
  dataDetail,
  dataTableTransactional,
  dataSpecial,
  openModalConfirmation,
  setOpenModalConfirmation,
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { listCurrency, listTiering, isLoading, formDataPayload } = useSelector(
    (state) => state.companyCharge
  );
  const history = useHistory();
  const [formData, setFormData] = useState(dataTablePeriodical);
  const [formDataTransactional, setFormDataTransactional] = useState(
    dataTableTransactional
  );

  const [dataPayload, setDataPayload] = useState([]);
  const [dataCurrency, setDataCurrency] = useState([]);
  const [dataTiering, setDataTiering] = useState([]);
  const [radioA, setRadioA] = useState([]);
  const [radioB, setRadioB] = useState([]);

  // const [disabled, setDisabled] = useState(true);

  const handleCheckRadioA = () => {
    let error = false;
    if (dataTablePeriodical.length) {
      const data = radioA.filter((item) => item.name !== "");
      if (data.length === 0) error = true;
    }

    return error;
  };

  const handleCheckRadioB = () => {
    let error = false;
    if (dataTableTransactional.length > 0) {
      const data = radioB.filter((item) => item.name !== "");
      if (data.length === 0) error = true;
    }
    return error;
  };

  useEffect(() => {
    const data = [];
    const result = [];
    dataTablePeriodical.forEach((elm) => data.push(elm.child.length));
    for (let i = 0; i <= data.length - 1; i++) {
      result.push({
        name: "",
      });
    }

    setRadioA(result);
  }, [dataTablePeriodical]);

  useEffect(() => {
    const data = [];
    const result = [];
    dataTableTransactional.forEach((elm) => data.push(elm.child.length));
    for (let i = 0; i <= data.length - 1; i++) {
      result.push({
        name: "",
      });
    }

    setRadioB(result);
  }, [dataTableTransactional]);

  const onSubmit = () => {
    const dataFinalPayload = dataPayload.map((item) => ({
      ...item,
      fee: item.radioFee === false ? null : Number(item.fee),
      tiering:
        item?.radioTieringSlab !== false
          ? {
              id: item?.tiering?.id,
              name: item?.tiering?.name,
            }
          : null,
    }));

    const dataSubmit = {
      companyCharge:
        Object.values(dataSpecial).length !== 0
          ? {
              corporateProfileId: detail.corporateProfileId,
              endEffectiveDate:
                dataSpecial.endEffectiveDate === undefined
                  ? dataDetail?.endEffectiveDate
                  : `${moment(dataSpecial.endEffectiveDate)
                      .format("YYYY/MM/DD HH:mm:ss")
                      .replaceAll("/", "-")}`,
              file:
                dataSpecial?.file === undefined
                  ? dataDetail?.fileName
                  : dataSpecial?.file,
              id:
                dataSpecial?.id === undefined
                  ? dataDetail?.id
                  : dataSpecial?.id,
              starEffectiveDate:
                dataSpecial?.starEffectiveDate === undefined
                  ? dataDetail.startEffectiveDate
                  : `${moment(dataSpecial.starEffectiveDate)
                      .format("YYYY/MM/DD HH:mm:ss")
                      .replaceAll("/", "-")}`,
            }
          : Object.values(dataDetail).length > 0 &&
            detail?.companyCharge?.id !== undefined
          ? {
              corporateProfileId: dataDetail?.corporateProfileid,
              endEffectiveDate:
                dataDetail.endEffectiveDate !== "-"
                  ? `${moment(dataDetail.endEffectiveDate)
                      .format("YYYY/MM/DD HH:mm:ss")
                      .replaceAll("/", "-")}`
                  : null,
              file: detail?.companyCharge?.file,
              id: dataDetail?.id !== null ? dataDetail?.id : null,
              starEffectiveDate:
                dataDetail?.startEffectiveDate !== "-"
                  ? `${moment(dataDetail.startEffectiveDate)
                      .format("YYYY/MM/DD HH:mm:ss")
                      .replaceAll("/", "-")}`
                  : null,
            }
          : {
              corporateProfileId: null,
              endEffectiveDate: null,
              file: null,
              id: null,
              starEffectiveDate: null,
            },
      id: detail?.companyCharge?.id ?? null,
      corporateProfileId: detail.corporateProfileId,
      corporateName: detail?.corporateName,
      corporateId: formDataPayload?.corporateId,
      serviceChargePackage: dataFinalPayload,
    };

    dispatch(addDataCompanyCharge(dataSubmit));
  };

  const dataHeader = () => [
    {
      title: "Charge Type",
      key: "chargeType",
      render: (rowData) => <span>{rowData?.serviceCharge?.charges?.name}</span>,
    },
    {
      title: "Currency",
      render: (rowData, index, dataIindex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-55px",
          }}
          key={index}
        >
          <SelectGroup
            placeholder="Select Currency"
            options={dataCurrency}
            value={rowData.currencyId}
            style={{
              width: "239px",
              height: "40px",
            }}
            onChange={(e) => {
              const array = [...formData];
              array[dataIindex].child[index] = {
                ...array[dataIindex]?.child[index],
                currencyId: e,
              };

              setFormData(array);
            }}
          />
        </div>
      ),
    },
    {
      title: "Fixed Amount",
      render: (rowData, index, dataIindex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-35px",
          }}
          key={index}
        >
          <RadioOption
            label=""
            checked={formData[dataIindex].child[index]?.radioFee}
            name="name"
            handleChange={(e) => {
              const array = [...formData];
              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                radioFee: true,
                radioTieringSlab: false,
              };

              if (array[dataIindex].child[index].existingField !== "tiering") {
                array[dataIindex].child[index] = {
                  ...array[dataIindex].child[index],
                  tiering: {
                    id: null,
                    name: null,
                  },
                };
              }

              setFormData(array);
            }}
          />
          <AntdTextField
            placeholder="Fixed Amount"
            disabled={!formData[dataIindex].child[index]?.radioFee}
            value={formatNumber(rowData?.fee)}
            onChange={(e) => {
              const data = [...formData];

              data[dataIindex].child[index] = {
                ...data[dataIindex].child[index],
                fee: NumberWithMaxLength(e?.target?.value) || null,
              };

              setFormData(data);
            }}
            style={{
              width: "180px",
              height: "40px",
            }}
          />
        </div>
      ),
    },
    {
      title: "Tiering/Slab",
      render: (rowData, index, dataIindex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-35px",
          }}
          key={index}
        >
          <RadioOption
            checked={formData[dataIindex].child[index]?.radioTieringSlab}
            handleChange={(e) => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                radioTieringSlab: true,
                radioFee: false,
              };

              if (array[dataIindex].child[index].existingField !== "fee") {
                array[dataIindex].child[index] = {
                  ...array[dataIindex].child[index],
                  fee: null,
                };
              }

              setFormData(array);
            }}
            name="name"
          />

          <SelectGroup
            placeholder="Select Tiering/Slab"
            disabled={!formData[dataIindex].child[index]?.radioTieringSlab}
            value={rowData?.tiering?.name}
            options={dataTiering}
            name="provinsi"
            style={{
              width: "180px",
              height: "40px",
              marginRight: "55px",
            }}
            onSelect={(e) => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                tiering: {
                  id: e[1],
                  name: e[0],
                },
              };

              setFormData(array);
            }}
          />
        </div>
      ),
    },
  ];
  const dataHeaderTransactional = () => [
    {
      title: "Charge Type",
      render: (rowData) => <span>{rowData?.serviceCharge?.charges?.name}</span>,
    },
    {
      title: "Currency",
      render: (rowData, index, dataIndex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-55px",
          }}
          key={index}
        >
          <SelectGroup
            placeholder="Select Currency"
            options={dataCurrency}
            // value="ACEH"
            value={rowData.currencyId}
            name="provinsi"
            style={{
              width: "239px",
              height: "40px",
            }}
            onChange={(e) => {
              const array = [...formDataTransactional];

              array[dataIndex].child[index] = {
                ...array[dataIndex].child[index],
                currencyId: e,
              };

              setFormDataTransactional(array);
            }}
          />
        </div>
      ),
    },
    {
      title: "Fixed Amount",
      render: (rowData, index, dataIndex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-35px",
          }}
          key={index}
        >
          <RadioOption
            label=""
            checked={formDataTransactional[dataIndex].child[index]?.radioFee}
            name="name"
            handleChange={(e) => {
              const array = [...formDataTransactional];
              array[dataIndex].child[index] = {
                ...array[dataIndex].child[index],
                radioFee: true,
                radioTieringSlab: false,
              };

              if (array[dataIndex].child[index].existingField !== "tiering") {
                array[dataIndex].child[index] = {
                  ...array[dataIndex].child[index],
                  tiering: {
                    id: null,
                    name: null,
                  },
                };
              }

              setFormDataTransactional(array);
            }}
          />
          <AntdTextField
            // closeIcon
            placeholder="Fixed Amount"
            disabled={!formDataTransactional[dataIndex].child[index]?.radioFee}
            value={formatNumber(rowData?.fee)}
            onChange={(e) => {
              const data = [...formDataTransactional];

              data[dataIndex].child[index] = {
                ...data[dataIndex]?.child[index],
                fee: NumberWithMaxLength(e?.target?.value) || null,
              };

              setFormDataTransactional(data);
            }}
            style={{
              width: "180px",
              height: "40px",
            }}
          />
        </div>
      ),
    },
    {
      title: "Tiering/Slab",
      render: (rowData, index, dataIndex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-35px",
          }}
          key={index}
        >
          <RadioOption
            checked={
              formDataTransactional[dataIndex].child[index]?.radioTieringSlab
            }
            handleChange={(e) => {
              const array = [...formDataTransactional];

              array[dataIndex].child[index] = {
                ...array[dataIndex].child[index],
                radioTieringSlab: true,
                radioFee: false,
              };

              if (array[dataIndex].child[index].existingField !== "fee") {
                array[dataIndex].child[index] = {
                  ...array[dataIndex].child[index],
                  fee: null,
                };
              }

              setFormDataTransactional(array);
            }}
            name="name"
          />

          <SelectGroup
            placeholder="Select Tiering/Slab"
            disabled={
              !formDataTransactional[dataIndex].child[index]?.radioTieringSlab
            }
            value={rowData?.tiering?.name}
            options={dataTiering}
            name="provinsi"
            style={{
              width: "180px",
              height: "40px",
              marginRight: "55px",
            }}
            onSelect={(e) => {
              const array = [...formDataTransactional];

              array[dataIndex].child[index] = {
                ...array[dataIndex].child[index],
                tiering: {
                  id: e[1],
                  name: e[0],
                },
              };

              setFormDataTransactional(array);
            }}
          />
        </div>
      ),
    },
  ];

  useEffect(() => {
    const data = [];
    formData.forEach((element) => {
      element.child.forEach((el2) => {
        data.push(el2);
      });
    });
    formDataTransactional.forEach((element) => {
      element.child.forEach((el2) => {
        data.push(el2);
      });
    });

    setDataPayload(data);
  }, [formData, formDataTransactional]);

  useEffect(() => {
    if (Object.values(listCurrency).length !== 0) {
      const data = listCurrency.map((item) => ({
        label: `${item.currencyCode} - ${item.nameEng}`,
        value: item.currencyCode,
      }));

      setDataCurrency(data);
    }
  }, [listCurrency]);

  useEffect(() => {
    if (Object.values(listTiering).length !== 0) {
      setDataTiering(listTiering);
    }
  }, [listTiering]);

  useEffect(() => {
    dispatch(getListCurrency({ currencyList: null }));
    dispatch(
      getListTiering({
        kode: null,
        pageNumber: 0,
        pageSize: 999,
      })
    );
  }, []);

  const handleDisable = () =>
    (formData ?? []).findIndex(
      (e) =>
        e?.child?.findIndex((f) => {
          if (f?.radioFee) {
            return !f.currencyId || (f?.fee === null ? true : f?.fee < 0);
          }

          if (f?.radioTieringSlab) {
            return !f.currencyId || !f?.tiering?.id;
          }

          return !f.currencyId || (!f?.fee && f?.fee < 0 && !f?.tiering?.id);
        }) >= 0
    ) >= 0 ||
    (formDataTransactional ?? []).findIndex(
      (e) =>
        e?.child?.findIndex((f) => {
          if (f?.radioFee) {
            return !f.currencyId || (f?.fee === null ? true : f?.fee < 0);
          }

          if (f?.radioTieringSlab) {
            return !f.currencyId || !f?.tiering?.id;
          }

          return !f.currencyId || (!f?.fee && f?.fee < 0 && !f?.tiering?.id);
        }) >= 0
    ) >= 0;

  return (
    <React.Fragment>
      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Edit Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoading}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onSubmit();
        }}
      />
      <div className={classes.tableSection}>
        <div className="title">Periodical Charge List</div>
        <TableICBB
          headerContent={dataHeader()}
          dataContent={formData ?? []}
          editable
          // page={page}
          // setPage={setPage}
          // totalData={totalPages}
          // isLoading={isLoading}

          selecable={false} // hide selecable
          defaultOpenCollapseBreakdown
          selectedValue={[]}
        />
      </div>

      <Gap height="20px" />
      <div className={classes.tableSection}>
        <div className="title"> Transactional Charge List</div>
        <TableICBB
          headerContent={dataHeaderTransactional()}
          dataContent={formDataTransactional ?? []}
          editable
          // page={page}
          // setPage={setPage}
          // totalData={totalPages}
          // isLoading={isLoading}
          selecable={false} // hide selecable
          defaultOpenCollapseBreakdown
          selectedValue={[]}
        />
      </div>

      <Gap height="20px" />
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",

          marginLeft: "-22px",
          // marginRight: "30px",
          position: "absolute",
          padding: "22px 40px",
          bottom: "0px",
          height: "88px",
          background: "#FFFFFF",
          width: "100%",
        }}
      >
        <ButtonOutlined
          label="Cancel"
          width="86px"
          onClick={() =>
            history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE)
          }
          color="#0061A7"
        />

        <GeneralButton
          loading={isLoading}
          onClick={() => {
            setOpenModalConfirmation(true);
          }}
          disabled={handleDisable()}
          label="Save"
          width="100px"
          height="43px"
        />
      </div>
    </React.Fragment>
  );
};

export default TableEdit;
