import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import AntdTextField from "components/BN/TextField/AntdTextField";
import styled from "styled-components";
import { pathnameCONFIG } from "configuration";
import {
  getDataCompanyLimit,
  getDataCompanyLimitDetail,
  setHandleClearError,
  setTypePayloadDetail,
} from "stores/actions/companyLimit";
import { makeStyles, Typography } from "@material-ui/core";

import Toast from "components/BN/Toats";
import TableICBB from "../../../../components/BN/TableIcBB";

// component
import Title from "../../../../components/BN/Title";

import GeneralButton from "../../../../components/BN/Button/GeneralButton";

const Card = styled.div`
  height: 613px;
  width: 100%;
  background-color: #fff;
  border-radius: 10px;
  color: #aeb3c6;
  font-size: 17px;
  font-family: FuturaHvBT;
  font-weight: 400;
  line-height: 613px;
`;

const TextEmpaty = styled.div`
  width: 100%;
  margin: auto;

  text-align: center;
`;

const dataEmpaty = () => (
  <React.Fragment>
    <Card>
      <TextEmpaty>Do a Search To View Data</TextEmpaty>
    </Card>
  </React.Fragment>
);

const CompanyLimit = () => {
  const { data, error, isLoading } = useSelector((state) => state.companyLimit);
  const history = useHistory();
  const dispatch = useDispatch();

  const [initialData, setInitialData] = useState(true);

  const [formData, setFormData] = useState({
    idPerusahaan: null,
    namePerusahaan: null,
  });

  const useStyles = makeStyles({
    companylimit: {
      backgroundColor: "#F4F7FB",
    },
    container: {
      padding: "20px 30px",
    },
    card: {
      width: "100%",
      height: 138,
      borderRadius: "10px",
      backgroundColor: "#FFFFFF",
      marginBottom: "20px",
      padding: "20px",
    },
    subCard: {
      display: "flex",
      justifyContent: "space-between",
    },
    title: {
      color: "#2B2F3C",
      fontSize: "15px",
      fontFamily: "FuturaHvBT",
      fontWeight: "bold",
      marginBottom: 14,
    },
  });

  const classes = useStyles();

  const dataHeader = () => [
    {
      title: "Company ID",
      key: "corporateId",
    },
    {
      title: "Company Name",
      key: "corporateName",
    },
    {
      title: "",
      width: 150,
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <GeneralButton
              width="80px"
              height="23px"
              style={{
                fontSize: "9px",
                fontWeigt: 900,
                fontFamily: "FuturaMdBT",
              }}
              label="Detail"
              onClick={() => {
                const payload = {
                  corporateProfileId: rowData?.id,
                  corporateId: rowData?.corporateId,
                  corporateName: rowData?.corporateName,
                };

                dispatch(getDataCompanyLimitDetail(payload));
                dispatch(setTypePayloadDetail(payload));
                history.push(
                  pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT_DETAIL
                );
              }}
            />
          </div>
        </React.Fragment>
      ),
    },
  ];

  const onSearchData = () => {
    const payload = {
      corporateId: formData?.idPerusahaan,
      corporateName: formData?.namePerusahaan,
    };

    dispatch(getDataCompanyLimit(payload));

    setInitialData(false);
  };

  return (
    <div className={classes.companylimit}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <React.Fragment>
        <Title label="Company Limit" />

        <div className={classes.container}>
          <div className={classes.card}>
            <Typography className={classes.title}>Company Search</Typography>
            <div className={classes.subCard}>
              <div
                style={{
                  display: "flex",
                }}
              >
                <div
                  style={{
                    marginRight: "30px",
                  }}
                >
                  <p>Company ID :</p>
                  <AntdTextField
                    value={formData.idPerusahaan}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        idPerusahaan: e.target.value.replace(
                          /[^0-9a-zA-Z ]/g,
                          ""
                        ),
                      })
                    }
                    placeholder="1093038201"
                    style={{
                      width: "199px",
                      height: "40px",
                      borderRadius: "10px",
                      border: "1px solid #BCC8E7",
                      fontSize: "16px",
                    }}
                  />
                </div>

                <div>
                  <p>Company Name :</p>
                  <AntdTextField
                    value={formData.namePerusahaan}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        namePerusahaan: e.target.value.replace(
                          /[^0-9a-zA-Z ]/g,
                          ""
                        ),
                      })
                    }
                    placeholder="Bank Employee Cooperative"
                    style={{
                      width: "199px",
                      height: "40px",
                      borderRadius: "10px",
                      border: "1px solid #BCC8E7",
                      fontSize: "16px",
                    }}
                  />
                </div>
              </div>
              <div>
                <GeneralButton
                  height="44px"
                  width="94px"
                  label="Search"
                  onClick={onSearchData}
                />
              </div>
            </div>
          </div>
          {initialData ? (
            dataEmpaty()
          ) : (
            <TableICBB
              headerContent={dataHeader()}
              dataContent={data?.companyList}
              isLoading={isLoading}
              scroll
            />
          )}
        </div>
      </React.Fragment>
    </div>
  );
};

CompanyLimit.propTypes = {};

CompanyLimit.defaultProps = {};

export default CompanyLimit;
