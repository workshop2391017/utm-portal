import React, { useState, useEffect } from "react";
import AntdTextField from "components/BN/TextField/AntdTextField";
import { makeStyles } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import {
  addDataCompanyLimit,
  getAllCurrencyCompanyLimit,
  getValidationLimit,
  setClearValidationLimit,
  setPopUp,
  setTypePayloadDetail,
} from "stores/actions/companyLimit";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import _ from "lodash";
import { useHistory } from "react-router-dom";
import DeleteConfirmation from "components/BN/Popups/Delete";
import TableICBB from "components/BN/TableIcBB/collapse";
import { pathnameCONFIG } from "configuration";
import formatNumber from "helpers/formatNumber";
import { NumberWithMaxLength, parseNum } from "utils/helpers";
import FormField from "components/BN/Form/InputGroupValidation";
import useDebounce from "utils/helpers/useDebounce";

const useStyles = () =>
  makeStyles(() => ({
    inputCustom: {
      "& .ant-input": {
        boxShadow: "none",
        color: "#BCC8E7",
      },
    },
  }));
function TableDetailEdit(props) {
  const classes = useStyles()();
  const { data, clickKembali } = props;
  const [formData, setFormData] = useState([]);
  const [currencyState, setCurrencyState] = useState([]);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [payloadValidation, setPayloadValidation] = useState({});
  const payloadDebounce = useDebounce(
    payloadValidation?.maxTransaction || payloadValidation?.numberOfTransaction,
    500
  );
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    dataDetail,
    isOpen,
    dataCurrency,
    payloadDetail,
    isLoadingExcute,
    validationLimit,
  } = useSelector((state) => state.companyLimit);

  useEffect(() => {
    if (data.length > 0) {
      setFormData(data);
    }
  }, [data]);

  useEffect(() => {
    if (Object.values(payloadValidation).length > 0) {
      dispatch(
        getValidationLimit({
          ...payloadValidation,
          corporateProfileId: payloadDetail?.corporateProfileId,
        })
      );
    }
  }, [payloadDebounce]);

  useEffect(() => {
    const mapping = dataCurrency?.currencyList?.map((item) => ({
      ...item,
      label: `${item.currencyCode}-${item.nameEng}`,
      value: item.currencyCode,
    }));

    setCurrencyState(mapping);
  }, [dataCurrency]);

  useEffect(() => {
    const disabled = formData.some(
      (x) =>
        x.child[0].maxTransaction === "" ||
        x.child[0].maxAmountTransaction === ""
    );
    setIsDisabled(disabled);
  }, [formData]);

  const dataHeader = () => [
    {
      title: "Currency Matrix",
      key: "name",
    },
    {
      title: "Number of Transactions",
      render: (rowData, index, dataIindex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-10",
            flexDirection: "column",
          }}
        >
          <AntdTextField
            className={classes.inputCustom}
            placeholder="10x"
            closeIcon
            value={formatNumber(rowData?.maxTransaction)}
            style={{
              width: "180px",

              input: {
                "& .ant-input": {
                  color: "#0061A7",
                },
              },
            }}
            onClear={(e) => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                maxTransaction: "",
              };

              setFormData(array);
            }}
            onChange={(e) => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                maxTransaction: NumberWithMaxLength(e.target.value),
              };

              const obj = {};
              obj[
                `numberOfTransaction${array[dataIindex]?.child[index]?.id}`
              ] = false;
              // dispatch(setValidationLimit(obj));

              setFormData(array);
              setPayloadValidation({
                limitPackageId: array[dataIindex].child[index].limitPackage?.id,
                maxTransaction: null,
                numberOfTransaction: Number(
                  NumberWithMaxLength(e.target.value)
                ), // kalu pengecekan number of transaction
                serviceCurrencyMatrixId:
                  array[dataIindex].child[index]?.serviceCurrencyMatrix?.id,
                table: 42,
                id: array[dataIindex]?.child[index]?.id,
              });
            }}
          />
          <span
            style={{ color: "#D14848", marginTop: 2, fontFamily: "FuturaBkBT" }}
          >
            {validationLimit[
              `numberOfTransaction${formData[dataIindex]?.child[index]?.id}`
            ]
              ? "Limit doesn't match"
              : ""}
          </span>
        </div>
      ),
    },
    {
      title: "Currency",
      render: (rowData, index, dataIindex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-10px",
          }}
        >
          <SelectWithSearch
            placeholder="Select Province"
            options={currencyState}
            value={rowData?.currencyCode}
            name="provinsi"
            style={{
              width: "239px",
              height: "40px",
              marginLeft: "-10px",
            }}
            onSelect={(e) => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                currencyCode: e,
              };

              setFormData(array);
            }}
          />
        </div>
      ),
    },
    {
      title: "Maximum Transaction",
      render: (rowData, index, dataIindex) => (
        <div
          style={{
            display: "flex",
            marginLeft: "-10px",
            flexDirection: "column",
          }}
        >
          <AntdTextField
            closeIcon
            value={formatNumber(rowData?.maxAmountTransaction)}
            style={{
              width: "180px",
              marginLeft: "-5px",
            }}
            placeholder="100.000.000"
            onClear={() => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                maxAmountTransaction: "",
              };

              setFormData(array);
            }}
            onChange={(e) => {
              const array = [...formData];

              array[dataIindex].child[index] = {
                ...array[dataIindex].child[index],
                maxAmountTransaction: NumberWithMaxLength(e.target.value),
              };

              const obj = {};
              obj[
                `maxTransaction${array[dataIindex]?.child[index]?.id}`
              ] = false;

              // dispatch(setValidationLimit(obj));

              setFormData(array);
              setPayloadValidation({
                limitPackageId:
                  array[dataIindex]?.child[index].limitPackage?.id,
                maxTransaction: Number(NumberWithMaxLength(e.target.value)),
                numberOfTransaction: null, // kalu pengecekan number of transaction
                serviceCurrencyMatrixId:
                  array[dataIindex].child[index]?.serviceCurrencyMatrix?.id,
                table: 42,
                id: array[dataIindex].child[index]?.id,
              });
            }}
          />

          <span
            style={{ color: "#D14848", marginTop: 2, fontFamily: "FuturaBkBT" }}
          >
            {validationLimit[
              `maxTransaction${formData[dataIindex]?.child[index]?.id}`
            ]
              ? "Limit doesn't match"
              : ""}
          </span>
        </div>
      ),
    },
  ];

  useEffect(() => {
    dispatch(
      getAllCurrencyCompanyLimit({
        currencyList: null,
      })
    );
  }, []);

  const onSaveHandler = () => {
    const validatePayload = {
      menuName: validateTaskConstant.COMPANY_LIMIT,
    };
    const dataChildUpdate = [];

    formData?.forEach((item) => {
      item.child.forEach((val) => dataChildUpdate.push(val));
    });

    const dataChild = formData?.map((item) => item.child);
    const dataResult = dataChildUpdate?.map((item) => ({
      companyLimit: _.isEmpty(item?.companyLimit)
        ? null
        : {
            corporateProfileId: item?.companyLimit?.corporateProfileId,
            id: item?.companyLimit?.id,
          },
      companyLimitGroup: item?.companyLimitGroup,
      currencyCode: item?.currencyCode,
      id: item?.id,
      limitPackage: _.isEmpty(item?.limitPackage)
        ? null
        : {
            id: item?.limitPackage?.id,
          },
      maxAmountTransaction: Number(
        item?.maxAmountTransaction.replace(/[^0-9]/g, "")
      ),
      maxAmountTransactionOfDay: Number(item?.maxAmountTransactionOfDay),
      maxTransaction: Number(item?.maxTransaction),
      minAmountTransaction: item?.minAmountTransaction,
      serviceCurrencyMatrix: _.isEmpty(item?.serviceCurrencyMatrix)
        ? null
        : {
            currencyMatrix: {
              id: item?.serviceCurrencyMatrix?.currencyMatrix.id,
              name: item?.serviceCurrencyMatrix?.currencyMatrix.name,
            },
            id: item?.serviceCurrencyMatrix?.id,
            service: {
              id: item?.serviceCurrencyMatrix?.service?.id,
              name: item?.serviceCurrencyMatrix?.service?.name,
              transactionCategory: {
                id: item?.serviceCurrencyMatrix?.service?.transactionCategory
                  ?.id,
                name: item?.serviceCurrencyMatrix?.service?.transactionCategory
                  ?.name,
              },
            },
          },
    }));

    const payload = {
      companyLimits: _.isEmpty(dataDetail?.companyLimit)
        ? {
            id: null,
            corporateProfileId: payloadDetail?.corporateProfileId,
            corporateName: payloadDetail?.corporateName,
          }
        : {
            id: dataDetail?.companyLimit.id,
            corporateProfileId: payloadDetail?.corporateProfileId,
            corporateName: payloadDetail?.corporateName,
          },

      segmentation: dataDetail?.segmentation,
      globalLimits: dataResult,
      corporateId: payloadDetail?.corporateId,
    };

    dispatch(addDataCompanyLimit(payload));
  };

  const handleDisabledButton = () => {
    let error = false;
    Object.values(validationLimit).forEach((elm) => {
      if (elm === true) error = true;
    });

    return error;
  };

  return (
    <div>
      <SuccessConfirmation
        isOpen={isOpen}
        handleClose={() => {
          dispatch(setPopUp(false));

          history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT);
        }}
      />

      <TableICBB
        headerContent={dataHeader()}
        dataContent={formData ?? []}
        selecable={false} // hide selecable
        collapse={false} // hide icon collapse
        defaultOpenCollapseBreakdown
        selectedValue={[]}
      />

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "#fff",
          padding: "20px",
          position: "fixed",
          bottom: 0,
          left: 200,
          right: 0,
        }}
      >
        <ButtonOutlined
          label="Cancel"
          width="86px"
          color="#0061A7"
          onClick={() => {
            setFormData([]);
            dispatch(setClearValidationLimit());
            clickKembali();
            // history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT);
          }}
        />
        <GeneralButton
          onClick={() => setOpenModalConfirmation(true)}
          label="Save"
          width="100px"
          height="43px"
          disabled={isDisabled || handleDisabledButton()}
        />
      </div>
      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Edit Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onSaveHandler();
        }}
      />
    </div>
  );
}

TableDetailEdit.propTypes = {};

export default TableDetailEdit;
