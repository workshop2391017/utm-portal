import React, { useState, useEffect } from "react";
import { CircularProgress, makeStyles, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import GeneralButton from "components/BN/Button/GeneralButton";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import Gap from "components/BN/Gap";
import { pathnameCONFIG } from "configuration";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import iconSync from "assets/icons/BN/pena.svg";
import TableICBB from "components/BN/TableIcBB/collapse";

import formatNumber from "helpers/formatNumber";
import {
  handleValidateEditDataCompanyLimit,
  setClearValidationLimit,
} from "stores/actions/companyLimit";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { validateTask } from "stores/actions/validateTaskPortal";
import TableDetailEdit from "../DetailEdit";

const dataHeader = () => [
  {
    title: "Currency Matrix",
    key: "name",
    render: (rowData) => <div>{rowData?.name}</div>,
  },
  {
    title: "Number of Transactions",
    key: "maxTransaction",
  },
  {
    title: "Currency",
    key: "currencyCode",
  },
  {
    title: "Maximum Transaction",
    key: "maxAmountTransaction",
  },
];

const Title = styled.div`
  color: #2b2f3c;
  font-size: 20px;
  font-weight: 400;
  font-family: FuturaHvBT;
`;

const CompanyLimitDetail = (props) => {
  const { dataDetail, isLoading, payloadDetail } = useSelector(
    (state) => state.companyLimit
  );
  const history = useHistory();
  const [initialData, setInitialData] = useState("0");

  const useStyles = makeStyles({
    container: {
      padding: "20px",
      height: "100%",
      backgroundColor: "#F4F7FB",
      position: "relative",
    },
    card: {
      height: "113px",
      width: "100%",
      padding: "24px 20px",
      backgroundColor: "#fff",
      borderRadius: "10px",
      marginBottom: "20px",
    },
    title: {
      color: "#2B2F3C",
      fontSize: "20px",
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      marginBottom: "10px",
    },
    subtitle: {
      color: "#7B87AF",
      fontSize: "15px",
      fontFamily: "FuturaBQ",
      fontWeight: 400,
      marginRight: "25px",
      alignItems: "center",
    },
    containerCard: {
      display: "flex",
      height: "76px",
      width: "100%",
      justifyContent: "space-between",
    },
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const [dataTable, setDataTable] = useState([]);
  const [typeButton, setTypeButton] = useState("edit");

  const [chackedData, setChackedData] = useState([]);

  const clickKembali = () => {
    if (initialData === "0") {
      history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT);
    } else {
      const dataTableMapping = dataDetail?.companyLimitDetail
        ?.sort((a, b) => {
          const firstCase = a?.serviceName?.split(" ")[0].toLowerCase();
          const secondCase = b?.serviceName?.split(" ")[0].toLowerCase();
          if (firstCase < secondCase) {
            return -1;
          }
          if (firstCase > secondCase) {
            return 1;
          }
          return 0;
        })
        ?.map((item) => ({
          ...item,
          name: item.serviceName,
          id: item.serviceId,
          child: item?.child?.map((val) => ({
            ...val,
            name: val?.serviceCurrencyMatrix?.currencyMatrix?.name,
            maxAmountTransaction: formatNumber(val?.maxAmountTransaction),
            maxTransaction: formatNumber(val?.maxTransaction),
          })),
        }));
      setDataTable(dataTableMapping);
      setInitialData("0");
      setTypeButton("edit");
    }

    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  useEffect(() => {
    if (dataDetail) {
      const dataTableMapping = dataDetail?.companyLimitDetail
        ?.sort((a, b) => {
          const firstCase = a?.serviceName?.split(" ")[0].toLowerCase();
          const secondCase = b?.serviceName?.split(" ")[0].toLowerCase();
          if (firstCase < secondCase) {
            return -1;
          }
          if (firstCase > secondCase) {
            return 1;
          }
          return 0;
        })
        ?.map((item) => ({
          ...item,
          name: item.serviceName,
          id: item.serviceId,
          child: item?.child?.map((val) => ({
            ...val,
            name: val?.serviceCurrencyMatrix?.currencyMatrix?.name,
            maxAmountTransaction: formatNumber(val?.maxAmountTransaction),
            maxTransaction: formatNumber(val?.maxTransaction),
          })),
        }));
      setDataTable(dataTableMapping);
    }
    if (!isLoading && !dataDetail)
      history.push(pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT);
  }, [dataDetail]);

  const renderTable = () => {
    const dataHasilMapping = dataTable?.filter((item) =>
      chackedData.includes(item?.id)
    );

    switch (initialData) {
      case "0":
        return (
          <TableICBB
            headerContent={dataHeader()}
            dataContent={dataTable ?? []}
            // page={page}
            // setPage={setPage}
            // totalData={totalPages}
            isLoading={isLoading}
            selecable
            handleSelect={(e) => {
              setChackedData(e);
            }}
            handleSelectAll={(e) => {
              setChackedData(e);
            }}
            selectedValue={chackedData}
          />
        );

      case "1":
        return (
          <TableDetailEdit
            data={dataHasilMapping}
            clickKembali={clickKembali}
          />
        );

      default:
        break;
    }
  };

  return (
    <div className={classes.container}>
      <div className={classes.containerCard}>
        <div>
          <button
            onClick={clickKembali}
            style={{
              marginBottom: -30,
              border: "none",
              backgroundColor: "#F4F7FB",
              cursor: "pointer",
            }}
          >
            <img
              src={arrowLeft}
              style={{
                paddingTop: "-7px",
                marginBottom: 20,
              }}
              alt="Back"
            />
          </button>
          <Title>{`${
            typeButton !== "edit" ? "Change" : ""
          } Company Limit Details`}</Title>
        </div>
        {typeButton === "edit" && (
          <GeneralButton
            disabled={!chackedData.length}
            iconPosition="startIcon"
            buttonIcon={
              <img
                src={iconSync}
                alt="iconsync"
                style={{
                  width: 18,
                  height: 17,
                }}
              />
            }
            style={{
              width: 224,
              height: 44,
              fontSize: 15,
              fontFamily: "FuturaMdBT",
              fontWeight: 700,
              paddingTop: 9.5,
              paddingBottom: 9.5,
              paddingRight: 20,
              paddingLeft: 20,
              marginLeft: 20,
            }}
            label="Edit Company Limit"
            onClick={() => {
              dispatch(setClearValidationLimit());
              dispatch(
                validateTask(
                  {
                    menuName: validateTaskConstant.COMPANY_LIMIT,
                    code: payloadDetail?.corporateProfileId,
                    id: dataDetail?.companyLimit?.id ?? null,
                    name: payloadDetail?.corporateName,
                    validate: true,
                  },

                  {
                    async onContinue() {
                      setInitialData("1");
                      setTypeButton("change");
                    },
                  },
                  {
                    redirect: false,
                  }
                )
              );
            }}
          />
        )}
      </div>

      <div className={classes.card}>
        {isLoading ? (
          <CircularProgress />
        ) : (
          <div>
            <Typography className={classes.title}>
              {dataDetail?.corporateName}
            </Typography>
            <div
              style={{
                display: "flex",
              }}
            >
              <h3 className={classes.subtitle}>{dataDetail?.corporateId}</h3>

              {dataDetail?.segmentation && (
                <div
                  style={{
                    border: "1px solid #0061A7",
                    width: "auto",
                    height: "auto",
                    borderRadius: "5px",
                    color: "#0061A7",
                    backgroundColor: "#EAF2FF",
                    padding: "2px 10px 0px 10px",
                  }}
                >
                  {dataDetail?.segmentation}
                </div>
              )}
            </div>
          </div>
        )}
      </div>
      {renderTable()}
      <Gap height="45px" />
    </div>
  );
};

CompanyLimitDetail.propTypes = {};

CompanyLimitDetail.defaultProps = {};

export default CompanyLimitDetail;
