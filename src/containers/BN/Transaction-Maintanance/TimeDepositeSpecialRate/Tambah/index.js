import { makeStyles } from "@material-ui/core";
import React, { useState } from "react";
// component
import Title from "../../../../../components/BN/Title";
import TextField from "../../../../../components/BN/TextField/AntdTextField";
import SelectGroup from "../../../../../components/BN/Select/SelectGroup";
import WhiteDatePicker from "../../../../../components/BN/DatePicker/WhiteDatePicker";
import ButtonOutlined from "../../../../../components/BN/Button/ButtonOutlined";
import GeneralButton from "../../../../../components/BN/Button/GeneralButton";
import SuccessConfirmation from "../../../../../components/BN/Popups/SuccessConfirmation";
import BatalConfirmation from "../../../../../components/BN/Popups/BatalConfirmation";

const TambahTimeDepositeSpecialRate = () => {
  const [openSuccess, setOpenSuccess] = useState(false);
  const [openBatal, setOpenBatal] = useState(false);

  const useStyles = makeStyles({
    container: {
      padding: "20px 30px",
      backgroundColor: "#F4F7FB",
      paddingBottom: "20px",
      height: "95%",
    },
    card: {
      width: "720px",
      height: "330px",
      margin: "46px 180px",
      backgroundColor: "#fff",
      padding: "40px",
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      borderRadius: "20px",
    },
  });

  const classes = useStyles();

  const clickPush = () => {
    document.scrollingElement.scrollTop = 0;
  };

  return (
    <div className={classes.container}>
      <Title label="Time Deposit Special Rate" />
      <div className={classes.card}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <span style={{ color: "#BCC8E7" }}>Nomor Reff</span>
            <br />
            <TextField
              placeholder="123213"
              style={{
                width: "200px",
              }}
            />
          </div>
          <div>
            <span>ID Perusahaan :</span>
            <br />
            <TextField
              placeholder="A1234"
              style={{
                width: "200px",
              }}
            />
          </div>
          <div>
            <span style={{ color: "#BCC8E7" }}>Nama Perusahaan :</span>
            <br />
            <TextField
              placeholder="123213"
              style={{
                width: "200px",
              }}
            />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "10px",
          }}
        >
          <div>
            <span style={{ color: "#BCC8E7" }}>Foreign Currency :</span>
            <br />
            <SelectGroup
              placeholder="123213"
              style={{
                width: "200px",
              }}
              options={[{ name: "Pilih currency", id: 0 }]}
            />
          </div>
          <div>
            <span>Quantity :</span>
            <br />
            <TextField
              placeholder="1"
              style={{
                width: "200px",
              }}
            />
          </div>
          <div>
            <span style={{ color: "#BCC8E7" }}>Nominal Transaksi :</span>
            <br />
            <TextField
              placeholder="Rp 15.000.000.000"
              style={{
                width: "200px",
              }}
            />
          </div>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "10px",
          }}
        >
          <div>
            <span style={{ color: "#BCC8E7" }}>Berlaku Sampai :</span>
            <br />

            <WhiteDatePicker placeholder="12/12/2021" />
          </div>
          <div style={{ width: "100%", marginLeft: "20px" }}>
            <span>Remark :</span>
            <br />
            <TextField placeholder="Remark" style={{ width: "100%" }} />
          </div>
        </div>
      </div>

      <BatalConfirmation
        isOpen={openBatal}
        handleClose={() => setOpenBatal(false)}
      />

      <SuccessConfirmation
        isOpen={openSuccess}
        title="Tambah Berhasil"
        message="Tambah Time Deposit Special Rate Berhasil"
        submessage="Anda telah berhasil menambah
        time deposit special rate"
        handleClose={() => setOpenSuccess(false)}
      />

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          position: "fixed",
          bottom: "10px",
          backgroundColor: "#fff",
          width: "80%",
          paddingTop: "20px",
          paddingBottom: "10px",
        }}
      >
        <div>
          <ButtonOutlined
            label="Batal"
            width="86px"
            // height="40px"
            style={{ marginRight: 30 }}
            color="#0061A7"
            onClick={() => setOpenBatal(true)}
            //   onClick={() => setDeleteModal(true)}
            //   disabled={isLoadingSubmit}
          />
        </div>

        <div>
          <GeneralButton
            label="Tambah"
            width="94px"
            onClick={() => setOpenSuccess(true)}

            //   onClick={() => handleApprove(SETUJUI_PENGGUNA)}
            //   disabled={isLoadingSubmit}
          />
        </div>
      </div>
    </div>
  );
};

TambahTimeDepositeSpecialRate.propTypes = {};

TambahTimeDepositeSpecialRate.defaultProps = {};

export default TambahTimeDepositeSpecialRate;
