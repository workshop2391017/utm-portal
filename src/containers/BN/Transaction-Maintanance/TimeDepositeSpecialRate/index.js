import { Avatar, makeStyles } from "@material-ui/core";
import TagsGroup from "components/BN/Tags/TagsGroup";
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

// assets
import plus from "../../../../assets/icons/BN/plus-white.svg";
import Button from "../../../../components/BN/Button/GeneralButton";
import Menu from "../../../../components/BN/Menus/TabMenuHostErrorMapping";
import IconDocuman from "../../../../assets/icons/BN/document.svg";
import HandlingOfficerModal from "../../../../components/BN/Popups/HandlingOfficerModal";
import TableICBB from "../../../../components/BN/TableIcBB";
import Search from "../../../../components/BN/Search/SearchWithoutDropdown";
import SelectGroup from "../../../../components/BN/Select/SelectGroup";
// component
import Title from "../../../../components/BN/Title";
import TextField from "../../../../components/BN/TextField/AntdTextField";
import TimeDepositeSpecialRateModal from "../../../../components/BN/Popups/DetailTimeDepositeSpecialRate";
// import DropDwon from "../../../../components/BN/DropDown";
// import NumberPhone from "../../../../components/BN/Inputs/NumberPhone";
// import CheckListHeader from "../../../../components/BN/CheckListHeader";

const TimeDepositeSpecialRate = () => {
  const [openModal, setOpenModal] = useState(false);

  const dataDummyTableCheckingAccount = [
    {
      reffNo: "12321321",
      idPerusahaan: "33433",
      namaPerusahaan: "PT Maju Jaya",
      foreignCurrency: "SGD",
      nominalTransaksi: "Rp 100.000.000",
    },
    {
      reffNo: "12321321",
      idPerusahaan: "33433",
      namaPerusahaan: "PT Maju Jaya",
      foreignCurrency: "SGD",
      nominalTransaksi: "Rp 100.000.000",
    },
    {
      reffNo: "12321321",
      idPerusahaan: "33433",
      namaPerusahaan: "PT Maju Jaya",
      foreignCurrency: "SGD",
      nominalTransaksi: "Rp 100.000.000",
    },
    {
      reffNo: "12321321",
      idPerusahaan: "33433",
      namaPerusahaan: "PT Maju Jaya",
      foreignCurrency: "SGD",
      nominalTransaksi: "Rp 100.000.000",
    },
    {
      reffNo: "12321321",
      idPerusahaan: "33433",
      namaPerusahaan: "PT Maju Jaya",
      foreignCurrency: "SGD",
      nominalTransaksi: "Rp 100.000.000",
    },
  ];

  const dataHeader = [
    {
      title: "Reff No",
      // headerAlign: "left",
      // align: "left",
      key: "reffNo",
      // render: (rowData) => <span>{rowData.id}</span>,
    },
    {
      title: "ID Perusahaan",
      key: "idPerusahaan",
      // render: (rowData) => <span>{rowData.name}</span>,
    },
    {
      title: "Nama Perusahaan",
      key: "namaPerusahaan",
    },
    {
      title: "Foreign Currency",
      key: "foreignCurrency",
    },
    {
      title: "Nominal Transaksi",
      key: "nominalTransaksi",
    },
    {
      title: "Berlaku Sampai",
      render: (rowData) => (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "space-between",
              // backgroundColor: "red",
            }}
          >
            <div
              style={{
                fontSize: "13px",
                color: "#374062",
                fontWeight: "normal",
              }}
            >
              12-12-2021
            </div>

            <button
              onClick={() => setOpenModal(true)}
              style={{
                fontSize: "15px",
                color: "#06377B",
                fontWeight: "bold",
                hover: "cursor",
                border: "none",
                outline: "none",
                background: "#fff",
              }}
            >
              Detail
            </button>
          </div>
        </React.Fragment>
      ),
    },
  ];

  const history = useHistory();

  const [openSuccess, setOpenSuccess] = useState(false);

  const [dataSearch, setDataSearch] = useState([]);

  const useStyles = makeStyles({
    handlingofficer: {},
    container: {
      padding: "20px 30px",
    },
    img: {
      height: "24px",
      width: "18px",
    },
  });

  const classes = useStyles();

  // useEffect(() => {
  //   setSelectedIds(
  //     dataDummyTableCheckingAccount
  //       .filter((item) => item.checked)
  //       .map((item) => item.id)
  //   );
  // }, [dataDummyTableCheckingAccount]);

  return (
    <div className={classes.handlingofficer}>
      <Title label="Time Deposit Special Rate">
        {/* ini pengaturan user */}
        {/* <Search
          options={[
            "Nomor token",
            "Input nomor",
            "Input nomor",
            "Input nomor",
            "Input nomor",
            "Status",
          ]}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        /> */}
        <Button
          label="Special Rate"
          iconPosition="startIcon"
          buttonIcon={<img src={plus} alt="plus" />}
          width="146px"
          height="44px"
          style={{ marginLeft: 30, padding: "10px" }}
          onClick={() =>
            history.push(
              "/portal/monitoring-report/statistik-transaki-time-desposite-special-rate-tambah"
            )
          }
        />
      </Title>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          padding: "16px 20px",
          margin: "16px 20px",
        }}
      >
        <div
          style={{
            width: "50%",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <TextField
            placeholder="Reff No"
            style={{ width: "230px", marginRight: "10px" }}
          />
          <TextField
            placeholder="ID Perusahaan"
            style={{ width: "230px", marginRight: "10px" }}
          />
          <SelectGroup
            style={{ width: "230px" }}
            options={[{ name: "SGD", id: 1 }]}
          />
        </div>
        <Button
          label="Terapkan"
          width="92px"
          height="44px"
          style={{ marginLeft: 30, padding: "20px 10px" }}
          onClick={() => setOpenModal(true)}
        />
      </div>
      <div className={classes.container}>
        {/* <Badge type="blue" label="Online" /> */}
        <TableICBB
          headerContent={dataHeader}
          dataContent={dataDummyTableCheckingAccount}
        />
        {/* <DropDwon />
        <NumberPhone /> */}
        {/* <CheckListHeader title="Kategori Bisnis" /> */}
      </div>

      <TimeDepositeSpecialRateModal
        isOpen={openModal}
        title="Tambah Berhasil"
        message="Tambah Time Deposit Special Rate Berhasil"
        submessage="Anda telah berhasil menambah
        time deposit special rate"
        handleClose={() => setOpenModal(false)}
      />
    </div>
  );
};

TimeDepositeSpecialRate.propTypes = {};

TimeDepositeSpecialRate.defaultProps = {};

export default TimeDepositeSpecialRate;
