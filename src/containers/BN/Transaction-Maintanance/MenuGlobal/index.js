import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles, Typography } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import Title from "components/BN/Title";
import GeneralTabs from "components/BN/Tabs/GeneralTabs";
import { pathnameCONFIG } from "configuration";
import { useDispatch, useSelector } from "react-redux";
import {
  getMenuDataGlobal,
  setDataDetail,
  setHandleClearError,
} from "stores/actions/menuGlobal";
import Toast from "components/BN/Toats";
import SearchWitouthDropdown from "components/BN/Search/SearchWithoutDropdown";
import iconSync from "assets/icons/BN/pena.svg";
import SelectGroup from "components/BN/Select/SelectGroup";
import TableICBB from "components/BN/TableIcBB/collapse";
import useDebounce from "utils/helpers/useDebounce";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";
import { validateTask } from "stores/actions/validateTaskPortal";

const dataHeader = () => [
  {
    title: "Menu",
    key: "menu",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {rowData?.menu}
      </div>
    ),
  },
  {
    title: "Menu Type",
    alignItems: "center",
    key: "jenisMenu",
  },
];
const useStyles = makeStyles({
  page: {
    backgroundColor: "#F4F7FB",
    padding: "26px 20px",
  },
  title: {
    fontSize: "20px",
    color: "#2B2F3C",
    fontFamily: "FuturaHvBT",
    fontWeight: 700,
  },
  card: {
    padding: "15px 20px",
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    marginTop: "36px",
  },
  container: {},
});

const MenuGlobal = (props) => {
  const { menuAccessList, isLoading, error, dropDown } = useSelector(
    (state) => state.menuGlobal
  );
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [nilaiDropDown, setNilaiDropDown] = useState(null);
  const [tab, setTab] = useState(0);
  const [checkedData, setCheckedData] = useState([]);

  const [dataTable, setDataTable] = useState([]);
  const [dataSearch, setDataSearch] = useState(null);

  const searchBounce = useDebounce(dataSearch, 1300);

  useEffect(() => {
    dispatch(
      getMenuDataGlobal({
        group: "USER",
        menuTypeId: null,
        searchBy: searchBounce || null,
      })
    );
  }, [searchBounce]);

  useEffect(() => {
    const res = menuAccessList?.map((item) => {
      const bahasa = "US";

      return {
        ...item,
        name: bahasa === "IDN" ? item.name_id : item.name_en,
        child: item?.menuAccessChild
          .sort((a, b) => a.order - b.order)
          .map((value) => ({
            ...value,
            menu: bahasa === "IDN" ? value.name_id : value.name_en,
            jenisMenu: value.menuTypeName,
          }))
          .filter((item) => item.name_en.split(" ")[0] !== "All"),
      };
    });

    // const data = menuAccessList?.map((item, index) => {
    //   const bahasa = "US";
    //   return {
    //     name: bahasa === "IDN" ? item.name_id : item.name_en,
    //     id: item.id,
    //     index,
    //     // value: bahasa === "IDN" ? item.name_id : item.name_en,
    //   };
    // });

    // setDropDown(data);

    setDataTable(res);
  }, [menuAccessList]);

  const onEditData = () => {
    const dataTampung = dataTable?.filter((item) =>
      checkedData.includes(item.id)
    );

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.MENU_GLOBAL,
          name: null,
          code: null,
          listId: dataTampung?.map(({ id }) => id),
        },
        {
          async onContinue() {
            dispatch(setDataDetail(dataTampung));

            history.push(pathnameCONFIG.GLOBAL_MAINTENANCE.MENU_GLOBAL_EDIT);
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

  const onSearchData = () => {
    const convertTab = (nilai) => {
      switch (nilai) {
        case 0:
          return null;

        case 1:
          return 0;

        case 2:
          return 1;
        default:
          return 0;
      }
    };
    const tabHasil = convertTab(tab);

    const payload = {
      group: "USER",
      menuTypeId: tabHasil,
      parent_id: nilaiDropDown === "all-menu" ? null : nilaiDropDown,
    };
    dispatch(getMenuDataGlobal(payload));
  };

  return (
    <div className={classes.page}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Title label="Menu Global">
        <SearchWitouthDropdown
          placeholder="Menu"
          placement="bottom"
          style={{ width: "200px" }}
          dataSearch={dataSearch}
          setDataSearch={setDataSearch}
        />

        <GeneralButton
          disabled={!checkedData.length}
          iconPosition="startIcon"
          buttonIcon={
            <img
              src={iconSync}
              alt="iconsync"
              style={{
                width: 18,
                height: 17,
              }}
            />
          }
          style={{
            width: 224,
            height: 44,
            fontSize: 15,
            fontFamily: "FuturaMdBT",
            fontWeight: 700,
            paddingTop: 9.5,
            paddingBottom: 9.5,
            paddingRight: 20,
            paddingLeft: 20,
            marginLeft: 20,
          }}
          label="Edit Menu Global"
          onClick={onEditData}
        />
      </Title>

      <div className={classes.card}>
        <div
          style={{
            display: "flex",
          }}
        >
          <SelectGroup
            options={dropDown?.filter((e) => e.name && e.id)}
            value={nilaiDropDown}
            placeholder="Menu Name"
            style={{
              width: "335px",
              height: "40px",
              marginRight: "50px",
            }}
            onChange={(e) => {
              setNilaiDropDown(e);
            }}
          />
          <GeneralTabs
            value={tab}
            tabs={["All", "Standard Menu", "Special Menu"]}
            onChange={(e, v) => setTab(v)}
          />
        </div>
        <GeneralButton
          style={{
            width: 73,
            height: 44,
            fontSize: 15,
            fontFamily: "FuturaMdBT",
            fontWeight: 700,
            paddingTop: 9.5,
            paddingBottom: 9.5,
            paddingRight: 20,
            paddingLeft: 20,
          }}
          label="Search"
          onClick={onSearchData}
        />
      </div>

      <div className={classes.container}>
        <TableICBB
          headerContent={dataHeader()}
          dataContent={dataTable ?? []}
          isLoading={isLoading}
          selecable
          handleSelect={(e) => {
            setCheckedData(e);
          }}
          handleSelectAll={(e) => {
            setCheckedData(e);
          }}
          selectedValue={checkedData}
        />
      </div>
    </div>
  );
};

MenuGlobal.propTypes = {};

export default MenuGlobal;
