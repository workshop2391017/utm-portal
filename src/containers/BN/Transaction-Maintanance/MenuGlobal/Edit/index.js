import React, { useState } from "react";

import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { makeStyles, Typography, Grid, Paper } from "@material-ui/core";
import styled from "styled-components";
import RadioGroup from "components/BN/Radio/RadioGroup";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import MenuListOptional from "components/BN/MenuList/MenuListOptional";
import { useDispatch, useSelector } from "react-redux";
import { addMenuDataGlobal, setDataSucces } from "stores/actions/menuGlobal";
import DeleteConfirmation from "components/BN/Popups/Delete";
import arrowLeft from "assets/icons/BN/buttonkembali.svg";
import { pathnameCONFIG } from "configuration";

const Title = styled.div`
  font-size: 20px;
  font-family: FuturaHvBT;
  font-weight: 400;
  color: #2b2f3c;
`;

const SubTitle = styled.div`
  font-size: 15px;
  color: #374062;
  font-weight: 900;
  font-family: FuturaHvBT;
  margin-bottom: 20px;
`;

const Label = styled.div`
  font-size: 13px;
  color: #aeb3c6;
  font-weight: 400;
  font-family: FuturaMdBT;
`;

const Line = styled.div`
  height: 1px;
  width: 100%;
  background-color: #aeb3c6;
`;

const SubContent = styled.div`
  font-size: 13px;
  font-weight: 400;
  color: #374062;
  font-family: FuturaBkBT;
`;

const useStyles = makeStyles({
  page: {
    padding: "20px",
    backgroundColor: "#F4F7FB",
    height: "100%",
    position: "relative",
  },

  title: {
    fontSize: "20px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#2B2F3C",
  },
  card: {
    width: 326,
    height: 608,
    borderRadius: "10px",
    backgroundColor: "#FFFFFF",
    marginRight: "20px",
  },
  container: {
    // height: 608,
    width: "100%",
    borderRadius: "10px",
    backgroundColor: "#fff",
    padding: "10px",
    marginBottom: "200px",
  },
  header: {
    backgroundColor: "#0061A7",
    height: "40px",
    width: "100%",
    fontSize: "17px",
    color: "#fff",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    padding: "10px",
  },

  itemContent: {
    width: "100%",
    height: "50px",
    borderBottom: "0.5px solid #E8EEFF",
    backgroundColor: "#fff",
    fontSize: "15px",
    color: "#0061A7",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
    padding: "10px",
    "&:hover": {
      backgroundColor: "#EAF2FF",
    },
    "&:active": {
      backgroundColor: "#EAF2FF",
    },
  },
});
const MenuGlobalEdit = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [menu, setMenu] = useState({});
  const [detail, setDetail] = useState(null);
  const [oneItem, setOneItem] = useState({ index: null, data: null });
  const [dataMenuState, setDataMenuState] = useState([]);

  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);

  const dispatch = useDispatch();

  const { dataDetail, isOpen, isLoading } = useSelector(
    (state) => state.menuGlobal
  );

  useState(() => {
    setDataMenuState(dataDetail);
  }, [dataDetail]);

  const clickKembali = () => {
    history.goBack();

    setTimeout(() => {
      document.scrollingElement.scrollTop = 0;
    }, 0);
  };

  const onSaveDataHandler = () => {
    console.warn("dataMenusState:", dataMenuState);

    const resultMappingPayload = dataMenuState?.map((item) => ({
      id: item?.id,
      name_en: item?.name_en,
      name_id: item?.name_id,

      menuAccessChild: item?.child?.map((val) => ({
        id: val?.id,
        menuTypeId: val?.menuTypeId,
        menuTypeName: val?.menuTypeId === 1 ? "Special" : "Standard",
        name_en: val?.name_en,
        name_id: val?.name_id,
      })),
    }));

    const dataSend = {
      menuAccessList: resultMappingPayload,
    };

    dispatch(addMenuDataGlobal(dataSend));
  };

  return (
    <div className={classes.page}>
      <SuccessConfirmation
        isOpen={isOpen}
        handleClose={() => {
          dispatch(setDataSucces(false));
          setOpenModalConfirmation(false);
          history.goBack(pathnameCONFIG.GLOBAL_MAINTENANCE.MENU_GLOBAL);
        }}
      />

      <Button
        style={{}}
        startIcon={
          <img
            src={arrowLeft}
            style={{
              paddingTop: "10px",
            }}
            alt="Back"
          />
        }
        onClick={clickKembali}
      />
      <Title>Edit Global Menu</Title>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
        }}
      >
        <Grid className={classes.card}>
          <MenuListOptional
            titleMenu="Menu"
            listData={dataMenuState}
            onClick={setOneItem}
            selectedItem={oneItem}
          />
        </Grid>
        <Grid className={classes.container}>
          <SubTitle>Menu {dataMenuState[oneItem.index]?.name}</SubTitle>

          {dataMenuState[oneItem.index]?.child?.map((item, index) => (
            <React.Fragment>
              <Label>{item.menu}</Label>
              <Line />
              <div
                style={{
                  marginTop: "15px",
                  marginBotom: "37px",
                }}
              >
                <SubContent>Menu Type:</SubContent>
                <RadioGroup
                  options={[
                    { label: "Special", value: 1 },
                    { label: "Standard", value: 0 },
                  ]}
                  direction="horizontal"
                  value={item?.menuTypeId}
                  onChange={(e) => {
                    console.warn("e:", e.target.value);

                    const arr = [...dataMenuState];
                    arr[oneItem.index].child[index] = {
                      ...arr[oneItem.index].child[index],
                      menuTypeId: e.target.value,
                    };

                    setDataMenuState(arr);

                    // console.warn("e:", e);
                    // console.warn("index:", index);
                  }}
                />
              </div>
            </React.Fragment>
          ))}
        </Grid>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          position: "fixed",
          bottom: 0,
          right: 0,
          width: "calc(100% - 200px)",
          padding: "20px",

          backgroundColor: "#fff",
          marginTop: "20px",
        }}
      >
        <ButtonOutlined
          label="Cancel"
          width="86px"
          color="#0061A7"
          onClick={clickKembali}
        />

        <GeneralButton
          onClick={() => setOpenModalConfirmation(true)}
          label="Save"
          width="100px"
          height="43px"
          disabled={isLoading}
        />
      </div>
      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Edit Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoading}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onSaveDataHandler();
        }}
      />
    </div>
  );
};

MenuGlobalEdit.propTypes = {};

export default MenuGlobalEdit;
