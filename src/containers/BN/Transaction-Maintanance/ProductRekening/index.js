import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MenuListOptional from "components/BN/MenuList/MenuListOptional";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import SearchWithDropdown from "components/BN/Search/SearchWithoutDropdown";
import Title from "components/BN/Title";
import DeleteConfirmation from "components/BN/Popups/Delete";
import { pathnameCONFIG } from "configuration";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  addProduct,
  getProduct,
  getProductAccountType,
  setAddData,
  setInitDataProduct,
} from "stores/actions/productRekening";
import useDebounce from "utils/helpers/useDebounce";
import ProductRekeningCheckbox from "./renderCheckboxs";

const useStyles = makeStyles({
  container1: {
    width: 300,
    backgroundColor: "#fff",
    borderRadius: 10,
    height: "372px",
  },
  container2: {
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    paddingTop: 30,
    borderRadius: 10,
    backgroundColor: "#fff",
    marginLeft: "20px",
    marginBottom: 100,
  },

  produkRekening: {
    backgroundColor: "#F4F7FB",
    height: "100%",
  },
  title: {
    fontSize: 15,
    fontFamily: "FuturaMdBT",
    color: "#fff",
  },
  card: {
    width: "100%",
    backgroundColor: "#0061A7",
    height: 40,
    border: "none",
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingLeft: 15,
    paddingTop: 8,
    paddingBottom: 8,
  },
  card2: {
    // backgroundColor: "yellow",
    display: "flex",
    justifyContent: "space-between",
    // backgroundColor: "#EAF2FF",
    "&:active": {
      backgroundColor: "#EAF2FF",
    },
    "&:hover": {
      backgroundColor: "#EAF2FF",
    },
  },
  content: {
    height: 372 - 38,
    backgroundColor: "#fff",
    width: "100%",
  },
  card3: {
    height: 60,
    display: "flex",
    justifyContent: "center",
    borderTop: "1px solid #E8EEFF",
  },
  subtitle: {
    fontSize: 13,
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#0061A7",
  },
  title2: {
    fontSize: 17,
    color: "#2B2F3C",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    marginBottom: 23,
  },
  scad: {
    marginBottom: 18,
  },
  loading: {
    display: "flex",
    justifyContent: "center",
    height: "100%",
    alignItems: "center",
  },
  checkbox: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
  },
});

const ProductRekening = (props) => {
  const classes = useStyles();
  const { isLoading, accountProducts, openSucccess } = useSelector(
    (state) => state.productRekening
  );

  const dispatch = useDispatch();
  const history = useHistory();

  const [dataSearch, setDataSearch] = useState("");
  const [oneItem, setOneItem] = useState({});
  const [formData, setFormData] = useState([]);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);

  const [temp, setTemp] = useState(accountProducts);

  const getData = () => {
    dispatch(getProductAccountType({}));
    const payload = {
      accountType: null,
      isProductAllowed: null,
      accountTypeList: ["SA", "CA", "LN", "CD"],
    };
    dispatch(getProduct(payload));
  };

  useEffect(() => {
    if (!accountProducts.length) {
      getData();
    }
  }, []);

  useEffect(() => {
    if (accountProducts?.length && !Object.keys(oneItem).length)
      setOneItem({ index: 0, data: accountProducts[0] });
  }, [accountProducts]);

  const handleNext = () => setOpenModalConfirmation(false);
  const handleResetData = () => dispatch(setInitDataProduct(accountProducts));

  const onSaveDataHandler = () => {
    const arr = [];

    const accType = [];

    accountProducts?.forEach((elm) => {
      elm.child
        .filter((e) => e.edited)
        .forEach((item) => {
          accType.push(item.accountType);
          arr.push({
            id: item.id,
            isProductAllowed: item.isProductAllowed,
            accountType: item.accountType,
            productName: item?.productName,
            productType: item.accountType,
          });
        });
    });

    dispatch(
      addProduct(
        {
          accountProducts: arr,
        },
        {
          code: [...new Set(accType)].join(";"),
        },
        handleNext,
        handleResetData
      )
    );
  };

  const searchDebounce = useDebounce(dataSearch, 1300);
  return (
    <div className={classes.produkRekening}>
      <SuccessConfirmation
        isOpen={openSucccess}
        title="Please Wait For Approval"
        message="Saved Successfully"
        handleClose={() => {
          dispatch(setAddData(false));
          setOpenModalConfirmation(false);
          history.push(
            pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_PRODUCT_REKENING
          );
        }}
      />
      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Edit Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoading}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          onSaveDataHandler();
        }}
      />
      <Title label="Account Product">
        <SearchWithDropdown
          setDataSearch={setDataSearch}
          dataSearch={dataSearch}
          placeholder="Type Product"
        />
      </Title>

      <div
        style={{
          display: "flex",
          padding: "20px",
        }}
      >
        <Grid xs={3} className={classes.container1}>
          <MenuListOptional
            titleMenu="Account Products"
            listData={accountProducts}
            onClick={(item) => {
              setOneItem(item);
            }}
            selectedItem={oneItem}
            childKey="label"
            isLoading={isLoading}
          />
        </Grid>
        <ProductRekeningCheckbox
          props={{
            classes,
            dataSearch,
            formData,
            setFormData,
            onSaveDataHandler,
            oneItem,
            getData,
            searchDebounce,
            setOpenModalConfirmation,
          }}
        />
      </div>
    </div>
  );
};

ProductRekening.propTypes = {};

export default ProductRekening;
