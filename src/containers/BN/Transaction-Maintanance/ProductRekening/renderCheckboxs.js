import { CircularProgress, Grid, Typography } from "@material-ui/core";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import React, { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setInitDataProduct } from "stores/actions/productRekening";
import styled from "styled-components";

const SubTitle = styled.div`
  color: #44495b;
  font-size: 15px;
  font-family: FuturaHvBT;
  margin-left: 10px;
  font-weight: 900;
  display: inline;
`;

const ProductRekeningCheckbox = ({
  props: {
    classes,
    formData,
    searchDebounce,
    onSaveDataHandler,
    oneItem,
    getData,
    setOpenModalConfirmation,
  },
}) => {
  const { isLoading, accountProducts, isLoadingSubmit, accountProductsTemp } =
    useSelector((state) => state.productRekening);

  const dispatch = useDispatch();

  const mappingData = useMemo(
    () =>
      accountProducts[oneItem?.index]?.child
        ?.sort((a, b) => {
          const ba = `${a.productName}${a.productCode}`?.toLowerCase()?.trim();
          const bb = `${b.productName}${b.productCode}`?.toLowerCase()?.trim();
          if (ba < bb) {
            return -1;
          }
          if (ba > bb) {
            return 1;
          }
          return 0;
        })
        .filter(
          (e) =>
            e.productName
              ?.toLowerCase()
              ?.trim()
              ?.includes(searchDebounce?.toLowerCase()?.trim() || "") ||
            e.productCode
              ?.toLowerCase()
              ?.trim()
              ?.includes(searchDebounce?.toLowerCase()?.trim() || "")
        ),
    [accountProducts, oneItem, searchDebounce]
  );

  const checkAll = () => {
    const checkedData = accountProducts[oneItem?.index]?.child?.filter(
      (e) => e.isProductAllowed && e.isAllowedEdited
    );

    return (
      checkedData?.length &&
      checkedData?.length ===
        accountProducts[oneItem?.index]?.child?.filter((e) => e.isAllowedEdited)
          ?.length
    );
  };

  const handleCheckIsEdit = (newData, oldDataArray) => {
    const currTemp = oldDataArray[oneItem?.index]?.child?.find(
      (f) => f.id === newData.id
    );
    return currTemp.isProductAllowed !== !newData.isProductAllowed;
  };

  const handleSelect = (item, index) => {
    const newArr = [...accountProducts];
    const newArrTemp = [...accountProductsTemp];

    const latest = newArr[oneItem?.index]?.child?.filter(
      (e) => e.id !== item.id
    );
    const curr = newArr[oneItem?.index]?.child?.find((f) => f.id === item.id);

    newArr[oneItem?.index].child = [
      ...latest,
      ...[
        {
          ...curr,
          isProductAllowed: !curr.isProductAllowed,
          edited: handleCheckIsEdit(curr, newArrTemp),
        },
      ],
    ];
    dispatch(setInitDataProduct(newArr));
  };

  const handleInterminate = () =>
    accountProducts[oneItem?.index]?.child?.find(
      (f) => f.isProductAllowed && f.isAllowedEdited
    ) && !checkAll();

  return (
    <React.Fragment>
      <Grid xs={9} className={classes.container2}>
        <Typography className={classes.title2}>Types of products</Typography>
        <div
          style={{
            marginBottom: 23,
          }}
        >
          <span
            style={{
              marginLeft: -10,
              marginBottom: 23,
              color: "#44495B",
              fontSize: "15px",
              fontFamily: "FuturaHvBT",
              fontWeight: 900,
            }}
          >
            <CheckboxSingle
              checked={checkAll()}
              indeterminate={handleInterminate()}
              name="jeniP"
              disabled={
                !accountProducts[oneItem.index]?.child.filter(
                  (e) => e.isAllowedEdited
                )?.length
              }
              onChange={(e) => {
                const filterCheck = accountProducts[
                  oneItem.index
                ]?.child?.findIndex(
                  (item) => item.isProductAllowed && item.isAllowedEdited
                );

                const newArrTemp = [...accountProductsTemp];
                const arr = [...accountProducts];
                arr[oneItem.index].child = arr[oneItem.index]?.child.map(
                  (item) => ({
                    ...item,
                    ...(item.isAllowedEdited
                      ? {
                          isProductAllowed: !(filterCheck > -1),
                        }
                      : {
                          isProductAllowed: item?.isProductAllowed,
                        }),
                    edited:
                      !!item.isAllowedEdited &&
                      handleCheckIsEdit(item, newArrTemp),
                  })
                );

                dispatch(setInitDataProduct(arr));
              }}
            />
            <SubTitle> Allow All Products</SubTitle>
          </span>
        </div>
        <Grid className={classes.container3}>
          {isLoading ? (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CircularProgress />
            </div>
          ) : (
            <div className={classes.checkbox}>
              {mappingData?.length !== 0 &&
                mappingData?.map((item, index) => (
                  <React.Fragment>
                    <Grid xs={4}>
                      <div className={classes.scad}>
                        <CheckboxSingle
                          label={`${item.productName} - ${item?.productCode}`}
                          name="btn"
                          onChange={() => handleSelect(item, index)}
                          checked={item.isProductAllowed}
                          disabled={!item?.isAllowedEdited}
                        />
                      </div>
                    </Grid>
                  </React.Fragment>
                ))}
              {mappingData?.length === 0 && (
                <Grid xs={12}>
                  <div className={classes.scad}>
                    <span>No Data</span>
                  </div>
                </Grid>
              )}
            </div>
          )}
        </Grid>
      </Grid>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: 70,
          position: "fixed",
          left: 200,
          right: 0,
          bottom: 0,
          padding: 20,
          backgroundColor: "white",
        }}
      >
        <div>
          <ButtonOutlined
            label="Cancel"
            width="86px"
            color="#0061A7"
            onClick={() => getData()}
            disabled={isLoadingSubmit}
          />
        </div>

        <div>
          <GeneralButton
            onClick={() => setOpenModalConfirmation(true)}
            label="Save"
            width="100px"
            disabled={
              accountProducts?.find((e) =>
                e?.child?.find((fc) => fc?.edited)
              ) === undefined
            }
            height="43px"
            loading={isLoadingSubmit}
          />
        </div>
      </div>
    </React.Fragment>
  );
};

export default React.memo(ProductRekeningCheckbox);
