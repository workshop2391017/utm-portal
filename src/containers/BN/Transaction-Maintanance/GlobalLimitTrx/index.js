import React, { Fragment, useEffect, useState } from "react";
import {
  CircularProgress,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Toast from "components/BN/Toats";
import {
  getCurrency,
  getDataCurrencyMatrix,
  getDataCurrencyTransactionMatrix,
  getDataGlobalLimit,
  getDataService,
  setDataCard,
  setGlobalLimit,
  setHandleClearError,
  setIsAdd,
} from "stores/actions/globalLimitTrx";

import formatNumber from "helpers/formatNumber";
import ScrollCustom from "components/BN/ScrollCustom";
import MenuListOptional from "components/BN/MenuList/MenuListOptional";
import Title from "components/BN/Title";

import { setClearAlreadyExist } from "stores/actions/validatedatalms";
import Details from "./Details";

const useStyles = makeStyles({
  billerKategori: {},
  page: {
    height: "auto",
    width: "100%",
    backgroundColor: "#F4F7FB",
  },
  container: {
    padding: "0px 20px 20px",
    display: "flex",
  },
  cardSection: {
    width: "260px",
    backgroundColor: "#FFF",
    borderTopLeftRadius: "10px",
    borderTopRightRadius: "10px",
  },
  cardTitle: {
    width: "260px",
    height: "38px",
    borderTopLeftRadius: "10px",
    borderTopRightRadius: "10px",
    fontSize: "15px",
    padding: "10px 20px",
    backgroundColor: "#0061A7",
    color: "#fff",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
  },
  listItem: {
    textAlign: "left",
    display: "block",
    width: "100%",
    cursor: "pointer",

    // border: "none",
    // backgroundColor: "#fff",
    height: "42px",
    padding: "14px 20px",
    "&:active": {
      backgroundColor: "#EAF2FF",
    },
    "&:hover": {
      backgroundColor: "#EAF2FF",
    },
  },
  listText: {
    color: "#0061A7",
    fontSize: "12px",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
  },
  cardSection2: {
    width: 330,
    height: "auto",
    overflowY: "scroll",
    backgroundColor: "#F4F7FB",
    marginLeft: "20px",
  },
  card: {
    height: 190,
    width: 302,
    padding: 20,
    backgroundColor: "#fff",
    borderRadius: "10px",
    cursor: "pointer",
  },
  span: {
    background: "#F4F7FB",
    color: "#7B87AF",
    padding: "4px 8px",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    marginTop: "15px",
    width: "fit-content",
    fontSize: "10px",
  },
  title: {
    color: "#374062",
    fontWeight: 900,
    fontSize: "15px",
  },
  titleTransaksi: {
    fontSize: "10px",
    fontWeight: 900,
    fontFamily: "FuturaHvBT",
    color: "#AEB3C6",
  },
  subtiteTransaksi: {
    marginTop: "6px",
    color: "#374062",
    fontWeight: "900",
    fontSize: "10px",
  },
  loading: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    height: "100%",
    alignItems: "center",
  },
});

const DataEmpaty = () => (
  <div
    style={{
      width: "410px",
      height: "700px",
      fontSize: "17px",
      textAlign: "center",
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      color: "#AEB3C6",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    }}
  >
    Choose one to change
  </div>
);

const GlobalLimitTrx = (props) => {
  const {
    serviceList,
    isLoading,
    isLoadingSubmit,
    error,
    dataMatrix,
    dataGlobalLimitTemp,
    isLoadingCard,
    currencyList,
    isAddService,
  } = useSelector((state) => state.globalLimitTrx);
  const dispatch = useDispatch();
  const history = useHistory();
  const [isService, setIsService] = useState(false);
  const [openSuccess, setOpenSuccess] = useState(false);

  const [detailCard, setDetailCard] = useState(null);
  const [serviceSelected, setServiceSelected] = useState({});
  const [oneItemSide, setOneItemSide] = useState(0);
  const [oneItemCard, setOneItemCard] = useState(null);

  const classes = useStyles();

  useEffect(() => {
    dispatch(getDataService({ id: null }));
    dispatch(getDataCurrencyMatrix({ id: null }));
    dispatch(
      getCurrency({
        currencyList: null,
      })
    );

    const formData = {
      service: {
        id: 57,
      },
    };

    dispatch(getDataGlobalLimit(formData));
    return () => {
      dispatch(setGlobalLimit([]));
      dispatch(setClearAlreadyExist());
    };
  }, []);

  const getService = (params) => {
    const { id } = params;

    const payload = {
      id: null,
      service: {
        id,
      },
    };

    dispatch(getDataCurrencyTransactionMatrix(payload));
  };

  const getServiceGlobaLimit = (params) => {
    const { id } = params;
    const formData = {
      service: {
        id,
      },
    };

    dispatch(getDataGlobalLimit(formData));
  };

  return (
    <div className={classes.page}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <SuccessConfirmation
        isOpen={openSuccess}
        handleClose={() => setOpenSuccess(false)}
      />
      <Title label="Transaction Limits" />
      <div className={classes.container}>
        <Grid className={classes.cardSection}>
          <MenuListOptional
            titleMenu="Service"
            listData={serviceList}
            onClick={(item) => {
              setServiceSelected(item?.data);
              getService(item.data);
              setOneItemSide(item);
              setOneItemCard(null);
              getServiceGlobaLimit(item.data);
              setIsService(false);
              if (isAddService === true) {
                dispatch(setIsAdd(!isAddService));
              }
              dispatch(setGlobalLimit([]));
              dispatch(setClearAlreadyExist());
            }}
            selectedItem={oneItemSide}
            childKey="name"
            maxHeight="500px"
            isLoading={isLoading}
          />
          {/*
          <div className={classes.cardTitle}>Service</div>
          <ScrollCustom height={500}>
            {isLoading ? (
              <div className={classes.loading}>
                <CircularProgress />
              </div>
            ) : (
              <div>
                {serviceList?.map((item, index) => (
                  <Paper
                    elevation={0}
                    style={{
                      backgroundColor: `${
                        index === oneItemSide ? "#EAF2FF" : "#fff"
                      }`,
                    }}
                    onClick={() => {
                      getService(item);
                      setOneItemSide(index);
                      setOneItemCard(null);

                      getServiceGlobaLimit(item);

                      setIsService(false);
                      if (isAddService === true) {
                        dispatch(setIsAdd(!isAddService));

                        // setIsAddService(!isAddService);
                      }
                    }}
                    className={classes.listItem}
                  >
                    <Typography className={classes.listText}>
                      {item.name}
                    </Typography>
                  </Paper>
                ))}
              </div>
            )}
          </ScrollCustom> */}
          {/* <div
            style={{
              display: "flex",
            }}
          >
            <GeneralButton
              label="Add Service"
              width="134px"
              style={{
                margin: "40px auto",
              }}
              onClick={() => {
                dispatch(setIsAdd(true));
                // setIsAddService(!isAddService);
                setIsService(false);
              }}
            />
          </div> */}
        </Grid>

        <Fragment>
          <Grid className={classes.cardSection2}>
            {isLoadingCard ? (
              <div className={classes.loading}>
                <CircularProgress />
              </div>
            ) : (
              <ScrollCustom height={500}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "20px",
                  }}
                >
                  {dataGlobalLimitTemp?.map((item, index) => (
                    <Paper
                      style={{
                        border: `${
                          index === oneItemCard
                            ? "1px solid #66A3FF"
                            : "1px solid #fff"
                        }`,
                      }}
                      elevation={0}
                      className={classes.card}
                      onClick={() => {
                        setDetailCard(item);
                        setIsService(true);
                        dispatch(setDataCard({ index }));
                        setOneItemCard(index);
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                        }}
                      >
                        <Typography className={classes.title}>
                          {item.serviceCurrencyMatrix?.currencyMatrix?.name}
                        </Typography>
                      </div>

                      <div className={classes.span}>
                        {formatNumber(item?.maxTransaction)} Transaction per day
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          marginTop: "12px",
                        }}
                      >
                        <div>
                          <Typography className={classes.titleTransaksi}>
                            Minimum Transaction
                          </Typography>
                          <Typography className={classes.subtiteTransaksi}>
                            IDR {formatNumber(item?.minAmountTransaction)}
                          </Typography>
                        </div>
                        <div>
                          <Typography className={classes.titleTransaksi}>
                            Maxsimum Transaction
                          </Typography>
                          <Typography className={classes.subtiteTransaksi}>
                            IDR {formatNumber(item?.maxAmountTransaction)}
                          </Typography>
                        </div>
                      </div>
                      <div
                        style={{
                          marginTop: "15px",
                        }}
                      >
                        <Typography className={classes.titleTransaksi}>
                          Maximum nominal per day
                        </Typography>
                        <Typography className={classes.subtiteTransaksi}>
                          IDR {formatNumber(item?.maxAmountTransactionOfDay)}
                        </Typography>
                      </div>
                    </Paper>
                  ))}
                </div>
              </ScrollCustom>
            )}
          </Grid>

          <Grid
            style={{
              padding: "30px",
              minHeight: "700px",
              width: "410",
              borderRadius: "10px",
              backgroundColor: "#fff",
              marginLeft: "8px",
            }}
          >
            {isService ? (
              <Details
                isLoading={isLoadingSubmit}
                serviceSelected={serviceSelected}
                item={detailCard}
              />
            ) : (
              DataEmpaty()
            )}
          </Grid>
        </Fragment>
      </div>
    </div>
  );
};

GlobalLimitTrx.propTypes = {};

GlobalLimitTrx.defaultProps = {};

export default GlobalLimitTrx;
