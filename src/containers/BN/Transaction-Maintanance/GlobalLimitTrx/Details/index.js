import { makeStyles, Paper, Typography } from "@material-ui/core";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeleteConfirmation from "components/BN/Popups/Delete";
import AntdTextField from "components/BN/TextField/AntdTextField";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addDataGlobalLimitDetail,
  setGlobalLimit,
  setIsAdd,
  setSuccessConfirmationGlobalLimit,
} from "stores/actions/globalLimitTrx";

import FormField from "components/BN/Form/InputGroupValidation";
import SelectGroup from "components/BN/Select/SelectWithSearch";
import formatNumber from "helpers/formatNumber";
import { parseNum } from "utils/helpers";
import { useValidateLms } from "utils/helpers/validateLms";
import { rest } from "lodash";
import {
  handleValidateOnSubmit,
  handlevalidationDataLms,
  setAlreadyExist,
  setClearAlreadyExist,
} from "stores/actions/validatedatalms";
import useDebounce from "utils/helpers/useDebounce";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";

const useStyles = makeStyles({
  page: {},
  title: {
    color: "#374062",
    fontWeight: 900,
    fontSize: "15px",
    marginBottom: "20px",
  },
  label: {
    fontFamily: "FuturaBkBT",
    color: "#374062",
    fontSize: "13px",
  },
  badge: {
    width: "38px",
    height: "38px",
    backgroundColor: "#F8F8FF",
    borderRadius: "10px",
    textAlign: "center",
    fontSize: "15px",
    color: "#BCC8E7",
    fontFamily: "FuturaBkBT",
    marginRight: "14px",
    cursor: "pointer",
    "&:active": {
      backgroundColor: "#0061A7",
      color: "#fff",
    },
    "&:hover": {
      backgroundColor: "#0061A7",
      color: "#fff",
    },
    lineHeight: "38px",
  },
  container: {
    marginTop: "30px",
  },

  badgeActive: {
    backgroundColor: "#0061A7",
    color: "#fff",
  },
  inputan: {
    padding: "10px",
    cursor: "auto",
  },
});

const DataNotEmpaty = ({ isLoading, serviceSelected }) => {
  const {
    openSuccessGLTRX,
    currencyList,
    dataCard,
    dataGlobalLimitTemp,
    dataGlobalLimit,
  } = useSelector((state) => state.globalLimitTrx);

  const { isAlreadyExist } = useSelector((e) => e.validatedatalms);

  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const classes = useStyles();
  const dispatch = useDispatch();

  const mappingForm = dataGlobalLimit[dataCard?.index];

  const handleValidate = ({ name, value }) => {
    dispatch(
      handlevalidationDataLms({
        globalLimitDto: {
          serviceCurrencyMatrix: {
            id: mappingForm?.serviceCurrencyMatrix?.id,
          },
          [name]: value,
        },
        key: `${name}${dataCard.index}`,
        table: "38",
      })
    );
  };

  const dmaxAmountTransaction = useDebounce(
    dataGlobalLimit[dataCard?.index].maxAmountTransaction,
    1300
  );
  const dmaxAmountTransactionOfDay = useDebounce(
    dataGlobalLimit[dataCard?.index].maxAmountTransactionOfDay,
    1300
  );
  const dmaxTransaction = useDebounce(
    dataGlobalLimit[dataCard?.index].maxTransaction,
    1300
  );
  const dminAmountTransaction = useDebounce(
    dataGlobalLimit[dataCard?.index].minAmountTransaction,
    1300
  );

  useEffect(() => {
    if (
      dmaxAmountTransactionOfDay !==
      dataGlobalLimitTemp[dataCard?.index]?.maxAmountTransactionOfDay
    )
      handleValidate({
        name: "maxAmountTransactionOfDay",
        value: dmaxAmountTransactionOfDay,
      });
    if (
      dmaxAmountTransactionOfDay ===
      dataGlobalLimitTemp[dataCard?.index]?.maxAmountTransactionOfDay
    )
      dispatch(
        setAlreadyExist({
          [`maxAmountTransactionOfDay${dataCard?.index}`]: false,
        })
      );
  }, [dmaxAmountTransactionOfDay]);

  useEffect(() => {
    if (
      dmaxAmountTransaction !==
      dataGlobalLimitTemp[dataCard?.index]?.maxAmountTransaction
    )
      handleValidate({
        name: "maxAmountTransaction",
        value: dmaxAmountTransaction,
      });
    if (
      dmaxAmountTransaction ===
      dataGlobalLimitTemp[dataCard?.index]?.maxAmountTransaction
    )
      dispatch(
        setAlreadyExist({ [`maxAmountTransaction${dataCard?.index}`]: false })
      );
  }, [dmaxAmountTransaction]);

  useEffect(() => {
    if (
      dmaxTransaction !== dataGlobalLimitTemp[dataCard?.index]?.maxTransaction
    )
      handleValidate({
        name: "maxTransaction",
        value: dmaxTransaction,
      });
    if (
      dmaxTransaction === dataGlobalLimitTemp[dataCard?.index]?.maxTransaction
    )
      dispatch(
        setAlreadyExist({ [`maxTransaction${dataCard?.index}`]: false })
      );
  }, [dmaxTransaction]);

  useEffect(() => {
    if (
      dminAmountTransaction !==
      dataGlobalLimitTemp[dataCard?.index]?.minAmountTransaction
    )
      handleValidate({
        name: "minAmountTransaction",
        value: dminAmountTransaction,
      });
    if (
      dminAmountTransaction ===
      dataGlobalLimitTemp[dataCard?.index]?.minAmountTransaction
    )
      dispatch(
        setAlreadyExist({ [`minAmountTransaction${dataCard?.index}`]: false })
      );
  }, [dminAmountTransaction]);

  const onSaveData = () => {
    const editedData = dataGlobalLimit.filter((e) => e.edited);

    const payload = {
      globalLimitList: editedData?.map(
        ({
          id,
          currencyCode,
          maxAmountTransaction,
          maxAmountTransactionOfDay,
          maxTransaction,
          minAmountTransaction,
          ...rest
        }) => ({
          id,
          currencyCode,
          maxAmountTransaction,
          maxAmountTransactionOfDay,
          maxTransaction,
          minAmountTransaction,
          serviceCurrencyMatrix: {
            id: rest?.serviceCurrencyMatrix?.id,
          },
        })
      ),
    };

    dispatch(addDataGlobalLimitDetail(payload));
  };

  const handleChange = (name) => (e) => {
    const arr = [...dataGlobalLimit];
    arr[dataCard?.index] = {
      ...arr[dataCard?.index],
      [name]:
        name === "currencyCode" || name === "maxTransaction"
          ? e
          : Number(parseNum(e.target.value)),
      edited: true,
    };

    dispatch(setGlobalLimit(arr));
  };

  const handleClick = (value) => handleChange("maxTransaction")(value);

  const handleAlreadyExistArr = () => {
    const mappKeys = [...new Array(dataGlobalLimit.length)];
    const keys = [
      "maxAmountTransaction",
      "maxAmountTransactionOfDay",
      "maxTransaction",
      "minAmountTransaction",
    ];

    const errors = {};

    mappKeys.forEach((e, i) => {
      keys.forEach((elm) => {
        errors[`${elm}${i}`] = isAlreadyExist[`${elm}${i}`];
      });
    });

    return Object.keys(errors).find((key) => errors[key]);
  };

  const handleError = () => {
    const err = {
      maxAmountTransactionOfDay: false,
      maxAmountTransaction: false,
      minAmountTransaction: false,
      error: false,
    };

    let maxAmountTransactionOfDay =
      dataGlobalLimit[dataCard.index]?.maxAmountTransactionOfDay;
    let maxAmountTransaction =
      dataGlobalLimit[dataCard.index]?.maxAmountTransaction;
    let minAmountTransaction =
      dataGlobalLimit[dataCard.index]?.minAmountTransaction;

    maxAmountTransactionOfDay = Number(maxAmountTransactionOfDay) || 0;
    maxAmountTransaction = Number(maxAmountTransaction) || 0;
    minAmountTransaction = Number(minAmountTransaction) || 0;

    err.error =
      dataGlobalLimit.findIndex((subf) => {
        if (subf.minAmountTransaction <= 0) return true;
        if (subf.maxAmountTransactionOfDay < subf.maxAmountTransaction)
          return true;
        if (subf.maxAmountTransaction > subf.maxAmountTransactionOfDay)
          return true;
        if (subf.maxAmountTransaction < subf.minAmountTransaction) return true;

        return false;
      }) >= 0;

    if (maxAmountTransaction > 0)
      if (maxAmountTransaction < minAmountTransaction) {
        err.maxAmountTransaction =
          "The number entered is below minimum transaction";
      } else err.maxAmountTransaction = false;

    if (maxAmountTransactionOfDay > 0)
      if (maxAmountTransaction > maxAmountTransactionOfDay)
        err.maxAmountTransaction =
          "The number entered is exceeds maximum Nominal / Day";

    if (
      (maxAmountTransaction > 0 && maxAmountTransactionOfDay > 0) ||
      (minAmountTransaction > 0 && maxAmountTransactionOfDay > 0)
    )
      if (maxAmountTransactionOfDay < maxAmountTransaction)
        err.maxAmountTransactionOfDay =
          "The number entered is below maximum nominal transaction";
      else if (
        maxAmountTransactionOfDay > 0 &&
        minAmountTransaction > 0 &&
        maxAmountTransactionOfDay < minAmountTransaction
      )
        err.maxAmountTransactionOfDay =
          "The number entered is below minimum transaction";
      else err.maxAmountTransactionOfDay = false;

    return err;
  };

  return (
    <div className={classes.page}>
      <div>
        <SuccessConfirmation
          isOpen={openSuccessGLTRX}
          handleClose={() => {
            dispatch(setSuccessConfirmationGlobalLimit(false));
            dispatch(setIsAdd(false));
            setOpenModalConfirmation(false);
          }}
        />
        <DeleteConfirmation
          title="Confirmation"
          message="Are You Sure To Edit Data?"
          submessage="You cannot undo this action"
          isOpen={openModalConfirmation}
          loading={isLoading}
          handleClose={() => {
            setOpenModalConfirmation(false);
          }}
          onContinue={() => {
            onSaveData();
          }}
        />
        <Typography className={classes.title}>
          {mappingForm.serviceCurrencyMatrix?.currencyMatrix?.name}
        </Typography>
        <FormField
          label="Number of transactions per day"
          errorMessage={isAlreadyExist[`maxTransaction${dataCard?.index}`]}
          error={isAlreadyExist[`maxTransaction${dataCard?.index}`]}
        >
          <div
            style={{
              display: "flex",
              marginTop: "10px",
            }}
          >
            <Paper
              elevation={0}
              className={
                mappingForm.maxTransaction === 5
                  ? `${classes.badgeActive} ${classes.badge}`
                  : classes.badge
              }
              onClick={() => handleClick(5)}
            >
              5x
            </Paper>
            <Paper
              elevation={0}
              className={
                mappingForm.maxTransaction === 10
                  ? `${classes.badgeActive} ${classes.badge}`
                  : classes.badge
              }
              onClick={() => handleClick(10)}
            >
              10x
            </Paper>
            <Paper
              elevation={0}
              className={
                mappingForm.maxTransaction === 15
                  ? `${classes.badgeActive} ${classes.badge}`
                  : classes.badge
              }
              onClick={() => handleClick(15)}
            >
              15x
            </Paper>
            <Paper
              elevation={0}
              className={
                mappingForm.maxTransaction === 20
                  ? `${classes.badgeActive} ${classes.badge}`
                  : classes.badge
              }
              onClick={() => handleClick(20)}
            >
              20x
            </Paper>
            <Paper
              elevation={0}
              className={
                mappingForm.maxTransaction === 25
                  ? `${classes.badgeActive} ${classes.badge}`
                  : classes.badge
              }
              onClick={() => handleClick(25)}
            >
              25x
            </Paper>
            <Paper
              elevation={0}
              className={
                mappingForm.maxTransaction === 30
                  ? `${classes.badgeActive} ${classes.badge}`
                  : classes.badge
              }
              onClick={() => handleClick(30)}
            >
              30x
            </Paper>
            <AntdTextField
              value={mappingForm.maxTransaction}
              className={`${classes.badge} ${classes.inputan}`}
              onChange={(e) => handleClick(e.target.value)}
              style={{
                width: "80px",
              }}
            />
          </div>
        </FormField>
        <div className={classes.container}>
          <Typography className={classes.label}>Choose Currency :</Typography>
          <SelectGroup
            placeholder="Choose Currency"
            options={currencyList?.map((item) => ({
              label: `${item.currencyCode}-${item.nameEng}`,
              value: item.currencyCode,
            }))}
            value={mappingForm.currencyCode}
            name="currency"
            style={{
              width: "100%",
            }}
            onSelect={handleChange("currencyCode")}
          />
        </div>
        <div className={classes.container}>
          {/* <Typography className={classes.label}>
          Minimum Nominal Transaction :
        </Typography> */}
          <FormField
            label="Minimum Nominal Transaction :"
            errorMessage={
              handleError().minAmountTransaction
                ? handleError().minAmountTransaction
                : isAlreadyExist[`minAmountTransaction${dataCard?.index}`]
            }
            error={
              isAlreadyExist[`minAmountTransaction${dataCard?.index}`] ||
              handleError().minAmountTransaction
            }
            margin={{ margin: "0" }}
          >
            <AntdTextField
              style={{
                width: "100%",
              }}
              align="left"
              placeholder="IDR 100.000"
              value={formatNumber(mappingForm.minAmountTransaction)}
              name="minTrxPerDay"
              onChange={handleChange("minAmountTransaction")}
            />
          </FormField>
        </div>
        <div className={classes.container}>
          <FormField
            label="Maximum nominal per day :"
            errorMessage={
              handleError().maxAmountTransactionOfDay
                ? handleError().maxAmountTransactionOfDay
                : isAlreadyExist[`maxAmountTransactionOfDay${dataCard?.index}`]
            }
            error={
              isAlreadyExist[`maxAmountTransactionOfDay${dataCard?.index}`] ||
              handleError().maxAmountTransactionOfDay
            }
            margin={{ margin: "0" }}
          >
            <AntdTextField
              style={{
                width: "100%",
              }}
              align="left"
              placeholder="IDR 100.000"
              value={formatNumber(mappingForm.maxAmountTransactionOfDay)}
              name="maxTrxPerDay"
              onChange={handleChange("maxAmountTransactionOfDay")}
            />
          </FormField>
        </div>
        <div className={classes.container}>
          <FormField
            label="Maximum Transaction :"
            errorMessage={
              handleError().maxAmountTransaction
                ? handleError().maxAmountTransaction
                : isAlreadyExist[`maxAmountTransaction${dataCard?.index}`]
            }
            error={
              isAlreadyExist[`maxAmountTransaction${dataCard?.index}`] ||
              handleError().maxAmountTransaction
            }
            margin={{ margin: "0" }}
          >
            <AntdTextField
              style={{
                width: "100%",
              }}
              placeholder="Rp 100.000"
              value={formatNumber(mappingForm.maxAmountTransaction)}
              align="left"
              name="maxTrx"
              onChange={handleChange("maxAmountTransaction")}
            />
          </FormField>
        </div>
      </div>

      <div
        style={{
          display: "flex",
          backgroundColor: "#fff",
          justifyContent: "space-between",
          marginTop: "30%",
          padding: "20px",
          position: "fixed",
          bottom: 0,
          left: 200,
          right: 0,
        }}
      >
        <div>
          <ButtonOutlined
            label="Cancel"
            width="86px"
            color="#0061A7"
            onClick={() => {
              dispatch(setIsAdd(false));
              dispatch(setGlobalLimit(dataGlobalLimitTemp));
              dispatch(setClearAlreadyExist());
            }}
          />
        </div>

        <div>
          <GeneralButton
            onClick={() =>
              dispatch(
                validateTask(
                  {
                    menuName: validateTaskConstant?.GLOBAL_LIMIT,
                    code: null,
                    name: null,
                    validate: true,
                    id: serviceSelected?.id,
                  },
                  {
                    onContinue() {
                      setOpenModalConfirmation(true);
                    },
                    onError() {},
                  },
                  {
                    redirect: false,
                  }
                )
              )
            }
            label="Save"
            width="100px"
            height="43px"
            disabled={handleAlreadyExistArr() || handleError().error}
          />
        </div>
      </div>
    </div>
  );
};

export default DataNotEmpaty;
