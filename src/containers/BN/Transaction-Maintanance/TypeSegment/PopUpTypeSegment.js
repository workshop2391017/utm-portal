import { Backdrop, Fade, Modal, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import AntdTextField from "components/BN/TextField/AntdTextField";
import React, { useEffect, useState } from "react";

const useStyles = makeStyles({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: "477px",
    borderRadius: "8px",
    height: "464px",
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px",
    "& .title": {
      fontSize: "20px",
      fontFamily: "FuturaHvBT",
      color: "#374062",
      fontWeight: 400,
    },
    "& .card": {
      marginTop: "20px",
      "& .label": {
        fontSize: "13px",
        color: "#374062",
        fontWeight: 400,
        fontFamily: "FuturaBkBT",
      },
    },
    "& .buttonGroup": {
      marginTop: "80px",
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
  },

  page: {},
});

const PopUpTypeSegment = ({
  open,
  handleClose,
  onChange,
  onContinue,
  // formData: formDataProps,
}) => {
  const classes = useStyles();

  const [formData, setFormData] = useState({
    type: "",
    desc: "",
  });

  return (
    <div className={classes.page}>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Typography className="title">Add Type Segment Product</Typography>

            <div className="card">
              <Typography className="label">Type Segement:</Typography>
              <AntdTextField
                type="text"
                value={formData?.type}
                onChange={(e) =>
                  setFormData({ ...formData, type: e.target.value })
                }
                placeholder="enter type Segment"
                style={{
                  width: "100%",
                }}
              />
            </div>

            <div className="card">
              <Typography className="label">Description :</Typography>
              <AntdTextField
                value={formData?.desc}
                type="textArea"
                onChange={(e) =>
                  setFormData({ ...formData, desc: e.target.value })
                }
                placeholder="Comment"
                style={{
                  height: "88px",
                }}
              />
            </div>

            <div className="buttonGroup">
              <div>
                <ButtonOutlined
                  label="Cancel"
                  width="86px"
                  style={{ marginRight: 30 }}
                  color="#0061A7"
                  onClick={handleClose}
                />
              </div>
              <div>
                <GeneralButton onClick={onContinue(formData)} label="Save" />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default PopUpTypeSegment;
