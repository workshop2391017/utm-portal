import { makeStyles, Typography } from "@material-ui/core";
import React, { useState } from "react";

import GeneralButton from "components/BN/Button/GeneralButton";
import useDebounce from "utils/helpers/useDebounce";
import { ReactComponent as SvgPlus } from "assets/icons/BN/plus-white.svg";

import DeleteConfirmation from "components/BN/Popups/Delete";

import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import TableICBB from "../../../../components/BN/TableIcBB";
import Search from "../../../../components/BN/Search/SearchWithoutDropdown";
import { ReactComponent as SvgEdit } from "../../../../assets/icons/BN/pena.svg";
import PopUpTypeSegment from "./PopUpTypeSegment";

const useStyles = makeStyles({
  card: {
    display: "flex",
  },

  page: {
    backgroundColor: "#F4F7FB",
    padding: "20px",

    width: "100%",
    height: "100%",
    "& .header": {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: "20px",
      "& .title": {
        fontSize: "20px",
        fontFamily: "FuturaHvBT",
        fontWeight: 400,
        color: " #374062",
      },
    },
  },
});

const dummyDataTypegment = [
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
  {
    typeSegment: "IBB",
    describtion: "Lorem",
  },
];

const TypeSegment = () => {
  const classes = useStyles();
  const [searchValue, setSearchValue] = useState(null);
  const [page, setPage] = useState(1);
  const [openModal, setOpenModal] = useState(false);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [comment, setComment] = useState("");
  const [isOpen, setOpenModalOpen] = useState(false);

  const dataHeader = [
    {
      title: "Type Segment",
      key: "typeSegment",
    },
    {
      title: "Description",
      key: "describtion",
    },
    {
      title: "",

      render: (rowData) => (
        <div
          style={{
            display: "flex",
            width: "100%",
            justifyContent: "flex-end",
          }}
        >
          <GeneralButton
            width="76px"
            height="23px"
            label="Edit"
            buttonIcon={<SvgEdit width={16.67} height={16.67} />}
            style={{
              cursor: "pointer",
              fontSize: "14px",
            }}
            onClick={() => console.warn("testing saja")}
          />
        </div>
      ),
    },
  ];
  const hasil = useDebounce(searchValue, 1500);

  return (
    <div className={classes.page}>
      <SuccessConfirmation
        isOpen={isOpen}
        title="Change Successful"
        message="Change Successful"
        submessage="Please wait for approval"
        handleClose={() => {
          setOpenModalOpen(false);
        }}
      />
      <div className="header">
        <Typography className="title">Type Segment Product</Typography>

        <div className={classes.card}>
          <Search
            style={{
              width: 286,
            }}
            dataSearch={searchValue}
            setDataSearch={setSearchValue}
          />
          <GeneralButton
            iconPosition="startIcon"
            buttonIcon={<SvgPlus width={16.67} height={16.67} />}
            label="Type Segment Product"
            width={220}
            style={{
              marginLeft: "10px",
            }}
            onClick={() => {
              console.warn("testing saja");

              setOpenModal(true);
            }}
          />
        </div>
      </div>

      <div>
        <TableICBB
          // isLoading={isLoading}
          headerContent={dataHeader}
          dataContent={dummyDataTypegment}
          page={page}
          setPage={setPage}
          totalElement={10}
          totalData={40}
        />
      </div>

      <PopUpTypeSegment
        open={openModal}
        handleClose={() => setOpenModal(false)}
        onContinue={(e) => {
          // setOpenModalConfirmation(true);
          console.warn("eee:", e);
        }}
      />

      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Add Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        // loading={isLoading}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          setOpenModal(false);
        }}
      />
    </div>
  );
};

export default TypeSegment;
