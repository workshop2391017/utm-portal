import React, { useEffect, useState } from "react";
import { Typography, makeStyles, Avatar, Paper } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import {
  addDataGlobalLimitTrx,
  setIsAdd,
  setSuccessConfirmation,
  setSuccessConfirmationGlobalLimit,
  getValidationService,
  setServiceAlready,
} from "stores/actions/globalLimitTrx";
import { validateMinMax } from "utils/helpers";
import AntdTextField from "components/BN/TextField/AntdTextField";
import GeneralButton from "components/BN/Button/GeneralButton";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import FormField from "components/BN/Form/InputGroupValidation";
import SelectGroup from "components/BN/Select/SelectWithSearch";
import DeletePopup from "components/BN/Popups/Delete";
import IconExit from "assets/icons/BN/exit.svg";
import IconAdd from "assets/icons/BN/add.svg";
import useDebounce from "utils/helpers/useDebounce";
import { validateTask } from "stores/actions/validateTaskPortal";
import { validateTaskConstant } from "stores/actions/validateTaskPortal/constantValidateTask";

const useStyles = makeStyles({
  label: {
    fontSize: "13px",
    fontWeight: 400,
    fontFamily: "FuturaBkBT",
    color: "#374062",
    marginBottom: "5px",
  },
  badge: {
    border: "1px solid #66A3FF",
    color: "#66A3FF",
    fontWeight: 400,
    fontSize: "15px",
    height: "26px",
    width: "167px",
    borderRadius: "4px",
    padding: "4px 10px",
    backgroundColor: "#EAF2FF",
    lineHeight: "26px",
    position: "relative",
    textAlign: "center",
    alignItems: "center",
    marginRight: "8px",
    fontFamily: "FuturaBkBT",
    marginBottom: "10px",
  },
  badgejrx: {
    border: "1px solid #AEB3C6",
    color: "##AEB3C6",
    fontWeight: 400,
    fontSize: "15px",
    height: "26px",
    width: "167px",
    borderRadius: "4px",
    padding: "4px 10px",
    backgroundColor: "#EAF2FF",
    lineHeight: "26px",
    position: "relative",
    textAlign: "center",
    alignItems: "center",
    marginRight: "8px",
    fontFamily: "FuturaBkBT",
    marginTop: "10px",
  },
  container: {
    position: "relative",
    width: "760px",
    marginLeft: "20px",
    padding: "50px",
    backgroundColor: "#fff",
    borderRadius: "10px",
  },
  card: {
    margin: "auto",
    // backgroundColor: "green",
    marginLeft: "122px",
  },
  error: {
    margin: "0 0 10px 10px",
    textAlign: "left",
    fontSize: "10px",
    color: "red",
  },
});

const dataCardTrx = [
  {
    id: 1,
    name: "Forex-Forex",
    chacked: false,
  },
  {
    id: 2,
    name: "Forex-Forex",
    chacked: false,
  },
  {
    id: 3,
    name: "Local-Local",
    chacked: false,
  },
  {
    id: 4,
    name: "Local-Forex",
    chacked: false,
  },
];

function TambahService() {
  const {
    transactionCategoryList,
    serviceCurrencyMatrixList,
    openSuccess,
    serviceList,
    dataMatrixDetail,
    isAddService,
    openSuccessGLTRX,
    isAlreadyExist,
  } = useSelector((state) => state.globalLimitTrx);
  const dispatch = useDispatch();
  // const [dataTrx, setDataTrx] = useState(dataCardTrx);
  const classes = useStyles();
  const [category, setCategory] = useState(null);
  const [categoryName, setCategoryName] = useState("");
  const [name, setName] = useState("");
  const [dataMatrix, setDataMatrix] = useState([]);
  const [isError, setIsError] = useState(false);
  const [isOpenConfirmation, setIsOpenConfirmation] = useState(false);
  const nameValidation = useDebounce(name, 1000);
  const [disableErrorTransaction, setDisableErrorTransaction] = useState(false);

  const toTitleCase = (str) =>
    str.replace(
      /\w\S*/g,
      (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    );
  // useEffect(() => {
  //   if (dataMatrixDetail.length > 0 && transactionCategoryList.length > 0) {
  //     const dataFilter = transactionCategoryList.filter(
  //       (item) => item.id === dataMatrixDetail[0]?.transactionCategoryId
  //     );

  //     if (dataFilter.length > 0) {
  //       setCategoryName(toTitleCase(dataFilter[0].name?.split("_").join(" ")));
  //       setCategory(dataFilter[0]?.transactionCategoryId);
  //     } else {
  //       setCategoryName("");
  //       setCategory(null);
  //     }
  //   }
  // }, [transactionCategoryList, dataMatrixDetail]);

  useEffect(() => {
    if (transactionCategoryList.length > 0) {
      const dataFilter = transactionCategoryList.filter(
        (item) => item.id === category
      );
      if (dataFilter.length > 0) {
        setCategoryName(toTitleCase(dataFilter[0].name?.split("_").join(" ")));
      }
    }
  }, [transactionCategoryList, category]);

  useEffect(() => {
    if (dataMatrixDetail.length > 0) {
      if (dataMatrixDetail[0]?.serviceName !== name) {
        dispatch(
          getValidationService({
            service: {
              name: nameValidation,
              transactionCategory: {
                id: null,
              },
            },
            table: 40,
          })
        );
      } else {
        dispatch(setServiceAlready(false));
      }
    }
  }, [nameValidation]);

  useEffect(() => {
    if (Object.keys(dataMatrixDetail).length !== 0) {
      if (category !== dataMatrixDetail[0]?.transactionCategoryId) {
        dispatch(
          getValidationService({
            service: {
              name: null,
              transactionCategory: {
                id: category,
              },
            },
            table: 40,
          })
        );
      } else {
        dispatch(setServiceAlready(false));
      }
    }
  }, [category]);

  useEffect(() => {
    // serviceCurrencyMatrixList?.forEach((item) => {
    //   const dataObject = {};
    //   dataObject.id = item?.id;
    //   dataObject.name = item?.name;
    //   dataObject.chacked = false;
    //   dataMatrix.push(dataObject);
    // });

    if (serviceCurrencyMatrixList.length > 0 && isAddService === 1) {
      const data = serviceCurrencyMatrixList.map((item) => ({
        id: item?.id,
        name: item?.name,
        chacked: false,
      }));
      setCategory(null);
      setName("");
      setDataMatrix(data);
    }
  }, [serviceCurrencyMatrixList, isAddService]);

  useEffect(() => {
    if (
      dataMatrixDetail.length > 0 &&
      isAddService === 2 &&
      serviceCurrencyMatrixList.length > 0
    ) {
      setName(dataMatrixDetail[0]?.serviceName);
      setCategory(dataMatrixDetail[0]?.transactionCategoryId);
      setCategoryName(
        toTitleCase(
          dataMatrixDetail[0]?.transactionCategoryName?.split("_").join(" ")
        )
      );
      const dataMap = serviceCurrencyMatrixList.map((item) => ({
        id: item?.id,
        name: item?.name,
        chacked: false,
      }));
      const data = dataMatrixDetail[0].serviceCurrencyMatrix.map((item) => ({
        id: item?.currencyId,
        name: item?.currencyName,
        chacked: true,
      }));

      const dataFilter = dataMap.filter(
        (el) => !data.find((element) => element.id === el.id)
      );
      const result = [...data, ...dataFilter];

      setDataMatrix(result);
    }
  }, [dataMatrixDetail, serviceCurrencyMatrixList, isAddService]);

  const handleClick = (id) => {
    const dataTampung = [...dataMatrix];

    const result = dataTampung.find((item) => item.id === id);

    const sisa = dataTampung.filter((item) => item.id !== id);

    if (result.chacked) {
      setDataMatrix([...sisa, { ...result, chacked: false }]);

      // ini buat hapus
    } else {
      setDataMatrix([...sisa, { ...result, chacked: true }]);
    }
  };

  // const conversiName = (firstInput, data) => {
  //   const resulFilter = data?.filter(
  //     (item) => item?.name.toLowerCase() === firstInput.toLowerCase()
  //   );

  //   if (resulFilter.length > 0) {
  //     return true;
  //   }
  //   return false;
  // };

  const handlePopUpConfirmation = () => {
    // if (name.length > 0 && conversiName(name, serviceList) === false) {
    if (name.length > 0) {
      setIsOpenConfirmation(true);
    }

    if (name.length <= 0) {
      setIsError(true);
    }

    // if (conversiName(name, serviceList)) {
    //   setIsError(true);
    // }
  };

  const onSaveData = () => {
    setIsOpenConfirmation(false);
    const dataMatrixTampung = [];
    dataMatrix
      .filter((item) => item.chacked === true)
      .forEach((val) => {
        dataMatrixTampung.push(val.id);
      });

    const formData = {
      currencyMatrixIdList: dataMatrixTampung,
      nameService: name,
      serviceId:
        dataMatrixDetail.length > 0 ? dataMatrixDetail[0]?.serviceId : null,
      transactionCategoryId: parseInt(category),
    };
    dispatch(addDataGlobalLimitTrx(formData));
  };

  const handleDisabledMatrix = () => {
    let error = false;
    const validate = dataMatrix.filter((item) => item.chacked === true);
    if (validate.length <= 0) error = true;

    return error;
  };

  return (
    <React.Fragment>
      <SuccessConfirmation
        isOpen={openSuccess}
        handleClose={() => {
          dispatch(setSuccessConfirmation(false));
          dispatch(setIsAdd(false));
        }}
      />
      <div className={classes.container}>
        <div className={classes.card}>
          <div style={{ display: "flex", flexDirection: "column", gap: 8 }}>
            <span
              style={{
                fontFamily: "FuturaMdBT",
                fontSize: "16px",
                color: "#374062",
                letterSpacing: "0.03em",
                fontWeight: 400,
              }}
            >
              {isAddService === 1 ? "Form Add Service" : "Form Edit Service"}
            </span>
            <span
              style={{
                fontFamily: "FuturaBkBT",
                fontSize: "13px",
                color: "#374062",
                letterSpacing: "0.01em",
                fontWeight: 400,
              }}
            >
              {isAddService === 1
                ? "Please fill in the data below to Add the service"
                : "Please fill in the data below to Edit the service"}
            </span>
          </div>
          <div
            style={{
              marginTop: 12,
              display: "flex",
            }}
          >
            <div>
              <FormField
                label="Transaction Category :"
                error={isAlreadyExist && disableErrorTransaction === false}
                errorMessage="Transaction category is already exist!"
              >
                <SelectGroup
                  placeholder="Select Transaction Category"
                  options={transactionCategoryList
                    ?.filter(
                      (item) =>
                        item.name !== "MONTHLY" && item.name !== "ANNUAL"
                    )
                    .map((item) => ({
                      label: toTitleCase(item?.name?.split("_").join(" ")),
                      value: item.id,
                    }))}
                  disabled={
                    isAlreadyExist &&
                    name !== "" &&
                    disableErrorTransaction === true
                  }
                  value={categoryName}
                  name="categori"
                  style={{
                    width: "226px",
                    marginRight: "22px",
                  }}
                  onSelect={(e) => {
                    setDisableErrorTransaction(false);
                    setCategory(e);
                    if (Object.keys(dataMatrixDetail).length === 0) {
                      setTimeout(
                        dispatch(
                          getValidationService({
                            service: {
                              name: null,
                              transactionCategory: {
                                id: e,
                              },
                            },
                            table: 40,
                          })
                        ),
                        1000
                      );
                    }
                  }}
                />
              </FormField>
            </div>

            <div>
              {/* <Typography className={classes.label}>Nama Service : </Typography> */}

              <FormField
                label="Service Name :"
                error={
                  !validateMinMax(name, 2, 50) ||
                  (isAlreadyExist && name !== "" && disableErrorTransaction)
                }
                errorMessage={
                  isAlreadyExist
                    ? "Service Name already exist!"
                    : "Min 2 and Max 50 Character!"
                }
                margin={{ margin: "0" }}
              >
                <AntdTextField
                  disabled={
                    category === "" ||
                    category === null ||
                    (category !== null &&
                      isAlreadyExist &&
                      disableErrorTransaction === false)
                  }
                  style={{
                    width: "226px",
                  }}
                  onFocus={() => {
                    setDisableErrorTransaction(true);
                    setIsError(false);
                  }}
                  placeholder="Nama Service"
                  value={name}
                  name="name_service"
                  maxLength="50"
                  onChange={(e) => {
                    setName(e.target.value);

                    if (Object.keys(dataMatrixDetail).length === 0) {
                      setTimeout(
                        dispatch(
                          getValidationService({
                            service: {
                              name: e.target.value,
                              transactionCategory: {
                                id: null,
                              },
                            },
                            table: 40,
                          })
                        ),
                        1000
                      );
                    }
                  }}
                />
                {isError && (
                  <p className={classes.error}>Name is Required And not Same</p>
                )}
              </FormField>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              marginTop: "40px",
              flexWrap: "wrap",
            }}
          >
            {dataMatrix
              ?.filter((val) => val.chacked === true)
              .map((item, index) => (
                <div key={index} className={classes.badge}>
                  <div
                    style={{
                      display: "flex",
                      marginTop: "-5px",
                    }}
                  >
                    {item.name}

                    <Paper
                      elevation={0}
                      onClick={() => {
                        handleClick(item.id);
                      }}
                    >
                      <Avatar
                        src={IconExit}
                        style={{
                          width: "14px",
                          height: "14px",
                          position: "absolute",
                          right: "6px",
                          bottom: "6px",
                        }}
                      />
                    </Paper>
                  </div>
                </div>
              ))}
          </div>
          <div
            style={{
              marginTop: "62px",
            }}
          >
            <Typography className={classes.label}>
              Select the type of transaction to be used:
            </Typography>
            <div
              style={{
                display: "flex",
                marginTop: "12px",

                flexWrap: "wrap",
                marginBottom: "20px",
              }}
            >
              {dataMatrix
                ?.filter((val) => val.chacked === false)
                .map((item, index) => (
                  <div key={index} className={classes.badgejrx}>
                    <div
                      style={{
                        display: "flex",

                        marginTop: "-5px",
                      }}
                    >
                      {item?.name}

                      <Paper
                        elevation={0}
                        onClick={() => {
                          handleClick(item.id);
                        }}
                      >
                        <Avatar
                          src={IconAdd}
                          style={{
                            width: "14px",
                            height: "14px",
                            position: "absolute",
                            right: "6px",
                            bottom: "6px",
                          }}
                        />
                      </Paper>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "space-between",

            position: "absolute",
            bottom: "20px",
            left: "20px",
            right: "20px",
          }}
        >
          <ButtonOutlined
            label="Cancel"
            width="86px"
            color="#0061A7"
            onClick={() => dispatch(setIsAdd(0))}
          />

          <GeneralButton
            onClick={() => {
              if (isAddService === 1) {
                handlePopUpConfirmation();
              } else {
                dispatch(
                  validateTask(
                    {
                      menuName: validateTaskConstant.SERVICE_GLOBAL_LIMIT,
                      code: null,
                      id:
                        dataMatrixDetail.length > 0
                          ? dataMatrixDetail[0]?.serviceId
                          : null,
                      name: null,
                      validate: true,
                    },
                    {
                      onContinue() {
                        handlePopUpConfirmation();
                      },
                      onError() {},
                    },
                    {
                      redirect: false,
                    }
                  )
                );
              }
            }}
            label="Save"
            width="100px"
            height="43px"
            disabled={
              !validateMinMax(name, 2, 50) ||
              category === undefined ||
              category === "" ||
              name === "" ||
              isAlreadyExist ||
              handleDisabledMatrix()
            }
          />
        </div>
        <SuccessConfirmation
          isOpen={openSuccessGLTRX}
          handleClose={() => {
            dispatch(setSuccessConfirmationGlobalLimit(true));
          }}
        />
        <DeletePopup
          message={
            isAddService === 1
              ? "Are You Sure To Add Data?"
              : "Are You Sure To Edit Your Data?"
          }
          handleClose={() => setIsOpenConfirmation(false)}
          isOpen={isOpenConfirmation}
          onContinue={() => {
            onSaveData();
          }}
        />
      </div>
    </React.Fragment>
  );
}

export default TambahService;
