import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import {
  getDataCurrencyMatrix,
  getDataCurrencyTransactionMatrix,
  getDataGlobalLimit,
  getDataService,
  getDataServiceTransactionCategory,
  setGetDataMatrix,
  setGlobalLimit,
  setHandleClearError,
  setIsAdd,
  setServiceAlready,
} from "stores/actions/globalLimitTrx";
import GeneralButton from "components/BN/Button/GeneralButton";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Toast from "components/BN/Toats";
import MenuListOptional from "components/BN/MenuList/MenuListOptional";
import Title from "components/BN/Title";
import TambahService from "./components/TambahService";

// const data = [
//   {
//     title: "Local-Local",
//   },
//   {
//     title: "Local-Forex",
//   },
//   {
//     title: "Forex - Forex Same",
//   },
//   {
//     title: "Local-Local",
//   },
//   {
//     title: "Local-Local",
//   },
//   {
//     title: "Local-Local",
//   },
//   {
//     title: "Local-Local",
//   },
//   {
//     title: "Local-Local",
//   },
// ];

// const dataCard = [
//   {
//     id: 1,
//     name: "Pindah Dana",
//   },
//   {
//     id: 2,
//     name: "Sesama Bank",
//   },
//   {
//     id: 3,
//     name: "Online",
//   },
//   {
//     id: 4,
//     name: "SKN",
//   },
//   {
//     id: 5,
//     name: "RTGS",
//   },
//   {
//     id: 6,
//     name: "BI Fast",
//   },
// ];

const useStyles = makeStyles({
  billerKategori: {},
  page: {
    height: "100%",
    width: "100%",
    backgroundColor: "#F4F7FB",
  },
  container: {
    padding: "0px 20px 20px",
    display: "flex",
  },
  cardSection: {
    width: "260px",
    backgroundColor: "#FFF",
    borderTopLeftRadius: "10px",
    borderTopRightRadius: "10px",
  },
  cardTitle: {
    width: "260px",
    height: "38px",
    borderTopLeftRadius: "10px",
    borderTopRightRadius: "10px",
    fontSize: "15px",
    padding: "10px 20px",
    backgroundColor: "#0061A7",
    color: "#fff",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
  },
  listItem: {
    textAlign: "left",
    display: "block",
    width: "100%",
    cursor: "pointer",

    // border: "none",
    // backgroundColor: "#fff",
    height: "42px",
    padding: "14px 20px",
    "&:active": {
      backgroundColor: "#EAF2FF",
    },
    "&:hover": {
      backgroundColor: "#EAF2FF",
    },
  },
  listText: {
    color: "#0061A7",
    fontSize: "12px",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
  },
  cardSection2: {
    width: 330,
    height: "100%",
    overflowY: "scroll",
    backgroundColor: "#F4F7FB",
    marginLeft: "20px",
  },
  card: {
    height: 190,
    width: 302,
    padding: 20,
    backgroundColor: "#fff",
    borderRadius: "10px",
    cursor: "pointer",
  },
  span: {
    background: "#F4F7FB",
    color: "#7B87AF",
    padding: "4px 8px",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    marginTop: "15px",
    width: "fit-content",
    fontSize: "10px",
  },
  title: {
    color: "#374062",
    fontWeight: 900,
    fontSize: "15px",
  },
  titleTransaksi: {
    fontSize: "10px",
    fontWeight: 900,
    fontFamily: "FuturaHvBT",
    color: "#AEB3C6",
  },
  subtiteTransaksi: {
    marginTop: "6px",
    color: "#374062",
    fontWeight: "900",
    fontSize: "10px",
  },
  loading: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    height: "100%",
    alignItems: "center",
  },
});

const ServiceLimitTransaction = (props) => {
  const { serviceList, isLoading, error, isAddService } = useSelector(
    (state) => state.globalLimitTrx
  );

  const dispatch = useDispatch();
  const history = useHistory();
  //   const [isService, setIsService] = useState(false);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [oneItemSide, setOneItemSide] = useState(0);

  const classes = useStyles();

  useEffect(() => {
    dispatch(getDataServiceTransactionCategory({ id: null }));
    dispatch(getDataCurrencyMatrix({ id: null }));
  }, []);

  useEffect(() => {
    dispatch(getDataService({ id: null }));
    // dispatch(getDataCurrencyMatrix({ id: null }));
    // const formData = {
    //   service: {
    //     id: 57,
    //   },
    // };

    // dispatch(getDataGlobalLimit(formData));
    return () => {
      dispatch(setServiceAlready(false));
    };
  }, []);

  const getService = (params) => {
    const { id } = params;

    const payload = {
      id: null,
      service: {
        id,
      },
    };

    dispatch(getDataCurrencyTransactionMatrix(payload));
  };

  const NoData = () => (
    <div
      style={{
        width: 760,
        height: 675,
        background: "#FFFFFF",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <span
        style={{ fontFamily: "FuturaMdBT", color: "#7B87AF", fontSize: 21 }}
      >
        Choose a Service First
      </span>
    </div>
  );

  return (
    <div className={classes.page}>
      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <SuccessConfirmation
        isOpen={openSuccess}
        handleClose={() => setOpenSuccess(false)}
      />
      <Title label="Service" />
      <div className={classes.container}>
        <Grid className={classes.cardSection}>
          <MenuListOptional
            titleMenu="Service"
            listData={serviceList}
            onClick={(item) => {
              getService(item.data);
              setOneItemSide(item);
              dispatch(setIsAdd(2));
              dispatch(setServiceAlready(false));
            }}
            selectedItem={oneItemSide}
            childKey="name"
            maxHeight="500px"
            isLoading={isLoading}
          />
          <div
            style={{
              display: "flex",
            }}
          >
            <GeneralButton
              label="Add Service"
              width="134px"
              style={{
                margin: "40px auto",
              }}
              onClick={() => {
                dispatch(setGetDataMatrix({}));
                dispatch(setServiceAlready(false));
                dispatch(setGlobalLimit({}));
                setOneItemSide(0);
                dispatch(setIsAdd(1));
              }}
            />
          </div>
        </Grid>
        {isAddService === 1 ? (
          <TambahService />
        ) : isAddService === 2 ? (
          <TambahService />
        ) : (
          <NoData />
        )}
      </div>
    </div>
  );
};

ServiceLimitTransaction.propTypes = {};

ServiceLimitTransaction.defaultProps = {};

export default ServiceLimitTransaction;
