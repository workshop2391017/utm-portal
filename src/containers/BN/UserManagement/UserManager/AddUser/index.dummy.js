export const roleDataDummy = {
  olistRole: [
    {
      roleId: 1,
      roleName: "ADMIN_APPROVER",
      description: "IBB",
      userTotal: 42,
      deleted: false,
    },
    {
      roleId: 2,
      roleName: "ADMIN_MAKER",
      description: "IBB",
      userTotal: 26,
      deleted: false,
    },
    {
      roleId: 3,
      roleName: "ADMIN_MAKER_APPROVER",
      description: "IBB",
      userTotal: 1,
      deleted: false,
    },
  ],
};

export const dataUnitDummy = [
  {
    unitId: 1,
    unitName: "Operational",
  },
  {
    unitId: 2,
    unitName: "HR",
  },
  {
    unitId: 3,
    unitName: "Finance",
  },
  {
    unitId: 4,
    unitName: "Marketing",
  },
  {
    unitId: 5,
    unitName: "Sales",
  },
];

export const dataRoleDummy = [
  {
    roleId: 1,
    roleName: "Monitoring",
  },
  {
    roleId: 2,
    roleName: "Bisnis",
  },
  {
    roleId: 3,
    roleName: "IT",
  },
];

export const dataWorkflowDummy = [
  {
    workflowId: 1,
    workflowName: "Maker",
  },
  {
    workflowId: 2,
    workflowName: "Approver",
  },
];

export const dataGroupBranchIdDummy = [];
