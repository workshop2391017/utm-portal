import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { makeStyles, Button, Typography } from "@material-ui/core";

import {
  AddSaveInternationalBank,
  setTypeDetailEdit,
  setTypeIsOpen,
} from "stores/actions/internationalBank";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";

import DeleteConfirmation from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import Footer from "components/BN/Footer";
import TextField from "components/BN/TextField/AntdTextField";
import Select from "components/BN/Select/SelectGroup";

import {
  dataUnitDummy,
  dataRoleDummy,
  dataWorkflowDummy,
} from "containers/BN/UserManagement/UserManager/AddUser/index.dummy";

const useStyles = makeStyles((theme) => ({
  backButton: {
    ...theme.typography.backButton,
  },
  page: {
    backgroundColor: "#F4F7FB",
    display: "flex",
    flexDirection: "column",
    height: "100%",
  },
  paper: {
    width: 696,
    height: 400,
    backgroundColor: "#fff",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    marginTop: "20px",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
  },
  subTitle: {
    fontFamily: "FuturaMdBT",
    fontSize: 14,
    fontWeight: 400,
    marginBottom: 30,
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 110,
  },
}));

const AddNewUser = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { dataCurrencyList, dataDetail, isOpen, isLoading, validate } =
    useSelector(({ internationalBank }) => internationalBank);

  const [codeValidateTask, setCodeValidationTask] = useState("");
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);

  const [fullname, setFullname] = useState(null);
  const [email, setEmail] = useState(null);
  const [phone, setPhone] = useState(null);
  const [unit, setUnit] = useState(null);
  const [workflow, setWorkflow] = useState(null);
  const [role, setRole] = useState(null);

  const handleDisabledSave = () => {
    if (fullname && email && phone && unit && workflow && role) {
      return false;
    } else {
      return true;
    }
  };

  useEffect(() => {
    return () => {
      dispatch(setTypeDetailEdit(null));
    };
  }, []);

  const handleBack = () => {
    dispatch(setTypeDetailEdit(null));
    history.goBack();
  };

  const handleNext = () => {
    setOpenModalConfirmation(false);
  };

  const onSaveHandler = () => {
    if (dataDetail !== null) {
      const payload = {};

      dispatch(AddSaveInternationalBank(payload, codeValidateTask));
    } else {
      const payload = {};

      dispatch(AddSaveInternationalBank(payload, codeValidateTask, handleNext));
    }
    setOpenModalConfirmation(false);
  };

  return (
    <div className={classes.page}>
      <div style={{ padding: "8px 20px 0 20px" }}>
        <Button
          disableElevation
          component="button"
          size="small"
          startIcon={<ArrowLeft />}
          className={classes.backButton}
          onClick={handleBack}
        >
          Back
        </Button>
        <Typography variant="h2">
          {dataDetail ? "Edit" : "Add"} Add New User
        </Typography>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          margin: "auto",
        }}
      >
        <div className={classes.paper}>
          <h1 className={classes.title}>Form Add New User</h1>
          <h3 className={classes.subTitle}>
            Please fill in the data below to add new user
          </h3>
          <div style={{ display: "flex" }}>
            <div style={{ width: "50%", paddingRight: 10 }}>
              <div style={{ marginBottom: 24, position: "relative" }}>
                <p
                  style={{
                    marginBottom: 2,
                  }}
                >
                  Full Name :
                </p>
                <TextField
                  disabled={false}
                  value={fullname}
                  onChange={(e) => setFullname(e.target.value)}
                  placeholder="Full Name"
                  style={{
                    width: 300,
                    height: 40,
                  }}
                />
              </div>
              <div style={{ marginBottom: 24 }}>
                <p style={{ marginBottom: 2 }}>Email :</p>
                <TextField
                  value={email}
                  disabled={false}
                  placeholder="xyz@example.com"
                  onChange={(e) => setEmail(e.target.value)}
                  style={{ width: 300, height: 40 }}
                />
              </div>
              <div className={classes.input}>
                <p style={{ marginBottom: 3 }}>Role :</p>
                <Select
                  disabled={false}
                  value={role}
                  onChange={(value) => setRole(value)}
                  placeholder="Select Role"
                  options={[
                    {
                      name: "Select Role",
                      options: dataRoleDummy.map((elm) => ({
                        name: elm?.roleName,
                        id: elm?.roleId,
                      })),
                    },
                  ]}
                  style={{ width: 300, height: 40 }}
                />
              </div>
            </div>
            <div style={{ width: "50%", paddingLeft: 10 }}>
              <div style={{ marginBottom: 24 }}>
                <p style={{ marginBottom: 2 }}>Phone :</p>
                <TextField
                  value={phone}
                  disabled={false}
                  placeholder="Phone"
                  onChange={(e) => setPhone(e.target.value)}
                  style={{ width: 300, height: 40 }}
                />
              </div>
              <div style={{ marginBottom: 24 }}>
                <p style={{ marginBottom: 2 }}>Unit :</p>
                <Select
                  disabled={false}
                  value={unit}
                  onChange={(value) => setUnit(value)}
                  placeholder="Select Unit"
                  options={[
                    {
                      name: "Select Unit",
                      options: dataUnitDummy.map((elm) => ({
                        name: elm?.unitName,
                        id: elm?.unitId,
                      })),
                    },
                  ]}
                  style={{ width: 300, height: 40 }}
                />
              </div>
              <div className={classes.input} style={{ marginBottom: 24 }}>
                <p style={{ marginBottom: 3 }}>Workflow :</p>
                <Select
                  disabled={false}
                  value={workflow}
                  onChange={(value) => setWorkflow(value)}
                  placeholder="Select Workflow"
                  options={[
                    {
                      name: "Select Workflow",
                      options: dataWorkflowDummy.map((elm) => ({
                        name: elm?.workflowName,
                        id: elm?.workflowId,
                      })),
                    },
                  ]}
                  style={{ width: 300, height: 40 }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer
        disabled={handleDisabledSave()}
        onCancel={() => {
          dispatch(setTypeDetailEdit(null));
          history.goBack();
        }}
        onContinue={() => setOpenModalConfirmation(true)}
        disableElevation
      />
      <SuccessConfirmation
        isOpen={isOpen}
        handleClose={() => {
          dispatch(setTypeIsOpen(false));
          dispatch(setTypeDetailEdit(null));
          setOpenModalConfirmation(false);
          history.goBack();
        }}
      />

      <DeleteConfirmation
        title="Confirmation"
        message={
          dataDetail
            ? "Are You Sure To Edit Data?"
            : "Are You Sure To Add Data?"
        }
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoading}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={onSaveHandler}
      />
    </div>
  );
};

export default AddNewUser;
