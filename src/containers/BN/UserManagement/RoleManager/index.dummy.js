export const dataRoleList = [
  {
    roleId: 1,
    roleName: "Super Admin",
    workflow: false,
    menu: [
      {
        id: 70,
        name_id: "User Management",
        name_en: "User Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 74,
            name_id: "User Manager",
            name_en: "User Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 72,
            name_id: "Role Manager",
            name_en: "Role Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 73,
            name_id: "Menu Manager",
            name_en: "Menu Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 71,
            name_id: "Unit Manager",
            name_en: "Unit Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 91,
        name_id: "Global Parameter",
        name_en: "Global Parameter",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 92,
            name_id: "General Parameter",
            name_en: "General Parameter ",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 93,
            name_id: "Host Error Mapping",
            name_en: "Host Error Mapping",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 107,
        name_id: "Transaction Management",
        name_en: "Transaction Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 108,
            name_id: "Transaction History",
            name_en: "Transaction History",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 83,
        name_id: "Limit Management",
        name_en: "Limit Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 84,
            name_id: "Limit",
            name_en: "Limit",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 111,
        name_id: "Promo Management",
        name_en: "Promo Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 113,
            name_id: "Promo List",
            name_en: "Promo List",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 112,
            name_id: "Merchant List",
            name_en: "Merchant List",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 114,
            name_id: "Blast Information",
            name_en: "Blast Information",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 114,
        name_id: "Notification Management",
        name_en: "Notification Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 115,
            name_id: "Notification Configuration",
            name_en: "Notification Configuration",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 76,
        name_id: "Consolidation Report",
        name_en: "Consolidation Report",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 78,
            name_id: "Summary",
            name_en: "Summary",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 77,
            name_id: "Transaction Report",
            name_en: "Transaction Report",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 282,
        name_id: "Audit Trail",
        name_en: "Audit Trail",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 285,
            name_id: "Admin",
            name_en: "Admin",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 286,
            name_id: "Middleware",
            name_en: "Middleware",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 95,
        name_id: "Transfer Manager",
        name_en: "Transfer Manager",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 100,
            name_id: "Bank",
            name_en: "Bank",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 101,
            name_id: "Transfer Category",
            name_en: "Transfer Category",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 98,
            name_id: "Transfer Configuration",
            name_en: "Transfer Configuration",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 69,
        name_id: "Dashboard",
        name_en: "Dashboard",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
      },
      {
        id: 84,
        name_id: "Surrounding Configuration",
        name_en: "Surrounding Configuration",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
      },
      {
        id: 276,
        name_id: "Biller Manager",
        name_en: "Biller Manager",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 278,
            name_id: "Biller Category",
            name_en: "Biller Category",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 277,
            name_id: "Biller Configuration",
            name_en: "Biller Configuration",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 120,
        name_id: "Master Data",
        name_en: "Master Data",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 194,
            name_id: "Harmonized Error Code",
            name_en: "Harmonized Error Code",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 190,
            name_id: "Currency",
            name_en: "Currency",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 191,
            name_id: "Channel",
            name_en: "Channel",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 192,
            name_id: "Channel Permission",
            name_en: "Channel Permission",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 193,
            name_id: "Module",
            name_en: "Module",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 367,
            name_id: "Terminal Id",
            name_en: "Terminal Id",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 368,
            name_id: "Product Code",
            name_en: "Product Code",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 79,
        name_id: "Task Manager",
        name_en: "Task Manager",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 80,
            name_id: "Task List",
            name_en: "Task List",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
    ],
  },
  {
    roleId: 2,
    roleName: "Maker",
    workflow: true,
    menu: [
      {
        id: 70,
        name_id: "User Management",
        name_en: "User Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 74,
            name_id: "User Manager",
            name_en: "User Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 72,
            name_id: "Role Manager",
            name_en: "Role Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 73,
            name_id: "Menu Manager",
            name_en: "Menu Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 71,
            name_id: "Unit Manager",
            name_en: "Unit Manager",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 91,
        name_id: "Global Parameter",
        name_en: "Global Parameter",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 92,
            name_id: "General Parameter",
            name_en: "General Parameter ",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 93,
            name_id: "Host Error Mapping",
            name_en: "Host Error Mapping",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 107,
        name_id: "Transaction Management",
        name_en: "Transaction Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 108,
            name_id: "Transaction History",
            name_en: "Transaction History",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 83,
        name_id: "Limit Management",
        name_en: "Limit Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 84,
            name_id: "Limit",
            name_en: "Limit",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 111,
        name_id: "Promo Management",
        name_en: "Promo Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 113,
            name_id: "Promo List",
            name_en: "Promo List",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 112,
            name_id: "Merchant List",
            name_en: "Merchant List",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 114,
            name_id: "Blast Information",
            name_en: "Blast Information",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 114,
        name_id: "Notification Management",
        name_en: "Notification Management",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 115,
            name_id: "Notification Configuration",
            name_en: "Notification Configuration",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 76,
        name_id: "Consolidation Report",
        name_en: "Consolidation Report",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 78,
            name_id: "Summary",
            name_en: "Summary",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 77,
            name_id: "Transaction Report",
            name_en: "Transaction Report",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 282,
        name_id: "Audit Trail",
        name_en: "Audit Trail",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 285,
            name_id: "Admin",
            name_en: "Admin",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 286,
            name_id: "Middleware",
            name_en: "Middleware",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 95,
        name_id: "Transfer Manager",
        name_en: "Transfer Manager",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 100,
            name_id: "Bank",
            name_en: "Bank",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 101,
            name_id: "Transfer Category",
            name_en: "Transfer Category",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 98,
            name_id: "Transfer Configuration",
            name_en: "Transfer Configuration",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 69,
        name_id: "Dashboard",
        name_en: "Dashboard",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
      },
      {
        id: 84,
        name_id: "Surrounding Configuration",
        name_en: "Surrounding Configuration",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
      },
      {
        id: 276,
        name_id: "Biller Manager",
        name_en: "Biller Manager",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 278,
            name_id: "Biller Category",
            name_en: "Biller Category",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 277,
            name_id: "Biller Configuration",
            name_en: "Biller Configuration",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 120,
        name_id: "Master Data",
        name_en: "Master Data",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 194,
            name_id: "Harmonized Error Code",
            name_en: "Harmonized Error Code",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 190,
            name_id: "Currency",
            name_en: "Currency",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 191,
            name_id: "Channel",
            name_en: "Channel",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 192,
            name_id: "Channel Permission",
            name_en: "Channel Permission",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 193,
            name_id: "Module",
            name_en: "Module",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 367,
            name_id: "Terminal Id",
            name_en: "Terminal Id",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
          {
            id: 368,
            name_id: "Product Code",
            name_en: "Product Code",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
      {
        id: 79,
        name_id: "Task Manager",
        name_en: "Task Manager",
        hakAkses: {
          add: true,
          edit: true,
          read: true,
          delete: true,
        },
        menuAccessChild: [
          {
            id: 80,
            name_id: "Task List",
            name_en: "Task List",
            hakAkses: {
              add: true,
              edit: true,
              read: true,
              delete: true,
            },
          },
        ],
      },
    ],
  },
];
