import { useState } from "react";
import { makeStyles, Typography } from "@material-ui/core";

import Title from "components/BN/Title";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Button from "components/BN/Button/GeneralButton";

import { ReactComponent as SvgPlus } from "assets/icons/BN/plus-white.svg";
const RoleManager = () => {
  const classes = useStyle();

  const [dataSearch, setDataSearch] = useState(null);
  return (
    <div>
      <Title label="Role Manager">
        <Search
          options={[
            "Nama Biller",
            "Kode Payee",
            "Kategori",
            "Payment Handler",
            "Sumber Dana",
            "Status",
          ]}
          placeholder="Name, Username, or NIP"
          placement="bottom"
          dataSearch={dataSearch}
          setDataSearch={[setDataSearch]}
          style={{ width: 286, height: 40 }}
        />
      </Title>
      <div className={classes.container}>
        <div className={classes.roleListContainer}>
          <div className={classes.headerRoleList}>
            <Typography className={classes.textTitle}>Role List</Typography>
            <Button
              label="Add Role"
              iconPosition="startIcon"
              buttonIcon={<SvgPlus width={16.67} height={16.67} />}
              style={{
                marginLeft: 30,
                width: 120,
                height: 35,
                fontFamily: "FuturaMdBT",
                fontSize: 12,
                fontWeight: "bold",
              }}
              onClick={() => console.log("hallo")}
            />
          </div>
        </div>
        <div className={classes.menuListContainer}>Menu</div>
      </div>
    </div>
  );
};

export default RoleManager;

const useStyle = makeStyles({
  container: {
    height: "100%",
    width: "100%",
    padding: "30px",
    display: "flex",
    justifyContent: "space-between",
    gap: "20px",
  },
  roleListContainer: {
    flex: 1,
    height: "100%",
    backgroundColor: "#fff",
    borderRadius: "15px",
    padding: "10px",
  },
  menuListContainer: {
    flex: 2,
    height: "100%",
    backgroundColor: "#fff",
    borderRadius: "15px",
    padding: "10px",
  },
  headerRoleList: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignContent: "center",
    alignItems: "center",
  },
  textTitle: {
    lineHeight: "26px",
    fontSize: "20px",
    fontWeight: 600,
  },
});
