import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  CircularProgress,
  makeStyles,
  Button as MUIButton,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import FormField from "components/BN/Form/InputGroupValidation";
import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";

import { setSelectedStepData } from "stores/actions/transactionHistory";

import { pathnameCONFIG } from "configuration";

import { dataTransactionStepHistory } from "../index.dummy";

const useStyle = makeStyles((theme) => ({
  page: {
    backgroundColor: Colors.gray.ultrasoft,
    height: "100%",
  },
  backButton: {
    ...theme.typography.backButton,
    marginLeft: 20,
    marginTop: 10,
  },
  card: {
    width: "100%",
    padding: "30px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    gap: "30px",
  },
  containerDetail: {
    width: "100%",
    backgroundColor: Colors.white,
    padding: "30px",
    borderRadius: "10px",
    display: "flex",
    flexDirection: "row",
  },
  childContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    gap: "50px",
  },
  containerTable: {
    width: "100%",
  },
}));

const columnStyle = {
  width: "auto",
  height: "18px",
  fontFamily: "FuturaBkBT",
  fontWeight: 400,
  fontSize: "13px",
  lineHeight: "18px",
  letterSpacing: "0.01em",
  color: "#374062",
  display: "flex",
  alignItems: "center",
};

const dataHeader = ({ dispatch, history }) => [
  {
    title: "Row ID",
    render: (rowData) => <span style={columnStyle}>{rowData?.rowId}</span>,
  },
  {
    title: "Step",
    render: (rowData) => <span style={columnStyle}>{rowData?.step}</span>,
  },
  {
    title: "Status",
    render: (rowData) => (
      <Badge
        outline
        label={rowData?.status}
        type={
          rowData?.status == "success"
            ? "green"
            : rowData?.status == "error"
            ? "danger"
            : "orange"
        }
      />
    ),
  },
  {
    title: "Start Date",
    render: (rowData) => <span style={columnStyle}>{rowData?.startDate}</span>,
  },
  {
    title: "End Date",
    render: (rowData) => <span style={columnStyle}>{rowData?.endDate}</span>,
  },
  {
    title: "Processing Time",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.processingTime + "ms"}</span>
    ),
  },
  {
    title: "Surrounding",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.surrounding}</span>
    ),
  },
  {
    title: "Err Code",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.errCode || "-"}</span>
    ),
  },
  {
    title: "Err Desc",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.errDesc || "-"}</span>
    ),
  },
  {
    title: "Compensation",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.compensation || "-"}</span>
    ),
  },
  {
    title: "Remark",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.remark || "-"}</span>
    ),
  },
  {
    title: "Additional 1",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.additionalData1 || "-"}</span>
    ),
  },
  {
    title: "Additional 2",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.additionalData2 || "-"}</span>
    ),
  },
  {
    title: "Additional 3",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.additionalData3 || "-"}</span>
    ),
  },
  {
    title: "",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          width: "100%",
          justifyContent: "space-around",
        }}
      >
        <ButtonOutlined
          width={120}
          label="View Detail"
          onClick={() => {
            dispatch(setSelectedStepData(rowData));
            history.push(
              pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_STEP_DETAIL
            );
          }}
        />
      </div>
    ),
    width: 180,
  },
];

const TransactionDetail = () => {
  const classes = useStyle();
  const history = useHistory();
  const dispatch = useDispatch();

  const { data } = useSelector(
    ({ transactionHistory }) => ({
      data: transactionHistory.data,
    }),
    shallowEqual
  );

  const handleClose = () => {
    // history.push(pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_HISTORY);
    history.goBack();
  };

  return (
    <div className={classes.page}>
      <Fragment>
        <MUIButton
          className={classes.backButton}
          startIcon={<ArrowLeft />}
          onClick={handleClose}
        >
          Back
        </MUIButton>
        <Typography variant="h2" style={{ marginLeft: 20 }}>
          Transaction Detail
        </Typography>

        <div className={classes.card}>
          <div className={classes.containerDetail}>
            <div className={classes.childContainer}>
              <FormField
                label={
                  <Typography variant="subtitle1">Transaction ID :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.transactionId || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Channel Trx ID :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.channelTranId || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Error Desc :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.errDesc || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">
                    Transaction Amount :
                  </Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.transactionAmount || "-"}</Typography>
              </FormField>
            </div>

            <div className={classes.childContainer}>
              <FormField
                label={
                  <Typography variant="subtitle1">
                    Transaction Type :
                  </Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.transactionType || "-"}</Typography>
              </FormField>

              <FormField
                label={<Typography variant="subtitle1">Device ID :</Typography>}
                marginTopLabel="0"
              >
                <Typography>{data?.deviceId || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Req Date & Time :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.reqDateTime || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">
                    Transaction Currency :
                  </Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.transactionCurr || "-"}</Typography>
              </FormField>
            </div>

            <div className={classes.childContainer}>
              <FormField
                label={
                  <Typography variant="subtitle1">Request Type :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.requestType || "-"}</Typography>
              </FormField>

              <FormField
                label={<Typography variant="subtitle1">Status :</Typography>}
                marginTopLabel="0"
              >
                <Badge
                  outline
                  label={data?.status}
                  type={
                    data?.status == "Success"
                      ? "green"
                      : data?.status == "Error"
                      ? "danger"
                      : "orange"
                  }
                  styleBadge={{ width: "100px" }}
                />
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Res Date & Time :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.resDateTime || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Source Account :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.accountFromName || "-"}</Typography>
                <Typography>{data?.accountNumber || "-"}</Typography>
              </FormField>
            </div>

            <div className={classes.childContainer}>
              <FormField
                label={
                  <Typography variant="subtitle1">Channel Code :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.channelCode || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Error Code :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.errCode || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Processing Time :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.processingTime + "ms" || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">
                    Destination Account :
                  </Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.accountDestName || "-"}</Typography>
                <Typography>{data?.accountDestNumber || "-"}</Typography>
              </FormField>
            </div>
          </div>

          <div className={classes.containerTable}>
            <TableICBB
              headerContent={dataHeader({
                dispatch,
                history,
              })}
              isLoading={false}
              dataContent={dataTransactionStepHistory ?? []}
              isFullTable={false}
              widthTable="2000px"
            />
          </div>
        </div>
      </Fragment>
    </div>
  );
};

export default TransactionDetail;
