import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Avatar } from "@material-ui/core";
import styled from "styled-components";

import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import Search from "components/BN/Search/SearchWithoutDropdown";
import ButtonGeneral from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";

import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";
import { ReactComponent as GreenCheck } from "assets/icons/BN/check-circle-green.svg";
import { ReactComponent as RedExit } from "assets/icons/BN/x-circle-large.svg";
import { ReactComponent as DownloadIcon } from "assets/icons/BN/download-white.svg";

import useDebounce from "utils/helpers/useDebounce";

import {
  actionBillerManagerGet,
  actionBillerManagerGetByPayeeCode,
  actionBillerManagerGetDenomListByPayeeCode,
  actionBillerManagerGetPrefixByPayeeCode,
  actionBillerManagerGetTemplateListByPayeeCode,
  actionBillerManagerSpecGet,
  actionBillerManagerTransactionCategoriesGet,
  actionBillerManagerTransactionGroupGet,
  actionDeleteBiller,
  clearForm,
  setId,
  setSuccess,
} from "stores/actions/billerManager";

import { pathnameCONFIG } from "../../../../configuration";

import {
  useStyles,
  searchStyle,
  buttonStyled,
  columnStyle,
  redBadgeStyle,
  greenBadgeStyle,
} from "./style";
import { validateTask } from "../../../../stores/actions/validateTaskPortal";
import { validateTaskConstant } from "../../../../stores/actions/validateTaskPortal/constantValidateTask";
import reload from "../../../../assets/icons/BN/reload.png";
import { setTypeIsOpen } from "../../../../stores/actions/internationalBank";
import {
  setSelectedData,
  filterDataFlow,
} from "stores/actions/transactionHistory";

import { dataBillerProduct } from "../../PaymentSetup/index.dummy";
import { dataTransactionHistory } from "./index.dummy";

const Title = styled.div`
  width: 250px;
  height: 24px;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.03em;
  color: #374062;
`;

const dataHeader = ({
  setDeleteConfirmation,
  dispatch,
  history,
  setOpenModalReloadActive,
}) => [
  {
    title: "Transaction ID",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.transactionId}</span>
    ),
    width: 120,
  },
  {
    title: "Channel Code",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.channelCode}</span>
    ),
    width: 150,
  },
  {
    title: "Channel Transaction ID",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.channelTranId}</span>
    ),
    width: 170,
  },
  {
    title: "Device ID",
    render: (rowData) => (
      <span style={columnStyle}>
        {rowData?.billerSpec === null ? "-" : rowData?.deviceId}
      </span>
    ),
    width: 150,
  },
  {
    title: "Transaction Type",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.transactionType}</span>
    ),
    width: 140,
  },
  {
    title: "Request Type",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.requestType}</span>
    ),
    width: 150,
  },
  {
    title: "Status",
    render: (rowData) => (
      <Badge
        outline
        label={rowData?.status}
        type={
          rowData?.status == "Success"
            ? "green"
            : rowData?.status == "Error"
            ? "danger"
            : "orange"
        }
      />
    ),
    width: 120,
  },
  {
    title: "Err Code",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.errCode || "-"}</span>
    ),
    width: 90,
  },
  {
    title: "Err Desc",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.errDesc || "-"}</span>
    ),
    width: 90,
  },
  {
    title: "Req Date & Time",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.reqDateTime}</span>
    ),
    width: 180,
  },
  {
    title: "Res Date & Time",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.resDateTime}</span>
    ),
    width: 180,
  },
  {
    title: "Processing Time",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.processingTime}</span>
    ),
    width: 90,
  },
  {
    title: "Compensation",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.compensation}</span>
    ),
    width: 120,
  },
  {
    title: "Transaction Key",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.transactionKey}</span>
    ),
    width: 120,
  },
  {
    title: "Transaction Option",
    render: (rowData) => (
      <span style={columnStyle}>
        {rowData?.transactionOption ? "Yes" : "No"}
      </span>
    ),
    width: 100,
  },
  {
    title: "Transaction Amount",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.transactionAmount}</span>
    ),
    width: 150,
  },
  {
    title: "Transaction Fee",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.transactionFee}</span>
    ),
    width: 150,
  },
  {
    title: "Transaction Currency",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.transactionCurr}</span>
    ),
    width: 120,
  },
  {
    title: "Source Account",
    render: (rowData) => (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <span style={columnStyle}>{rowData?.accountNumber}</span>
        <span style={columnStyle}>{rowData?.accountFromName}</span>
      </div>
    ),
    width: 150,
  },
  {
    title: "Destinantion Account",
    render: (rowData) => (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <span style={columnStyle}>{rowData?.accountDestNumber}</span>
        <span style={columnStyle}>{rowData?.accountDestName}</span>
      </div>
    ),
    width: 150,
  },
  {
    title: "",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          width: "100%",
          justifyContent: "space-around",
        }}
      >
        <ButtonOutlined
          width={120}
          label="View Detail"
          onClick={() => {
            dispatch(setSelectedData(rowData));
            history.push(
              pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_DETAIL
            );
          }}
        />
        <ButtonOutlined
          width={120}
          label="Flow Detail"
          onClick={async () => {
            await dispatch(filterDataFlow());
            history.push(
              pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_FLOW_DETAIL
            );
          }}
        />
      </div>
    ),
    width: 300,
  },
];
const TransactionHistory = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [openModalReloadActive, setOpenModalReloadActive] = useState(false);

  const [dataSearch, setDataSearch] = useState("");
  const [page, setPage] = useState(1);
  const [deleteConfirmation, setDeleteConfirmation] = useState(false);
  const [refetching, setRefetching] = useState(false);

  const searchDebounce = useDebounce(dataSearch, 1000);

  useEffect(() => {
    const payload = {
      pageNumber: page - 1,
      limit: 10,
      billerName: searchDebounce,
      billerCategory: searchDebounce,
    };
    // billerFetchHandler(payload);
  }, [page, refetching, searchDebounce]);

  useEffect(() => {
    setPage(1);
  }, [searchDebounce]);

  return (
    <div className={classes.parentContainer}>
      <div className={classes.headerContainer}>
        <Title>Transaction History</Title>
        <div className={classes.filterContainer}>
          <Search
            dataSearch={dataSearch}
            setDataSearch={setDataSearch}
            style={searchStyle}
            placeholder="Biller's Name, Payee Code"
          />
          <ButtonGeneral
            label="Download"
            iconPosition="startIcon"
            buttonIcon={<DownloadIcon />}
            style={buttonStyled}
            onClick={() => {
              dispatch(clearForm());
              history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER_ADD);
            }}
          />
        </div>
      </div>
      <TableICBB
        headerContent={dataHeader({
          setDeleteConfirmation,
          setOpenModalReloadActive,
          dispatch,
          history,
        })}
        isLoading={false}
        dataContent={dataTransactionHistory ?? []}
        page={page}
        setPage={setPage}
        totalElement={dataBillerProduct?.totalElements || 0}
        totalData={dataBillerProduct?.totalPages || 0}
        isFullTable={false}
        widthTable="3000px"
      />
      {/* <DeletePopup
        isOpen={deleteConfirmation}
        onContinue={handleDelete}
        handleClose={() => setDeleteConfirmation(false)}
        loading={isLoadingExcute}
      /> */}
      {/* <DeletePopup
        title="Confirmation"
        message="Are You Sure Active the Data?"
        isOpen={openModalReloadActive}
        loading={isLoading}
        handleClose={() => {
          setOpenModalReloadActive(false);
        }}
        // onContinue={onReloadItem}
      /> */}
      {/* <SuccessConfirmation
        isOpen={isSuccess}
        message="Deleted Successfully"
        handleClose={() => {
          dispatch(setSuccess(false));
          setRefetching((prev) => !prev);
        }}
      /> */}
    </div>
  );
};

TransactionHistory.propTypes = {};

export default TransactionHistory;
