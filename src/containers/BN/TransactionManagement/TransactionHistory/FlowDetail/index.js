import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  CircularProgress,
  makeStyles,
  Button as MUIButton,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";

import { RESET_TRANSACTION_HISTORY } from "stores/actions/transactionHistory";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import TableICBB from "components/BN/TableIcBB";
import FlowComponent from "./FlowComponent";

import { dataFlowDetailTransaction } from "../index.dummy";

const columnStyle = {
  width: "auto",
  height: "18px",
  fontFamily: "FuturaBkBT",
  fontWeight: 400,
  fontSize: "13px",
  lineHeight: "18px",
  letterSpacing: "0.01em",
  color: "#374062",
  display: "flex",
  alignItems: "center",
};

const dataHeader = ({ dispatch, history }) => [
  {
    title: "No",
    render: (rowData) => <span style={columnStyle}>{rowData?.no}</span>,
  },
  {
    title: "Time Stamp",
    render: (rowData) => <span style={columnStyle}>{rowData?.timeStamp}</span>,
  },
  {
    title: "From",
    render: (rowData) => <span style={columnStyle}>{rowData?.from}</span>,
  },
  {
    title: "To",
    render: (rowData) => <span style={columnStyle}>{rowData?.to}</span>,
  },
  {
    title: "Activity",
    render: (rowData) => <span style={columnStyle}>{rowData?.activity}</span>,
  },
];

const FlowDetail = () => {
  const classes = useStyle();
  const history = useHistory();
  const dispatch = useDispatch();

  const { data } = useSelector(
    ({ transactionHistory }) => ({
      data: transactionHistory.data,
    }),
    shallowEqual
  );

  const handleClose = () => {
    // history.push(pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_HISTORY);
    dispatch({
      type: RESET_TRANSACTION_HISTORY,
    });
    history.goBack();
  };

  return (
    <div className={classes.page}>
      <Fragment>
        <MUIButton
          className={classes.backButton}
          startIcon={<ArrowLeft />}
          onClick={handleClose}
        >
          Back
        </MUIButton>
        <Typography variant="h2" style={{ marginLeft: 20 }}>
          Flow Detail
        </Typography>

        <div className={classes.card}>
          <div className={classes.containerDetail}>
            <div className={classes.flowContainer}>
              <FlowComponent />
            </div>
            <div className={classes.containerTable}>
              <TableICBB
                headerContent={dataHeader({
                  dispatch,
                  history,
                })}
                isLoading={false}
                dataContent={dataFlowDetailTransaction ?? []}
                isFullTable={false}
              />
            </div>
          </div>
        </div>
      </Fragment>
    </div>
  );
};

export default FlowDetail;

const useStyle = makeStyles((theme) => ({
  page: {
    backgroundColor: Colors.gray.ultrasoft,
    height: "100%",
  },
  backButton: {
    ...theme.typography.backButton,
    marginLeft: 20,
    marginTop: 10,
  },
  card: {
    width: "100%",
    padding: "30px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    gap: "30px",
  },
  containerDetail: {
    width: "100%",
    backgroundColor: Colors.white,
    padding: "30px",
    borderRadius: "10px",
    display: "flex",
    flexDirection: "column",
    gap: "15px",
  },
  childContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    gap: "50px",
  },
  containerTable: {
    width: "100%",
  },
  flowContainer: {
    width: "100%",
    height: "500px",
    border: "1px solid #E3E8EF",
    borderRadius: "10px",
  },
}));
