import { memo } from "react";
import { Handle, Position, NodeResizer } from "reactflow";
import { useSelector, shallowEqual } from "react-redux";
import ChannelImage from "assets/images/BN/ChannelImage.png";

function SurroundingNode({ data }) {
  const { dataSurrounding } = useSelector(
    ({ transactionHistory }) => ({
      dataSurrounding: transactionHistory.dataSurrounding,
    }),
    shallowEqual
  );

  return (
    <>
      <div
        style={{
          width: "75px",
          height: "75px",
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <img
          style={{ backgroundSize: "contain", width: "100%", height: "100%" }}
          src={ChannelImage}
        />
        <div>{data.label}</div>
        <div
          style={{
            display: "flex",
            position: "absolute",
            height: "100%",
            justifyContent: "space-evenly",
            flexDirection: "column",
            left: -5,
            top: 0,
          }}
        >
          {dataSurrounding.length > 0 &&
            dataSurrounding.map((item, index) => (
              <Handle
                key={index}
                style={{
                  position: "relative",
                  top: 0,
                  left: 0,
                  transform: "none",
                }}
                id={item.no.toString()}
                type={item.from == "middleware" ? "target" : "source"}
                position={Position.Left}
              />
            ))}
        </div>
      </div>
    </>
  );
}

export default memo(SurroundingNode);
