import { memo } from "react";
import { Handle, Position, NodeResizer } from "reactflow";
import { useSelector, shallowEqual } from "react-redux";
import backgroundImage from "assets/images/BN/Server.png";

function MiddlewareNode({ data }) {
  const { dataMdwLeft, dataMdwRight } = useSelector(
    ({ transactionHistory }) => ({
      dataMdwLeft: transactionHistory.dataMdwLeft,
      dataMdwRight: transactionHistory.dataMdwRight,
    }),
    shallowEqual
  );
  return (
    <>
      <div
        style={{
          width: "90px",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <img
          style={{ backgroundSize: "contain", width: "100%", height: "100%" }}
          src={backgroundImage}
        />
        <div>{data.label}</div>
        <div
          style={{
            display: "flex",
            position: "absolute",
            height: "100%",
            justifyContent: "space-evenly",
            flexDirection: "column",
            right: -5,
            top: -10,
          }}
        >
          {dataMdwRight.length > 0 &&
            dataMdwRight.map((item, index) => (
              <Handle
                key={index}
                style={{
                  position: "relative",
                  top: 0,
                  right: 0,
                  transform: "none",
                }}
                id={item.no.toString()}
                type={item.from == "middleware" ? "source" : "target"}
                position={Position.Right}
              />
            ))}
        </div>
        <div
          style={{
            display: "flex",
            position: "absolute",
            height: "100%",
            justifyContent: "space-evenly",
            flexDirection: "column",
            left: -5,
            top: 0,
          }}
        >
          {dataMdwLeft.length > 0 &&
            dataMdwLeft.map((item, index) => (
              <Handle
                key={index}
                style={{
                  position: "relative",
                  top: 0,
                  left: 0,
                  transform: "none",
                }}
                id={item.no.toString()}
                type={item.to == "middleware" ? "target" : "source"}
                position={Position.Left}
              />
            ))}
        </div>
      </div>
    </>
  );
}

export default memo(MiddlewareNode);
