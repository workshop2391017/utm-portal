import { memo } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { Handle, Position, NodeResizer } from "reactflow";
import ChannelImage from "assets/images/BN/ChannelImage.png";

function ChannelNode({ data }) {
  const { dataChannel } = useSelector(
    ({ transactionHistory }) => ({
      dataChannel: transactionHistory.dataChannel,
    }),
    shallowEqual
  );
  return (
    <>
      <div
        style={{
          width: "75px",
          height: "75px",
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <img
          style={{ backgroundSize: "contain", width: "100%", height: "100%" }}
          src={ChannelImage}
        />
        <div>{data.label}</div>
        <div
          style={{
            display: "flex",
            position: "absolute",
            height: "100%",
            justifyContent: "space-evenly",
            flexDirection: "column",
            right: 0,
            top: 0,
          }}
        >
          {dataChannel.length > 0 &&
            dataChannel.map((item, index) => (
              <Handle
                key={index}
                style={{ position: "relative", top: 0, transform: "none" }}
                id={item.no.toString()}
                type={item.from == "channel" ? "source" : "target"}
                position={Position.Right}
              />
            ))}
        </div>
      </div>
    </>
  );
}

export default memo(ChannelNode);
