import React from "react";
import { MarkerType } from "reactflow";

export const nodes = [
  {
    id: "channel",
    type: "channel",
    data: {
      label: "Channel",
    },
    position: { x: 0, y: 0 },
  },
  {
    id: "middleware",
    type: "middleware",
    data: {
      label: "Middleware",
    },
    position: { x: 250, y: 0 },
  },
  {
    id: "core",
    type: "core",
    data: {
      label: "Core",
    },
    position: { x: 500, y: -100 },
  },
  {
    id: "surrounding",
    type: "surrounding",
    data: {
      label: "Surrounding",
    },
    position: { x: 500, y: 50 },
  },
  {
    id: "surrounding2",
    type: "surrounding",
    data: {
      label: "Surrounding2",
    },
    position: { x: 500, y: 190 },
  },
];

export const edges = [
  {
    id: "e1-1",
    source: "channel",
    target: "middleware",
    label: "1",
    sourceHandle: "1",
    targetHandle: "1",
    type: "custom",
    data: {
      label: "1",
    },
    animated: true,
  },
  {
    id: "e1-2",
    source: "middleware",
    target: "channel",
    label: "4",
    sourceHandle: "2",
    targetHandle: "2",
    type: "custom",
    data: {
      label: "4",
    },
    animated: true,
  },
  {
    id: "e1-3",
    source: "channel",
    target: "middleware",
    label: "5",
    sourceHandle: "3",
    targetHandle: "3",
    type: "custom",
    data: {
      label: "5",
    },
    animated: true,
  },
  {
    id: "e1-4",
    source: "middleware",
    target: "channel",
    label: "10",
    sourceHandle: "4",
    targetHandle: "4",
    type: "custom",
    data: {
      label: "10",
    },
    animated: true,
  },
  {
    id: "e2-1",
    source: "middleware",
    target: "core",
    label: "2",
    sourceHandle: "1",
    targetHandle: "1",
    type: "custom",
    data: {
      label: "2",
    },
    animated: true,
  },
  {
    id: "e2-2",
    source: "core",
    target: "middleware",
    label: "3",
    sourceHandle: "2",
    targetHandle: "2",
    type: "custom",
    data: {
      label: "3",
    },
    animated: true,
  },
  {
    id: "e2-3",
    source: "middleware",
    target: "core",
    label: "6",
    sourceHandle: "3",
    targetHandle: "3",
    type: "custom",
    data: {
      label: "6",
    },
    animated: true,
  },
  {
    id: "e2-4",
    source: "core",
    target: "middleware",
    label: "7",
    sourceHandle: "4",
    targetHandle: "4",
    type: "custom",
    data: {
      label: "7",
    },
    animated: true,
  },
  {
    id: "e2-5",
    source: "middleware",
    target: "core",
    label: "8",
    sourceHandle: "5",
    targetHandle: "5",
    type: "custom",
    data: {
      label: "8",
    },
    animated: true,
  },
  {
    id: "e2-6",
    source: "core",
    target: "middleware",
    label: "9",
    sourceHandle: "6",
    targetHandle: "6",
    type: "custom",
    data: {
      label: "9",
    },
    animated: true,
  },
  {
    id: "e3-1",
    source: "middleware",
    target: "surrounding",
    label: "11",
    sourceHandle: "7",
    targetHandle: "1",
    type: "custom",
    data: {
      label: "11",
    },
    animated: true,
  },
  {
    id: "e3-2",
    source: "surrounding",
    target: "middleware",
    label: "12",
    sourceHandle: "2",
    targetHandle: "8",
    type: "custom",
    data: {
      label: "12",
    },
    animated: true,
  },
];
