import React, { useCallback } from "react";
import ReactFlow, {
  addEdge,
  MiniMap,
  Controls,
  Background,
  useNodesState,
  useEdgesState,
} from "reactflow";

import { useSelector, shallowEqual } from "react-redux";

import { nodes as initialNodes, edges as initialEdges } from "./initialElement";
import ChannelNode from "./ChannelNode";
import MiddlewareNode from "./MiddlewareNode";
import CoreNode from "./CoreNode";
import SurroundingNode from "./SurroundingNode";
import CustomEdge from "./CustomEdge";

import "reactflow/dist/style.css";
import "./overview.css";

const nodeTypes = {
  channel: ChannelNode,
  middleware: MiddlewareNode,
  core: CoreNode,
  surrounding: SurroundingNode,
};

const edgeTypes = {
  custom: CustomEdge,
};

const nodeClassName = (node) => node.type;

const Flow = () => {
  const { dataEdges, dataNodes } = useSelector(
    ({ transactionHistory }) => ({
      dataEdges: transactionHistory.dataEdges,
      dataNodes: transactionHistory.dataNodes,
    }),
    shallowEqual
  );

  const [nodes, setNodes, onNodesChange] = useNodesState(dataNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(dataEdges);
  const onConnect = useCallback(
    (params) => setEdges((eds) => addEdge(params, eds)),
    []
  );

  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      fitView
      attributionPosition="top-right"
      nodeTypes={nodeTypes}
      edgeTypes={edgeTypes}
      panOnDrag={false}
      selectionOnDrag={false}
      zoomOnScroll={false}
      zoomOnPinch={false}
      zoomOnDoubleClick={false}
      nodesDraggable={false}
    />
  );
};

export default Flow;
