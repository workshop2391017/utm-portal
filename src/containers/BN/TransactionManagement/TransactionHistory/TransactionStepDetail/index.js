import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  CircularProgress,
  makeStyles,
  Button as MUIButton,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import FormField from "components/BN/Form/InputGroupValidation";
import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import PayloadPopup from "components/BN/Popups/PayloadPopup";

import { pathnameCONFIG } from "configuration";

import {
  dataTransactionStepHistory,
  dataDetailStepTransaction,
} from "../index.dummy";

const useStyle = makeStyles((theme) => ({
  page: {
    backgroundColor: Colors.gray.ultrasoft,
    height: "100%",
  },
  backButton: {
    ...theme.typography.backButton,
    marginLeft: 20,
    marginTop: 10,
  },
  card: {
    width: "100%",
    padding: "30px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    gap: "30px",
  },
  containerDetail: {
    width: "100%",
    backgroundColor: Colors.white,
    padding: "30px",
    borderRadius: "10px",
    display: "flex",
    flexDirection: "row",
  },
  childContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    gap: "50px",
  },
  containerTable: {
    width: "100%",
  },
}));

const columnStyle = {
  width: "auto",
  height: "18px",
  fontFamily: "FuturaBkBT",
  fontWeight: 400,
  fontSize: "13px",
  lineHeight: "18px",
  letterSpacing: "0.01em",
  color: "#374062",
  display: "flex",
  alignItems: "center",
};

const dataHeader = ({
  dispatch,
  history,
  setShowPopup,
  setValuePayload,
  setTitle,
}) => [
  {
    title: "Row ID",
    render: (rowData) => <span style={columnStyle}>{rowData?.rowId}</span>,
  },
  {
    title: "Log Date & Time",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.logDateTime}</span>
    ),
  },
  {
    title: "Activity Name",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.activityName}</span>
    ),
  },
  {
    title: "Description",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.description}</span>
    ),
  },
  {
    title: "Log Data",
    render: (rowData) => (
      <ButtonOutlined
        width={150}
        label="View Log Data"
        onClick={() => {
          setValuePayload(rowData?.logData);
          setTitle("Log Data");
          setShowPopup(true);
        }}
      />
    ),
    width: 150,
  },
  {
    title: "Payload",
    render: (rowData) => (
      <ButtonOutlined
        width={150}
        label="View Payload"
        onClick={() => {
          setValuePayload(rowData?.payload);
          setTitle("Payload");
          setShowPopup(true);
        }}
      />
    ),
    width: 150,
  },
];

const TransactionStepDetail = () => {
  const classes = useStyle();
  const history = useHistory();
  const dispatch = useDispatch();

  const [showPopup, setShowPopup] = useState(false);
  const [valuePayload, setValuePayload] = useState("");
  const [title, setTitle] = useState("");

  const { data, dataStepDetail } = useSelector(
    ({ transactionHistory }) => ({
      data: transactionHistory.data,
      dataStepDetail: transactionHistory.dataStepDetail,
    }),
    shallowEqual
  );

  const handleClose = () => {
    // history.push(pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_HISTORY);
    history.goBack();
  };

  return (
    <div className={classes.page}>
      <Fragment>
        <MUIButton
          className={classes.backButton}
          startIcon={<ArrowLeft />}
          onClick={handleClose}
        >
          Back
        </MUIButton>
        <Typography variant="h2" style={{ marginLeft: 20 }}>
          Transaction Step Detail
        </Typography>

        <div className={classes.card}>
          <div className={classes.containerDetail}>
            <div className={classes.childContainer}>
              <FormField
                label={
                  <Typography variant="subtitle1">Transaction ID :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.transactionId || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Error Code :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.errCode || "-"}</Typography>
              </FormField>
            </div>

            <div className={classes.childContainer}>
              <FormField
                label={
                  <Typography variant="subtitle1">
                    Transaction Type :
                  </Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.transactionType || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Error Desc :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.errDesc || "-"}</Typography>
              </FormField>
            </div>

            <div className={classes.childContainer}>
              <FormField
                label={<Typography variant="subtitle1">Step Name :</Typography>}
                marginTopLabel="0"
              >
                <Typography>{dataStepDetail?.step || "-"}</Typography>
              </FormField>

              <FormField
                label={
                  <Typography variant="subtitle1">Source Account :</Typography>
                }
                marginTopLabel="0"
              >
                <Typography>{data?.accountFromName || "-"}</Typography>
                <Typography>{data?.accountNumber || "-"}</Typography>
              </FormField>
            </div>

            <div className={classes.childContainer}>
              <FormField
                label={<Typography variant="subtitle1">Status :</Typography>}
                marginTopLabel="0"
              >
                <Badge
                  outline
                  label={data?.status}
                  type={
                    data?.status == "Success"
                      ? "green"
                      : data?.status == "Error"
                      ? "danger"
                      : "orange"
                  }
                  styleBadge={{ width: "100px" }}
                />
              </FormField>
            </div>
          </div>

          <div className={classes.containerTable}>
            <TableICBB
              headerContent={dataHeader({
                setShowPopup,
                setValuePayload,
                setTitle,
                dispatch,
                history,
              })}
              isLoading={false}
              dataContent={dataDetailStepTransaction ?? []}
              isFullTable={false}
            />
          </div>
        </div>
        <PayloadPopup
          isOpen={showPopup}
          handleClose={() => setShowPopup(false)}
          value={valuePayload}
          title={title}
        />
      </Fragment>
    </div>
  );
};

export default TransactionStepDetail;
