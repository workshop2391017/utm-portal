import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  CircularProgress,
  makeStyles,
  Button as MUIButton,
  Typography,
} from "@material-ui/core";

import Colors from "helpers/colors";

import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left.svg";
import AntdTextField from "components/BN/TextField/AntdTextField";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import FormField from "components/BN/Form/InputGroupValidation";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import ToggleSwitchBlue from "components/BN/Switch/ToggleSwitchBlue";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import DeleteConfirmation from "components/BN/Popups/Delete";

import {
  actionBillerCategoryEdit,
  setSuccess,
} from "stores/actions/billerCategory";

import { pathnameCONFIG } from "configuration";

const useStyle = makeStyles((theme) => ({
  page: {
    backgroundColor: Colors.gray.ultrasoft,
    height: "100%",
    position: "relative",
  },
  backButton: {
    ...theme.typography.backButton,
    marginLeft: 20,
    marginTop: 10,
  },
  card: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    backgroundColor: "white",
    padding: "30px 40px",
    width: 630,
    height: 380,
    borderRadius: 20,
    "& .form-title": {
      fontFamily: "FuturaHvBT",
      fontSize: 17,
      letterSpacing: "0.01em",
      color: Colors.dark.hard,
    },
  },
  loading: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    width: "630px",
    height: "411px",
    borderRadius: "20px",
    backgroundColor: "white",

    "& span": {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
    },
  },
  formWrapper: {
    display: "flex",
    marginTop: 20,
    "& .field": {
      flex: 1,
      "& .field:nth-child(1)": {
        marginRight: 10,
      },
      "& .field:nth-child(2)": {
        marginLeft: 10,
      },
    },
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-between",
    padding: "22px 40px",
    backgroundColor: "white",
    bottom: 0,
    position: "fixed",
    left: 200,
    right: 0,
  },
}));

const InsertDataParameterSetting = () => {
  const classes = useStyle();
  const history = useHistory();
  const dispatch = useDispatch();

  const { isSuccess, isLoading } = useSelector(
    ({ billerCategory }) => billerCategory
  );

  const [formData, setFormData] = useState({
    id: null,
    code: "",
    description: "",
    nameEn: "",
    nameId: "",
    noOrder: 0,
    serviceCategory: "",
    tranCode: "",
    usePrefix: false,
  });
  const [loading, setLoading] = useState(false);
  const [categoryList, setCategoryList] = useState([]);
  const [openConfirmation, setOpenConfirmation] = useState(false);

  useEffect(() => {
    setFormData({
      id: history.location.state?.data?.id,
      code: history.location.state?.data?.code,
      description: history.location.state?.data?.description,
      nameEn: history.location.state?.data?.nameEn,
      nameId: history.location.state?.data?.nameId,
      noOrder: history.location.state?.data?.noOrder,
      serviceCategory: history.location.state?.data?.serviceCategory,
      tranCode: history.location.state?.data?.tranCode,
      usePrefix: history.location.state?.data?.isPrefix,
    });

    const categories = history.location.state?.categories?.map((category) => ({
      label: category.name,
      value: category.code,
    }));
    setCategoryList(categories);
  }, [history]);

  const handleClose = () => {
    history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI);
  };

  const continueHandler = () => {
    const payload = {
      id: formData.id,
      code: formData.code,
      deleted: false,
      description: formData.description,
      nameEn: formData.nameEn,
      nameId: formData.nameId,
      noOrder: +formData.noOrder,
      serviceCategory: formData.serviceCategory,
      tranCode: formData.tranCode,
      usePrefix: formData.usePrefix,
    };

    dispatch(actionBillerCategoryEdit(payload, setOpenConfirmation));
  };

  return (
    <div className={classes.page}>
      <Fragment>
        <MUIButton
          className={classes.backButton}
          startIcon={<ArrowLeft />}
          onClick={handleClose}
        >
          Back
        </MUIButton>
        <Typography variant="h2" style={{ marginLeft: 20 }}>
          Edit Biller Category
        </Typography>

        <div className={classes.card}>
          <Fragment>
            <h6 className="form-title">Category Edit Biller Form</h6>
            <Typography variant="subtitle1" style={{ marginBottom: 20 }}>
              Please fill in the data below to add Biller Category
            </Typography>

            <div className={classes.formWrapper}>
              <div className="field">
                <FormField
                  label={
                    <Typography variant="subtitle1">
                      Transaction Code :
                    </Typography>
                  }
                  marginTopLabel="0"
                >
                  <AntdTextField
                    style={{ width: "100%" }}
                    value={formData.tranCode}
                    onChange={(event) =>
                      setFormData((prevData) => {
                        const object = { ...prevData };
                        object.tranCode = event.target.value;
                        return object;
                      })
                    }
                    placeholder="Transaction Code"
                  />
                </FormField>
              </div>
            </div>

            <div className={classes.formWrapper}>
              <div className="field">
                <FormField
                  label={
                    <Typography variant="subtitle1">Description :</Typography>
                  }
                  marginTopLabel="0"
                >
                  <AntdTextField
                    style={{ width: "100%" }}
                    type="textArea"
                    textAreaRows={3}
                    value={formData.description}
                    onChange={(event) =>
                      setFormData((prevData) => {
                        const object = { ...prevData };
                        object.description = event.target.value;
                        return object;
                      })
                    }
                    placeholder="Add Description"
                  />
                </FormField>
              </div>
            </div>
          </Fragment>
        </div>
      </Fragment>

      <div className={classes.buttonGroup}>
        <ButtonOutlined label="Cancel" onClick={handleClose} />
        <GeneralButton
          label="Save"
          onClick={() => {
            setOpenConfirmation(true);
          }}
        />
      </div>

      <DeleteConfirmation
        loading={isLoading}
        isOpen={openConfirmation}
        message="Are You Sure To Edit Your Data?"
        submessage="You cannot undo this action"
        onContinue={continueHandler}
        handleClose={() => {
          setOpenConfirmation(false);
        }}
      />
      <SuccessConfirmation
        isOpen={isSuccess}
        handleClose={() => {
          dispatch(setSuccess(false));
          history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI);
        }}
      />
    </div>
  );
};

export default InsertDataParameterSetting;
