import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import {
  makeStyles,
  Typography,
  CircularProgress,
  Avatar,
} from "@material-ui/core";

import { ReactComponent as ArrowUp } from "assets/icons/BN/arrow-up.svg";
import NoData from "assets/images/BN/illustrationred.png";
import reload from "assets/icons/BN/reload.png";
import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";

import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import Search from "components/BN/Search/SearchWithoutDropdown";
import Filter from "components/BN/Filter/GeneralFilter";
import ConfirmationPopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import ButtonGeneral from "components/BN/Button/GeneralButton";

import Colors from "helpers/colors";
import useDebounce from "utils/helpers/useDebounce";

import { pathnameCONFIG } from "configuration";

import {
  actionBillerCategoryDelete,
  actionBillerCategoryGet,
  actionBillerCategoryGetCategories,
  setSuccess,
} from "stores/actions/billerCategory";

import { clearForm } from "stores/actions/billerManager";
import { dataBillerCategory } from "../index.dummy";

const buttonStyle = {
  width: "200px",
  height: "44px",
  padding: "10px 20px",
  gap: "4px",
  backgroundColor: "#0061A7",
  borderRadius: "10px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const useStyles = makeStyles((theme) => ({
  page: {
    backgroundColor: Colors.gray.ultrasoft,
    height: "100%",
    padding: "20px",
  },
  card: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "16px",
    height: "44px",
  },
  generalMenu: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  no: {
    minWidth: "20%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  chevron: {
    border: "none",
    backgroundColor: "transparent",
    cursor: "pointer",
    borderTopLeftRadius: "6px",
    borderTopRightRadius: "6px",
    transition: "all ease-out forwards",
    transitionDuration: "500ms",
    "&:hover": {
      backgroundColor: "gray",
      opacity: 0.5,
    },
    "&:active": {
      backgroundColor: "gray",
      opacity: 0.8,
    },
    "&:disabled": {
      cursor: "not-allowed",
      backgroundColor: "transparent",
      "& svg path": {
        stroke: "gray",
      },
    },
  },
  loading: {
    position: "relative",
    height: "500px",
    borderRadius: "8px",
    backgroundColor: "white",

    "& span": {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
    },
  },
  noData: {
    position: "relative",
    height: "500px",
    borderRadius: "8px",
    backgroundColor: "white",

    "& div": {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      gap: "20px",

      "& img": {
        width: "160px",
        height: "160px",
      },
      "& span": {
        ...theme.typography.noDataMessage,
      },
    },
  },
}));

const dataHeader = ({
  classes,
  history,
  tableData,
  categoryList,
  setTableData,
  setChangeOrderFrequency,
  setOpenConfirmation,
  setOpenModalReloadActive,
}) => [
  {
    title: "Category Name",
    render: (rowData) => (
      <Typography variant="subtitle1">{rowData.code ?? "-"}</Typography>
    ),
    width: "10%",
  },
  {
    title: "Description",
    render: (rowData) => (
      <Typography variant="subtitle1">{rowData.code ?? "-"}</Typography>
    ),
    width: "24%",
  },
  {
    width: "20%",
    title: "Status",
    render: (rowData) => (
      <div className={classes.generalMenu}>
        <Badge
          outline
          label={!rowData.status ? "Active" : "Inactive"}
          type={!rowData.status ? "green" : "red"}
        />
        {!rowData.status ? (
          <GeneralMenu
            rowData={rowData}
            options={["edit", "inactive"]}
            onDelete={() => {
              const payload = {
                id: rowData.id,
                code: rowData.code,
                deleted: true,
                description: rowData.description,
                nameEn: rowData.nameEn,
                nameId: rowData.nameId,
                noOrder: rowData.noOrder,
                serviceCategory: rowData.serviceCategory,
                tranCode: rowData.tranCode,
                usePrefix: rowData.usePrefix,
              };

              setOpenConfirmation({ open: true, payload });
            }}
            onEdit={() =>
              history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI_EDIT, {
                data: rowData,
                categories: categoryList,
              })
            }
          />
        ) : (
          <Avatar
            onClick={() =>
              // setOneItem(rowData);
              setOpenModalReloadActive(true)
            }
            src={reload}
            style={{
              width: "21px",
              height: "21px",
              display: "flex",
              justifyContent: "center",
              cursor: "pointer",
            }}
          />
        )}
      </div>
    ),
  },
];

const BillerManager = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { data, categories, isSuccess, isLoading } = useSelector(
    (e) => e.billerCategory
  );

  const [filter, setFilter] = useState(null);
  const [filterCodePayload, setFilterCodePayload] = useState(null);
  const [categoryList, setCategoryList] = useState([]);
  const [search, setSearch] = useState("");
  const [tableData, setTableData] = useState([]);
  const [changeOrderFrequency, setChangeOrderFrequency] = useState(0);
  const [loading, setLoading] = useState(false);
  const [openModalReloadActive, setOpenModalReloadActive] = useState(false);
  const [openConfirmation, setOpenConfirmation] = useState({
    open: false,
    payload: null,
  });

  const [page, setPage] = useState(1);

  const searchDebounced = useDebounce(search, 1000);

  const deleteHandler = () => {
    dispatch(
      actionBillerCategoryDelete(openConfirmation.payload, setOpenConfirmation)
    );
  };

  useEffect(() => {
    if (filter) {
      const code = categoryList?.find(
        (category) => category.name === filter?.dropdown?.dropdownundefined
      )?.code;
      setFilterCodePayload(code);
    }
  }, [filter]);

  const loadingHandler = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  useEffect(() => {
    if (filterCodePayload) {
      const payload = {
        additionalSearch: searchDebounced,
        serviceCategory: filterCodePayload,
      };

      dispatch(actionBillerCategoryGet(payload, setTableData));
    }
  }, [searchDebounced, filter, filterCodePayload]);

  useEffect(() => {
    // dispatch(actionBillerCategoryGetCategories());
    const payload = {
      additionalSearch: "",
      serviceCategory: "2",
    };
    dispatch(actionBillerCategoryGet(payload, setTableData));
  }, []);

  useEffect(() => {
    if (categories.length !== 0) {
      setCategoryList(categories);
    }
  }, [categories]);

  useEffect(() => {
    // if (data?.length !== 0) {
    //   console.log("data respone biller : ", data);
    //   setTableData(data);
    // }
    setTableData(dataBillerCategory.billerCategoryList);
  }, [data]);

  useEffect(() => {
    const array = [...tableData].sort((a, b) => {
      const noOrderA = a.noOrder;
      const noOrderB = b.noOrder;

      if (noOrderA < noOrderB) {
        return -1;
      }
      if (noOrderA > noOrderB) {
        return 1;
      }
      return 0;
    });
    setTableData(array);
  }, [changeOrderFrequency]);

  const options = [
    {
      type: "dropdown",
      placeholder: "Choose Category",
      options: categoryList?.map(({ name }) => name),
    },
  ];

  return (
    <div className={classes.page}>
      <div className={classes.card}>
        <Typography variant="h2">Billers Category</Typography>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <Search
            dataSearch={search}
            setDataSearch={setSearch}
            placeholder="Code, Category Name"
            placement="bottom"
          />
          <ButtonGeneral
            label="Add Category"
            iconPosition="startIcon"
            buttonIcon={<Plus />}
            style={buttonStyle}
            onClick={() => {
              dispatch(clearForm());
              history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI_EDIT);
            }}
          />
        </div>
      </div>

      {/* <Filter
        isDropdown
        title="Show :"
        dataFilter={filter}
        setDataFilter={(event) => {
          setFilter(event);
          loadingHandler();
        }}
        options={options}
        style={{
          marginBottom: "20px",
          boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
        }}
      /> */}

      {tableData.length > 0 ? (
        <TableICBB
          headerContent={dataHeader({
            classes,
            history,
            tableData,
            categoryList,
            setTableData,
            setChangeOrderFrequency,
            setOpenConfirmation,
            setOpenModalReloadActive,
          })}
          dataContent={tableData}
          isLoading={isLoading}
          page={page}
          setPage={setPage}
          totalElement={10}
          totalData={20}
        />
      ) : (
        <div className={classes.noData}>
          <div>
            <img src={NoData} alt="no data" />
            <span>No Data</span>
          </div>
        </div>
      )}
      <ConfirmationPopup
        title="Confirmation"
        message="Are You Sure Active the Data?"
        isOpen={openModalReloadActive}
        loading={isLoading}
        handleClose={() => {
          setOpenModalReloadActive(false);
        }}
        // onContinue={onReloadItem}
      />
      <ConfirmationPopup
        loading={isLoading}
        isOpen={openConfirmation.open}
        onContinue={deleteHandler}
        handleClose={() => setOpenConfirmation({ open: false, id: null })}
        message="
        Are You Sure to Inactive the Data?"
        submessage="You cannot undo this action"
      />
      <SuccessConfirmation
        isOpen={isSuccess}
        handleClose={() => dispatch(setSuccess(false))}
        message="Saved Successfully"
      />
    </div>
  );
};

export default BillerManager;
