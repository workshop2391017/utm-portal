/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";
import { Form, Formik } from "formik";
import * as Yup from "yup";

import { Button, Grid, styled, Typography } from "@material-ui/core";

import GeneralTabs from "components/BN/Tabs/GeneralTabs";
import TableScroll from "components/BN/Table/TabbleScrollblue";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import Input from "components/BN/TextField/AntdTextField";
import FormField from "components/BN/Form/InputGroupValidation";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import GeneralButton from "components/BN/Button/GeneralButton";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralSwitch from "components/BN/Switch/ToggleSwitchBlue";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import RadioGroup from "components/BN/Radio/RadioGroup";

import { ReactComponent as AddBlue } from "assets/icons/BN/addBlue.svg";

import {
  actionBillerManagerCategoriesGet,
  actionBillerManagerGetByPayeeCode,
  actionBillerManagerGetDenomListByPayeeCode,
  actionBillerManagerGetPrefixByPayeeCode,
  actionBillerManagerGetTemplateListByPayeeCode,
  actionBillerManagerSpecGet,
  actionBillerManagerTransactionCategoriesGet,
  actionBillerManagerTransactionGroupGet,
  actionSaveBillerManager,
  setBillerDenomList,
  setDataList,
  setDenomFormMode,
  setSuccess,
} from "stores/actions/billerManager";

import { pathnameCONFIG } from "configuration";

import Colors from "helpers/colors";

import FormHeader from "../components/FormHeader";
import PaperTitle from "../components/PaperTitle";
import ListFieldInput from "./listFieldForm/input";

import { useStyles, tableRow } from "./style";
import { parseNumber } from "../../../../../utils/helpers";

const renderTabs = (active, formikRef) => {
  switch (active) {
    case 0:
      return (
        <ListFieldInput
          formikRef={formikRef}
          dataRef="listFieldInput"
          activeTab={active}
        />
      );
    case 1:
      return (
        <ListFieldInput
          formikRef={formikRef}
          dataRef="listFieldConfirmation"
          activeTab={active}
        />
      );
    case 2:
      return (
        <ListFieldInput
          formikRef={formikRef}
          dataRef="listFieldReceipt"
          activeTab={active}
        />
      );
    case 3:
      return (
        <ListFieldInput
          formikRef={formikRef}
          dataRef="listFieldPartial"
          activeTab={active}
        />
      );
    default:
      return null;
  }
};

export default function BillerManagerForm() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const { pathname } = useLocation();

  const {
    id,
    detail,
    categories,
    transactionCategories,
    specs,
    transactionGroups,
    dataForm,
    isSuccess,
    isLoadingExcute,
    prefixes,
    denomList,
    templateList,
    isListField,
  } = useSelector((state) => state.billerManagers);

  const formikRef = useRef();
  const denomFormRef = useRef();

  const [prefixCodes, setPrefixCodes] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [tab, setTab] = useState(0);
  const [isConf, setConf] = useState(false);
  const [payload, setPayload] = useState(null);
  const [cummulativeFee, setCummulativeFee] = useState("Yes");

  const { dataList, denomFormMode } = dataForm;

  const handleChangeDenomList = (denomFormType, rowData) => {
    // eslint-disable-next-line default-case
    switch (denomFormType) {
      case "populate": {
        dispatch(setBillerDenomList(rowData));
        break;
      }
      case "add": {
        const newObj = {
          denominationValue: rowData.denominationValue?.trim() ?? null,
          denominationCategory: rowData.denominationCategory?.trim() ?? null,
          labelId: rowData.labelId?.trim() ?? null,
          labelEn: rowData.labelEn?.trim() ?? null,
          noOrder: dataList.billerDenomList.length + 1,
        };
        const newArr = dataList.billerDenomList.concat(newObj);
        dispatch(setBillerDenomList(newArr));
        break;
      }
      case "edit":
        {
          const newArr = [...dataList.billerDenomList];
          const findIndex = newArr.findIndex(
            ({ noOrder }) => noOrder === denomFormMode.editId
          );
          if (findIndex >= 0) {
            const replacingObj = {
              noOrder: +newArr[findIndex].noOrder,
              denominationValue: rowData.denominationValue?.trim() ?? null,
              denominationCategory:
                rowData.denominationCategory?.trim() ?? null,
              labelId: rowData.labelId?.trim() ?? null,
              labelEn: rowData.labelEn?.trim() ?? null,
            };
            // console.log("ini replacingObj", replacingObj);
            newArr[findIndex] = replacingObj;
            dispatch(setBillerDenomList(newArr));
          }
        }
        break;
      case "delete": {
        const newArr = dataList?.billerDenomList
          .filter(({ noOrder }) => noOrder !== rowData.noOrder)
          .map((r, index) => {
            r.noOrder = index + 1;
            return r;
          });
        dispatch(setBillerDenomList(newArr));
        break;
      }
    }
    dispatch(setDenomFormMode({ mode: "add", editId: null }));
  };

  // const handleMoveDenomList = (noOrder, moveType) => {
  //   const arrRef = dataList.billerDenomList;
  //   // eslint-disable-next-line default-case
  //   switch (moveType) {
  //     case "up":
  //       {
  //         const index = arrRef.findIndex((e) => e.noOrder === noOrder);
  //         if (index > 0) {
  //           const el = arrRef[index];
  //           arrRef[index] = arrRef[index - 1];
  //           arrRef[index - 1] = el;
  //         }
  //       }
  //       break;
  //     case "down":
  //       {
  //         const index = arrRef.findIndex((e) => e.noOrder === noOrder);
  //         if (index !== -1 && index < arrRef.length - 1) {
  //           const el = arrRef[index];
  //           arrRef[index] = arrRef[index + 1];
  //           arrRef[index + 1] = el;
  //         }
  //       }
  //       break;
  //   }
  //   arrRef.map((r, index) => {
  //     r.noOrder = index + 1;
  //     return r;
  //   });
  //   dispatch(setBillerDenomList(arrRef));
  // };

  useEffect(() => {
    const res = pathname.split("/");
    if (res[res.length - 1] === "edit") setIsEdit(true);

    dispatch(actionBillerManagerCategoriesGet());
    dispatch(actionBillerManagerTransactionCategoriesGet());
    dispatch(actionBillerManagerSpecGet());
    dispatch(actionBillerManagerTransactionGroupGet());
  }, [pathname]);

  useEffect(() => {
    if (id) {
      const payload = { payeeCode: id };
      dispatch(actionBillerManagerGetByPayeeCode(payload));
      dispatch(actionBillerManagerGetPrefixByPayeeCode(payload));
      dispatch(actionBillerManagerGetDenomListByPayeeCode(payload));
      dispatch(actionBillerManagerGetTemplateListByPayeeCode(payload));
    }
  }, [id]);

  useEffect(() => {
    if (prefixes.length > 0) {
      const data = prefixes.map(({ prefix }) => prefix.toString());
      setPrefixCodes(data);
    }
  }, [prefixes]);

  useEffect(() => {
    if (denomList.length > 0) {
      handleChangeDenomList("populate", denomList);
    }
    // console.log("ini denomList", denomList);
  }, [denomList]);

  useEffect(() => {
    if (templateList.length > 0) {
      const data = {
        listFieldInput: [],
        listFieldConfirmation: [],
        listFieldReceipt: [],
        listFieldPartial: [],
      };
      templateList.forEach((item) => {
        if (item.templateType === "I") {
          data.listFieldInput.push(item);
        } else if (item.templateType === "C") {
          data.listFieldConfirmation.push(item);
        } else if (item.templateType === "R") {
          data.listFieldReceipt.push(item);
        } else {
          data.listFieldPartial.push(item);
        }
      });
      dispatch(setDataList(data));
    }
  }, [templateList]);

  useEffect(() => {
    if (detail) {
      // console.log("ini formikRef", formikRef.current.values);
      formikRef.current.setFieldValue(
        "billerCategory",
        detail.billerCategory?.toString() ?? ""
      );
      formikRef.current.setFieldValue("billerCode", detail.billerCode ?? "");
      formikRef.current.setFieldValue("billerName", detail.billerName ?? "");
      formikRef.current.setFieldValue("labelId", detail.billerLabel ?? "");
      formikRef.current.setFieldValue("labelEn", detail.billerLabelEn ?? "");
      formikRef.current.setFieldValue("spec", detail.billerSpec ?? "");
      formikRef.current.setFieldValue(
        "transactionCategory",
        detail.transactionCategory ?? ""
      );
      formikRef.current.setFieldValue(
        "transactionGroup",
        detail.transactionGroup?.toString() ?? ""
      );
      formikRef.current.setFieldValue("payeeCode", id);
      formikRef.current.setFieldValue("denom", detail.isDenom);
    }
  }, [detail]);

  const dataHeader = [
    // {
    //   title: "Order Number",
    //   render: (rowData, i) => (
    //     <span style={tableRow}>
    //       {rowData.noOrder}
    //       <Grid container direction="column">
    //         <Grid item>
    //           {i > 0 && (
    //             <span
    //               onClick={() => handleMoveDenomList(rowData.noOrder, "up")}
    //               className={classes.iconMove}
    //             >
    //               <SvgChevronUp style={{ marginLeft: 8.5 }} />
    //             </span>
    //           )}
    //         </Grid>
    //         <Grid item>
    //           {i < dataList.billerDenomList.length - 1 && (
    //             <span
    //               onClick={() => handleMoveDenomList(rowData.noOrder, "down")}
    //               className={classes.iconMove}
    //             >
    //               <SvgChevronDown style={{ marginLeft: 8.5 }} />
    //             </span>
    //           )}
    //         </Grid>
    //       </Grid>
    //     </span>
    //   ),
    // },
    {
      title: "Value",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.denominationValue}</span>
      ),
    },
    {
      title: "Description ID",
      render: (rowData) => <span style={tableRow}>{rowData?.labelId}</span>,
    },
    {
      title: "Description EN",
      render: (rowData) => <span style={tableRow}>{rowData?.labelEn}</span>,
    },
    {
      title: "Denom Category",
      render: (rowData) => (
        <span style={tableRow}>{rowData?.denominationCategory}</span>
      ),
    },
    {
      render: (rowData) => (
        <GeneralMenu
          rowData={rowData}
          options={["edit", "delete"]}
          onDelete={() => {
            handleChangeDenomList("delete", rowData);
          }}
          onEdit={() => {
            dispatch(
              setDenomFormMode({
                mode: "edit",
                editId: rowData.noOrder,
              })
            );
            denomFormRef.current.setFieldValue(
              "denominationValue",
              rowData.denominationValue
            );
            denomFormRef.current.setFieldValue(
              "denominationCategory",
              rowData.denominationCategory
            );
            denomFormRef.current.setFieldValue("labelId", rowData.labelId);
            denomFormRef.current.setFieldValue("labelEn", rowData.labelEn);
          }}
        />
      ),
    },
  ];

  const initValues = {
    billerCategory: "",
    spec: "",
    labelId: "",
    labelEn: "",
    billerCode: "",
    transactionCategory: "",
    transactionGroup: "",
    billerName: "",
    payeeCode: "",
    prefixCode: "",
    denom: true,
  };

  const initValuesDenomForm = {
    denominationValue: "",
    denominationCategory: "",
    labelId: "",
    labelEn: "",
  };
  const BillerSchema = Yup.object().shape({
    billerCategory: Yup.number().required("Biller category is required"),
    transactionCategory: Yup.string().nullable(),
    spec: Yup.number().nullable(),
    transactionGroup: Yup.number().nullable(),
    labelId: Yup.string().nullable(),
    labelEn: Yup.string().nullable(),
    billerName: Yup.string().nullable(),
    billerCode: Yup.string().nullable(),
    payeeCode: Yup.string().required("Payee code is required"),
    // prefixCode: Yup.number().required("Prefix code is required"),
  });

  const DenomSchema = Yup.object().shape({
    denominationValue: Yup.string().required("Denomination value is required"),
    denominationCategory: Yup.string()
      .min(3, "Min 3 Max 100 Character")
      .max(100, "Min 3 Max 100 Character")
      .required("Denomination category is required"),
    labelId: Yup.string().required("Description ID is required"),
    labelEn: Yup.string().required("Description EN is required"),
  });

  const getSchema = () => BillerSchema;
  const getDenomSchema = () => DenomSchema;

  const onSubmit = (values) => {
    // console.log("+++ values", values);
    const chooseCategoryData =
      values.category !== ""
        ? categories?.find(({ value }) => value === values.billerCategory)
        : null;
    if (chooseCategoryData) {
      chooseCategoryData.value = +chooseCategoryData.value;
    }

    const chooseSpecData =
      values.spec !== ""
        ? specs?.find(({ value }) => value === values.spec)
        : null;
    if (chooseSpecData) {
      chooseSpecData.value = +chooseSpecData.value;
    }

    const transactionCategoryData =
      values.transactionCategory !== ""
        ? transactionCategories?.find(
            ({ value }) => value === values.transactionCategory
          )
        : null;
    if (transactionCategoryData) {
      transactionCategoryData.value = +transactionCategoryData.value;
    }

    const transactionGroupData =
      values.transactionGroup !== ""
        ? transactionGroups?.find(
            ({ value }) => value === values.transactionGroup
          )
        : null;
    if (transactionGroupData) {
      transactionGroupData.value = +transactionGroupData.value;
    }

    const dataPayload = {
      billerCategory: +values.billerCategory,
      billerName: values.billerName.trim(),
      payeeCode: values.payeeCode.trim(),
      paymentHandler: 1,
      denom: values.denom,
      creditCardAllowed: false,
      savingAllowed: false,
      currentAllowed: false,
      isDeleted: false,
      isDenom: values.denom,
      transactionGroup: +values.transactionGroup || null,
      transactionCategory: values.transactionCategory || null,
      billerSpec: +values.spec || null,
      chooseCategoryData,
      chooseSpecData,
      transactionCategoryData,
      transactionGroupData,
      billerDenomList:
        dataList.billerDenomList.length > 0 && values.denom
          ? dataList.billerDenomList.map((item) => ({
              ...item,
              payeeCode: values.payeeCode.trim(),
            }))
          : null,
      billerPrefixList: prefixCodes.map((code) => ({
        billerCategory: +values.billerCategory,
        payeeCode: values.payeeCode.trim(),
        prefix: +code,
      })),
      billerTemplateList: [
        ...dataList.listFieldInput,
        ...dataList.listFieldConfirmation,
        ...dataList.listFieldReceipt,
        ...dataList.listFieldPartial,
      ].map((item) => ({
        ...item,
        payeeCode: values.payeeCode.trim(),
      })),
    };
    // console.log("+++ dataPayload", dataPayload);

    setPayload(dataPayload);
    setConf(true);
  };

  // useEffect(() => {
  //   formikRef.current.setFieldValue("labelId", "Test");
  // }, []);dispatch
  return (
    <Formik
      initialValues={initValues}
      validationSchema={getSchema()}
      onSubmit={(values) => {
        onSubmit(values);
      }}
      innerRef={formikRef}
    >
      {({
        errors,
        values,
        touched,
        handleChange,
        handleSubmit,
        handleBlur,
        setFieldValue,
        isValid,
      }) => (
        <Form>
          <div className={classes.parentContainer}>
            {!isEdit && <FormHeader headerText="Add Biller Manager" />}
            {isEdit && <FormHeader headerText="Edit Biller Manager" />}
            <div className={classes.paperContainer}>
              <div className={classes.paper}>
                <div className={classes.paperChildren}>
                  <div className={classes.paperGrandChildren}>
                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Biller Name :"
                        style={{ width: "100%" }}
                        error={errors?.billerName && touched?.billerName}
                        errorMessage={errors?.billerName}
                      >
                        <Input
                          style={{ width: "100%" }}
                          placeholder="Enter Biller Name"
                          name="billerName"
                          value={values.billerName}
                          onBlur={handleBlur}
                          onChange={handleChange}
                        />
                      </FormField>
                      <FormField
                        label="Transaction Category :"
                        style={{ width: "100%" }}
                        error={
                          errors?.transactionCategory &&
                          touched?.transactionCategory
                        }
                        errorMessage={errors?.transactionCategory}
                      >
                        <SelectWithSearch
                          options={transactionCategories || []}
                          placeholder="Choose Transaction Category"
                          name="transactionCategory"
                          value={values.transactionCategory}
                          onBlur={handleBlur}
                          onChange={(val) => {
                            setFieldValue("transactionCategory", val);
                          }}
                        />
                      </FormField>
                    </div>

                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Description :"
                        style={{ width: "100%" }}
                        error={errors?.billerName && touched?.billerName}
                        errorMessage={errors?.billerName}
                      >
                        <Input
                          style={{ width: "100%" }}
                          placeholder="Enter Description"
                          name="billerName"
                          value={values.billerName}
                          onBlur={handleBlur}
                          onChange={handleChange}
                        />
                      </FormField>
                    </div>

                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Surrounding :"
                        style={{ width: "100%" }}
                        error={errors?.spec && touched?.spec}
                        errorMessage={errors?.spec}
                      >
                        <SelectWithSearch
                          options={specs || []}
                          placeholder="Select Surrounding"
                          name="spec"
                          value={values.spec}
                          onBlur={handleBlur}
                          onChange={(val) => {
                            setFieldValue("spec", val);
                          }}
                        />
                      </FormField>
                      <FormField
                        label="Currency :"
                        style={{ width: "100%" }}
                        error={
                          errors?.transactionGroup && touched?.transactionGroup
                        }
                        errorMessage={errors?.transactionGroup}
                      >
                        <SelectWithSearch
                          options={transactionGroups || []}
                          placeholder="Select Currency"
                          name="transactionGroup"
                          value={values.transactionGroup}
                          onBlur={handleBlur}
                          onChange={(val) => {
                            setFieldValue("transactionGroup", val);
                          }}
                        />
                      </FormField>
                    </div>

                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Channel :"
                        style={{ width: "100%" }}
                        error={
                          errors?.transactionGroup && touched?.transactionGroup
                        }
                        errorMessage={errors?.transactionGroup}
                      >
                        <SelectWithSearch
                          options={transactionGroups || []}
                          placeholder="Select Currency"
                          name="transactionGroup"
                          value={values.transactionGroup}
                          onBlur={handleBlur}
                          onChange={(val) => {
                            setFieldValue("transactionGroup", val);
                          }}
                        />
                      </FormField>
                      <FormField
                        label="Product Code :"
                        style={{ width: "100%" }}
                        error={errors?.billerName && touched?.billerName}
                        errorMessage={errors?.billerName}
                      >
                        <Input
                          style={{ width: "100%" }}
                          placeholder="Enter Product Code"
                          name="billerName"
                          value={values.billerName}
                          onBlur={handleBlur}
                          onChange={handleChange}
                        />
                      </FormField>
                    </div>

                    <div className={classes.lineDevider}> </div>

                    <div style={{ marginBottom: "20px" }}>
                      <h1 className={classes.headingTitle}>
                        Fee Configuration
                      </h1>
                    </div>

                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Bank Fee :"
                        style={{ width: "100%" }}
                        error={errors?.labelEn && touched?.labelEn}
                        errorMessage={errors?.labelEn}
                      >
                        <Input
                          style={{ width: "100%" }}
                          placeholder="Input Bank Fee"
                          name="labelEn"
                          value={values.labelEn}
                          onBlur={handleBlur}
                          onChange={handleChange}
                        />
                      </FormField>
                      <FormField
                        label="Surrounding Fee :"
                        style={{ width: "100%" }}
                        error={errors?.payeeCode && touched?.payeeCode}
                        errorMessage={errors?.payeeCode}
                      >
                        <Input
                          style={{ width: "100%" }}
                          placeholder="Input Surrounding Fee"
                          name="payeeCode"
                          value={parseNumber(values.payeeCode)}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={10}
                        />
                      </FormField>
                    </div>

                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Cummulative :"
                        style={{ width: "100%" }}
                        error={errors?.billerName && touched?.billerName}
                        errorMessage={errors?.billerName}
                      >
                        <RadioGroup
                          value={cummulativeFee}
                          onChange={(e) => setCummulativeFee(e.target.value)}
                          options={["Yes", "No"]}
                          direction="horizontal"
                          width={200}
                          style={{ display: "flex", marginTop: "15px" }}
                        />
                      </FormField>
                    </div>

                    <div className={classes.lineDevider}> </div>

                    <div style={{ marginBottom: "20px" }}>
                      <h1 className={classes.headingTitle}>
                        Payment Configuration
                      </h1>
                    </div>

                    <div className={classes.paperGreatGrandChildren}>
                      <FormField
                        label="Payment Amount :"
                        style={{ width: "100%" }}
                        error={
                          errors?.transactionGroup && touched?.transactionGroup
                        }
                        errorMessage={errors?.transactionGroup}
                      >
                        <SelectWithSearch
                          options={transactionGroups || []}
                          placeholder="Select Payment Amount"
                          name="transactionGroup"
                          value={values.transactionGroup}
                          onBlur={handleBlur}
                          onChange={(val) => {
                            setFieldValue("transactionGroup", val);
                          }}
                        />
                      </FormField>
                      <FormField
                        label="Payment Option :"
                        style={{ width: "100%" }}
                        error={
                          errors?.transactionGroup && touched?.transactionGroup
                        }
                        errorMessage={errors?.transactionGroup}
                      >
                        <SelectWithSearch
                          options={transactionGroups || []}
                          placeholder="Select Payment Option"
                          name="transactionGroup"
                          value={values.transactionGroup}
                          onBlur={handleBlur}
                          onChange={(val) => {
                            setFieldValue("transactionGroup", val);
                          }}
                        />
                      </FormField>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className={classes.buttonGroupAll}>
              <ButtonOutlined label="Cancel" />
              <GeneralButton
                label="Save"
                type="submit"
                disabled={
                  !isValid ||
                  (values.denom && dataList.billerDenomList?.length === 0) ||
                  !isListField
                }
              />
            </div>
            <DeletePopup
              isOpen={isConf}
              message={`Are You Sure To ${
                isEdit ? "Edit" : "Add"
              } Biller Manager?`}
              onContinue={() => {
                setConf(false);
                dispatch(actionSaveBillerManager(payload));
              }}
              handleClose={() => {
                setConf(false);
              }}
              loading={isLoadingExcute}
            />
            <SuccessConfirmation
              isOpen={isSuccess}
              handleClose={() => {
                dispatch(setSuccess(false));
                history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER);
              }}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}

BillerManagerForm.propTypes = {};
