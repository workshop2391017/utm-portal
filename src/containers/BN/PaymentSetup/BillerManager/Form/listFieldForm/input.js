/* eslint-disable array-callback-return */
import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";

import TableScroll from "components/BN/Table/TabbleScrollblue";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import FormField from "components/BN/Form/InputGroupValidation";
import Input from "components/BN/TextField/AntdTextField";
import GeneralSwitch from "components/BN/Switch/ToggleSwitchBlue";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";

import {
  setIsListField,
  setListField,
  setTemplateFormMode,
} from "stores/actions/billerManager";

import { useStyles, tableRow, border } from "../style";
import { numberIgnoreZero, parseNumber } from "../../../../../../utils/helpers";

const dataHeader = ({
  dispatch,
  handleChangeTemplateList,
  initValues,
  listInputRef,
}) => [
  {
    title: "Input Type",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.inputType || "-"}</span>
    ),
  },
  {
    title: "Line Number",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.lineNum || "-"}</span>
    ),
  },
  {
    title: "Field Name (ID)",
    render: (rowData) => <span style={tableRow}>{rowData?.nameId || "-"}</span>,
  },
  {
    title: "Field Name (EN)",
    render: (rowData) => <span style={tableRow}>{rowData?.nameEn || "-"}</span>,
  },
  {
    title: "Placeholder ID",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.placeHolderId || "-"}</span>
    ),
  },
  {
    title: "Placeholder EN",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.placeHolderEn || "-"}</span>
    ),
  },
  {
    title: "Field Key",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.fieldKey || "-"}</span>
    ),
  },
  {
    title: "Field Type",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.fieldType || "-"}</span>
    ),
  },
  {
    title: "Min Field Length",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.minFieldLength || "-"}</span>
    ),
  },
  {
    title: "Max Field Length",
    render: (rowData) => (
      <span style={tableRow}>{rowData?.maxFieldLength || "-"}</span>
    ),
  },
  {
    width: 100,
    render: (rowData) => (
      <GeneralMenu
        rowData={rowData}
        options={["edit", "delete"]}
        onDelete={() => {
          handleChangeTemplateList("delete", rowData);
        }}
        onEdit={() => {
          dispatch(
            setTemplateFormMode({
              mode: "edit",
              editId: rowData.noOrder,
            })
          );
          // rowData.denominationValue
          const elements = Object.keys(initValues);
          elements.map((item) => {
            listInputRef.current.setFieldValue(
              item,
              rowData[item] || initValues[item]
            );
          });
        }}
      />
    ),
    // width: 60,
  },
];

const getTemplateType = (tab) => {
  switch (tab) {
    case 0:
      return "I";
    case 1:
      return "C";
    case 2:
      return "R";
    case 3:
      return "P";
    default:
      return "I";
  }
};

const ListFieldInput = (props) => {
  const classes = useStyles();
  const listInputRef = useRef();
  const dispatch = useDispatch();

  const { formikRef, dataRef, activeTab } = props;

  const { dataForm, isListField } = useSelector(
    (state) => state.billerManagers
  );

  const { dataList, templateFormMode } = dataForm;

  const initValues = {
    fieldKey: "",
    lineNum: "",
    nameId: "",
    nameEn: "",
    placeHolderId: "",
    placeHolderEn: "",
    inputType: "Y",
    templateType: "I",
    fieldType: "",
    maxFieldLength: "",
    minFieldLength: "",
  };

  const Schema = Yup.object().shape({
    fieldKey: Yup.string().required("Field key is required"),
    lineNum: Yup.string()
      .min(1, "Min 1 Max 2 Character")
      .max(2, "Min 1 Max 2 Character")
      .required("Line number is required"),
    nameId: Yup.string()
      .min(3, "Min 3 Character")
      .required("Field name (ID) is required"),
    nameEn: Yup.string()
      .min(3, "Min 3 Character")
      .required("Field name (EN) is required"),
    placeHolderId: Yup.string()
      .min(3, "Min 3 Character")
      .required("Placeholder (ID) is required"),
    placeHolderEn: Yup.string()
      .min(3, "Min 3 Character")
      .required("Placeholder (EN) is required"),
    fieldType: Yup.string()
      .min(3, "Min 3 Character")
      .required("Field type is required"),
    maxFieldLength: Yup.string()
      .min(3, "Must Input 3 Character")
      .max(3, "Must Input 3 Character")
      .required("Max field length is required"),
    minFieldLength: Yup.string()
      .min(3, "Must Input 3 Character")
      .max(3, "Must Input 3 Character")
      .required("Min field length is required"),
  });

  const handleChangeTemplateList = (formType, rowData) => {
    // eslint-disable-next-line default-case
    switch (formType) {
      case "add": {
        const noOrder = dataList[dataRef].length + 1;
        const array = dataList[dataRef];
        const newObj = {
          ...rowData,
          templateType: getTemplateType(activeTab),
          lineNum: +rowData.lineNum,
          minFieldLength: +rowData.minFieldLength || null,
          maxFieldLength: +rowData.maxFieldLength || null,
          noOrder,
        };
        const newArr = array.concat(newObj);
        dispatch(setListField({ data: newArr, dataRef }));
        break;
      }
      case "edit":
        {
          const newArr = dataList[dataRef];
          const findIndex = newArr.findIndex(
            ({ noOrder }) => noOrder === templateFormMode.editId
          );
          if (findIndex >= 0) {
            const replacingObj = {
              ...rowData,
              minFieldLength: +rowData.minFieldLength || null,
              maxFieldLength: +rowData.maxFieldLength || null,
              noOrder: newArr[findIndex].noOrder,
            };
            newArr[findIndex] = replacingObj;
            dispatch(setListField({ data: newArr, dataRef }));
          }
        }
        break;
      case "delete": {
        const newArr = dataList[dataRef].filter(
          ({ noOrder }) => noOrder !== rowData.noOrder
        );
        newArr.map((r, index) => {
          r.noOrder = index + 1;
          return r;
        });
        dispatch(setListField({ data: newArr, dataRef }));
        break;
      }
    }
    dispatch(setTemplateFormMode({ mode: "add", editId: null }));
  };

  const getSchema = () => Schema;

  useEffect(() => {
    listInputRef.current?.resetForm();
    dispatch(setTemplateFormMode({ mode: "add", editId: null }));
  }, [activeTab]);

  useEffect(() => {
    if (
      dataList.listFieldInput?.length > 0 ||
      dataList.listFieldConfirmation?.length > 0 ||
      dataList.listFieldPartial?.length > 0 ||
      dataList.listFieldReceipt?.length > 0
    ) {
      dispatch(setIsListField(true));
    } else {
      dispatch(setIsListField(false));
    }
  }, [dataList[dataRef]?.length]);

  return (
    <React.Fragment>
      <div
        className={classes.paperGreatGrandChildren}
        style={{ marginTop: 20 }}
      >
        <TableScroll
          cols={dataHeader({
            dispatch,
            handleChangeTemplateList,
            initValues,
            listInputRef,
          })}
          data={dataList[dataRef] || []}
          height="304px"
          bordered
          scroll
        />
      </div>
      <Formik
        initialValues={initValues}
        validationSchema={getSchema()}
        onSubmit={(values, { resetForm }) => {
          handleChangeTemplateList(templateFormMode.mode, values);
          resetForm();
        }}
        innerRef={listInputRef}
      >
        {(subformik) => (
          <div style={border}>
            <div className={classes.paperGrandChildren}>
              <div className={classes.paperGreatGrandChildren}>
                <FormField
                  label="Line Number"
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.lineNum && subformik?.touched?.lineNum
                  }
                  errorMessage={subformik?.errors?.lineNum}
                >
                  <Input
                    style={{ width: "100%" }}
                    placeholder="Enter Line Number"
                    name="lineNum"
                    value={parseNumber(subformik.values.lineNum)}
                    onBlur={subformik.handleBlur}
                    onChange={subformik.handleChange}
                  />
                </FormField>
                <div
                  style={{
                    gap: 20,
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <FormField
                    label="Min. Field Length :"
                    style={{ width: "100%" }}
                    error={
                      subformik?.errors?.minFieldLength &&
                      subformik?.touched?.minFieldLength
                    }
                    errorMessage={subformik?.errors?.minFieldLength}
                  >
                    <Input
                      style={{ width: "100%" }}
                      placeholder="enter Min. Field Length"
                      name="minFieldLength"
                      value={parseNumber(subformik.values.minFieldLength)}
                      onBlur={subformik.handleBlur}
                      onChange={subformik.handleChange}
                      maxLength={3}
                    />
                  </FormField>
                  <FormField
                    label="Max. Field Length :"
                    style={{ width: "100%" }}
                    error={
                      subformik?.errors?.maxFieldLength &&
                      subformik?.touched?.maxFieldLength
                    }
                    errorMessage={subformik?.errors?.maxFieldLength}
                  >
                    <Input
                      style={{ width: "100%" }}
                      placeholder="enter Max. Field Length"
                      name="maxFieldLength"
                      value={parseNumber(subformik.values.maxFieldLength)}
                      onBlur={subformik.handleBlur}
                      onChange={subformik.handleChange}
                      maxLength={3}
                    />
                  </FormField>
                </div>
              </div>
              <div className={classes.paperGreatGrandChildren}>
                <FormField
                  label="Field Name (EN) :"
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.nameEn && subformik?.touched?.nameEn
                  }
                  errorMessage={subformik?.errors?.nameEn}
                >
                  <Input
                    style={{ width: "100%" }}
                    placeholder="Enter Field Name"
                    name="nameEn"
                    value={subformik.values.nameEn}
                    onBlur={subformik.handleBlur}
                    onChange={subformik.handleChange}
                  />
                </FormField>
                <FormField
                  label="Field Name (ID) :"
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.nameId && subformik?.touched?.nameId
                  }
                  errorMessage={subformik?.errors?.nameId}
                >
                  <Input
                    style={{ width: "100%" }}
                    placeholder="Enter Field Name"
                    name="nameId"
                    value={subformik.values.nameId}
                    onBlur={subformik.handleBlur}
                    onChange={subformik.handleChange}
                  />
                </FormField>
              </div>
              <div className={classes.paperGreatGrandChildren}>
                <FormField
                  label="Placeholder (EN) :"
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.placeHolderEn &&
                    subformik?.touched?.placeHolderEn
                  }
                  errorMessage={subformik?.errors?.placeHolderEn}
                >
                  <Input
                    style={{ width: "100%" }}
                    placeholder="Enter Placeholder"
                    name="placeHolderEn"
                    value={subformik.values.placeHolderEn}
                    onBlur={subformik.handleBlur}
                    onChange={subformik.handleChange}
                  />
                </FormField>
                <FormField
                  label="Placeholder (ID) :"
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.placeHolderId &&
                    subformik?.touched?.placeHolderId
                  }
                  errorMessage={subformik?.errors?.placeHolderId}
                >
                  <Input
                    style={{ width: "100%" }}
                    placeholder="Eenter Placeholder"
                    name="placeHolderId"
                    value={subformik.values.placeHolderId}
                    onBlur={subformik.handleBlur}
                    onChange={subformik.handleChange}
                  />
                </FormField>
              </div>
              <div className={classes.paperGreatGrandChildren}>
                <FormField
                  label="Field Type :"
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.fieldType &&
                    subformik?.touched?.fieldType
                  }
                  errorMessage={subformik?.errors?.fieldType}
                >
                  <Input
                    style={{ width: "100%" }}
                    placeholder="Enter Field Type"
                    name="fieldType"
                    value={subformik.values.fieldType}
                    onBlur={subformik.handleBlur}
                    onChange={subformik.handleChange}
                  />
                </FormField>
                <FormField
                  label="Fieldkey : "
                  style={{ width: "100%" }}
                  error={
                    subformik?.errors?.fieldKey && subformik?.touched?.fieldKey
                  }
                  errorMessage={subformik?.errors?.fieldKey}
                >
                  <SelectWithSearch
                    options={[{ label: "DUMMY CUSTNAME", value: "CUSTNAME" }]}
                    placeholder="Fieldkey"
                    name="fieldKey"
                    value={subformik.values.fieldKey}
                    onBlur={subformik.handleBlur}
                    onChange={(val) => {
                      subformik.setFieldValue("fieldKey", val);
                    }}
                  />
                </FormField>
              </div>
              <div className={classes.paperGreatGrandChildren}>
                <div
                  style={{
                    gap: 20,
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <div>Input Type?</div>
                  <GeneralSwitch
                    checked={subformik.values.inputType === "Y"}
                    onBlur={subformik.handleBlur}
                    onChange={(event) => {
                      const val = event.target.checked ? "Y" : "N";
                      subformik.setFieldValue("inputType", val);
                    }}
                  />
                </div>
                <div className={classes.paperGreatGrandChildren} />
              </div>
            </div>
            <div className={classes.buttonGroup}>
              <ButtonOutlined
                label="Clear"
                onClick={() => subformik.resetForm()}
              />
              <GeneralButton
                label="Save"
                onClick={() => subformik.submitForm()}
                disabled={Object.keys(subformik.values).some(
                  (item) =>
                    item !== "minFieldLength" &&
                    item !== "maxFieldLength" &&
                    !subformik.values[item]
                )}
              />
            </div>
          </div>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default ListFieldInput;
