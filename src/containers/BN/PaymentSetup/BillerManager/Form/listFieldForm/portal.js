import React, { useState } from "react";
import TableScroll from "components/BN/Table/TabbleScrollblue";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import FormField from "components/BN/Form/InputGroupValidation";
import Input from "components/BN/TextField/AntdTextField";

import GeneralSwitch from "components/BN/Switch/GeneralSwitch";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import RichTextAlt from "components/BN/RichTextAlt";
import { useStyles, tableRow, border } from "../style";

const dataHeader = [
  {
    title: "Confirmation Text ID",
    render: (rowData) => <span style={tableRow}>{rowData?.textId}</span>,
  },
  {
    title: "Confirmation Text EN",
    render: (rowData) => <span style={tableRow}>{rowData?.textEn}</span>,
  },
  {
    width: 70,
    render: (rowData) => (
      <GeneralMenu
        rowData={rowData}
        options={["edit", "delete"]}
        onDelete={() => {
          console.warn("Isi rowData : ", rowData);
        }}
        onEdit={() => {
          // history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER_EDIT);
        }}
      />
    ),
    // width: 60,
  },
];

const dummyData = [
  {
    id: 1,
    value: "Paket Data",
    textId:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
    textEn:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
  },
  {
    id: 1,
    value: "Paket Data",
    textId:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
    textEn:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
  },
  {
    id: 1,
    value: "Paket Data",
    textId:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
    textEn:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
  },

  {
    id: 1,
    value: "Paket Data",
    textId:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
    textEn:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
  },
  {
    id: 1,
    value: "Paket Data",
    textId:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
    textEn:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
  },
  {
    id: 1,
    value: "Paket Data",
    textId:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
    textEn:
      "Lorem ipsum dolor sit amet, elite adipiscing consectetur. Amet consectetur elite integer in lectus. Sodales sed sem faucibus et velit volutpat egestas.",
  },
];

const ListFieldPortal = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div
        className={classes.paperGreatGrandChildren}
        style={{ marginTop: 20 }}
      >
        <TableScroll
          cols={dataHeader}
          data={dummyData}
          height="304px"
          bordered
          scroll
        />
      </div>
      <div style={border}>
        <div className={classes.paperGrandChildren}>
          <FormField label="Confirmation Text ID :">
            <RichTextAlt onChange={() => {}} />
          </FormField>
          <FormField label="Confirmation Text EN :" style={{ marginTop: 20 }}>
            <RichTextAlt onChange={() => {}} />
          </FormField>
        </div>
        <div className={classes.buttonGroup}>
          <ButtonOutlined label="Clear" />
          <GeneralButton label="Save" />
        </div>
      </div>
    </React.Fragment>
  );
};

export default ListFieldPortal;
