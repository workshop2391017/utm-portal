import { makeStyles } from "@material-ui/core";

export const buttonsContainer = {
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  gap: "20px",
  width: "100%",
};

export const buttonSmall = {
  width: "72px",
  height: "43px",
  padding: "10px 20px",
  gap: "10px",
};

export const buttonMedium = {
  width: "93px",
  height: "43px",
  padding: "10px 20px",
  gap: "10px",
};

export const buttonLarge = {
  width: "122px",
  height: "43px",
  padding: "10px 20px",
  gap: "10px",
};

export const border = {
  width: "100%",
  border: "1px solid #B3C1E7",
  borderRadius: "8px",
  padding: "30px",
  gap: "30px",
  display: "flex",
  flexDirection: "column",
};

export const tableRow = {
  height: "16px",
  fontFamily: "FuturaBkBT",
  fontWeight: 400,
  fontSize: "13px",
  lineHeight: "16px",
  display: "flex",
  alignItems: "center",
  color: "#374062",
};

export const useStyles = makeStyles({
  parentContainer: {
    backgroundColor: "#F4F7FB",
    width: "100%",
    height: "100%",
  },
  paperContainer: {
    width: "100%",
    height: "auto",
    padding: "0px 20px 20px 20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  paper: {
    backgroundColor: "#FFFFFF",
    width: "72%",
    height: "auto",
    padding: "30px",
    gap: "50px",
    borderRadius: "20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: "100px",
  },
  paperChildren: {
    width: "100%",
    gap: "20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  paperGrandChildren: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  paperGreatGrandChildren: {
    gap: 40,
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  buttonGroup: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
  },
  buttonGroupAll: {
    position: "fixed",
    bottom: 0,
    right: 0,
    left: 200,
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "white",
    padding: 20,
  },
  iconMove: {
    cursor: "pointer",
  },
  lineDevider: {
    width: "100%",
    height: "2px",
    backgroundColor: "#E0E3EE",
    margin: "20px 0",
  },
  headingTitle: {
    fontSize: "18px",
    fontWeight: 600,
    lineHeight: "24px",
    color: "#374062",
  },
});
