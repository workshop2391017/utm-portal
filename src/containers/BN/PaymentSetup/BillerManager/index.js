import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Avatar } from "@material-ui/core";
import styled from "styled-components";

import Badge from "components/BN/Badge";
import TableICBB from "components/BN/TableIcBB";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import Search from "components/BN/Search/SearchWithoutDropdown";
import ButtonGeneral from "components/BN/Button/GeneralButton";
import DeletePopup from "components/BN/Popups/Delete";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";

import { ReactComponent as Plus } from "assets/icons/BN/plus.svg";
import { ReactComponent as GreenCheck } from "assets/icons/BN/check-circle-green.svg";
import { ReactComponent as RedExit } from "assets/icons/BN/x-circle-large.svg";

import useDebounce from "utils/helpers/useDebounce";

import {
  actionBillerManagerGet,
  actionBillerManagerGetByPayeeCode,
  actionBillerManagerGetDenomListByPayeeCode,
  actionBillerManagerGetPrefixByPayeeCode,
  actionBillerManagerGetTemplateListByPayeeCode,
  actionBillerManagerSpecGet,
  actionBillerManagerTransactionCategoriesGet,
  actionBillerManagerTransactionGroupGet,
  actionDeleteBiller,
  clearForm,
  setId,
  setSuccess,
} from "stores/actions/billerManager";

import { pathnameCONFIG } from "../../../../configuration";

import {
  useStyles,
  searchStyle,
  buttonStyle,
  columnStyle,
  redBadgeStyle,
  greenBadgeStyle,
} from "./style";
import { validateTask } from "../../../../stores/actions/validateTaskPortal";
import { validateTaskConstant } from "../../../../stores/actions/validateTaskPortal/constantValidateTask";
import reload from "../../../../assets/icons/BN/reload.png";
import { setTypeIsOpen } from "../../../../stores/actions/internationalBank";

import { dataBillerProduct } from "../index.dummy";

const Title = styled.div`
  width: 150px;
  height: 24px;
  font-family: FuturaHvBT;
  font-weight: 400;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.03em;
  color: #374062;
`;

const dataHeader = ({
  setDeleteConfirmation,
  dispatch,
  history,
  setOpenModalReloadActive,
}) => [
  {
    title: "Biller",
    render: (rowData) => <span style={columnStyle}>{rowData?.billerName}</span>,
  },
  {
    title: "Category",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.billerCategory}</span>
    ),
  },
  {
    title: "Description",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.description}</span>
    ),
  },
  {
    title: "Surrounding Spec",
    render: (rowData) => (
      <span style={columnStyle}>
        {rowData?.billerSpec === null ? "-" : rowData?.billerSpec}
      </span>
    ),
    width: 120,
  },
  {
    title: "Fee Bank",
    render: (rowData) => <span style={columnStyle}>{rowData?.feeBank}</span>,
    width: 80,
  },
  {
    title: "Fee Surrounding",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.feeSurrounding}</span>
    ),
    width: 120,
  },
  {
    title: "Cummulative Fee",
    render: (rowData) => {
      return (
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
          }}
        >
          {rowData?.cummulativeFee ? <GreenCheck /> : <RedExit />}
        </div>
      );
    },
    width: 120,
  },
  {
    title: "Payment Amount",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.paymentAmount}</span>
    ),
    width: 120,
  },
  {
    title: "Payment Option",
    render: (rowData) => (
      <span style={columnStyle}>{rowData?.paymentOption}</span>
    ),
    width: 120,
  },
  {
    title: "Currency",
    render: (rowData) => <span style={columnStyle}>{rowData?.currency}</span>,
    width: 120,
  },
  {
    title: "Status",
    render: (rowData) => (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
        }}
      >
        <Badge
          outline
          label={rowData.status ? "Active" : "Inactive"}
          type={rowData.status ? "green" : "red"}
        />
        {rowData.status ? (
          <GeneralMenu
            rowData={rowData}
            options={["edit", "inactive"]}
            onDelete={() => {}}
            onEdit={() => {
              history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER_ADD);
            }}
          />
        ) : (
          <Avatar
            onClick={() =>
              // setOneItem(rowData);
              setOpenModalReloadActive(true)
            }
            src={reload}
            style={{
              width: "21px",
              height: "21px",
              display: "flex",
              justifyContent: "center",
              cursor: "pointer",
            }}
          />
        )}
      </div>
    ),
  },
];
export default function BillerManager() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [openModalReloadActive, setOpenModalReloadActive] = useState(false);

  const {
    isLoading,
    isLoadingExcute,
    data,
    isSuccess,
    detail,
    prefixes,
    denomList,
    templateList,
    transactionCategories,
    specs,
    transactionGroups,
  } = useSelector((state) => state.billerManagers);

  const [dataSearch, setDataSearch] = useState("");
  const [page, setPage] = useState(1);
  const [deleteConfirmation, setDeleteConfirmation] = useState(false);
  const [refetching, setRefetching] = useState(false);

  const searchDebounce = useDebounce(dataSearch, 1000);

  const billerFetchHandler = (payload) => {
    dispatch(actionBillerManagerGet(payload));
  };

  const handleDelete = () => {
    const chooseSpecData =
      specs?.find(({ value }) => +value === detail.billerSpec) ?? null;
    if (chooseSpecData) {
      chooseSpecData.value = +chooseSpecData.value;
    }

    let transactionCategoryData =
      transactionCategories?.find(
        ({ labelEng }) => labelEng === detail.transactionCategory
      ) ?? null;
    if (transactionCategoryData) {
      transactionCategoryData = {
        label: transactionCategoryData.labelEng,
        value: transactionCategoryData.value,
      };
    }

    const transactionGroupData =
      transactionGroups?.find(
        ({ value }) => +value === detail.transactionGroup
      ) ?? null;
    if (transactionGroupData) {
      transactionGroupData.value = +transactionGroupData.value;
    }

    const payload = {
      billerCategory: detail.billerCategory ?? null,
      billerName: detail.billerCategoryName ?? null,
      payeeCode: detail.payeeCode,
      paymentHandler: 1,
      denom: detail.isDenom,
      isDenom: detail.isDenom,
      savingAllowed: detail.savingAllowed,
      currentAllowed: detail.currentAllowed,
      creditCardAllowed: detail.creditCardAllowed,
      isDeleted: true,
      transactionGroup: detail.transactionGroup ?? null,
      transactionCategory: detail.transactionCategory ?? null,
      billerSpec: detail.billerSpec ?? null,
      billerPrefixList: prefixes,
      billerDenomList: denomList,
      billerTemplateList: templateList,
      chooseCategoryData: {
        value: detail.billerCategory,
        label: detail.billerCategoryName,
      },
      chooseSpecData,
      transactionCategoryData,
      transactionGroupData,
    };
    dispatch(actionDeleteBiller(payload, setDeleteConfirmation));
  };

  useEffect(() => {
    const payload = {
      pageNumber: page - 1,
      limit: 10,
      billerName: searchDebounce,
      billerCategory: searchDebounce,
    };
    billerFetchHandler(payload);
  }, [page, refetching, searchDebounce]);

  useEffect(() => {
    setPage(1);
  }, [searchDebounce]);

  useEffect(() => {
    dispatch(actionBillerManagerTransactionGroupGet());
    dispatch(actionBillerManagerSpecGet());
    dispatch(actionBillerManagerTransactionCategoriesGet());
  }, []);

  return (
    <div className={classes.parentContainer}>
      <div className={classes.headerContainer}>
        <Title>Biller Manager</Title>
        <div className={classes.filterContainer}>
          <Search
            dataSearch={dataSearch}
            setDataSearch={setDataSearch}
            style={searchStyle}
            placeholder="Biller's Name, Payee Code"
          />
          <ButtonGeneral
            label="Biller"
            iconPosition="startIcon"
            buttonIcon={<Plus />}
            style={buttonStyle}
            onClick={() => {
              dispatch(clearForm());
              history.push(pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER_ADD);
            }}
          />
        </div>
      </div>
      <TableICBB
        headerContent={dataHeader({
          setDeleteConfirmation,
          setOpenModalReloadActive,
          dispatch,
          history,
          detail,
        })}
        isLoading={isLoading}
        dataContent={dataBillerProduct?.billerProducts ?? []}
        page={page}
        setPage={setPage}
        totalElement={data?.totalElements || 0}
        totalData={data?.totalPages || 0}
        isFullTable={false}
        widthTable="2200px"
      />
      <DeletePopup
        isOpen={deleteConfirmation}
        onContinue={handleDelete}
        handleClose={() => setDeleteConfirmation(false)}
        loading={isLoadingExcute}
      />
      <DeletePopup
        title="Confirmation"
        message="Are You Sure Active the Data?"
        isOpen={openModalReloadActive}
        loading={isLoading}
        handleClose={() => {
          setOpenModalReloadActive(false);
        }}
        // onContinue={onReloadItem}
      />
      <SuccessConfirmation
        isOpen={isSuccess}
        message="Deleted Successfully"
        handleClose={() => {
          dispatch(setSuccess(false));
          setRefetching((prev) => !prev);
        }}
      />
    </div>
  );
}

BillerManager.propTypes = {};
