import { makeStyles } from "@material-ui/core";

export const searchStyle = {
  width: "200px",
  height: "40px",
  boxSizing: "border-box",
  backgroundColor: "#FFFFFF",
  border: "1px solid #B3C1E7",
  borderRadius: "8px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

export const buttonStyle = {
  width: "107px",
  height: "44px",
  padding: "10px 20px",
  gap: "4px",
  backgroundColor: "#0061A7",
  borderRadius: "10px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

export const columnStyle = {
  width: "auto",
  height: "18px",
  fontFamily: "FuturaBkBT",
  fontWeight: 400,
  fontSize: "13px",
  lineHeight: "18px",
  letterSpacing: "0.01em",
  color: "#374062",
  display: "flex",
  alignItems: "center",
};

export const redBadgeStyle = {
  border: "1px solid #D14848",
  textAlign: "center",
  width: "69px",
};

export const greenBadgeStyle = {
  border: "1px solid #75D37F",
  textAlign: "center",
  width: "69px",
};

export const useStyles = makeStyles({
  parentContainer: {
    backgroundColor: "#F4F7FB",
    width: "100%",
    height: "100%",
    padding: "0px 40px 20px 20px",
  },
  headerContainer: {
    width: "100%",
    height: "76px",
    marginBottom: "20px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  filterContainer: {
    width: "327px",
    height: "44px",
    gap: "20px",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  content: {
    display: "flex",
    marginTop: "14px",
  },
});
