import React from "react";

import AntdTextField from "components/BN/TextField/AntdTextField";
import AntdSelect from "components/BN/Select/SelectGroup";

import { useStyles, selectStyle } from "./style";

export default function Input({
  caption,
  value,
  placeholder,
  suffix,
  isDropDown,
}) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <span className={classes.caption}>{caption}</span>
      {!isDropDown && (
        <AntdTextField
          suffix={suffix}
          className={classes.textField}
          placeholder={placeholder}
        />
      )}
      {isDropDown && <AntdSelect style={selectStyle} value={value} />}
    </div>
  );
}
