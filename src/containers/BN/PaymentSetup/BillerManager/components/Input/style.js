import { makeStyles } from "@material-ui/core";

export const selectStyle = {
  width: "450px",
  height: "40px",
  gap: "10px",
  boxSizing: "border-box",
  boxShadow: "inset 0px -1px 0px #E8EEFF",
  borderRadius: "10px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontFamily: "FuturaBQ",
  fontWeight: 500,
  fontSize: "15px",
  lineHeight: "24px",
  letterSpacing: "0.03em",
};

export const useStyles = makeStyles({
  container: {
    width: "100%",
    height: "60px",
    gap: "4px",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  caption: {
    width: "100%",
    height: "16px",
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
  textField: {
    width: "100%",
    height: "40px",
    padding: "11px 10px",
    gap: "5px",
    boxSizing: "border-box",
    border: "1px solid #B3C1E7",
    borderRadius: "10px",
    display: "flex",
    alignItems: "flex-start",
  },
});
