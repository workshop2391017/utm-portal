import React from "react";

import GeneralSwitch from "components/BN/Switch/GeneralSwitch";

import { useStyles } from "./style";

export default function Toggle({ caption, isDenomination, setIsDenomination }) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <span className={classes.caption}>{caption}</span>
      <GeneralSwitch onChange={() => setIsDenomination(!isDenomination)} />
    </div>
  );
}
