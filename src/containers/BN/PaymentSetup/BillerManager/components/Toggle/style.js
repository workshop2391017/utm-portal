import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    width: "450px",
    height: "16px",
    gap: "123px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  caption: {
    width: "123px",
    height: "16px",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "16px",
    color: "#374062",
  },
});
