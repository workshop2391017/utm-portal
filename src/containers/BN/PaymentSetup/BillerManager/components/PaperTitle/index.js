import React from "react";

import { useStyles } from "./style";

export default function PaperTitle({ titleText }) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <h6 className={classes.h6}>{titleText}</h6>
      <hr className={classes.line} />
    </div>
  );
}
