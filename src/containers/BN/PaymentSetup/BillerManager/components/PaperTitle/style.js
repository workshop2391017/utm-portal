import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  container: {
    width: "100%",
    height: "23px",
    gap: "4px",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  h6: {
    width: "960px",
    height: "18px",
    fontFamily: "FuturaMdBT",
    fontWeight: 400,
    fontSize: "15px",
    lineHeight: "18px",
    color: "#AEB3C6",
  },
  line: {
    border: 0,
    borderBottom: "1px solid #AEB3C6",
    width: "100%",
  },
});
