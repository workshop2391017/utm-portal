import React from "react";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "components/BN/Button/GeneralButton";

export default function Buttons({
  height,
  padding,
  backgroundColor,
  styleContainer,
  styleOutlined,
  styleGeneral,
  labelOutlined,
  labelGeneral,
  buttonFunction1,
  buttonFunction2,
}) {
  return (
    <div style={{ height, padding, backgroundColor, ...styleContainer }}>
      <ButtonOutlined
        style={styleOutlined}
        label={labelOutlined}
        onClick={buttonFunction1}
      />
      <GeneralButton
        style={styleGeneral}
        label={labelGeneral}
        onClick={buttonFunction2}
      />
    </div>
  );
}
