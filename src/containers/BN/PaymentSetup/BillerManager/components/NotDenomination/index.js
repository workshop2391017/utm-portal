import React from "react";

export default function NotDenomination() {
  return (
    <div
      style={{
        width: "100%",
        height: "65px",
        padding: "20px",
        gap: "10px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <h1
        style={{
          width: "920px",
          height: "25px",
          fontFamily: "FuturaMdBT",
          fontWeight: 400,
          fontSize: "21px",
          lineHeight: "25px",
          textAlign: "center",
          letterSpacing: "0.01em",
          color: "#7B87AF",
        }}
      >
        You Don&apos;t Enable Denomination
      </h1>
    </div>
  );
}
