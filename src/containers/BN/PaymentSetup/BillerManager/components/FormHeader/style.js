import { makeStyles } from "@material-ui/core";

export const buttonStyle = {
  backgroundColor: "#F4F7FB",
  fontWeight: 700,
  fontSize: "13px",
  letterSpacing: "0.03em",
  boxShadow: "none",
  color: "#0061A7",
};

export const useStyles = makeStyles({
  container: {
    width: "100%",
    height: "76px",
    padding: "0px 40px 0px 20px",
    marginBottom: "20px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "flex-start",
  },
  h1: {
    width: "auto",
    height: "24px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    fontSize: "20px",
    lineHeight: "24px",
    letterSpacing: "0.03em",
    color: "#374062",
  },
});
