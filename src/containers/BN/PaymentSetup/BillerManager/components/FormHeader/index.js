import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";

import ArrowLeft from "assets/icons/BN/buttonkembali.svg";

import { useStyles, buttonStyle } from "./style";

export default function FormHeader({ headerText }) {
  const history = useHistory();
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Button
        style={buttonStyle}
        startIcon={<img src={ArrowLeft} alt="Kembali" />}
        onClick={() => history.goBack()}
      />
      <h1 className={classes.h1}>{headerText}</h1>
    </div>
  );
}
