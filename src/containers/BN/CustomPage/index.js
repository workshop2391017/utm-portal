import { useState } from "react";
import { TextField, Button, Typography, makeStyles } from "@material-ui/core";

import CustomComponent from "components/BN/CustomComponent";

const CustomPage = () => {
  const classes = useStyles();

  const [namaDepan, setNamaDepan] = useState("");
  const [namaBelakang, setNamaBelakang] = useState("");
  const [hobi, setHobi] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const onChangeText = (event, type) => {
    switch (type) {
      case "nama-depan":
        setNamaDepan(event.target.value);
      case "nama-belakang":
        setNamaBelakang(event.target.value);
      case "hobi":
        setHobi(event.target.value);
    }
  };

  const onClickButton = () => {
    console.log("nama depan: ", namaDepan);
    console.log("nama belakang: ", namaBelakang);
    console.log("hobi: ", hobi);
    setIsOpen(true);
  };

  const handleClosePopup = () => {
    setIsOpen(false);
  };

  return (
    <div className={classes.container}>
      <div className={classes.content}>
        <TextField
          id="outlined-basic"
          label="Nama Depan"
          variant="outlined"
          onChange={(e) => onChangeText(e, "nama-depan")}
        />
        <TextField
          id="outlined-basic"
          label="Nama Belakang"
          inputMode="numeric"
          variant="outlined"
          onChange={(e) => onChangeText(e, "nama-belakang")}
        />
        <TextField
          id="outlined-basic"
          label="Hobi"
          variant="outlined"
          onChange={(e) => onChangeText(e, "hobi")}
        />

        <Button color="primary" variant="contained" onClick={onClickButton}>
          <Typography>Simpan</Typography>
        </Button>
      </div>
      <CustomComponent
        namaDepan={namaDepan}
        namaBelakang={namaBelakang}
        hobi={hobi}
        isOpen={isOpen}
        handleClose={handleClosePopup}
      />
    </div>
  );
};

export default CustomPage;

const useStyles = makeStyles({
  container: {
    padding: "20px",
    margin: "auto",
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    width: "400px",
    padding: "30px",
    backgroundColor: "#ffffff",
    display: "flex",
    flexDirection: "column",
    gap: "15px",
  },
});
