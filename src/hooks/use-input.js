import { useState } from "react";

const useInput = (validate, format) => {
  const [enteredValue, setEnteredValue] = useState("");
  const [isTouched, setIsTouched] = useState(false);

  let isValid;
  let isError;

  if (validate) {
    isValid = validate(enteredValue);
    isError = !isValid && isTouched;
  }

  const valueChangeHandler = (event) => {
    // for populating form inputs
    if (typeof event === "string") {
      setEnteredValue(event);
      return;
    }

    if (format) {
      setEnteredValue(format(event.target.value));
    } else {
      setEnteredValue(event.target.value);
    }
  };

  const inputBlurHandler = () => setIsTouched(true);

  const resetHandler = () => {
    setEnteredValue("");
    setIsTouched(false);
  };

  return {
    value: enteredValue,
    isValid,
    isError,
    valueChangeHandler,
    inputBlurHandler,
    resetHandler,
  };
};

export default useInput;
