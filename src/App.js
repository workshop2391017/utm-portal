import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import Route from "./router";
import { BrowserRouter } from "react-router-dom";
import UserService from "services/UserService";
import { Provider } from "react-redux";
import store from "./stores/store";

const App = () => {
  const renderApp = () => {
    return <Route />;
  };
  useEffect(() => {
    const initializeApp = async () => {
      await UserService.initKeycloak(renderApp);
    };

    initializeApp();
  }, []); // Empty dependency array ensures this effect runs only once on mount

  return <Route />; // or any loading indicator
};

export default App;
