import { makeStyles } from "@material-ui/styles";
import React from "react";
import { ReactComponent as SVGUser } from "assets/icons/BN/welcomeuser.svg";
import { getLocalStorage } from "utils/helpers";

const useStyle = makeStyles({
  noAccess: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: 552,
    backgroundColor: "white",
    fontFamily: "FuturaBkBT",
    fontSize: 15,
    margin: 20,
    borderRadius: 20,
  },
});

const NoAccess = () => {
  const userLogin = getLocalStorage("portalUserLogin");

  const classes = useStyle();

  const username = userLogin ? JSON.parse(userLogin).username : "";

  return (
    <div className={classes.noAccess}>
      <div style={{ textAlign: "center" }}>
        <SVGUser />
        <div style={{ marginTop: 20 }}>
          Welcome ({username}) to Bank Back Office
        </div>
      </div>
    </div>
  );
};
export default NoAccess;
