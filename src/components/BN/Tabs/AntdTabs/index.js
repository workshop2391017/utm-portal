// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Tabs } from "antd";

const AntdTabs = (props) => {
  const { tabPosition } = props;
  const useStyles = makeStyles({
    tabs: {
      fontFamily: "FuturaMdBT",
      "& .ant-tabs-bar": {
        "& .ant-tabs-nav-container": {
          "& .ant-tabs-nav-wrap": {
            "& .ant-tabs-nav-scroll": {
              textAlign: "center",
              color: "#BCC8E7",
              fontFamily: "Futura",
              fontSize: 15,
            },
          },
        },
      },
      "& .ant-tabs-nav-wrap": {
        justifyContent: tabPosition === "left" ? "flex-left" : "center",
      },
      "& .ant-tabs-nav::before": {
        borderBottom: "none !important",
      },
      "& .ant-tabs-nav": {
        "& .ant-tabs-tab-active": {
          color: "#374062",
        },
        "& .ant-tabs-tab": {
          color: "#BCC8E7",
          "& .ant-tabs-tab-btn": {
            fontFamily: "FuturaMdBT",
            fontSize: 13,
          },
        },
        "& .ant-tabs-nav-more": {
          display: "none",
        },
      },
    },
  });
  const classes = useStyles();
  const { activeKey, onChange, tabs, style, value } = props;

  return (
    <Tabs
      activeKey={activeKey}
      onChange={onChange}
      className={classes.tabs}
      style={style}
    >
      {tabs.map((item, i) => (
        <Tabs.TabPane tab={item} key={(i + 1).toString()} />
      ))}
    </Tabs>
  );
};

AntdTabs.propTypes = {
  value: PropTypes.string,
  tabs: PropTypes.array,
  onChange: PropTypes.func,
  tabPosition: PropTypes.string,
};

AntdTabs.defaultProps = {
  value: "",
  tabs: [],
  tabPosition: "center",
  onChange: () => console.warn("AntdTabs"),
};

export default AntdTabs;
