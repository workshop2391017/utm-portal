/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

const TabPanel = (props) => {
  const { children, value, currentValue, ...other } = props;
  console.warn("value", value);
  console.warn("currentValue", currentValue);

  return (
    <div
      role="tabpanel"
      hidden={value !== currentValue}
      id={`simple-tabpanel-${currentValue}`}
      aria-labelledby={`simple-tab-${currentValue}`}
      {...other}
    >
      {value === currentValue && <React.Fragment>{children}</React.Fragment>}
    </div>
  );
};

export default TabPanel;
