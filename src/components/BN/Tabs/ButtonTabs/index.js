// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Tabs, Tab } from "@material-ui/core";

const GeneralTabs = (props) => {
  const { value, onChange, tabs, style } = props;
  const useStyles = makeStyles({
    rootTabs: {
      minHeight: 30,
      borderRadius: 8,
      height: 30,
      color: "#fff",
      width: "fit-content",
      position: "relative",
    },
    rootItemTabs: {
      minHeight: 30,
      minWidth: 77,
      padding: "0px 10px",
      borderRadius: 8,
      marginRight: 10,
      backgroundColor: "#F4F7FB",
      "& .MuiTouchRipple-root": {
        color: "#BCC8E7",
      },
    },
    selectedTabItem: {
      backgroundColor: "#0061A7",
      "& .MuiTab-wrapper": {
        color: "#fff",
      },
      "& .MuiTouchRipple-root": {
        color: "#fff",
      },
    },
    wrapperTabItem: {
      textTransform: "none",
      fontSize: 13,
      color: "#BCC8E7",
    },
    tabsIndicator: {
      display: "none",
    },
  });
  const classes = useStyles();

  // tabs styles
  const tabsStyles = {
    root: classes.rootTabs,
    indicator: classes.tabsIndicator,
  };
  const tabItemStyles = {
    root: classes.rootItemTabs,
    selected: classes.selectedTabItem,
    wrapper: classes.wrapperTabItem,
  };

  return (
    <Tabs
      classes={tabsStyles}
      value={value}
      onChange={onChange}
      style={style}
      centered
    >
      {tabs.map((item) => (
        <Tab classes={tabItemStyles} label={item} />
      ))}
    </Tabs>
  );
};

GeneralTabs.propTypes = {
  value: PropTypes.string,
  tabs: PropTypes.array,
  onChange: PropTypes.func,
};

GeneralTabs.defaultProps = {
  value: "",
  tabs: [],
  onChange: () => console.warn("GeneralTabs"),
};

export default GeneralTabs;
