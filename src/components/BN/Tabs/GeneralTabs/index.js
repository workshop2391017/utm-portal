// main
import React, { Fragment } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Tabs, Tab } from "@material-ui/core";
import Colors from "helpers/colors";

const GeneralTabs = (props) => {
  const { value, onChange, tabs, style, tabWidth, disabled, feature } = props;
  const useStyles = makeStyles((theme) => ({
    rootTabs: {
      minHeight: 40,
      backgroundColor: "#F4F7FB",
      borderRadius: 8,
      height: 40,
      color: "#BCC8E7",
      width: "fit-content",
      position: "relative",

      "& svg path": {
        fill: "#BCC8E7",
      },
      "& button": {
        border: "none !important",
        height: "40px",
        fontFamily: "FuturaMdBT",
        textTransform: "capitalize",
        color: "#B3C1E7",
        latterSpacing: "3%",
        fontSize: 13,
      },
    },
    tabsIndicator: {
      display: "none",
    },
    rootItemTabs: {
      minHeight: "30px",
      display: "flex",
      minWidth: 77,
      ...(tabWidth && { width: tabWidth }),
      [theme.breakpoints.up("md")]: {
        minWidth: 60,
      },
      [theme.breakpoints.up("lg")]: {
        minWidth: 70,
      },
      [theme.breakpoints.up("xl")]: {
        minWidth: 77,
      },
      padding: "7px 10px !important",
    },
    selectedTabItem: {
      backgroundColor: "#0061A7",
      color: "#ffff !important",
      height: 40,
      "& svg path": {
        fill: Colors.white,
      },
      "& button": {
        backgroundColor: "#0061A7",
        color: "#ffff !important",
      },
    },
    wrapperTabItem: {
      textTransform: "none",
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 13,
      display: "flex",
      flexDirection: "row",
      gap: 10,
    },
  }));
  const classes = useStyles();

  // tabs styles
  const tabsStyles = {
    root: classes.rootTabs,
    indicator: classes.tabsIndicator,
  };
  const tabItemStyles = {
    root: classes.rootItemTabs,
    selected: classes.selectedTabItem,
    wrapper: classes.wrapperTabItem,
  };

  return (
    <Tabs
      classes={tabsStyles}
      value={value}
      onChange={onChange}
      style={style}
      centered
    >
      {tabs.map((item, i) =>
        typeof item === "string" ? (
          <Tab
            classes={tabItemStyles}
            className={
              i === value && feature ? "tabs-forcalendar-selected" : ""
            }
            label={item}
          />
        ) : (
          <Tab
            classes={tabItemStyles}
            icon={<item.icon /> ?? item.icon}
            label={item.label}
            lassName={i === value && feature ? "tabs-forcalendar-selected" : ""}
          />
        )
      )}
    </Tabs>
  );
};

GeneralTabs.propTypes = {
  value: PropTypes.string,
  tabs: PropTypes.array,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

GeneralTabs.defaultProps = {
  value: "",
  disabled: false,
  tabs: [],
  onChange: () => console.warn("GeneralTabs"),
};

export default GeneralTabs;
