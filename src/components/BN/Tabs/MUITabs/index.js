// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { withStyles, Tabs, Tab } from "@material-ui/core";

const ContentTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      width: "100%",
      backgroundColor: "#0061A7",
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const ContentTab = withStyles((theme) => ({
  root: {
    textTransform: "none",
    fontSize: 15,
    marginRight: theme.spacing(1),
    color: "#BCC8E7",
    fontFamily: "Futura",
    minWidth: 110,
    minHeight: 50,
    "&$selected": {
      color: "#374062",
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const MUITabs = (props) => {
  const { value, onChange, tabs, style } = props;

  const a11yProps = (index) => ({
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  });

  return (
    <ContentTabs value={value} onChange={onChange} aria-label="MUITabs">
      {tabs.map((item, i) => (
        <ContentTab label={item} {...a11yProps(i)} />
      ))}
    </ContentTabs>
  );
};

MUITabs.propTypes = {
  value: PropTypes.string,
  tabs: PropTypes.array,
  onChange: PropTypes.func,
};

MUITabs.defaultProps = {
  value: "",
  tabs: [],
  onChange: () => console.warn("MUITabs"),
};

export default MUITabs;
