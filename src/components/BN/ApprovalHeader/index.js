import { makeStyles } from "@material-ui/core";
import React from "react";
import Colors from "helpers/colors";
import Badge from "../Badge/BadgeCustom";

const useStyle = makeStyles({
  containerPattern: {
    width: "100%",
    backgroundColor: "white",
    padding: "24px",
    marginBottom: 20,
    borderRadius: 10,
    display: "flex",
    justifyContent: "space-between",
    position: "relative",
    overflow: "hidden",
    "& .title": {
      fontSize: 20,
      fontFamily: "FuturaHvBT",
      color: Colors.dark.hard,
    },
  },
});

export const ApprovalHeader = ({ status, label }) => {
  const classes = useStyle();

  const getStatus = () => {
    const s = status?.toLowerCase();

    if (s?.includes("waiting")) {
      return "blue";
    }

    if (s?.includes("approved")) {
      return "green";
    }

    return "red";
  };

  return (
    <div className={classes.containerPattern}>
      <div className="title">{label}</div>
      {/* <img
        src=""
        alt="pattern"
        style={{ position: "absolute", right: 0, top: 0 }}
      /> */}

      <Badge
        label={status}
        type={getStatus()}
        styleBadge={{ textTransform: "capitalize" }}
        outline
      />
    </div>
  );
};

export default ApprovalHeader;
