// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Checkbox } from "antd";

const CheckboxGroup = (props) => {
  const { value, onChange, options, type, width, margin, style, flex } = props;
  const useStyles = makeStyles({
    checkbox: {
      display: flex ? "flex" : null,
      "& .ant-checkbox-group-item": {
        fontSize: 15,
        width,
        margin,
        color: "#374062",
        "& .ant-checkbox": {
          width: 20,
          height: 20,
          borderRadius: 3,
          "& .ant-checkbox-inner": {
            width: 20,
            height: 20,
            borderRadius: 3,
            border: "1px solid #BCC8E7",
            "&::after": {
              width: 6.5,
              height: 11.17,
              top: "42%",
            },
          },
        },
        "& .ant-checkbox-checked": {
          "& .ant-checkbox-inner": {
            border: "1px solid #0061A7 !important",
          },
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <Checkbox.Group
      value={value}
      onChange={onChange}
      options={options}
      className={classes.checkbox}
    />
  );
};

CheckboxGroup.propTypes = {
  value: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  style: PropTypes.object,
  type: PropTypes.string,
  width: PropTypes.string,
  margin: PropTypes.string,
  flex: PropTypes.bool,
};

CheckboxGroup.defaultProps = {
  options: [],
  type: "checkbox",
  width: "initial",
  margin: "0 30px 0 0",
  style: {},
  flex: false,
};

export default CheckboxGroup;
