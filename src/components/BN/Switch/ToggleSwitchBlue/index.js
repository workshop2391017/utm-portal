// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Switch } from "@material-ui/core";

const GeneralSwitch = ({ value, checked, onChange, disableRadio }) => {
  const useStyles = makeStyles({
    switch: {
      "& .MuiSwitch-switchBase": {
        color: "#BCC8E7",
      },
      "& .MuiSwitch-track": {
        backgroundColor: "#E6EAF3",
      },
      "& .Mui-checked.MuiSwitch-switchBase": {
        color: "#0061A7",
      },
      "& .Mui-checked + .MuiSwitch-track": {
        backgroundColor: "#0061A7",
      },
      "& .MuiSwitch-thumb": {
        boxShadow: "none",
        border: "1px solid #FFFFFF",
      },
      "& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track": {
        opacity: "initial",
      },
    },
  });

  const classes = useStyles();
  return (
    <Switch
      value={value}
      checked={checked}
      onChange={onChange}
      color="primary"
      size="small"
      className={classes.switch}
      disabled={disableRadio}
    />
  );
};

GeneralSwitch.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

GeneralSwitch.defaultProps = {};

export default GeneralSwitch;
