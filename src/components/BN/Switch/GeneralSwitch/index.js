// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Switch } from "@material-ui/core";

const GeneralSwitch = (props) => {
  const { checked, onChange, ...rest } = props;
  const useStyles = makeStyles({
    switch: {
      "& .MuiSwitch-switchBase": {
        color: "#BCC8E7",
      },
      "& .MuiSwitch-track": {
        backgroundColor: "#E6EAF3",
      },
      "& .Mui-checked.MuiSwitch-switchBase": {
        color: "#0061A7",
      },
      "& .Mui-checked + .MuiSwitch-track": {
        backgroundColor: "#EAF2FF",
      },
    },
  });
  const classes = useStyles();

  return (
    <Switch
      checked={checked}
      onChange={onChange}
      color="primary"
      size="small"
      className={classes.switch}
      {...rest}
    />
  );
};

GeneralSwitch.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

GeneralSwitch.defaultProps = {};

export default GeneralSwitch;
