import React from "react";
import { makeStyles, styled } from "@material-ui/core/styles";
import PropsType from "prop-types";
import { ReactComponent as UploadIcon } from "assets/icons/BN/upload.svg";
import { CircularProgress } from "@material-ui/core";

const InputImage = ({
  placeholder,
  width,
  onChange,
  value,
  withProgress,
  error,
  errorMessage,
  id,
  accept,
  percentage,
  disabled,
  isLoading,
}) => {
  const useStyles = makeStyles({
    container: {
      width,
      position: "relative",
    },
    disabled: {
      backgroundColor: "#f4f7fb !important",
      borderColor: "#BCC8E7 !important",
    },
    inputContainer: {
      border: "1px solid #BCC8E7",
      padding: "6px 10px",
      width,
      height: "40px",
      borderRadius: 10,
      overflow: "hidden",
      "& .MuiInputBase-input": {
        borderBottom: 0,
      },
      "&.error": {
        border: "1px solid #D14848",
        "& path": {
          stroke: "#D14848",
        },
      },
    },
    input: {
      display: "none",
    },
    placeholder: {
      fontSize: 15,
      color: "#BCC8E7",
      fontFamily: "FuturaBkBT",
    },
    value: {
      fontSize: 15,
      color: "#0061A7",
      fontFamily: "FuturaBkBT",
    },
    percentage: {
      color: "#374062",
    },
    errorMessage: {
      fontFamily: "FuturaBQ",
      fontSize: 13,
      marginTop: 5,
      color: "#D14848",
    },
  });

  const WrapperPersen = styled("div")(() => ({
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 14,
  }));

  const ContainerGraph = styled("div")(() => ({
    height: "4px",
    width: "100px",
    backgroundColor: "#E6EAF3",
    borderRadius: 10,
    overflow: "hidden",
    position: "relative",
  }));

  const Graph = styled("div")(() => ({
    width: percentage,
    height: "4px",
    background: "#0061A7",
    position: "absolute",
    borderRadius: 10,
    overflow: "hidden",
  }));

  const handleValue = (value) => {
    if (value?.length > 35) {
      return `${value?.slice(0, 35)}...`;
    }

    return value;
  };

  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div
        className={`${classes.inputContainer} ${error ? "error" : ""} ${
          disabled ? classes.disabled : ""
        }`}
      >
        <label htmlFor={id}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              overflow: "hidden",
            }}
          >
            {value ? (
              <div className={classes.value}>{handleValue(value)}</div>
            ) : (
              <div className={classes.placeholder}>
                {error ? "" : placeholder}
              </div>
            )}
            {isLoading ? (
              <CircularProgress size={20} />
            ) : (
              <UploadIcon width={20} height={20} />
            )}

            <input
              disabled={disabled}
              accept={accept}
              className={classes.input}
              id={id}
              type="file"
              onChange={(e) => (disabled ? () => {} : onChange(e))}
            />
          </div>
        </label>
      </div>
      {withProgress ? (
        <WrapperPersen>
          <ContainerGraph>
            <Graph />
          </ContainerGraph>
          <div className={classes.percentage}>{percentage}%</div>
        </WrapperPersen>
      ) : null}
      {error && errorMessage ? (
        <div className={classes.errorMessage}>{errorMessage}</div>
      ) : null}
    </div>
  );
};

InputImage.defaultProps = {
  placeholder: "Upload dokumen anda",
  onChange: () => {},
  value: null,
  width: 297,
  withProgress: false,
  error: false,
  errorMessage: "",
  id: "",
  percentage: 0,
};
InputImage.PropsTypes = {
  placeholder: PropsType.string,
  onChange: PropsType.func,
  value: PropsType.string,
  width: PropsType.number,
  withProgress: PropsType.bool,
  error: PropsType.bool,
  errorMessage: PropsType.string,
  id: PropsType.string,
  accept: PropsType.string.isRequired,
  percentage: PropsType.number,
};
export default InputImage;
