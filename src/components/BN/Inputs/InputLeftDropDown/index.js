import React, { Fragment } from "react";

import { makeStyles } from "@material-ui/core";
import { Input, Select } from "antd";
import styled from "styled-components";

import PropTypes from "prop-types";

const HelperText = styled.span`
  font-family: FuturaBkBT;
  font-weight: 400;
  font-size: 13px;
  letter-spacing: 0.01em;
  color: #d14848;
`;

const useStyles = makeStyles({
  input: {
    borderRadius: "10px",
    height: 40,
    overflow: "hidden",
    "& .ant-input": {
      borderRadius: "10px",
      border: "1px solid #BCC8E7",
      height: 40,
    },
  },

  select: {
    height: 40,
    color: "white !important",
    "& .ant-select-arrow": {
      color: "#fff ",
    },
    "& .ant-select-selector": {
      height: "40px !important",
      backgroundColor: "#0061A7 !important",
      display: "flex",
      alignItems: "center",
      color: "white !important",
    },
    "& .ant-input-wrapper ant-input-group": {
      borderTopLeftRadius: "10px",
      borderBottomLeftRadius: "10px",
    },
  },
});

const { Option } = Select;

export default function InputLeftDropDown(props) {
  const {
    fieldIndex,
    selected,
    placeholder,
    value,
    style,
    disabled,
    onChangeSelect,
    onChangeInput,
    defaultValue,
    helperText,
    isEdit,
  } = props;

  const classes = useStyles();

  const operators = ["<", ">", "<=", ">="];

  const selectBefore = (
    <Select
      disabled={disabled}
      value={fieldIndex === 0 || !selected ? defaultValue : selected}
      onChange={onChangeSelect}
      className={classes.select}
    >
      {fieldIndex === 0 &&
        operators?.map((operator, index) => (
          <Fragment key={index}>
            <Option value={operator}>{operator}</Option>
          </Fragment>
        ))}
      {fieldIndex > 0 && (
        <Option value={selected ?? defaultValue}>
          {selected ?? defaultValue}
        </Option>
      )}
    </Select>
  );

  return (
    <Fragment>
      <Input
        disabled={disabled}
        value={value}
        placeholder={placeholder}
        onChange={onChangeInput}
        className={classes.input}
        addonBefore={selectBefore}
        style={style}
      />
      {helperText && (
        <HelperText
          style={{
            position: isEdit ? "absolute" : undefined,
            top: isEdit ? "95%" : undefined,
          }}
        >
          {helperText}
        </HelperText>
      )}
    </Fragment>
  );
}

InputLeftDropDown.propTypes = {
  value: PropTypes.object,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};
InputLeftDropDown.defaultProps = {
  value: null,
  style: {},
  placeholder: "Placeholder",
  //   onChange: () => console.warn("click onChange"),
};
