// /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
// /* eslint-disable jsx-a11y/click-events-have-key-events */
// // main
// import React from "react";
// import PropTypes from "prop-types";

// // libraries
// import { Input } from "antd";
// import { makeStyles } from "@material-ui/core";

// // assets
// import search from "../../../../assets/icons/BN/search.svg";

// // Jss
// const useStyles = makeStyles({
//   input: {
//     fontFamily: "FuturaBkBT",
//     border: "1px solid #BCC8E7",
//     borderRadius: 5,
//     width: 320,
//     height: 40,
//     color: "#BCC8E7",
//     "&.ant-input-affix-wrapper-focused": {
//       boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
//       border: "1px solid #BCC8E7",
//     },
//     "& .ant-input": {
//       boxShadow: "none",
//       color: "#BCC8E7",
//     },
//     "&:focus": {
//       boxShadow: "0 0 0 2px rgb(87 168 233 / 20%) !important",
//       border: "1px solid #BCC8E7",
//     },
//   },
//   textarea: {
//     border: "1px solid #BCC8E7",
//     borderRadius: 10,
//   },
// });

// const AntdTextField = (props, ref) => {
//   // props
//   const {
//     placeholder,
//     value,
//     onChange,
//     style,
//     onSubmit,
//     textAreaRows,
//     type,
//     disabled,
//     prefix,
//     suffix,
//     className,
//     endAdornment,
//     startAdornment,
//   } = props;
//   // hooks
//   const classes = useStyles();

//   return type === "textArea" ? (
//     <Input.TextArea
//       rows={textAreaRows}
//       placeholder={placeholder}
//       value={value}
//       ref={ref}
//       onChange={onChange}
//       prefix={prefix}
//       style={{
//         ...style,
//       }}
//       disabled={disabled}
//       className={
//         className ? `${className} ${classes.textarea}` : classes.textarea
//       }
//     />
//   ) : type === "password-with-eye" ? (
//     <Input.Password
//       placeholder={placeholder}
//       value={value}
//       ref={ref}
//       onChange={onChange}
//       prefix={prefix}
//       style={{
//         height: 40,
//         ...style,
//       }}
//       className={className ? `${className} ${classes.input}` : classes.input}
//       disabled={disabled}
//     />
//   ) : type === "password" ? (
//     <Input
//       type="password"
//       placeholder={placeholder}
//       value={value}
//       ref={ref}
//       onChange={onChange}
//       prefix={prefix}
//       suffix={suffix}
//       style={{
//         height: 40,
//         ...style,
//       }}
//       className={className ? `${className} ${classes.input}` : classes.input}
//       disabled={disabled}
//     />
//   ) : type === "search" ? (
//     <Input
//       placeholder={placeholder}
//       value={value}
//       ref={ref}
//       onChange={onChange}
//       onPressEnter={onSubmit}
//       prefix={prefix}
//       style={{
//         height: 40,
//         ...style,
//       }}
//       suffix={
//         <img
//           src={search}
//           alt="search"
//           style={{ cursor: "pointer" }}
//           onClick={onSubmit}
//         />
//       }
//       className={className ? `${className} ${classes.input}` : classes.input}
//       disabled={disabled}
//     />
//   ) : (
//     <Input
//       placeholder={placeholder}
//       value={value}
//       onChange={onChange}
//       style={{
//         height: 40,
//         ...style,
//       }}
//       ref={ref}
//       disabled={disabled}
//       prefix={prefix}
//       suffix={suffix}
//       className={className ? `${className} ${classes.input}` : classes.input}
//       endAdornment={endAdornment}
//       startAdornment={startAdornment}
//     />
//   );
// };

// AntdTextField.propTypes = {
//   placeholder: PropTypes.string,
//   value: PropTypes.string,
//   onChange: PropTypes.func,
//   onSubmit: PropTypes.func,
//   type: PropTypes.string,
//   textAreaRows: PropTypes.number,
//   disabled: PropTypes.bool,
//   prefix: PropTypes.element,
//   suffix: PropTypes.element,
//   className: PropTypes.string,
//   endAdornment: PropTypes.node,
//   startAdornment: PropTypes.node,
//   style: PropTypes.object,
//   // onClickSearch: PropTypes.func,
// };

// AntdTextField.defaultProps = {
//   placeholder: "",
//   value: "",
//   onChange: () => console.warn("AntdTextField"),
//   onSubmit: () => console.warn("AntdTextField"),
//   type: "input",
//   textAreaRows: 6,
//   disabled: false,
//   prefix: null,
//   suffix: null,
//   className: "",
//   endAdornment: "",
//   startAdornment: "",
//   style: {},
//   // onClickSearch: () => console.warn('click search'),
// };

// export default React.forwardRef(AntdTextField);
