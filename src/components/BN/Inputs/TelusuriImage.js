import React from "react";
import { makeStyles, styled } from "@material-ui/core/styles";
import PropsType from "prop-types";
import { ReactComponent as UploadIcon } from "assets/icons/BN/upload.svg";
import Colors from "helpers/colors";

const TelusuriImage = ({
  placeholder,
  width,
  onChange,
  value,
  withProgress,
  error,
  errorMessage,
  id,
  accept,
  percentage,
}) => {
  const useStyles = makeStyles({
    container: {
      width,
      position: "relative",
    },
    inputContainer: {
      overflow: "hidden",
      width,
      borderRadius: 10,
      "& .MuiInputBase-input": {
        borderBottom: 0,
      },
      "&.error": {
        border: "1px solid #D14848",
        "& path": {
          stroke: "#D14848",
        },
      },
    },
    input: {
      display: "none",
    },
    placeholder: {
      fontSize: 15,
      color: "#BCC8E7",
      fontFamily: "FuturaBkBT",
    },
    value: {
      fontSize: 15,
      color: "#0061A7",
      fontFamily: "FuturaBkBT",
    },
    percentage: {
      color: "#374062",
    },
    errorMessage: {
      fontFamily: "FuturaBQ",
      fontSize: 13,
      marginTop: 5,
      color: "#D14848",
    },
    whiteSpace: {
      padding: "11px 10px",
      border: "1px solid #BCC8E7",
      width: "100%",
      height: 45,
      borderTopLeftRadius: 10,
      borderBottomLeftRadius: 10,
    },
    buttonUpload: {
      backgroundColor: Colors.primary.hard,
      color: "white",
      fontFamily: "FuturaMdBT",
      fontSize: 13,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: 110,
    },
  });

  const WrapperPersen = styled("div")(() => ({
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 14,
  }));

  const ContainerGraph = styled("div")(() => ({
    height: "4px",
    width: "100px",
    backgroundColor: "#E6EAF3",
    borderRadius: 10,
    overflow: "hidden",
    position: "relative",
  }));

  const Graph = styled("div")(() => ({
    width: percentage,
    height: "4px",
    background: "#0061A7",
    position: "absolute",
    borderRadius: 10,
    overflow: "hidden",
  }));

  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={`${classes.inputContainer} ${error ? "error" : ""}`}>
        <label htmlFor={id}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div className={classes.whiteSpace}>
              {value ? (
                <div className={classes.value}>{value}</div>
              ) : (
                <div className={classes.placeholder}>
                  {error ? "" : placeholder}
                </div>
              )}
            </div>
            <div className={classes.buttonUpload}>Browse</div>

            <input
              accept={accept}
              className={classes.input}
              id={id}
              type="file"
              onChange={onChange}
            />
          </div>
        </label>
      </div>
      {withProgress ? (
        <WrapperPersen>
          <ContainerGraph>
            <Graph />
          </ContainerGraph>
          <div className={classes.percentage}>{percentage}%</div>
        </WrapperPersen>
      ) : null}
      {error && errorMessage ? (
        <div className={classes.errorMessage}>{errorMessage}</div>
      ) : null}
    </div>
  );
};

TelusuriImage.defaultProps = {
  placeholder: "Upload dokumen anda",
  onChange: () => {},
  value: null,
  width: 297,
  withProgress: false,
  error: false,
  errorMessage: "",
  id: "",
  percentage: 0,
};
TelusuriImage.PropsTypes = {
  placeholder: PropsType.string,
  onChange: PropsType.func,
  value: PropsType.string,
  width: PropsType.number,
  withProgress: PropsType.bool,
  error: PropsType.bool,
  errorMessage: PropsType.string,
  id: PropsType.string,
  accept: PropsType.string.isRequired,
  percentage: PropsType.number,
};
export default TelusuriImage;
