import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import PropsType from "prop-types";
import { ReactComponent as UploadIcon } from "assets/icons/BN/upload.svg";

const InputImage = ({ placeholder, width, onChange, value, pdf }) => {
  const useStyles = makeStyles({
    inputContainer: {
      border: "1px solid #BCC8E7",
      padding: "11px 10px",
      width,
      borderRadius: 10,
      "& .MuiInputBase-input": {
        borderBottom: 0,
      },
    },
    input: {
      display: "none",
    },
    placeholder: {
      fontSize: 15,
      color: "#BCC8E7",
      fontFamily: "FuturaBkBT",
    },
    value: {
      fontSize: 15,
      color: "#0061A7",
      fontFamily: "FuturaBkBT",
    },
  });

  const classes = useStyles();
  return (
    <div className={classes.inputContainer}>
      <label htmlFor="upload">
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          {value ? (
            <div className={classes.value}>{value}</div>
          ) : (
            <div className={classes.placeholder}>{placeholder}</div>
          )}
          <UploadIcon />
          <input
            className={classes.input}
            id="upload"
            label="Standard"
            type="file"
            onChange={onChange}
            accept=".pdf"
          />
        </div>
      </label>
    </div>
  );
};

InputImage.defaultProps = {
  placeholder: "Upload dokumen anda",
  onChange: () => {},
  value: null,
  width: 297,
};
InputImage.PropsTypes = {
  placeholder: PropsType.string,
  onChange: PropsType.func,
  value: PropsType.string,
  width: PropsType.number,
};
export default InputImage;
