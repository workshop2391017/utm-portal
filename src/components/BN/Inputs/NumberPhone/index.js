// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { DatePicker, Input, InputNumber } from "antd";
// import { TextField } from "../../TextField/AntdTextField";

// assets
import plus62 from "../../../../assets/icons/BN/p62.svg";
// import { Input } from "antd";

const NumberPhone = (props) => {
  const { value, onChange, style, placeholder } = props;
  const useStyles = makeStyles({
    datePicker: {
      height: 40,
      width: 195,
      marginLeft: "45px",
      borderRadius: 10,
      border: "1px solid #BCC8E7",
      "&:hover": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #BCC8E7",
        // backgroundColor: "yellow",
      },
      "& .ant-picker-input": {
        cursor: "pointer",
        "& input": {
          color: "#BCC8E7",

          // backgroundColor: "red",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "& .ant-picker-suffix": {
          cursor: "pointer",
          // padding: "0px 0px 0px 0px",
          // backgroundColor: "red",
        },
        "& .ant-picker-clear": {
          width: 21,
          textAlign: "center",
          lineHeight: "22px",
          paddingLeft: "5px",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      "& .ant-picker-panel-container": {
        borderRadius: 12,
        padding: 20,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        "& .ant-picker-panel": {
          border: "none",
          "& .ant-picker-date-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& thead": {
                  "& tr th": {
                    color: "#374062",
                  },
                },
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                      },
                    },
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        lineHeight: "28px",
                        minWidth: 28,
                        height: 28,
                        color: "#F2F5FD !important",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-month-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-year-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-decade-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-footer": {
            border: "none",
            "& .ant-picker-today-btn": {
              color: "#06377B",
            },
          },
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <Input
      //   value={value}
      //   onChange={onChange}
      placeholder={placeholder}
      className={classes.datePicker}
      style={
        {
          //       display: 'block',
          //    float: 'left',
        }
      }
      suffixIcon={
        <div
          style={{
            height: "40px",
            width: "52px",
            position: "relative",
            left: "-175px",
            display: "flex",
            // justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            backgroundColor: "#0061A7",
            borderTopLeftRadius: "10px",
            borderBottomLeftRadius: "10px",
            padding: "0px 0px 0px 10px",
          }}
        >
          <img
            src={plus62}
            alt="icon-suffix"
            style={{
              cursor: "pointer",
            }}
          />
        </div>
      }
      //   dropdownClassName={classes.dropdown}
    />
  );
};

NumberPhone.propTypes = {
  value: PropTypes.object,
  //   onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};

NumberPhone.defaultProps = {
  value: null,
  //   onChange: () => console.warn("click onChange"),
  style: null,
  placeholder: "Placeholder",
};

export default NumberPhone;
