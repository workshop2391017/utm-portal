/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from "react";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core";
import AutoComplete from "@material-ui/lab/Autocomplete";

import IconSearch from "../../../assets/icons/BN/search.svg";
// import IconSearch from '@material-ui/icons/Search';

const useStyles = makeStyles({
  root: {},
  textFieldRoot: {
    width: 190,
    height: 40,
    top: 4,
    borderLeft: "none",
    borderRadius: "0px 8px 8px 0px",
  },
  inputRoot: {
    padding: "0px 11px 0px 0px !important",
  },
  inputElement: {
    font: "normal normal normal 14px FuturaBkBT",
    padding: "9.5px 11px !important",
  },
  searchIcon: {
    cursor: "pointer",
  },
});

/* --------------------------------- START --------------------------------- */
export default function MuiAutoComplete({
  onChange,
  onKeyPress,
  options,
  placeholder,
  name,
  onSubmit,
}) {
  const classes = useStyles();

  return (
    <AutoComplete
      className={classes.root}
      classes={{
        inputRoot: classes.inputRoot,
      }}
      onInputChange={onChange}
      onSelect={onChange}
      onKeyUp={onKeyPress}
      options={options}
      freeSolo
      disableClearable
      getOptionLabel={(option) => option.value}
      renderInput={(params) => (
        <TextField
          {...params}
          type="search"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            classes: {
              root: classes.textFieldRoot,
              input: classes.inputElement,
              adornedEnd: classes.searchIcon,
            },
            endAdornment: (
              <img src={IconSearch} alt="icon-search" onClick={onSubmit} />
            ),
            name,
          }}
          placeholder={placeholder}
        />
      )}
    />
  );
}
