// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

const Badge = ({
  type,
  label,
  styleBadge,
  outline,
  className,
  fitWidth,
  padding,
}) => {
  const useStyles = makeStyles({
    badge: {
      "&:hover": {
        cursor: "pointer",
      },
      padding: "4px 8px !important",
      fontFamily: "FuturaBkBT",
      fontSize: "12px",
      position: "relative",
      margnin: 0,
      left: 0,
      borderRadius: "8px",
      ...styleBadge,
      ...(type === "red"
        ? {
            color: "#D14848",
            backgroundColor: "#FFE4E4",
            ...(outline && { border: '1px solid  "#D14848"' }),
          }
        : type === "redoutlined"
        ? {
            color: "#D14848",
            backgroundColor: "#FFE4E4",
            ...(outline && { border: "1px solid #D14848" }),
            padding: "3px 7px !important",
          }
        : type === "blue"
        ? {
            color: "#66A3FF",
            backgroundColor: "#EAF2FF",
            border: "1px solid #66A3FF",
            ...(padding && { padding: "3px 7px " }),
            ...(outline && { border: "1px solid  #66A3FF" }),
          }
        : type === "orange"
        ? {
            color: "#FFA24B",
            backgroundColor: "#FFF2E3",
            ...(outline && { border: "1px solid  #FFA24B" }),
          }
        : type === "purple"
        ? {
            color: "#575FFF",
            backgroundColor: "#E3E4FF",
            ...(outline && { border: "1px solid  #575FFF" }),
          }
        : type === "green"
        ? {
            color: "#75D37F",
            backgroundColor: "#E7FFDC",
            ...(outline && { border: "1px solid  #75D37F" }),
          }
        : type === "primary"
        ? {
            color: "white",
            backgroundColor: "#0061A7",
          }
        : type === "pink"
        ? {
            color: "#D375AE",
            backgroundColor: "#FCDCFF",
            border: "1px solid #D375AE",
            padding: "3px 7px !important",
            ...(outline && { border: "1px solid  #D375AE" }),
          }
        : {
            color: "#7B87AF",
            backgroundColor: "#F4F7FB",
            ...(outline && { border: "1px solid  #75D37F" }),
          }),
    },
  });
  const classes = useStyles();

  return (
    <div>
      <div
        color="secondary"
        className={className ? `${classes.badge} ${className}` : classes.badge}
        style={{
          padding: 0,
          fontFamily: "FuturaBkBT !important",
          width: fitWidth && "100%",
        }}
      >
        {label}
      </div>
    </div>
  );
};

Badge.propTypes = {
  label: PropTypes.string.isRequired,
  styleBadge: PropTypes.object,
  type: PropTypes.oneOf(["blue", "orange", "red", "purple"]).isRequired,
  fitWidth: PropTypes.bool,
};

Badge.defaultProps = {
  styleBadge: {},
  fitWidth: false,
};

export default Badge;
