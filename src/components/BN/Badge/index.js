// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Badge as BadgeMui } from "@material-ui/core";
import Colors from "helpers/colors";

const Badge = ({ type, label, styleBadge, outline, className, fitWidth }) => {
  const useStyles = makeStyles({
    badge: {
      "&:hover": {
        cursor: "pointer",
      },
      padding: "4px 8px !important",
      fontFamily: "FuturaBkBT",
      fontSize: "12px",
      position: "relative",
      margin: 0,
      left: 0,
      borderRadius: "8px",
      display: "flex",
      justifyContent: "center",
      alignContent: "center",
      alignItems: "center",
      ...styleBadge,
      ...(type === "maker"
        ? {
            color: "#575FFF",
            backgroundColor: "#E3E4FF",
            ...(outline && { border: '1px solid  "#575FFF"' }),
          }
        : type === "assigner"
        ? {
            color: "#E28820",
            backgroundColor: "#FFF2E3",
            ...(outline && { border: '1px solid  "#E28820"' }),
          }
        : type === "admin"
        ? {
            color: "#EF1515",
            backgroundColor: "#FFE3E3",
            ...(outline && { border: '1px solid  "#EF1515"' }),
          }
        : type === "singleOreintation"
        ? {
            color: "#6880A1",
            backgroundColor: "#E3E8EF",
            ...(outline && { border: '1px solid  "#6880A1"' }),
          }
        : type === "checker"
        ? {
            color: "#02A539",
            backgroundColor: "#CAFFDC",
            ...(outline && { border: '1px solid  "#02A539"' }),
          }
        : type === "approver"
        ? {
            color: "#1B82BD",
            backgroundColor: "#D3EFFF",
            ...(outline && { border: '1px solid  "#1B82BD"' }),
          }
        : type === "red"
        ? {
            color: "#D14848",
            backgroundColor: "#FFE4E4",
            ...(outline && { border: "1px solid  #D14848" }),
          }
        : type === "redoutlined"
        ? {
            color: "#D14848",
            backgroundColor: "#FFE4E4",
            ...(outline && { border: "1px solid #D14848" }),
            padding: "3px 7px !important",
          }
        : type === "primary"
        ? {
            color: "#0061A7",
            backgroundColor: "#EAF2FF",
            ...(outline && { border: "1px solid #0061A7" }),
            padding: "3px 7px !important",
            fontFamily: "FuturaMdBT",
          }
        : type === "blue"
        ? {
            color: "#66A3FF",
            backgroundColor: "#EAF2FF",
            border: "1px solid #66A3FF",
            padding: "3px 7px !important",
            ...(outline && { border: "1px solid  #66A3FF" }),
          }
        : type === "orange"
        ? {
            color: "#FFA24B",
            backgroundColor: "#FFF2E3",
            ...(outline && { border: "1px solid  #FFA24B" }),
          }
        : type === "purple"
        ? {
            color: "#575FFF",
            backgroundColor: "#E3E4FF",
            ...(outline && { border: "1px solid  #575FFF" }),
          }
        : type === "purpleTextDark"
        ? {
            color: "#7B87AF",
            backgroundColor: "#E3E4FF",
            ...(outline && { border: "1px solid  #575FFF" }),
          }
        : type === "green"
        ? {
            color: "#75D37F",
            backgroundColor: "#E7FFDC",
            ...(outline && { border: "1px solid  #75D37F" }),
          }
        : type === "danger"
        ? {
            color: "#EF1515",
            backgroundColor: "#FFE3E3",
            ...(outline && { border: "1px solid  #75D37F" }),
          }
        : type === "primary"
        ? {
            color: "white",
            backgroundColor: "#0061A7",
          }
        : type === "bluesolid"
        ? {
            color: "white",
            backgroundColor: Colors.primary.hard,
          }
        : type === "pink"
        ? {
            color: "#D375AE",
            backgroundColor: "#FCDCFF",
            border: "1px solid #D375AE",
            padding: "3px 7px !important",
            ...(outline && { border: "1px solid  #D375AE" }),
          }
        : type === "primary-border"
        ? {
            color: "#0061A7",
            backgroundColor: "#FFFFFF",
            ...(outline && { border: "1px solid #0061A7" }),
            padding: "5px 10px !important",
            fontFamily: "FuturaMdBT",
            fontSize: 12,
            fontWeight: 400,
          }
        : {
            color: "#7B87AF",
            backgroundColor: "#F4F7FB",
            ...(outline && { border: "1px solid  #7B87AF" }),
          }),
    },
  });
  const classes = useStyles();

  return (
    <div>
      <div
        color="secondary"
        className={className ? `${classes.badge} ${className}` : classes.badge}
        style={{
          padding: 0,
          fontFamily: "FuturaBkBT !important",
          width: fitWidth && "100%",
        }}
      >
        {label}
      </div>
    </div>
  );
};

Badge.propTypes = {
  label: PropTypes.string.isRequired,
  styleBadge: PropTypes.object,
  fitWidth: PropTypes.bool,
  type: PropTypes.oneOf([
    "blue",
    "orange",
    "red",
    "purple",
    "bluesolid",
    "primary",
    "danger",
    "redoutlined",
  ]).isRequired,
};

Badge.defaultProps = {
  styleBadge: {},
  fitWidth: false,
};

export default Badge;
