import React from "react";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { CircularProgress } from "@material-ui/core";

const ButtonOutlined = ({
  label,
  iconPosition,
  buttonIcon,
  onClick,
  width,
  height,
  className,
  color,
  style,
  disabled,
  loading,
  withHoverColor,
  type,
}) => {
  const useStyles = makeStyles({
    root: {
      fontFamily: "FuturaMdBT",
      fontSize: 15,
      height: "15px",
      padding: "0 20px 3px",
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
      fontWeight: "bold",
      border: `1px solid ${color}`,
      backgroundColor: "#fff",
      textTransform: "none",
      color,
      borderRadius: 8,
      "&.Mui-disabled": {
        border: "1px solid rgba(0, 0, 0, 0.26)",
        boxShadow: "none",
        "& path": {
          stroke: "rgba(0, 0, 0, 0.26)",
        },
      },
      ...(withHoverColor && {
        "&:hover": {
          backgroundColor: "#0061A7",
          color: "white",
          borderColor: "white",
        },
      }),
    },
  });
  const classes = useStyles();

  let button;
  if (iconPosition === "endIcon") {
    button = (
      <Button
        className={className ? `${className} ${classes.root}` : classes.root}
        endIcon={buttonIcon}
        onClick={onClick}
        style={{
          width,
          height,
          ...style,
        }}
        disabled={disabled}
        type={type}
      >
        {loading ? <CircularProgress size={20} /> : label}
      </Button>
    );
  } else {
    button = (
      <Button
        className={className ? `${className} ${classes.root}` : classes.root}
        startIcon={buttonIcon}
        onClick={onClick}
        style={{
          width,
          height,
          ...style,
        }}
        disabled={disabled}
        type={type}
      >
        {loading ? <CircularProgress size={20} /> : label}
      </Button>
    );
  }
  return <React.Fragment>{button}</React.Fragment>;
};

ButtonOutlined.propTypes = {
  label: PropTypes.string,
  iconPosition: PropTypes.string, // startIcon or endIcon
  buttonIcon: PropTypes.string,
  style: PropTypes.object,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClick: PropTypes.func,
  className: PropTypes.string,
  color: PropTypes.string,
  loading: PropTypes.bool,
};

ButtonOutlined.defaultProps = {
  label: "Submit",
  // eslint-disable-next-line react/default-props-match-prop-types
  buttonIcon: "",
  iconPosition: "endIcon",
  // eslint-disable-next-line react/default-props-match-prop-types
  width: "116px",
  // eslint-disable-next-line react/default-props-match-prop-types
  height: "40px",
  className: "",
  color: "#0061A7",
  style: {},
  onClick: () => {},
  loading: false,
};

export default ButtonOutlined;
