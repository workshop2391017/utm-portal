// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Select } from "antd";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";
// import search from '../../../../assets/icons/BN/search.svg';

const adjustSize = (options, length, type) => {
  let size = type === "font" ? 13 : 110;
  options.forEach((item) => {
    item.split(" ").forEach(() => {
      if (item.split(" ").length === 2) {
        size = type === "font" ? 12 : 150;
      } else if (item.split(" ").length === 3) {
        size = type === "font" ? 12 : 220;
      }
    });
  });
  return size;
};

// JSS
const useStyles = (props, select) =>
  makeStyles({
    // access props + state
    search: {
      height: "40px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        height: "40px",
        borderRadius: "0 8px 8px 0",
        marginTop: -4,
        border: "1px solid #BCC8E7",
        "&:hover": {
          boxShadow: "none",
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
      "& .ant-select-item-option-active": {
        "&:hover": {
          backgroundColor: "#F4F7FB",
        },
      },
      "& .ant-select-item-option-selected": {
        backgroundColor: "#cfe0e6 !important",
      },
    },
    select: {
      display: "inline-block",
      "& .ant-select": {
        width: adjustSize(props.options, 6, "width"),
        fontFamily: "FuturaMdBT",
        "& .ant-select-selector": {
          borderRadius: "8px",
          border: `1px solid #0061A7`,
          fontWeight: 0,
          color: "#0061A7",
          backgroundColor: "#FFFFFF",
        },
        "& .ant-select-arrow": {
          width: 16,
          height: 16,
          top: "45%",
          transition: "0.3s",
        },
        "&.ant-select-open": {
          "& .ant-select-selector": {
            // boxShadow: '0 0 0 2px rgb(87 168 223 / 20%)'
            boxShadow: "none",
          },
          "& .ant-select-arrow": {
            top: "45%",
            transform: "rotate(-180deg)",
          },
        },
        "&.ant-select-focused": {
          "& .ant-select-selector": {
            // boxShadow: '0 0 0 2px rgb(87 168 223 / 20%)'
            boxShadow: "none",
          },
        },
      },
    },
  });

/* --------------------------------- START --------------------------------- */
const ButtonWithDropdown = (props) => {
  // props & hooks
  const { options, onClick, style } = props;
  const [select, setSelect] = useState(options ? options[0] : "");
  const [input, setInput] = useState("");
  const classes = useStyles(props, select)(); // like function composition call to passing props and state

  // handlers

  return (
    <div className={classes.search}>
      <div className={classes.select}>
        <Select
          style={style}
          size="large"
          value={select}
          onChange={(value) => {
            setSelect(value);
            setInput("");
          }}
          onClick={onClick}
          suffixIcon={<img alt="icon-suffix" src={arrow} />}
          dropdownClassName={classes.dropdown}
        >
          {options.map((item, index) => (
            <Select.Option key={index} value={item}>
              {item}
            </Select.Option>
          ))}
        </Select>
      </div>
    </div>
  );
};

ButtonWithDropdown.propTypes = {
  options: PropTypes.array,
  onClick: PropTypes.func,
};

ButtonWithDropdown.defaultProps = {
  options: [],
  onClick: () => {},
};

export default ButtonWithDropdown;
