import React, { memo } from "react";

import { Tooltip, IconButton, makeStyles } from "@material-ui/core";

import { ReactComponent as InfoCircle } from "assets/icons/BN/info-circle-s3.svg";

import Colors from "helpers/colors";

const useStyles = makeStyles((theme) => ({
  tooltip: {
    ...theme.typography.subtitle1,
    padding: "8px 10px",
    color: Colors.dark.hard,
    backgroundColor: "white",
    textAlign: "center",
    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.14)",
  },
  arrow: {
    color: "white",
  },
  paddingZero: { padding: 0 },
}));

const InfoButton = ({ placement, description }) => {
  const classes = useStyles();

  const placementProp = placement ?? "top";
  const descriptionProp =
    description ??
    "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.";

  return (
    <Tooltip
      arrow
      placement={placementProp}
      title={descriptionProp}
      classes={{
        tooltip: classes.tooltip,
        arrow: classes.arrow,
      }}
    >
      <IconButton className={classes.paddingZero}>
        <InfoCircle />
      </IconButton>
    </Tooltip>
  );
};

export default memo(InfoButton);
