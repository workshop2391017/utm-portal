import React, { useState } from "react";
import { makeStyles, Button, CircularProgress } from "@material-ui/core";
import propTypes from "prop-types";

const useStyles = makeStyles({
  buttonGroup: {
    display: "flex",
    "& .MuiButton-outlinedPrimary.Mui-disabled": {
      border: "1px solid #BCC8E7",
      color: "#BCC8E7",
      backgroundColor: "#fff",
    },
    "& .MuiButton-containedPrimary.Mui-disabled": {
      backgroundColor: "#BCC8E7",
      color: "#fff",
    },
  },
  buttonTolak: {
    fontFamily: "FuturaMdBT",
    borderRadius: "6px !important",
    border: "1px solid #0061A7",
    color: "#0061A7",
    marginRight: "25px",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    "&:hover": {
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
  },
  buttonSetujui: {
    fontFamily: "FuturaMdBT",
    borderRadius: "6px !important",
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    "&:hover": {
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
  },
});

const ButtonGroup = ({
  disabled,
  onCancel,
  onApprove,
  isLoading,
  index,
  activeIndex,
}) => {
  const [active, setActive] = useState(0);

  const classes = useStyles();

  return (
    <div className={classes.buttonGroup}>
      <Button
        color="primary"
        variant="outlined"
        onClick={() => {
          onCancel();
          setActive(0);
        }}
        disabled={disabled || isLoading}
        label="Tolak"
        className={classes.buttonTolak}
      >
        Tolak
      </Button>
      <Button
        onClick={() => {
          onApprove();
          setActive(1);
        }}
        color="primary"
        disabled={disabled || isLoading}
        variant="contained"
        className={classes.buttonSetujui}
      >
        Setuju
      </Button>
    </div>
  );
};

ButtonGroup.propTypes = {
  onCancel: propTypes.func,
  onApprove: propTypes.func,
  disabled: propTypes.bool,
  isLoading: propTypes.bool,
};

ButtonGroup.defaultProps = {
  onCancel: () => {},
  onApprove: () => {},
  disabled: false,
  isLoading: false,
};

export default ButtonGroup;
