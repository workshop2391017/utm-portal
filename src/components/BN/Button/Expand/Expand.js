import React, { memo } from "react";

import { styled, IconButton } from "@material-ui/core";

import { ReactComponent as ExpandMoreIcon } from "assets/icons/BN/arrow-show-more.svg";

const ExpandMore = styled((props) => {
  const { expand, disabled, ...other } = props;
  return <IconButton {...other} />;
})(({ expand, disabled }) => ({
  padding: 0,
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  transition: "300ms ease all",
  backgroundColor: "transparent !important",
  cursor: disabled ? "not-allowed" : undefined,
}));

const ExpandButton = ({ ...other }) => (
  <ExpandMore disableRipple aria-label="show more" {...other.props}>
    <ExpandMoreIcon />
  </ExpandMore>
);

export default memo(ExpandButton);
