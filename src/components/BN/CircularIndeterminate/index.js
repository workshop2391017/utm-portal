import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "80%",
    backgroundColor: "#fff",
    height: "80%",
    margin: "auto",
  },
  container: {
    marginTop: "20%",
    textAlign: "center",
    margin: theme.spacing(10),
  },
}));

export default function CircularIndeterminate() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <CircularProgress />
      </div>
    </div>
  );
}
