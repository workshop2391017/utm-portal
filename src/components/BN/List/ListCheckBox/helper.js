export const check = (data) => {
  const clickCheck = (data?.childs ?? []).filter(({ checked }) => checked);
  return data.childs.length > 0
    ? clickCheck?.length !== data?.childs?.length
    : false;
};

export const isCheckAll = (data) => {
  const clickCheck = (data?.childs ?? []).filter(({ checked }) => checked);
  return data.childs.length > 0
    ? clickCheck?.length === data?.childs?.length
    : false;
};
