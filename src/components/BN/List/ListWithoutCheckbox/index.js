/* eslint-disable space-before-function-paren */
import React, { useState } from "react";
import PropTypes from "prop-types";
import { styled, Box, FormControlLabel, Paper } from "@material-ui/core";

import { ReactComponent as ChervonUpIcon } from "assets/icons/BN/chevron-up-alt.svg";
import { ReactComponent as ChervonDownIcon } from "assets/icons/BN/chevron-down-alt.svg";
import Colors from "helpers/colors";

function Childs({ data, handleChange, parentKey }) {
  console.warn("data di listCheckBox:", data);
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      {data.map((el, childKey) => (
        <div
          style={{
            display: "flex",
            padding: "10px 20px",
            marginLeft: 30,
          }}
          key={`${childKey}---child`}
        >
          <div>{el.label}</div>
        </div>
      ))}
    </Box>
  );
}

Childs.propTypes = {
  data: PropTypes.array,
  handleChange: PropTypes.func,
};

Childs.defaultProps = {
  data: [],
  handleChange: () => {},
};

const DropDown = styled("div")(({ isExpand, sumChild }) => ({
  overflow: "hidden",
  transition: "all ease .5s",
  maxHeight: isExpand ? `${sumChild * 53 + 53}px` : "43px",
}));

const Wrapper = styled("div")(({ forVA }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  // borderBottom: "1px solid #E6EAF3",
  padding: "10px 0",
  cursor: "pointer",
}));
const WrappetClikcable = styled("div")(() => ({
  display: "flex",
  justifyContent: "space-between",
  width: "100%",
}));

function Parent({
  data,
  handleChange,
  handleChangeParent,
  expand,
  forVA,
  parentKey,
}) {
  const [isExpand, setIsExpand] = useState(expand);
  console.warn("dataCheck Parent:", data);
  return (
    <DropDown isExpand={isExpand} sumChild={data?.childs?.length || 0}>
      <Wrapper forVA={forVA}>
        <div
          style={{
            display: "flex",
            width: "100%",
          }}
        >
          <WrappetClikcable onClick={() => setIsExpand(!isExpand)}>
            <div
              style={{
                fontFamily: "FuturaBkBT",
                fontSize: 15,
                fontWeight: "bold",
                color: Colors.dark.soft,
              }}
            >
              {data.label}
            </div>
            <div>{isExpand ? <ChervonUpIcon /> : <ChervonDownIcon />}</div>
          </WrappetClikcable>
        </div>
      </Wrapper>

      {forVA === true ? <Wrapper sx={{ py: 1 }}>Pembayaran</Wrapper> : null}

      <Childs
        data={data.childs}
        parentKey={parentKey}
        handleChange={(child) => handleChange(child, data)}
      />
    </DropDown>
  );
}

Parent.propTypes = {
  data: PropTypes.object,
  handleChange: PropTypes.func,
  handleChangeParent: PropTypes.func,
  expand: PropTypes.bool,
  forVA: PropTypes.bool,
};

Parent.defaultProps = {
  data: [],
  handleChange: () => {},
  handleChangeParent: () => {},
  expand: false,
  forVA: false,
};

function ListCheckBox({
  data,
  handleChange,
  handleChangeParent,
  isExpand,
  forVA,
}) {
  return (
    <div>
      {data.map((el, index) => (
        <Parent
          parentKey={index}
          data={el}
          key={`${index}--parent`}
          handleChange={handleChange}
          handleChangeParent={handleChangeParent}
          expand={isExpand}
          forVA={forVA}
        />
      ))}
    </div>
  );
}

ListCheckBox.propTypes = {
  data: PropTypes.array,
  handleChange: PropTypes.func,
  handleChangeParent: PropTypes.func,
  isExpand: PropTypes.bool,
  forVA: PropTypes.bool,
};

ListCheckBox.defaultProps = {
  data: [],
  handleChange: () => {},
  handleChangeParent: () => {},
  isExpand: false,
  forVA: false,
};

export default ListCheckBox;
