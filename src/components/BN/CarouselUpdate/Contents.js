import React from "react";
import PropTypes from "prop-types";
import { styled } from "@material-ui/styles";
import BlueImg from "assets/images/BN/segmentasi_blue.jpeg";
import RedImg from "assets/images/BN/segmentasi_red.jpeg";
import GeneralButton from "../Button/GeneralButton";

const Content = styled("div")(() => ({
  width: "380px",
  height: "522px",
  display: "flex",
  flexDirection: "column",
  position: "relative",
  boxShadow: "0px 12px 20px 0px #BCC8E733",
  borderRadius: "30px",
}));

const TitleSegmentasi = styled("div")(() => ({
  fontFamily: "FuturaHvBT",
  fontSize: 20,
  color: "#374062",
  marginBottom: 10,
}));

const TitleSegmentasiBg = styled("div")(() => ({
  fontSize: 24,
  fontFamily: "FuturaHvBT",
  fontWeight: 900,
  color: "white",
}));

const CardBG = styled("div")(({ active }) => ({
  backgroundImage: `url( ${active ? BlueImg : RedImg})`,
  backgroundRepeat: "no-repeat",
  objectFit: "cover",
  borderRadius: "18px 18px 0px 0",
  height: "175px",
  marginBottom: "20px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));

const Contents = ({
  active,
  idx,
  data,
  setSelectedSegmantion,
  selectedSegmantion,
}) => {
  const handleNext = async () => {
    setSelectedSegmantion(data);
    // const payload = {
    //   ...responseUpdateStep,
    //   metadata: {
    //     ...responseUpdateStep.metadata,
    //     segmentationId: data?.id,
    //   },
    //   step: 6,
    // };
    // dispatch(updateRegistrationStep(payload, 5));
  };
  return (
    <div>
      <Content active={active} id={idx}>
        <CardBG active={active}>
          <div
            style={{
              position: "absolute",
              color: "white",
              textAlign: "center",
              padding: "42px 26px",
            }}
          >
            <TitleSegmentasiBg style={{ color: "white !important" }}>
              Segmentasis
              {/* {data?.name || ""} */}
            </TitleSegmentasiBg>
            {/* <div> */}
            {/* {data?.description || ""} */}
            {/* </div> */}
          </div>
          {/* <img src={active ? BlueImg : RedImg} alt="img" /> */}
        </CardBG>
        <div style={{ padding: "0px 31px 0px 20px" }}>
          <div style={{ marginBottom: 20 }}>
            <TitleSegmentasi>Limit Segmentasi Nasabah :</TitleSegmentasi>
            {/* <div>{data?.description || ""}</div> */}
          </div>

          {/* <div>
            <TitleSegmentasi>Fitur Segmentasi</TitleSegmentasi>
            <div>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna,
              fringilla dolor elit vulputate vitae nibh. Tellus augue eu a risus
              dolor eu ipsum cras ornare. Purus risus nisi ac,
            </div>
          </div> */}

          <div style={{ display: "flex", justifyContent: "center" }}>
            <GeneralButton
              label="Pilih"
              style={{ width: "251px", marginTop: 32 }}
              disabled={selectedSegmantion?.id === data?.id}
              onClick={() => handleNext()}
            />
          </div>
        </div>
      </Content>
    </div>
  );
};

Contents.propTypes = {
  active: PropTypes.bool,
  idx: PropTypes.number,
  data: PropTypes.object,
  setSelectedSegmantion: PropTypes.func,
  selectedSegmantion: PropTypes.object,
};

Contents.defaultProps = {
  active: false,
  idx: 0,
  data: {},
  setSelectedSegmantion: () => {},
  selectedSegmantion: {},
};

export default Contents;
