import React, { useState } from "react";
import PropTypes from "prop-types";
import Slider from "react-slick";
import { styled } from "@material-ui/styles";

import "./style.scss";
import GeneralButton from "../Button/GeneralButton";

import Contents from "./Contents";

const Container = styled("div")(() => ({
  width: "100%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
}));

const ContentWrapper = styled("div")(() => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "100%",
  // overflow: "hidden",
}));

const CardBG = styled("div")(({ active }) => ({
  width: 378,
  height: 175,
  background: "linear-gradient(4.37deg, #C84E89 3.14%, #F15F79 96.14%)",
  // background: "linear-gradient(180deg, #4F75FF 0%, #345EF5 100%)",
}));
const TitleSegmentasi = styled("div")(() => ({
  fontFamily: "FuturaMdBT",
  fontSize: 15,
  color: "#374062",
  marginBottom: 10,
}));

const CarouselUpdate = ({ onClickNext, title }) => {
  const [current, setCurrent] = useState(0);

  const NextArrow = ({ onClick }) => (
    <GeneralButton
      variant="text"
      size="sm"
      onClick={onClick}
      label=""
      style={{
        position: "absolute",
        top: 0,
        right: "223px",
        height: "100%",
        width: "75px",
        background:
          "linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%)",
      }}
    />
  );

  const PrevArrow = ({ onClick }) => (
    <GeneralButton
      variant="text"
      size="sm"
      onClick={onClick}
      label=""
      style={{
        position: "absolute",
        top: 0,
        left: "223px",
        height: "100%",
        width: "75px",
        zIndex: "999",
        background:
          "linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%)",
      }}
    />
  );

  NextArrow.defaultProps = {
    onClick: () => {},
  };

  NextArrow.propTypes = {
    onClick: PropTypes.func,
  };

  PrevArrow.defaultProps = {
    onClick: () => {},
  };

  PrevArrow.propTypes = {
    onClick: PropTypes.func,
  };

  const setting = {
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    centerMode: true,
    centerPadding: 0,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    beforeChange: (curr, next) => setCurrent(next),
    appendDots: (dots) => <ul style={{ marginTop: -200 }}>{dots}</ul>,
    customPaging: (i) => <div className="dots" style={{ marginTop: -200 }} />,
  };

  return (
    <Container>
      <ContentWrapper>
        <div style={{ width: 1300 }}>
          <Slider {...setting}>
            {[...Array(4)].map((el, idx) => (
              <div className={idx === current ? "slide activeSlide" : "slide"}>
                <Contents active={idx === current} title={title} img={""} />{" "}
              </div>
            ))}
          </Slider>
        </div>
      </ContentWrapper>
    </Container>
  );
};

CarouselUpdate.propTypes = {
  onClickNext: PropTypes.func,
  onClickBack: PropTypes.func,
  title: PropTypes.string,
  img: PropTypes.string,
};

CarouselUpdate.defaultProps = {
  onClickNext: () => {},
  onClickBack: () => {},
  title: "Bank  ",
  img: "",
};

export default CarouselUpdate;
