import React from "react";
import { makeStyles } from "@material-ui/styles";
import { Grid, Typography } from "@material-ui/core";
import { Input, Select, Radio, DatePicker, Button, Upload, Form } from "antd";
import NumberFormat from "react-number-format";
import { FastField } from "formik";
// import {PlusOutlined, MinusOutlined, KeyboardArrowDownIcon } from "@ant-design/icons";

import ButtonOutlined from "../Button/ButtonOutlined";
import SelectWithSearch from "../Select/SelectWithSearch";

const regularStyle = () => ({
  fontSize: "15px",
  lineHeight: "18px",
  padding: 10,
  border: `1px solid #F6FAFF`,
  borderRadius: "6px",
  width: "100%",
});

const useStyles = makeStyles({
  field: {
    ...regularStyle(),
    "&::placeholder": {
      color: "#F6FAFF",
    },
    "& .ant-picker-input input": {
      fontSize: "15px",
      lineHeight: "18px",
      height: 18,
      "&::placeholder": {
        color: "#F6FAFF",
      },
    },
  },
  select: {
    "&.ant-select:not(.ant-select-customize-input) .ant-select-selector": {
      ...regularStyle(),
      height: 40,
      "& .ant-select-selection-item": {
        fontSize: "15px",
        lineHeight: "18px",
      },
    },
  },
  counter: {
    "& .ant-input-wrapper.ant-input-group": {
      ...regularStyle(),
      width: "120px",
      padding: 0,
      "& .ant-input-group-addon": {
        padding: 0,
        border: "none",
        background: "none",
        "& button": {
          border: "none",
          padding: 0,
          boxShadow: "none",
        },
      },
      "& .ant-input": {
        border: "none",
        fontSize: "15px",
        lineHeight: "18px",
        padding: 10,
        textAlign: "center",
      },
    },
  },
  radioItem: {
    padding: "10px, 10px, 10px, 0",
    fontSize: 15,
    lineHeight: "18px",
    "& .ant-radio-checked .ant-radio-inner": {
      borderColor: "#137EE1",
      "&:after": {
        backgroundColor: "#137EE1",
      },
    },
  },
});

const GridItemField = ({
  xs,
  className,
  type,
  title,
  label,
  placeholder,
  placeholderSelect,
  onChange,
  value,
  options,
  minusAction,
  plusAction,
  defaultValue,
  isPhysicalModal,
  disabled,
  helperText,
  style,
  name,
  onSelect,
  error = false,
  errorMessage = false,
  onFocus,
}) => {
  const classes = useStyles();
  const { Option } = Select;
  return (
    <Grid item xs={xs}>
      <div style={{ marginBottom: 5 }}>
        <Typography
          style={{
            fontSize: "13px",
            lineHeight: "16px",
            ...(isPhysicalModal && {
              fontSize: "15px",
              lineHeight: "24px",
              paddingTop: "5px",
              marginBottom: "10px",
            }),
          }}
        >
          {title || label}
        </Typography>
      </div>
      {type === "radio" ? (
        <Radio.Group>
          {options?.map(({ value: val, label }) => (
            <Radio
              value={val}
              className={classes.radioItem}
              disabled={disabled}
              style={{
                ...style,
              }}
            >
              {label}
            </Radio>
          ))}
        </Radio.Group>
      ) : type === "date" ? (
        <DatePicker
          className={classes.field}
          {...{ onChange, defaultValue }}
          format="DD/MM/YYYY"
          disabled={disabled}
        />
      ) : type === "upload" ? (
        <React.Fragment>
          <Upload>
            <ButtonOutlined label="Choose image" />
          </Upload>
          <Typography
            style={{
              fontSize: "13px",
              lineHeight: "16px",
              color: "#137EE1",
              marginTop: 5,
            }}
          >
            {helperText}
          </Typography>
        </React.Fragment>
      ) : type === "selectSearch" ? (
        <SelectWithSearch
          {...{
            options,
            placeholder: placeholderSelect,
            defaultValue,
            value,
            disabled,
            onSelect,
            className,
          }}
        />
      ) : type === "inputAmount" ? (
        <FastField name={name}>
          {({ field }) => (
            <Form.Item
              validateStatus={error ? "error" : "validating"}
              help={errorMessage.length !== 0 ? errorMessage : ""}
            >
              <NumberFormat
                className={classes.field}
                displayType="inputType"
                customInput={Input}
                thousandSeparator="."
                decimalSeparator=","
                fixedDecimalScale
                {...{
                  placeholder,
                  defaultValue,
                  value,
                  disabled,
                  style,
                  className,
                  onChange,
                  type,
                  onFocus,
                }}
                {...field}
              />
            </Form.Item>
          )}
        </FastField>
      ) : (
        <Form.Item
          validateStatus={error ? "error" : "validating"}
          help={errorMessage.length !== 0 ? errorMessage : ""}
        >
          <Input
            className={classes.field}
            type="text"
            {...{
              placeholder,
              defaultValue,
              value,
              disabled,
              style,
              className,
              name,
              onChange,
              type,
              onFocus,
            }}
          />
        </Form.Item>
      )}
    </Grid>
  );
};

GridItemField.defaultProps = {
  type: "text",
};

export default GridItemField;
