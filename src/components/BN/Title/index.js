// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

const Title = (props) => {
  const { label, children, paddingLeft, childSize } = props;
  const useStyles = makeStyles({
    title: {
      width: "100%",
      height: 80,
      backgroundColor: "transparent",
      // boxShadow: 'inset 0px -1px 0px #E6EAF3',
      lineHeight: "80px",
      fontSize: 20,
      paddingLeft,
      fontWeight: 700,
      position: "relative",
      fontFamily: "FuturaHvBT",
      color: "#2B2F3C",
    },
    children: {
      position: "absolute",
      right: 30,
      top: "50%",
      transform: "translateY(-50%)",
      lineHeight: "40px",
      height: 40,
      marginTop: -3,
      display: "flex",
      alignItems: "center",
    },
    md: {
      width: "320px",
    },
    sm: {
      width: "200px",
    },
  });
  const classes = useStyles();

  return (
    <header className={classes.title}>
      {label}
      <div
        className={`${classes.children} ${
          childSize === "sm" ? classes.sm : ""
        } ${childSize === "md" ? classes.md : ""}`}
      >
        {children}
      </div>
    </header>
  );
};

Title.propTypes = {
  label: PropTypes.string,
  paddingLeft: PropTypes.number,
  childSize: PropTypes.string,
};

Title.defaultProps = {
  label: "",
  paddingLeft: 30,
  childSize: "",
};

export default Title;
