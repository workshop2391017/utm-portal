// CustomerSegmentation Component
// --------------------------------------------------------

import { Card, CardContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { ReactComponent as CheckCircle } from "assets/icons/BN/check-circle-green.svg";
import { ReactComponent as EditIcon } from "assets/icons/BN/edit-blue.svg";
import { ReactComponent as UnCheckedCircle } from "assets/icons/BN/uncheck-circle-green.svg";
import GeneralButton from "components/BN/Button/GeneralButton";
import Colors from "helpers/colors";
import React from "react";
import PropTypes from "prop-types";

const SegmentationCard = ({ unchecked, data, onEdit }) => {
  const useStyles = makeStyles({
    sementationCard: {
      width: 300,
      height: 570,
      borderRadius: 10,
      marginBottom: 30,
      overflow: "hidden",
      boxShadow: "none",
      "&:hover": {
        boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
        cursor: "pointer",
      },
      "& .card-header": {
        width: "100%",
        height: 110,
        background:
          "radial-gradient(100% 388.28% at 100% 0%, #0084F1 0%, #0061A7 100%)",
        position: "relative",
        "& .segmentation-bg": {
          position: "absolute",
          right: 0,
        },
        "& .header-content": {
          position: "absolute",
          padding: 16,
          width: "100%",
          height: "100%",
          "& .title": {
            fontSize: 17,
            fontFamily: "FuturaHvBT",
            color: "white",
            marginTop: 16,
          },
        },
      },
    },
    cardContent: {
      padding: "0 25px",
      "& .filed-grop": {
        marginTop: 20,
        "& .title": {
          fontSize: 12,
          fontFamily: "FuturaBkBT",
          color: Colors.dark.medium,
        },
        "& .value": {
          fontFamily: "FuturaHvBT",
          color: Colors.dark.hard,
          fontSize: 15,
        },

        "& .checklist": {
          marginTop: 15,
        },
        "& .checklist-group": {
          display: "flex",
          marginTop: 15,
          "& .checklist-title": {
            marginLeft: 10,
            fontFamily: "FuturaBkBT",
            fontSize: 13,
            color: Colors.dark.hard,
          },
        },
      },
    },
    editbutton: {
      marginTop: 76,
    },
  });
  const classes = useStyles();

  return (
    <Card className={classes.sementationCard}>
      <div className="card-header">
        <div className="header-content">
          <div className="title">{data?.name ?? "-"}</div>
        </div>
      </div>
      <CardContent className={classes.cardContent}>
        <div className="filed-grop">
          <div className="title">Product Account Type</div>
          <div className="value">{data?.jenisProduct?.[0]?.name}</div>
        </div>
        <div className="filed-grop">
          <div className="title">Limit Package</div>
          <div className="value">{data?.limitPackage?.name}</div>
        </div>
        <div className="filed-grop">
          <div className="title">Charge Package</div>
          <div className="value">{data?.chargePackage?.name}</div>
        </div>
        <div className="filed-grop">
          <div className="title">Menu Package</div>
          <div className="value">{data?.menuPackage[0]?.name}</div>
        </div>

        <div className="filed-grop">
          <div className="checklist">
            <div className="checklist-group">
              {(data?.segmentBranch ?? []).length ||
              Object.keys(data?.menuKhusus ?? {}).length ? (
                <CheckCircle />
              ) : (
                <UnCheckedCircle />
              )}
              <div className="checklist-title">Parameter Khusus</div>
            </div>
            <div className="checklist-group">
              {!(data?.parameterAdvance ?? []).length ? (
                <UnCheckedCircle />
              ) : (
                <CheckCircle />
              )}
              <div className="checklist-title">Parameter Advance</div>
            </div>
          </div>
        </div>

        <GeneralButton
          label="Edit Segmentation"
          width={250}
          height={44}
          buttonIcon={<EditIcon />}
          iconPosition="startIcon"
          onClick={onEdit}
          className={classes.editbutton}
        />
      </CardContent>
    </Card>
  );
};

SegmentationCard.propTypes = {
  unchecked: PropTypes.bool,
  onEdit: PropTypes.func,
};

SegmentationCard.defaultProps = {
  unchecked: false,
  onEdit: () => {},
};

export default SegmentationCard;
