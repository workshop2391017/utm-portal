import { Card, makeStyles } from "@material-ui/core";
import GeneralButton from "components/BN/Button/GeneralButton";
import Colors from "helpers/colors";
import React from "react";
import { ReactComponent as IconCheck } from "assets/icons/BN/check.svg";
import PropTypes from "prop-types";

const SegmentationPackageCard = ({
  selected,
  button,
  title,
  subtitle,
  onClick,
  light = false,
}) => {
  const useStyles = makeStyles({
    card: {
      boxShadow: "none",
      "& .produk-card": {
        height: 60,
        padding: 10,
        borderBottom: "1px solid #E8EEFF",
        "&:hover": {
          backgroundColor: Colors.gray.soft,
          cursor: "pointer",
          "& .button-check": {
            display: "block",
          },
        },
        "& .title": {
          fontSize: 13,
          fontFamily: "FuturaHvBT",
        },
        "& .total": {
          fontFamily: "FuturaBQ",
          fontSize: 11,
        },
        "& .button-check": {
          display: "none",
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <Card className={classes.card} onClick={onClick}>
      <div
        className="produk-card"
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          ...(selected
            ? {
                backgroundColor: light ? Colors.gray.soft : Colors.primary.hard,
                color: "white",
              }
            : {
                color: Colors.dark.hard,
              }),
        }}
      >
        <div>
          <div className="title">{title}</div>
          <div className="total">{subtitle}</div>
        </div>
        {button ? (
          <div className="button-check">
            <GeneralButton
              iconPosition="startIcon"
              buttonIcon={<IconCheck />}
              label="Use"
              width={50}
              height={24}
              style={{ fontSize: 9 }}
            />
          </div>
        ) : null}
      </div>
    </Card>
  );
};

SegmentationPackageCard.propTypes = {
  selected: PropTypes.bool,
  button: PropTypes.bool,
};

SegmentationPackageCard.defaultProps = {
  selected: false,
  button: true,
};

export default SegmentationPackageCard;
