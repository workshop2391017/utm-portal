import { makeStyles } from "@material-ui/styles";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import ScrollCustom from "components/BN/ScrollCustom";
import Colors from "helpers/colors";
import React, { useState } from "react";

// assets
import { ReactComponent as EditIcon } from "assets/icons/BN/edit-2.svg";

const useStyles = makeStyles({
  header: {
    display: "flex",
    justifyContent: "space-between",
    minHeight: "42px",
    paddingLeft: "15px",
    paddingRight: "15px",
  },
  content: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  cardContent: {
    height: "289px",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "10px",
    marginBottom: "18px",
    border: "1px solid #E6EAF3",
    paddingTop: "15px",
    paddingBottom: "15px",
  },

  titleCard: {
    fontSize: "10px",
    fontFamily: "FuturaBkBT",
    color: "#808080",
    fontWeight: 400,
  },
  subtitleCard: {
    fontFamily: "FuturaHvBT",
    fontSize: 13,
    fontWeight: 400,
    color: Colors.dark.hard,
  },
  description: {
    color: Colors.dark.hard,
    fontSize: "13px",
    fontFamily: "FuturaBkBT",
    paddingLeft: "15px",
    paddingRight: "15px",
  },
  footer: {
    display: "flex",
    justifyContent: "flex-end",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
    fontSize: 15,
    color: Colors.dark.hard,
    marginTop: 15,
    paddingLeft: "15px",
    paddingRight: "15px",
  },
  icon: {
    cursor: "pointer",
  },
  line: {
    display: "flex",
    alignItems: "center",
    marginBottom: 5,
    marginTop: 5,
  },
});

const RichTextCardCM = ({
  code,
  name,
  description,

  footer,
  onEdit,
  style,
}) => {
  const classes = useStyles();

  const [language, setLanguage] = useState("en");

  return (
    <div className={classes.cardContent} style={style}>
      <div className={classes.header}>
        <div style={{ display: "flex" }}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              marginRight: 10,
            }}
          >
            <div className={classes.titleCard}>Code</div>
            <div className={classes.subtitleCard}>{code}</div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div className={classes.titleCard}>Feature</div>
            <div className={classes.subtitleCard}>{name}</div>
          </div>
        </div>
        <div className={classes.line}>
          <EditIcon
            className={classes.icon}
            style={{ marginRight: 20 }}
            onClick={onEdit}
            width={22}
            height={22}
          />
          {/* <GeneralMenu
            options={["id-flag", "en-flag"]}
            customIcon={languageIcon}
            onId={() => setLanguage("id")}
            onEn={() => setLanguage("en")}
          /> */}
        </div>
      </div>
      <hr style={{ border: "1px solid #E8EEFF" }} />
      <ScrollCustom height={168}>
        <div className={classes.description}>{description(language)}</div>
      </ScrollCustom>
      <div className={classes.footer}>{footer}</div>
    </div>
  );
};

export default RichTextCardCM;
