import { makeStyles } from "@material-ui/styles";
import GeneralMenu from "components/BN/Menus/GeneralMenu";
import ScrollCustom from "components/BN/ScrollCustom";
import React from "react";
import styled from "styled-components";
import { ReactComponent as Globe } from "assets/icons/BN/globe.svg";

const useStyles = makeStyles({
  card: {
    height: "240px",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "10px",
  },
  header: {
    padding: "12px 15px",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    height: "42px",

    borderBottom: "1px solid #E8EEFF",
  },
  content: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  page: {},
  cardContent: {
    height: "240px",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "10px",
    // marginRight: "28px",
    marginBottom: "18px",
    border: "1px solid #E8EEFF",
  },
  titleRightCard: {
    display: "flex",
    alignItems: "center",
    gap: 10,
  },
});
const TitleCard = styled.div`
  color: #374062;
  font-size: 15px;
  font-family: FuturaHvBT;
  font-weight: 400;
`;
const Text = styled.p`
  color: #374062;
  font-size: 13px;
  font-family: FuturaBQ;
  padding: 15px;
`;

const RichTextCardCM = ({ title, description, iconGlobe }) => {
  const classes = useStyles();

  return (
    <div className={classes.cardContent}>
      <div className={classes.header}>
        <TitleCard>{title}</TitleCard>
        <div className={classes.titleRightCard}>
          {iconGlobe ? <Globe /> : null}
          <GeneralMenu options={["edit", "delete"]} />
        </div>
      </div>
      <ScrollCustom height={159}>
        <Text>{description}</Text>
      </ScrollCustom>
    </div>
  );
};

export default RichTextCardCM;
