// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Button, Menu, MenuItem, Box } from "@material-ui/core";
import Cards from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Grid from "@material-ui/core/Grid";

// components
import Delete from "../../Popups/Delete";
import DeleteConfirmation from "../../Popups/DeleteConfirmation";

// assets
import menu from "../../../../assets/icons/BN/ellipsis-horizontal.svg";
import trash from "../../../../assets/icons/BN/trash-2.svg";
import edit from "../../../../assets/icons/BN/edit-2.svg";

const SettingCard = (props) => {
  const { name, image } = props;
  const useStyles = makeStyles({
    // dataDetailKartu: {},
    menuButton: {
      minWidth: 20,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    menuIcon: {
      marginLeft: "50px",
    },
    topRight: {
      position: "relative",
      display: "flex",
      minHeight: 20,
      alignItems: "left",
      marginRight: "10px",
      marginTop: "10px",
    },
    topLeft: {
      fontFamily: "FuturaMdBT",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "17px",
      lineHeight: "20px",
      letterSpacing: "0.01em",
    },
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    root: {
      width: "150%",
      height: "270px",
      backgroundColor: "#fff",
      WebkitBoxShadow: "0px 0px 15px -10px rgba(0,0,0,0.75)",
      boxShadow: "0px 0px 15px -10px rgba(0,0,0,0.75)",
      borderRadius: 8,
      alignItems: "center",
      margin: "10.2px",
    },
    banner: {
      position: "realtive",
      width: "100%",
      height: "220px",
      top: "50px",
      display: "flex",
      alignItems: "center",
    },
  });
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(false);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    setDeleteConfirmModal(true);
  };

  return (
    <div className={classes.pengaturanKartu}>
      <Delete
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={clickDelete}
      />
      <DeleteConfirmation
        isOpen={deleteConfirmModal}
        handleClose={() => setDeleteConfirmModal(false)}
      />
      <div className={classes.containerCard}>
        <Grid container>
          <Cards className={classes.root}>
            <CardHeader
              avatar={<div className={classes.topLeft}>{name}</div>}
              action={
                <div className={classes.topRight}>
                  <Button
                    startIcon={<img src={menu} alt="Menu" />}
                    className={classes.menuButton}
                    color="primary"
                    aria-controls="simple-menu"
                    aria-haspopup="true"
                    onClick={handleClick}
                  />
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    className={classes.menuPopup}
                  >
                    <MenuItem style={{ color: "rgba(0, 97, 167, 0.3)" }}>
                      <span style={{ color: "#0061A7" }}>Ubah</span>
                      <img src={edit} alt="Edit" className={classes.menuIcon} />
                    </MenuItem>
                    <MenuItem
                      style={{ color: "rgba(0, 97, 167, 0.3)" }}
                      onClick={() => setDeleteModal(true)}
                    >
                      <span style={{ color: "#E31C23" }}>Hapus</span>
                      <img
                        src={trash}
                        alt="Trash"
                        className={classes.menuIcon}
                      />
                    </MenuItem>
                  </Menu>
                </div>
              }
            />
            <img className={classes.banner} alt="" src={image} />
          </Cards>
        </Grid>
      </div>
    </div>
  );
};

SettingCard.propTypes = {};

SettingCard.defaultProps = {};

export default SettingCard;
