/* eslint-disable jsx-a11y/mouse-events-have-key-events */
// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Fade, CircularProgress } from "@material-ui/core";

// assets
import arrowDown from "../../../../assets/icons/BN/arrow-square-down.svg";
import arrowUp from "../../../../assets/icons/BN/arrow-square-up.svg";
import tachometer from "../../../../assets/icons/BN/tachometer-alt.svg";
import blue from "../../../../assets/images/BN/card-backdrop-blue.svg";
import green from "../../../../assets/images/BN/card-backdrop-green.svg";
import red from "../../../../assets/images/BN/card-backdrop-red.svg";
import purple from "../../../../assets/images/BN/card-backdrop-purple.svg";
import { ReactComponent as InfoIcon } from "../../../../assets/icons/BN/info.svg";

const DashboardCard = (props) => {
  const {
    title,
    number1,
    number2,
    status,
    percentage,
    color,
    showInfo,
    loading,
  } = props;
  const useStyles = makeStyles({
    card: {
      width: "90%",
      height: "120px",
      backgroundColor: "#fff",
      borderRadius: 8,
      display: "flex",
      // overflow: "hidden",
    },
    left: {
      width: 48,
      background: `url(${
        color === "blue"
          ? blue
          : color === "red"
          ? red
          : color === "green"
          ? green
          : purple
      }) no-repeat`,
      backgroundSize: "cover",
      backgroundPosition: "left",
      textAlign: "center",
      borderTopLeftRadius: 8,
      borderBottomLeftRadius: 8,
    },
    right: {
      flex: "auto",
      padding: "4px 0 4px 15px",
      fontSize: 16,
      position: "relative",
      borderTopRightRadius: 8,
      borderBottomRightRadius: 8,
    },
    number1: {
      fontFamily: "FuturaHvBT",
      fontWeight: 600,
    },
    number2: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      color: "#BCC8E7",
      margin: "-5px 0 -1px",
    },
    percentage: {
      fontSize: 13,
      color:
        status === "down" ? "#FF6F6F" : status === "up" ? "#75D37F" : "#BCC8E7",
      lineHeight: "16px",
      fontFamily: "FuturaMdBT",
      fontWeight: 700,
    },
    info: {
      position: "absolute",
      right: 5,
      bottom: 5,
    },
    infoIcon: {
      position: "relative",
      zIndex: 1,
      transition: "0.8s",
      tintColor: "#000000",
      "&:hover": {
        opacity: 0,
      },
    },
    infoCard: {
      width: 240,
      height: "auto",
      backgroundColor: "#f4f7fb",
      borderRadius: 8,
      padding: "10px",
      lineHeight: 1.4,
      position: "absolute",
      right: -5,
      bottom: -135,
      zIndex: 100,
      border: "1px solid #BCC8E7",
    },
    netral: {
      display: "inline-block",
      width: 14,
      height: 14,
      borderRadius: "50%",
      border: "2px solid #BCC8E7",
      verticalAlign: "middle",
      marginTop: -2,
      position: "relative",
    },
    netralDot: {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
      width: 5,
      height: 5,
      borderRadius: "50%",
      backgroundColor: "#BCC8E7",
    },
    loading: {
      position: "absolute",
      top: "50%",
      left: "45%",
      transform: "translate(-50%,-50%)",
    },
  });
  const classes = useStyles();

  const [infoState, setInfoState] = useState(false);

  return (
    <div className={classes.card}>
      <div className={classes.left}>
        <img src={tachometer} alt="tachometer" style={{ marginTop: 15 }} />
      </div>
      <div className={classes.right}>
        {loading ? (
          <div className={classes.loading}>
            <CircularProgress color="primary" size={25} />
          </div>
        ) : (
          <div>
            <div style={{ marginBottom: -2, fontFamily: "FuturaBQ" }}>
              {title}
            </div>
            <div className={classes.number1}>{number1 || 0}</div>
            <div className={classes.number2}>{number2 || 0}</div>
            <div className={classes.percentage}>
              {status ? (
                <img
                  src={status === "up" ? arrowUp : arrowDown}
                  alt={status === "up" ? "Arrow Up" : "Arrow Down"}
                  style={{ marginTop: -2, width: 9.79, height: 9.85 }}
                />
              ) : (
                <div className={classes.netral}>
                  <div className={classes.netralDot} />
                </div>
              )}
              <span style={{ marginLeft: 4 }}>{percentage || 0}</span>
            </div>
            {showInfo && (
              <div className={classes.info}>
                {/* <img
                  src={info}
                  alt="info"
                  onMouseOver={() => setInfoState(true)}
                  onMouseOut={() => setInfoState(false)}
                  className={classes.infoIcon}
                /> */}
                <InfoIcon
                  onMouseOver={() => setInfoState(true)}
                  onMouseOut={() => setInfoState(false)}
                  className={classes.infoIcon}
                  stroke="#000000"
                />
                <Fade in={infoState} timeout={800}>
                  <div className={classes.infoCard}>
                    <p
                      style={{
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        marginBottom: "5px",
                        fontSize: 16,
                      }}
                    >
                      Disclaimer
                    </p>
                    <p style={{ fontSize: 13 }}>
                      The projected income is obtained from any admin fee and
                      transfer fee included on each transactions that is debited
                      directly from Bank system
                    </p>
                  </div>
                </Fade>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

DashboardCard.propTypes = {
  title: PropTypes.string,
  number1: PropTypes.number,
  number2: PropTypes.number,
  status: PropTypes.number,
  percentage: PropTypes.number,
  color: PropTypes.string,
  showInfo: PropTypes.bool,
  loading: PropTypes.bool,
};

DashboardCard.defaultProps = {
  title: "Title",
  number1: 0,
  number2: 0,
  status: "",
  percentage: 0,
  color: "blue",
  showInfo: false,
  loading: false,
};

export default DashboardCard;
