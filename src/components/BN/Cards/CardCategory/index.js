import React from "react";
import PropTypes from "prop-types";
import { makeStyles, Card, CardContent } from "@material-ui/core";

// component
import { ReactComponent as StoreIcon } from "assets/icons/BN/store.svg";
import { ReactComponent as OfficeIcon } from "assets/icons/BN/office.svg";
import { ReactComponent as HumanSingleIcon } from "assets/icons/BN/human_single.svg";
import { ReactComponent as HumanMultipleIcon } from "assets/icons/BN/human_multiple.svg";
import { ReactComponent as ManChecklistIcon } from "assets/icons/BN/manchecklist.svg";
import { ReactComponent as ManxIcon } from "assets/icons/BN/manx.svg";
import { ReactComponent as ManualIcon } from "assets/icons/BN/manual.svg";
import { ReactComponent as InstantIcon } from "assets/icons/BN/instant.svg";

const useStyles = makeStyles({
  cardDisabled: {
    borderRadius: 20,
    width: 345,
    height: 200,
    transition: "all ease .3s",
    border: "1px solid #E6EAF3",
    cursor: "pointer",
    boxShadow: "none",
    "&:hover": {
      cursor: "not-allowed",
    },
  },
  card: {
    borderRadius: 20,
    width: 345,
    height: 200,
    transition: "all ease .3s",
    border: "1px solid #E6EAF3",
    cursor: "pointer",
    boxShadow: "none",
    // boxShadow: "0px 8px 10px rgba(188, 200, 231, 0.5)",
    "&:hover, &.active": {
      backgroundColor: "#0061A7",
      border: "none",
      "& path": {
        fill: "#fff",
      },
      "& .title, & .text,": {
        color: "#fff",
      },
      "& .icon": {
        color: "#fff",
      },
    },
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignContent: "center",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    "& path": {
      fill: "#0061A7",
    },
    "& .title": {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: "17px",
      marginTop: 24.22,
      marginBottom: 10,
    },
    "& .text": {
      textAlign: "center",
      fontFamily: "FuturaBkBT",
      fontSize: "13px",
    },
  },
});

const CardCategory = ({
  text,
  type,
  title,
  onClick,
  className,
  isActive,
  disabled,
}) => {
  const classes = useStyles();

  const icon = {
    perorangan: <StoreIcon />, // icon store
    lembaga: <OfficeIcon />, // icon building
    single: <HumanSingleIcon />, // icon human single
    multiple: <HumanMultipleIcon />, // icon human multiple
    dengan_sys_admin: <ManChecklistIcon />, // icon manChecklist
    tanpa_sys_admin: <ManxIcon />, // icon man x
    manual: <ManualIcon />,
    instant: <InstantIcon />,
  };

  return (
    <Card
      className={
        disabled
          ? `${classes.cardDisabled}`
          : `${classes.card} ${isActive ? "active" : ""}`
      }
      onClick={() => (!disabled ? onClick(type) : null)}
    >
      <CardContent className={classes.content}>
        {icon[type]}
        <div className="title">{title}</div>
        <div className="text">{text}</div>
      </CardContent>
    </Card>
  );
};

CardCategory.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  className: PropTypes.object,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
  type: PropTypes.oneOf([
    "perorangan",
    "lembaga",
    "single",
    "multiple",
    "dengan_sys_admin",
    "tanpa_sys_admin",
  ]),
};
CardCategory.defaultProps = {
  className: {},
  onClick: () => {},
  title: "",
  text: "",
  type: "",
  isActive: false,
};

export default CardCategory;
