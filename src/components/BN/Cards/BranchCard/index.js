import React from "react";

// library
import PropTypes from "prop-types";
import { makeStyles, Button, Paper } from "@material-ui/core";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";

// assets
import { ReactComponent as IconArrowRight } from "assets/icons/BN/arrow-right.svg";

const useStyles = makeStyles({
  mainContainer: {
    borderRadius: "20px",
    overflow: "hidden",
    position: "relative",
    width: 350,
    height: "auto",
    paddingLeft: 20,
    paddingTop: 21,
    paddingBottom: 15,
    boxShadow: "none",
    border: "1px solid #E6EAF3",
    backgroundColor: "#FFFFFF",
    "&:hover, &.active": {
      border: "1px solid #0061A7",
      backgroundColor: "#EAF2FF",
    },
    "&:hover": {
      cursor: "pointer",
    },
  },
  branchName: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    color: "#374062",
    paddingBottom: 11,
    display: "flex",
    alignItems: "center",
  },
  text: {
    fontSize: 13,
    fontFamily: "FuturaBQ",
    color: "#7B87AF",
  },
  mainContent: {
    padding: "50px",
  },
  seemore: {
    color: "#0061A7",
    textTransform: "capitalize",
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    padding: 0,
    marginTop: 10,
    display: "flex",
    "& .icon-more": {
      marginLeft: 7,
    },
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
});

const BranchCard = ({
  isActive,
  className,
  onClick,
  onMore,
  branchName,
  telephone,
  fax,
  address,
  id,
  preview,
}) => {
  const classes = useStyles();

  return (
    <Paper
      className={`${classes.mainContainer} ${isActive ? "active" : ""}`}
      onClick={() => onClick(id)}
    >
      <div className={classes.branchName}>
        {isActive ? (
          <div style={{ marginRight: 10 }}>
            <CheckboxSingle checked={isActive} />
          </div>
        ) : null}
        {branchName}
      </div>
      <div className={classes.text}>Telephone : {telephone}</div>
      <div className={classes.text}>Fax : {fax}</div>
      <div className={classes.text}>{address}</div>
      {!preview && (
        <div>
          <Button className={classes.seemore} onClick={onMore}>
            View Location <IconArrowRight className="icon-more" />
          </Button>
        </div>
      )}
    </Paper>
  );
};

BranchCard.propTypes = {
  className: PropTypes.object,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
  onMore: PropTypes.func,
  branchName: PropTypes.string,
  telephone: PropTypes.string,
  fax: PropTypes.string,
  address: PropTypes.string,
  id: PropTypes.number,
};
BranchCard.defaultProps = {
  className: {},
  isActive: false,
  onClick: () => {},
  onMore: () => {},
  branchName: "",
  telephone: "",
  fax: "",
  address: "",
  id: 0,
};

export default BranchCard;
