// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

// components
// import Paper from "@material-ui/core/Paper";
// import Grid from "@material-ui/core/Grid";
import ScrollCustom from "../../ScrollCustom/ScrollContent";

// assets

const CardSyarat = (props) => {
  const { placeholder } = props;
  const useStyles = makeStyles({
    linescroll: {
      border: "1px solid #E6EAF3",
      borderRadius: "10px",
      marginTop: "18px",
      marginBottom: 10,
    },
    textjudul: {
      textAlign: "justify",
      fontFamily: "FuturaBkBT",
      fontSize: "12px",
      fontWeight: "400",
      marginLeft: "20px",
      marginRight: "10px",
      marginTop: "25px",
    },
    textcontent: {
      textAlign: "justify",
      fontFamily: "FuturaBkBT",
      fontSize: "12px",
      fontWeight: "400",
      marginLeft: "20px",
      marginRight: "10px",
      marginTop: "10px",
    },
    textcontentsub: {
      textAlign: "justify",
      fontFamily: "FuturaBkBT",
      fontSize: "12px",
      fontWeight: "400",
      marginLeft: "30px",
      marginRight: "10px",
      marginTop: "10px",
    },
    textcontentsub2: {
      textAlign: "justify",
      fontFamily: "FuturaBkBT",
      fontSize: "12px",
      fontWeight: "400",
      marginLeft: "40px",
      marginRight: "10px",
      marginTop: "10px",
    },
  });

  const classes = useStyles();
  const [modal, setModal] = useState(false);
  const [checkboxSingle, setCheckboxSingle] = useState(false);

  const [confirmSuccess, setConfirmSuccess] = useState(false);
  // const [input, setInput] = useState(null);

  // const clickSearch = () => {
  //   setDataSearch({
  //     input,
  //   });
  // };

  return (
    <div className={classes.linescroll}>
      <ScrollCustom
        style={{
          height: "490px",
          width: "590px",
        }}
      >
        <p className={classes.textjudul}>
          SYARAT DAN KETENTUAN PADA SAAT AKTIVASI INTERNET BANKING BANK
        </p>
        <p className={classes.textcontent}>1. Istilah</p>
        <p className={classes.textcontent}>
          1.1. INTERNET BANKING BANK adalah saluran distribusi Bank untuk
          mengakses rekening yang dimiliki Nasabah melalui jaringan internet
          dengan menggunakan perangkat lunak browser pada komputer.
        </p>
        <p className={classes.textcontent}>
          1.2. Bank adalah PT BANK TABUNGAN NEGARA (PERSERO) TBK yang meliputi
          Kantor Pusat dan kantor cabang serta kantor lainnya yang merupakan
          bagian yang tidak terpisahkan dari PT BANK TABUNGAN NEGARA (PERSERO)
          TBK.
        </p>
        <p className={classes.textcontent}>
          1.2. Bank adalah PT BANK TABUNGAN NEGARA (PERSERO) TBK yang meliputi
          Kantor Pusat dan kantor cabang serta kantor lainnya yang merupakan
          bagian yang tidak terpisahkan dari PT BANK TABUNGAN NEGARA (PERSERO)
          TBK.
        </p>
        <p className={classes.textcontent}>
          1.3. Nasabah adalah perorangan pemilik rekening simpanan dalam mata
          uang rupiah berupa Tabungan & Giro Rupiah Perorangan.
        </p>
        <p className={classes.textcontent}>
          1.4. Nasabah Pengguna adalah Nasabah yang telah terdaftar sebagai
          pengguna layanan INTERNET BANKING BANK .
        </p>
        <p className={classes.textcontent}>
          1.5. Daftar Rekening adalah nomor rekening Rupiah di semua cabang yang
          dimiliki oleh Nasabah di Bank yang telah didaftarkan dan karenanya
          dapat diakses oleh Nasabah Pengguna.
        </p>
        <p className={classes.textcontent}>
          1.6. User ID adalah identitas yang dimiliki oleh setiap Nasabah
          Pengguna yang harus dicantumkan/diinput dalam setiap penggunaan
          layanan INTERNET BANKING BANK .
        </p>
        <p className={classes.textcontent}>
          1.7. PIN Mobile Token (Personal Identification Number) INTERNET
          BANKING dan MOBILE BANKING BANK adalah nomor identifikasi pribadi yang
          bersifat rahasia dan hanya diketahui oleh Nasabah Pengguna serta harus
          dicantumkan/diinput oleh Nasabah Pengguna pada saat menggunakan
          layanan INTERNET BANKING dan MOBILE BANKING BANK . Bersama-sama dengan
          User ID, Password dan PIN Mobile Token digunakan untuk membuktikan
          bahwa nasabah bersangkutan adalah nasabah yang berhak atas layanan
          INTERNET BANKING BANK .
        </p>

        <p className={classes.textjudul}>
          2. Syarat Pendaftaran INTERNET BANKING BANK
        </p>
        <p className={classes.textcontent}>
          2.1. Nasabah mengisi dan menandatangani Formulir Aplikasi Internet
          Banking dapat diperoleh di cabang atau Melakukan pendaftaran melalui
          ATM .
        </p>
        <p className={classes.textcontent}>
          2.2. Menunjukkan bukti asli identitas diri yang sah (KTP, SIM, Paspor,
          KIMS) dan bukti kepemilikan pemegang rekening.
        </p>
        <p className={classes.textcontent}>
          2.3. Nasabah harus memiliki alamat E-mail.
        </p>
        <p className={classes.textcontent}>
          2.4. Nasabah Pengguna telah memperoleh Kode Registrasi (Registration
          Code) dari Bank untuk keperluan aktivasi online di situs INTERNET
          BANKING BANK .
        </p>
        <p className={classes.textcontent}>
          2.5. Telah membaca dan memahami Syarat dan Ketentuan INTERNET BANKING
          BANK .
        </p>
        <p className={classes.textjudul}>
          3. Ketentuan Penggunaan INTERNET BANKING BANK
        </p>
        <p className={classes.textcontent}>
          3.1. Nasabah Pengguna dapat menggunakan layanan INTERNET BANKING BANK
          untuk mendapatkan informasi dan atau melakukan transaksi Perbankan
          yang telah ditentukan oleh Bank.
        </p>
        <p className={classes.textcontent}>
          3.2. Pada saat pertama kali menggunakan layanan INTERNET BANKING BANK
          , Nasabah Pengguna diharuskan melakukan aktivasi di situs INTERNET
          BANKING BANK dengan cara memasukkan Kode Registrasi (Registration
          Code) yang diperoleh dari Bank untuk diubah menjadi User ID, Password
          dan PIN MOBILE TOKEN INTERNET BANKING BANK .
        </p>
        <p className={classes.textcontent}>
          3.3. Untuk setiap pelaksanaan transaksi:
        </p>
        <p className={classes.textcontentsub}>
          a. Nasabah Pengguna wajib memastikan ketepatan dan kelengkapan
          perintah transaksi (termasuk memastikan bahwa semua data yang
          diperlukan untuk transaksi telah diisi secara lengkap dan benar). Bank
          tidak bertanggung jawab terhadap segala dampak apapun yang mungkin
          timbul yang diakibatkan kelalaian, ketidaklengkapan, ketidakjelasan,
          atau ketidaktepatan perintah/data dari Nasabah Pengguna.
        </p>
        <p className={classes.textcontentsub}>
          b. Nasabah Pengguna memiliki kesempatan untuk memeriksa kembali dan
          atau membatalkan data yang telah diisi pada saat konfirmasi yang
          dilakukan secara otomatis oleh sistem sebelum adanya tanda persetujuan
          sebagaimana diatur di bawah ini.
        </p>
        <p className={classes.textcontentsub}>
          c. Apabila telah diyakini kebenaran dan kelengkapan data yang diisi,
          sebagai tanda persetujuan pelaksanaan transaksi maka Nasabah Pengguna
          wajib memasukkan PIN pada kolom yang telah disediakan pada halaman
          layanan transaksi INTERNET BANKING BANK .
        </p>
        <p className={classes.textcontent}>
          3.4. Segala transaksi yang telah diperintahkan kepada Bank dan
          disetujui oleh Nasabah Pengguna tidak dapat dibatalkan.
        </p>
        <p className={classes.textcontent}>
          3.5. Setiap perintah yang telah disetujui dari Nasabah Pengguna yang
          tersimpan pada pusat data Bank merupakan data yang benar yang diterima
          sebagai bukti perintah dari Nasabah Pengguna kepada Bank untuk
          melaksanakan transaksi yang dimaksud.
        </p>
        <p className={classes.textcontent}>
          3.6. Bank menerima dan menjalankan setiap perintah dari Nasabah
          Pengguna sebagai perintah yang sah berdasarkan penggunaan User ID,
          Password dan PIN Mobile Token , dan untuk itu Bank tidak mempunyai
          kewajiban untuk meneliti atau menyelidiki keaslian maupun keabsahan
          atau kewenangan pengguna User ID, Password dan PIN Mobile Token atau
          menilai maupun membuktikan ketepatan maupun kelengkapan perintah
          dimaksud, dan oleh karena itu perintah tersebut sah mengikat Nasabah
          Pengguna dengan sebagaimana mestinya, kecuali Nasabah Pengguna dapat
          membuktikan sebaliknya.
        </p>
        <p className={classes.textcontent}>
          3.7. Bank berhak untuk tidak melaksanakan perintah dari Nasabah
          Pengguna, apabila:
        </p>
        <p className={classes.textcontentsub}>
          a. Saldo rekening Nasabah Pengguna di Bank tidak cukup.
        </p>
        <p className={classes.textcontentsub}>
          b. Bank mengetahui atau mempunyai alasan untuk menduga bahwa penipuan
          atau aksi kejahatan telah atau akan dilakukan.
        </p>
        <p className={classes.textcontent}>
          3.8. Sebagai bukti bahwa transaksi yang diperintahkan Nasabah Pengguna
          telah berhasil dilakukan oleh Bank, Nasabah Pengguna akan mendapatkan
          bukti transaksi berupa nomor transaksi pada halaman transaksi layanan
          INTERNET BANKING BANK dan bukti tersebut akan tersimpan di dalam menu
          aktivitas transaksi selama 3 (tiga) bulan sejak tanggal transaksi.
        </p>
        <p className={classes.textcontent}>
          3.9. Nasabah Pengguna menyetujui dan mengakui bahwa:
        </p>
        <p className={classes.textcontentsub}>
          a. Dengan dilaksanakannya transaksi melalui INTERNET BANKING dan
          MOBILE BANKING BANK , semua perintah dan komunikasi dari Nasabah
          Pengguna yang diterima Bank akan diperlakukan sebagai alat bukti yang
          sah meskipun tidak dibuat dokumen tertulis dan atau dikeluarkan
          dokumen yang tidak ditandatangani.
        </p>
        <p className={classes.textcontent}>
          b. Bukti atas perintah dari Nasabah Pengguna kepada Bank dan segala
          bentuk komunikasi antara Bank dan Nasabah Pengguna yang dikirim secara
          elektronik yang tersimpan pada pusat data Bank dan atau tersimpan
          dalam bentuk penyimpanan informasi dan data lainnya di Bank, baik yang
          berupa dokumen tertulis, catatan, tape/cartridge, print out komputer
          dan atau salinan, merupakan alat bukti yang sah yang tidak akan
          dibantah keabsahan, kebenaran atau keasliannya.
        </p>
        <p className={classes.textcontent}>
          3.10. Atas pertimbangannya sendiri, Bank berhak untuk mengubah limit
          transaksi.
        </p>
        <p className={classes.textcontent}>
          3.11. Semua komunikasi melalui e-mail yang aman dan memenuhi standar
          serta dianggap sah, otentik, asli dan benar serta memberikan efek yang
          sama sebagaimana bila hal tersebut dilakukan secara tertulis dan atau
          melalui dokumen tertulis.
        </p>
        <p className={classes.textcontent}>
          3.12. Bank tidak diwajibkan untuk melaksanakan setiap perintah baik
          yang ditandatangani maupun tidak atau menjawab pertanyaan apapun yang
          diterima melalui e-mail yang tidak aman. Nasabah disarankan untuk
          tidak mengirim informasi rahasia melalui e-mail yang tidak aman.
        </p>
        <p className={classes.textcontent}>
          3.13. Bank berhak menghentikan layanan INTERNET BANKING dan MOBILE
          BANKING BANK untuk sementara waktu maupun untuk jangka waktu tertentu
          yang ditentukan oleh Bank untuk keperluan pembaharuan, pemeliharaan
          atau untuk tujuan lain dengan alasan apapun yang dianggap baik oleh
          Bank, dan untuk itu Bank tidak berkewajiban mempertanggungjawabkannya
          kepada siapapun.
        </p>
        <p className={classes.textjudul}>
          4. User ID, Password dan PIN Mobile Token BANK
        </p>
        <p className={classes.textcontent}>
          4.1. User ID, Password dan PIN Mobile Token merupakan kode yang
          bersifat rahasia dan kewenangan penggunaannya ada pada Nasabah
          Pengguna. User ID bersifat tetap dan tidak dapat diubah kembali.
        </p>
        <p className={classes.textcontent}>
          4.2. Nasabah wajib mengamankan User ID, Password dan PIN Mobile Token
          dengan cara:
        </p>
        <p className={classes.textcontentsub}>
          a. Tidak memberitahukan User ID, Password dan PIN Mobile Token kepada
          orang lain.
        </p>
        <p className={classes.textcontentsub}>
          b. Tidak mencatatkan User ID, Password dan PIN Mobile Token pada
          kertas atau menyimpannya secara tertulis pada handphone atau sarana
          penyimpanan lainnya yang memungkinkan diketahui orang lain.
        </p>
        <p className={classes.textcontentsub}>
          c. Memusnahkan secepatnya Email yang bersifat rahasia dari Internet
          Banking & Mobile Banking setelah menerimanya.
        </p>
        <p className={classes.textcontentsub}>
          d. Berhati-hati menggunakan User ID, Password dan PIN Mobile Token
          BANK agar tidak terlihat orang lain.
        </p>
        <p className={classes.textcontentsub}>
          e. Mengganti Password Internet Banking & Mobile Banking BANK secara
          berkala.
        </p>
        <p className={classes.textcontent}>
          4.3. Dalam hal Nasabah Pengguna mengetahui atau menduga User ID,
          Password dan PIN Mobile Token telah diketahui oleh orang lain yang
          tidak berwenang, maka Nasabah Pengguna wajib segera melakukan
          pengamanan dengan melakukan perubahan User ID dan Password. Apabila
          karena suatu sebab Nasabah Pengguna tidak dapat melakukan perubahan
          User ID dan Password Internet Banking dan Mobile Banking maka Nasabah
          Pengguna wajib memberitahukan kepada Bank. Sebelum diterimanya
          pemberitahuan secara tertulis oleh pejabat Bank yang berwenang, maka
          segala perintah, transaksi dan komunikasi berdasarkan penggunaan User
          ID, Password dan PIN Mobile Token oleh pihak yang tidak berwenang
          sepenuhnya menjadi tanggung jawab Nasabah Pengguna.
        </p>
        <p className={classes.textcontent}>
          4.4. Penggunaan User ID, Password & PIN Mobile Token mempunyai
          kekuatan hukum yang sama dengan perintah tertulis yang ditandatangani
          oleh Nasabah Pengguna, sehingga karenanya Nasabah Pengguna dengan ini
          menyatakan bahwa penggunaan User ID, Password dan PIN Mobile Token
          dalam setiap perintah atas transaksi INTERNET BANKING BANK juga
          merupakan pemberian kuasa dari Nasabah Pengguna kepada Bank untuk
          melaksanakan transaksi termasuk namun tidak terbatas untuk melakukan
          pendebetan rekening Nasabah baik dalam rangka pelaksanaan transaksi
          yang diperintahkan maupun untuk pembayaran biaya transaksi yang telah
          dan atau akan ditetapkan kemudian oleh Bank.
        </p>
        <p className={classes.textcontent}>
          4.5. Segala penyalahgunaan User ID, Password dan PIN Mobile Token
          INTERNET BANKING dan MOBILE BANKING BANK merupakan tanggung jawab
          Nasabah Pengguna. Nasabah Pengguna dengan ini membebaskan Bank dari
          segala tuntutan yang mungkin timbul, baik dari pihak lain maupun
          Nasabah Pengguna sendiri sebagai akibat penyalahgunaan User ID,
          Password dan PIN Mobile Token INTERNET BANKING dan MOBILE BANKING BANK
          .
        </p>
        <p className={classes.textcontent}>
          5. Penghentian Akses Layanan INTERNET BANKING BANK
        </p>
        <p className={classes.textcontent}>
          5.1. Akses layanan INTERNET BANKING BANK akan dihentikan oleh Bank
          apabila:
        </p>
        <p className={classes.textcontentsub}>
          a. Nasabah Pengguna meminta kepada Bank untuk menghentikan akses
          layanan INTERNET BANKING BANK secara permanen yang antara lain
          disebabkan oleh:
        </p>
        <p className={classes.textcontentsub2}>
          (i) User ID, Password dan PIN Mobile Token Internet Banking dan Mobile
          Banking Nasabah Pengguna lupa.
        </p>
        <p className={classes.textcontentsub2}>
          (ii) Nasabah Pengguna menutup semua rekening yang dapat diakses
          melalui layanan INTERNET BANKING BANK .
        </p>
        <p className={classes.textcontentsub}>
          b. Salah memasukkan User ID, Password dan PIN Mobile Token INTERNET
          BANKING dan MOBILE BANKING BANK sebanyak 3 (tiga) kali berturut-turut.
        </p>
        <p className={classes.textcontentsub}>
          c. Diterimanya laporan tertulis dari Nasabah Pengguna mengenai dugaan
          atau diketahuinya User ID, Password dan PIN Mobile Token oleh pihak
          lain yang tidak berwenang.
        </p>
        <p className={classes.textcontentsub}>
          d. Bank melaksanakan suatu keharusan sesuai ketentuan
          perundang-undangan yang berlaku.
        </p>
        <p className={classes.textcontent}>
          5.2. Untuk melakukan aktivasi kembali karena penghentian akses layanan
          tersebut di atas, Nasabah Pengguna harus menghubungi Contact Center
          Bank atau melakukan pendaftaran ulang di cabang pengelola rekening.
        </p>
        <p className={classes.textjudul}>6. Force Majeure</p>
        <p className={classes.textcontent}>
          Nasabah Pengguna akan membebaskan Bank dari segala tuntutan apapun,
          dalam hal Bank tidak dapat melaksanakan perintah dari Nasabah Pengguna
          baik sebagian maupun seluruhnya karena kejadian-kejadian atau
          sebab-sebab di luar kekuasaan atau kemampuan Bank termasuk namun tidak
          terbatas pada segala gangguan virus komputer atau sistem Trojan Horses
          atau komponen membahayakan yang dapat menggangu layanan INTERNET
          BANKING BANK , web browser atau komputer sistem Bank, Nasabah, atau
          Internet Service Provider, karena bencana alam, perang, huru-hara,
          keadaan peralatan, sistem atau transmisi yang tidak berfungsi,
          gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, serta
          kejadian-kejadian atau sebab-sebab lain di luar kekuasaan atau
          kemampuan Bank.
        </p>
        <p className={classes.textcontent}>7. Lain-lain</p>
        <p className={classes.textcontent}>
          7.1. Bukti perintah Nasabah Pengguna melalui layanan INTERNET BANKING
          dan MOBILE BANKING BANK adalah mutasi yang tercatat dalam Rekening
          Koran atau Buku Tabungan jika dicetak.
        </p>
        <p className={classes.textcontent}>
          7.2. Nasabah Pengguna dapat menghubungi Contact Center Bank atas
          setiap permasalahan yang berkaitan dengan transaksi dan perubahan
          akses layanan INTERNET BANKING dan MOBILE BANKING BANK .
        </p>
        <p className={classes.textcontent}>
          7.3. Bank dapat mengubah syarat dan ketentuan ini setiap saat dengan
          pemberitahuan terlebih dahulu kepada Nasabah Pengguna dalam bentuk dan
          melalui sarana apapun.
        </p>
        <p className={classes.textcontent}>
          7.4. Nasabah Pengguna tunduk pada ketentuan-ketentuan dan
          peraturan-peraturan yang berlaku pada Bank serta syarat-syarat
          pembukaan rekening dan syarat rekening gabungan, termasuk setiap
          perubahan yang akan diberitahukan terlebih dahulu oleh Bank dalam
          bentuk dan sarana apapun.
        </p>
        <p className={classes.textcontent}>
          7.5. Kuasa-kuasa baik yang tersurat dalam Syarat dan Ketentuan ini
          merupakan kuasa yang sah yang tidak akan berakhir selama Nasabah
          Pengguna masih memperoleh layanan INTERNET BANKING dan MOBILE BANKING
          BANK atau masih adanya kewajiban lain dari Nasabah Pengguna kepada
          Bank.
        </p>
      </ScrollCustom>
    </div>
  );
};

CardSyarat.propTypes = {
  placeholder: PropTypes.func,
};

CardSyarat.defaultProps = {
  placeholder: "syarat",
};

export default CardSyarat;
