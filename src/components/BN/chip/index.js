// Chip Component
// --------------------------------------------------------

import React from "react";
import PropTypes from "prop-types";
import { Chip, Avatar, styled } from "@material-ui/core";
import stringAvatar from "helpers/helperChip";

// import    from "../../../helpers/colors";
// import stringAvatar from "./helperChip";

const CustomChip = styled(Chip)(() => ({
  backgroundColor: "#F4F7FB",
  width: "fit-content",
  ".MuiChip-label": {
    margin: "0px 3px",
  },
  svg: {
    fill: "#BCC8E7",
  },
  ".MuiAvatar-root": {
    color: "#fff",
  },
}));

const ChipCom = ({ handleDelete, label }) => (
  <CustomChip
    size="small"
    label={label}
    onDelete={handleDelete}
    avatar={
      <Avatar style={{ backgroundColor: stringAvatar(label), color: "#fff" }}>
        {label[0]}
      </Avatar>
    }
  />
);

ChipCom.propTypes = {
  handleDelete: PropTypes.func,
  label: PropTypes.string,
};

ChipCom.defaultProps = {
  handleDelete: () => {},
  label: "",
};

export default ChipCom;
