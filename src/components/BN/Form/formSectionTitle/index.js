import { Divider, makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";
import React from "react";
import PropTypes from "prop-types";

const FormSectionTitle = ({ title, endContent, color, borderColor }) => {
  const useStyles = makeStyles({
    title: {
      fontFamily: "FuturaHvBT",
      color: color || Colors.primary.hard,
      display: "flex",
      justifyContent: "space-between",
    },
  });

  const classes = useStyles();

  return (
    <div>
      <div className={classes.title}>
        {title} {endContent}
      </div>
      <Divider
        style={{
          borderColor: borderColor || Colors.primary.hard,
          backgroundColor: borderColor || Colors.primary.hard,
        }}
      />
    </div>
  );
};

export default FormSectionTitle;

FormSectionTitle.propTypes = {
  title: PropTypes.string.isRequired,
  endContent: PropTypes.string,
  color: PropTypes.string,
  borderColor: PropTypes.string,
};

FormSectionTitle.defaultProps = {
  endContent: null,
  color: null,
  borderColor: null,
};
