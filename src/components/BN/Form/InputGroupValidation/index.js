import React from "react";
import { makeStyles } from "@material-ui/core";
import PropTypes from "prop-types";

const useStyles = makeStyles({
  inputFieldContainer: {
    "& .ant-select-selector": {
      width: 350,
    },
    "& .md": {
      width: 350,
    },
    "& .sm": {
      width: 235,
    },
  },
  inputFieldContainerGreen: {
    "& .ant-select-selector": {
      border: "1px solid #75D37F !important",
    },
    "& .ant-input": {
      border: "1px solid #75D37F !important",
    },
  },
  inputFieldContainerError: {
    "& .ant-select-selector": {
      border: "1px solid #D14848 !important",
    },
    "& .ant-input": {
      border: "1px solid #D14848 !important",
    },
  },
});

const FormField = ({
  children,
  label,
  error,
  errorMessage,
  style,
  absolute,
  marginTopLabel,
  margin,
  green,
  bottom,
  top,
  left,
  right,
}) => {
  const classes = useStyles();
  return (
    <div className="inputGroup" style={{ ...style, position: "relative" }}>
      <div
        className="formTitle"
        style={{
          color: green ? "#75D37F" : error ? "#D14848" : "",
          ...(margin ? { margin } : { marginBottom: "13px" }),
        }}
      >
        {label}
      </div>
      <div
        className={
          green
            ? `${classes.inputFieldContainerGreen}`
            : error
            ? `${classes.inputFieldContainerError}`
            : `${classes.inputFieldContainer}`
        }
      >
        {children}
      </div>
      {error && (
        <span
          style={{
            color: "#D14848",
            marginTop: marginTopLabel,
            position: absolute ? "absolute" : "",
            bottom: bottom ?? "",
            top: top ?? "",
            left: left ?? "",
            right: right ?? "",
          }}
        >
          {errorMessage}
        </span>
      )}
    </div>
  );
};

FormField.propTypes = {
  children: PropTypes.node,
  label: PropTypes.string,
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  absolute: PropTypes.bool,
  marginTopLabel: PropTypes.number,
  margin: PropTypes.object,
};

FormField.defaultProps = {
  error: false,
  errorMessage: "",
  absolute: true,
  marginTopLabel: 5,
  margin: {},
  children: <div />,
  label: "",
};

export default FormField;
