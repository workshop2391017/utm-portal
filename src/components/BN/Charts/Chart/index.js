import { CircularProgress } from "@material-ui/core";
import React from "react";
import ReactApexChart from "react-apexcharts";
import Tabs from "../../Tabs/AntdTabs";

const Chart = (props) => {
  // const series = props.series;
  const {
    type,
    activeChartTabs,
    categories,
    series,
    isLoading,
    onChangeTabsChart,
    tabsValue,
    chartTabs,
  } = props;

  const options = {
    chart: {
      height: 380,
      type: "area",
      stacked: false,
      zoom: {
        enabled: false,
      },
      toolbar: {
        show: false,
        autoSelected: "pan",
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
    },
    xaxis: {
      show: true,
      tooltip: {
        enabled: true,
        offsetY: 5,
        style: {
          fontSize: 12,
          fontFamily: "FuturaMdBT",
        },
      },
      // type: 'datetime',
      categories,

      // dataLabels: {
      //   enabled: false,
      //   enabledOnSeries: [2]
      // },

      labels: {
        show: true,
        align: "right",
        // minWidth: 0,
        // maxWidth: 160,
        style: {
          colors: [
            "#374062",
            "#374062",
            "#374062",
            "#374062",
            "#374062",
            "#374062",
            "#374062",
          ],
          fontSize: "13px",
          fontFamily: "FuturaBQ",
          fontWeight: 400,
          cssClass: "apexcharts-yaxis-label",
        },
        // offsetY: 0,
      },
      // tickAmount: 10,
      tickPlacement: "between",
      // tickPlacement: 'on',
    },
    yaxis: {
      type: "numeric",
      opposite: false,
      tickAmount: 7,
      // min: 10000,
      // max: 1,
      labels: {
        show: true,
        // align: 'right',
        // minWidth: 0,
        // maxWidth: 160,
        style: {
          colors: ["#7B87AF"],
          fontSize: "10px",
          fontFamily: "FuturaMdBT",
          fontWeight: 400,
          // cssClass: 'apexcharts-yaxis-label',
        },
        offsetY: 0,
      },
    },
    legend: {
      show: false,
    },

    tooltip: {
      enabled: true,
      shared: true,

      y: {
        title: {
          formatter: (seriesName, { series, seriesIndex, dataPointIndex }) =>
            `${seriesName.split(",")[dataPointIndex]}:`,
        },
      },
      z: {
        formatter: undefined,
        title: "Size: ",
      },
    },

    fixed: {
      enabled: true,
      position: "topLeft",
      offsetX: 60,
      offsetY: 50,
    },

    onDatasetHover: {
      highlightDataSeries: false,
    },
    grid: {
      show: false,
      row: {
        colors: ["#F6F7FF"],
        opacity: 1,
      },
    },
    markers: {
      size: 4,
      colors:
        activeChartTabs === "1"
          ? ["#FFDD00", "#75D37F", "#FFA24B"]
          : ["#FC5757", "#B35AFA", "#FFA24B"],
      // colors: ['#75D37F', '#FF6F6F', '#FFA24B'], default sama dengan color bawah
      strokeColors: "#fff",
      strokeWidth: 2,
      hover: {
        size: 7,
      },
    },
    colors:
      activeChartTabs === "1"
        ? ["#FFDD00", "#75D37F", "#FFA24B"]
        : ["#FC5757", "#B35AFA", "#FFA24B"],
    selection: "one_year",
  };

  return (
    <div>
      {/* {chartTabs !== undefined ? (
        chartTabs
      ) : (
        <Tabs
          activeKey={tabsValue}
          onChange={onChangeTabsChart}
          tabs={["Transaction", "Error"]}
          style={{ marginBottom: -20 }}
        />
      )} */}
      {isLoading ? (
        <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
          <div style={{ margin: "auto" }}>
            <CircularProgress color="primary" size={40} />
          </div>
        </div>
      ) : (
        <ReactApexChart
          options={options}
          series={series || []}
          type="line"
          height={325}
        />
      )}
    </div>
  );
};

export default Chart;
