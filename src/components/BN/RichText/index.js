// RichText Component
// --------------------------------------------------------

import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import draftToHtml from "draftjs-to-html";
import { unemojify } from "node-emoji";
import SVGEraser from "assets/icons/BN/eraser.svg";
import SVGSmile from "assets/icons/BN/smile.svg";

import "./text-editor.scss";
import "./text-editor-2.scss";

const ToolbarOptions = {
  options: [
    "inline",
    // "blockType",
    // "fontSize",
    // "fontFamily",
    "list",
    "textAlign",
    "link",
    // "image",
    // "colorPicker",
    // "remove",
    // "emoji",
    // "history",
  ],
  inline: {
    options: [
      "bold",
      "italic",
      "underline",
      // "strikethrough",
      // "monospace",
      // "superscript",
      // "subscript",
    ],
  },
  // fontFamily: {
  //   options: [
  //     "Arial",
  //     "Georgia",
  //     "Impact",
  //     "Tahoma",
  //     "Times New Roman",
  //     "Verdana",
  //   ],

  //   className: undefined,
  //   component: undefined,
  //   dropdownClassName: undefined,
  // },
  // colorPicker: {
  //   className: undefined,
  //   component: undefined,
  //   popupClassName: undefined,
  // },
  // remove: { icon: SVGEraser, className: undefined, component: undefined },
  // fontSize: {
  //   options: [8, 9, 10, 11, 12, 13, 14, 16, 18, 24, 30, 36, 48, 60, 72, 96],
  //   className: undefined,
  //   component: undefined,
  //   dropdownClassName: undefined,
  // },
  // blockType: {
  //   inDropdown: true,
  //   options: [
  //     "Normal",
  //     "H1",
  //     "H2",
  //     "H3",
  //     "H4",
  //     "H5",
  //     "H6",
  //     "Blockquote",
  //     "Code",
  //   ],
  //   className: undefined,
  //   component: undefined,
  //   dropdownClassName: undefined,
  // },
  // emoji: {
  //   icon: SVGSmile,
  //   className: undefined,
  //   component: undefined,
  //   popupClassName: undefined,
  //   emojis: [
  //     "😀",
  //     "😁",
  //     "😂",
  //     "😃",
  //     "😉",
  //     "😋",
  //     "😎",
  //     "😍",
  //     "😗",
  //     "🤗",
  //     "🤔",
  //     "😣",
  //     "😫",
  //     "😴",
  //     "😌",
  //     "🤓",
  //     "😛",
  //     "😜",
  //     "😠",
  //     "😇",
  //     "😷",
  //     "😈",
  //     "👻",
  //     "😺",
  //     "😸",
  //     "😹",
  //     "😻",
  //     "😼",
  //     "😽",
  //     "🙀",
  //     "🙈",
  //     "🙉",
  //     "🙊",
  //     "👼",
  //     "👮",
  //     "🕵",
  //     "💂",
  //     "👳",
  //     "🎅",
  //     "👸",
  //     "👰",
  //     "👲",
  //     "🙍",
  //     "🙇",
  //     "🚶",
  //     "🏃",
  //     "💃",
  //     "⛷",
  //     "🏂",
  //     "🏌",
  //     "🏄",
  //     "🚣",
  //     "🏊",
  //     "⛹",
  //     "🏋",
  //     "🚴",
  //     "👫",
  //     "💪",
  //     "👈",
  //     "👉",
  //     "👉",
  //     "👆",
  //     "🖕",
  //     "👇",
  //     "🖖",
  //     "🤘",
  //     "🖐",
  //     "👌",
  //     "👍",
  //     "👎",
  //     "✊",
  //     "👊",
  //     "👏",
  //     "🙌",
  //     "🙏",
  //     "🐵",
  //     "🐶",
  //     "🐇",
  //     "🐥",
  //     "🐸",
  //     "🐌",
  //     "🐛",
  //     "🐜",
  //     "🐝",
  //     "🍉",
  //     "🍄",
  //     "🍔",
  //     "🍤",
  //     "🍨",
  //     "🍪",
  //     "🎂",
  //     "🍰",
  //     "🍾",
  //     "🍷",
  //     "🍸",
  //     "🍺",
  //     "🌍",
  //     "🚑",
  //     "⏰",
  //     "🌙",
  //     "🌝",
  //     "🌞",
  //     "⭐",
  //     "🌟",
  //     "🌠",
  //     "🌨",
  //     "🌩",
  //     "⛄",
  //     "🔥",
  //     "🎄",
  //     "🎈",
  //     "🎉",
  //     "🎊",
  //     "🎁",
  //     "🎗",
  //     "🏀",
  //     "🏈",
  //     "🎲",
  //     "🔇",
  //     "🔈",
  //     "📣",
  //     "🔔",
  //     "🎵",
  //     "🎷",
  //     "💰",
  //     "🖊",
  //     "📅",
  //     "✅",
  //     "❎",
  //     "💯",
  //   ],
  // },
};
const RichText = ({
  onChange,
  value,
  formName,
  placeholder,
  customClassName,
}) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [touched, setTouched] = useState(false);

  const onEditorStateChange = (editorState) => {
    const newValue = unemojify(
      draftToHtml(convertToRaw(editorState.getCurrentContent()))
    );

    if (value !== newValue) {
      onChange(newValue);
    }
    setEditorState(editorState);
  };

  if (value && !touched && formName === "update") {
    const contentBlock = htmlToDraft(value);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(
        contentBlock.contentBlocks
      );
      setEditorState(EditorState.createWithContent(contentState));
      setTouched(true);
    }
  }
  return (
    <div className={customClassName ? "text-editor-2" : "text-editor"}>
      <Editor
        placeholder={placeholder}
        editorState={editorState}
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        onEditorStateChange={onEditorStateChange}
        toolbar={ToolbarOptions}
      />
    </div>
  );
};

RichText.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.bool.isRequired,
  formName: PropTypes.string.isRequired,
  input: PropTypes.shape({
    onChange: PropTypes.func,
    name: PropTypes.string,
  }).isRequired,
  placeholder: PropTypes.string,
  customClassName: PropTypes.bool,
};

RichText.defaultProps = {
  onChange: () => {},
  placeholder: "Masukkan text editor",
  customClassName: false,
};

export default RichText;
