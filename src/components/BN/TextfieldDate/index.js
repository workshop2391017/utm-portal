// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import TextField from "../TextField/AntdTextField";

import WhiteDatePicker from "../DatePicker/WhiteDatePicker";

// assets

const TextfieldDate = (props) => {
  const { dataSearch, setDataSearch, placeholder } = props;
  const useStyles = makeStyles({
    search: {
      height: "44px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        marginTop: 8,
        "&:hover": {
          boxShadow: "none",
        },
        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "&.ant-input:placeholder-shown": {
          fontSize: 15,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
  });

  const classes = useStyles();

  const [dateValue, setDateValue] = useState(null);
  // const [input, setInput] = useState(null);

  // const clickSearch = () => {
  //   setDataSearch({
  //     input,
  //   });
  // };

  return (
    <div style={{ display: "flex", marginBottom: 25 }}>
      <WhiteDatePicker
        placeholder="Pilih tanggal lahir Anda"
        value={dateValue}
        onChange={(e) => setDateValue(e)}
        //   icon={plus62}
        style={{
          // width: "240px",
          // height: "40px",
          // borderRadius: "10px",
          // border: "1px solid #BCC8E7",
          width: "350px",
          height: "40px",
          // borderRadius: "0px 10px 10px 0px ",
          border: "1px solid #BCC8E7",
          fontSize: "15px",
          // marginLeft: "-5px",
          // marginTop: -5,
          fontFamily: "FuturaBq",
        }}
      />
    </div>
  );
};

TextfieldDate.propTypes = {
  dataSearch: PropTypes.object.isRequired,
  setDataSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.func,
};

TextfieldDate.defaultProps = {
  placeholder: "tanggal",
};

export default TextfieldDate;
