// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Fade } from "@material-ui/core";
import { DatePicker } from "antd";

// assets
import { ReactComponent as ChervonRightBlue } from "assets/icons/BN/chevron-right.svg";
import { ReactComponent as CheckClose } from "assets/icons/BN/close-blue.svg";
import calendar from "assets/icons/BN/calendarblue.svg";

const { RangePicker } = DatePicker;

const RangeDatePicker = (props) => {
  const {
    value,
    onChange,
    style,
    placeholder,
    datePickerID,
    openCalendar,
    setOpenCalendar,
    placeholderState,
    setPlaceholderState,
    disabledDate,
    disabled,
    defaultValue,
    ...rest
  } = props;
  const useStyles = makeStyles((theme) => ({
    datePicker: {
      height: 40,
      width: 245,
      [theme.breakpoints.up("md")]: {
        width: 230,
      },
      [theme.breakpoints.up("lg")]: {
        width: 245,
      },
      [theme.breakpoints.up("xl")]: {
        width: 245,
      },
      borderRadius: 6,
      bacgroundColor: "blue",
      border: "1px solid #0061A7",
      transform: "translateY(-40%)",
      cursor: "pointer",
      transition: "0.25s",
      overflow: "hidden",
      "&:hover": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #0061A7",
      },
      "& .ant-picker-input": {
        "& input": {
          color: "#0061A7",

          // backgroundColor: 'red',
          "&::placeholder": {
            color: "#0061A7",
            // backgroundColor: "red",
          },
        },
      },
      "& .ant-picker-range-separator": {
        padding: 0,
      },
      "& .ant-picker-suffix": {
        cursor: "pointer",
      },
      // "& .ant-picker-clear": {
      //   width: 0,
      //   display: "none",
      // },
    },
    dropdown: {
      zIndex: 1300,
      "& .ant-picker-range-wrapper": {
        "& .ant-picker-panel-container": {
          borderRadius: 12,
          padding: 20,
          boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
          "& .ant-picker-panels": {
            "& .ant-picker-panel": {
              "&:last-child": {
                display: "none",
              },
              "&:first-child": {
                display: "block",
                border: "none",
                "& .ant-picker-date-panel": {
                  "& .ant-picker-header": {
                    color: "#374062",
                    border: "none",
                    "& .ant-picker-header-view": {
                      fontFamily: "Futura",
                      fontSize: 15,
                    },
                    "& .ant-picker-header-next-btn": {
                      visibility: "visible !important",
                    },
                    "& .ant-picker-header-super-next-btn": {
                      visibility: "visible !important",
                    },
                  },
                  "& .ant-picker-body": {
                    "& .ant-picker-content": {
                      "& thead": {
                        "& tr th": {
                          color: "#374062",
                        },
                      },
                      "& tbody": {
                        "& tr": {
                          "& td": {
                            position: "relative",
                            "&.ant-picker-cell-today": {
                              "& .ant-picker-cell-inner::before": {
                                border: "none",
                              },
                            },
                            "&.ant-picker-cell-range-start": {
                              "& .ant-picker-cell-inner": {
                                borderRadius: "50%",
                                position: "absolute",
                                top: "50%",
                                left: "50%",
                                transform: "translate(-50%, -50%)",
                                lineHeight: "28px",
                                minWidth: 28,
                                height: 28,
                                color: "#F2F5FD",
                              },
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                              "&.ant-picker-cell-range-start-single::before": {
                                backgroundColor: "transparent",
                              },
                            },
                            "&.ant-picker-cell-range-end": {
                              "& .ant-picker-cell-inner": {
                                borderRadius: "50%",
                                position: "absolute",
                                top: "50%",
                                left: "50%",
                                transform: "translate(-50%, -50%)",
                                lineHeight: "28px",
                                minWidth: 28,
                                height: 28,
                                color: "#F2F5FD",
                              },
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                            },
                            "&.ant-picker-cell-in-range": {
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                            },
                            "&.ant-picker-cell-in-view": {
                              "& .ant-picker-cell-inner": {
                                color: "#06377B",
                              },
                            },
                            "&.ant-picker-cell": {
                              "& .ant-picker-cell-inner": {
                                borderRadius: "50%",
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
                "& .ant-picker-month-panel": {
                  "& .ant-picker-header": {
                    color: "#374062",
                    border: "none",
                    "& .ant-picker-header-view": {
                      fontFamily: "Futura",
                      fontSize: 16,
                    },
                  },
                  "& .ant-picker-body": {
                    "& .ant-picker-content": {
                      "& thead": {
                        "& tr th": {
                          color: "#374062",
                        },
                      },
                      "& tbody": {
                        "& tr": {
                          "& td": {
                            "&.ant-picker-cell-range-start": {
                              "& .ant-picker-cell-inner": {
                                color: "#F2F5FD",
                              },
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                              "&.ant-picker-cell-range-start-single::before": {
                                backgroundColor: "transparent",
                              },
                            },
                            "&.ant-picker-cell-range-end": {
                              "& .ant-picker-cell-inner": {
                                color: "#F2F5FD",
                              },
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                            },
                            "&.ant-picker-cell-in-range": {
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                            },
                            "&.ant-picker-cell-in-view": {
                              "& .ant-picker-cell-inner": {
                                color: "#06377B",
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
                "& .ant-picker-year-panel": {
                  "& .ant-picker-header": {
                    color: "#374062",
                    border: "none",
                    "& .ant-picker-header-view": {
                      fontFamily: "Futura",
                      fontSize: 16,
                    },
                  },
                  "& .ant-picker-body": {
                    "& .ant-picker-content": {
                      "& thead": {
                        "& tr th": {
                          color: "#374062",
                        },
                      },
                      "& tbody": {
                        "& tr": {
                          "& td": {
                            "&.ant-picker-cell-range-start": {
                              "& .ant-picker-cell-inner": {
                                color: "#F2F5FD",
                              },
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                              "&.ant-picker-cell-range-start-single::before": {
                                backgroundColor: "transparent",
                              },
                            },
                            "&.ant-picker-cell-range-end": {
                              "& .ant-picker-cell-inner": {
                                color: "#F2F5FD",
                              },
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                            },
                            "&.ant-picker-cell-in-range": {
                              "&::before": {
                                backgroundColor: "#E6EAF3",
                              },
                            },
                            "&.ant-picker-cell-in-view": {
                              "& .ant-picker-cell-inner": {
                                color: "#06377B",
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
                "& .ant-picker-decade-panel": {
                  "& .ant-picker-header": {
                    color: "#374062",
                    border: "none",
                    "& .ant-picker-header-view": {
                      fontFamily: "Futura",
                      fontSize: 16,
                    },
                  },
                  "& .ant-picker-body": {
                    "& .ant-picker-content": {
                      "& thead": {
                        "& tr th": {
                          color: "#374062",
                        },
                      },
                      "& tbody": {
                        "& tr": {
                          "& td": {
                            "&.ant-picker-cell-in-view": {
                              "& .ant-picker-cell-inner": {
                                color: "#06377B",
                              },
                            },
                            "&.ant-picker-cell-selected": {
                              "& .ant-picker-cell-inner": {
                                color: "#F2F5FD !important",
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    placeholder: {
      fontSize: 15,
      color: "#0061A7",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      zIndex: 1,
      lineHeight: "40px",
      borderRadius: 6,
      textAlign: "center",
      cursor: "pointer",
      display: "flex",
      border: "1px solid #0061A7",
      overflow: "hidden",
    },
    container: {
      width: "fit-content",
      height: 40,
      position: "relative",
    },
  }));
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <RangePicker
        value={value}
        onChange={onChange}
        className={`picker${datePickerID} ${classes.datePicker}`}
        style={style}
        inputReadOnly
        suffixIcon={
          <div
            style={{
              marginRight: 12.5,
            }}
          >
            <img
              src={calendar}
              alt="icon-suffix"
              style={{
                cursor: "pointer",
              }}
            />
          </div>
        }
        clearIcon={
          value ? (
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                alignContent: "center",
                alignItems: "center",
                backgroundColor: "white",
              }}
            >
              <div
                style={{
                  height: "40px",
                  width: "34px",
                  borderTopRightRadius: "10px",
                  borderBottomRightRadius: "10px",
                  padding: "0px 0px 0px 0px",
                  marginRight: "-7px",
                  textAlign: "center",
                  display: "flex",
                  justifyContent: "center",
                  alignContent: "center",
                  alignItems: "center",
                }}
              >
                <CheckClose />
              </div>
            </div>
          ) : null
        }
        dropdownClassName={`rangeDropdown${datePickerID} ${classes.dropdown}`}
        placeholder={placeholder}
        format="DD/MM/YYYY"
        disabledDate={disabledDate}
        disabled={disabled}
        defaultValue={defaultValue}
        separator={<ChervonRightBlue width={20} height={20} />}
        {...rest}
      />
    </div>
  );
};

RangeDatePicker.propTypes = {
  value: PropTypes.array,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  datePickerID: PropTypes.number,
  disabledDate: PropTypes.func,
  defaultValue: PropTypes.array,
};

RangeDatePicker.defaultProps = {
  value: null,
  onChange: () => console.warn("onChange"),
  style: null,
  placeholder: ["Start", "End"],
  datePickerID: 0,
  disabledDate: null,
  defaultValue: [],
};

export default RangeDatePicker;
