/* eslint-disable react/jsx-no-bind */
// main
import React from "react";
import PropTypes from "prop-types";
// libraries
import { makeStyles } from "@material-ui/core";
import { DatePicker } from "antd";
import moment from "moment";

// assets
import calendar from "../../../../assets/icons/BN/calendar.svg";

const DateTimePicker = (props) => {
  const { value, onChange, style, placeholder } = props;
  const useStyles = makeStyles({
    datePicker: {
      height: 40,
      width: 170,
      borderRadius: 6,
      border: "1px solid #0061A7",
      "&:hover": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #0061A7",
      },
      "& .ant-picker-input": {
        cursor: "pointer",

        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#0061A7",
          },
        },
        "& .ant-picker-suffix": {
          cursor: "pointer",
        },
        "& .ant-picker-clear": {
          width: 0,
          display: "none",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      "& .ant-picker-panel-container": {
        borderRadius: 12,
        padding: 20,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        "& .ant-picker-panel": {
          border: "none",
          "& .ant-picker-date-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& thead": {
                  "& tr th": {
                    color: "#374062",
                  },
                },
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                        "&:hover": {
                          backgroundColor: "#F4F7FB",
                        },
                      },
                    },
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        lineHeight: "28px",
                        minWidth: 28,
                        height: 28,
                        color: "#F2F5FD !important",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-month-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-year-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-decade-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-time-panel": {
            "& .ant-picker-content": {
              "& .ant-picker-time-panel-column": {
                "& .ant-picker-time-panel-cell": {
                  "& .ant-picker-time-panel-cell-inner:hover": {
                    backgroundColor: "#F4F7FB",
                  },
                },
              },
            },
          },
          "& .ant-picker-footer": {
            border: "none",
            "& .ant-picker-today-btn": {
              color: "#06377B",
            },
            "& .ant-btn-primary[disabled]": {
              color: "#374062",
            },
          },
        },
      },
    },
  });
  const classes = useStyles();
  const dateFormat = "DD/MM/YYYY";

  const customFormat = (value) => `custom format: ${value.format(dateFormat)}`;

  const onOk = (value) => {
    console.warn("onOk: ", value);
  };

  return (
    <DatePicker
      showTime={{ format: "HH:mm A" }}
      //   defaultValue={moment('2015/01/01 HH', dateFormat)}
      format="DD/MM/YYYY, HH:mm A"
      //   value={value}
      onChange={onChange}
      placeholder={placeholder}
      onOk={onOk}
      className={classes.datePicker}
      style={style}
      suffixIcon={
        <img src={calendar} alt="icon-suffix" style={{ cursor: "pointer" }} />
      }
      dropdownClassName={classes.dropdown}
    />
  );
};

DateTimePicker.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};

DateTimePicker.defaultProps = {
  value: null,
  onChange: () => console.warn("click onChange"),
  style: null,
  placeholder: "Placeholder",
};

export default DateTimePicker;
