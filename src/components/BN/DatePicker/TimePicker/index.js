import React from "react";
import PropTypes from "prop-types";
import { TimePicker } from "antd";
import { makeStyles } from "@material-ui/core";
import moment from "moment";

const AntdTimePicker = ({ onChange, value, width, ...rest }) => {
  const useStyles = makeStyles({
    timepicker: {
      position: "relative",
      cursor: "pointer",
      transition: "0.25s",
      "& .ant-picker": {
        width,
        height: "40px",
        borderRadius: "10px",
        border: "1px solid #BCC8E7",
        fontFamily: "FuturaBkBT",
        fontSize: "15px",
        paddingRight: 0,
        overflow: "hidden",
        "& .ant-picker-input": {
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "& .ant-picker-suffix": {
          height: "40px",
          backgroundColor: "#0061A7",
          padding: "8px",
          display: "flex",
          alignItems: "center",
          "& svg": {
            fill: "#fff",
          },
        },
      },
    },
    dropdown: {
      zIndex: 1300,
    },
  });

  const classes = useStyles();

  return (
    <div className={classes.timepicker}>
      <TimePicker
        onChange={onChange}
        value={value}
        popupClassName={classes.dropdown}
        {...rest}
      />
    </div>
  );
};

export default AntdTimePicker;

AntdTimePicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  width: PropTypes.number,
};

AntdTimePicker.defaultProps = {
  onChange: () => {},
  value: "",
  width: 195,
};
