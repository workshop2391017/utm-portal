// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { DatePicker } from "antd";
import { ReactComponent as CheckClose } from "assets/icons/BN/close.svg";

// assets
import calendarblue from "assets/icons/BN/calendarblue.svg";

const WhiteDatePicker = (props) => {
  const { value, onChange, style, placeholder, name, format, ...rest } = props;
  const useStyles = makeStyles({
    datePicker: {
      height: 40,
      width: 240,

      borderRadius: 10,
      border: "1px solid #BCC8E7",
      "&:hover": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #BCC8E7",
      },
      "& .ant-picker-input": {
        cursor: "pointer",
        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "& .ant-picker-suffix": {
          cursor: "pointer",
        },
        "& .ant-picker-clear": {
          width: 0,
          display: "none",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      "& .ant-picker-panel-container": {
        borderRadius: 12,
        padding: 20,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        "& .ant-picker-panel": {
          border: "none",
          "& .ant-picker-date-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& thead": {
                  "& tr th": {
                    color: "#374062",
                  },
                },
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                      },
                    },
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        lineHeight: "28px",
                        minWidth: 28,
                        height: 28,
                        color: "#F2F5FD !important",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-month-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-year-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-decade-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#06377B",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-footer": {
            border: "none",
            "& .ant-picker-today-btn": {
              color: "#06377B",
            },
          },
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <DatePicker
      inputReadOnly
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      className={classes.datePicker}
      format={format}
      style={style}
      name={name}
      allowClear={false}
      {...(format && { format })}
      clearIcon={
        value ? (
          <div
            style={{
              height: "40px",
              width: "34px",
              display: "flex",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center",
              backgroundColor: "#0061A7",
              borderTopRightRadius: "10px",
              borderBottomRightRadius: "10px",
              padding: "0px 0px 0px 0px",
              marginRight: "-3px",
            }}
          >
            <CheckClose />
          </div>
        ) : null
      }
      suffixIcon={
        <div
          style={{
            height: "40px",
            width: "52px",
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            borderTopRightRadius: "5px",
            borderBottomRightRadius: "5px",
            padding: "0px 0px 0px 0px",
            marginRight: -17,
          }}
        >
          <img
            src={calendarblue}
            alt="icon-suffix"
            style={{
              cursor: "pointer",
            }}
          />
        </div>
      }
      dropdownClassName={classes.dropdown}
      {...rest}
    />
  );
};

WhiteDatePicker.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  format: PropTypes.string,
};

WhiteDatePicker.defaultProps = {
  value: null,
  onChange: () => console.warn("click onChange"),
  style: null,
  placeholder: "Pilih tanggal lahir Anda",
  name: null,
  format: null,
};

export default WhiteDatePicker;
