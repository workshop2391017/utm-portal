// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { DatePicker, TimePicker } from "antd";

// assets
// import calendar from "../../../../assets/icons/BN/calendar.svg";
import jam from "../../../../assets/icons/BN/jam-white.svg";

const GeneralDatePickerTime = (props) => {
  const { value, onChange, style, placeholder, isFormat } = props;
  const useStyles = makeStyles({
    datePicker: {
      height: 40,
      width: 200,
      borderRadius: 6,
      border: "1px solid #BCC8E7",
      "&:hover": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #BCC8E7",
        // backgroundColor: "yellow",
      },
      "& .ant-picker-input": {
        cursor: "pointer",
        "& input": {
          color: "#0061A7",

          // backgroundColor: "red",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "& .ant-picker-suffix": {
          cursor: "pointer",
          // padding: "0px 0px 0px 0px",
          // backgroundColor: "red",
        },
        "& .ant-picker-clear": {
          width: 0,
          display: "none",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      "& .ant-picker-panel-container": {
        borderRadius: 12,
        padding: 20,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        "& .ant-picker-panel": {
          border: "none",
          "& .ant-picker-date-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& thead": {
                  "& tr th": {
                    color: "#374062",
                  },
                },
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                      },
                    },
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#BCC8E7",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        borderRadius: "50%",
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        lineHeight: "28px",
                        minWidth: 28,
                        height: 28,
                        color: "#F2F5FD !important",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-month-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#BCC8E7",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-year-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#BCC8E7",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-decade-panel": {
            "& .ant-picker-header": {
              border: "none",
              color: "#374062",
              "& .ant-picker-header-view": {
                fontFamily: "Futura",
                fontSize: 16,
              },
            },
            "& .ant-picker-body": {
              "& .ant-picker-content": {
                "& tbody": {
                  "& tr td": {
                    "&.ant-picker-cell-in-view": {
                      "& .ant-picker-cell-inner": {
                        color: "#BCC8E7",
                        "&::before": {
                          display: "none",
                        },
                      },
                    },
                    "&.ant-picker-cell-selected": {
                      "& .ant-picker-cell-inner": {
                        color: "#F2F5FD !important",
                      },
                    },
                  },
                },
              },
            },
          },
          "& .ant-picker-footer": {
            border: "none",
            "& .ant-picker-today-btn": {
              color: "#BCC8E7",
            },
          },
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <React.Fragment>
      {isFormat ? (
        <TimePicker
          format="HH:mm"
          onChange={onChange}
          placeholder={placeholder}
          className={classes.datePicker}
          style={style}
          placement="topRight"
          suffixIcon={
            <div
              style={{
                height: "40px",
                width: "34px",
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                alignItems: "center",
                backgroundColor: "#0061A7",
                borderTopRightRadius: "6px",
                borderBottomRightRadius: "6px",
                padding: "0px 0px 0px 0px",
                marginRight: "-20px",
              }}
            >
              <img
                src={jam}
                alt="icon-suffix"
                style={{
                  cursor: "pointer",
                }}
              />
            </div>
          }

          // dropdownClassName={classes.dropdown}
        />
      ) : (
        <TimePicker
          format="hh:mm"
          onChange={onChange}
          value={value}
          placeholder={placeholder}
          className={classes.datePicker}
          style={style}
          suffixIcon={
            <div
              style={{
                height: "40px",
                width: "52px",
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                alignItems: "center",
                // backgroundColor: "#fff",
                borderTopRightRadius: "5px",
                borderBottomRightRadius: "5px",
                padding: "0px 0px 0px 0px",
                marginRight: "-20px",
              }}
            >
              <img
                src={jam}
                alt="icon-suffix"
                style={{
                  cursor: "pointer",
                }}
              />
            </div>
          }
          dropdownClassName={classes.dropdown}
        />
      )}
    </React.Fragment>
  );
};

GeneralDatePickerTime.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  isFormat: PropTypes.bool,
};

GeneralDatePickerTime.defaultProps = {
  value: null,
  onChange: () => console.warn("click onChange"),
  style: null,
  placeholder: "Placeholder",
  isFormat: false,
};

export default GeneralDatePickerTime;
