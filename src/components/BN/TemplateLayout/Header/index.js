/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
// main
import React, { useState, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from "moment";

// libraries
import {
  makeStyles,
  AppBar,
  Toolbar,
  IconButton,
  ClickAwayListener,
  Grow,
  Paper,
  Popper,
  MenuItem,
  MenuList,
  Badge as MUIBadge,
} from "@material-ui/core";

import { useDispatch } from "react-redux";
import { handleLogout } from "stores/actions/login";

import Avatar from "@material-ui/core/Avatar";
import { withStyles } from "@material-ui/core/styles";

import { getLocalStorage } from "utils/helpers";
import { setData } from "stores/actions/nofitInformation";
import stringAvatar from "helpers/helperChip";
import { pathnameCONFIG } from "configuration";
import { RootContext } from "../../../../router";

// components
import NotificationDetail from "../../Popups/NotificationDetail";
import Badge from "../../Badge";

// assets
import bell from "../../../../assets/icons/BN/bell.svg";
import bellDot from "../../../../assets/icons/BN/bell-dot.svg";
import envelope from "../../../../assets/icons/BN/envelope.svg";
import envelopeOpen from "../../../../assets/icons/BN/envelope-open.svg";
import { ReactComponent as SvgLogout } from "../../../../assets/icons/BN/log-out.svg";
import avalogo from "../../../../assets/images/BN/mhidayat.png";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    height: "60px",
    borderBottom: "1px solid #E6EAF3",
    boxShadow: "none",
    fontFamily: "FuturaMdBT",
  },
  logo: {
    height: 28,
    margin: "16px 20px",
    cursor: "pointer",
  },
  navbar: {
    position: "absolute",
    right: 20,
    top: "47%",
    transform: "translateY(-50%)",
    color: "#0061A7",
    fontSize: 14,
    paddingRight: 42,
    display: "flex",
  },
  icon: {
    marginRight: 8,
  },
  // avatar: {
  //   borderRadius: "50%",
  //   height: 60,
  //   width: 60,
  //   border: "3px solid #FFF",
  //   boxShadow: "0px 10px 10px rgba(193, 207, 227, 0.24)",
  //   backgroundColor: "#0061A7",
  //   color: "#fff",
  //   textAlign: "center",
  //   fontSize: 18,
  //   position: "absolute",
  //   right: 0,
  //   top: "50%",
  //   transform: "translateY(-50%)",
  // },
  avabaru: {
    display: "flex",
    alignItems: "center",

    "& > *": {
      margin: theme.spacing(1),
      height: "50",
      width: "50",
    },
  },
  avalarge: {
    marginTop: 10,
    width: theme.spacing(6),
    height: theme.spacing(6),
  },
  notificationPaper: {
    borderRadius: "0px 0px 10px 10px",
    boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.18)",
  },
  notification: {
    marginTop: 20,
    width: 280,
    height: 300,
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
    "& li": {
      padding: 0,
      "&:hover": {
        backgroundColor: "#F4F7FB",
      },
    },
  },
  notif: {
    width: "100%",
    height: 60,
    boxShadow: "inset 0px -1px 0px #E6EAF3",
    padding: "8px 16px 6px",
    position: "relative",
  },
  envelope: {
    position: "absolute",
    top: 14,
    right: 10,
  },
  profilePosition: {
    left: "42px !important",
  },
  profilePaper: {
    marginTop: 6,
    borderRadius: 8,
    boxShadow: "none",
    border: "1px solid #BCC8E7",
  },
  profile: {
    width: 176,
    minHeight: 135,
    padding: "6px 8px",
  },
  profileContent: {
    fontSize: 10,
    marginBottom: 6,
    fontFamily: "FuturaBkBT",
  },
  profileDot: {
    borderRadius: "50%",
    width: 10,
    height: 10,
    backgroundColor: "#75D37F",
    margin: "3px 5px 0 0",
  },
  profileContentLast: {
    fontSize: 10,
    borderTop: "1px solid #BCC8E7",
    color: "#FF6F6F",
    fontWeight: 900,
    paddingTop: 10,
    position: "relative",
    cursor: "pointer",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  logout: {},
}));

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: "#44b700",
    color: "#44b700",
    top: theme.spacing(2),
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    "&::after": {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      borderRadius: "50%",
      animation: "$ripple 1.2s infinite ease-in-out",
      border: "1px solid currentColor",
      content: "",
    },
  },
  "@keyframes ripple": {
    "0%": {
      transform: "scale(.8)",
      opacity: 1,
    },
    "100%": {
      transform: "scale(2.4)",
      opacity: 0,
    },
  },
}))(MUIBadge);

// const useStyles = makeStyles((theme) => ({
//   avabaru: {
//     display: 'flex',
//     '& > *': {
//       margin: theme.spacing(1),
//     },
//   },
// }));

const Header = ({ onRefresh }) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const lsData = getLocalStorage("portalUserLogin");

  const [bellValue, setBellValue] = useState(true);
  const [dataNotif, setDataNotif] = useState([]);

  // dropdown
  const [openNotif, setOpenNotif] = useState(false);
  const [openProfile, setOpenProfile] = useState(false);
  const anchorRefNotif = useRef(null);
  const anchorRefProfile = useRef(null);

  // profile state
  const [fullName, setFullName] = useState("-");
  const [role, setRole] = useState("-");
  const [branch, setBranch] = useState("-");
  const [lastLogin, setLastLogin] = useState("-");
  const [ipAddress, setIpAddress] = useState("-");

  const [modal, setModal] = useState(false);
  const [isRowSelected, setRowSelected] = useState(false);
  const [selectedRow, setSelectedRow] = useState(null);
  const [userLogin, setUserLogin] = useState({});

  const dummyNotif = [
    {
      title: "Terdapat 2 Transaksi Gagal",
      date: "26/12/2021",
      status: 0,
    },
    {
      title: "Terdapat 5 Transaksi Gagal",
      date: "26/12/2021",
      status: 1,
    },
    {
      title: "Terdapat 2 Transaksi Gagal",
      date: "26/12/2021",
      status: 1,
    },
    {
      title: "Terdapat 3 Transaksi Gagal",
      date: "26/12/2021",
      status: 0,
    },
    {
      title: "Terdapat 10 Transaksi Gagal",
      date: "26/12/2021",
      status: 0,
    },
    {
      title: "Terdapat 2 Transaksi Gagal",
      date: "26/12/2021",
      status: 0,
    },
    {
      title: "Terdapat 5 Transaksi Gagal",
      date: "26/12/2021",
      status: 1,
    },
    {
      title: "Terdapat 2 Transaksi Gagal",
      date: "26/12/2021",
      status: 1,
    },
    {
      title: "Terdapat 3 Transaksi Gagal",
      date: "26/12/2021",
      status: 0,
    },
    {
      title: "Terdapat 10 Transaksi Gagal",
      date: "26/12/2021",
      status: 0,
    },
  ];

  // const handleToggleNotif = () => {
  //   setOpenNotif((prevOpenNotif) => !prevOpenNotif);
  // };
  // const handleToggleProfile = () => {
  //   setOpenProfile((prevOpenProfile) => !prevOpenProfile);
  // };

  useEffect(() => {
    if (lsData) {
      setUserLogin(JSON.parse(lsData));
    } else {
      dispatch(handleLogout());
    }
  }, [lsData]);

  const handleCloseNotif = (event) => {
    if (
      anchorRefNotif.current &&
      anchorRefNotif.current.contains(event.target)
    ) {
      return;
    }
    setOpenNotif(false);
  };
  const handleCloseProfile = (event) => {
    if (
      anchorRefProfile.current &&
      anchorRefProfile.current.contains(event.target)
    ) {
      return;
    }
    setOpenProfile(false);
  };

  const handleListKeyDownNotif = (event) => {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpenNotif(false);
    }
  };
  const handleListKeyDownProfile = (event) => {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpenProfile(false);
    }
  };
  const clickPush = () => {
    history.push("/portal/dashboard");
    document.scrollingElement.scrollTop = 0;
  };

  const prevOpenNotif = useRef(openNotif);
  const prevOpenProfile = useRef(openProfile);

  const handleClickNotif = (row) => {
    setRowSelected(true);
    setSelectedRow(row);
    // setModal(true);
    history.push(pathnameCONFIG.NOTIF);
    dispatch(setData(true));
  };

  useEffect(() => {
    if (prevOpenNotif.current === true && openNotif === false) {
      anchorRefNotif.current.focus();
    }
    prevOpenNotif.current = openNotif;
  }, [openNotif]);

  useEffect(() => {
    if (prevOpenProfile.current === true && openProfile === false) {
      anchorRefProfile.current.focus();
    }
    prevOpenProfile.current = openProfile;
  }, [openProfile]);

  const getExtraZero = (hours) =>
    hours.toString().length < 2 ? `0${hours}` : hours;

  useEffect(() => {
    if (userLogin) {
      setFullName(userLogin.fullName ? userLogin.fullName : "-");
      setRole(userLogin.role ? userLogin.role : "-");
      setBranch(userLogin.branch ? userLogin.branch : "-");
      setIpAddress(userLogin.ip ? userLogin.ip : "-");
      // formatting last login
      const date = new Date(parseInt(userLogin.lastLoginDatetime));
      const fdate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}|${getExtraZero(date.getHours())}:${getExtraZero(
        date.getMinutes()
      )}:${getExtraZero(date.getSeconds())}`;
      setLastLogin(userLogin.lastLoginDatetime ? fdate : "-");

      // get notification data
      // const getNotifData = () => {
      //   const headers = {
      //     headers: {
      //       'Access-Control-Allow-Origin': '*',
      //       'Access-Control-Allow-Credentials': true,
      //       Authorization: `Bearer ${userLogin.token}`,
      //     },
      //   };
      //   axios
      //     .post(
      //       `${process.env.REACT_APP_API_BN}/getnewnotification`,
      //       {},
      //       headers
      //     )
      //     .then((res) => {
      //       setDataNotif(res.data.notificationList);
      //     })
      //     .catch((err) => alert(err));
      // };
      // getNotifData();
      // setInterval(getNotifData, 300000);
    }
  }, [userLogin]);

  return (
    <div>
      <AppBar className={classes.root}>
        <Toolbar style={{ padding: 0 }}>
          {/* <img
            src={''}
            alt="Logo"
            className={classes.logo}
            onClick={clickPush}
          /> */}
          <div className={classes.navbar}>
            <div
              className={classes.icon}
              style={{ position: "relative", zIndex: 2 }}
            >
              <div>
                <div className={classes.avabaru}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-end",
                    }}
                  >
                    <div>
                      <span>{userLogin.fullName}</span>
                    </div>
                    <div>
                      <Badge
                        label={
                          <div style={{ textTransform: "capitalize" }}>
                            {userLogin?.role
                              ?.replaceAll("_", ", ")
                              ?.toLowerCase()}
                          </div>
                        }
                        styleBadge={{
                          padding: "2px 8px !important",
                          height: 20,
                          borderRadius: "4px",
                          fontFamily: "FuturaMdBT",
                          fontSize: 12,
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          backgroundColor: stringAvatar(userLogin?.role ?? ""),
                          color: "#fff",
                        }}
                      />
                    </div>
                  </div>
                  <StyledBadge
                    overlap="circular"
                    anchorOrigin={{
                      vertical: "bottom",
                      horizontal: "right",
                    }}
                    variant="dot"
                    ref={anchorRefProfile}
                    controls={openProfile ? "menu-list-grow" : undefined}
                    aria-haspopup="true"
                    onClick={() => setOpenProfile(!openProfile)}
                    style={{ cursor: "pointer", marginRight: "12px" }}
                  >
                    <Avatar
                      src={avalogo}
                      alt="AV"
                      className={classes.avalarge}
                    />
                    {/* <IconButton
                      aria-label="show 17 new notifications"
                      color="inherit"
                    >
                      <Badge badgeContent={17} color="secondary">
                        <Bell />
                      </Badge>
                    </IconButton> */}
                  </StyledBadge>

                  <IconButton
                    color="primary"
                    size="small"
                    ref={anchorRefNotif}
                    aria-controls={openNotif ? "menu-list-grow" : undefined}
                    aria-haspopup="true"
                    onClick={() => setOpenNotif(!openNotif)}
                    style={{ cursor: "pointer", height: 35, width: 35 }}
                  >
                    <img
                      style={{ width: 20, height: 20 }}
                      src={bellValue ? bellDot : bell}
                      alt="Notification"
                    />
                  </IconButton>
                </div>
              </div>
              <Popper
                open={openNotif}
                anchorEl={anchorRefNotif.current}
                role={undefined}
                transition
                disablePortal
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "center bottom",
                    }}
                  >
                    <Paper className={classes.notificationPaper}>
                      <ClickAwayListener onClickAway={handleCloseNotif}>
                        <MenuList
                          autoFocusItem={openNotif}
                          id="menu-list-grow"
                          onKeyDown={handleListKeyDownNotif}
                          className={classes.notification}
                        >
                          {dummyNotif.map((item) => (
                            <MenuItem>
                              <div
                                className={classes.notif}
                                onClick={() => handleClickNotif(item)}
                              >
                                <p
                                  style={{
                                    fontSize: 16,
                                    marginBottom: 3,
                                    fontFamily: "FuturaMdBT",
                                  }}
                                >
                                  Terdapat{" "}
                                  {item.totalNotification
                                    ? item.totalNotification
                                    : 0}{" "}
                                  Transaksi Gagal
                                </p>
                                <p style={{ fontSize: 12, color: "#BCC8E7" }}>
                                  {moment(item.date).format("DD/MM/YYYY")}
                                </p>
                                <img
                                  src={item.isSeen ? envelopeOpen : envelope}
                                  alt={
                                    item.isSeen
                                      ? "Opened Envelope"
                                      : "Closed Envelope"
                                  }
                                  className={classes.envelope}
                                />
                              </div>
                            </MenuItem>
                          ))}
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
              <Popper
                open={openProfile}
                anchorEl={anchorRefProfile.current}
                role={undefined}
                transition
                disablePortal
                className={classes.profilePosition}
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "center bottom",
                    }}
                  >
                    <Paper className={classes.profilePaper}>
                      <ClickAwayListener onClickAway={handleCloseProfile}>
                        <MenuList
                          autoFocusItem={openProfile}
                          id="menu-list-grow"
                          onKeyDown={handleListKeyDownProfile}
                          className={classes.profile}
                        >
                          {/* <div
                            className={classes.profileContent}
                            style={{ fontWeight: 700, fontFamily: "Futura" }}
                          >
                            {fullName}
                          </div> */}
                          <div
                            className={classes.profileContent}
                            style={{ textTransform: "capitalize" }}
                          >
                            {role?.replaceAll("_", ", ")?.toLowerCase()}
                          </div>
                          <div className={classes.profileContent}>{branch}</div>
                          <div
                            className={classes.profileContent}
                            style={{ display: "flex" }}
                          >
                            <div className={classes.profileDot} />
                            <div>Last Login {lastLogin}</div>
                          </div>
                          <div className={classes.profileContent}>
                            {ipAddress}
                          </div>
                          <div
                            className={classes.profileContentLast}
                            onClick={() => dispatch(handleLogout())}
                          >
                            <span style={{ fontFamily: "FuturaMdBT" }}>
                              Log Out
                            </span>
                            <SvgLogout
                              className={classes.logout}
                              width={20}
                              height={14.29}
                            />
                          </div>
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </div>
          </div>
        </Toolbar>
      </AppBar>
      <NotificationDetail
        isOpen={modal}
        isRowSelected={isRowSelected}
        selectedRow={selectedRow}
        handleClose={() => setModal(false)}
      />
    </div>
  );
};

export default Header;
