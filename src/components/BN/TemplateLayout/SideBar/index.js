// main
import React from "react";
import { I18nextProvider } from "react-i18next";

// libraries
import { Layout } from "antd";
import { makeStyles } from "@material-ui/core";

// components
import SideBarMenu from "./SideBarMenu";

const { Sider } = Layout;

const SideBar = () => {
  const useStyles = makeStyles({
    sider: {
      fontFamily: "FuturaMdBT",
      position: "fixed",
      height: "100%",
      left: 0,
      backgroundColor: "#fff",
      overflowY: "auto",
      zIndex: 1100,
      "&::-webkit-scrollbar": {
        display: "none",
      },
    },
  });
  const classes = useStyles();

  return (
    <I18nextProvider>
      <Sider data-test-id="sidebar-menu" width={200} className={classes.sider}>
        <div style={{ marginTop: 60 }}>
          <SideBarMenu />
        </div>
      </Sider>
    </I18nextProvider>
  );
};

export default SideBar;
