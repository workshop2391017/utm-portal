// main
import React, { useState, useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";

// libraries
import { makeStyles } from "@material-ui/core";
import { Menu } from "antd";
import { getLocalStorage, setLocalStorage } from "utils/helpers";

import { pathnameCONFIG } from "configuration";
import { useDispatch } from "react-redux";
import { setMenuId } from "stores/actions/menus";
import { allMenus, menuMaker, menuApprover, menuMaster } from "./dataMenu";

const { SubMenu } = Menu;

const useStyles = makeStyles({
  menu: {
    "& .ant-menu": {
      fontSize: 10,
      borderRight: "none",
      color: "#374062",
      "& .ant-menu-item": {
        margin: 0,
        minHeight: 44,
        lineHeight: "44px",
        "& span": {
          marginLeft: 10,
        },
        "&::after": {
          borderRight: "none",
        },
        "&.ant-menu-item-selected": {
          backgroundColor: "#0061A7",
          color: "#fff",
          "&:hover": {
            color: "#fff",
            backgroundColor: "#0061A7",
          },
        },
        "&:hover": {
          color: "#374062",
          backgroundColor: "#F4F7FB",
        },
      },
      "& .ant-menu-submenu": {
        minHeight: 44,
        "& .ant-menu-submenu-title": {
          margin: 0,
          minHeight: 44,
          lineHeight: "44px",
          "& span": {
            marginLeft: 10,
          },
          "& .ant-menu-submenu-arrow::before": {
            backgroundImage:
              "linear-gradient(to right, rgba(0, 97, 167, 0.9), rgba(0, 97, 167, 0.9))",
          },
          "& .ant-menu-submenu-arrow::after": {
            backgroundImage:
              "linear-gradient(to right, rgba(0, 97, 167, 0.9), rgba(0, 97, 167, 0.9))",
          },
        },
        "& .ant-menu-sub.ant-menu-inline": {
          backgroundColor: "#fff",
          "& li": {
            paddingLeft: "58px !important",
            "&::after": {
              borderRight: "none",
            },
            "&.ant-menu-item-selected": {
              backgroundColor: "#0061A7",
              color: "#fff",
            },
          },
        },
        "&.ant-menu-submenu-open": {
          backgroundColor: "#fff",
          color: "#374062",
          "&.ant-menu-submenu-active .ant-menu-submenu-title:hover": {
            color: "#374062",
            backgroundColor: "#fff",
          },
          "&.ant-menu-submenu-selected": {
            color: "#374062",
          },
          "& .ant-menu-submenu-title": {
            "& .ant-menu-submenu-arrow::before": {
              backgroundImage:
                "linear-gradient(to right, rgba(0, 97, 167, 0.9), rgba(0, 97, 167, 0.9))",
            },
            "& .ant-menu-submenu-arrow::after": {
              backgroundImage:
                "linear-gradient(to right, rgba(0, 97, 167, 0.9), rgba(0, 97, 167, 0.9))",
            },
          },
        },
        "&.ant-menu-submenu-active": {
          "& .ant-menu-submenu-title": {
            "&:hover": {
              color: "#374062",
              backgroundColor: "#F4F7FB",
            },
          },
        },
        "&.ant-menu-submenu-selected": {
          color: "#374062",
        },
      },
    },
    "& .ant-menu:not(:has(.ant-menu-submenu)) .ant-menu-item": {
      // Apply styles only to parent menus without submenus
    },
    "& .ant-menu .ant-menu-submenu-title": {
      fontSize: "11px",
      fontWeight: "bold",
    },
  },
  menuItem: {
    fontSize: "11px",
    fontWeight: "bold",
  },
});
const filter = (array, path) => {
  const getChildren = (result, object) => {
    if (object?.path === path) {
      result.push(object);
      return result;
    }
    if (Array.isArray(object.submenus)) {
      const submenus = object.submenus.reduce(getChildren, []);
      if (submenus.length) result.push({ ...object, submenus });
    }
    return result;
  };

  return array.reduce(getChildren, []);
};

const SideBarMenu = () => {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const lsData = getLocalStorage("portalUserLogin");
  const dispatch = useDispatch();

  const [openKeys, setOpenKeys] = useState("0");
  const [selectedMenu, setSelectedMenu] = useState("0");
  const [menusPermission, setMenusPermission] = useState([]);

  const grlfchar = (value) =>
    value
      ?.split(" ")
      ?.filter((a) => a)
      ?.join(" ");

  useEffect(() => {
    if (lsData) {
      const userData = JSON.parse(lsData);
      const userPrivelege = userData.portalGroupDetailDtoList[0]?.masterMenu;
      const userLoginStorage = getLocalStorage("portalUserLogin");
      const userLogin = userLoginStorage ? JSON.parse(userLoginStorage) : {};

      let menuUser = menuMaster;
      const privilegeNameP = [];

      userPrivelege?.forEach((a) => {
        privilegeNameP.push(grlfchar(a?.name_en));
        a?.menuAccessChild?.forEach((b) => {
          privilegeNameP.push(
            [grlfchar(b.name_en), grlfchar(a.name_en)].join("")
          );
        });
      });

      const menusAccess = menuUser?.filter((a) => {
        // contain child
        if (a?.submenus?.length) {
          const filterData = a?.submenus?.filter((b) =>
            privilegeNameP?.includes(
              [grlfchar(b.name), grlfchar(a.name)].join("")
            )
          );

          if (filterData.length && privilegeNameP?.includes(grlfchar(a.name))) {
            a.submenus = filterData;
            return a;
          }
        }

        //  just parent
        if (!a?.submenus?.length && privilegeNameP?.includes(grlfchar(a.name)))
          return a;
        // Hard code Menu Parent
        if (a.name === "Dashboard") return a;

        return false;
      });

      const addMenuIdToMenus = (menusAccess ?? []).map((elm, i) => {
        const fIndex = userPrivelege.find(
          (f) => grlfchar(elm.name) === grlfchar(f.name_en)
        );

        return {
          ...elm,
          key: `${i}`,
          menuId: fIndex?.id,
          ...(elm?.submenus && {
            submenus: (elm?.submenus ?? []).map((c, ci) => {
              const cfIndex = null;

              return {
                ...c,
                key: `${i}-${ci}`,
                menuId: fIndex?.menuAccessChild?.find(
                  (cf) => grlfchar(c.name) === grlfchar(cf.name_en)
                )?.id,
              };
            }),
          }),
        };
      });
      setMenusPermission(addMenuIdToMenus);
    }
  }, [lsData]);

  const handleCurrentSidebar = () => {
    const path = location.pathname;

    const currentMenu = filter(menusPermission, path);

    const menuId = Number(getLocalStorage("portalMenuId"));
    dispatch(setMenuId(menuId));

    if (currentMenu?.length) {
      setSelectedMenu(
        currentMenu[0]?.submenus
          ? currentMenu[0]?.submenus[0]?.key
          : currentMenu[0]?.key
      );
      setOpenKeys([currentMenu[0]?.key ?? "0"]);
    }
  };

  useEffect(() => {
    if (menusPermission) handleCurrentSidebar();
  }, [menusPermission]);

  const handlePushPage = (menu, submenu) => {
    if (menu) {
      if (!menu.submenus) {
        dispatch(setMenuId(menu?.menuId));
        setLocalStorage("portalMenuId", menu?.menuId?.toString());
      }
      let newOpenKey = [...openKeys];
      if (newOpenKey.find((e) => e === menu.key) && !submenu) {
        newOpenKey = newOpenKey?.filter((e) => e !== menu.key);
      } else {
        newOpenKey = [...openKeys, menu.key];
      }

      setOpenKeys(newOpenKey);
      setSelectedMenu(menu?.key);
      if (menu?.path) history.push(menu?.path);
    }
    if (submenu) {
      dispatch(setMenuId(submenu?.menuId));
      setLocalStorage("portalMenuId", submenu?.menuId?.toString());
      setSelectedMenu(submenu?.key);
      history.push(submenu?.path);
    }
  };

  return (
    <div className={classes.menu}>
      <Menu
        mode="inline"
        openKeys={openKeys}
        selectedKeys={[selectedMenu]}
        style={{ width: "100%" }}
      >
        {menusPermission.map((menu, i) =>
          !menu?.submenus ? (
            <Menu.Item
              key={menu.key}
              onClick={() => {
                handlePushPage(menu);
                document.scrollingElement.scrollTop = 0;
              }}
              className={classes.menuItem}
            >
              {menu.name}
            </Menu.Item>
          ) : (
            <SubMenu
              key={menu.key}
              title={menu.name}
              onTitleClick={() => {
                handlePushPage(menu);
                document.scrollingElement.scrollTop = 0;
              }}
            >
              {menu.submenus.map((submenu) => (
                <Menu.Item
                  key={submenu.key}
                  onClick={() => {
                    handlePushPage(menu, submenu);
                    document.scrollingElement.scrollTop = 0;
                  }}
                >
                  {submenu.name}
                </Menu.Item>
              ))}
            </SubMenu>
          )
        )}
      </Menu>
    </div>
  );
};

SideBarMenu.propTypes = {};

SideBarMenu.defaultProps = {};

export default SideBarMenu;
