import { pathnameCONFIG } from "configuration";

export const allMenus = [
  {
    idSubMenu: "Dashboard",
    name: "Dashboard",
    path: "/portal/dashboard",
  },
  {
    idSubMenu: "pengaturan",
    name: "Settings",
    privilegeCategory: "pengaturan",
    submenus: [
      {
        id: 1,
        name: "General Parameter",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      // {
      //   id: 2,
      //   name: "Privilege Settings",
      //   path: pathnameCONFIG.SETTING.PENGATURAN_HAK_AKSES,
      // },
      {
        id: 3,
        name: "User Settings",
        path: pathnameCONFIG.SETTING.USER_SETTING,
      },
      // {
      //   id: 5,
      //   name: "Branch Settings",
      //   path: pathnameCONFIG.SETTING.BRANCH_SETTING,
      // },
      // {
      //   id: 4,
      //   name: "User Role",
      //   path: pathnameCONFIG.SETTING.PERAN_PENGGUNA,
      // },
    ],
  },
  {
    idSubMenu: "audittrail",
    name: "Audit Trail",
    privilegeCategory: "audittrail",
    submenus: [
      {
        id: 1,
        name: "Customer",
        path: pathnameCONFIG.AUDIT_TRAIL.CUSTOMER,
      },
      {
        id: 2,
        name: "Admin Bank",
        path: pathnameCONFIG.AUDIT_TRAIL.ADMIN_BANK,
      },
    ],
  },
  {
    idSubMenu: "persetujuanMatrix",
    name: "Approval Matrix",
    privilegeCategory: "pengaturan",
    submenus: [
      {
        id: 15,
        name: "Customers",
        path: pathnameCONFIG.PERSETUJUAN_MATRIX.NASABAH.LIST,
      },
      {
        id: 16,
        name: "Bank Admin",
        path: pathnameCONFIG.PERSETUJUAN_MATRIX.ADMIN_BANK.LIST,
      },
    ],
  },
  {
    idSubMenu: "Approval",
    name: "Approval",
    // path: "/portal/security",
    privilegeCategory: "approval",
    submenus: [
      {
        id: 6,
        name: "Privilege Approval",
        path: pathnameCONFIG.APPROVAL.HAK_AKSES,
      },
      {
        id: 7,
        name: "User Approval",
        path: pathnameCONFIG.APPROVAL.USER_APPROVAL,
      },
      {
        id: 8,
        // name: "Config Approval",
        name: "Parameter Approval",
        path: pathnameCONFIG.APPROVAL.CONFIG_APPROVAL,
      },
      {
        id: 9,
        name: "Matrix Approval",
        path: pathnameCONFIG.APPROVAL.MATRIX,
      },
    ],
  },
  // {
  //   idSubMenu: "managementPerusahaan",
  //   name: "Company Management",
  //   // path: "/portal/management-company",
  //   privilegeCategory: "management_corp",
  //   submenus: [
  //     {
  //       id: 10,
  //       name: "Registration",
  //       path: pathnameCONFIG.MANAGEMENT_COMPANY.REGISTRATION,
  //     },
  //     {
  //       id: 11,
  //       name: "Change Authorization",
  //       path: pathnameCONFIG.MANAGEMENT_COMPANY.CHANGE_AUTHORIZATION,
  //     },
  //     {
  //       id: 12,
  //       name: "Company",
  //       path: pathnameCONFIG.MANAGEMENT_COMPANY.COMPANY,
  //     },
  //     // // TODO: wf dan template blm nemu
  //     // {
  //     //   id: 13,
  //     //   name: "Pengaturan Alur Kerja",
  //     //   privilegeCategory: "wf_template_settings",
  //     //   path: pathnameCONFIG.MANAGEMENT_COMPANY.WORKFLOW_SETTING,
  //     // },
  //     // {
  //     //   id: 14,
  //     //   name: "Pengaturan Template",
  //     //   privilegeCategory: "wf_template_settings",
  //     //   path: pathnameCONFIG.MANAGEMENT_COMPANY.TEMPLATE_SETTING,
  //     // },
  //   ],
  // },
  {
    idSubMenu: "globalMaintenance",
    name: "Global Maintenance",
    privilegeCategory: "global_maintenance",
    submenus: [
      // {
      //   id: 14,
      //   name: "Global Menu",
      //   path: pathnameCONFIG.GLOBAL_MAINTENANCE.MENU_GLOBAL,
      // },
      // {
      //   id: 14,
      //   name: "Account Product",
      //   path: pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_PRODUCT_REKENING,
      // },
      // {
      //   id: 14,
      //   name: "Service",
      //   path: pathnameCONFIG.GLOBAL_MAINTENANCE.SERVICE_LIMIT_TRANSACTION,
      // },
      {
        id: 14,
        name: "Transaction Limit",
        path: pathnameCONFIG.GLOBAL_MAINTENANCE.GLOBAL_LIMIT_TRANSACTION,
      },
    ],
  },
  // {
  //   idSubMenu: "segmentasiNasabah",
  //   name: "Customers Segmentation",
  //   // privilegeCategory: null,
  //   // path: pathnameCONFIG.SEGMENTATION_USER.BASE_URL,
  //   submenus: [
  //     {
  //       name: "Segmentation List",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_USER.BASE_URL,
  //     },

  //     {
  //       name: "Account Type",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.JENIS_REKENING,
  //     },
  //     {
  //       name: "Limit Package",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.LIMIT_PACKAGE,
  //     },
  //     {
  //       name: "Charge List",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.CHARGE_LIST,
  //     },
  //     {
  //       name: "Service Charge List",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.SERVICE_CHARGE_PACKAGE,
  //     },
  //     {
  //       name: "Charge Package",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.CHARGE_PACKAGE,
  //     },
  //     {
  //       name: "Menu Package",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.MENU_PACKAGE,
  //     },
  //     {
  //       name: "Tiering/Slab",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.TEIRING_SLAB,
  //     },
  //     {
  //       name: "Branch Segmentation",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.SEGMEN_CABANG,
  //     },
  //     {
  //       name: "Special Menu",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_USER.MENU_KHUSUS,
  //     },
  //     {
  //       name: "Parameter Setting",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.PARAMETER_SETTING,
  //     },

  //     // {
  //     //   id: 14,
  //     //   name: "Report",
  //     //   privilegeCategory: "report",
  //     //   path: pathnameCONFIG.SEGMENTATION_USER.REPORT,
  //     // },
  //   ],
  // },
  // {
  //   idSubMenu: "companyLimitCompanyCharge",
  //   name: "Company Limit & Charge",
  //   submenus: [
  //     {
  //       name: "Company Limit",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT,
  //     },
  //     {
  //       name: "Company Charge",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE,
  //     },
  //   ],
  // },

  {
    idSubMenu: "maintenanceUmum",
    name: "General Maintenance",
    // path: "/portal/maintenance-umum",
    privilegeCategory: "maintenance_umum",
    submenus: [
      {
        id: 14,
        name: "Host Error Mapping",
        privilegeCategory: "maintenance_umum",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.HOST_ERROR_MAPPING,
      },
      {
        id: 15,
        name: "Domestic Bank",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK,
      },
      {
        id: 16,
        name: "International Bank",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.INTERNATIONAL_BANK,
      },
      // TODO: blm nemu
      // {
      //   id: 17,
      //   name: "Exchange Rate",
      //   privilegeCategory: null,
      //   path: pathnameCONFIG.GENERAL_MAINTENANCE.EXCHANGE_RATE,
      // },
      {
        id: 18,
        name: "Handling Officer",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.HANDLING_OFFICER,
      },
      // {
      //   id: 19,
      //   name: "VA Institution",
      //   path: pathnameCONFIG.GENERAL_MAINTENANCE.INSTITUSI_VA,
      // },
      // {
      //   id: 20,
      //   name: "Organization Unit",
      //   privilegeCategory: "organization_unit",
      //   path: pathnameCONFIG.GENERAL_MAINTENANCE.ORGANIZATION_UNIT,
      // },
    ],
  },
  // {
  //   idSubMenu: "sub6",
  //   name: "Maintenance Transaksi",
  //   path: "/portal/monitoring-report",
  //   submenus: [
  //     // TODO: Belum Nemu
  //     {
  //       id: 20,
  //       name: "Global Limit Transaksi",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_LIMIT_TRANSACTION,
  //     },
  //     {
  //       id: 21,
  //       name: "Global Limit Forex",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_LIMIT_FOREX,
  //     },
  //     {
  //       id: 22,
  //       name: "Global Forex Usage",
  //       path: "/aktivitas-nasabah",
  //     },
  //     {
  //       id: 23,
  //       name: "Company Limit",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT,
  //     },
  //     {
  //       id: 24,
  //       name: "Forex Special Rate",
  //       path: "/aktivitas-nasabah2",
  //     },
  //     {
  //       id: 25,
  //       name: "Company Charge",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHANGE,
  //     },
  //     {
  //       id: 26,
  //       name: "Time Deposit Special Rate",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.TIME_DEPOSIT_SPECIAL_RATE,
  //     },
  //   ],
  // },

  // TODO: Benarkah
  {
    idSubMenu: "approvalWorkflow",
    name: "Approval Workflow",
    privilegeCategory: "approval_workflow",
    path: pathnameCONFIG.APPROVAL_WORKFLOW.BASE_URL,
  },
  {
    idSubMenu: "pendingTask",
    name: "Pending Task",
    // path: "/portal/pending-task",
    privilegeCategory: "pending_task",
    submenus: [
      {
        id: 33,
        name: "Registration",
        path: pathnameCONFIG.PENDING_TASK.REGISTRATION,
      },
      {
        id: 34,
        name: "Change Authorization",
        path: pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION,
      },
      {
        id: 35,
        name: "Soft Token Registration",
        path: pathnameCONFIG.PENDING_TASK.SOFT_TOKEN_REGISTRATION,
      },
      // {
      //   id: 36,
      //   name: "Virtual Account Registration",
      //   privilegeCategory: "pending_task_va",
      //   path: pathnameCONFIG.PENDING_TASK.ACTIVATION_VA,
      // },
      {
        id: 37,
        name: "Auto Collection",
        path: pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION,
      },
      // {
      //   id: 36,
      //   name: "Pengaktifan Virtual Account",
      //   privilegeCategory: "pending_task_va",
      //   path: pathnameCONFIG.PENDING_TASK.ACTIVATION_VA,
      // },
    ],
  },
  // {
  //   idSubMenu: "sub8",
  //   name: "Periodical Charge",
  //   path: pathnameCONFIG.PERIODICAL_CHARGE_INSTRUCTION,
  // },
  {
    idSubMenu: "OnlineSubmissionReport ",
    name: "Online Submission Report ",
    submenus: [
      // {
      //   id: 37,
      //   name: "Virtual Account Report",
      //   privilegeCategory: null,
      //   path: pathnameCONFIG.ONLINE_SUBMISSION_REPORT.VA,
      // },
      {
        id: 37,
        name: "Auto Collection Report",
        privilegeCategory: null,
        path: pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION,
      },
      {
        id: 37,
        name: "Auto Collection Detail",
        privilegeCategory: null,
        path: pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION_DETAIL,
      },
    ],
  },
  {
    idSubMenu: "sub10",
    name: "Utility",
    submenus: [
      {
        id: 37,
        name: "Event Calendar",
        path: pathnameCONFIG.UTILITAS.CALENDAR_EVENT,
      },
      {
        id: 38,
        name: "User Settings",
        path: pathnameCONFIG.UTILITAS.MANAGEMENT_USER,
      },
      {
        id: 39,
        name: "Content Management",
        path: pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT,
      },
      {
        id: 40,
        name: "Product Information",
        path: pathnameCONFIG.UTILITAS.PRODUCT_INFORMATION,
      },
      {
        id: 41,
        name: "Help Desk",
        path: pathnameCONFIG.UTILITAS.HELP_DESK.INBOX,
      },
    ],
  },
  {
    idSubMenu: "sub7",
    name: "Cut Off Time Settings",
    privilegeCategory: null,
    path: pathnameCONFIG.CUT_OFF_TIME_SETTINGS.BASE_URL,
    submenus: [
      {
        id: 27,
        name: "Cut Off Time",
        privilegeCategory: null,
        path: pathnameCONFIG.CUT_OFF_TIME_SETTINGS.BASE_URL,
      },
    ],
  },
  {
    idSubMenu: "ConsolidatedReport",
    name: "Consolidation Report",
    submenus: [
      {
        id: 37,
        name: "Consolidation Report",
        path: pathnameCONFIG.CONSOLIDATION_REPORT.LIST,
      },
    ],
  },
  {
    idSubMenu: "Inquiry",
    name: "Inquiry",
    submenus: [
      {
        id: 380,
        name: "Periodical Charge List",
        path: pathnameCONFIG.INQUIRY.PERIODICAL_CHARGE_LIST,
      },
      {
        id: 381,
        name: "Transaction Status ",
        path: pathnameCONFIG.INQUIRY.TRANSACTION_STATUS,
      },
      {
        id: 382,
        name: "Non Transaction Status ",
        path: pathnameCONFIG.INQUIRY.NON_TRANSACTION_STATUS,
      },
    ],
  },
  {
    idSubMenu: "sub11",
    name: "Promo Management",
    submenus: [
      {
        id: 37,
        name: "Promo List",
        path: pathnameCONFIG.MANAGEMENT_PROMO.LIST_PROMO,
      },
      {
        id: 37,
        name: "Merchant List",
        path: pathnameCONFIG.MERCHANT.LIST_MERCHANT,
      },
      {
        id: 37,
        name: "Promo Category",
        path: pathnameCONFIG.MANAGEMENT_PROMO.PROMO_CATEGORY,
      },
    ],
  },
  // {
  //   idSubMenu: "NotificationSettings",
  //   name: "Notification Settings",
  //   privilegeCategory: null,
  //   // path: pathnameCONFIG.CONSOLIDATION_REPORT.LIST,
  // },
  // {
  //   idSubMenu: "FAQFileDownload",
  //   name: "FAQ File Download",
  //   privilegeCategory: null,
  //   // path: pathnameCONFIG.CONSOLIDATION_REPORT.LIST,
  // },
  // {
  //   idSubMenu: "sub12",
  //   name: "Notifikasi",
  //   path: "/portal/notification",
  // },
  {
    idSubMenu: "blastPromo",
    name: "Blast Promo",
    path: pathnameCONFIG.NOTIFICATION,
  },
  {
    idSubMenu: "notifInformation",
    name: "Notif Information",
    path: pathnameCONFIG.NOTIF,
  },
  {
    idSubMenu: "sub9",
    name: "Payment Setup",
    privilegeCategory: "payment_setup",
    submenus: [
      {
        id: 50,
        name: "Biller Manager",
        path: pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER,
      },
      {
        id: 51,
        name: "Category Biller",
        path: pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI,
      },
    ],
  },
  {
    idSubMenu: "workshop",
    name: "Workshop Page",
    path: pathnameCONFIG.WORKSHOP,
  },
  {
    idSubMenu: "workshoptwo",
    name: "Workshop Custom",
    path: pathnameCONFIG.WORKSHOP_CUSTOM,
  },

  {
    idSubMenu: "custompage",
    name: "Custom Page",
    path: pathnameCONFIG.CUSTOM_PAGE,
  },
  // {
  //   idSubMenu: "FAQ Download",
  //   name: "FAQ Download",
  //   submenus: [
  //     {
  //       id: 1,
  //       name: "FAQ Management",
  //       path: pathnameCONFIG.FAQ_DOWNLOAD.CATEGORY,
  //     },
  //     {
  //       id: 2,
  //       name: "FAQ Admin Bank",
  //       path: pathnameCONFIG.FAQ_DOWNLOAD.LIST,
  //     },
  //   ],
  // },
  // {
  //   idSubMenu: "virtualAccount",
  //   name: "Virtual Account",
  //   path: pathnameCONFIG.VIRTUAL_ACCOUNT,
  // },
  // {
  //   idSubMenu: "hardToken",
  //   name: "Hard Token",
  //   path: pathnameCONFIG.HARD_TOKEN,
  // },
];

export const menuMaker = [
  {
    idSubMenu: "Dashboard",
    name: "Dashboard",
    path: "/portal/dashboard",
  },
  {
    idSubMenu: "pengaturan",
    name: "Settings",
    privilegeCategory: "pengaturan",
    submenus: [
      {
        id: 1,
        name: "General Parameter",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      // {
      //   id: 2,
      //   name: "Privilege Settings",
      //   path: pathnameCONFIG.SETTING.PENGATURAN_HAK_AKSES,
      // },
      {
        id: 3,
        name: "User Settings",
        path: pathnameCONFIG.SETTING.USER_SETTING,
      },
      // {
      //   id: 5,
      //   name: "Branch Settings",
      //   path: pathnameCONFIG.SETTING.BRANCH_SETTING,
      // },
      // {
      //   id: 4,
      //   name: "User Role",
      //   path: pathnameCONFIG.SETTING.PERAN_PENGGUNA,
      // },
    ],
  },
  {
    idSubMenu: "audittrail",
    name: "Audit Trail",
    privilegeCategory: "audittrail",
    submenus: [
      {
        id: 1,
        name: "Customer",
        path: pathnameCONFIG.AUDIT_TRAIL.CUSTOMER,
      },
      {
        id: 2,
        name: "Admin Bank",
        path: pathnameCONFIG.AUDIT_TRAIL.ADMIN_BANK,
      },
    ],
  },
  {
    idSubMenu: "persetujuanMatrix",
    name: "Approval Matrix",
    privilegeCategory: "pengaturan",
    submenus: [
      {
        id: 15,
        name: "Customers",
        path: pathnameCONFIG.PERSETUJUAN_MATRIX.NASABAH.LIST,
      },
      {
        id: 16,
        name: "Bank Admin",
        path: pathnameCONFIG.PERSETUJUAN_MATRIX.ADMIN_BANK.LIST,
      },
    ],
  },
  {
    idSubMenu: "managementPerusahaan",
    name: "Company Management",
    // path: "/portal/management-company",
    privilegeCategory: "management_corp",
    submenus: [
      {
        id: 10,
        name: "Registration",
        path: pathnameCONFIG.MANAGEMENT_COMPANY.REGISTRATION,
      },
      {
        id: 11,
        name: "Change Authorization",
        path: pathnameCONFIG.MANAGEMENT_COMPANY.CHANGE_AUTHORIZATION,
      },
      {
        id: 12,
        name: "Company",
        path: pathnameCONFIG.MANAGEMENT_COMPANY.COMPANY,
      },
      // // TODO: wf dan template blm nemu
      // {
      //   id: 13,
      //   name: "Pengaturan Alur Kerja",
      //   privilegeCategory: "wf_template_settings",
      //   path: pathnameCONFIG.MANAGEMENT_COMPANY.WORKFLOW_SETTING,
      // },
      // {
      //   id: 14,
      //   name: "Pengaturan Template",
      //   privilegeCategory: "wf_template_settings",
      //   path: pathnameCONFIG.MANAGEMENT_COMPANY.TEMPLATE_SETTING,
      // },
    ],
  },
  // {
  //   idSubMenu: "segmentasiNasabah",
  //   name: "Customers Segmentation",
  //   // privilegeCategory: null,
  //   // path: pathnameCONFIG.SEGMENTATION_USER.BASE_URL,
  //   submenus: [
  //     {
  //       name: "Segmentation List",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_USER.BASE_URL,
  //     },

  //     {
  //       name: "Account Type",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.JENIS_REKENING,
  //     },
  //     {
  //       name: "Limit Package",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.LIMIT_PACKAGE,
  //     },
  //     {
  //       name: "Charge List",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.CHARGE_LIST,
  //     },
  //     {
  //       name: "Service Charge List",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.SERVICE_CHARGE_PACKAGE,
  //     },
  //     {
  //       name: "Charge Package",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.CHARGE_PACKAGE,
  //     },
  //     {
  //       name: "Menu Package",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.MENU_PACKAGE,
  //     },
  //     {
  //       name: "Tiering/Slab",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.TEIRING_SLAB,
  //     },
  //     {
  //       name: "Branch Segmentation",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.SEGMEN_CABANG,
  //     },
  //     {
  //       name: "Special Menu",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_USER.MENU_KHUSUS,
  //     },
  //     {
  //       name: "Parameter Setting",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.SEGMENTATION_PACKAGE.PARAMETER_SETTING,
  //     },

  //     // {
  //     //   id: 14,
  //     //   name: "Report",
  //     //   privilegeCategory: "report",
  //     //   path: pathnameCONFIG.SEGMENTATION_USER.REPORT,
  //     // },
  //   ],
  // },
  // {
  //   idSubMenu: "companyLimitCompanyCharge",
  //   name: "Company Limit & Charge",
  //   submenus: [
  //     {
  //       name: "Company Limit",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT,
  //     },
  //     {
  //       name: "Company Charge",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE,
  //     },
  //   ],
  // },
  {
    idSubMenu: "globalMaintenance",
    name: "Global Maintenance",
    privilegeCategory: "global_maintenance",
    submenus: [
      {
        id: 14,
        name: "Global Menu",
        path: pathnameCONFIG.GLOBAL_MAINTENANCE.MENU_GLOBAL,
      },
      // {
      //   id: 14,
      //   name: "Account Product",
      //   path: pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_PRODUCT_REKENING,
      // },
      {
        id: 14,
        name: "Transaction Limit",
        path: pathnameCONFIG.GLOBAL_MAINTENANCE.GLOBAL_LIMIT_TRANSACTION,
      },
      // {
      //   id: 14,
      //   name: "Service",
      //   path: pathnameCONFIG.GLOBAL_MAINTENANCE.SERVICE_LIMIT_TRANSACTION,
      // },
    ],
  },
  {
    idSubMenu: "maintenanceUmum",
    name: "General Maintenance",
    // path: "/portal/maintenance-umum",
    privilegeCategory: "maintenance_umum",
    submenus: [
      {
        id: 14,
        name: "Host Error Mapping",
        privilegeCategory: "maintenance_umum",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.HOST_ERROR_MAPPING,
      },
      {
        id: 15,
        name: "Domestic Bank",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK,
      },
      {
        id: 16,
        name: "International Bank",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.INTERNATIONAL_BANK,
      },
      // TODO: blm nemu
      {
        id: 17,
        name: "Exchange Rate",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.EXCHANGE_RATE,
      },
      {
        id: 18,
        name: "Handling Officer",
        path: pathnameCONFIG.GENERAL_MAINTENANCE.HANDLING_OFFICER,
      },
      // {
      //   id: 19,
      //   name: "VA Institution",
      //   path: pathnameCONFIG.GENERAL_MAINTENANCE.INSTITUSI_VA,
      // },
      {
        id: 20,
        name: "Organization Unit",

        path: pathnameCONFIG.GENERAL_MAINTENANCE.ORGANIZATION_UNIT,
      },
    ],
  },
  {
    idSubMenu: "blastPromo",
    name: "Blast Promo",
    path: pathnameCONFIG.NOTIFICATION,
  },
  {
    idSubMenu: "notifInformation",
    name: "Notif Information",
    path: pathnameCONFIG.NOTIF,
  },

  // {
  //   idSubMenu: "sub6",
  //   name: "Maintenance Transaksi",
  //   path: "/portal/monitoring-report",
  //   submenus: [
  //     // TODO: Belum Nemu
  //     {
  //       id: 20,
  //       name: "Global Limit Transaksi",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_LIMIT_TRANSACTION,
  //     },
  //     {
  //       id: 21,
  //       name: "Global Limit Forex",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_LIMIT_FOREX,
  //     },
  //     {
  //       id: 22,
  //       name: "Global Forex Usage",
  //       path: "/aktivitas-nasabah",
  //     },
  //     {
  //       id: 23,
  //       name: "Company Limit",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT,
  //     },
  //     {
  //       id: 24,
  //       name: "Forex Special Rate",
  //       path: "/aktivitas-nasabah2",
  //     },
  //     {
  //       id: 25,
  //       name: "Company Charge",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHANGE,
  //     },
  //     {
  //       id: 26,
  //       name: "Time Deposit Special Rate",
  //       privilegeCategory: null,
  //       path: pathnameCONFIG.TRANSACTION_MAINTENANCE.TIME_DEPOSIT_SPECIAL_RATE,
  //     },
  //   ],
  // },

  // {
  //   idSubMenu: "sub8",
  //   name: "Periodical Charge",
  //   path: pathnameCONFIG.PERIODICAL_CHARGE_INSTRUCTION,
  // },
  {
    idSubMenu: "OnlineSubmissionReport ",
    name: "Online Submission Report ",
    submenus: [
      // {
      //   id: 37,
      //   name: "Virtual Account Report",
      //   privilegeCategory: null,
      //   path: pathnameCONFIG.ONLINE_SUBMISSION_REPORT.VA,
      // },
      {
        id: 37,
        name: "Auto Collection Report",
        privilegeCategory: null,
        path: pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION,
      },
      {
        id: 37,
        name: "Auto Collection Detail",
        privilegeCategory: null,
        path: pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION_DETAIL,
      },
    ],
  },
  {
    idSubMenu: "sub10",
    name: "Utility",
    submenus: [
      {
        id: 37,
        name: "Event Calendar",
        path: pathnameCONFIG.UTILITAS.CALENDAR_EVENT,
      },
      {
        id: 38,
        name: "User Settings",
        path: pathnameCONFIG.UTILITAS.MANAGEMENT_USER,
      },
      {
        id: 39,
        name: "Content Management",
        path: pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT,
      },
      {
        id: 40,
        name: "Product Information",
        path: pathnameCONFIG.UTILITAS.PRODUCT_INFORMATION,
      },
      {
        id: 41,
        name: "Help Desk",
        path: pathnameCONFIG.UTILITAS.HELP_DESK.INBOX,
      },
    ],
  },
  {
    idSubMenu: "sub7",
    name: "Cut Off Time Settings",
    privilegeCategory: null,
    path: pathnameCONFIG.CUT_OFF_TIME_SETTINGS.BASE_URL,
    submenus: [
      {
        id: 27,
        name: "Cut Off Time",
        privilegeCategory: null,
        path: pathnameCONFIG.CUT_OFF_TIME_SETTINGS.BASE_URL,
      },
    ],
  },
  {
    idSubMenu: "ConsolidatedReport",
    name: "Consolidation Report",
    submenus: [
      {
        id: 37,
        name: "Consolidation Report",
        path: pathnameCONFIG.CONSOLIDATION_REPORT.LIST,
      },
    ],
  },
  {
    idSubMenu: "Inquiry",
    name: "Inquiry",
    submenus: [
      {
        id: 380,
        name: "Periodical Charge List",
        path: pathnameCONFIG.INQUIRY.PERIODICAL_CHARGE_LIST,
      },
      {
        id: 381,
        name: "Transaction Status ",
        path: pathnameCONFIG.INQUIRY.TRANSACTION_STATUS,
      },
      {
        id: 382,
        name: "Non Transaction Status ",
        path: pathnameCONFIG.INQUIRY.NON_TRANSACTION_STATUS,
      },
    ],
  },
  {
    idSubMenu: "sub9",
    name: "Payment Setup",
    privilegeCategory: "payment_setup",
    submenus: [
      {
        id: 50,
        name: "Biller Manager",
        path: pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER,
      },
      {
        id: 51,
        name: "Category Biller",
        path: pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI,
      },
    ],
  },
  {
    idSubMenu: "sub11",
    name: "Promo Management",
    submenus: [
      {
        id: 37,
        name: "Promo List",
        path: pathnameCONFIG.MANAGEMENT_PROMO.LIST_PROMO,
      },
      {
        id: 37,
        name: "Merchant List",
        path: pathnameCONFIG.MERCHANT.LIST_MERCHANT,
      },
      {
        id: 37,
        name: "Category List",
        path: pathnameCONFIG.MANAGEMENT_PROMO.PROMO_CATEGORY,
      },
    ],
  },
  // {
  //   idSubMenu: "NotificationSettings",
  //   name: "Notification Settings",
  //   privilegeCategory: null,
  //   // path: pathnameCONFIG.CONSOLIDATION_REPORT.LIST,
  // },
  // {
  //   idSubMenu: "FAQFileDownload",
  //   name: "FAQ File Download",
  //   privilegeCategory: null,
  //   // path: pathnameCONFIG.CONSOLIDATION_REPORT.LIST,
  // },
  // {
  //   idSubMenu: "sub12",
  //   name: "Notifikasi",
  //   path: "/portal/notification",
  // },
  // {
  //   idSubMenu: "FAQ Download",
  //   name: "FAQ Download",
  //   submenus: [
  //     {
  //       id: 1,
  //       name: "FAQ Management",
  //       path: pathnameCONFIG.FAQ_DOWNLOAD.CATEGORY,
  //     },
  //     {
  //       id: 2,
  //       name: "FAQ Admin Bank",
  //       path: pathnameCONFIG.FAQ_DOWNLOAD.LIST,
  //     },
  //   ],
  // },
  {
    idSubMenu: "virtualAccount",
    name: "Virtual Account",
    path: pathnameCONFIG.VIRTUAL_ACCOUNT,
  },

  {
    idSubMenu: "hardToken",
    name: "Hard Token",
    path: pathnameCONFIG.HARD_TOKEN,
  },
  {
    idSubMenu: "workshop",
    name: "Workshop Page",
    path: pathnameCONFIG.WORKSHOP,
  },
  {
    idSubMenu: "workshoptwo",
    name: "Workshop Custom",
    path: pathnameCONFIG.WORKSHOP_CUSTOM,
  },
  {
    idSubMenu: "custompage",
    name: "Custom Page",
    path: pathnameCONFIG.CUSTOM_PAGE,
  },
];

export const menuApprover = [
  {
    idSubMenu: "Dashboard",
    name: "Dashboard",
    path: "/portal/dashboard",
  },
  {
    idSubMenu: "audittrail",
    name: "Audit Trail",
    privilegeCategory: "audittrail",
    submenus: [
      {
        id: 1,
        name: "Customer",
        path: pathnameCONFIG.AUDIT_TRAIL.CUSTOMER,
      },
      {
        id: 2,
        name: "Admin Bank",
        path: pathnameCONFIG.AUDIT_TRAIL.ADMIN_BANK,
      },
    ],
  },
  {
    idSubMenu: "Approval",
    name: "Approval",
    // path: "/portal/security",
    privilegeCategory: "approval",
    submenus: [
      {
        id: 6,
        name: "Privilege Approval",
        path: pathnameCONFIG.APPROVAL.HAK_AKSES,
      },
      {
        id: 7,
        name: "User Approval",
        path: pathnameCONFIG.APPROVAL.USER_APPROVAL,
      },
      {
        id: 8,
        // name: "Config Approval",
        name: "Parameter Approval",
        path: pathnameCONFIG.APPROVAL.CONFIG_APPROVAL,
      },
      {
        id: 9,
        name: "Matrix Approval",
        path: pathnameCONFIG.APPROVAL.MATRIX,
      },
    ],
  },
  // TODO: Benarkah
  {
    idSubMenu: "approvalWorkflow",
    name: "Approval Workflow",
    privilegeCategory: "approval_workflow",
    path: pathnameCONFIG.APPROVAL_WORKFLOW.BASE_URL,
  },
  {
    idSubMenu: "pendingTask",
    name: "Pending Task",
    // path: "/portal/pending-task",
    privilegeCategory: "pending_task",
    submenus: [
      {
        id: 33,
        name: "Registration",
        path: pathnameCONFIG.PENDING_TASK.REGISTRATION,
      },
      {
        id: 34,
        name: "Change Authorization",
        path: pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION,
      },
      {
        id: 35,
        name: "Soft Token Registration",
        path: pathnameCONFIG.PENDING_TASK.SOFT_TOKEN_REGISTRATION,
      },
      // {
      //   id: 36,
      //   name: "Virtual Account Registration",
      //   privilegeCategory: "pending_task_va",
      //   path: pathnameCONFIG.PENDING_TASK.ACTIVATION_VA,
      // },
      {
        id: 37,
        name: "Auto Collection",
        path: pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION,
      },
      // {
      //   id: 36,
      //   name: "Pengaktifan Virtual Account",
      //   privilegeCategory: "pending_task_va",
      //   path: pathnameCONFIG.PENDING_TASK.ACTIVATION_VA,
      // },
    ],
  },
  {
    idSubMenu: "blastPromo",
    name: "Blast Promo",
    path: pathnameCONFIG.NOTIFICATION,
  },
  {
    idSubMenu: "notifInformation",
    name: "Notif Information",
    path: pathnameCONFIG.NOTIF,
  },
  // {
  //   idSubMenu: "sub9",
  //   name: "Payment Setup",
  //   path: "/portal/payment-setup",
  //   privilegeCategory: "payment_setup",
  //   submenus: [
  //     {
  //       id: 50,
  //       name: "Biller Manager",
  //       path: pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER,
  //     },
  //     {
  //       id: 51,
  //       name: "Category Biller",
  //       path: pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI,
  //     },
  //   ],
  // },
  {
    idSubMenu: "virtualAccount",
    name: "Virtual Account",
    path: pathnameCONFIG.VIRTUAL_ACCOUNT,
  },
  {
    idSubMenu: "hardToken",
    name: "Hard Token",
    path: pathnameCONFIG.HARD_TOKEN,
  },
  {
    idSubMenu: "workshop",
    name: "Workshop Page",
    path: pathnameCONFIG.WORKSHOP,
  },
  {
    idSubMenu: "workshoptwo",
    name: "Workshop Custom",
    path: pathnameCONFIG.WORKSHOP_CUSTOM,
  },
  {
    idSubMenu: "custompage",
    name: "Custom Page",
    path: pathnameCONFIG.CUSTOM_PAGE,
  },
];

export const menuMaster = [
  {
    idSubMenu: "dashboard",
    name: "Dashboard",
    path: "/portal/dashboard",
  },
  {
    idSubMenu: "usermanagement",
    name: "User Management",
    submenus: [
      {
        id: 1,
        name: "User Manager",
        path: pathnameCONFIG.USER_MANAGEMENT.USER_MANAGER,
      },
      {
        id: 2,
        name: "Role Manager",
        path: pathnameCONFIG.USER_MANAGEMENT.ROLE_MANAGER,
      },
      {
        id: 3,
        name: "Menu Manager",
        path: pathnameCONFIG.SETTING.USER_SETTING,
      },
      {
        id: 4,
        name: "Unit Manager",
        path: pathnameCONFIG.SETTING.USER_SETTING,
      },
    ],
  },
  {
    idSubMenu: "globalparameter",
    name: "Global Parameter",
    submenus: [
      {
        id: 1,
        name: "General Parameter",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "transactionmanagement",
    name: "Transaction Management",
    submenus: [
      {
        id: 1,
        name: "Transaction History",
        path: pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_HISTORY,
      },
    ],
  },
  {
    idSubMenu: "limitmanagement",
    name: "Limit Management",
    submenus: [
      {
        id: 1,
        name: "Limit",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "promomanagement",
    name: "Promo Management",
    submenus: [
      {
        id: 1,
        name: "Promo List",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 2,
        name: "Merchant List",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 3,
        name: "Blast Information",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "notificationmanagement",
    name: "Notification Management",
    submenus: [
      {
        id: 1,
        name: "Notification Configuration",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "surroundingconfiguration",
    name: "Surrounding Configuration",
    path: "/portal/dashboard",
  },
  {
    idSubMenu: "audittrail",
    name: "Audit Trail",
    submenus: [
      {
        id: 1,
        name: "Admin",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "transfermanager",
    name: "Transfer Manager",
    submenus: [
      {
        id: 1,
        name: "Bank",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 2,
        name: "Transfer Category",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 3,
        name: "Transfer Configuration",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "billermanager",
    name: "Biller Manager",
    submenus: [
      {
        id: 1,
        name: "Biller Category",
        path: pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI,
      },
      {
        id: 2,
        name: "Biller Configuration",
        path: pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER,
      },
    ],
  },
  {
    idSubMenu: "masterdata",
    name: "Master Data",
    submenus: [
      {
        id: 1,
        name: "Harmonized Error Code",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 2,
        name: "Currency",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 3,
        name: "Channel",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
      {
        id: 4,
        name: "Channel Permission",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
  {
    idSubMenu: "taskmanager",
    name: "Task Manager",
    submenus: [
      {
        id: 1,
        name: "Task List",
        path: pathnameCONFIG.SETTING.GENERAL_PARAMETER,
      },
    ],
  },
];
