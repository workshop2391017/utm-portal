import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import TapContactSupport from "../Menus/TapContactSupport";
import ScrollCustom from "../ScrollCustom";
import PaginationComponent from "./Pagination";

const useStyles = makeStyles({
  card: {
    height: "240px",
    width: "320px",
    backgroundColor: "#fff",
    borderRadius: "10px",
  },
  header: {
    padding: "12px 15px",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    height: "42px",

    borderBottom: "1px solid #E8EEFF",
  },
  content: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  page: {},
  cardContent: {
    height: "240px",
    width: "320px",
    backgroundColor: "#fff",
    borderRadius: "10px",
    // marginRight: "28px",
    marginBottom: "18px",
  },
});
const TitleCard = styled.div`
  color: #374062;
  font-size: 15px;
  font-family: FuturaHvBT;
  font-weight: 400;
`;
const Text = styled.p`
  color: #374062;
  font-size: 13px;
  font-family: FuturaBQ;
  padding: 15px;
`;

// const data = {
//   title: "Response",
// };

const TableCard = (props) => {
  const { data, onDelete, onEdit, totalData, page, setPage, totalElement } =
    props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <Grid className={classes.content}>
        {data?.map((item) => (
          <div className={classes.cardContent}>
            <div className={classes.header}>
              <TitleCard>{item?.title} </TitleCard>
              <TapContactSupport
                rowData={data}
                options={["edit", "delete"]}
                onEdit={() => onEdit(item)}
                onDelete={onDelete}
              />
            </div>
            <ScrollCustom height={159}>
              <Text>{item?.description}</Text>
            </ScrollCustom>
          </div>
        ))}
      </Grid>
      {totalData ? (
        <div style={{ marginTop: "20px" }}>
          <PaginationComponent
            totalElement={totalElement}
            currentData={data.length}
            totalPage={totalData} // total page
            page={page}
            onChange={setPage}
          />
        </div>
      ) : null}
    </React.Fragment>
  );
};

TableCard.propTypes = {};

export default TableCard;
