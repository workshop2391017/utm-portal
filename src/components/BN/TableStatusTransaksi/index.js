/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
// Table Component
// --------------------------------------------------------
import React, { Fragment } from "react";
import PropTypes from "prop-types";
import classname from "classnames";
import {
  Card,
  makeStyles,
  CardContent,
  CircularProgress,
} from "@material-ui/core";
import Colors from "helpers/colors";

// assets
import NoData from "assets/images/BN/illustrationred.png";

import CheckboxSingle from "../../SC/Checkbox/CheckboxSingle";
import PaginationComponent from "./Pagination";

const Tables = ({
  className,
  type,
  headerContent,
  dataContent,
  extraComponents,
  isFullTable,
  widthTable,
  tableRounded,
  onClickRow,
  selecable,
  handleSelect,
  handleSelectAll,
  selectedValue,
  setPage,
  page,
  totalData,
  isLoading,
  totalElement,
  noDataMessage,
  rowColor,
  bordered,
  headerClassName,
}) => {
  const classNames = classname("o-table", className);

  const handleSelectRow = (id) => {
    const ids = [...selectedValue];

    if (ids.includes(id)) {
      ids.splice(ids.indexOf(id), 1);
    } else {
      ids.push(id);
    }

    handleSelect(ids);
  };

  const handleSelectRowAll = () => {
    const ids = [...selectedValue];

    const allIds = dataContent
      .filter((elm) => !elm.status)
      .map((item) => item.id);

    handleSelectAll(allIds.length === ids.length ? [] : allIds);
  };

  const useStyle = makeStyles({
    column: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
      width: `calc(100% / ${headerContent.length})`,
      //   marginRight: "30px",
      fontFamily: "FuturaMdBT",
      lineHeight: "16px",
      border: "none",

      fontWeight: 400,
      fontSize: 13,
    },
    cardHeader: {
      minWidth: 37,
      border: 0,
      marginBottom: "20px",
      borderRadius: "0px",
      backgroundColor: "#0061A7",
      color: "white",
      ...(tableRounded && {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }),
    },
    cardContentHeader: {
      display: "none",
      flexDirection: "row",
      justifyContent: "space-between",
      padding: "10px 20px",
    },
    rowWrapper: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    columnWrapper: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
      width: `calc(100% / ${headerContent.length})`,
      fontFamily: "FuturaBQ",
      position: "relative",
      paddingRight: "15px",
      fontSize: 13,
      minHeight: 46,
    },
    tableBlank: {
      width: "100%",
      height: 416,
      boxShadow: `0px 3px 10px rgba(188, 200, 231, 0.2)`,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    noDataImg: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  });
  const classes = useStyle();

  return (
    <div>
      <div
        className={classNames}
        style={{ width: isFullTable ? "1056px" : widthTable }}
      >
        <div
          className={
            headerClassName
              ? `${classes.cardHeader} ${headerClassName}`
              : classes.cardHeader
          }
          style={{ maxHeight: "37px" }}
        >
          <CardContent className={classes.cardContentHeader}>
            {headerContent.map((headerItem, index) => (
              <React.Fragment key={index}>
                {selecable && index === 0 ? (
                  <div style={{ width: "50px" }}>
                    <CheckboxSingle
                      checked={
                        selectedValue.length ===
                        (dataContent ?? []).filter(({ status }) => !status)
                          .length
                      }
                      onChange={handleSelectRowAll}
                    />
                  </div>
                ) : null}

                <div
                  className={classes.column}
                  style={{
                    ...(headerItem.width && {
                      width: headerItem.width,
                    }),
                  }}
                >
                  {headerItem.title}
                </div>
              </React.Fragment>
            ))}
          </CardContent>
        </div>
        {isLoading ? (
          <Card className={classes.tableBlank}>
            <CircularProgress color="primary" size={40} />
          </Card>
        ) : (
          type === "detail" && (
            <Fragment>
              {dataContent.map((item, dataIindex) => (
                <Card
                  onClick={() => onClickRow(item)}
                  style={{
                    // table type 'bordered'
                    ...(bordered
                      ? {
                          borderBottom: "1px solid #E8EEFF",
                          boxShadow: "none",
                        }
                      : {
                          boxShadow: rowColor
                            ? "none"
                            : `0px 3px 10px rgba(188, 200, 231, 0.2)`,
                          marginBottom: 5,
                        }),
                    backgroundColor: rowColor || "white",

                    ...(dataIindex === dataContent.length - 1 &&
                      tableRounded && {
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10,
                      }),
                  }}
                  key={dataIindex}
                >
                  <CardContent
                    style={{
                      minHeight: 46,
                      paddingTop: 0,
                      paddingBottom: 0,
                    }}
                  >
                    <div className={classes.rowWrapper}>
                      {headerContent.map((headerItem, index) => (
                        <React.Fragment key={index}>
                          {selecable && index === 0 ? (
                            <div style={{ width: "50px" }}>
                              <CheckboxSingle
                                disabled={item.status}
                                checked={
                                  selectedValue.includes(item.id) || item.status
                                }
                                onChange={() => handleSelectRow(item.id)}
                              />
                            </div>
                          ) : null}

                          <div
                            className={classes.columnWrapper}
                            style={{
                              ...(headerItem.width && {
                                width: headerItem.width,
                              }),
                              ...(headerItem.fontFamily && {
                                fontFamily: headerItem.fontFamily,
                              }),
                            }}
                          >
                            {headerItem?.render
                              ? headerItem?.render(item, dataIindex)
                              : item[headerItem.key]}
                          </div>
                        </React.Fragment>
                      ))}
                    </div>
                  </CardContent>
                </Card>
              ))}

              {dataContent.length === 0 && (
                <Card className={classes.tableBlank}>
                  <div>
                    <div className={classes.noDataImg}>
                      <img src={NoData} alt="no data" />
                    </div>
                    <div className={classes.noDataMessage}>{noDataMessage}</div>
                  </div>
                </Card>
              )}
            </Fragment>
          )
        )}
        <div style={{ marginTop: 20 }}>
          {selectedValue.length > 0 && extraComponents ? extraComponents : null}
        </div>
      </div>

      {totalData ? (
        <div style={{ marginTop: "20px" }}>
          <PaginationComponent
            totalElement={totalElement}
            currentData={dataContent.length}
            totalPage={totalData} // total page
            page={page}
            onChange={setPage}
          />
        </div>
      ) : null}
    </div>
  );
};

Tables.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(["detail", "text"]),
  headerContent: PropTypes.array,
  dataContent: PropTypes.array,
  isFullTable: PropTypes.bool,
  widthTable: PropTypes.string,
  selecable: PropTypes.bool,
  handleSelect: PropTypes.func,
  selectedValue: PropTypes.array,
  isLoading: PropTypes.bool,
  handleSelectAll: PropTypes.func,
  setPage: PropTypes.func,
  page: PropTypes.number,
  totalData: PropTypes.number,
  onClickRow: PropTypes.func,
  tableRounded: PropTypes.bool,
  noDataMessage: PropTypes.node,
  totalElement: PropTypes.number,
  rowColor: PropTypes.string,
  bordered: PropTypes.bool,
  headerClassName: PropTypes.object,
};

Tables.defaultProps = {
  className: "",
  type: "detail",
  headerContent: [],
  dataContent: [],
  handleSelectAll: () => {},
  handleSelect: () => {},
  setPage: () => {},
  onClickRow: () => {},
  page: 1,
  totalData: 0,
  isFullTable: true,
  selectedValue: [],
  widthTable: "",
  isLoading: false,
  selecable: false,
  tableRounded: true,
  noDataMessage: <div>Data Tidak Ditemukan</div>,
  totalElement: 0,
  rowColor: "",
  bordered: false,
  headerClassName: {},
};

export default Tables;
