/* eslint-disable jsx-a11y/aria-role */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
// RoleCard Component
// --------------------------------------------------------

import React from "react";
import PropTypes from "prop-types";
import { styled, Switch } from "@material-ui/core";
import { formatCurrency } from "utils/helpers/formatCurrency";
import AntdTextField from "../TextField/AntdTextField";

// import colors from "../../../helpers/colors";
// import { Icon } from "../../atoms";

const Container = styled("div")(({ active }) => ({
  padding: "11px 13px",
  borderRadius: "10px",
  width: "fit-content",
  backgroundColor: "#fff",
  minWidth: "280px",
  transition: "all ease .3s",
  ":hover": {
    borderColor: "#0061A7",
  },
  ...(active
    ? {
        border: `1px solid #0061A7`,
      }
    : {
        border: `1px solid #BCC8E7`,
      }),
}));

const SpaceBetween = styled("div")(({ alignItems = "center" }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems,
  svg: {
    color: "white",
    cursor: "pointer",
  },
  marginTop: "10px",
}));

const Text = styled("span")(({ fontSize = "15px", fontWeight }) => ({
  fontSize,
  color: "#374062",
  fontWeight,
  fontFamily: "FuturaMdBT",
}));

const Nominal = styled("span")(() => ({
  color: "#0061A7",
  fontSize: "13px",
  fontFamily: "FuturaMdBT",
}));

const WrapperNominal = styled("div")(() => ({
  marginTop: "5px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));

const Currency = styled("div")(() => ({
  //   color: colors.primary.hard,
  fontSize: "10px",
  marginRight: "5px",

  color: "#0061A7",
}));

const RoleCard = ({ className, data, handleClickDelete, icon, onClick }) => (
  <Container
    className={className}
    onClick={data.handleClickSuccess}
    active={data.active}
    style={{
      cursor: "pointer",
      height: "89px",
    }}
  >
    <SpaceBetween>
      <Text>{data.title}</Text>
      <div role="persentation" onClick={data.handleClickDelete}>
        {icon}
      </div>
    </SpaceBetween>
    <SpaceBetween alignItems="flex-end">
      <Text fontWeight="300" fontSize="13px">
        {data.subTitle}
      </Text>
      <WrapperNominal>
        <Currency> {data.currency}</Currency>

        <div
          style={{
            color: "#0061A7",
            fontSize: "13px",
            fontFamily: "FuturaMdBT",
          }}
        >
          {formatCurrency(data?.nominal)}
        </div>
      </WrapperNominal>
    </SpaceBetween>
  </Container>
);

RoleCard.propTypes = {
  className: PropTypes.string,
  data: PropTypes.object,
  handleClickDelete: PropTypes.func,
};

RoleCard.defaultProps = {
  className: "",
  data: {
    handleClickDelete: () => {},
  },
  handleClickDelete: () => {},
};

export default RoleCard;
