import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Collapse as MUICollapse, Grid } from "@material-ui/core";
import propTypes from "prop-types";
import { ReactComponent as IconArrowDown } from "assets/icons/BN/chevron-down-med.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "15px 20px 15px 20px",
    borderRadius: 10,
    border: "1px solid #B3C1E7",
    width: "100%",
  },
  container: {
    width: "100%",
    paddingTop: 10,
  },
  paperContent: {
    boxShadow: "none",
    width: "100%",
    height: "auto",
  },
  collapseButton: {
    display: "flex",
    position: "relative",
    justifyContent: "space-between",
    boxShadow: "none",
    cursor: "pointer",
    alignItems: "center",
    "& .icongroup": {
      display: "flex",
      marginRight: 10,
      alignItems: "center",
      "&:hover": {
        cursor: "pointer",
      },
      "& .pen": {
        marginRight: 13,
      },
    },
  },
  label: {
    fontFamily: "FuturaMdBT",
    fontSize: 17,
    color: "#374062",
  },
  desc: {
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    color: "#7B87AF",
  },
}));

const Collapse = ({
  child,
  label,
  desc,
  defaultOpen,
  style,
  collapse,
  isOpenCollapse,
  customOpenCollapse,
}) => {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  useEffect(() => {
    setChecked(defaultOpen);
  }, [defaultOpen]);

  return (
    <div className={classes.root}>
      <Paper
        className={classes.collapseButton}
        onClick={collapse ? handleChange : () => {}}
        style={style}
      >
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.label}>
            {label}
          </Grid>
          <Grid item className={classes.desc}>
            {desc}
          </Grid>
        </Grid>
        <div className="icongroup">
          {collapse && (
            <IconArrowDown
              style={
                isOpenCollapse || checked ? { transform: "rotate(180deg)" } : {}
              }
            />
          )}
        </div>
      </Paper>
      <div className={classes.container}>
        <MUICollapse in={customOpenCollapse ? isOpenCollapse : checked}>
          <Paper elevation={4} className={classes.paperContent}>
            {child}
          </Paper>
        </MUICollapse>
      </div>
    </div>
  );
};

Collapse.propTypes = {
  child: propTypes.node.isRequired,
  label: propTypes.string,
  desc: propTypes.string,
  defaultOpen: propTypes.bool,
  style: propTypes.object,
  collapse: propTypes.bool,
};

Collapse.defaultProps = {
  label: "Label",
  desc: "",
  defaultOpen: false,
  style: {},
  collapse: true,
};

export default Collapse;
