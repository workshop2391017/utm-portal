import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Collapse as MUICollapse } from "@material-ui/core";
import propTypes from "prop-types";

import { ReactComponent as IconArrowDown } from "assets/icons/BN/chevron-down-med.svg";

import { ReactComponent as IconPen } from "assets/icons/BN/edit-2.svg";

const useStyles = makeStyles((theme) => ({
  container: {
    width: "100%",
  },
  paperContent: {
    boxShadow: "none",
    width: "100%",
    height: "auto",
  },
  collapseButton: {
    display: "flex",
    position: "relative",
    justifyContent: "space-between",
    padding: "12px 15px",
    backgroundColor: "#F4F7FB",
    boxShadow: "none",
    cursor: "pointer",
    alignItems: "center",
    "& .icongroup": {
      display: "flex",
      marginRight: 15,
      alignItems: "center",
      "&:hover": {
        cursor: "pointer",
      },
      "& .pen": {
        marginRight: 18,
      },
    },
  },
  label: {
    fontFamily: "FuturaMdBT",
    fontSize: 17,
    color: "#374062",
  },
}));

const Collapse = ({
  children,
  label,
  labelClassName,
  editable,
  onEdit,
  defaultOpen,
  className,
  style,
  collapse,
  isOpenCollapse,
  customOpenCollapse,
  rightCollapse,
  labelFull,
}) => {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  useEffect(() => {
    setChecked(defaultOpen);
  }, [defaultOpen]);

  return (
    <div>
      <Paper
        className={
          className
            ? `${classes.collapseButton} ${className}`
            : classes.collapseButton
        }
        onClick={collapse ? handleChange : () => {}}
        style={style}
      >
        {collapse && !rightCollapse && (
          <IconArrowDown
            style={
              isOpenCollapse
                ? {
                    transform: "rotate(180deg)",
                  }
                : {}
            }
          />
        )}
        <div
          className={labelClassName ?? classes.label}
          style={{ width: labelFull && "100%" }}
        >
          {label}
        </div>
        <div className="icongroup">
          {editable ? <IconPen className="pen" onClick={onEdit} /> : null}
          {collapse && rightCollapse && (
            <div
              style={{
                transform:
                  isOpenCollapse || checked ? "rotate(180deg)" : "rotate(0deg)",
                transition: "transform 300ms",
              }}
            >
              <IconArrowDown />
            </div>
          )}
        </div>
      </Paper>
      <div className={classes.container}>
        <MUICollapse in={customOpenCollapse ? isOpenCollapse : checked}>
          <Paper
            elevation={4}
            className={classes.paperContent}
            style={{
              backgroundColor: labelFull && "transparent",
            }}
          >
            {children}
          </Paper>
        </MUICollapse>
      </div>
    </div>
  );
};

Collapse.propTypes = {
  children: propTypes.node.isRequired,
  label: propTypes.string,
  editable: propTypes.bool,
  onEdit: propTypes.func,
  defaultOpen: propTypes.bool,
  className: propTypes.object,
  style: propTypes.object,
  collapse: propTypes.bool,
  rightCollapse: propTypes.bool,
  labelFull: propTypes.bool,
};

Collapse.defaultProps = {
  label: "Label",
  editable: false,
  onEdit: () => {},
  defaultOpen: false,
  className: {},
  style: {},
  collapse: true,
  rightCollapse: true,
  labelFull: false,
};

export default Collapse;
