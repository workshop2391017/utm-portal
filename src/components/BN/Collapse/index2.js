import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Collapse as MUICollapse } from "@material-ui/core";
import propTypes from "prop-types";

import { ReactComponent as IconArrowDown } from "assets/icons/BN/chevron-down-med.svg";
import { ReactComponent as IconPen } from "assets/icons/BN/pen.svg";

const useStyles = makeStyles((theme) => ({
  container: {
    width: "100%",
  },
  paperContent: {
    boxShadow: "none",
    width: "100%",
    height: "auto",
  },
  collapseButton: {
    display: "flex",
    position: "relative",
    justifyContent: "space-between",
    padding: "12px 15px",
    backgroundColor: "#F4F7FB",
    boxShadow: "none",
    cursor: "pointer",
    alignItems: "center",
    "& .icongroup": {
      display: "flex",
      marginRight: 15,
      alignItems: "center",
      "&:hover": {
        cursor: "pointer",
      },
      "& .pen": {
        marginRight: 18,
      },
    },
  },
  label: {
    fontFamily: "FuturaHvBT",
    fontSize: 15,
    fontWeight: 900,
    color: "#0061A7",
  },
}));

const Collapse = ({ children, label, editable, onEdit, defaultOpen }) => {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  useEffect(() => {
    setChecked(defaultOpen);
  }, [defaultOpen]);

  return (
    <div>
      <Paper className={classes.collapseButton} onClick={handleChange}>
        <div className={classes.label}>{label}</div>
        <div className="icongroup">
          {editable ? <IconPen className="pen" onClick={onEdit} /> : null}
          <IconArrowDown
            style={checked ? { transform: "rotate(180deg)" } : {}}
          />
        </div>
      </Paper>
      <div className={classes.container}>
        <MUICollapse in={checked}>
          <Paper elevation={4} className={classes.paperContent}>
            {children}
          </Paper>
        </MUICollapse>
      </div>
    </div>
  );
};

Collapse.propTypes = {
  children: propTypes.node.isRequired,
  label: propTypes.string,
  editable: propTypes.bool,
  onEdit: propTypes.func,
  defaultOpen: propTypes.bool,
};

Collapse.defaultProps = {
  label: "Label",
  editable: false,
  onEdit: () => {},
  defaultOpen: false,
};

export default Collapse;
