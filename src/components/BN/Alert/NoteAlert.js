import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles({
  alert: {
    textAlign: "center",
    backgroundColor: "#EAF2FF",
    border: "1px solid #0061A7",
    width: 500,
    borderRadius: 5,
    color: "#0061A7",
  },
  mainContent: {
    padding: "50px",
  },
});

const NoteAlert = ({ text, children, className, icon }) => {
  const classes = useStyles();

  return (
    <Alert
      className={className ? `${classes.alert} ${className}` : classes.alert}
      icon={icon || false}
    >
      {children || text}
    </Alert>
  );
};

NoteAlert.propTypes = {
  text: PropTypes.node,
  children: PropTypes.node,
  icon: PropTypes.node,
};
NoteAlert.defaultProps = {
  text: " Data yang Anda masukkan akan digunakan untuk melakukan pendaftaran pada TEKEN AJA selaku fasilitator Bank dalam melakukan tanda tangan digital",
  children: null,
  icon: false,
};

export default NoteAlert;
