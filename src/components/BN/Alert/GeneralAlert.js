import Alert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/styles";
import Colors from "helpers/colors";
import React from "react";
import PropTypes from "prop-types";

const GeneralALert = ({ icon, text, color }) => {
  const colors = {
    blue: Colors.info.soft,
    border_blue: "#0061A7",
  };

  const alertColor = color ? colors[`border_${color}`] : Colors.info.soft;

  const useStyles = makeStyles({
    alert: {
      "& .MuiAlert-root": {
        border: `1px solid ${alertColor}`,
        backgroundColor: colors[color],
        width: "100% !important",
      },
    },
  });

  const classes = useStyles();

  return (
    <div className={classes.alert}>
      <Alert icon={icon} severity="success">
        {text}
      </Alert>
    </div>
  );
};

export default GeneralALert;

GeneralALert.propTypes = {
  text: PropTypes.node.isRequired,
  icon: PropTypes.node.isRequired,
  color: PropTypes.string.isRequired,
};
