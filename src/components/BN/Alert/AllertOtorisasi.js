import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

const useStyles = () =>
  makeStyles({
    alert: {
      textAlign: "center",
      backgroundColor: "#EAF2FF",
      border: "1px solid #0061A7",
      width: 500,
      fontWeight: "400",
      borderRadius: 5,
      color: "#0061A7",
      fontSize: "10px",
      fontFamily: "FuturaBkBt",
      margin: "35px 145px 35px",
    },
    mainContent: {
      padding: "50px",
    },
  });

const NoteAlert = ({ text, style }) => {
  const classes = useStyles()();

  return (
    <Alert className={classes.alert} icon={false}>
      {text}
    </Alert>
  );
};

NoteAlert.propTypes = {
  text: PropTypes.string,
  style: PropTypes.object,
};
NoteAlert.defaultProps = {
  style: {},
  text: "Either individually or together hereinafter referred to as “Admin Maker” and “Admin Approver”",
};

export default NoteAlert;
