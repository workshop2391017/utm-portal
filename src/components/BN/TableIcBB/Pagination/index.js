// Pagination Component
// --------------------------------------------------------

import React from "react";
import PropTypes from "prop-types";
import { styled } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

const Container = styled("div")(() => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
}));

const Info = styled("p")(() => ({
  fontSize: "15px",
  color: "#7B87AF",
  margin: "0px",
}));

const CustomPagination = styled(Pagination)(() => ({
  backgroundColor: "#ffffff",
  borderRadius: "10px",
  boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
  padding: "7px 6px",
  "& .MuiButtonBase-root": {
    color: "#0061A7 !important",
    margin: "0px",
    fontFamily: "FuturaMdBT",
    fontSize: "15px",
  },
  "& .Mui-selected": {
    backgroundColor: "transparent!important",
    color: "#BCC8E7 !important",
    fontSize: "15px",
  },
  "& .MuiPaginationItem-icon": {
    width: "26px",
    height: "34px",
  },
}));

const WrapperPagination = styled("div")(() => ({}));

const PaginationComponent = ({
  count,
  currentData,
  totalPage,
  onChange,
  page,
  language,
  totalElement,
}) => (
  <Container>
    <Info
      style={{ fontFamily: "FuturaMdBT", color: "#7B87AF", fontWeight: "400" }}
    >
      {language === "id" ? "Showing" : "Show"} {currentData}{" "}
      {language === "id" ? "of " : "from "}
      {totalElement} {language === "id" ? "Lines" : "Column"}
    </Info>
    <WrapperPagination>
      <CustomPagination
        onChange={(e, change) => onChange(change)}
        count={totalPage === 0 ? 1 : totalPage}
        page={page}
        showFirstButton
        showLastButton
      />
    </WrapperPagination>
  </Container>
);

PaginationComponent.propTypes = {
  count: PropTypes.number,
  currentData: PropTypes.string,
  totalPage: PropTypes.number,
  onChange: PropTypes.func,
  page: PropTypes.number,
  language: PropTypes.string,
  totalElement: PropTypes.number,
};

PaginationComponent.defaultProps = {
  count: 0,
  currentData: "",
  totalPage: 0,
  onChange: () => {},
  page: 1,
  language: "id",
  totalElement: 0,
};

export default PaginationComponent;
