// Table Component
// --------------------------------------------------------
import React, { Fragment } from "react";
import PropTypes from "prop-types";
import classname from "classnames";
import {
  Card,
  makeStyles,
  CardContent,
  CircularProgress,
} from "@material-ui/core";
import Colors from "helpers/colors";

// assets
import NoData from "assets/images/BN/illustrationred.png";

import CheckboxSingle from "../../SC/Checkbox/CheckboxSingle";
import PaginationComponent from "./Pagination";
import ScrollCustom from "../ScrollCustom";

const Tables = ({
  scroll,
  className,
  type,
  headerContent,
  dataContent,
  extraComponents,
  isFullTable,
  widthTable,
  tableRounded,
  onClickRow,
  selecable,
  handleSelect,
  handleSelectAll,
  selectedValue,
  setPage,
  page,
  totalData,
  isLoading,
  totalElement,
  noDataMessage,
  rowColor,
  bordered,
  headerClassName,
  noSubMessage,
  columnStyle,
  allowNullStripped,
  scrollHeight,
  stripe,
}) => {
  const classNames = classname("o-table", className);

  const handleSelectRow = (id, dataIindex) => {
    const ids = [...selectedValue];

    if (ids.includes(id)) {
      ids.splice(ids.indexOf(id), 1);
    } else {
      ids.push(id);
    }

    handleSelect(ids, dataIindex);
  };

  const handleSelectRowAll = (e) => {
    if (e.target.checked) {
      const arr = dataContent
        .filter((item) => item?.disabled === false)
        .map((item) => item.id);
      const dataTemp = [...selectedValue, ...arr];

      handleSelectAll(dataTemp);
    } else {
      handleSelectAll([]);
    }
  };

  const useStyle = makeStyles((theme) => ({
    column: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
      width: `calc(100% / ${headerContent.length})`,
      //   marginRight: "30px",
      fontFamily: "FuturaMdBT",
      lineHeight: "16px",
      border: "none",

      fontWeight: 400,
      fontSize: 13,
      [theme.breakpoints.up("md")]: {
        fontSize: 11,
      },
      [theme.breakpoints.up("lg")]: {
        fontSize: 13,
      },
      [theme.breakpoints.up("xl")]: {
        fontSize: 13,
      },
    },
    cardHeader: {
      minWidth: 37,
      border: 0,
      marginBottom: "20px",
      borderRadius: "0px",
      backgroundColor: "#0061A7",
      color: "white",
      ...(tableRounded && {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }),
    },
    cardContentHeader: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      padding: "10px 15px",
      paddingBottom: "10px !important",
    },
    rowWrapper: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    columnWrapper: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
      width: `calc(100% / ${headerContent.length})`,
      fontFamily: "FuturaBQ",
      position: "relative",
      paddingRight: "15px",
      fontSize: 13,
      paddingBottom: 12,
      paddingTop: 12,
      minHeight: 46,
      color: "#374062",
      ...columnStyle,
      [theme.breakpoints.up("md")]: {
        fontSize: 11,
      },
      [theme.breakpoints.up("lg")]: {
        fontSize: 13,
      },
      [theme.breakpoints.up("xl")]: {
        fontSize: 13,
      },
    },
    tableBlank: {
      width: "100%",
      height: 416,
      boxShadow: `0px 3px 10px rgba(188, 200, 231, 0.2)`,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    noDataSubMessage: {
      fontSize: 13,
      color: Colors.dark.medium,
      fontFamily: "FuturaBkBT",
      marginTop: 10,
      textAlign: "center",
    },
    noDataImg: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  }));
  const classes = useStyle();

  const isRowCheck = () => {
    let check = false;
    const dataFilter = dataContent?.filter(
      (item) =>
        item?.isAllowedTransaction === true || item?.isAllowTransaction === true
    );
    const dataCheck = dataFilter.filter((elm) =>
      selectedValue.find((element) => elm.id === element)
    );

    const dataCount =
      dataCheck.length === 0 ? dataFilter.length + 1 : dataCheck.length;
    if (dataCount === dataFilter.length) check = true;
    return check;
  };

  // if (window.location.href.indexOf("approval-workflow" || "persetujuan")) {
  //   selecable = false;
  // }

  return (
    <Fragment>
      <div>
        <div style={{ width: "100%", overflowX: "auto" }}>
          <div
            className={classNames}
            style={{
              width: isFullTable ? "100%" : widthTable,
              overflowX: "auto",
            }}
          >
            <div
              className={
                headerClassName
                  ? `${classes.cardHeader} ${headerClassName}`
                  : classes.cardHeader
              }
            >
              <CardContent className={classes.cardContentHeader}>
                {headerContent.map((headerItem, index) => (
                  <React.Fragment key={index}>
                    {selecable && index === 0 ? (
                      <div
                        style={{
                          width: "50px",
                        }}
                      >
                        <CheckboxSingle
                          checked={isRowCheck()}
                          onChange={handleSelectRowAll}
                        />
                      </div>
                    ) : null}

                    <div
                      className={classes.column}
                      style={{
                        ...(headerItem.width && {
                          width: headerItem.width,
                        }),
                      }}
                    >
                      {headerItem.title}
                    </div>
                  </React.Fragment>
                ))}
              </CardContent>
            </div>

            <ScrollCustom height={scrollHeight} scroll={scroll}>
              {isLoading ? (
                <Card className={classes.tableBlank}>
                  <CircularProgress color="primary" size={40} />
                </Card>
              ) : (
                type === "detail" && (
                  <Fragment>
                    {dataContent.map((item, dataIindex) => (
                      <Card
                        onClick={() => onClickRow(item)}
                        style={{
                          // table type 'bordered'
                          ...(bordered
                            ? {
                                borderBottom: "1px solid #E8EEFF",
                                boxShadow: "none",
                              }
                            : {
                                boxShadow: rowColor
                                  ? "none"
                                  : `0px 3px 10px rgba(188, 200, 231, 0.2)`,
                                marginBottom: 5,
                              }),
                          backgroundColor: stripe
                            ? dataIindex % 2
                              ? rowColor
                              : "white"
                            : rowColor || "white",

                          ...(dataIindex === dataContent.length - 1 &&
                            tableRounded &&
                            !stripe && {
                              borderBottomLeftRadius: 10,
                              borderBottomRightRadius: 10,
                            }),
                          borderRadius: stripe && "0px",
                        }}
                        elevation={0}
                        key={dataIindex}
                      >
                        <CardContent
                          style={{
                            minHeight: 46,
                            paddingTop: 0,
                            paddingBottom: 0,
                          }}
                        >
                          <div className={classes.rowWrapper}>
                            {headerContent.map((headerItem, index) => (
                              <React.Fragment key={index}>
                                {selecable && index === 0 ? (
                                  <div style={{ width: "50px" }}>
                                    <CheckboxSingle
                                      disabled={item.disabled}
                                      checked={selectedValue.includes(item.id)}
                                      onChange={() =>
                                        handleSelectRow(item.id, dataIindex)
                                      }
                                    />
                                  </div>
                                ) : null}

                                <div
                                  className={classes.columnWrapper}
                                  style={{
                                    ...(headerItem.width && {
                                      width: headerItem.width,
                                    }),
                                    ...(headerItem.fontFamily && {
                                      fontFamily: headerItem.fontFamily,
                                    }),
                                    // background:
                                    //   index % 2 === 0 ? "pink" : "yellow",
                                    overflowWrap: "anywhere",
                                  }}
                                >
                                  {headerItem?.render
                                    ? headerItem?.render(item, dataIindex)
                                    : item[headerItem.key] ||
                                      (allowNullStripped ? "-" : null)}
                                </div>
                              </React.Fragment>
                            ))}
                          </div>
                        </CardContent>
                      </Card>
                    ))}

                    {dataContent.length === 0 && (
                      <Card className={classes.tableBlank}>
                        <div>
                          <div className={classes.noDataImg}>
                            <img src={NoData} alt="no data" />
                          </div>
                          <div className={classes.noDataMessage}>
                            {noDataMessage}
                          </div>
                          <div className={classes.noDataSubMessage}>
                            {noSubMessage}
                          </div>
                        </div>
                      </Card>
                    )}
                  </Fragment>
                )
              )}
            </ScrollCustom>
          </div>
        </div>

        {totalElement ? (
          <div style={{ marginTop: "20px" }}>
            <PaginationComponent
              totalElement={totalElement}
              currentData={dataContent.length}
              totalPage={totalData} // total page
              page={page}
              onChange={setPage}
            />
          </div>
        ) : null}
      </div>

      {selectedValue.length > 0 && extraComponents ? (
        <div>
          <div style={{ marginTop: 75 }} />
          <div
            style={{
              marginTop: 20,
              backgroundColor: "white",
              position: "fixed",
              left: 0,
              right: 0,
              bottom: 0,
              padding: "22px 0",
            }}
          >
            {extraComponents}
          </div>
        </div>
      ) : null}
    </Fragment>
  );
};

Tables.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(["detail", "text"]),
  headerContent: PropTypes.array,
  dataContent: PropTypes.array,
  isFullTable: PropTypes.bool,
  widthTable: PropTypes.string,
  selecable: PropTypes.bool,
  handleSelect: PropTypes.func,
  selectedValue: PropTypes.array,
  isLoading: PropTypes.bool,
  handleSelectAll: PropTypes.func,
  setPage: PropTypes.func,
  page: PropTypes.number,
  totalData: PropTypes.number,
  onClickRow: PropTypes.func,
  tableRounded: PropTypes.bool,
  noDataMessage: PropTypes.node,
  totalElement: PropTypes.number,
  rowColor: PropTypes.string,
  bordered: PropTypes.bool,
  headerClassName: PropTypes.object,
  noSubMessage: PropTypes.string,
  columnStyle: PropTypes.object,
  scroll: PropTypes.bool,
  allowNullStripped: PropTypes.bool,
  stripe: PropTypes.bool,
  scrollHeight: PropTypes.number,
};

Tables.defaultProps = {
  className: "",
  type: "detail",
  headerContent: [],
  dataContent: [],
  handleSelectAll: () => {},
  handleSelect: () => {},
  setPage: () => {},
  onClickRow: () => {},
  page: 1,
  totalData: 0,
  isFullTable: true,
  selectedValue: [],
  widthTable: "",
  isLoading: false,
  selecable: false,
  tableRounded: true,
  noDataMessage: <div>No Data</div>,
  totalElement: 0,
  rowColor: "",
  bordered: false,
  headerClassName: {},
  noSubMessage: "",
  columnStyle: {},
  scroll: false,
  allowNullStripped: true,
  stripe: false,
  scrollHeight: 500,
};

export default Tables;
