import React, { Fragment } from "react";

import { Card, CircularProgress, makeStyles } from "@material-ui/core";
import NoData from "assets/images/BN/illustrationred.png";
import Colors from "helpers/colors";
import PropTypes from "prop-types";

const TableMenuBaru = ({
  headerContent,
  dataContent,
  isLoading,
  removeBorder,
}) => {
  const useStyles = makeStyles({
    container: {
      display: "flex",
      flexDirection: "column",
      width: "100%",
    },
    header: {
      display: "flex",
      backgroundColor: "#F4F7FB",
      // padding: "10px 0",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    headerItem: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      // justifyContent: "center",
      width: "100%",
      fontSize: "13px",
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      color: "#7B87AF",
      padding: "10px 0",
      paddingLeft: 15,
    },
    titleHeader: {},
    body: {
      width: "100%",
    },
    rowBody: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    rowItem: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
      width: "100%",
      fontSize: "13px",
      fontFamily: "FuturaBkBT",
      color: "#374062",
      padding: "10px 0",
      paddingLeft: 15,
    },
    tableBlank: {
      width: "100%",
      height: 300,
      boxShadow: "none",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
  });

  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.header}>
        {(headerContent ?? []).map((item) => (
          <div
            className={classes.headerItem}
            style={{
              ...(item.width && {
                width: item.width,
              }),
            }}
          >
            {item?.title}
          </div>
        ))}
      </div>
      <div className={classes.body}>
        {isLoading ? (
          <Card className={classes.tableBlank}>
            <CircularProgress color="primary" size={40} />
          </Card>
        ) : (
          <Fragment>
            {(dataContent ?? []).map((row, index) => (
              <div
                className={classes.rowBody}
                style={{
                  border: removeBorder ? undefined : "1px solid #DDEBF9",
                }}
              >
                {(headerContent ?? []).map((item) => (
                  <div
                    className={classes.rowItem}
                    style={{
                      ...(item.width && {
                        width: item.width,
                      }),
                    }}
                  >
                    {item.render ? item?.render(row, index) : row[item.key]}
                  </div>
                ))}
              </div>
            ))}
            {dataContent.length === 0 && (
              <Card className={classes.tableBlank}>
                <div>
                  <div className={classes.noDataImg}>
                    <img src={NoData} alt="no data" />
                  </div>
                  <div className={classes.noDataMessage}>No Data</div>
                </div>
              </Card>
            )}
          </Fragment>
        )}
      </div>
    </div>
  );
};

export default TableMenuBaru;

TableMenuBaru.propTypes = {
  isLoading: PropTypes.bool,
  headerContent: PropTypes.array,
  dataContent: PropTypes.array,
};

TableMenuBaru.defaultProps = {
  isLoading: false,
  headerContent: [],
  dataContent: [],
};
