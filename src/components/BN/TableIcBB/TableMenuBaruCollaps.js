import { Collapse, makeStyles, Typography } from "@material-ui/core";
import React, { useState } from "react";
import Colors from "helpers/colors";
import PropTypes from "prop-types";
import { formatCurrency } from "utils/helpers/formatCurrency";
import { useHistory } from "react-router-dom";
import { pathnameCONFIG } from "configuration";
import IconAddWhite from "assets/icons/BN/add_white.svg";
import IconEdit from "../../../assets/icons/BN/edit.svg";
import IconTrash from "../../../assets/icons/BN/trash-2.svg";
import IconChevronBottom from "../../../assets/icons/BN/chevron-down-med.svg";
import IconChevronUp from "../../../assets/icons/BN/chevron-up-alt.svg";
import TableMenuBaru from "./TableMenuBaru";
import PaginationComponent from "./Pagination";

import GeneralButton from "../Button/GeneralButton";

const useStyles = makeStyles({
  page: {
    "& .cardCollaps": {
      width: "100%",
      border: "1px solid",
      borderColor: Colors.gray.medium,
      borderRadius: "10px",
      overflow: "hidden",
      "& .row": {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        "& .column": {
          display: "flex",
          flexDirection: "column",
          padding: 20,
          "& .title": {
            fontSize: "15px",
            fontFamily: "FuturaHvBT",
            color: "#374062",
            fontWeight: 400,
          },
          "& .row": {
            display: "flex",
            flexDirection: "row",

            "& .subtitle": {
              fontSize: "13px",
              fontFamily: "FuturaMdBT",
              color: "#7B87AF",
              fontWeight: 400,
            },
            "& .button": {
              border: "none",
              backgroundColor: "#fff",
              cursor: "pointer",
            },
          },
        },
      },
    },
  },
});

const CardTable = ({ item, dataHeader, onEdit, onDelete }) => {
  // console.warn("onEdit:", onEdit);

  const [checked, setChecked] = useState(false);

  return (
    <div className="cardCollaps">
      <div className="row">
        <div className="column">
          <Typography className="title">
            {formatCurrency(item?.min)}-{formatCurrency(item?.max)}
          </Typography>
          <div
            style={{
              fontSize: "13px",
              fontFamily: "FuturaMdBT",
              color: "#7B87AF",
            }}
          >
            Number of Approvals: {item?.count}
          </div>
        </div>
        <div className="column">
          <div className="row">
            <button className="button" onClick={() => onEdit(item)}>
              <img width={24} height={24} src={IconEdit} alt="ubah " />
            </button>
            <button className="button" onClick={() => onDelete(item)}>
              <img width={24} height={24} src={IconTrash} alt="hapus " />
            </button>
            <button
              className="button"
              onClick={() => {
                console.warn("ubah");
                setChecked(!checked);
              }}
            >
              {checked ? (
                <img width={24} height={24} src={IconChevronUp} alt="ubah " />
              ) : (
                <img
                  width={24}
                  height={24}
                  src={IconChevronBottom}
                  alt="ubah "
                />
              )}
            </button>
          </div>
        </div>
      </div>
      <Collapse in={checked}>
        <TableMenuBaru
          headerContent={dataHeader}
          dataContent={item?.dataContent}
        />
      </Collapse>
    </div>
  );
};

const TableMenuBaruCollaps = (props) => {
  const {
    data,
    dataHeader,
    dataContent,
    totalData,
    totalElement,
    setPage,
    page,
    onDelete,
    onEdit,
    onCollaps,
  } = props;
  const classes = useStyles();
  const router = useHistory();

  return (
    <div className={classes.page}>
      <div style={{ backgroundColor: "white", padding: 20 }}>
        <div className="row">
          <Typography className="title">Matrix Approval List</Typography>
          <GeneralButton
            style={{
              width: "98px",
              fontSize: "9px",
              fontFamily: "FuturaMdBT",
              fontWeight: "700",
              textAlign: "center",
              height: "24px",
            }}
            label="Add List"
            buttonIcon={
              <img src={IconAddWhite} width={10} height={10} alt="add" />
            }
            iconPosition="startIcon"
            width={128}
            onClick={() => {
              router.push(
                pathnameCONFIG.MANAGEMENT_COMPANY.PENGATURAN_MATRIX_FINANCIAL
                  .ADD
              );
            }}
          />
        </div>
        {data?.length
          ? data?.map((item) => (
              <CardTable
                item={item}
                onDelete={onDelete}
                onEdit={onEdit}
                dataHeader={dataHeader}
              />
            ))
          : null}
      </div>
      <React.Fragment>
        {totalData && data.length ? (
          <div style={{ marginTop: "20px" }}>
            <PaginationComponent
              totalElement={totalElement}
              currentData={data?.length}
              totalPage={totalData} // total page
              page={page}
              onChange={setPage}
            />
          </div>
        ) : null}
      </React.Fragment>
    </div>
  );
};

TableMenuBaruCollaps.propTypes = {
  data: PropTypes.array,
  dataHeader: PropTypes.array,
  totalData: PropTypes.number,
  totalElement: PropTypes.number,
  setPage: PropTypes.func,
  page: PropTypes.number,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
};

TableMenuBaruCollaps.defaultProps = {
  data: [],
  dataHeader: [],
  totalData: null,
  totalElement: null,
  setPage: () => {},
  page: null,
  onDelete: () => {},
  onEdit: () => {},
};

export default TableMenuBaruCollaps;
