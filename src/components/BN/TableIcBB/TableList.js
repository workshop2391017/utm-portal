import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  table: {
    height: "281",
    width: "430px",
  },
  header: {
    width: "100%",
    backgroundColor: "#F5FAFF",
    display: "flex",
    flexWrap: "nowrap",
    justifyContent: "space-between",
    padding: "10px",
  },
  title: {
    fontSize: "13px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#7B87AF",
    width: "100%",
  },
  titleBody: {
    fontSize: "13px",
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    color: "#7B87AF",
    width: "100%",
  },
  container: {
    border: "1px solid #B3C1E7",
    borderRadius: "10px",
    width: "100%",
    marginTop: "20px",
  },
  row: {
    width: "100%",
    height: "46px",
    backgroundColor: "#fff",
    display: "flex",
    flexWrap: "nowrap",
    justifyContent: "space-between",
    padding: "10px",
  },
  body: {
    width: "100%",
    height: "251px",
    padding: "10px",
  },
  scrolbar: {
    width: "100%",
    height: "251px",

    // height: "286px",
    // width: "440px",
    "&::-webkit-scrollbar": {
      WebkitAppearance: "none",
    },
    "&::-webkit-scrollbar:vertical": {
      height: "12px",
    },
    "&::-webkit-scrollbar-thumb": {
      borderRadius: "8px",
      border: "2px solid white",
      backgroundColor: "#003566",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#fff",
      borderRadius: "8px",
    },
    overflowY: "auto",
  },
});

export default function TableList({ headerContent, dataContent }) {
  const classes = useStyles();

  // console.warn("headerContent:", headerContent);
  // console.warn("dataContent:", dataContent);

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        {headerContent?.map((item) => (
          <div className={classes.title}>{item.title} </div>
        ))}
      </div>

      <div className={classes.body}>
        <div className={classes.scrolbar}>
          {dataContent?.map((row) => (
            <div className={classes.row}>
              {headerContent?.map((item) => (
                <div className={classes.titleBody}>
                  {item.render ? item.render(row) : row[item.key]}
                </div>
              ))}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
