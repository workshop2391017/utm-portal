import React, { Fragment, memo, useEffect, useState } from "react";
import PropTypes from "prop-types";
import classname from "classnames";
import {
  Card,
  makeStyles,
  CardContent,
  CircularProgress,
} from "@material-ui/core";
import Colors from "helpers/colors";

// assets
import NoData from "assets/images/BN/illustrationred.png";

import CheckboxSingle from "../../SC/Checkbox/CheckboxSingle";
import PaginationComponent from "./Pagination";
import Collapse from "../Collapse";
import ScrollCustom from "../ScrollCustom";

const ContentTable = ({
  props: {
    collapse,
    defaultOpenCollapseBreakdown,
    selecable,
    item,
    selectedValue,
    handleSelectRow,
    dataIindex,
    classes,
    headerContent,
    editable,
  },
}) => {
  const [openCollapse, setOpenCOllapse] = useState(
    defaultOpenCollapseBreakdown
  );

  useEffect(() => {
    setOpenCOllapse(defaultOpenCollapseBreakdown);
  }, [defaultOpenCollapseBreakdown]);

  return (
    <React.Fragment>
      <CardContent
        style={{
          minHeight: 46,
          paddingTop: 0,
          paddingBottom: 0,
          padding: 0,
        }}
      >
        <Collapse
          customOpenCollapse
          isOpenCollapse={openCollapse}
          collapse={collapse}
          defaultOpen={defaultOpenCollapseBreakdown}
          label={
            <div>
              <div
                style={{
                  display: "flex",
                }}
              >
                <div style={{ marginRight: 10 }}>
                  {selecable ? (
                    <div style={{ width: "30px" }}>
                      <CheckboxSingle
                        disabled={item.status}
                        checked={selectedValue.includes(item.id)}
                        onChange={() => handleSelectRow(item.id, dataIindex)}
                      />
                    </div>
                  ) : null}
                </div>
                <div>{item?.name ?? "-"}</div>
              </div>

              <button
                style={{
                  backgroundColor: "transparent",
                  border: "0",
                  width: "100%",
                  height: "100%",
                  position: "absolute",
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  cursor: "pointer",
                }}
                onClick={() => setOpenCOllapse(!openCollapse)}
              />
            </div>
          }
          className={classes.rowWrapper}
        >
          <div className={classes.rowWrapperCollapseBackground}>
            {(item?.child ?? []).map((childItem, idx) => (
              <div className={classes.rowWrapperCollapseContent} key={idx}>
                {headerContent.map((headerItem, index) => (
                  <React.Fragment key={index}>
                    {selecable && index === 0 ? (
                      <div style={{ width: "30px" }} />
                    ) : null}
                    <div
                      className={classes.columnWrapper}
                      style={{
                        ...(headerItem.width && {
                          width: headerItem.width,
                        }),
                        ...(headerItem.fontFamily && {
                          fontFamily: headerItem.fontFamily,
                        }),
                      }}
                    >
                      {selectedValue.includes(item?.id) && editable
                        ? headerItem?.renderEditForm
                          ? headerItem?.renderEditForm(
                              childItem,
                              idx,
                              dataIindex
                            )
                          : childItem[headerItem.key]
                        : headerItem?.render
                        ? headerItem?.render(childItem, idx, dataIindex)
                        : childItem[headerItem.key]}
                    </div>
                  </React.Fragment>
                ))}
              </div>
            ))}
          </div>
        </Collapse>
      </CardContent>
    </React.Fragment>
  );
};
const CTable = memo(ContentTable);
const Tables = ({
  className,
  type,
  headerContent,
  dataContent,
  extraComponents,
  isFullTable,
  widthTable,
  tableRounded,
  onClickRow,
  selecable,
  handleSelect,
  handleSelectAll,
  selectedValue,
  setPage,
  page,
  totalData,
  isLoading,
  totalElement,
  noDataMessage,
  rowColor,
  bordered,
  headerClassName,
  collapse,
  defaultOpenCollapseBreakdown,
  rightCollapse,
  checkListAll,
  editable,
}) => {
  const classNames = classname("o-table", className);

  const handleSelectRow = (id, dataIindex) => {
    const ids = [...selectedValue];

    if (ids.includes(id)) {
      ids.splice(ids.indexOf(id), 1);
    } else {
      ids.push(id);
    }

    handleSelect(ids, dataIindex);
  };

  const handleSelectRowAll = () => {
    const ids = [...selectedValue];

    const allIds = dataContent
      .filter((elm) => !elm.status)
      .map((item) => item.id);

    handleSelectAll(allIds.length === ids.length ? [] : allIds);
  };

  const useStyle = makeStyles((theme) => ({
    column: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
      width: `calc(100% / ${headerContent.length})`,

      fontFamily: "FuturaMdBT",
      lineHeight: "16px",
      border: "none",
      fontWeight: 400,
      [theme.breakpoints.up("md")]: {
        fontSize: "10px",
      },
      [theme.breakpoints.up("lg")]: {
        fontSize: "13px",
      },
      [theme.breakpoints.up("xl")]: {
        fontSize: "13px",
      },
    },
    cardHeader: {
      alignItems: "center",
      minWidth: 37,
      border: 0,
      marginBottom: "20px",
      borderRadius: "0px",
      backgroundColor: "#0061A7",
      color: "white",
      ...(tableRounded && {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }),
    },
    cardContentHeader: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      padding: "10px 20px",
      alignItems: "center",
    },
    rowWrapper: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      backgroundColor: "white !important",
      fontFamily: "FuturaHvBT !important",
      fontWeight: "400 !important",
      fontSize: "15px !important",
      ".makeStyles-label-80": {
        width: "100%",
        paddingLeft: "25px",
      },
    },
    rowWrapperCollapseBackground: {
      backgroundColor: "#F4F7FB",

      borderRadius: 10,
      margin: "15px 10px",
      paddingLeft: 20,
      padringRight: 20,
    },
    rowWrapperCollapseContent: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      // alignItems: "center",
    },
    columnWrapper: {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      // justifyContent: "center",
      width: `calc(100% / ${headerContent.length})`,
      fontFamily: "FuturaBQ",
      position: "relative",
      paddingRight: "15px",
      [theme.breakpoints.up("md")]: {
        fontSize: "10px",
      },
      [theme.breakpoints.up("lg")]: {
        fontSize: "13px",
      },
      [theme.breakpoints.up("xl")]: {
        fontSize: "13px",
      },
      paddingTop: 23,
      paddingBottom: 23,
      height: "auto",
    },
    tableBlank: {
      width: "100%",
      height: 416,
      boxShadow: `0px 3px 10px rgba(188, 200, 231, 0.2)`,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    noDataImg: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  }));
  const classes = useStyle();

  return (
    <div>
      <div
        className={classNames}
        style={{ width: isFullTable ? "100%" : widthTable }}
      >
        <div
          className={
            headerClassName
              ? `${classes.cardHeader} ${headerClassName}`
              : classes.cardHeader
          }
          style={{ maxHeight: "46px" }}
        >
          <CardContent
            className={classes.cardContentHeader}
            style={{ paddingLeft: type === "text" && "50px" }}
          >
            {headerContent.map((headerItem, index) => (
              <React.Fragment key={index}>
                {selecable && index === 0 && checkListAll ? (
                  <div
                    style={{
                      width: "30px",
                    }}
                  >
                    <CheckboxSingle
                      checked={
                        selectedValue.length ===
                        (dataContent ?? []).filter(({ status }) => !status)
                          .length
                      }
                      onChange={handleSelectRowAll}
                    />
                  </div>
                ) : null}

                <div
                  className={classes.column}
                  style={{
                    ...(headerItem.width && {
                      width: headerItem.width,
                    }),
                  }}
                >
                  {headerItem.title}
                </div>
              </React.Fragment>
            ))}
          </CardContent>
        </div>

        {isLoading ? (
          <Card className={classes.tableBlank}>
            <CircularProgress color="primary" size={40} />
          </Card>
        ) : type === "detail" ? (
          <Fragment>
            {dataContent.map((item, dataIindex) => (
              <Card
                onClick={() => onClickRow(item)}
                style={{
                  // table type 'bordered'

                  ...(bordered && !collapse
                    ? {
                        borderBottom: "1px solid #E8EEFF",
                        boxShadow: "none",
                      }
                    : bordered && collapse
                    ? {
                        border: "1px solid #E8EEFF",
                        boxShadow: "none",
                        marginBottom: 10,
                        borderRadius: 10,
                      }
                    : {
                        boxShadow: rowColor
                          ? "none"
                          : `0px 3px 10px rgba(188, 200, 231, 0.2)`,
                        marginBottom: 5,
                      }),
                  backgroundColor: rowColor || "white",

                  ...(dataIindex === dataContent.length - 1 &&
                    tableRounded && {
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                    }),
                }}
                key={dataIindex}
              >
                <CTable
                  props={{
                    collapse,
                    defaultOpenCollapseBreakdown:
                      defaultOpenCollapseBreakdown ||
                      (editable && selectedValue?.includes(item.id)), // open editing row
                    selecable,
                    item,
                    selectedValue,
                    handleSelectRow,
                    dataIindex,
                    classes,
                    headerContent,
                    editable,
                  }}
                />
              </Card>
            ))}
            {dataContent.length === 0 && (
              <Card className={classes.tableBlank}>
                <div>
                  <div className={classes.noDataImg}>
                    <img src={NoData} alt="no data" />
                  </div>
                  <div className={classes.noDataMessage}>{noDataMessage}</div>
                  {/* <div className={classes.noDataSubMessage}>{noSubMessage}</div> */}
                </div>
              </Card>
            )}
          </Fragment>
        ) : null}

        <div style={{ marginTop: 20 }}>
          {selectedValue.length > 0 && extraComponents ? extraComponents : null}
        </div>
      </div>

      {totalData ? (
        <div style={{ marginTop: "20px" }}>
          <PaginationComponent
            totalElement={totalElement}
            currentData={dataContent.length}
            totalPage={totalData} // total page
            page={page}
            onChange={setPage}
          />
        </div>
      ) : null}
    </div>
  );
};

Tables.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(["detail", "text"]),
  headerContent: PropTypes.array,
  dataContent: PropTypes.array,
  isFullTable: PropTypes.bool,
  widthTable: PropTypes.string,
  selecable: PropTypes.bool,
  handleSelect: PropTypes.func,
  selectedValue: PropTypes.array,
  isLoading: PropTypes.bool,
  handleSelectAll: PropTypes.func,
  setPage: PropTypes.func,
  page: PropTypes.number,
  totalData: PropTypes.number,
  onClickRow: PropTypes.func,
  tableRounded: PropTypes.bool,
  noDataMessage: PropTypes.node,
  totalElement: PropTypes.number,
  rowColor: PropTypes.string,
  bordered: PropTypes.bool,
  headerClassName: PropTypes.object,
  collapse: PropTypes.bool,
  defaultOpenCollapseBreakdown: PropTypes.bool,
  rightCollapse: PropTypes.bool,
  checkListAll: PropTypes.bool,
};

Tables.defaultProps = {
  className: "",
  type: "detail",
  headerContent: [],
  dataContent: [],
  handleSelectAll: () => {},
  handleSelect: () => {},
  setPage: () => {},
  onClickRow: () => {},
  page: 1,
  totalData: 0,
  isFullTable: true,
  selectedValue: [],
  widthTable: "",
  isLoading: false,
  selecable: false,
  tableRounded: true,
  noDataMessage: <div>No Data</div>,
  totalElement: 0,
  rowColor: "",
  bordered: false,
  headerClassName: {},
  collapse: true,
  defaultOpenCollapseBreakdown: false,
  rightCollapse: true,
  checkListAll: true,
};

export default Tables;
