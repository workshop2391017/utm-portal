import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import { Card, CircularProgress, Grid } from "@material-ui/core";
import Colors from "helpers/colors";
import NoData from "assets/images/BN/illustrationred.png";
import purify from "dompurify";
import PaginationComponent from "./Pagination";
import RichTextCardCM from "../Cards/RichTextCard/contentManagement";
import RichTextCardRS from "../Cards/RichTextCard/responseSetting";

const useStyles = (background) =>
  makeStyles({
    card: {
      height: "240px",
      width: "320px",
      backgroundColor: "#fff",
      borderRadius: "10px",
    },
    header: {
      padding: "12px 15px",
      display: "flex",
      justifyContent: "space-between",
      width: "100%",
      height: "42px",
      borderBottom: "1px solid #E8EEFF",
    },
    container: {
      ...(!background ? {} : { backgroundColor: "#fff", padding: 15 }),
      borderRadius: 10,
      "& .title": {
        fontSize: 15,
        fontFamily: "FuturaHvBT",
        color: Colors.dark.hard,
        marginBottom: 20,
      },
    },
    content: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
    },
    page: {},
    cardContent: {
      height: "240px",
      width: "320px",
      backgroundColor: "#fff",
      borderRadius: "10px",
      marginBottom: "18px",
    },
    tableBlank: {
      width: "100%",
      height: 500,
      boxShadow: "none",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    noDataImg: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  });

const TableCard = (props) => {
  const {
    data,
    onDelete,
    onEdit,
    totalData,
    page,
    setPage,
    totalElement,
    type,
    children,
    loading,
    title,
    background,
    iconGlobe,
  } = props;
  const classes = useStyles(background)();
  return (
    <React.Fragment>
      <div className={classes.container}>
        {title && <div className="title">Title</div>}
        <Grid className={classes.content} container rowSpacing={2} spacing={2}>
          {loading ? (
            <Card className={classes.tableBlank}>
              <CircularProgress color="primary" size={40} />
            </Card>
          ) : (
            children ||
            data?.map((item, i) => (
              <Grid item md="6" sm="6" key={i}>
                {type === "ResponseSetting" ? (
                  <RichTextCardRS
                    iconGlobe={iconGlobe}
                    title={item.title}
                    description={item.value}
                    onEdit={() => onEdit(item)}
                  />
                ) : (
                  <RichTextCardCM
                    code={item.code}
                    name={item.name}
                    onEdit={() => onEdit(item)}
                    description={(e) => (
                      <div
                        dangerouslySetInnerHTML={{
                          __html: purify.sanitize(
                            e === "id" ? item[item.keyID] : item[item.keyEN]
                          ),
                        }}
                      />
                    )}
                    footer={item.createdDate}
                  />
                )}
              </Grid>
            ))
          )}

          {data.length === 0 && (
            <Grid item md="12" className={classes.tableBlank}>
              <div>
                <div className={classes.noDataImg}>
                  <img src={NoData} alt="no data" />
                </div>
                <div className={classes.noDataMessage}>No Data</div>
              </div>
            </Grid>
          )}
        </Grid>
      </div>
      {totalData ? (
        <div style={{ marginTop: "20px" }}>
          <PaginationComponent
            totalElement={totalElement}
            currentData={data.length}
            totalPage={totalData} // total page
            page={page}
            onChange={setPage}
          />
        </div>
      ) : null}
    </React.Fragment>
  );
};

TableCard.propTypes = {
  type: PropTypes.string,
  loading: PropTypes.bool,
  title: PropTypes.string,
  background: PropTypes.bool,
  iconGlobe: PropTypes.bool,
};

TableCard.defaultProps = {
  type: "ResponseSetting",
  loading: false,
  title: null,
  background: true,
  iconGlobe: false,
};

export default TableCard;
