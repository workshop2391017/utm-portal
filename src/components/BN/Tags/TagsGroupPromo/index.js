// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Chip } from "@material-ui/core";

// assets
import deleteIcon from "../../../../assets/icons/BN/times-circle-gray-small.svg";
import GeneralButton from "../../Button/GeneralButton";
import { ReactComponent as Plus } from "../../../../assets/icons/BN/plusSmall.svg";

const TagsGroupPromo = (props) => {
  const {
    tags,
    setTags,
    chipWidth,
    scroll,
    onClick,

    label,
    buttonStyle,
    type,
    onContinue,
    disabled,
  } = props;
  const useStyles = makeStyles({
    TagsGroupPromo: {
      marginTop: 5,
      height: scroll ? 92 : "fit-content",
      overflowX: "auto",
      "&::-webkit-scrollbar": {
        height: 12,
      },
      "&::-webkit-scrollbar-track": {
        background: "#F4F7FB",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb": {
        background: "#005099",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb:hover": {
        background: "#004a80",
      },
    },
    tagContainer: {
      height: scroll ? 63 : "fit-content",
      width: scroll ? "fit-content" : "100%",
      display: "flex",
      flexDirection: scroll ? "column" : "row",
      justifyContent: "flex-start",
      flexWrap: "wrap",
    },
    chip: {
      backgroundColor: "#E6EAF3",
      minWidth: chipWidth,
      height: 24,
      borderRadius: 20,
      "& .MuiChip-label": {
        fontFamily: "FuturaMdBT",
        fontSize: 13,
        color: "#7B87AF",
        padding: "0 10px 0 8px",
      },
      "& .MuiChip-deleteIcon": {
        width: 14,
        height: 14,
      },
    },
  });
  const classes = useStyles();

  const handleDelete = (value) => {
    setTags(
      tags.filter((e) => {
        if (e.id) {
          return e.id !== value;
        }
        return e !== value;
      })
    );
  };

  return (
    <div className={classes.TagsGroupPromo}>
      <div className={classes.tagContainer}>
        {type === "button" && (
          <GeneralButton
            label={label}
            iconPosition="endIcon"
            buttonIcon={<Plus />}
            height="24px"
            style={{
              fontSize: "10px",
              fontFamily: " FuturaMdBT",
              marginRight: "10px",
              padding: "0 10px",
              ...buttonStyle,
            }}
            onClick={onContinue}
            disabled={disabled}
          />
        )}
        {/* {console.log("ini tags", tags)} */}
        {tags.map((item, i) => (
          <Chip
            label={item.name ?? item.merchantName ?? item.branchName ?? item}
            onDelete={() => handleDelete(item.id ?? item)}
            deleteIcon={<img alt="delete-icon" src={deleteIcon} />}
            className={classes.chip}
            style={{
              marginBottom: i % 2 === 0 ? 13 : 0,
              marginRight: 10,
            }}
          />
        ))}
      </div>
    </div>
  );
};

TagsGroupPromo.propTypes = {
  tags: PropTypes.array,
  setTags: PropTypes.func.isRequired,
  chipWidth: PropTypes.number,
  scroll: PropTypes.bool,
  onClick: PropTypes.func,
  label: PropTypes.string,
  buttonStyle: PropTypes.object,
  disabled: PropTypes.bool,
};

TagsGroupPromo.defaultProps = {
  tags: [],
  chipWidth: 68,
  scroll: false,
  onClick: () => {},
  disabled: false,
  label: "",
  buttonStyle: {},
};

export default TagsGroupPromo;
