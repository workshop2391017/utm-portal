import React from "react";

import {
  Button,
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core";

import Colors from "helpers/colors";

import PropTypes from "prop-types";

export default function Footer(props) {
  const {
    // to connect the button with a form when it's outside of the form
    formId,
    type,
    // functions
    onContinue,
    onCancel,
    // custom styles
    containerColor,
    buttonContainedColor,
    buttonOutlinedColor,
    buttonContainedTextColor,
    buttonContainedWidth,
    buttonOutlinedWidth,
    radius,
    // validations
    outlinedDisabled,
    disabled,
    buttonContainedTextColorDisabled,
    backgroundColorDisabled,
    // labels
    buttonContainedText,
    buttonOutlinedText,
    // others
    disableElevation,
    disableRipple,
  } = props;

  const buttonContainedTheme = createTheme({
    palette: {
      primary: {
        main: buttonContainedColor,
      },
    },
  });

  const buttonOutlinedTheme = createTheme({
    palette: {
      primary: {
        main: buttonOutlinedColor,
      },
    },
  });

  const useStyles = makeStyles((theme) => ({
    container: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      position: "sticky",
      marginTop: "auto",
      padding: 20,
      bottom: 0,
      backgroundColor: containerColor,
      zIndex: 10,
    },
    buttonOutlined: {
      ...theme.typography.actionButton,
      height: 44,
      width: buttonOutlinedWidth,
      borderRadius: radius,
      color: buttonOutlinedColor,
      borderColor: buttonOutlinedColor,
    },
    buttonContained: {
      ...theme.typography.actionButton,
      height: 44,
      width: buttonContainedWidth,
      borderRadius: radius,
      color: buttonContainedTextColor,
      backgroundColor: buttonContainedColor,
    },
    disabled: {
      color: `${buttonContainedTextColorDisabled} !important`,
      backgroundColor: `${backgroundColorDisabled} !important`,
    },
  }));

  const classes = useStyles();

  return (
    <div className={classes.container}>
      <ThemeProvider theme={buttonOutlinedTheme}>
        <Button
          variant="outlined"
          color="primary"
          className={classes.buttonOutlined}
          onClick={onCancel}
          disabled={outlinedDisabled}
        >
          {buttonOutlinedText}
        </Button>
      </ThemeProvider>
      <ThemeProvider theme={buttonContainedTheme}>
        <Button
          variant="contained"
          color="primary"
          type={type}
          form={formId}
          classes={{
            disabled: classes.disabled,
            root: classes.buttonContained,
          }}
          disabled={disabled}
          onClick={type !== "submit" ? onContinue : undefined}
          disableElevation={disableElevation}
          disableRipple={disableRipple}
        >
          {buttonContainedText}
        </Button>
      </ThemeProvider>
    </div>
  );
}

Footer.propTypes = {
  onContinue: PropTypes.func,
  onCancel: PropTypes.func,
  containerColor: PropTypes.string,
  buttonContainedColor: PropTypes.string, // ! supported formats: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
  buttonOutlinedColor: PropTypes.string,
  buttonContainedTextColor: PropTypes.string,
  buttonContainedWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  buttonOutlinedWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  radius: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  formId: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  outlinedDisabled: PropTypes.bool,
  buttonContainedTextColorDisabled: PropTypes.string,
  backgroundColorDisabled: PropTypes.string,
  buttonContainedText: PropTypes.string,
  buttonOutlinedText: PropTypes.string,
  disableElevation: PropTypes.bool,
  disableRipple: PropTypes.bool,
};

Footer.defaultProps = {
  onContinue: () => console.warn("onContinue!"),
  onCancel: () => console.warn("onCancel!"),
  containerColor: "white",
  buttonContainedColor: Colors.primary.hard,
  buttonOutlinedColor: Colors.primary.hard,
  buttonContainedTextColor: "white",
  buttonContainedWidth: 78,
  buttonOutlinedWidth: 93,
  radius: 10,
  formId: undefined,
  type: "button",
  disabled: false,
  outlinedDisabled: false,
  buttonContainedTextColorDisabled: "white",
  backgroundColorDisabled: Colors.gray.medium,
  buttonContainedText: "Save",
  buttonOutlinedText: "Cancel",
  disableElevation: false,
  disableRipple: false,
};
