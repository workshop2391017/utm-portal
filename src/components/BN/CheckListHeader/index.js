import React from "react";
import PropTypes from "prop-types";
import CheckCircleRoundedIcon from "@material-ui/icons/CheckCircleRounded";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
  card: {
    width: "750px",
    height: "44px",
    backgroundColor: "#F4F7FB",
    display: "flex",
    justifyContent: "space-between",
    padding: "12px 15px",
  },
  avatar: {
    backgroundColor: "#F4F7FB",
    width: "24px",
    height: "24px",
  },
  title: {
    fontSize: "17px",
    fontFamily: "FuturaMdBT",
    color: "#374062",
  },
});

export default function CheckListHeader({ title, isChecklist }) {
  const classes = useStyles();

  return (
    <div className={classes.card}>
      <Typography className={classes.title}>{title}</Typography>
      {isChecklist ? (
        <CheckCircleRoundedIcon
          style={{
            color: "#75D37F",
            width: "24px",
            height: "24px",
            borderRadius: "12px",
          }}
        />
      ) : null}
    </div>
  );
}
CheckListHeader.propTypes = {
  title: PropTypes.string,
  isChecklist: PropTypes.bool,
};
CheckListHeader.defaultProps = {
  title: "",
  isChecklist: false,
};
