// TextStyle Component
// --------------------------------------------------------
import React from "react";
import PropTypes from "prop-types";
import classname from "classnames";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/core";
import theme from "../../../../helpers/theme";

const TextStyle = ({
  className,
  color,
  weight,
  children,
  variant,
  size,
  sx,
  gutterBottom,
  ...others
}) => {
  const classNames = classname("a-text-style", className, color, weight);
  return (
    <ThemeProvider theme={theme}>
      <Typography
        variant={variant}
        sx={{ color, fontWeight: weight, fontSize: size, ...sx }}
        className={classNames}
        gutterBottom={gutterBottom}
        {...others}
      >
        {children}
      </Typography>
    </ThemeProvider>
  );
};

TextStyle.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  weight: PropTypes.string,
  variant: PropTypes.string,
  size: PropTypes.string,
  sx: PropTypes.object,
  children: PropTypes.node,
  gutterBottom: PropTypes.bool,
};

TextStyle.defaultProps = {
  className: "",
  color: "",
  weight: "400",
  variant: "body2",
  size: "17px",
  children: "",
  sx: {},
  gutterBottom: false,
};

export default TextStyle;
