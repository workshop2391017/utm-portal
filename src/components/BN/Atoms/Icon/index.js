// Icon Component
// --------------------------------------------------------

import React from "react";
import PropTypes from "prop-types";
import classname from "classnames";
import SvgIcon from "@material-ui/core/SvgIcon";
// ---
import getIcon from "./iconHelper";

const Icon = ({
  name,
  color,
  colorStroke,
  colorFill,
  width,
  type,
  height,
  className,
  sx,
  size,
  ...others
}) => {
  const classNames = classname(
    "a-icon",
    `icon-${name}`,
    color,
    size,
    className
  );

  return (
    <SvgIcon
      sx={{
        verticalAlign: "middle",
        "&.xs": {
          fontSize: "14px",
        },
        "&.sm": {
          fontSize: "18px",
        },
        "&.md": {
          fontSize: "1.5rem",
        },
        "&.lg": {
          fontSize: "32px",
        },
        "&.xl": {
          fontSize: "40px",
        },
        path: {
          fill: `${type === "fill" ? color : colorFill || "none"}`,
          stroke: `${type === "stroke" ? color : colorStroke || "none"}`,
        },
        ...sx,
      }}
      className={classNames}
      width={width}
      height={height}
      component={getIcon(name)}
      viewBox={`0 0 ${width} ${height}`}
      {...others}
    />
  );
};

Icon.propTypes = {
  name: PropTypes.string,
  type: PropTypes.oneOf(["fill", "stroke"]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.string,
  sx: PropTypes.object,
  colorStroke: PropTypes.string,
  colorFill: PropTypes.string,
};

Icon.defaultProps = {
  name: "",
  type: "fill",
  width: "24",
  height: "24",
  color: "currentColor",
  className: "",
  size: "md",
  sx: {},
  colorStroke: "",
  colorFill: "",
};

export default Icon;
