// eslint-disable-next-line no-unused-vars
// import React from 'react';

import { ReactComponent as arrowLeft } from "assets/icons/BN/arrow-left.svg";

import { ReactComponent as fileText } from "assets/icons/BN/file-text.svg";
import { ReactComponent as clipboard } from "assets/icons/BN/clipboardnew.svg";

import { ReactComponent as grid } from "assets/icons/BN/grid.svg";
import { ReactComponent as file } from "assets/icons/BN/file.svg";
import { ReactComponent as mapPin } from "assets/icons/BN/map-pin.svg";

const getIcon = (iconeName) => {
  switch (iconeName) {
    case "arrow-left":
      return arrowLeft;
    case "file":
      return file;
    case "file-text":
      return fileText;
    case "clipboard":
      return clipboard;
    case "grid":
      return grid;
    case "map-pin":
      return mapPin;
    default:
      return arrowLeft;
  }
};

export default getIcon;
