import React from "react";
import PropTypes from "prop-types";

import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";

import { ReactComponent as ClipBoardFailed } from "assets/images/BN/clipboard-failed.svg";

export default function TableComponent({
  cols,
  data,
  onClickRow,
  maxHeight,
  showHeader = true,
  headerContent = null,
  loading,
  rowShadow,
  emptyState,
  width,
  height,
}) {
  const useStyles = makeStyles({
    paper: {
      width,
      height,
      position: "relative",
      overflow: "hidden",
      borderRadius: "8px",
      boxShadow: "none",
    },
    tableHead: {
      width: "100%",
      display: "table",
      tableLayout: "fixed",
    },
    tableBody: {
      display: "block",
      height: maxHeight,
      position: "relative",
      overflow: "auto",
      "&::-webkit-scrollbar": {
        width: 7,
      },
      "&::-webkit-scrollbar-track": {
        background: "#F4F7FB",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb": {
        background: "#0061A7",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb:hover": {
        background: "#004a80",
      },
    },
    tableRow: {
      display: "table",
      width: "100%",
      tableLayout: "fixed",
      "&:hover": {
        cursor: onClickRow ? "pointer" : "initial",
        "& td span": {
          fontWeight: 600,
        },
      },
      boxShadow: rowShadow ? "inset 0px -1px 0px #E8EEFF" : "",
    },
    emptyText: {
      color: "#BCC8E7",
      fontSize: "13px",
      fontFamily: "FuturaHvBT",
      top: "50%",
      left: "46%",
      position: "absolute",
    },
    emptyState: {
      display: "flex",
      height: 386,
      alignItems: "center",
      justifyContent: "center",
    },
    wrapEmpty: {
      display: "block",
    },
    wrapEmptyText: {
      display: "block",
      marginTop: 20,
      textAlign: "center",
    },
    textEmpty: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 21,
      color: "#7B87AF",
      lineHeight: "25px",
      letterSpacing: "0.03em",
    },
  });

  const classes = useStyles();

  const renderEmpty = () => (
    <span className={classes.emptyText}>Data Tidak Tersedia</span>
  );

  return (
    <div>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table size="small">
            {headerContent}
            {showHeader ? (
              <TableHead className={classes.tableHead}>
                <TableRow>
                  {cols.map((headerItem, index) => (
                    <TableCell
                      align="left"
                      style={{
                        // backgroundColor: "pink",
                        width: "93px",
                        height: "36px",
                        fontFamily: "FuturaHvBT",
                        fontWeight: 400,
                        fontSize: "13px",
                        lineHeight: "16px",
                        border: "none",
                        color: "#FFFFFF",
                        backgroundColor: "#0061A7",
                      }}
                    >
                      {headerItem.title}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
            ) : (
              <div />
            )}
            <TableBody className={classes.tableBody}>
              {loading ? (
                <div
                  style={{ display: "flex", height: "100%", marginTop: "50px" }}
                >
                  <CircularProgress style={{ margin: "auto" }} />
                </div>
              ) : data.length > 0 ? (
                <React.Fragment>
                  {data.map((row) => (
                    <TableRow
                      key={row.id}
                      className={classes.tableRow}
                      onClick={onClickRow}
                    >
                      {cols.map((col, key) => (
                        <TableCell
                          align={col.align}
                          key={key}
                          width={col.width}
                          style={{
                            background: "pink",

                            // border: "none",
                          }}
                        >
                          {col.render(row)}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {emptyState ? (
                    <div className={classes.emptyState}>
                      <div className={classes.wrapEmpty}>
                        <ClipBoardFailed />
                        <div className={classes.wrapEmptyText}>
                          <p className={classes.textEmpty}>Add New Group</p>
                          <p className={classes.textEmpty}>
                            Into Account Group
                          </p>
                        </div>
                      </div>
                    </div>
                  ) : (
                    renderEmpty()
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}

TableComponent.propTypes = {
  cols: PropTypes.array,
  data: PropTypes.array,
  onClickRow: PropTypes.func,
  maxHeight: PropTypes.number,
  loading: PropTypes.bool,
  rowShadow: PropTypes.bool,
  emptyState: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
};

TableComponent.defaultProps = {
  cols: [],
  data: [],
  onClickRow: null,
  maxHeight: 304,
  loading: false,
  rowShadow: false,
  emptyState: false,
  width: "100%",
  height: "auto",
};
