// main
import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
// libraries
import {
  Card,
  Collapse,
  IconButton,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import PaginationComponent from "components/BN/TableIcBB/Pagination";
import Colors from "helpers/colors";

// assets
import NoData from "assets/images/BN/illustrationred.png";
import CircularIndeterminate from "components/BN/CircularIndeterminate";
import minussquare from "../../../../assets/icons/BN/minus-square.svg";
import plussquare from "../../../../assets/icons/BN/plus-square.svg";
import ChervonRight from "../../../../assets/icons/BN/chevron-right.svg";

const CollapseComponent = (props) => {
  const {
    cols,
    row,
    onClickRow,
    collapseLocation,
    disableCollapseCell,
    withParentTitleCollapse,
  } = props;
  const useStyles = makeStyles({
    tableRow: {
      padding: "14px",
      borderBottom: "1px solid #F4F7FB",
      "&:hover": {
        cursor: onClickRow ? "pointer" : "initial",
        "& td span": {
          fontWeight: 600,
        },
      },
    },
    tableCell: {
      fontSize: 13,
      fontFamily: "FuturaBkBT",
      height: 46,
      fontWeight: 400,
      border: "none",
    },
    tableCellWithTitle: {
      fontSize: 14,
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      border: "none",
      color: "#374062",
      padding: "13px 20",
    },
  });
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [openCollapse, setOpenCollapse] = useState(false);

  return (
    <React.Fragment>
      {withParentTitleCollapse ? (
        <TableRow style={{ backgroundColor: "#F4F7FB" }}>
          <TableCell
            colSpan={withParentTitleCollapse}
            className={classes.tableCellWithTitle}
          >
            {row.title}
          </TableCell>
          <TableCell className={classes.tableCell}>
            <IconButton
              size="small"
              color="primary"
              onClick={() => setOpen(!open)}
            >
              <img
                alt="chevron"
                src={ChervonRight}
                style={{
                  transform: open ? "rotate(-90deg)" : "rotate(90deg)",
                }}
              />
            </IconButton>
          </TableCell>
        </TableRow>
      ) : (
        <TableRow key={row.id} className={classes.tableRow}>
          {cols.map((col, key) => (
            <TableCell
              align={col.align}
              key={key}
              className={classes.tableCell}
            >
              {col.title === collapseLocation && row?.child.length > 0 ? (
                <span>
                  <IconButton
                    size="small"
                    color="primary"
                    onClick={() => setOpen(!open)}
                  >
                    <img alt="chevron" src={open ? minussquare : plussquare} />
                  </IconButton>
                  <span style={{ marginLeft: 5 }}>{col.render(row)}</span>
                </span>
              ) : (
                col.render(row)
              )}
            </TableCell>
          ))}
        </TableRow>
      )}

      {/* <Collapse in={true} timeout="auto" unmountOnExit> */}

      {open &&
        row.child.map((item) => (
          <React.Fragment>
            <TableRow
              className={classes.tableRow}
              onClick={onClickRow}
              style={{
                backgroundColor: withParentTitleCollapse
                  ? "#FFFFFF"
                  : "#F4F7FB",
              }}
            >
              {cols.map((col, key) => (
                <TableCell
                  align={col.align}
                  key={key}
                  style={{
                    fontSize: 13,
                    fontFamily: "FuturaBkBT",
                    height: 46,
                    fontWeight: 400,
                    border: "none",
                    paddingLeft: col.title === collapseLocation ? 77 : 16,
                  }}
                >
                  {col.render(item)}

                  {/* {!disableCollapseCell.includes(col.title) &&
                  col.title === collapseLocation
                    ? col.render(item)
                    : // <React.Fragment>
                      //   <span>
                      //     <IconButton
                      //       size="small"
                      //       color="primary"
                      //       onClick={() => setOpenCollapse(!openCollapse)}
                      //     >
                      //       <img
                      //         alt="chevron"
                      //         src={openCollapse ? minussquare : plussquare}
                      //       />
                      //     </IconButton>
                      //   </span>
                      //   <span style={{ marginLeft: 5 }}>{col.render(item)}</span>
                      // </React.Fragment>

                      col.render(item)} */}
                </TableCell>
              ))}
            </TableRow>

            {openCollapse &&
              item.child.map((item) => (
                <TableRow
                  className={classes.tableRow}
                  onClick={onClickRow}
                  style={{ backgroundColor: "#F4F7FB" }}
                >
                  {cols.map((col, key) => (
                    <TableCell
                      align={col.align}
                      key={key}
                      style={{
                        fontSize: 13,
                        fontFamily: "FuturaBkBT",
                        height: 46,
                        fontWeight: 400,
                        border: "none",
                        paddingLeft: col.title === collapseLocation ? 60 : 16,
                      }}
                    >
                      {!disableCollapseCell.includes(col.title) &&
                        col.render(item)}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
          </React.Fragment>
        ))}
      {/* </Collapse> */}
    </React.Fragment>
  );
};

const TableComponent = (props) => {
  const {
    cols,
    data,
    pagination,
    page,
    setPage,
    totalPages,
    totalData,
    onClickRow,
    extraComponents,
    showExtra,
    collapseLocation,
    disableCollapseCell,
    isLoading,
    withParentTitleCollapse,
    noDataMessage,
    noSubMessage,
  } = props;

  const useStyles = makeStyles({
    paperTable: {
      borderRadius: 8,
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      overflow: "hidden",
    },
    paginationContainer: {
      display: "flex",
      marginTop: 24,
      justifyContent: "space-between",
    },
    pagination: {
      height: 40,
      display: "inline-block",
      "& ul": {
        height: 40,
        "& li": {
          height: 40,
          width: 40,
          margin: "0 5px",
          "& button": {
            fontFamily: "FuturaMdBT",
            height: 40,
            width: 40,
            backgroundColor: "#fff",
            borderRadius: 8,
            margin: 0,
            color: "#0061A7",
            "Mui-selected": {
              backgroundColor: "#0061A7",
            },
          },
          "& div": {
            marginTop: 15,
          },
        },
      },
    },
    buttonPagination: {
      minWidth: 40,
      height: 40,
      padding: 6,
      borderRadius: 8,
    },
    extra: {
      marginTop: 20,
      display: "flex",
      justifyContent: "flex-end",
    },
    loading: {
      display: "flex",
      paddingBottom: "20%",
      boxShadow: "none",
      justifyContent: "center",
    },
    tableBlank: {
      width: "100%",
      height: 416,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
      flexDirection: "column",
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    noDataSubMessage: {
      fontSize: 13,
      color: Colors.dark.medium,
      fontFamily: "FuturaBkBT",
      marginTop: 10,
      textAlign: "center",
    },
  });
  const classes = useStyles();

  const rowsPerPage = 10;
  const emptyRows = rowsPerPage - data.length;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const clickFirstPage = () => {
    setPage(1);
  };
  const clickPrevPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };
  const clickNextPage = () => {
    if (page < totalPages) {
      setPage(page + 1);
    }
  };
  const clickLastPage = () => {
    setPage(totalPages);
  };

  const createEmptyRow = () => {
    const row = [];
    for (let i = 0; i < emptyRows; i++) {
      row.push(
        <TableRow>
          <TableCell
            colSpan={8}
            style={{
              height: 46,
              borderBottom: "none",
            }}
          />
        </TableRow>
      );
    }
    return row;
  };

  return (
    <div>
      <Paper className={classes.paperTable}>
        <TableContainer>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                {cols.map((headerItem, index) => (
                  <TableCell
                    align={headerItem.headerAlign}
                    style={{
                      height: 36,
                      fontWeight: 400,
                      fontSize: 13,
                      fontFamily: "FuturaMdBT",
                      lineHeight: "18px",
                      border: "none",
                      backgroundColor: "#0061A7",
                      color: "#ffffff",
                    }}
                  >
                    {headerItem.title}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            {data.length !== 0 ? (
              <TableBody>
                {isLoading ? (
                  <TableRow>
                    <TableCell
                      colSpan={8}
                      style={{
                        height: 46,
                        borderBottom: "none",
                      }}
                    >
                      <Paper className={classes.loading}>
                        <CircularIndeterminate />
                      </Paper>
                    </TableCell>
                  </TableRow>
                ) : (
                  <Fragment>
                    <React.Fragment>
                      {data.map((row) => (
                        <React.Fragment>
                          <CollapseComponent
                            style={{
                              border: "1px solid #BCC8E7",
                              padding: "14px",
                              fontFamily: "Futura BQ",
                              fontWeight: 400,
                              color: "#374062",
                            }}
                            cols={cols}
                            row={row}
                            onClickRow={onClickRow}
                            collapseLocation={collapseLocation}
                            disableCollapseCell={disableCollapseCell}
                            withParentTitleCollapse={withParentTitleCollapse}
                          />
                          {/* function ini ngerusak UI branch segment ketika melakukan search, apakah ada keperluan dinamisme container lain? */}
                          {/* {createEmptyRow()} */}
                        </React.Fragment>
                      ))}
                    </React.Fragment>
                  </Fragment>
                )}
              </TableBody>
            ) : (
              <TableBody>
                <TableRow>
                  <TableCell
                    colSpan={8}
                    style={{
                      height: 46,
                      borderBottom: "none",
                    }}
                  >
                    <div className={classes.tableBlank}>
                      <div className={classes.noDataImg}>
                        <img src={NoData} alt="no data" />
                      </div>
                      <div className={classes.noDataMessage}>
                        {noDataMessage}
                      </div>
                      <div className={classes.noDataSubMessage}>
                        {noSubMessage}
                      </div>
                    </div>
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </Table>
        </TableContainer>
      </Paper>
      <Collapse in={showExtra}>
        <div className={classes.extra}>
          <div>{extraComponents}</div>
        </div>
      </Collapse>

      {totalData ? (
        <div style={{ marginTop: "20px" }}>
          <PaginationComponent
            totalElement={totalData}
            currentData={data?.length}
            count={totalData / totalPages}
            totalPage={totalPages} // total page
            page={page}
            onChange={setPage}
          />
        </div>
      ) : null}
    </div>
  );
};

TableComponent.propTypes = {
  cols: PropTypes.array,
  data: PropTypes.array,
  pagination: PropTypes.bool,
  page: PropTypes.number,
  setPage: PropTypes.func.isRequired,
  totalPages: PropTypes.number,
  totalData: PropTypes.number,
  onClickRow: PropTypes.func,
  extraComponents: PropTypes.element,
  showExtra: PropTypes.bool,
  collapseLocation: PropTypes.string,
  disableCollapseCell: PropTypes.array,
  withParentTitleCollapse: PropTypes.bool,
  noDataMessage: PropTypes.node,
  noSubMessage: PropTypes.string,
};

TableComponent.defaultProps = {
  cols: [],
  data: [],
  pagination: true,
  page: 1,
  totalPages: 0,
  totalData: 0,
  onClickRow: null,
  extraComponents: null,
  showExtra: false,
  collapseLocation: "",
  disableCollapseCell: [],
  withParentTitleCollapse: false,
  noDataMessage: <div>No Data</div>,
  noSubMessage: "",
};

export default TableComponent;
