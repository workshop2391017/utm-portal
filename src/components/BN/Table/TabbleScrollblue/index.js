// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  makeStyles,
  CircularProgress,
  Card,
} from "@material-ui/core";

// assets
import NoData from "assets/images/BN/illustrationred.png";
import { ReactComponent as ClipBoardFailed } from "assets/images/BN/clipboard-failed.svg";
import Colors from "helpers/colors";

const TableComponent = (props) => {
  const {
    rounded,
    cols,
    data,
    onClickRow,
    maxHeight,
    minHeight,
    showHeader,
    headerContent,
    loading,
    rowShadow,
    emptyState,
    noDataMessage,
    noSubMessage,
    bordered,
  } = props;

  const useStyles = makeStyles({
    paperTable: {
      borderRadius: 0,
      overflow: "hidden",
      boxShadow: "none",
      position: "relative",
    },
    tableHead: {
      display: "table",
      width: "100%",
      tableLayout: "fixed",
    },
    tr: {
      backgroundColor: Colors.primary.hard,
      "& th:first-child": {
        borderRadius: "10px 0 0 0",
        borderRight: 0,
      },
      "& th:last-child": {
        borderRadius: "0 10px 0 0",
        borderLeft: 0,
      },
    },
    tableBody: {
      display: "block",
      position: "relative",
      overflow: "auto",
      "&::-webkit-scrollbar": {
        width: 7,
      },
      "&::-webkit-scrollbar-track": {
        background: "#F4F7FB",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb": {
        background: "#0061A7",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb:hover": {
        background: "#004a80",
      },
    },
    tableRow: {
      display: "table",
      width: "100%",
      tableLayout: "fixed",
      "&:hover": {
        cursor: onClickRow ? "pointer" : "initial",
        "& td span": {
          fontWeight: 600,
        },
      },
      boxShadow: rowShadow ? "inset 0px -1px 0px #E8EEFF" : "",
    },
    emptyText: {
      color: "#BCC8E7",
      fontSize: "13px",
      fontFamily: "FuturaHvBT",
      top: "50%",
      left: "46%",
      position: "absolute",
    },
    emptyState: {
      display: "flex",
      height: 386,
      alignItems: "center",
      justifyContent: "center",
    },
    wrapEmpty: {
      display: "block",
    },
    wrapEmptyText: {
      display: "block",
      marginTop: 20,
      textAlign: "center",
    },
    textEmpty: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 21,
      color: "#7B87AF",
      lineHeight: "25px",
      letterSpacing: "0.03em",
    },
    noDataMessage: {
      textAlign: "center",
      fontFamily: "FuturaMdBT",
      fontSize: 21,
      color: Colors.dark.medium,
      marginTop: 24,
    },
    noDataSubMessage: {
      fontSize: 13,
      color: Colors.dark.medium,
      fontFamily: "FuturaBkBT",
      marginTop: 10,
      textAlign: "center",
    },
    noDataImg: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    tableBlank: {
      width: "100%",
      height: "100%",
      boxShadow: `0px 3px 10px rgba(188, 200, 231, 0.2)`,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginRight: -10,
    },
  });

  const classes = useStyles();

  const renderEmpty = () => (
    <Card className={classes.tableBlank}>
      <div>
        <div className={classes.noDataImg}>
          <img src={NoData} alt="no data" />
        </div>
        <div className={classes.noDataMessage}>{noDataMessage}</div>
        <div className={classes.noDataSubMessage}>{noSubMessage}</div>
      </div>
    </Card>
  );

  return (
    <div>
      <Paper className={classes.paperTable}>
        <TableContainer>
          <Table size="small">
            {headerContent}
            {showHeader ? (
              <TableHead
                className={classes.tableHead}
                style={
                  bordered
                    ? {
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        overflow: "hidden",
                      }
                    : {}
                }
              >
                <TableRow className={rounded && classes.tr}>
                  {cols.map((headerItem, index) => (
                    <TableCell
                      align={headerItem.headerAlign}
                      style={{
                        height: 36,
                        fontWeight: 900,
                        fontSize: 13,
                        fontFamily: "FuturaMdBT",
                        lineHeight: "20px",
                        border: "none",
                        backgroundColor: "#0061A7",
                        color: "#ffff",
                      }}
                    >
                      {headerItem.title}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
            ) : (
              <div />
            )}
            <TableBody
              className={classes.tableBody}
              style={{
                maxHeight,
                minHeight,
                ...(bordered && {
                  border: "1px solid #E6EAF3",
                  borderBottomLeftRadius: 10,
                  borderBottomRightRadius: 10,
                }),
              }}
            >
              {loading ? (
                <div
                  style={{ display: "flex", height: "100%", marginTop: "50px" }}
                >
                  <CircularProgress style={{ margin: "auto" }} />
                </div>
              ) : data.length > 0 ? (
                <React.Fragment>
                  {data.map((row, index) => (
                    <TableRow
                      key={row.id}
                      className={classes.tableRow}
                      onClick={onClickRow}
                      style={
                        bordered
                          ? {
                              borderBottom: "1px solid",
                              borderColor: bordered ? "#E6EAF3" : "transparent",
                            }
                          : {}
                      }
                    >
                      {cols.map((col, key) => (
                        <TableCell
                          align={col.align}
                          key={key}
                          width={col.width}
                          style={{
                            fontSize: 13,
                            fontFamily: "FuturaHvBT",
                            height: 46,
                            fontWeight: 400,
                            border: "none",
                            padding: "5px 15px",
                          }}
                        >
                          {col.render ? col.render(row, index) : row[col.key]}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {emptyState ? (
                    <div className={classes.emptyState}>
                      <div className={classes.wrapEmpty}>
                        <ClipBoardFailed />
                        <div className={classes.wrapEmptyText}>
                          <p className={classes.textEmpty}>Add New Group</p>
                          <p className={classes.textEmpty}>
                            Into Account Group
                          </p>
                        </div>
                      </div>
                    </div>
                  ) : (
                    renderEmpty()
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

TableComponent.propTypes = {
  cols: PropTypes.array,
  data: PropTypes.array,
  onClickRow: PropTypes.func,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  loading: PropTypes.bool,
  rowShadow: PropTypes.bool,
  emptyState: PropTypes.bool,
  showHeader: PropTypes.bool,
  headerContent: PropTypes.oneOfType([PropTypes.object, PropTypes.node]),
  noDataMessage: PropTypes.node,
  noSubMessage: PropTypes.string,
};

TableComponent.defaultProps = {
  cols: [],
  data: [],
  onClickRow: null,
  maxHeight: 300,
  minHeight: 300,
  loading: false,
  rowShadow: false,
  emptyState: false,
  showHeader: true,
  headerContent: null,
  noDataMessage: <div>No Data</div>,
  noSubMessage: "",
};

export default TableComponent;
