// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";

const TableComponent = (props) => {
  const {
    cols,
    data,
    onClickRow,
    maxHeight,
    showHeader = true,
    headerContent = null,
    loading,
  } = props;

  const useStyles = makeStyles({
    paperTable: {
      borderRadius: 8,
      border: "1px solid #BCC8E7",
      overflow: "hidden",
      boxShadow: "none",
    },
    tableHead: {
      display: "table",
      width: "100%",
      tableLayout: "fixed",
    },
    tableBody: {
      display: "block",
      height: maxHeight,
      position: "relative",
      overflow: "auto",
      "&::-webkit-scrollbar": {
        width: 12,
      },
      "&::-webkit-scrollbar-track": {
        background: "#F4F7FB",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb": {
        background: "#005099",
        borderRadius: 10,
      },
      "&::-webkit-scrollbar-thumb:hover": {
        background: "#004a80",
      },
    },
    tableRow: {
      display: "table",
      width: "100%",
      tableLayout: "fixed",
      "&:hover": {
        cursor: onClickRow ? "pointer" : "initial",
        "& td span": {
          fontWeight: 600,
        },
      },
    },
    emptyText: {
      color: "#BCC8E7",
      fontSize: "13px",
      fontFamily: "FuturaMdBT",
      top: "50%",
      left: "46%",
      position: "absolute",
    },
  });
  const classes = useStyles();

  const renderEmpty = () => (
    <span className={classes.emptyText}>Data Tidak Tersedia</span>
  );

  return (
    <div>
      <Paper className={classes.paperTable}>
        <TableContainer>
          <Table size="small">
            {headerContent}
            {showHeader ? (
              <TableHead className={classes.tableHead}>
                <TableRow>
                  {cols.map((headerItem, index) => (
                    <TableCell
                      align={headerItem.headerAlign}
                      style={{
                        height: 36,
                        fontWeight: 900,
                        fontSize: 13,
                        fontFamily: "FuturaHvBT",
                        lineHeight: "18px",
                        border: "none",
                        backgroundColor: "#F4F7FB",
                        color: "#7B87AF",
                      }}
                    >
                      {headerItem.title}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
            ) : (
              <div />
            )}
            <TableBody className={classes.tableBody}>
              {loading ? (
                <div style={{ display: "flex", height: "100%" }}>
                  <CircularProgress style={{ margin: "auto" }} />
                </div>
              ) : data.length > 0 ? (
                <React.Fragment>
                  {data.map((row) => (
                    <TableRow
                      key={row.id}
                      className={classes.tableRow}
                      onClick={onClickRow}
                    >
                      {cols.map((col, key) => (
                        <TableCell
                          align={col.align}
                          key={key}
                          width={col.width}
                          style={{
                            fontSize: 13,
                            fontFamily: "FuturaBkBT",
                            height: 46,
                            fontWeight: 400,
                            border: "none",
                          }}
                        >
                          {col.render(row)}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </React.Fragment>
              ) : (
                renderEmpty()
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

TableComponent.propTypes = {
  cols: PropTypes.array,
  data: PropTypes.array,
  onClickRow: PropTypes.func,
  maxHeight: PropTypes.number,
  loading: PropTypes.bool,
};

TableComponent.defaultProps = {
  cols: [],
  data: [],
  onClickRow: null,
  maxHeight: 220,
  loading: false,
};

export default TableComponent;
