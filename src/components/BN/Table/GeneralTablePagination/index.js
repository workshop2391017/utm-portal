// main
import React, { useState, useContext } from "react";
import PropTypes from "prop-types";

// libraries
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  CircularProgress,
  Paper,
  Typography,
  makeStyles,
  Collapse,
} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

// assets
import chevronLeft from "../../../../assets/icons/BN/chevron-left-white.svg";
import chevronsLeft from "../../../../assets/icons/BN/chevrons-left-white.svg";
import chevronRight from "../../../../assets/icons/BN/chevron-right-white.svg";
import chevronsRight from "../../../../assets/icons/BN/chevrons-right-white.svg";

const useStyles = (props) =>
  makeStyles({
    paperTable: {
      borderRadius: 8,
      // border: "1px solid #BCC8E7",
      overflow: "hidden",
      boxShadow: "none",
    },
    tableRow: {
      "&:hover": {
        cursor: props.onClickRow ? "pointer" : "initial",
        borderBottom: "1px solid #BCC8E7",
        "& td span": {
          fontWeight: 600,
        },
      },
    },
    paginationContainer: {
      display: "flex",
      marginTop: 24,
      justifyContent: "space-between",
    },
    pagination: {
      height: 40,
      display: "inline-block",
      "& ul": {
        height: 40,
        "& li": {
          height: 40,
          width: 40,
          margin: "0 5px",
          "& button": {
            fontFamily: "FuturaMdBT",
            height: 40,
            width: 40,
            backgroundColor: "#fff",
            borderRadius: 8,
            margin: 0,
            color: "#0061A7",
            "Mui-selected": {
              backgroundColor: "#0061A7",
            },
          },
          "& div": {
            marginTop: 15,
          },
        },
      },
    },
    buttonPagination: {
      minWidth: 40,
      height: 40,
      padding: 6,
      borderRadius: 8,
    },
    extra: {
      marginTop: 20,
      display: "flex",
      justifyContent: "flex-end",
    },
  });

/* --------------------------------- START --------------------------------- */
const TableComponent = (props) => {
  const {
    cols,
    data,
    pagination,
    page,
    setPage,
    totalPages,
    totalData,
    onClickRow,
    extraComponents,
    showExtra,
    loading,
  } = props;

  const classes = useStyles(props)();

  const rowsPerPage = 10;
  const emptyRows = rowsPerPage - data.length;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const clickFirstPage = () => {
    setPage(1);
  };

  const clickPrevPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const clickNextPage = () => {
    if (page < totalPages) {
      setPage(page + 1);
    }
  };
  const clickLastPage = () => {
    setPage(totalPages);
  };

  const createEmptyRow = () => {
    const row = [];
    for (let i = 0; i < emptyRows; i++) {
      row.push(
        <TableRow>
          <TableCell
            colSpan={8}
            style={{
              height: 46,
              borderBottom: "none",
            }}
          />
        </TableRow>
      );
    }
    return row;
  };

  return (
    <div>
      <Paper className={classes.paperTable}>
        {loading ? (
          <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
            <div style={{ margin: "auto" }}>
              <CircularProgress color="primary" size={40} />
            </div>
          </div>
        ) : (
          <TableContainer>
            <Table className={classes.table} size="small">
              <TableHead>
                <TableRow>
                  {cols.map((headerItem, index) => (
                    <TableCell
                      key={index}
                      align={headerItem.headerAlign}
                      style={{
                        height: 37,
                        fontWeight: 900,
                        fontSize: 13,
                        fontFamily: "Futura",
                        lineHeight: "18px",
                        border: "none",
                        backgroundColor: "#0061A7",
                        color: "white",
                      }}
                    >
                      {headerItem.title}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row) => (
                  <TableRow
                    key={row.id}
                    id={row.id}
                    className={classes.tableRow}
                    onClick={onClickRow ? () => onClickRow(row) : null}
                  >
                    {cols.map((col, key) => (
                      <TableCell
                        align={col.align}
                        key={key}
                        style={{
                          fontSize: 13,
                          fontFamily: "FuturaBkBT",
                          height: 46,
                          fontWeight: 400,
                          border: "none",
                        }}
                      >
                        {col.render(row)}
                      </TableCell>
                    ))}
                  </TableRow>
                ))}
                {createEmptyRow()}
              </TableBody>
            </Table>
          </TableContainer>
        )}
      </Paper>
      <Collapse in={showExtra}>
        <div className={classes.extra}>
          <div>{extraComponents}</div>
        </div>
      </Collapse>
      {pagination && (
        <div className={classes.paginationContainer}>
          <div style={{ width: "fit-content" }}>
            <Typography
              style={{
                color: "#7B87AF",
                fontFamily: "FuturaBkBT",
                fontSize: 13,
                lineHeight: "40px",
              }}
            >
              Menampilkan {(page - 1) * 11 + 1} -{" "}
              {(page - 1) * 10 + data.length} dari {totalData}
            </Typography>
          </div>
          <div style={{ width: "fit-content" }}>
            <Button
              color="primary"
              variant="contained"
              disableElevation
              disabled={page === 1}
              onClick={clickFirstPage}
              className={classes.buttonPagination}
              style={{ marginRight: 10 }}
            >
              <img alt="chevron" src={chevronsLeft} />
            </Button>
            <Button
              color="primary"
              variant="contained"
              disableElevation
              disabled={page === 1}
              onClick={clickPrevPage}
              className={classes.buttonPagination}
              style={{ marginRight: 5 }}
            >
              <img alt="chevron" src={chevronLeft} />
            </Button>
            <Pagination
              count={totalPages}
              page={page}
              onChange={handleChangePage}
              className={`hookPagination ${classes.pagination}`}
              color="primary"
              shape="rounded"
              hideNextButton
              hidePrevButton
            />
            <Button
              color="primary"
              variant="contained"
              disableElevation
              disabled={page === totalPages}
              onClick={clickNextPage}
              className={classes.buttonPagination}
              style={{ marginLeft: 5 }}
            >
              <img alt="chevron" src={chevronRight} />
            </Button>
            <Button
              color="primary"
              variant="contained"
              disableElevation
              disabled={page === totalPages}
              onClick={clickLastPage}
              className={classes.buttonPagination}
              style={{ marginLeft: 10 }}
            >
              <img alt="chevron" src={chevronsRight} />
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

TableComponent.propTypes = {
  cols: PropTypes.array,
  data: PropTypes.array,
  pagination: PropTypes.bool,
  page: PropTypes.number,
  setPage: PropTypes.func.isRequired,
  totalPages: PropTypes.number,
  totalData: PropTypes.number,
  onClickRow: PropTypes.func,
  extraComponents: PropTypes.element,
  showExtra: PropTypes.bool,
  loading: PropTypes.bool,
};

TableComponent.defaultProps = {
  cols: [],
  data: [],
  pagination: true,
  page: 1,
  totalPages: 1,
  totalData: 0,
  onClickRow: null,
  extraComponents: null,
  showExtra: false,
  loading: false,
};

export default TableComponent;
