import React, { useState } from "react";
import PropTypes from "prop-types";
import Slider from "react-slick";
import { styled } from "@material-ui/styles";
import ButtonIcon from "components/SC/IconButton";
import MagicSliderDots from "react-magic-slider-dots";
import Colors from "helpers/colors";

import "./style.scss";
import { ReactComponent as ChevronLeft } from "assets/icons/BN/chevron-left-white.svg";
import { ReactComponent as ChevronRight } from "assets/icons/BN/chevron-right-white.svg";

import Contents from "./Contents";

const Container = styled("div")(() => ({
  width: "100%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  backgroundColor: "#F4F7FB",
  marginBottom: "300px",
}));

const ContentWrapper = styled("div")(() => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "850px",
  height: "663px",
  backgroundColor: "#fff",
  borderRadius: "20px",
  overflow: "hidden",
  boxShadow: "0px 3px 10px 0px #BCC8E733",
}));

const MainContent = styled("div")(() => ({
  display: "flex",
  justifyContent: "center",
}));

const Carousel = ({ onClickNext, children, dataLength }) => {
  const [current, setCurrent] = useState(0);

  const NextArrow = ({ onClick }) =>
    current !== dataLength - 1 ? (
      <ButtonIcon
        onClick={onClick}
        style={{
          position: "absolute",
          top: "50%",
          right: "240px",
          height: "40px",
          width: "40px",
          borderRadius: "40px",
          background: Colors.primary.hard,
        }}
        icon={<ChevronRight />}
      />
    ) : null;

  const PrevArrow = ({ onClick }) =>
    current !== 0 ? (
      <ButtonIcon
        onClick={onClick}
        style={{
          position: "absolute",
          top: "50%",
          left: "240px",
          height: "40px",
          width: "40px",
          zIndex: "999",
          borderRadius: "40px",
          background: Colors.primary.hard,
        }}
        icon={<ChevronLeft />}
      />
    ) : null;

  NextArrow.defaultProps = {
    onClick: () => {},
  };

  NextArrow.propTypes = {
    onClick: PropTypes.func,
  };

  PrevArrow.defaultProps = {
    onClick: () => {},
  };

  PrevArrow.propTypes = {
    onClick: PropTypes.func,
  };

  const setting = {
    speed: 600,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    beforeChange: (curr, next) => setCurrent(next),
    appendDots: (dots) => (
      <MagicSliderDots
        dotContainerClassName="magic-dots slick-dots"
        dots={dots}
        numDotsToShow={6}
        dotWidth={40}
      />
    ),
    centerMode: true,
    centerPadding: "420px",
    slidesToShow: 1,
    slideToScroll: 1,
    focusOnSelect: true,
    dots: true,
    infinite: false,
    dotsClass: "slick-dots slick-thumb",
  };

  return (
    <Container>
      <MainContent>
        <ContentWrapper>
          <div className="slider">
            <Slider {...setting}>{children && children(current)}</Slider>
          </div>
        </ContentWrapper>
      </MainContent>
    </Container>
  );
};

Carousel.propTypes = {
  onClickNext: PropTypes.func,
  onClickBack: PropTypes.func,
};

Carousel.defaultProps = {
  onClickNext: () => {},
  onClickBack: () => {},
};

export default Carousel;
