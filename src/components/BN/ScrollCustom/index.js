/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const ScrollCustom = ({
  height,
  children,
  style,
  scrollHorizontal,
  width,
  scroll,
  ...props
}) => {
  const useStyles = makeStyles({
    scrollCustom: {
      height,
      width,
      overflowY: "scroll",
      overflowX: scrollHorizontal ? "scroll" : "hidden",
      "&::-webkit-scrollbar": {
        width: "5px",
        height: scrollHorizontal ? "5px" : "100px",
      },
      "&::-webkit-scrollbar-track": {
        width: "5px",
        borderRight: "10px solid #fff",
        borderLeft: "10px solid #fff",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#0061A7",
        height: "30px",
        width: "5px",
        borderRadius: "10px",
      },
      "&::-webkit-scrollbar-track-piece:start": {
        background: "#F4F7FB",
      },
      "&::-webkit-scrollbar-track-piece:end": {
        background: "#F4F7FB",
      },
    },
  });

  const classes = useStyles();

  if (!scroll) {
    return <div>{children}</div>;
  }

  return (
    <div className={classes.scrollCustom} style={style} {...props}>
      {children}
    </div>
  );
};

ScrollCustom.propTypes = {
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.string,
  children: PropTypes.node,
  style: PropTypes.object,
  scrollHorizontal: PropTypes.bool,
  scroll: PropTypes.bool,
};

ScrollCustom.defaultProps = {
  height: 300,
  width: "auto",
  children: null,
  style: {},
  scrollHorizontal: false,
  scroll: true,
};

export default ScrollCustom;
