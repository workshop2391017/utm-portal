import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const ScrollCustom = ({ height, children }) => {
  const useStyles = makeStyles({
    scrollCustom: {
      height,
      overflow: "auto",
      "&::-webkit-scrollbar": {
        width: "26px",
        height: "100px",
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff",
        width: "10px",
        borderRight: "10px solid #fff",
        borderLeft: "10px solid #fff",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#0061A7",
        height: "30px",
        width: "10px",
        borderRadius: "10px",
        borderRight: "11px solid #fff",
        borderLeft: "11px solid #fff",
      },
      "&::-webkit-scrollbar-track-piece:start": {
        background: "#F4F7FB",
        borderRight: "9px solid #fff",
        borderLeft: "10px solid #fff",
        marginTop: "45px",
      },
      "&::-webkit-scrollbar-track-piece:end": {
        background: "#F4F7FB",
        marginBottom: "120px",
        borderRight: "9px solid #fff",
        borderLeft: "10px solid #fff",
      },
    },
  });

  const classes = useStyles();

  return <div className={classes.scrollCustom}>{children}</div>;
};

ScrollCustom.propTypes = {
  height: PropTypes.string,
  children: PropTypes.node,
};

ScrollCustom.defaultProps = {
  height: 490,
  children: null,
};

export default ScrollCustom;
