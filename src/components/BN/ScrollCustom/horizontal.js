import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const ScrollCustomHorizontal = ({
  height,
  children,
  style,
  scrollHorizontal,
  width,
}) => {
  const useStyles = makeStyles({
    scrollCustom: {
      height,
      width,
      overflowY: "hidden",
      overflowX: "scroll",
      "&::-webkit-scrollbar": {
        width: "26px",
        height: "5px",
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff",
        width: "10px",
        borderRight: "10px solid #fff",
        borderLeft: "10px solid #fff",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#0061A7",
        height: "30px",
        width: "10px",
        borderRadius: "10px",
        borderRight: "11px solid #fff",
        borderLeft: "11px solid #fff",
      },
      "&::-webkit-scrollbar-track-piece:start": {
        background: "#F4F7FB",
        borderRight: "9px solid #fff",
        borderLeft: "10px solid #fff",
        marginTop: "45px",
      },
      "&::-webkit-scrollbar-track-piece:end": {
        background: "#F4F7FB",
        marginBottom: "120px",
        borderRight: "9px solid #fff",
        borderLeft: "10px solid #fff",
      },
    },
  });

  const classes = useStyles();

  return (
    <div className={classes.scrollCustom} style={style}>
      {children}
    </div>
  );
};

ScrollCustomHorizontal.propTypes = {
  height: PropTypes.string,
  width: PropTypes.string,
  children: PropTypes.node,
  style: PropTypes.object,
  scrollHorizontal: PropTypes.bool,
};

ScrollCustomHorizontal.defaultProps = {
  height: 300,
  width: 500,
  children: null,
  style: {},
  scrollHorizontal: false,
};

export default ScrollCustomHorizontal;
