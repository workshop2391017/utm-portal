import React from "react";
import propTypes from "prop-types";

export default function Gap(props) {
  const { width, height } = props;
  return (
    <div
      style={{
        width,
        height,
      }}
    />
  );
}

Gap.propTypes = {
  width: propTypes.string,
  height: propTypes.string,
};
Gap.defaultProps = {
  width: "20px",
  height: "20px",
};
