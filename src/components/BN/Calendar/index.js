import React from "react";
import { Calendar as AntdCalendar, Select, Col, Row } from "antd";
import { makeStyles } from "@material-ui/core";
import moment from "moment";
import classNames from "classnames";
import { ReactComponent as ChevronLeft } from "assets/icons/BN/chevron-left-blue.svg";
import PropsType from "prop-types";

const useStyle = makeStyles({
  root: {
    width: 560,
    borderRadius: 20,
    boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    overflow: "hidden",
    minHeight: 517,
    "& .ant-picker-calendar-header": {
      display: "flex",
      justifyContent: "space-between",
    },
    "& .ant-picker-calendar-year-select": {
      display: "none",
    },
    // panel
    "& .ant-picker-panel": {
      border: "none !important",
    },
    "& .ant-picker-content": {
      padding: 30,
      width: 560,
      border: "none",
    },
    "& thead tr th": {
      fontFamily: "FuturaMdBT",
      color: "#7B87AF",
      fontSize: 18,
    },
    "& tr td": {
      fontFamily: "FuturaHvBT",
      fontSize: 20,
    },
    "& tr:first-child td.ant-picker-cell": {
      paddingTop: 40,
    },
    "& .selected-range": {
      backgroundColor: "#E3E4FF",
      padding: "0 !important",
      margin: "0 !important",
      width: "135px",
      marginLeft: "-27px !important",
      display: "flex",
      justifyContent: "center",
    },
    "& .selected-range-first-child": {
      border: "2px solid #0061A7",
      height: 35,
      width: 35,
      borderRadius: 50,
      backgroundColor: "#E3E4FF",
      zIndex: 12,
      color: "#0061A7",
    },
    "& .selected-range-last-child": {
      border: "2px solid #0061A7",
      height: 35,
      width: 35,
      borderRadius: 50,
      backgroundColor: "#E3E4FF",
      color: "#0061A7",
    },
    "& tr td.ant-picker-cell": {
      paddingTop: 15,
      paddingBottom: 15,

      "& .event-dot": {
        marginTop: 6,
        width: 5,
        height: 5,
        position: "absolute",
        borderRadius: 50,
        backgroundColor: "#0061A7",
        justifyContent: "center",
      },
    },
    "& tr td.ant-picker-cell-in-view:not(.ant-picker-cell-end)": {
      color: "#374062",
    },
    "& tr td.ant-picker-cell:not(.ant-picker-cell-in-view)": {
      color: "#AEB3C6 !important",
      "& .event-dot": {
        backgroundColor: "#AEB3C6 !important",
      },
    },
    "& tr td.ant-picker-cell-in-view:first-child, & tr td.ant-picker-cell-in-view:last-child":
      {
        color: "#D14848",
      },
    "& .ant-picker-cell .ant-picker-cell-inner": {
      lineHeight: "normal",
    },
    "& .ant-picker-cell-in-view.ant-picker-cell-selected .ant-picker-cell-inner, .ant-picker-cell-in-view.ant-picker-cell-range-start .ant-picker-cell-inner, .ant-picker-cell-in-view.ant-picker-cell-range-end .ant-picker-cell-inner":
      {
        background: "none",
        "& .event-dot": {
          display: "none",
        },
        "& .ant-picker-calendar-date-value": {
          height: 35,
          width: 35,
          backgroundColor: "#0061A7",
          borderRadius: 50,
          verticalAlign: "middle",
          lineHeight: "normal",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        },
      },
    "& .ant-picker-cell-in-view.ant-picker-cell-today": {
      // height: 35,
      // width: 35,
      // border: "1px solid #0061A7",
      // borderRadius: 50,
      // verticalAlign: "middle",
      // lineHeight: "normal",
      // display: "flex",
      // alignItems: "center",
      // justifyContent: "center",
    },
    "& .ant-picker-cell-in-view.ant-picker-cell-today .ant-picker-cell-inner::before":
      {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 1,
        border: "none",
        borderRadius: 50,
        content: "",
      },
  },
});

const isSameYear = (date1, date2) =>
  date1 && date2 && moment(date1).year() === moment(date2).year();

const isSameMonth = (date1, date2) =>
  isSameYear(date1, date2) && moment(date1).month() === moment(date2).month();

const isSameDate = (date1, date2) =>
  isSameMonth(date1, date2) && moment(date1).date() === moment(date2).date();

const Calendar = ({ getListData, onPanelChange }) => {
  const classes = useStyle();

  const dateCellRender = (value, a, b) => {
    const listData = getListData(value);
    return listData.map((item) => (
      <div
        style={{
          textAlign: "center",
          display: "flex",
          justifyContent: "center",
          width: "100%",
          position: "relative",
        }}
      >
        <div className="event-dot" />
      </div>
    ));
  };

  return (
    <div className={classes.root}>
      <div className="site-calendar-demo-card">
        <AntdCalendar
          locale={{
            lang: {
              locale: "en_US",
              dateFormat: "M/D/YYYY",
              // dayFormat: "D",
              // dateTimeFormat: "M/D/YYYY HH:mm:ss",
              dayFormat: moment.updateLocale("en", {
                weekdaysMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                monthsShort: [
                  "January",
                  "February",
                  "March",
                  "April",
                  "May",
                  "June",
                  "July",
                  "August",
                  "September",
                  "October",
                  "November",
                  "December",
                ],
              }),
            },
            dateFormat: "YYYY-MM-D",
            dateTimeFormat: "YYYY-MM-D HH:mm:ss",
          }}
          dateFullCellRender={(date) => (
            <div
              className={classNames(
                `ant-picker-cell-inner`,
                `ant-picker-calendar-date`,
                {
                  [`ant-picker-calendar-date-today`]: isSameDate(
                    new Date(),
                    date
                  ),
                }
              )}
            >
              <div className="ant-picker-calendar-date-value">
                {moment(date).date()}
              </div>
              <div className="ant-picker-calendar-date-content">
                {dateCellRender && dateCellRender(date)}
              </div>
            </div>
          )}
          headerRender={({ value, onChange }) => {
            const start = 0;
            const end = 12;
            const monthOptions = [];

            const current = value.clone();
            const localeData = value.localeData();
            const months = [];
            for (let i = 0; i < 12; i++) {
              current.month(i);
              months.push(localeData.monthsShort(current));
            }

            for (let index = start; index < end; index++) {
              monthOptions.push(
                <Select.Option className="month-item" key={index}>
                  {months[index]}
                </Select.Option>
              );
            }

            const year = value.year();
            const options = [];
            for (let i = year - 10; i < year + 10; i += 1) {
              options.push(
                <Select.Option key={i} value={i} className="year-item">
                  {i}
                </Select.Option>
              );
            }

            return (
              <div
                style={{
                  padding: "30px 30px",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Row gutter={8}>
                  <Col>
                    <div
                      style={{
                        fontFamily: "FuturaHvBT",
                        color: "#374062",
                        fontSize: 22,
                      }}
                    >
                      <span>{months[value.month()]}</span>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      const now = value.clone().month(value.month() - 1);
                      onChange(now);
                    }}
                  >
                    <ChevronLeft />
                  </Col>
                  <Col
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      const now = value.clone().month(value.month() + 1);
                      onChange(now);
                    }}
                  >
                    <ChevronLeft
                      style={{ transform: "rotate(180deg)", marginLeft: 33 }}
                    />
                  </Col>
                </Row>
              </div>
            );
          }}
          fullscreen={false}
          onPanelChange={onPanelChange}
        />
      </div>
    </div>
  );
};

Calendar.prototype = {
  getListData: PropsType.func,
  onPanelChange: PropsType.func,
};

Calendar.defaultProps = {
  getListData: () => {},
  onPanelChange: () => {},
};

export default Calendar;
