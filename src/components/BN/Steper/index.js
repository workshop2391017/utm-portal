import React from "react";
import PropTypes from "prop-types";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import StepContent from "@material-ui/core/StepContent";

const ColorlibConnectorVertical = withStyles({
  alternativeLabel: {
    // top: 22,
  },
  active: {
    //   backgroundColor: "#0061A7 !important",
    //   border: "3px solid #BCC8E7 !important",
    "& $line": {
      width: "1px",
    },
  },
  completed: {
    "& $line": {
      // backgroundColor: "red",
      // border: "1px solid red",
      width: "1px",
    },
  },
  line: {
    // display: "none",
    // top: 0,
    marginTop: -58,
    border: 0,
    backgroundColor: "#0061A7",
    borderRadius: 1,
    width: "2px",
    marginLeft: -3,
    position: "absolute",
    height: 70,
    // zIndex: -1
  },
})(StepConnector);

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    // backgroundColor: "red !important",
    "& $line": {
      backgroundColor: "#0061A7 !important",
      border: "1px solid #0061A7 !important",
    },
  },
  completed: {
    "& $line": {
      backgroundColor: "#0061A7",
      border: "1px solid #0061A7",
    },
  },
  line: {
    height: 5,
    border: 0,
    backgroundColor: "#E6EAF3",
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: "#fff",
    zIndex: 10,
    color: "#0061A7",
    width: 50,
    height: 50,
    display: "flex",
    border: "1px solid #BCC8E7",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",
    "& path": {
      stroke: "#BCC8E7",
    },
  },
  active: {
    border: "1px solid #BCC8E7",
    backgroundColor: "#fff",
    "& path": {
      stroke: "#BCC8E7",
    },
  },
  completed: {
    border: "1px solid #0061A7",
    backgroundColor: "#EAF2FF",
    "& path": {
      stroke: "#0061A7",
    },
  },
});

const useColorlibStepIconVerticalStyles = makeStyles({
  root: {
    // zIndex: 1,
    color: "#0061A7",
    width: 20,
    height: 20,
    display: "flex",
    border: "4px solid #EAF2FF",
    backgroundColor: "#0061A7",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",

    "& path": {
      stroke: "#BCC8E7",
    },
  },
  active: {
    // boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    // border: "4px solid #EAF2FF",
    // backgroundColor: "#0061A7",
    "& path": {
      stroke: "#0061A7",
    },
  },
  completed: {
    // boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
    // border: "4px solid #EAF2FF",
    // backgroundColor: "#0061A7",
    "& path": {
      stroke: "#0061A7",
    },
  },
});

// ColorlibStepIcon.propTypes = {
//   /**
//    * Whether this step is active.
//    */
//   active: PropTypes.bool,
//   /**
//    * Mark the step as completed. Is passed to child components.
//    */
//   completed: PropTypes.bool,
//   /**
//    * The label displayed in the step icon.
//    */
//   icon: PropTypes.node,
// };

const useStyles = makeStyles({
  root: {
    width: "100%",
    "& .MuiPaper-root": {
      backgroundColor: "transparent",
    },
  },
  verticalLabel: {
    display: "flex",
    "& .vertical-label": {
      color: "#7B87AF",
      fontFamily: "FuturaMdBT",
      fontSize: 13,
    },
    "& .vertical-regular-extra": {
      color: "#0061A7",
      fontSize: 13,
      fontFamily: "FuturaMdBT",
    },
  },
});

const CustomizedSteppers = ({
  activeStep,
  icons,
  options,
  style,
  label,
  orientation,
}) => {
  const classes = useStyles();

  const ColorlibStepIcon = (props) => {
    const classes = useColorlibStepIconStyles();
    const { active, completed, icon } = props;

    return (
      <div
        className={clsx(classes.root, {
          [classes.active]: active,
          [classes.completed]: completed,
        })}
      >
        {icons[String(icon)]}
      </div>
    );
  };

  const ColorlibStepIconVertical = (props) => {
    const classes = useColorlibStepIconVerticalStyles();
    const { active, completed, icon } = props;

    return (
      <div
        className={clsx(classes.root, {
          [classes.active]: active,
          [classes.completed]: completed,
        })}
      >
        {icons[String(icon)]}
      </div>
    );
  };

  function getSteps() {
    return options;
  }

  const steps = getSteps();

  return (
    <div className={classes.root}>
      {label}

      <Stepper
        alternativeLabel={orientation !== "vertical"}
        orientation={orientation}
        activeStep={activeStep + 1}
        connector={
          orientation === "vertical" ? (
            <ColorlibConnectorVertical />
          ) : (
            <ColorlibConnector />
          )
        }
      >
        {orientation === "vertical"
          ? steps.map((elm, index) => (
              <Step key={index}>
                <StepLabel StepIconComponent={ColorlibStepIconVertical}>
                  <div className={classes.verticalLabel}>
                    <div className="vertical-label">{elm.label}</div>
                    {elm?.subLabel ? (
                      <div>
                        -{" "}
                        <span className="vertical-regular-extra">
                          {elm.subLabel}
                        </span>
                      </div>
                    ) : null}
                  </div>
                </StepLabel>
                <div
                  className="MuiStep-root MuiStep-vertical"
                  style={{
                    marginLeft: 30,
                    fontFamily: "FuturaHvBT",
                    fontSize: 16,
                    color: "#374062",
                    height: 58,
                  }}
                >
                  <div className="MuiCollapse-root MuiStepContent-transition MuiCollapse-entered">
                    {elm.content}
                  </div>
                </div>
              </Step>
            ))
          : steps.map((elm, index) => (
              <Step key={index}>
                <StepLabel StepIconComponent={ColorlibStepIcon}>
                  {elm.label}
                </StepLabel>
              </Step>
            ))}
      </Stepper>
    </div>
  );
};

CustomizedSteppers.propTypes = {
  activeStep: PropTypes.func.isRequired,
  icons: PropTypes.node.isRequired,
  options: PropTypes.array.isRequired,
  label: PropTypes.string.isRequired,
  style: PropTypes.object.isRequired,
  orientation: PropTypes.string,
};

CustomizedSteppers.defaultProps = {
  orientation: "horizontal",
};

export default CustomizedSteppers;
