// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Select } from "antd";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";

const AntdSelectNoBorder = ({
  value,
  onChange,
  options,
  style,
  placeholder,
  onClick,
}) => {
  const useStyles = makeStyles(() => ({
    select: {
      fontFamily: "FuturaMdBT",
      fontSize: 13,

      "& .ant-select-selector": {
        height: "40px !important",
        border: "#d9d9d9 !important",
        borderRadius: "none  !important",
        "&.ant-select-focused .ant-select-selector,.ant-select-selector:focus,.ant-select-selector:active,.ant-select-open .ant-select-selector ":
          {
            bordeColor: "#d9d9d9 !important",
            boxShadow: "none !important",
          },
        "& .ant-select-selection-search input": {
          height: "40px !important",
          lineHeight: "40px",
          border: "#d9d9d9 !important",
        },
        "& .ant-select-focused": {
          borderColor: "#d9d9d9 !important",
          boxShadow: "none !important",
          outline: "none",
        },

        "& .ant-select-item-option-active": {
          borderColor: "#d9d9d9 !important",
          boxShadow: "none !important",
        },
        "& .ant-select-selection-item": {
          fontFamily: "FuturaMdBT",
          lineHeight: "40px",
          fontSize: 13,
          color: "#0061A7",
          fontWeight: 900,
          letterSpacing: "0.03em",
          border: "#d9d9d9 !important",
        },
        "& .ant-select-selection-placeholder": {
          lineHeight: "40px",
          color: "#0061A7",
          border: "#d9d9d9 !important",
        },
      },
      "& .ant-select-arrow": {
        width: 20,
        height: 20,
        top: "41%",
        transition: "0.3s",
      },
      "&.ant-select-open": {
        border: "#d9d9d9 !important",
        "& .ant-select-arrow": {
          top: "41%",
          transform: "rotate(-180deg)",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      display: "none",
      borderRadius: 10,

      border: "#d9d9d9 !important",
      "& .ant-select-item": {
        color: "#374062",
      },
      "& .ant-select-item-option-active": {
        backgroundColor: "#F4F7FB",
      },
      "& .ant-select-item-option-selected": {
        backgroundColor: "#cfe0e6 !important",
      },
    },
    arrow: {
      height: 20,
      width: 20,
      objectFit: "cover",
      objectPosition: "center",
    },
  }));
  const classes = useStyles();

  return (
    <Select
      value={value}
      onChange={onChange}
      className={classes.select}
      style={style}
      placeholder={placeholder}
      dropdownClassName={classes.dropdown}
      suffixIcon={<img alt="arrow" src={arrow} className={classes.arrow} />}
      onClick={onClick}
      border="none"
    >
      {options.map((option) => (
        <Select.Option value={option}>{option}</Select.Option>
      ))}
    </Select>
  );
};

AntdSelectNoBorder.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};

AntdSelectNoBorder.defaultProps = {
  options: [],
  style: null,
  placeholder: "",
};

export default AntdSelectNoBorder;
