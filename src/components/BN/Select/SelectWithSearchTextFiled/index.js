/* eslint-disable no-underscore-dangle */
// main
import React, { useState, useEffect } from "react";

// libraries
import { Select } from "antd";
import { makeStyles } from "@material-ui/core";

// components
import Textfield from "../../TextField/AntdTextField";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";

const { Option } = Select;

const useStyles = makeStyles(() => ({
  select: {
    fontSize: 15,
    "& .ant-select-selector": {
      height: "40px !important",
      border: "none !important",

      borderRadius: "10px !important",
      "& .ant-select-selection-search input": {
        height: "40px !important",
        lineHeight: "40px",
      },
      "& .ant-select-selection-item": {
        lineHeight: "40px",
        fontSize: 15,
        color: "#0061A7",
      },
      "& .ant-select-selection-placeholder": {
        lineHeight: "40px",
      },
    },

    "& .ant-select-arrow": {
      width: 20,
      height: 20,
      top: "41%",
      transition: "0.3s",
    },
    "&.ant-select-open": {
      border: "none !important",

      "& .ant-select-arrow": {
        top: "41%",
        transform: "rotate(-180deg)",
      },
    },

    "& .ant-select:not(.ant-select-customize-input) .ant-select-selector": {
      border: "none !important",
    },
    "& .ant-select makeStyles-select-66 ant-select-single ant-select-show-arrow":
      {
        border: "none !important",
        "&.ant-select-open": {
          border: "none !important",

          "& .ant-select-arrow": {
            top: "41%",
            transform: "rotate(-180deg)",
          },
        },
      },

    "& .ant-select-focused, .ant-select-selector , .ant-select-selector:focus, .ant-select-selector:active, .ant-select-open, .ant-select-selector":
      {
        boxShadow: "none !important",
      },
  },
  dropdown: {
    zIndex: 1300,
    boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
    borderRadius: 10,
    "& .ant-select-item": {
      color: "#374062",
    },
    "& .ant-select-item-option-active": {
      backgroundColor: "#F4F7FB",
    },
    "& .ant-select-item-option-selected": {
      backgroundColor: "#cfe0e6 !important",
    },
  },
  arrow: {
    height: 20,
    width: 20,
    objectFit: "cover",
    objectPosition: "center",
  },
}));

const SelectWithSearchTextFiled = (props) => {
  const {
    width = "100%",
    options = [],
    value,
    onSelect,
    placeholder,
    style,
    onBlur,
  } = props;
  const classes = useStyles();
  const [filter, setFilter] = useState("");
  const [filteredOptions, setFilteredOptions] = useState(options);

  useEffect(() => {
    let _filteredOptions = options;
    _filteredOptions = _filteredOptions.filter((item) =>
      item.label.toLowerCase().startsWith(filter.toLowerCase())
    );
    setFilteredOptions(_filteredOptions);
  }, [filter, options]);

  return (
    <Select
      options={filteredOptions}
      value={value}
      placeholder={placeholder}
      onSelect={onSelect}
      style={{
        width,
        heigh: "40px",
        ...style,
      }}
      className={classes.select}
      dropdownClassName={classes.dropdown}
      suffixIcon={<img alt="arrow" src={arrow} className={classes.arrow} />}
      // onBlur={onBlur}
      dropdownRender={(menu) => (
        <React.Fragment>
          {options.length > 0 && (
            <Textfield
              type="search"
              style={{
                width: "100%",
              }}
              placeholder="Search"
              onChange={(e) => {
                setFilter(e.target.value);
              }}
              value={filter}
            />
          )}
          {menu}
        </React.Fragment>
      )}
    >
      {filteredOptions.map((item) => (
        <Option value={item.value}>{item.label}</Option>
      ))}
    </Select>
  );
};

export default SelectWithSearchTextFiled;
