/* eslint-disable no-underscore-dangle */
// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// libraries
import { Select } from "antd";
import { makeStyles } from "@material-ui/core";

// components
import Textfield from "../../TextField/AntdTextField";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";

const { Option } = Select;

const useStyles = (isResponsive) =>
  makeStyles((theme) => ({
    select: {
      fontSize: 15,
      ...(isResponsive && {
        [theme.breakpoints.up("md")]: {
          width: "180px !important",
          fontSize: "13px !important",
        },
        [theme.breakpoints.up("lg")]: {
          width: "100%",
          fontSize: 15,
        },
        [theme.breakpoints.up("xl")]: {
          width: "100%",
          fontSize: 15,
        },
      }),
      "& .ant-select-selector": {
        height: "40px !important",
        border: "1px solid #BCC8E7 !important",
        borderRadius: "10px !important",
        "& .ant-select-selection-search input": {
          height: "40px !important",
          lineHeight: "40px",
        },
        "& .ant-select-selection-item": {
          lineHeight: "40px",
          fontSize: 15,
          ...(isResponsive && {
            [theme.breakpoints.up("md")]: {
              fontSize: "13px !important",
            },
            [theme.breakpoints.up("lg")]: {
              fontSize: 15,
            },
            [theme.breakpoints.up("xl")]: {
              fontSize: 15,
            },
          }),
          color: "#0061A7",
        },
        "& .ant-select-selection-placeholder": {
          lineHeight: "40px",
        },
      },
      "& .ant-select-arrow": {
        width: 20,
        height: 20,
        top: "41%",
        transition: "0.3s",
      },
      "&.ant-select-open": {
        border: "#d9d9d9 !important",
        "& .ant-select-arrow": {
          top: "41%",
          transform: "rotate(-180deg)",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
      borderRadius: 10,
      width: 300,
      border: "#d9d9d9 !important",
      "& .ant-select-item": {
        color: "#374062",
      },
      "& .ant-select-item-option-active": {
        backgroundColor: "#F4F7FB",
      },
      "& .ant-select-item-option-selected": {
        backgroundColor: "#cfe0e6 !important",
      },
    },
    arrow: {
      height: 20,
      width: 20,
      objectFit: "cover",
      objectPosition: "center",
    },
  }));

const SelectWithSearch = (props) => {
  const {
    width,
    dropdownMatchSelectWidth,
    options,
    value,
    onSelect,
    onChange,
    placeholder,
    style,
    onBlur,
    disabled,
    searchKey,
    isResponsive,
    onClick,
    loading,
  } = props;
  const classes = useStyles(isResponsive)();
  const [filter, setFilter] = useState("");
  const [filteredOptions, setFilteredOptions] = useState(options);

  useEffect(() => {
    let _filteredOptions = options;
    _filteredOptions = _filteredOptions?.filter(
      (item) =>
        item[searchKey]?.toLowerCase().includes(filter.toLowerCase()) ||
        item?.label?.toLowerCase().includes(filter.toLowerCase())
    );
    setFilteredOptions(_filteredOptions);
  }, [filter, options]);

  const handleChangeVisibile = (open) => {
    if (!open) {
      setFilter("");
    }
  };

  return (
    <Select
      loading={loading}
      options={filteredOptions}
      value={value || null}
      placeholder={placeholder}
      onSelect={onSelect}
      onChange={onChange}
      onClick={onClick}
      style={{
        width,
        heigh: "40px",
        ...style,
      }}
      className={classes.select}
      dropdownStyle={{
        zIndex: 1300,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        borderRadius: 10,
        width: 300,
        border: "#d9d9d9 !important",
        "& .ant-select-item": {
          color: "#374062",
        },
        "& .ant-select-item-option-active": {
          backgroundColor: "#F4F7FB",
        },
        "& .ant-select-item-option-selected": {
          backgroundColor: "#cfe0e6 !important",
        },
      }}
      suffixIcon={<img alt="arrow" src={arrow} className={classes.arrow} />}
      onBlur={onBlur}
      disabled={disabled}
      onDropdownVisibleChange={handleChangeVisibile}
      dropdownMatchSelectWidth={dropdownMatchSelectWidth}
      dropdownRender={(menu) => (
        <React.Fragment>
          {options?.length > 0 && (
            <Textfield
              id="dropdown-search"
              type="search"
              autoFocus
              style={{
                width: "100%",
              }}
              placeholder="search..."
              onChange={(e) => {
                setFilter(e.target.value);
              }}
              value={filter}
            />
          )}
          {menu}
        </React.Fragment>
      )}
    >
      {/* {filteredOptions.map((item, i) => (
        <Option value={item.value} key={`${item?.id}-${i}`}>
          {item.label}
        </Option>
      ))} */}
    </Select>
  );
};

SelectWithSearch.propTypes = {
  options: PropTypes.array,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  value: PropTypes.string,
  onSelect: PropTypes.func,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  style: PropTypes.object,
  onBlur: PropTypes.func,
  disabled: PropTypes.bool,
  searchKey: PropTypes.string,
  isResponsive: PropTypes.bool,
  onClick: PropTypes.func,
  loading: PropTypes.bool,
};

SelectWithSearch.defaultProps = {
  options: [],
  width: "100%",
  value: "",
  onSelect: () => {},
  onChange: () => {},
  placeholder: "",
  style: {},
  onBlur: () => {},
  disabled: false,
  searchKey: "label",
  isResponsive: false,
  onClick: () => {},
  loading: false,
};
export default SelectWithSearch;
