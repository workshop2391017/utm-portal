import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { handleSegmenTypeSelect } from "stores/actions/segmenTypeSelect";
import SelectWithCheckbox from "../SelectWithCheckBox";

const SegmentationTypeSelect = ({
  onSelect = () => {},
  value,
  placeholder,
  mode,
  width,
}) => {
  const dispatch = useDispatch();
  const { data, isLoading } = useSelector((e) => e.segmenTypeSelect);

  useEffect(() => {
    dispatch(handleSegmenTypeSelect({}));
  }, []);

  const options = (data?.listSegmentationProduct ?? []).map((elm) => ({
    label: elm.name,
    value: elm.id,
    ...elm,
  }));

  const handleValue = () => {
    if (mode === "multiple") {
      return (options ?? [])
        .filter((e) => value.includes(e.value))
        .map((elm) => elm.label);
    }

    return (options ?? []).find((e) => e.value === value)?.label;
  };

  return (
    <SelectWithCheckbox
      options={options}
      onSelect={onSelect}
      value={handleValue()}
      isLoading={isLoading}
      mode={mode}
      placeholder={placeholder}
      width={width}
    />
  );
};

export default React.memo(SegmentationTypeSelect);
