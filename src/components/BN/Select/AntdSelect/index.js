// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Select } from "antd";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";
import check from "../../../../assets/icons/BN/check-blue.svg";

const AntdSelect = ({
  value,
  onChange,
  options,
  style,
  placeholder,
  disabled,
}) => {
  const useStyles = makeStyles(() => ({
    select: {
      fontSize: 15,
      "& .ant-select-selector": {
        height: "40px !important",
        border: "1px solid #0061A7 !important",
        borderRadius: "10px !important",
        "& .ant-select-selection-search input": {
          height: "40px !important",
          lineHeight: "40px",
        },
        "& .ant-select-selection-item": {
          lineHeight: "40px",
          fontSize: 15,
          color: "#0061A7",
        },
        "& .ant-select-selection-placeholder": {
          lineHeight: "40px",
        },
      },
      "& .ant-select-arrow": {
        width: 20,
        height: 20,
        top: "41%",
        transition: "0.3s",
      },
      "&.ant-select-open": {
        "& .ant-select-arrow": {
          top: "41%",
          transform: "rotate(-180deg)",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
      borderRadius: 10,
      "& .ant-select-item": {
        color: "#374062",
      },
      "& .ant-select-item-option-active": {
        backgroundColor: "#F4F7FB",
      },
      "& .ant-select-item-option-selected": {
        backgroundColor: "#cfe0e6 !important",
      },
    },
    arrow: {
      height: 20,
      width: 20,
      objectFit: "cover",
      objectPosition: "center",
    },
  }));
  const classes = useStyles();

  return (
    <Select
      disabled={disabled}
      value={value}
      onChange={onChange}
      className={classes.select}
      style={style}
      placeholder={placeholder}
      dropdownClassName={classes.dropdown}
      suffixIcon={
        <img
          alt="arrow"
          src={arrow}
          className={classes.arrow}
          multiple={false}
        />
      }
    >
      {options.map((option) => (
        <Select.Option value={option}>{option}</Select.Option>
      ))}
    </Select>
  );
};

AntdSelect.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};

AntdSelect.defaultProps = {
  options: [],
  style: null,
  placeholder: "",
};

export default AntdSelect;
