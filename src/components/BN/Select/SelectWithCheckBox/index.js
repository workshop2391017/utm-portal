/* eslint-disable no-underscore-dangle */
// main
import React, { useState, useEffect } from "react";

// libraries
import { Select } from "antd";
import { CircularProgress, makeStyles } from "@material-ui/core";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";

// components
import Textfield from "../../TextField/AntdTextField";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";

const { Option } = Select;

const useStyles = (mode, { width }) =>
  makeStyles(() => ({
    select: {
      width: width || "286px !important",
      fontSize: 15,
      "& .ant-select:not(.ant-select-customize-input) .ant-select-selector": {
        borderColor: "#0061A7 !important",
      },
      "& .ant-select-selector": {
        position: "relative",
        height: "40px !important",
        borderRadius: "10px !important",
        "& .ant-select-selection-search input": {
          height: "40px !important",
          lineHeight: "40px",
        },
        "& .ant-select-selection-item": {
          lineHeight: "40px",
          fontSize: 15,
          color: "#0061A7",
        },
        "& .ant-select-selection-placeholder": {
          lineHeight: "40px",
        },
      },
      ...(mode === "multiple" && {
        "& .ant-select-selection-item": {
          lineHeight: "20px !important",
          border: "1px solid #66A3FF",
          color: "#66A3FF !important",
          backgroundColor: "#EAF2FF",
          borderRadius: "8px",
        },
      }),
      "& .ant-select-arrow": {
        width: 20,
        height: 20,
        top: "41%",
        transition: "0.3s",
      },
      "&.ant-select-open": {
        border: "none !important",

        "& .ant-select-arrow": {
          top: "41%",
          transform: "rotate(-180deg)",
        },
      },

      "& .ant-select makeStyles-select-66 ant-select-single ant-select-show-arrow":
        {
          border: "none !important",
          "&.ant-select-open": {
            border: "none !important",

            "& .ant-select-arrow": {
              top: "41%",
              transform: "rotate(-180deg)",
            },
          },
        },

      "& .ant-select-focused, .ant-select-selector , .ant-select-selector:focus, .ant-select-selector:active, .ant-select-open, .ant-select-selector":
        {
          boxShadow: "none !important",
        },
    },
    dropdown: {
      zIndex: 1300,

      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
      borderRadius: 10,
      "& .ant-select-item": {
        color: "#374062",
      },
      "& .ant-select-item-option-active": {
        backgroundColor: "#F4F7FB",
      },
      "& .ant-select-item-option-selected": {
        backgroundColor: "#cfe0e6 !important",
      },
    },
    arrow: {
      height: 20,
      width: 20,
      objectFit: "cover",
      objectPosition: "center",
    },
  }));

const SelectWithCheckbox = (props) => {
  const {
    width,
    options = [],
    value,
    onSelect,
    placeholder,
    style,
    onBlur,
    isLoading,
    // mode = "multiple",
    mode,
  } = props;
  const classes = useStyles(mode, { width })();
  const [filter, setFilter] = useState("");
  const [filteredOptions, setFilteredOptions] = useState(options);

  useEffect(() => {
    let _filteredOptions = options;
    _filteredOptions = _filteredOptions.filter((item) =>
      item?.label?.toLowerCase()?.startsWith(filter?.toLowerCase())
    );

    setFilteredOptions(_filteredOptions);
  }, [filter, options]);

  return (
    <Select
      value={value}
      placeholder={placeholder}
      onChange={onSelect}
      style={{
        heigh: "40px",
        ...style,
      }}
      className={classes.select}
      dropdownClassName={classes.dropdown}
      suffixIcon={<img alt="arrow" src={arrow} className={classes.arrow} />}
      onBlur={onBlur}
      mode={mode}
      optionLabelProp="label"
      dropdownRender={(menu) => (
        <React.Fragment>
          {options.length > 0 && (
            <Textfield
              type="search"
              style={{
                width: "100%",
              }}
              placeholder="pencarian..."
              onChange={(e) => {
                setFilter(e.target.value);
              }}
              value={filter}
            />
          )}
          {menu}
        </React.Fragment>
      )}
    >
      {isLoading ? (
        <div>
          <CircularProgress color="primary" size={40} />
        </div>
      ) : (
        filteredOptions?.map((option, i) => (
          <Option
            key={option.value}
            value={option.value}
            label={options.label}
            suffixIcon={false}
          >
            {mode === "multiple" ? (
              <CheckboxSingle
                checked={
                  (value || [])?.findIndex((e) => e === option?.value) >= 0
                }
                label={option?.label}
              />
            ) : (
              option?.label
            )}
          </Option>
        ))
      )}
    </Select>
  );
};

export default SelectWithCheckbox;
