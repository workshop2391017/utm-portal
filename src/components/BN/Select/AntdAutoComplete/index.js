/* eslint-disable react/destructuring-assignment */
// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { AutoComplete } from "antd";

// JSS
const useStyles = makeStyles({
  auto: {
    "& .ant-input-affix-wrapper-focused": {
      borderColor: "#0061A7",
    },
    "& .ant-select-selector": {
      "& .ant-select-selection-placeholder": {
        fontWeight: 400,
        color: "#BCC8E7",
        lineHeight: "32px",
        opacity: 1,
      },
    },
  },
  dropdown: {
    boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
  },
});

const AntdAutoComplete = (props) => {
  const classes = useStyles();
  return (
    <AutoComplete
      type={props.type}
      style={props.style}
      options={props.options}
      placeholder={props.placeholder}
      filterOption={(inputValue, option) =>
        option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
      }
      onChange={props.onChange}
      className={classes.auto}
      dropdownClassName={classes.dropdown}
    >
      {props.children}
    </AutoComplete>
  );
};

AntdAutoComplete.propTypes = {
  type: PropTypes.string,
  options: PropTypes.array,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  //   suffixIcon: PropTypes.object,
  //   onClickSearch: PropTypes.func,
  onChange: PropTypes.func,
};

AntdAutoComplete.defaultProps = {
  options: [],
  style: null,
  placeholder: "",
  //   suffixIcon: null,
  type: "search",
  //   onClickSearch: () => console.warn('click search'),
  onChange: () => console.warn("input"),
};

export default AntdAutoComplete;
