// RichText Component
// --------------------------------------------------------

import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import draftToHtml from "draftjs-to-html";
import { unemojify } from "node-emoji";
import SVGEraser from "assets/icons/BN/eraser.svg";
import SVGSmile from "assets/icons/BN/smile.svg";

import "./text-editor.scss";

const RichText = ({ onChange, value, formName, placeholder, ...rest }) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [touched, setTouched] = useState(false);

  const onEditorStateChange = (editorState) => {
    const newValue = unemojify(
      draftToHtml(convertToRaw(editorState.getCurrentContent()))
    );

    if (value !== newValue) {
      onChange(newValue);
    }
    setEditorState(editorState);
  };

  if (value && !touched && formName === "update") {
    const contentBlock = htmlToDraft(value);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(
        contentBlock.contentBlocks
      );
      setEditorState(EditorState.createWithContent(contentState));
      setTouched(true);
    }
  }
  return (
    <div className="text-editor">
      <Editor
        placeholder={placeholder}
        editorState={editorState}
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        onEditorStateChange={onEditorStateChange}
        {...rest}
      />
    </div>
  );
};

RichText.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.bool.isRequired,
  formName: PropTypes.string.isRequired,
  input: PropTypes.shape({
    onChange: PropTypes.func,
    name: PropTypes.string,
  }).isRequired,
  placeholder: PropTypes.string,
};

RichText.defaultProps = {
  onChange: () => {},
  placeholder: "Masukkan text editor",
};

export default RichText;
