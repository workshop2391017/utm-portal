// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Radio } from "antd";

const RadioGroup = (props) => {
  const { value, onChange, options, direction, width, name, style, disabled } =
    props;
  const useStyles = makeStyles({
    radio: {
      fontSize: 15,
      minWidth: width,
      color: "#374062",
      display: direction === "horizontal" ? "inline-block" : "block",
      marginBottom: 10,
      "& .ant-radio": {
        position: "relative",
        "& .ant-radio-inner": {
          width: 20,
          height: 20,
          border: "1px solid #BCC8E7",
          position: "relative",
          "&::after": {
            width: 10,
            height: 10,
            top: 4,
            left: 4,
            margin: "0px !important",
            transform: "scale(1) !important",
          },
        },
        "& .ant-radio-input": {
          width: 20,
          height: 20,
        },
      },
      "& .ant-radio-checked": {
        "& .ant-radio-inner": {
          border: "1px solid #0061A7 !important",
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <Radio.Group
      value={value}
      onChange={onChange}
      disabled={disabled}
      size="large"
      name={name}
      style={style}
    >
      {options.map((item) => (
        <Radio
          value={typeof item === "object" ? item?.value : item}
          checked={typeof item === "object" ? item?.value === value : item}
          className={classes.radio}
          disabled={item?.disabled}
          style={{ display: "flex", alignItems: "center" }}
        >
          {typeof item === "object" ? <div>{item?.label}</div> : item}
        </Radio>
      ))}
    </Radio.Group>
  );
};

RadioGroup.propTypes = {
  value: PropTypes.number.isRequired,
  name: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  direction: PropTypes.string,
  width: PropTypes.number,
  style: PropTypes.object,
  disabled: PropTypes.bool,
};

RadioGroup.defaultProps = {
  options: [],
  direction: "horizontal",
  width: 140,
  name: "",
  style: {},
  disabled: false,
};

export default RadioGroup;
