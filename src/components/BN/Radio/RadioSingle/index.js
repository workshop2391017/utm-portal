// RadioOption Component
// --------------------------------------------------------

import React from "react";
import PropTypes from "prop-types";
import { FormControlLabel } from "@material-ui/core";
import { makeStyles, styled } from "@material-ui/styles";
import { Radio } from "antd";
// import FormControlLabel from "@mui/material/FormControlLabel";
// import Radio from "@mui/material/Radio";

const RadioOption = ({
  label,
  checked,
  handleChange,
  value,
  disabled,
  style,
}) => {
  const useStyles = makeStyles({
    wrapperRadioSingle: {
      "& .MuiFormControlLabel-root": {
        marginLeft: "0px",
      },
      "& .ant-radio": {
        "& .ant-radio-inner": {
          width: 20,
          height: 20,
          border: "1px solid #BCC8E7",
          "&::after": {
            width: 10,
            height: 10,
            top: 4,
            left: 4,
            margin: "0px !important",
            transform: "scale(1) !important",
          },
        },
        "& .ant-radio-input": {
          width: 20,
          height: 20,
        },
      },
      "& .ant-radio-checked": {
        "& .ant-radio-inner": {
          border: "1px solid #0061A7 !important",
        },
      },
    },
  });

  const classes = useStyles();
  return (
    <FormControlLabel
      className={classes.wrapperRadioSingle}
      control={
        <Radio
          style={style}
          onChange={handleChange}
          checked={checked}
          value={value}
          disabled={disabled}
        />
      }
      label={label}
    />
  );
};

RadioOption.propTypes = {
  label: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  handleChange: PropTypes.func,
  value: PropTypes.string,
};

RadioOption.defaultProps = {
  label: "",
  checked: false,
  disabled: false,
  handleChange: () => {},
  value: "",
};

export default RadioOption;
