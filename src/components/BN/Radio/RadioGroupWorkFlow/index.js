// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Radio } from "antd";

const RadioGroupWorkFlow = (props) => {
  const { value, onChange, options, direction, width, name, ...rest } = props;
  const useStyles = makeStyles({
    radio: {
      fontSize: 15,
      minWidth: width,
      color: "#374062",
      display: direction === "horizontal" ? "inline-block" : "block",
      marginBottom: 10,
      "& .ant-radio": {
        "& .ant-radio-inner": {
          width: 20,
          height: 20,
          border: "1px solid #BCC8E7",
          "&::after": {
            width: 10,
            height: 10,
            top: 4,
            left: 4,
            margin: "0px !important",
            transform: "scale(1) !important",
          },
        },
        "& .ant-radio-input": {
          width: 20,
          height: 20,
        },
      },
      "& .ant-radio-checked": {
        "& .ant-radio-inner": {
          border: "1px solid #0061A7 !important",
        },
      },
    },
  });
  const classes = useStyles();

  return (
    <Radio.Group
      value={value}
      onChange={onChange}
      size="large"
      name={name}
      {...rest}
    >
      {options.map((item) => (
        <Radio
          value={item?.value ? item?.value : item}
          className={classes.radio}
        >
          {item?.label ? item?.label : item}
        </Radio>
      ))}
    </Radio.Group>
  );
};

RadioGroupWorkFlow.propTypes = {
  value: PropTypes.number.isRequired,
  name: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  direction: PropTypes.string,
  width: PropTypes.number,
};

RadioGroupWorkFlow.defaultProps = {
  options: [],
  direction: "horizontal",
  width: 140,
  name: "",
};

export default RadioGroupWorkFlow;
