/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { Input, InputNumber } from "antd";
import { makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";

// assets
import search from "assets/icons/BN/search.svg";
import { ReactComponent as SvgCircle } from "assets/icons/BN/textfield-x-circle.svg";
import { parseFirstSpace } from "utils/helpers";
import Date from "../../DatePicker/GeneralDatePicker";

// Jss
const useStyles = (isResponsive) =>
  makeStyles((theme) => ({
    input: {
      fontFamily: "FuturaBkBT",
      border: "1px solid #BCC8E7",
      borderRadius: 10,
      width: 320,
      height: 40,
      color: "#0061A7",
      overflow: "hidden",
      ...(isResponsive && {
        [theme.breakpoints.up("sm")]: {
          width: "100px",
          fontSize: "12px",
        },
        [theme.breakpoints.up("md")]: {
          width: "160px",
          fontSize: "13px ",
        },
        [theme.breakpoints.up("lg")]: {
          width: "170px",
          fontSize: "14px",
        },
        [theme.breakpoints.up("xl")]: {
          width: "170px",
          fontSize: 15,
        },
      }),
      "&.ant-input-affix-wrapper-focused": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #BCC8E7",
      },
      "& .ant-input": {
        boxShadow: "none",
        color: "#0061A7",
        fontSize: "16px",
        ...(isResponsive && {
          [theme.breakpoints.up("md")]: {
            fontSize: "13px !important",
          },
          [theme.breakpoints.up("lg")]: {
            fontSize: "16px",
          },
          [theme.breakpoints.up("xl")]: {
            fontSize: "16px",
          },
        }),
      },
      "&:disabled": {
        backgroundColor: Colors.gray.ultrasoft,
        border: `1px solid ${Colors.gray.medium}`,
      },
      "&:focus": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%) !important",
        border: "1px solid #BCC8E7",
      },
      "&::-webkit-input-placeholder": {
        fontSize: 15,
      },
    },
    align: {
      textAlign: "right !important",
    },
    nominalAlignRight: {
      "& .ant-input-number-input": {
        textAlign: "right !important",
        paddingRight: 2,
      },
      "& .ant-input-number-handler": {
        visibility: "hidden",
        width: 0,
      },
    },
    textarea: {
      border: "1px solid #BCC8E7",
      borderRadius: 10,
      fontFamily: "FuturaBkBT",

      color: "#0061A7",
      fontSize: 15,
      overflow: "hidden",
      "&.ant-input-affix-wrapper-focused": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
        border: "1px solid #BCC8E7",
      },
      "& .ant-input": {
        boxShadow: "none",
        color: "#0061A7",
      },
      "&:disabled": {
        backgroundColor: Colors.gray.ultrasoft,
        border: `1px solid ${Colors.gray.medium}`,
      },
      "&:focus": {
        boxShadow: "0 0 0 2px rgb(87 168 233 / 20%) !important",
        border: "1px solid #BCC8E7",
      },
      "&::-webkit-input-placeholder": {
        fontSize: 15,
      },
    },
    inputContainer: {
      display: "flex",
      lineHeight: "73px",
      alignItems: "center",
      // margin: align === "center" ? "0 10px" : "0 0 0 20px",
    },
  }));

const AntdTextField = (props, ref) => {
  // props
  const {
    placeholder,
    value,
    onChange,
    style,
    onSubmit,
    textAreaRows,
    type,
    disabled,
    prefix,
    suffix,
    className,
    endAdornment,
    startAdornment,
    name,
    onBlur,
    closeIcon,
    onClear,
    align,
    onFocus,
    isResponsive,
    maxLength,
    autoFocus,
    suffixStyle,
    disabledDate,
    ...rest
  } = props;
  // hooks
  const classes = useStyles(isResponsive)();

  return type === "textArea" ? (
    <Input.TextArea
      {...rest}
      rows={textAreaRows}
      placeholder={placeholder}
      value={value}
      // ref={ref}
      onChange={(e) =>
        onChange({
          ...e,
          target: { ...e.target, value: parseFirstSpace(e.target.value) },
        })
      }
      prefix={prefix}
      style={{
        ...style,
      }}
      disabled={disabled}
      className={
        className ? `${className} ${classes.textarea}` : classes.textarea
      }
      name={name}
      onBlur={onBlur}
      maxLength={maxLength}
    />
  ) : type === "password-with-eye" ? (
    <Input.Password
      {...rest}
      placeholder={placeholder}
      value={value}
      ref={ref}
      onChange={onChange}
      prefix={prefix}
      style={{
        height: 40,
        ...style,
      }}
      className={className ? `${className} ${classes.input}` : classes.input}
      disabled={disabled}
      name={name}
    />
  ) : type === "password" ? (
    <Input
      {...rest}
      type="password"
      placeholder={placeholder}
      value={value}
      ref={ref}
      onChange={onChange}
      prefix={prefix}
      suffix={suffix}
      style={{
        height: 40,
        ...style,
      }}
      className={className ? `${className} ${classes.input}` : classes.input}
      disabled={disabled}
      name={name}
    />
  ) : type === "search" ? (
    <Input
      {...rest}
      placeholder={placeholder}
      value={value}
      onChange={(e) =>
        onChange({
          ...e,
          target: { ...e.target, value: parseFirstSpace(e.target.value) },
        })
      }
      onPressEnter={onSubmit}
      prefix={prefix}
      style={{
        height: 40,
        ...style,
      }}
      suffix={
        <img
          src={search}
          alt="search"
          style={{ cursor: "pointer" }}
          onClick={onSubmit}
        />
      }
      className={className ? `${className} ${classes.input}` : classes.input}
      disabled={disabled}
      name={name}
    />
  ) : type === "date" ? (
    <div className={classes.inputContainer}>
      <Date
        value={value}
        format="DD/MM/YYYY"
        onChange={onChange}
        placeholder={placeholder}
        style={{
          fontFamily: "FuturaMdBT",
          padding: "4px 0px 4px 14px",
          ...style,
        }}
        {...rest}
        disabled={disabled}
        disabledDate={disabledDate}
      />

      {/* {item.dash && <div className={classes.range}>{item.dash}</div>} */}
    </div>
  ) : type === "nominal" ? (
    <InputNumber
      {...rest}
      disabled={disabled}
      placeholder={placeholder}
      value={value ? Number(value) : null}
      name={name}
      formatter={(value) =>
        `${value ? `${value}` : ""}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      }
      parser={(value) => value.replace(/[^0-9]\s?|(,*)/g, "")}
      onChange={(e) => onChange({ target: { name, value: e } })}
      className={
        className
          ? `${className} ${classes.input}`
          : align !== "left"
          ? `${classes.nominalAlignRight} ${className} ${classes.input}`
          : classes.input
      }
      style={{ height: "auto", padding: "5px 7px", ...style }}
      maxLength={maxLength}
    />
  ) : type === "number" ? (
    <Input
      {...rest}
      type="number"
      placeholder={placeholder}
      value={value}
      onChange={(e) =>
        onChange({
          ...e,
          target: { ...e.target, value: parseFirstSpace(e.target.value) },
        })
      }
      style={{
        height: 40,
        ...style,
      }}
      ref={ref}
      disabled={disabled}
      prefix={prefix}
      suffix={
        closeIcon ? (
          <SvgCircle onClick={onClear} style={{ cursor: "pointer" }} />
        ) : (
          suffix
        )
      }
      className={className ? `${className} ${classes.input}` : classes.input}
      endAdornment={endAdornment}
      startAdornment={startAdornment}
      name={name}
      onBlur={onBlur}
      maxLength={maxLength}
    />
  ) : (
    <Input
      {...rest}
      placeholder={placeholder}
      value={value}
      onChange={(e) =>
        onChange({
          ...e,
          target: {
            ...e.target,
            name: e.target.name,
            value: parseFirstSpace(e.target.value),
          },
        })
      }
      onFocus={onFocus}
      ref={ref}
      disabled={disabled}
      prefix={prefix}
      suffix={
        closeIcon ? (
          <SvgCircle onClick={onClear} style={{ cursor: "pointer" }} />
        ) : suffix ? (
          <button
            type="button"
            onClick={onClear}
            style={{
              display: "flex",
              alignItems: "center",
              cursor: "pointer",
              background: "transparent",
              padding: "0px",
              border: "0px",
              ...suffixStyle,
            }}
          >
            {suffix}
          </button>
        ) : undefined
      }
      className={
        className && align !== "right"
          ? `${className} ${classes.input}`
          : align === "right"
          ? `${classes.align} ${className} ${classes.input}`
          : classes.input
      }
      style={{
        height: 40,
        ...style,
      }}
      maxLength={maxLength}
      endAdornment={endAdornment}
      startAdornment={startAdornment}
      name={name}
      onBlur={onBlur}
    />
  );
};

AntdTextField.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  type: PropTypes.string,
  textAreaRows: PropTypes.number,
  disabled: PropTypes.bool,
  prefix: PropTypes.element,
  suffix: PropTypes.element,
  className: PropTypes.string,
  endAdornment: PropTypes.node,
  startAdornment: PropTypes.node,
  style: PropTypes.object,
  name: PropTypes.string,
  onBlur: PropTypes.func,
  closeIcon: PropTypes.bool,
  onClear: PropTypes.func,
  align: PropTypes.string,
  isResponsive: PropTypes.bool,
  maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  // onClickSearch: PropTypes.func,
};

AntdTextField.defaultProps = {
  placeholder: "",
  value: "",
  onChange: () => console.warn("AntdTextField"),
  onSubmit: () => console.warn("AntdTextField"),
  type: "input",
  textAreaRows: 6,
  disabled: false,
  prefix: null,
  suffix: null,
  className: "",
  endAdornment: "",
  startAdornment: "",
  style: {},
  name: "",
  closeIcon: false,
  onBlur: () => {},
  onClear: () => {},
  align: "left",
  isResponsive: false,
  maxLength: undefined,
  // onClickSearch: () => console.warn('click search'),
};

export default AntdTextField;
