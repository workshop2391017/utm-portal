/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { Input, InputNumber } from "antd";
import { makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";

// assets
import search from "../../../../assets/icons/BN/search.svg";

import Date from "../../DatePicker/GeneralDatePicker";

// Jss
const useStyles = makeStyles({
  input: {
    fontFamily: "FuturaBkBT",
    borderRadius: 10,
    width: 320,
    height: 40,
    color: "#0061A7",
    fontSize: 15,
    overflow: "hidden",
    "&.ant-input-affix-wrapper-focused": {
      boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
      border: "1px solid #BCC8E7",
    },
    "& .ant-input": {
      boxShadow: "none",
      color: "#BCC8E7",
    },
    "&:focus": {
      boxShadow: "0 0 0 2px rgb(87 168 233 / 20%) !important",
      border: "1px solid #BCC8E7",
    },
    "&::-webkit-input-placeholder": {
      fontSize: 15,
    },
    "& .ant-input-number-group-wrapper": {
      height: 40,
    },
    "& .ant-input-number-input-wrap": {
      border: "none",
    },
    "& .ant-input-number-input-wrap input": {
      height: 40,
    },
    "& .ant-input-number-group-addon": {
      backgroundColor: Colors.primary.hard,
      color: Colors.white,
      height: 40,
    },
  },
  textarea: {
    border: "1px solid #BCC8E7",
    borderRadius: 10,
  },
  inputContainer: {
    display: "flex",
    lineHeight: "73px",
    alignItems: "center",
    // margin: align === "center" ? "0 10px" : "0 0 0 20px",
  },
});

const AntdTextField = (props, ref) => {
  // props
  const {
    placeholder,
    value,
    onChange,
    style,
    onSubmit,
    textAreaRows,
    type,
    disabled,
    prefix,
    suffix,
    className,
    endAdornment,
    startAdornment,
    name,
    onBlur,
  } = props;
  // hooks
  const classes = useStyles();

  return (
    <InputNumber
      addonBefore={<div>Rp.</div>}
      placeholder={placeholder}
      value={value}
      name={name}
      formatter={(value) => value.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
      parser={(value) => value.replace(/[^0-9]\s?|(,*)/g, "")}
      onChange={(e) => onChange({ target: { name, value: e } })}
      className={className ? `${className} ${classes.input}` : classes.input}
      onBlur={onBlur}
      style={style}
    />
  );
};

AntdTextField.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  type: PropTypes.string,
  textAreaRows: PropTypes.number,
  disabled: PropTypes.bool,
  prefix: PropTypes.element,
  suffix: PropTypes.element,
  className: PropTypes.string,
  endAdornment: PropTypes.node,
  startAdornment: PropTypes.node,
  style: PropTypes.object,
  name: PropTypes.string,
  onBlur: PropTypes.func,
  // onClickSearch: PropTypes.func,
};

AntdTextField.defaultProps = {
  placeholder: 100000,
  value: "",
  onChange: () => console.warn("AntdTextField"),
  onSubmit: () => console.warn("AntdTextField"),
  type: "input",
  textAreaRows: 6,
  disabled: false,
  prefix: null,
  suffix: null,
  className: "",
  endAdornment: "",
  startAdornment: "",
  style: {},
  name: "",
  onBlur: () => {},
  // onClickSearch: () => console.warn('click search'),
};

export default AntdTextField;
