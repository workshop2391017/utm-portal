// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import TextField from "../../TextField/AntdTextField";

import { ReactComponent as SearchIcon } from "../../../../assets/icons/BN/search.svg";
// assets
import arrow from "../../../../assets/icons/BN/chevron-down-white.svg";
import Title from "../../Title";

const SearchWithDropdown = (props) => {
  const { dataSearch, setDataSearch, placeholder, style, prefix } = props;
  const useStyles = makeStyles({
    box: {
      height: "40px",
      display: "inline-block",
      width: 10,
      position: "relative",
      fontFamily: "Futura",
      borderRadius: "0 0px 0px 0",
      border: "none",
      fontSize: 15,
      fontWeight: 700,
      color: "#fff",
      paddingLeft: "20px",
      backgroundColor: "#0061A7",
    },
    search: {
      height: "40px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        marginTop: 8,
        "&:hover": {
          boxShadow: "none",
        },
        "& input": {
          color: "#0061A7",
          fontSize: 15,
          "&::placeholder": {
            color: "#BCC8E7",
            fontSize: 15,
          },
          "&::-webkit-input-placeholder": {
            fontSize: 15,
          },
        },
        "&.ant-input:placeholder-shown": {
          fontSize: 15,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
    searchInput: {
      padding: "0px 15px 0px 15px",
      borderRadius: "0px 0px 0px 0px",
      width: "100%",
    },
    span: {
      height: "40px",
      width: "110px",
      backgroundColor: "#0061A7",
      color: "#FFFFFF",
      borderRadius: "8px 0px 0px 8px",
      fontSize: "13px",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      letterSpacing: "0.03em",
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      lineHeight: "16px",
    },
  });

  const classes = useStyles();

  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <div className={classes.span}>
        <span>Marchant ID </span>
      </div>
      <TextField
        style={{ borderRadius: "0px 8px 8px 0px" }}
        prefix={prefix}
        placeholder={placeholder}
        value={dataSearch}
        onChange={(e) => setDataSearch(e.target.value)}
        // className={classes.searchInput}
        suffix={<SearchIcon style={{ width: "16px", height: "16px" }} />}
        // onClickSearch={clickSearch}
      />
    </div>
  );
};

SearchWithDropdown.propTypes = {
  dataSearch: PropTypes.object.isRequired,
  setDataSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.func,
  style: PropTypes.object,
  prefix: PropTypes.element,
};

SearchWithDropdown.defaultProps = {
  placeholder: "pencarian...",
  prefix: null,
  style: {},
};

export default SearchWithDropdown;
