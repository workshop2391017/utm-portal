// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Tooltip } from "antd";
import { ReactComponent as SearchIcon } from "assets/icons/BN/search.svg";
import Colors from "helpers/colors";
import { parseFirstSpace } from "utils/helpers";
// components
import TextField from "../../TextField/AntdTextField";

const SearchWithDropdown = (props) => {
  const {
    dataSearch,
    setDataSearch,
    placeholder,
    style,
    prefix,
    trigger,
    placement,
    inputPlaceholder,
  } = props;
  const useStyles = makeStyles({
    box: {
      height: "40px",
      display: "inline-block",
      width: 10,
      position: "relative",
      fontFamily: "FuturaBkBT",
      borderRadius: "8px 0 0 8px",
      border: "none",
      fontSize: "16px !important",
      fontWeight: 700,
      color: "#fff",
      paddingLeft: "20px",
      backgroundColor: "#0061A7",
    },
    search: {
      height: "40px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        marginTop: 8,
        "&:hover": {
          boxShadow: "none",
        },
        "& input": {
          color: "#0061A7",
          fontSize: 15,
          "&::placeholder": {
            color: "#BCC8E7",
            fontSize: 15,
          },
          "&::-webkit-input-placeholder": {
            fontSize: 15,
          },
        },
        "&.ant-input:placeholder-shown": {
          fontSize: 15,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
    searchInput: {
      padding: "0px 15px 0px 15px",
      borderRadius: "8px 8px 8px 8px",
      width: "100%",
    },
  });

  return (
    <div style={{ position: "relative" }}>
      <Tooltip
        placement={placement}
        title={<span style={{ color: Colors.dark.hard }}>{placeholder}</span>}
        color="#E6EAF3"
        trigger={trigger}
      >
        <div>
          <TextField
            style={style}
            prefix={prefix}
            placeholder={inputPlaceholder}
            value={dataSearch}
            onChange={(e) => setDataSearch(parseFirstSpace(e.target.value))}
            suffix={<SearchIcon style={{ width: "16px", height: "16px" }} />}
          />
        </div>
      </Tooltip>
    </div>
  );
};

SearchWithDropdown.propTypes = {
  dataSearch: PropTypes.object.isRequired,
  setDataSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.func,
  style: PropTypes.object,
  prefix: PropTypes.element,
  placement: PropTypes.oneOf(["bottomleft", "left"]),
  trigger: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  inputPlaceholder: PropTypes.string,
};

SearchWithDropdown.defaultProps = {
  placeholder: "Search...",
  prefix: null,
  style: {},
  placement: "left",
  trigger: ["hover", "focus", "contextMenu"],
  inputPlaceholder: "Search...",
};

export default SearchWithDropdown;
