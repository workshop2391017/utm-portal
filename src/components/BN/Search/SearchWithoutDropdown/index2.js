// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import TextField from "../../TextField/AntdTextField";

import { ReactComponent as SearchIcon } from "../../../../assets/icons/BN/search.svg";
// assets
import arrow from "../../../../assets/icons/BN/chevron-down-white.svg";
import Title from "../../Title";

const SearchWithDropdown = (props) => {
  const { dataSearch, setDataSearch, placeholder } = props;
  const useStyles = makeStyles({
    box: {
      height: "40px",
      // marginBottom: '50px',
      display: "inline-block",
      width: 10,
      // marginTop: 3,
      position: "relative",
      fontFamily: "Futura",
      borderRadius: "8px 0 0 8px",
      border: "none",
      // width: 110,
      fontSize: 15,
      fontWeight: 700,
      color: "#fff",
      paddingLeft: "20px",
      backgroundColor: "#0061A7",
    },

    search: {
      height: "40px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        marginTop: 8,
        "&:hover": {
          boxShadow: "none",
        },
        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#BCC8E7",
          },
          "&::-webkit-input-placeholder": {
            fontSize: 15,
          },
        },
        "&.ant-input:placeholder-shown": {
          fontSize: 15,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
  });

  const classes = useStyles();
  // const [input, setInput] = useState(null);

  // const clickSearch = () => {
  //   setDataSearch({
  //     input,
  //   });
  // };

  return (
    <div className={classes.search}>
      {/* <div className={classes.box}>Jenis Kartu</div> */}
      <TextField
        // type="searching"
        placeholder={placeholder}
        value={dataSearch}
        onChange={(e) => setDataSearch(e.target.value)}
        style={{
          width: 240,
          padding: "0px 15px 0px 15px",
          borderRadius: "8px 8px 8px 8px",
          top: -4,
        }}
        suffix={<SearchIcon style={{ width: "16px", height: "16px" }} />}
        // onClickSearch={clickSearch}
      />
    </div>
  );
};

SearchWithDropdown.propTypes = {
  dataSearch: PropTypes.object.isRequired,
  setDataSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.func,
};

SearchWithDropdown.defaultProps = {
  placeholder: "pencarian...",
};

export default SearchWithDropdown;
