// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";
import { Select } from "antd";

// components
import TextField from "../../TextField/AntdTextField";
import MuiAutoComplete from "../../Inputs/MuiAutoComplete";

// assets
import arrow from "../../../../assets/icons/BN/chevron-down-white.svg";

const adjustSize = (options, length, type) => {
  let size = type === "font" ? 13 : 110;
  options.forEach((item) => {
    item.split(" ").forEach(() => {
      if (item.split(" ").length === 2) {
        size = type === "font" ? 12 : 150;
      } else if (item.split(" ").length === 3) {
        size = type === "font" ? 12 : 220;
      }
    });
  });
  return size;
};

const optionsActivity = [
  {
    id: 0,
    value: "OPENING ACCOUNT",
    name: "0",
  },
  {
    id: 1,
    value: "PURCHASE",
    name: "1",
  },
  {
    id: 2,
    value: "BILLPAYMENT",
    name: "2",
  },
  {
    id: 3,
    value: "TRANSFER",
    name: "3",
  },
];

const optionsTransactionTypes = [
  {
    id: 0,
    value: "TRANSFER ALL",
    name: "TRANSFER_ALL",
  },
  {
    id: 1,
    value: "TRANSFER BANK LAIN",
    name: "TRANSFER_OFF_US",
  },
  {
    id: 2,
    value: "TRANSFER SESAMA Bank",
    name: "TRANSFER_ON_US",
  },
  {
    id: 3,
    value: "OPENING ACCOUNT",
    name: "OPENING_ACCOUNT",
  },
  {
    id: 4,
    value: "PINDAH DANA",
    name: "TRANSFER_OA",
  },
  {
    id: 5,
    value: "RTGS",
    name: "RTGS",
  },
  {
    id: 6,
    value: "SKN",
    name: "SKN",
  },
  {
    id: 7,
    value: "BILLPAYMENT PDAM",
    name: "BILLPAYMENT_PDAM",
  },
  {
    id: 8,
    value: "BILLPAYMENT DONATION",
    name: "BILLPAYMENT_DONATION",
  },
  {
    id: 9,
    value: "BILLPAYMENT INTERNET TV",
    name: "BILLPAYMENT_INTERNET_TV",
  },
  {
    id: 10,
    value: "BILLPAYMENT INSURANCE",
    name: "BILLPAYMENT_INSURANCE",
  },
  {
    id: 11,
    value: "BILLPAYMENT PEGADAIAN",
    name: "BILLPAYMENT_PEGADAIAN",
  },
  {
    id: 12,
    value: "BILLPAYMENT VA",
    name: "BILLPAYMENT_VA",
  },
  {
    id: 13,
    value: "BILLPAYMENT BANK LOAN",
    name: "BILLPAYMENT_BANK_LOAN",
  },
  {
    id: 14,
    value: "BILLPAYMENT OTHER LOAN",
    name: "BILLPAYMENT_OTHER_LOAN",
  },
  {
    id: 15,
    value: "BILLPAYMENT PHONE",
    name: "BILLPAYMENT_PHONE",
  },
  {
    id: 16,
    value: "BILLPAYMENT TRANSPORTATION",
    name: "BILLPAYMENT_TRANSPORTATION",
  },
  {
    id: 17,
    value: "BILLPAYMENT MULTIBILLER",
    name: "BILLPAYMENT_MULTIBILLER",
  },
];

const useStyles = (props, select) =>
  makeStyles({
    search: {
      height: "40px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        height: "40px",
        borderRadius: "0 8px 8px 0",
        marginTop: -4,
        border: "1px solid #BCC8E7",
        "&:hover": {
          boxShadow: "none",
        },
        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
    dropdown: {
      zIndex: 1300,
      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
      "& .ant-select-item-option-active": {
        "&:hover": {
          backgroundColor: "#F4F7FB",
        },
      },
      "& .ant-select-item-option-selected": {
        backgroundColor: "#cfe0e6 !important",
      },
    },
    select: {
      display: "inline-block",
      "& .ant-select": {
        width: adjustSize(props.options, 6, "width"),
        fontFamily: "Futura",
        "& .ant-select-selector": {
          borderRadius: "8px 0 0 8px",
          border: "none",
          width: "100%",
          fontSize: select.length >= 14 ? 12 : 13,
          fontWeight: 700,
          color: "#fff",
          backgroundColor: "#0061A7",
        },
        "& .ant-select-arrow": {
          width: 16,
          height: 16,
          top: "45%",
          transition: "0.3s",
        },
        "&.ant-select-open": {
          "& .ant-select-selector": {
            boxShadow: "none",
          },
          "& .ant-select-arrow": {
            top: "45%",
            transform: "rotate(-180deg)",
          },
        },
        "&.ant-select-focused": {
          "& .ant-select-selector": {
            boxShadow: "none",
          },
        },
      },
    },
    autoSelect: {
      display: "inline-block",
      fontFamily: "Futura",
      fontSize: 15,
    },
  });

/* --------------------------------- START --------------------------------- */
const SearchWithDropdown = (props) => {
  const { options, onClickSearch, setDataSearch } = props;
  const [select, setSelect] = useState(options ? options[0] : "");
  const [input, setInput] = useState(null);
  const [os, setOs] = useState("");

  const classes = useStyles(props, select)();

  const handleChange = (e, name) => {
    const value = e.target.value;
    const patt = /^[0-9:|-\s]*$/;

    switch (name) {
      case "Tgl & Waktu":
        if (patt.test(value)) {
          setInput(value);
        }
        break;
      case "Date & Time":
        if (patt.test(value)) {
          setInput(value);
        }
        break;
      default:
        setInput(value);
    }
  };

  const handleSubmit = () => {
    let inputNew = "";
    onClickSearch();
    switch (select) {
      case "Aktivitas":
        if (optionsActivity.forEach((item) => item.value).includes(input)) {
          optionsActivity.forEach((item) => {
            if (item.value.toLowerCase().includes(input.toLowerCase())) {
              setDataSearch({
                select,
                input: item.id,
              });
            }
          });
        } else if (input === "") {
          setDataSearch({
            select,
            input: null,
          });
        }
        break;
      case "Jenis Transaksi":
        if (
          optionsTransactionTypes.forEach((item) => item.value).includes(input)
        ) {
          optionsTransactionTypes.forEach((item) => {
            if (item.value.toLowerCase().includes(input.toLowerCase())) {
              setDataSearch({
                select,
                input: item.name,
              });
            }
          });
        } else if (input === "") {
          setDataSearch({
            select,
            input: null,
          });
        }
        break;
      case "Date & Time":
        if (input.includes(" | ")) {
          inputNew = input.split(" | ").join(" ");
        } else if (input.includes(" |")) {
          inputNew = input.split(" |").join(" ");
        } else if (input.includes("| ")) {
          inputNew = input.split("| ").join(" ");
        } else if (input.includes("|")) {
          inputNew = input.split("|").join(" ");
        } else {
          inputNew = input;
        }
        setDataSearch({
          select,
          input: input !== "" ? inputNew : null,
        });
        break;
      case "Tgl & Waktu":
        if (input.includes(" | ")) {
          inputNew = input.split(" | ").join(" ");
        } else if (input.includes(" |")) {
          inputNew = input.split(" |").join(" ");
        } else if (input.includes("| ")) {
          inputNew = input.split("| ").join(" ");
        } else if (input.includes("|")) {
          inputNew = input.split("|").join(" ");
        } else {
          inputNew = input;
        }
        setDataSearch({
          select,
          input: inputNew,
        });
        console.warn(`Submit Date & Time`);
        setInput("");
        break;
      case "Transaksi Kategori":
        setDataSearch({
          select: "transactionCategory",
          input: input !== "" ? input : null,
        });
        break;
      default:
        setDataSearch({
          select,
          input: input !== "" ? input : null,
        });
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleSubmit();
    }
  };

  useEffect(() => {
    let Syst = "Not known";
    if (navigator.appVersion.indexOf("Win") !== -1) Syst = "Windows OS";
    if (navigator.appVersion.indexOf("Mac") !== -1) Syst = "MacOS";
    if (navigator.appVersion.indexOf("X11") !== -1) Syst = "UNIX OS";
    if (navigator.appVersion.indexOf("Linux") !== -1) Syst = "Linux OS";
    setOs(Syst);
  }, []);

  const inputSelectOptions = (
    <div className={classes.autoSelect}>
      <MuiAutoComplete
        options={
          select === "Aktivitas" ? optionsActivity : optionsTransactionTypes
        }
        onChange={handleChange}
        onSubmit={handleSubmit}
        onKeyPress={handleKeyPress}
        placeholder="search..."
        name={select}
      />
    </div>
  );

  const inputRegular = (
    <TextField
      type="search"
      placeholder="search..."
      value={input}
      onChange={(e) => handleChange(e, select)}
      onSubmit={handleSubmit}
      style={{
        width: 240,
        borderRadius: "0 8px 8px 0",
        borderLeft: "none",
        top: os.toLowerCase().includes("win") ? -3 : -1,
      }}
    />
  );

  return (
    <div className={classes.search}>
      <form>
        <div className={classes.select}>
          <Select
            size="large"
            value={select}
            onChange={(value) => {
              setSelect(value);
              setInput("");
            }}
            suffixIcon={<img alt="icon-suffix" src={arrow} />}
            dropdownClassName={classes.dropdown}
          >
            {options.map((item, index) => (
              <Select.Option key={index} value={item}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </div>
        {select === "Aktivitas" || select === "Jenis Transaksi"
          ? inputSelectOptions
          : inputRegular}
      </form>
    </div>
  );
};

SearchWithDropdown.propTypes = {
  options: PropTypes.array,
  setDataSearch: PropTypes.func.isRequired,
  onClickSearch: PropTypes.func,
};

SearchWithDropdown.defaultProps = {
  options: [],
  onClickSearch: () => {},
};

export default SearchWithDropdown;
