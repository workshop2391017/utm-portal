// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  createTheme,
  ThemeProvider,
  CircularProgress,
} from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import HapusCuttTime from "../../../../assets/images/BN/HapusCuttTime.png";
import SuccessConfirmationCuttTime from "../SuccessConfirmationCuttTime";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 420,
    minHeight: 480,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "16px 25px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  desc: {
    fontFamily: "FuturaBkBT",
    margin: "5px 0 25px",
  },
}));

// ubah button disini
const buttonRedTheme = createTheme({
  palette: {
    primary: {
      // light: 'yellow',
      main: "#0061A7",
      // dark: 'green',
      contrastText: "#fff",
    },
  },
});

const DeleteCuttOffTime = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  loading,
  message,
  submessage,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);
  const [pop, setPop] = useState(false);
  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            {titles.map((item) => (
              <h1 className={classes.title}>{item}</h1>
            ))}

            <img
              src={HapusCuttTime}
              style={{ paddingTop: 30 }}
              alt="Exclamation Triangle"
            />
            <Typography
              style={{
                // color: '#374062',
                fontSize: "18px",
                fontWeight: "bold",
              }}
            >
              {message}
            </Typography>
            <p className={classes.desc}>{submessage}</p>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label={loading ? <CircularProgress size={20} /> : "Tidak"}
                width="150px"
                height="40px"
                onClick={handleClose}
                disabled={!!loading}
              />
            </div>
            <div className={classes.button} style={{ left: 30 }}>
              <ThemeProvider theme={buttonRedTheme}>
                <ButtonOutlined
                  label="Ya"
                  width="150px"
                  height="40px"
                  color="#0061A7"
                  onClick={() => setPop(true)}
                  disabled={!!loading}
                />
              </ThemeProvider>
            </div>
          </div>
        </Fade>
      </Modal>
      <SuccessConfirmationCuttTime
        isOpen={pop}
        handleClose={() => setPop(false)}
        submessage="Anda telah berhasil menghapus cut off time,Sedang menunggu persetujuan"
        message="Cut Off Time Dihapus"
      />
    </div>
  );
};

DeleteCuttOffTime.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string,
};

DeleteCuttOffTime.defaultProps = {
  title: "",
  message: "Hapus Cut Off Time ?",
  submessage: "Apakah Anda yakin ingin manghapus cit off time ?",
  onContinue: () => {},
};

export default DeleteCuttOffTime;
