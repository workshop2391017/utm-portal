// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 750,
    height: 500,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  card: {
    display: "flex",

    marginTop: "20px",
  },
  label: {
    fontWeight: "700",
    color: "#374062",
    fontSize: "13px",
    marginBottom: "10px",
    fontFamily: "Futura Md BT",
    textAlign: "start",
  },
  subtitle: {
    fontFamily: "Futura Md BT",
    fontSize: "15px",
    color: "#374062",
    textAlign: "start",
  },
  card2: {
    flex: 1,
  },
}));

const DetailTimeDespositeSpecialRate = ({
  isOpen,
  handleClose,
  title,
  message,
  submessage,
}) => {
  //   const [data,setData]=useState[
  //     {
  //       title1:"Reff No",
  //       subtitle1:"1231231231",
  //       title2:"ID Perusahaan",
  //       subtitle2:"2432423424"

  //     },
  //     {
  //       title1:"Reff No",
  //       subtitle1:"1231231231",
  //       title2:"ID Perusahaan",
  //       subtitle2:"2432423424"

  //     },
  //     {
  //       title1:"Reff No",
  //       subtitle1:"1231231231",
  //       title2:"ID Perusahaan",
  //       subtitle2:"2432423424"

  //     }
  // ];

  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => {
        handleClose();
      }, 10000);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              <p className={classes.title}>Time Deposit Special Rate</p>

              <div className={classes.card}>
                <div className={classes.card2}>
                  <Typography className={classes.label}>Reff No</Typography>
                  <Typography className={classes.subtitle}>
                    1231231231
                  </Typography>
                </div>

                <div className={classes.card2}>
                  <Typography className={classes.label}>
                    ID Perusahaan
                  </Typography>
                  <Typography className={classes.subtitle}>
                    2432423424
                  </Typography>
                </div>
              </div>

              <div className={classes.card}>
                <div className={classes.card2}>
                  <Typography className={classes.label}>
                    Foreign Currency
                  </Typography>
                  <Typography className={classes.subtitle}>
                    1231231231
                  </Typography>
                </div>

                <div className={classes.card2}>
                  <Typography className={classes.label}>
                    Nama Perusahaan
                  </Typography>
                  <Typography className={classes.subtitle}>
                    PT. Maju Jaya Abadi
                  </Typography>
                </div>
              </div>

              <div className={classes.card}>
                <div className={classes.card2}>
                  <Typography className={classes.label}>Unit</Typography>
                  <Typography className={classes.subtitle}>12</Typography>
                </div>

                <div className={classes.card2}>
                  <Typography className={classes.label}>
                    Nominal Transaksi
                  </Typography>
                  <Typography className={classes.subtitle}>
                    Rp 15.000.000.000
                  </Typography>
                </div>
              </div>

              <div className={classes.card}>
                <div className={classes.card2}>
                  <Typography className={classes.label}>
                    Berlaku Sampai
                  </Typography>
                  <Typography className={classes.subtitle}>
                    12/12/2021
                  </Typography>
                </div>

                <div className={classes.card2}>
                  <Typography className={classes.label}>Remark</Typography>
                  <Typography className={classes.subtitle}>-</Typography>
                </div>
              </div>

              <br />
              <br />
              <GeneralButton
                label="Kembali"
                width="370px"
                height="40px"
                onClick={handleClose}
                // paddingTop= '30'
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

DetailTimeDespositeSpecialRate.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string,
};

DetailTimeDespositeSpecialRate.defaultProps = {
  title: "Berhasil",
  message: "Aktivitas Berhasil Disetujui",
  submessage: "Silahkan Menunggu Persetujuan",
};

export default DetailTimeDespositeSpecialRate;
