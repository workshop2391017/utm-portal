// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import logo from "../../../../assets/images/BN/illustrationyellow.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 10,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    marginBottom: 20,
  },
}));

const AlertPopup = ({ isOpen, handleClose, title, message, submessage }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            {title && <p className={classes.title}>{title}</p>}
            <img src={logo} alt="check" />
            <Typography
              style={{
                fontSize: "18px",
                fontWeight: "bold",
                marginTop: "10px",
              }}
            >
              {message}
            </Typography>
            {submessage && (
              <React.Fragment>
                <br />
                <Typography>{submessage}</Typography>{" "}
              </React.Fragment>
            )}
            <GeneralButton
              label="OK"
              width="100%"
              height="40px"
              onClick={handleClose}
              style={{ marginTop: 20 }}
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

AlertPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string,
};

AlertPopup.defaultProps = {
  title: "",
  message: "Something went wrong",
  submessage: "",
};

export default AlertPopup;
