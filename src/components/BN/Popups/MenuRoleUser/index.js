// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { getDataListMenu, setHandleClearError } from "stores/actions/roleUser";
// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import ScrollCustom from "components/BN/ScrollCustom";
import Toast from "components/BN/Toats";
import GeneralButton from "../../Button/GeneralButton";

// assets
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 585,
    height: 696,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,

    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    padding: "30px 30px 30px 50px",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 36,
    fontWeight: 900,
    color: Colors.dark.hard,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  span1: {
    borderBottom: "1px solid #E6EAF3",
    height: "32px",
    marginTop: "8px",
  },
}));

const MenuRoleUser = ({ isOpen, handleClose, title, message, height }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [titles, setTitles] = useState([]);
  const { dataListMenu, error } = useSelector((state) => state.roleUser);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  // useEffect(() => {
  //   if (isOpen) {
  //     setTimeout(() => {
  //       handleClose();
  //     }, 2500);
  //   }
  // }, [handleClose, isOpen]);
  console.warn("dataku", dataListMenu);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 900,
        // }}
      >
        {/* <Toast
          open="true"
          message={error?.message ?? null}
          handleClose={() => dispatch(setHandleClearError())}
        /> */}
        <Fade in={isOpen}>
          <div className={classes.paper} style={{ height }}>
            <div className={classes.xContainer}>
              <XIcon className="close" onClick={handleClose} />
            </div>
            <div className={classes.content}>
              <p className={classes.title}>Menu List</p>
              <div
                style={{
                  width: "100%",
                  textAlign: "start",
                  marginTop: "32px",
                }}
              >
                <ScrollCustom height={440} width={470}>
                  {Object.keys(dataListMenu)?.length !== 0 &&
                  dataListMenu?.portalGroupDetailDto?.length !== 0 ? (
                    (
                      dataListMenu?.portalGroupDetailDto[0]?.masterMenu ?? []
                    ).map((item, index) => (
                      <div style={{ width: "900px" }}>
                        <div key={index} className={classes.span1}>
                          <span
                            style={{
                              color: "#374062",
                              fontFamily: "FuturaHvBt",
                              fontSize: 15,
                              fontWeight: 400,
                            }}
                          >
                            {item?.name_en}
                          </span>
                        </div>
                        <div>
                          {item?.menuAccessChild?.map((child, index) => (
                            <div key={index}>
                              <span
                                style={{
                                  color: "#374062",
                                  fontFamily: "FuturaBkBt",
                                  fontSize: 15,
                                  fontWeight: 400,
                                  letterSpacing: "0.03em",
                                }}
                              >
                                {child?.name_en}
                              </span>
                            </div>
                          ))}
                        </div>
                      </div>
                    ))
                  ) : (
                    <div
                      style={{
                        width: "100%",
                        height: "100%",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        marginLeft: "-8px",
                      }}
                    >
                      <span
                        style={{
                          fontFamily: "FuturaHvBT",
                          fontSize: "24px",
                          color: "#374062",
                          fontWeight: 800,
                        }}
                      >
                        No Data
                      </span>
                    </div>
                  )}
                </ScrollCustom>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  width: "100%",
                  marginTop: 28,
                }}
              >
                <GeneralButton
                  label="Close"
                  width="130px"
                  height="44px"
                  onClick={handleClose}
                  style={{
                    fontFamily: "FuturaMdBT",
                    fontSize: 15,
                  }}
                />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

MenuRoleUser.propTypes = {};

export default MenuRoleUser;
