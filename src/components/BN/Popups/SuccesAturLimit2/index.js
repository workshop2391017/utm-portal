// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 495,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const SuccessConfirmation = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      // setTimeout(() => {
      //   handleClose();
      // }, 2500);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}
              {/* <SvgIcon component={check} alt='check' style={{marginTop: 30}} /> */}
              <img src={check} alt="check" style={{ marginTop: 25 }} />
              <Typography
                style={{
                  // color: '#374062',
                  fontSize: "18px",
                  fontWeight: "bold",
                }}
              >
                {message}
              </Typography>{" "}
              <br />
              <Typography style={{ lineHeight: 0 }}>
                Anda tidak dapat membatalkan tindakan ini.
              </Typography>
              <br />
              <br />
              <GeneralButton
                label="OK"
                width="370px"
                height="40px"
                onClick={handleClose}
                // paddingTop= '30'
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

SuccessConfirmation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

SuccessConfirmation.defaultProps = {
  title: "Berhasil Atur Limit",
  message: "Rekening Berhasil Disimpan",
};

export default SuccessConfirmation;
