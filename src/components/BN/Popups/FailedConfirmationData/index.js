// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import Colors from "helpers/colors";

import GeneralButton from "../../Button/GeneralButton";

import times from "../../../../assets/images/BN/illustrationred.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 391,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "36px 26px 22px 36px",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  content: {
    // paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 6,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
  message: {
    fontSize: 24,
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    marginTop: 20,
  },
  submessage: {
    fontSize: 15,
    fontFamily: "FuturaBkBT",
  },
}));

const FailedConfirmation = ({
  isOpen,
  handleClose,
  title,
  message,
  submessage,
  warning,
  img,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.xContainer}>
              <XIcon
                className="close"
                onClick={handleClose}
                width={12}
                height={12}
              />
            </div>
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}
              <img
                src={warning ? img : times}
                alt="Times"
                style={{ marginTop: 43 }}
              />{" "}
              <br />
              <Typography className={classes.message}>{message}</Typography>
              <p className={classes.submessage}>{submessage}</p> <br />
              <GeneralButton
                // ini data user
                label="OK"
                width="380px"
                height="40px"
                onClick={handleClose}
                style={{ marginTop: 43 }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

FailedConfirmation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string,
  warning: PropTypes.bool,
  img: PropTypes.string,
};

FailedConfirmation.defaultProps = {
  title: "Fail",
  message: "Changes Failed to Save",
  submessage: "NIP Already Registered",
  warning: false,
  img: "",
};

export default FailedConfirmation;
