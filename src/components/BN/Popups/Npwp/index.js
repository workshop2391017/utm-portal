// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";
import npwp from "../../../../assets/images/BN/npwp.png";
import chevronleft from "../../../../assets/icons/BN/chevron-left-white.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paperlr: {
    width: "30px",
    position: "absolute",
    top: "30px",
    left: "30px",
  },
  textlr: {
    width: "30px",
    position: "absolute",
    top: "30px",
    left: "70px",
    color: "#fff",
    fontFamily: "FuturaHvBT",
    fontSize: 20,
  },
  paper: {
    width: 490,
    minHeight: 490,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    // borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const NpwpPopup = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => {
        handleClose();
      }, 2500);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div>
            <img className={classes.paperlr} src={chevronleft} alt="NPWP.jpg" />
            <p className={classes.textlr}>NPWP.jpg</p>
            <div className={classes.paper}>
              <div className={classes.content}>
                <img
                  src={npwp}
                  style={{
                    display: "flex",
                    width: "340px",
                    height: "200px",
                    alignContent: "center",
                    margin: "20px 50px 0px",
                    //   marginTop: "40px",
                  }}
                  alt="npwp"
                />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

NpwpPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

NpwpPopup.defaultProps = {
  title: "NPWP.jpg",
  message: "Preview NPWP",
};

export default NpwpPopup;
