// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import SuccessConfirmation from "../SuccessConfirmation";
import CheckboxGroup from "../../Checkbox/ChexboxGroup";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 700,
    minHeight: 600,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  access: {
    border: "1px solid #D3DCF4",
    borderRadius: 8,
    minHeight: 387,
    maxHeight: 387,
    padding: "10px 10px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  accessTitle: {
    fontSize: 12,
    color: "#7B87AF",
    borderBottom: "1px solid #7B87AF",
    paddingBottom: 2,
    margin: "10px 0 16px",
  },
}));

const EditPromoTags = ({
  isOpen,
  handleClose,
  label,
  tags,
  setTags,
  type,
  onContinue,
}) => {
  const classes = useStyles();
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [input, setInput] = useState(null);
  const [tagsList, setTagsList] = useState(tags);

  const province = [
    "Aceh",
    "Bali",
    "Banten",
    "Bengkulu",
    "DKI Jakarta",
    "DI Yogyakarta",
    "Gorontalo",
    "Jambi",
    "Jawa Barat",
    "Jawa Tengah",
    "Jawa Timur",
    "Kalimantan Barat",
    "Kalimantan Selatan",
    "Kalimantan Tengah",
    "Kalimantan Timur",
    "Kalimantan Utara",
    "Kepulauan Bangka Belitung",
    "Kepulauan Riau",
    "Lampung",
    "Maluku",
    "Maluku Utara",
    "Nusa Tenggara Barat",
    "Nusa Tenggara Timur",
    "Papua",
    "Papua Barat",
    "Riau",
    "Sulawesi Barat",
    "Sulawesi Selatan",
    "Sulawesi Tengah",
    "Sulawesi Tenggara",
    "Sulawesi Utara",
    "Sumatra Barat",
    "Sumatra Selatan",
    "Sumatra Utara",
  ];
  const city = [
    "Makassar",
    "Padang",
    "Palopo",
    "Parepare",
    "Palu",
    "Palembang",
    "Medan",
    "Banda Aceh",
    "Jambi",
    "Bali",
    "Denpasar",
    "Yogyakarta",
    "Jakarta",
    "Gorontalo",
    "Bengkulu",
    "Pangkalpinang",
    "Bogor",
    "Bekasi",
    "Bandung",
    "Depok",
    "Sukabumi",
    "Magelang",
    "Banjar",
    "Salatiga",
    "Semarang",
    "Surakarta",
    "Madiun",
    "Malang",
    "Mojokerto",
    "Surabaya",
    "Banjarmasin",
    "Probolinggo",
    "Ternate",
  ];
  const brand = ["Chatime", "Ace Hardware", "XXI"];
  const mid = [
    "XXI 01",
    "XXI 02",
    "Chatime 01",
    "Chatime 02",
    "Chatime 03",
    "Ace Hardware 01",
  ];
  const segment = ["Platinum", "Silver", "Gold"];
  const [tagsValue, setTagsValue] = useState([]);

  const handleCloseModal = () => {
    handleClose();
    setInput(null);
    setTagsList(tags.map((item) => item.label));
  };

  const handleOpenConfirm = () => {
    handleCloseModal();
    setConfirmSuccess(true);
    const arr = tagsList.map((item, i) => ({
      key: i,
      label: item,
    }));
    setTags(arr);
  };

  const clickSearch = (e) => {
    e.preventDefault();
    if (input) {
      const search = input.toLowerCase();
      if (type === "province") {
        setTagsValue(
          province.filter((item) => item.toLowerCase().includes(search))
        );
      } else if (type === "city") {
        setTagsValue(
          city.filter((item) => item.toLowerCase().includes(search))
        );
      } else if (type === "brand") {
        setTagsValue(
          brand.filter((item) => item.toLowerCase().includes(search))
        );
      } else if (type === "mid") {
        setTagsValue(mid.filter((item) => item.toLowerCase().includes(search)));
      } else {
        setTagsValue(
          segment.filter((item) => item.toLowerCase().includes(search))
        );
      }
    } else if (type === "province") {
      setTagsValue(province);
    } else if (type === "city") {
      setTagsValue(city);
    } else if (type === "brand") {
      setTagsValue(brand);
    } else if (type === "mid") {
      setTagsValue(mid);
    } else {
      setTagsValue(segment);
    }
  };

  const handleChangeCheckbox = (checkedValues) => {
    const add = checkedValues.filter(
      (item) => tagsList.findIndex((tag) => tag === item) === -1
    );
    const remove = tagsValue.filter(
      (item) => checkedValues.findIndex((tag) => tag === item) === -1
    );
    const tagsListRemoved = tagsList.filter(
      (item) => remove.findIndex((tag) => tag === item) === -1
    );
    setTagsList([...tagsListRemoved, ...add]);
  };

  // mapping default tags
  useEffect(() => {
    if (type) {
      if (type === "province") {
        setTagsValue(province);
      } else if (type === "city") {
        setTagsValue(city);
      } else if (type === "brand") {
        setTagsValue(brand);
      } else if (type === "mid") {
        setTagsValue(mid);
      } else {
        setTagsValue(segment);
      }
    }
  }, [type, isOpen, province, city, brand, mid, segment]);

  // mapping checked initial tags
  useEffect(() => {
    if (tags) {
      const arr = tags.map((item) => item.label);
      setTagsList(arr);
    }
  }, [tags]);

  return (
    <div>
      <SuccessConfirmation
        title="Data Ditambahkan\nMenunggu Persetujuan"
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={12}>
                <form onSubmit={clickSearch}>
                  <TextField
                    type="search"
                    placeholder="pencarian..."
                    value={input}
                    onChange={(e) => setInput(e.target.value)}
                    style={{
                      width: "100%",
                      borderRadius: "8px",
                    }}
                    onClickSearch={clickSearch}
                  />
                </form>
              </Grid>
              <Grid item xs={12}>
                <div className={classes.access}>
                  <div>
                    <CheckboxGroup
                      value={tagsList}
                      onChange={handleChangeCheckbox}
                      options={tagsValue}
                      width="170px"
                      margin="0 30px 20px 5px"
                    />
                  </div>
                </div>
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleCloseModal}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Tambah"
                width="92px"
                height="40px"
                onClick={handleOpenConfirm}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditPromoTags.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string,
  type: PropTypes.string,
};

EditPromoTags.defaultProps = {
  label: "",
  type: "province",
  onContinue: () => {},
};

export default EditPromoTags;
