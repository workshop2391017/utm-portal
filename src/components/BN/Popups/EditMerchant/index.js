// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import AddComment from "../AddComment";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 392,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
}));

const EditMerchant = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [inputID, setInputID] = useState(null);
  const [inputName, setInputName] = useState(null);
  const [inputCity, setInputCity] = useState(null);
  const [inputAddress, setInputAddress] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);

  const handleOpenConfirm = () => {
    setAddComment(false);
    setConfirmSuccess(true);
  };

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };

  return (
    <div>
      <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={4}>
                <p style={{ marginBottom: 2 }}>Merchant ID :</p>
                <TextField
                  value={inputID}
                  onChange={(e) => setInputID(e.target.value)}
                  placeholder="2131"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Nama Merchant :</p>
                <TextField
                  value={inputName}
                  onChange={(e) => setInputName(e.target.value)}
                  placeholder="Tokopedia"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <p style={{ marginBottom: 2 }}>Kota :</p>
                <TextField
                  value={inputCity}
                  onChange={(e) => setInputCity(e.target.value)}
                  placeholder="Jakarta"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Alamat :</p>
                <TextField
                  type="textArea"
                  value={inputAddress}
                  onChange={(e) => setInputAddress(e.target.value)}
                  placeholder="lantai 52 Tokopedia Tower Ciputra World 2, Jl. Prof. DR. Satrio No.3, Karet Semanggi, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950"
                  style={{ width: "100%", height: "100px" }}
                />
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={handleOpenComment}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditMerchant.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditMerchant.defaultProps = { onContinue: () => {} };

export default EditMerchant;
