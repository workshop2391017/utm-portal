// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";
import profilallert from "../../../../assets/images/BN/profilallert.png";
import ButtonOutlined from "../../Button/ButtonOutlined";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 408,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const NonaktifkanConfirmation = ({
  isOpen,
  handleClose,
  title,
  message,
  onContinue,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => {
        handleClose();
      }, 4500);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 4500,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}
              {/* <SvgIcon component={check} alt='check' style={{marginTop: 30}} /> */}
              <img src={profilallert} alt="check" style={{ marginTop: 25 }} />
              <Typography
                style={{
                  fontSize: "24px",
                  fontFamily: "FuturaMdBT",
                  fontWeight: "400",
                  width: "310px",
                  marginLeft: "55px",
                }}
              >
                {message}
              </Typography>{" "}
              <br />
              {/* <Typography style={{ lineHeight: 0 }}>
                Berhasil Menambahkan Pengguna Pengguna berhasil ditambahkan
                Silakan tunggu persetujuan dari admin approver Content Kembali
              </Typography> */}
              <br />
              <br />
              <ButtonOutlined
                label="No"
                width="158px"
                height="40px"
                style={{ float: "left", display: "block" }}
                onClick={handleClose}
                // paddingTop= '30'
              />
              <GeneralButton
                label="Yes "
                width="158px"
                height="40px"
                style={{
                  float: "right",
                  display: "block",
                }}
                onClick={() => {
                  onContinue();
                  handleClose();
                }}
                // paddingTop= '30'
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

NonaktifkanConfirmation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  //   title: PropTypes.string,
  message: PropTypes.string,
};

NonaktifkanConfirmation.defaultProps = {
  //   title: "Berhasil Menambahkan Pengguna",
  message: "Are you sure you want to deactivate this user?",
};

export default NonaktifkanConfirmation;
