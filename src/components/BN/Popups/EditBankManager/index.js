// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";
import { Checkbox } from "antd";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import DeleteModal from "../DeleteConfirmation";
import GridItemField from "../../GridItemField";
import AddComment from "../AddComment";
import SuccessConfirmation from "../SuccessConfirmation";
import RadioGroup from "../../Radio/RadioGroup";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 590,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
  checkbox: {
    marginTop: 10,
    "& .ant-checkbox-wrapper": {
      "& .ant-checkbox": {
        width: 20,
        height: 20,
        borderRadius: 3,
        "& .ant-checkbox-inner": {
          width: 20,
          height: 20,
          borderRadius: 3,
          border: "1px solid #BCC8E7",
          "&::after": {
            width: 6.5,
            height: 11.17,
            top: "42%",
          },
        },
      },
      "& .ant-checkbox-checked": {
        "& .ant-checkbox-inner": {
          border: "1px solid #0061A7 !important",
        },
      },
      "& span:last-child": {
        paddingLeft: 10,
        fontSize: 15,
        color: "#374062",
      },
    },
  },
}));

const EditBankManager = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [inputBank, setInputBank] = useState(null);
  const [inputSwift, setInputSwift] = useState(null);
  const [checkOnline, setCheckOnline] = useState(false);
  const [checkSKNClearing, setCheckSKNClearing] = useState(false);
  const [checkRTGS, setCheckRTGS] = useState(false);
  const [checkBIFast, setCheckBIFast] = useState(false);
  const [inputCodeBank1, setInputCodeBank1] = useState(null);
  const [inputCodeBank2, setInputCodeBank2] = useState(null);
  const [inputCodeBank3, setInputCodeBank3] = useState(null);
  const [inputCodeBank4, setInputCodeBank4] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [onlineSwitching, setOnlineSwitching] = useState("Jalin");

  //   const [value, setValue] = useState(null);

  //   const onChange = e => {
  //     setValue(e.target.value);
  //   };
  const handleOpenConfirm = () => {
    setAddComment(false);
    setConfirmSuccess(true);
  };

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };

  return (
    <div>
      <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Nama Bank :</p>
                <TextField
                  value={inputBank}
                  onChange={(e) => setInputBank(e.target.value)}
                  placeholder="Bank Ester"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <p style={{ marginBottom: 2 }}>Kode Swift :</p>
                <TextField
                  value={inputSwift}
                  onChange={(e) => setInputSwift(e.target.value)}
                  placeholder="2131"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid
                container
                spacing={2}
                item
                xs={8}
                style={{ marginLeft: 0, minHeight: 115 }}
              >
                <Grid item xs={4} style={{ paddingLeft: 0 }}>
                  <p style={{ marginBottom: 2 }}>Tipe Transfer :</p>
                  <div className={classes.checkbox}>
                    <Checkbox
                      checked={checkOnline}
                      onChange={(e) => {
                        setCheckOnline(!checkOnline);
                        setInputCodeBank1(null);
                      }}
                    >
                      Online
                    </Checkbox>
                  </div>
                </Grid>
                <Grid item xs={8} style={{ padding: "8px 0" }}>
                  <p style={{ marginBottom: 2, marginTop: 20 }}>Kode Bank :</p>
                  <TextField
                    value={inputCodeBank1}
                    onChange={(e) => setInputCodeBank1(e.target.value)}
                    placeholder="1231"
                    style={{ width: "100%" }}
                    disabled={!checkOnline}
                  />
                </Grid>
              </Grid>
              {checkOnline && (
                <Grid item xs={4}>
                  <p style={{ margin: "20px 0 10px 10px" }}>
                    Jenis Online Switching :
                  </p>
                  <div style={{ marginLeft: 10 }}>
                    <RadioGroup
                      value={onlineSwitching}
                      onChange={(e) => setOnlineSwitching(e.target.value)}
                      options={["Jalin", "Alto"]}
                      width={100}
                    />
                  </div>
                </Grid>
              )}
              <Grid container spacing={2} item xs={8} style={{ marginLeft: 0 }}>
                <Grid item xs={4} style={{ paddingLeft: 0 }}>
                  <div className={classes.checkbox}>
                    <Checkbox
                      checked={checkSKNClearing}
                      onChange={(e) => {
                        setCheckSKNClearing(!checkSKNClearing);
                        setInputCodeBank2(null);
                      }}
                    >
                      SKN/Clearing
                    </Checkbox>
                  </div>
                </Grid>
                <Grid item xs={4} style={{ padding: "8px 0" }}>
                  <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                  <TextField
                    value={inputCodeBank2}
                    onChange={(e) => setInputCodeBank2(e.target.value)}
                    placeholder="1231"
                    style={{ width: "100%" }}
                    disabled={!checkSKNClearing}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} item xs={8} style={{ marginLeft: 0 }}>
                <Grid item xs={4} style={{ paddingLeft: 0 }}>
                  <div className={classes.checkbox}>
                    <Checkbox
                      checked={checkRTGS}
                      onChange={(e) => {
                        setCheckRTGS(!checkRTGS);
                        setInputCodeBank3(null);
                      }}
                    >
                      RTGS
                    </Checkbox>
                  </div>
                </Grid>
                <Grid item xs={4} style={{ padding: "8px 0" }}>
                  <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                  <TextField
                    value={inputCodeBank3}
                    onChange={(e) => setInputCodeBank3(e.target.value)}
                    placeholder="1231"
                    style={{ width: "100%" }}
                    disabled={!checkRTGS}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} item xs={8} style={{ marginLeft: 0 }}>
                <Grid item xs={4} style={{ paddingLeft: 0 }}>
                  <div className={classes.checkbox}>
                    <Checkbox
                      checked={checkBIFast}
                      onChange={(e) => {
                        setCheckBIFast(!checkBIFast);
                        setInputCodeBank4(null);
                      }}
                    >
                      BI-Fast
                    </Checkbox>
                  </div>
                </Grid>
                <Grid item xs={4} style={{ padding: "8px 0" }}>
                  <p style={{ marginBottom: 2 }}>Kode Bank :</p>
                  <TextField
                    value={inputCodeBank4}
                    onChange={(e) => setInputCodeBank4(e.target.value)}
                    placeholder="1231"
                    style={{ width: "100%" }}
                    disabled={!checkBIFast}
                  />
                </Grid>
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={handleOpenComment}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditBankManager.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditBankManager.defaultProps = { onContinue: () => {} };

export default EditBankManager;

const roleOptions = [
  {
    value: 1,
    label: "Jalin",
  },
  {
    value: 2,
    label: "Alto",
  },
];
