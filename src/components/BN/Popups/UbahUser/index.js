// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";

// libraries

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import AddComment from "../AddComment";
import SuccessConfirmation from "../SuccessConfirmation";
import Select from "../../Select/AntdSelect";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 590,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
  error: {
    fontSize: 13,
    color: "#FF6F6F",
    position: "absolute",
    left: 8,
    transition: "0.3s ease-in-out",
  },
}));

const EditUser = ({
  isOpen,
  handleClose,
  onContinue,
  label,
  rowData,
  tableData,
}) => {
  const classes = useStyles();

  const [roleList, setRoleList] = useState([
    "Admin Blokir",
    "Staff Approver",
    "Staff Input",
    "Staff",
    "Staff Admin",
    "Buka Blokir",
  ]);
  const [branchList, setBranchList] = useState([
    "Kantor Pusat",
    "Wirosaban",
    "Banguntapan",
  ]);

  const [inputName, setInputName] = useState(null);
  const [inputNIP, setInputNIP] = useState(null);
  const [inputEmail, setInputEmail] = useState(null);
  const [inputUser, setInputUser] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [branch, setBranch] = useState("Kantor Pusat");
  const [role, setRole] = useState("Admin Blokir");
  const [title, setTitle] = useState("");
  const [error, setError] = useState(false);
  const [commentInput, setCommentInput] = useState(null);
  // console.log('LOOK NIP', inputNIP);
  // console.log('LOOK USER', inputUser);
  // console.log('LOOK COMMENT', commentInput);

  const handleOpenConfirm = () => {
    // addEditUser({
    //   idUser: rowData.idUser,
    //   name: inputName,
    //   username: inputUser,
    //   phoneNumber: null,
    //   email: inputEmail,
    //   role: role,
    //   branch: branch,
    //   password: null,
    //   requestComment: commentInput
    // }).then(res => console.log('LOOK RES', res))
    //   .catch(err => alert(err));
    setAddComment(false);
    setConfirmSuccess(true);
  };

  const handleOpenComment = (username) => {
    if (tableData.map((item) => item.user).includes(username)) {
      setError(true);
    } else {
      handleClose();
      setAddComment(true);
      setTimeout(() => {
        setError(false);
        setInputUser(null);
      }, 500);
    }
  };

  const handleCloseModal = () => {
    handleClose();
    setTimeout(() => {
      setError(false);
      setInputUser(null);
    }, 500);
  };

  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    label.toLowerCase().includes("ubah")
      ? setTitle("Rejection Comment")
      : setTitle("Komentar Penambahan");
  }, [label]);

  useEffect(() => {
    if (rowData !== undefined && rowData !== null) {
      setInputName(rowData.name);
      setInputNIP(rowData.nip);
      setInputEmail(rowData.email);
      setInputUser(rowData.username);
      setRole(
        roleList.find(
          (item) => item.toLowerCase() === rowData.role.toLowerCase()
        ) ?? roleList[0]
      );
      setBranch(
        branchList.find(
          (item) => item.toLowerCase() === rowData.branch.toLowerCase()
        ) ?? branchList[0]
      );
    }
  }, [rowData]);

  return (
    <div className={classes.edit}>
      <div className={classes.title}>Ubahhhh User</div>
      <div className={classes.lastField}>
        <div style={{ width: "50%", paddingRight: 30 }}>
          <div className={classes.input} style={{ marginBottom: 20 }}>
            <p style={{ marginBottom: 3 }}>Username :</p>
            <TextField
              //   value={lineNumber}
              //   onChange={(e) => setLineNumber(e.target.value)}
              style={{ width: "100%" }}
              placeholder="EntertainEster"
            />
          </div>
          <div className={classes.input} style={{ marginBottom: 20 }}>
            <p style={{ marginBottom: 3 }}>Email :</p>
            <TextField
              //   value={namaFieldEn}
              //   onChange={(e) => setNamaFieldEn(e.target.value)}
              style={{ width: "100%" }}
              placeholder="EntertainEster"
            />
          </div>
          <div className={classes.input}>
            <p style={{ marginBottom: 3 }}>Cabang :</p>
            <Select
              //   value={categorycabang}
              //   onChange={(value) => setCategoryCabang(value)}
              placeholder="Kategori"
              options={[
                {
                  name: "BILLPAY_INTERNET_TV",
                  options: ["Bizznet", "Indihome"],
                },
                {
                  name: "BILLPAY_DONASI",
                  options: ["Kitabisa1", "Kitabisa2", "Kitabisa3"],
                },
                {
                  name: "BILLPAY_LOAN",
                },
              ]}
              style={{ width: "100%" }}
            />
          </div>
        </div>
        <div style={{ width: "50%", paddingLeft: 30 }}>
          <div className={classes.input} style={{ marginBottom: 20 }}>
            <p style={{ marginBottom: 3 }}>Nama :</p>
            <TextField
              //   value={namaFieldId}
              //   onChange={(e) => setNamaFieldId(e.target.value)}
              style={{ width: "100%" }}
              placeholder="EntertainEster"
            />
          </div>
          <div className={classes.input} style={{ marginBottom: 20 }}>
            <p style={{ marginBottom: 3 }}>NIP :</p>
            <TextField
              //   value={placeholderId}
              //   onChange={(e) => setPlaceholderId(e.target.value)}
              style={{ width: "100%" }}
              placeholder="EntertainEster"
            />
          </div>
          <div className={classes.input}>
            <p style={{ marginBottom: 3 }}>Peran :</p>
            <Select
              //   value={category}
              //   onChange={(value) => setCategory(value)}
              placeholder="Kategori"
              options={[
                {
                  name: "BILLPAY_INTERNET_TV",
                  options: ["Bizznet", "Indihome"],
                },
                // {
                //   name: 'BILLPAY_DONASI',
                //   options: ['Kitabisa1', 'Kitabisa2', 'Kitabisa3'],
                // },
                // {
                //   name: 'BILLPAY_LOAN',
                // },
              ]}
              style={{ width: "100%" }}
            />
          </div>
        </div>
        <div className={classes.button} style={{ left: 30 }}>
          <ButtonOutlined
            label="Batal"
            width="77px"
            height="40px"
            //   onClick={() => history.goBack()}
          />
        </div>
        <div className={classes.button} style={{ right: 30 }}>
          <GeneralButton
            label="Simpan"
            width="92px"
            height="40px"
            //   onClick={() => setCommentModal(true)}
          />
        </div>
      </div>

      <AddComment
      // isOpen={commentModal}
      // handleClose={() => setCommentModal(false)}
      // onContinue={commentContinue}
      />
      <SuccessConfirmation
      // isOpen={successModal}
      // handleClose={() => setSuccessModal(false)}
      />
    </div>
  );
};

EditUser.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditUser.defaultProps = {
  onContinue: () => {},
};

export default EditUser;

const roleOptions = [
  {
    value: 1,
    label: "Ya",
  },
  {
    value: 2,
    label: "Tidak",
  },
];
