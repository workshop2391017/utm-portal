// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  LinearProgress,
} from "@material-ui/core";

// assets
import { Progress } from "antd";
import file from "../../../../assets/icons/BN/file-download.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 320,
    minHeight: 270,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
  },
  // MuiLinearProgress
  LProot: {
    borderRadius: 20,
    height: 13,
    top: 50,
  },
  barBottom: {
    backgroundColor: "#D3DCF4",
  },
  barTop: {
    backgroundColor: "#0061A7",
    borderRadius: 13,
    border: "2px solid #D3DCF4",
    height: "calc(100% - 1px)",
    width: "calc(100% - 1px)",
  },
}));

const InformationDownloaded = ({ isOpen, handleClose /* title */ }) => {
  const classes = useStyles();

  const [progress, setProgress] = useState(0);
  const [title, setTitle] = useState("CSV Sedang Diunduh");
  const [exit, setExit] = useState(false);

  const handleCloseModal = () => {
    handleClose();
    setTimeout(() => {
      setProgress(0);
    }, 500);
  };

  useEffect(() => {
    if (isOpen) {
      const timer = setInterval(() => {
        setProgress((oldProgress) => {
          if (oldProgress === 100) {
            setTitle("CSV Berhasil Diunduh");
            setTimeout(() => {
              handleCloseModal();
              clearInterval(timer);
              setTimeout(() => setTitle("CSV Sedang Diunduh"), 500);
              return 0;
            }, 500);
          }
          const diff = Math.random() * 10;
          return Math.min(oldProgress + diff, 100);
        });
      }, 500);
    }
  }, [handleCloseModal, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              <p className={classes.title}>
                {progress < 99 ? title : "CSV Berhasil Diunduh"}
              </p>
              <img src={file} alt="file" style={{ marginTop: 30 }} />
              <LinearProgress
                classes={{
                  root: classes.LProot,
                  colorPrimary: classes.barBottom,
                  bar: classes.barTop,
                }}
                value={progress}
                variant="determinate"
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

InformationDownloaded.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  // title: PropTypes.string
};

InformationDownloaded.defaultProps = {
  // title: 'CSV Berhasil DiUnduh'
};

export default InformationDownloaded;
