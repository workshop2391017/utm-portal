// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";

import Grid from "@material-ui/core/Grid";

import TextField from "../../TextField/AntdTextField";

// import Button from "@material-ui/core/Button";

import ButtonOutlined from "../../Button/ButtonOutlined";
import GeneralButton from "../../Button/GeneralButton";
import Search from "../../Search/SearchWithoutDropdown";
import DeletePopup from "../Delete";

import SuccessConfirm from "../BerhasilRekeningKelompok";
// assets

import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    fontFamily: "FuturaBkBt",
    fontWeight: "400",
    fontSize: 13,
    width: "214px",
    margin: "35px 0px 0px",
    // marginBottom: 15,
    textAlign: "left",
  },
  paper: {
    width: 480,
    height: 345,
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    // paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const KomentarPengubahan = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();

  const [modalLimit, setModalLimit] = useState(false);

  const [successConfirm, setSuccessConfirm] = useState(false);
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  // useEffect(() => {
  //   if (isOpen) {
  //     // setTimeout(() => {
  //     //   handleClose();
  //     // }, 2500);
  //   }
  // }, [handleClose, isOpen]);

  const handleCloseModal = () => {
    handleClose();
    // setCheckList([]);
  };

  const handleOpenConfirm = () => {
    handleCloseModal();
    setSuccessConfirm(true);
  };

  return (
    <div>
      <SuccessConfirm
        isOpen={successConfirm}
        handleClose={() => setSuccessConfirm(false)}
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 2500,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid item xs={12}>
              <Typography
                style={{
                  color: "#374062",
                  fontSize: "22px",
                  fontWeight: "400",
                  fontFamily: "FuturaHvBt",
                  textAlign: "center",
                }}
              >
                Rejection Comment
              </Typography>
              <div>
                <div className={classes.card}>
                  <div>
                    <span>Tambah Komentar</span>
                    <TextField
                      placeholder="Tidak sesuai dengan SOP"
                      style={{
                        maxWidth: "400px",
                        maxHeight: "105px",
                        minHeight: "105px",
                        minWidth: "400px",
                      }}
                      type="textArea"
                    />
                    <span style={{ color: "#BCC8E7" }}>Opsional</span>
                  </div>
                </div>
              </div>

              <div>
                <ButtonOutlined
                  label="Batal"
                  // iconPosition="startIcon"
                  // buttonIcon={<Plus />}
                  width="92px"
                  height="43px"
                  style={{
                    position: "absolute",
                    marginTop: 40,
                    left: 30,
                  }}

                  // onClick={() => setModal(true)}
                />
                <GeneralButton
                  label="Simpan"
                  // iconPosition="startIcon"
                  // buttonIcon={<Plus />}
                  width="92px"
                  height="43px"
                  style={{
                    position: "absolute",
                    marginTop: 40,
                    // marginRight: 100,
                    // position: "relative",
                    // position: 'absolute',
                    // bottom: '8px',
                    right: "30px",
                    // marginLeft: 30
                  }}
                  //   onClick={handleOpenConfirm}
                />
              </div>
            </Grid>

            <DeletePopup
              isOpen={modalLimit}
              handleClose={() => setModalLimit(false)}
              // onContinue={handleBerhasil}
              title="Konfirmasi"
              message="Anda Yakin Menyetujui Data?"
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

KomentarPengubahan.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

KomentarPengubahan.defaultProps = {
  title: "Comment",
  message: "Rejection Comment New",
};

export default KomentarPengubahan;
