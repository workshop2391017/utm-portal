// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Box,
  Typography,
  Backdrop,
  Fade,
  Modal,
} from "@material-ui/core";
import { Statistic } from "antd";

// components
import GeneralButton from "../../Button/GeneralButton";

// assets
import logo from "../../../../assets/icons/BN/exclamation-triangle.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& ::selection": {
      background: "#137EE1",
    },
  },
  titleModal: {
    marginTop: 24.44,
    fontFamily: "FuturaHvBT",
    textAlign: "center",
    color: "#374062",
    fontWeight: "900px",
    fontSize: "20px",
    lineHeight: "23.97px",
  },
  close: {
    textAlign: "right",
    cursor: "pointer",
  },
  paper: {
    width: "410px",
    height: "auto",
    backgroundColor: "white",
    border: "1px solid #BCC8E7",
    padding: "30px",
    alignItems: "center",
    borderRadius: "6px",
  },
  logo: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    top: "5.56%",
  },
  timer: {
    "& .ant-statistic-content": {
      fontFamily: "Futura",
      color: "#374062",
      fontSize: 15,
      fontWeight: 700,
      textAlign: "center",
    },
  },
}));

// eslint-disable-next-line react/prop-types
const PopupUpWrongPass = ({ isOpen, handleClose }) => {
  const classes = useStyles();
  const [disable, setDisable] = useState(true);
  const [timerValue, setTimerValue] = useState(0);

  const clickTutup = () => {
    handleClose();
    setDisable(true);
  };

  const handleTimerFinish = () => {
    setTimerValue(0);
    setDisable(false);
  };

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => setTimerValue(Date.now() + 60200), 200);
    } else {
      setTimerValue(0);
    }
  }, [isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <img src={logo} alt="Warning Sign" className={classes.logo} />
            <div>
              <Typography className={classes.titleModal}>
                Akun Anda Ditangguhkan
              </Typography>
            </div>
            <Box display="flex" justifyContent="flex-start" mt={3}>
              <Box>
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    textAlign: "center",
                    lineHeight: "16px",
                    marginBottom: 20,
                  }}
                >
                  Akun anda saat ini sedang ditangguhkan dikarenakan salah
                  memasukan password sebanyak 3x
                </Typography>
              </Box>
            </Box>
            <Box>
              <Statistic.Countdown
                value={timerValue}
                format="HH : mm : ss"
                className={classes.timer}
                onFinish={handleTimerFinish}
              />
            </Box>
            <Box>
              <Typography
                variant="subtitle2"
                style={{
                  color: "#374062",
                  fontSize: "13px",
                  textAlign: "center",
                  lineHeight: "16px",
                  marginTop: "20px",
                }}
              >
                Silahkan coba kembali setelah timer diatas selesai melakukan
                hitungan mundur
              </Typography>
            </Box>
            <Box display="flex" justifyContent="space-around" mt={3}>
              <Box>
                <GeneralButton
                  label="Tutup"
                  width="357px"
                  height="40px"
                  variant="contained"
                  onClick={clickTutup}
                  disabled={disable}
                />
              </Box>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupUpWrongPass.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default PopupUpWrongPass;
