import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import { useDispatch } from "react-redux";
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import DeletePopup from "../Delete";
import SuccessConfirmation from "../SuccessConfirmation";
import { bindToken } from "stores/actions/managementHardToken";
import { useHistory } from "react-router-dom";

const EditHardToken = ({ isOpen, handleClose, data, corporateId }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useHistory();
  const [showModalConfirmation, setShowModalConfirmation] = useState(false);
  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const [hardTokenInput, setHardTokenInput] = useState("");

  /* ---------------------------- HANDLER FUNCTIONS --------------------------- */
  const handleShowSuccess = () => setShowModalSuccess(true);

  const handleNewUser = async () => {
    const payload = {
      corporateId,
      serialNumber: hardTokenInput,
      userId: data.id,
    };

    dispatch(bindToken(payload, handleShowSuccess));
  };

  /* ---------------------------------- VIEWS --------------------------------- */
  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{ timeout: 900 }}
        disableEnforceFocus
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Bind Hard Token</h1>
            <div style={{ display: "flex" }}>
              <div style={{ width: "50%", paddingRight: 10 }}>
                <div style={{ marginBottom: 24, position: "relative" }}>
                  <p style={{ marginBottom: 2 }}>Username :</p>
                  <TextField
                    disabled={true}
                    value={data.username}
                    placeholder="Username"
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div style={{ marginBottom: 24, position: "relative" }}>
                  <p style={{ marginBottom: 2 }}>Phone Number :</p>
                  <TextField
                    disabled={true}
                    value={data.phoneNumber}
                    placeholder="Phone Number"
                    style={{ width: 300, height: 40 }}
                  />
                </div>
              </div>
              <div style={{ width: "50%", paddingLeft: 10 }}>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Name :</p>
                  <TextField
                    value={data.nameAlias}
                    disabled={true}
                    placeholder="Full Name"
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div style={{ marginBottom: 24, position: "relative" }}>
                  <p style={{ marginBottom: 2 }}>Hard Token :</p>
                  <TextField
                    value={hardTokenInput}
                    onChange={(e) => setHardTokenInput(e.target.value)}
                    placeholder="Hard Token"
                    style={{ width: 300, height: 40 }}
                  />
                </div>
              </div>
            </div>
            <div className={classes.button}>
              <ButtonOutlined
                label="Cancel"
                width={157.5}
                height="40px"
                color="#0061A7"
                onClick={() => handleClose()}
              />

              <GeneralButton
                label="Save"
                width={157.5}
                height="40px"
                disabled={!hardTokenInput}
                onClick={() => {
                  handleClose();
                  setShowModalConfirmation(true);
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>

      <DeletePopup
        isOpen={showModalConfirmation}
        handleClose={() => {
          setShowModalConfirmation(false);
        }}
        onContinue={handleNewUser}
        title="Confirmation"
        message="Are You Sure to Save Data?"
        submessage=""
      />

      <SuccessConfirmation
        isOpen={showModalSuccess}
        title="Success"
        message="Data has been saved."
        handleClose={() => {
          router.push("/hard-token");
        }}
      />
    </div>
  );
};

EditHardToken.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
};
EditHardToken.defaultProps = { onContinue: () => {} };
export default EditHardToken;

/* --------------------------------- STYLES --------------------------------- */
const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 696,
    minHeight: 420,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 40,
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 110,
  },
}));
