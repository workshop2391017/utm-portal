// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import ScrollCustom from "components/BN/ScrollCustom";
import GeneralButton from "../../Button/GeneralButton";

// assets
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 495,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 36,
    fontWeight: 900,
    color: Colors.dark.hard,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
}));

const PesanFaq = ({
  isOpen,
  handleClose,
  title,
  message,

  height,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  // useEffect(() => {
  //   if (isOpen) {
  //     setTimeout(() => {
  //       handleClose();
  //     }, 2500);
  //   }
  // }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 900,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper} style={{ height }}>
            <div className={classes.xContainer}>
              <XIcon className="close" onClick={handleClose} />
            </div>
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}

              <ScrollCustom height={254}>
                <Typography
                  style={{
                    fontSize: 13,
                    marginTop: 20,
                    fontFamily: "Futura Bk BT",
                    fontWeight: 400,
                    color: Colors.dark.hard,
                    textAlign: "justify",
                  }}
                >
                  {message}
                </Typography>
              </ScrollCustom>

              <GeneralButton
                label="OK"
                width="380px"
                height="40px"
                onClick={handleClose}
                style={{
                  marginTop: 45,
                  fontFamily: "FuturaMdBT",
                  fontSize: 15,
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PesanFaq.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,

  height: PropTypes.string,
};

PesanFaq.defaultProps = {
  title: "Berhasil",
  message:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem faucibus odio massa pellentesque tellus augue enim. Mattis id iaculis ullamcorper erat sit. Fames elementum leo vitae habitant quam posuere potenti purus lectus. Pellentesque leo convallis sed mattis. Aliquam aliquam morbi vel elit cursus. Convallis aliquam id turpis vitae id felis feugiat laoreet amet. Libero, varius aliquet arcu nisi. Lacus volutpat, id mi tincidunt suscipit nullam egestas penatibus velit. In accumsan, aliquet enim felis diam scelerisque tellus. Mauris ornare scelerisque curabitur gravida enim quis molestie nulla arcu. Aliquam, vitae convallis cras habitant tellus. Etiam pellentesque pellentesque lectus cursus nunc.In ipsum elit aliquet interdum orci tincidunt. Velit phasellus sed interdum augue in egestas. Magna cras in commodo facilisis. Nulla egestas quam sit id risus sapien pharetra tristique. Cras massa elit.",
  height: 495,
};

export default PesanFaq;
