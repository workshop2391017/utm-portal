// main
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  CircularProgress,
} from "@material-ui/core";
import { useSelector } from "react-redux";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 477,
    height: 350,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 40,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
}));

const AddComment = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  inputRef,
  comment = null,
  loading,
  value,
  formLabel,
}) => {
  const classes = useStyles();

  // state
  const [requestComment, setRequestComment] = useState(null);

  const { isLoadingSubmit } = useSelector(({ approvalConfig }) => ({
    ...approvalConfig,
  }));

  useEffect(() => {
    setRequestComment(value);
  }, [value]);

  const closeModal = () => {
    setRequestComment(null);
    handleClose();
  };

  const acceptComment = async () => {
    await onContinue(true, requestComment);
    setRequestComment(null);
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={closeModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>

            <div style={{ marginLeft: 10 }}>
              <p style={{ marginBottom: 2 }}>{formLabel}</p>
              <TextField
                value={requestComment}
                ref={inputRef}
                onChange={(e) => setRequestComment(e.target.value)}
                placeholder="Comment"
                style={{ height: 92, width: 397 }}
                type="textArea"
              />
              <p style={{ color: "#BCC8E7" }}>Optional</p>
              <div className={classes.button} style={{ left: 30 }}>
                <ButtonOutlined
                  label="Cancel"
                  width={157.5}
                  height="40px"
                  color="#0061A7"
                  onClick={closeModal}
                  disabled={loading}
                  style={{ fontFamily: "FuturaMdBT", fontSize: 15 }}
                />
              </div>
            </div>

            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                loading={isLoadingSubmit}
                label={
                  loading ? (
                    <CircularProgress size={20} color="primary" />
                  ) : (
                    "Save"
                  )
                }
                width={157.5}
                height="40px"
                onClick={() => acceptComment()}
                disabled={loading}
                style={{ fontFamily: "FuturaMdBT", fontSize: 15 }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

AddComment.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
  loading: PropTypes.bool,
  formLabel: PropTypes.string,
};

AddComment.defaultProps = {
  title: "Comment Change",
  formLabel: "Add Comment",
  onContinue: () => {},
  loading: false,
};

export default AddComment;
