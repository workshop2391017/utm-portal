// main
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import Colors from "helpers/colors";

// redux
import {
  getGroupByBranchId,
  handleGetAllBranch,
  handlegetAllRole,
  setUserData,
} from "stores/actions/managementuser";
import { userDropdownRole } from "containers/BN/BisnisPengaturan/UserSetting";

// components
import Select from "../../Select/SelectGroup";
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 696,
    minHeight: 420,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 40,
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 110,
  },
}));

const EditBillerKategori = ({ isOpen, handleClose, onContinue }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { userData, roleData, branchData, dataGroupBranchId } = useSelector(
    (data) => ({
      userData: data.managementuser?.userData,
      roleData: data.managementuser?.role,
      branchData: data.managementuser?.branch,
      dataGroupBranchId: data.managementuser?.dataGroupBranchId,
    })
  );

  const offices = (branchData?.olistBranch ?? []).map((elm) => ({
    id: elm?.branchCode,
    name: elm?.branchName,
  }));

  const [username, setUsername] = useState(null);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [nip, setNip] = useState(null);
  const [branch, setBranch] = useState(null);
  const [role, setRole] = useState(null);
  const [group, setGroup] = useState(null);
  const [officeType, setOfficetype] = useState(null);

  useEffect(() => {
    if (Object.values(userData).filter((n) => n).length > 0) {
      setUsername(userData?.username);
      setName(userData?.name);
      setEmail(userData?.email);
      setNip(userData?.nip);
      setBranch(userData?.branchCode);
      setRole(userData?.roleId);
      setOfficetype(
        (branchData?.olistBranch ?? []).find(
          (a) => a?.branchCode === userData?.branchCode
        )?.officeType
      );
    }
  }, [userData]);

  useEffect(() => {
    if (officeType) dispatch(getGroupByBranchId({ officeType }));
  }, [officeType]);

  const handleChangeOffice = (e) => {
    const fi = offices.find((fe) => fe.id === e);

    setOfficetype(fi?.officeType);
    setBranch(fi?.id);
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Edit User</h1>
            <div style={{ display: "flex" }}>
              <div style={{ width: "50%", paddingRight: 10 }}>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2, color: Colors.gray.medium }}>
                    Username :
                  </p>
                  <TextField
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="Username"
                    style={{ width: 300, height: 40 }}
                    disabled
                  />
                </div>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2, color: Colors.gray.medium }}>
                    Email :
                  </p>
                  <TextField
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="xyz@example.com"
                    style={{ width: 300, height: 40 }}
                    disabled
                  />
                </div>
                <div className={classes.input} style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 3 }}>Office :</p>
                  <Select
                    value={
                      (branchData?.olistBranch ?? []).find(
                        (e) => e?.branchCode === `${branch}`
                      )?.branchName
                    }
                    onChange={handleChangeOffice}
                    placeholder="Select Office"
                    options={[
                      {
                        name: "Select Office",
                        options: offices,
                      },
                    ]}
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div className={classes.input}>
                  <p style={{ marginBottom: 3 }}>Role :</p>
                  <Select
                    value={
                      (roleData?.olistRole ?? []).find(
                        (f) => f?.roleId === role
                      )?.roleName
                    }
                    onChange={(value) => setRole(value)}
                    placeholder="Select Role"
                    options={[
                      {
                        name: "Select Role",
                        options: (roleData?.olistRole ?? []).map((elm) => ({
                          name: elm?.roleName
                            ?.replaceAll("_", " ")
                            .toLowerCase(),
                          id: elm?.roleId,
                        })),
                      },
                    ]}
                    style={{ width: 300, height: 40 }}
                  />
                </div>
              </div>
              <div style={{ width: "50%", paddingLeft: 10 }}>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2, color: Colors.gray.medium }}>
                    Nama :
                  </p>
                  <TextField
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    placeholder="Full Name"
                    style={{ width: 300, height: 40 }}
                    disabled
                  />
                </div>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2, color: Colors.gray.medium }}>
                    NIP :
                  </p>
                  <TextField
                    value={nip}
                    onChange={(e) => setNip(e.target.value)}
                    placeholder="192019928812"
                    style={{ width: 300, height: 40 }}
                    disabled
                  />
                </div>
                <div className={classes.input}>
                  <p style={{ marginBottom: 3 }}>Group :</p>
                  <Select
                    value={
                      (dataGroupBranchId?.portalGroupList ?? []).find(
                        (a) => a?.roleId === `${group}`
                      )?.name
                    }
                    onChange={(value) => setGroup(value)}
                    placeholder="Select Group"
                    options={[
                      {
                        name: "Select Group",
                        options: (dataGroupBranchId?.portalGroupList ?? []).map(
                          (elm) => ({
                            name: elm?.name,
                            id: elm?.id,
                          })
                        ),
                      },
                    ]}
                    style={{ width: 300, height: 40 }}
                  />
                </div>
              </div>
            </div>
            <div className={classes.button}>
              <ButtonOutlined
                label="Cancel"
                width={157.5}
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />

              <GeneralButton
                label="Save"
                width={157.5}
                height="40px"
                onClick={() => {
                  dispatch(
                    setUserData({
                      username,
                      name,
                      email,
                      nip,
                      branchCode: `${branch}`,
                      role,
                    })
                  );
                  onContinue(true);
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditBillerKategori.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
};

EditBillerKategori.defaultProps = { onContinue: () => {} };

export default EditBillerKategori;
