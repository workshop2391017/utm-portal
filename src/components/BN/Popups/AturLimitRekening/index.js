// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";

import Grid from "@material-ui/core/Grid";

import TextField from "../../TextField/AntdTextField";

// import Button from "@material-ui/core/Button";

import ButtonOutlined from "../../Button/ButtonOutlined";
import GeneralButton from "../../Button/GeneralButton";

import DeletePopup from "../Delete";

import SuccessConfirm from "../BerhasilRekeningKelompok";
// assets

import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 674,
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const AturLimitRekening = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();

  const [modalLimit, setModalLimit] = useState(false);

  const [successConfirm, setSuccessConfirm] = useState(false);
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  const handleCloseModal = () => {
    handleClose();
    // setCheckList([]);
  };

  const handleOpenConfirm = () => {
    handleCloseModal();
    setSuccessConfirm(true);
  };

  return (
    <div>
      <SuccessConfirm
        isOpen={successConfirm}
        handleClose={() => setSuccessConfirm(false)}
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 2500,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid item xs={12}>
              <Typography
                style={{
                  color: "#374062",
                  fontSize: "32px",
                  fontWeight: "400",
                  fontFamily: "FuturaHvBt",
                  textAlign: "center",
                }}
              >
                Atur Limit Rekening
              </Typography>
              <div style={{ display: "flex", marginTop: 35, marginBottom: 15 }}>
                <div
                  style={{
                    color: "#374062",
                    fontSize: "17px",
                    fontWeight: "400",
                    fontFamily: "FuturaMdBt",
                    textAlign: "left",
                  }}
                >
                  Transfer :
                </div>
              </div>
              <div style={{ display: "flex", marginBottom: 15 }}>
                <div
                  style={{
                    fontSize: "15px",
                    textAlign: "left",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Limit Transaksi :
                </div>
              </div>
              <div style={{ display: "flex", marginTop: -40, marginLeft: 310 }}>
                <div
                  style={{
                    fontSize: "15px",
                    // textAlign: "right",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Rp 100.000.
                </div>
              </div>
              <div style={{ display: "flex", marginTop: 5, marginBottom: 5 }}>
                <div
                  style={{
                    fontSize: "13px",
                    textAlign: "left",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Masukan Limit :
                </div>
              </div>
              <div style={{ display: "flex" }}>
                <TextField
                  //   value={inputJudul}
                  //   onChange={(e) => setInputJudul(e.target.value)}
                  placeholder="Rp 200.000.000"
                  style={{
                    width: "400px",
                    height: "40px",
                    borderRadius: "10px",
                    border: "1px solid #BCC8E7",
                    fontSize: "15px",
                    // marginTop: -5,
                  }}
                />
              </div>
              <div style={{ display: "flex", marginTop: 15, marginBottom: 15 }}>
                <div
                  style={{
                    color: "#374062",
                    fontSize: "17px",
                    fontWeight: "400",
                    fontFamily: "FuturaMdBt",
                    textAlign: "left",
                  }}
                >
                  Pembayaran :
                </div>
              </div>
              <div style={{ display: "flex", marginBottom: 15 }}>
                <div
                  style={{
                    fontSize: "15px",
                    textAlign: "left",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Limit Transaksi :
                </div>
              </div>
              <div style={{ display: "flex", marginTop: -40, marginLeft: 310 }}>
                <div
                  style={{
                    fontSize: "15px",
                    // textAlign: "right",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Rp 100.000.
                </div>
              </div>
              <div style={{ display: "flex", marginTop: 5, marginBottom: 5 }}>
                <div
                  style={{
                    fontSize: "13px",
                    textAlign: "left",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Masukan Limit :
                </div>
              </div>
              <div style={{ display: "flex" }}>
                <TextField
                  //   value={inputJudul}
                  //   onChange={(e) => setInputJudul(e.target.value)}
                  placeholder="Rp 200.000.000"
                  style={{
                    width: "400px",
                    height: "40px",
                    borderRadius: "10px",
                    border: "1px solid #BCC8E7",
                    fontSize: "15px",
                    // marginTop: -5,
                  }}
                />
              </div>
              <div style={{ display: "flex", marginTop: 15, marginBottom: 15 }}>
                <div
                  style={{
                    color: "#374062",
                    fontSize: "17px",
                    fontWeight: "400",
                    fontFamily: "FuturaMdBt",
                    textAlign: "left",
                  }}
                >
                  Pembelian :
                </div>
              </div>
              <div style={{ display: "flex", marginBottom: 15 }}>
                <div
                  style={{
                    fontSize: "15px",
                    textAlign: "left",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Limit Transaksi :
                </div>
              </div>
              <div style={{ display: "flex", marginTop: -40, marginLeft: 310 }}>
                <div
                  style={{
                    fontSize: "15px",
                    // textAlign: "right",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Rp 100.000.
                </div>
              </div>
              <div style={{ display: "flex", marginTop: 5, marginBottom: 5 }}>
                <div
                  style={{
                    fontSize: "13px",
                    textAlign: "left",
                    fontWeight: "400",
                    fontFamily: "FuturaBkBt",
                  }}
                >
                  Masukan Limit :
                </div>
              </div>
              <div style={{ display: "flex" }}>
                <TextField
                  //   value={inputJudul}
                  //   onChange={(e) => setInputJudul(e.target.value)}
                  placeholder="Rp 200.000.000"
                  style={{
                    width: "400px",
                    height: "40px",
                    borderRadius: "10px",
                    border: "1px solid #BCC8E7",
                    fontSize: "15px",
                    // marginTop: -5,
                  }}
                />
              </div>
              <div>
                <ButtonOutlined
                  label="Batal"
                  // iconPosition="startIcon"
                  // buttonIcon={<Plus />}
                  width="92px"
                  height="43px"
                  style={{
                    position: "absolute",
                    marginTop: 40,
                    left: 30,
                  }}
                  // onClick={() => setModal(true)}
                />
                <GeneralButton
                  label="Simpan"
                  // iconPosition="startIcon"
                  // buttonIcon={<Plus />}
                  width="92px"
                  height="43px"
                  style={{
                    position: "absolute",
                    marginTop: 40,
                    // marginRight: 100,
                    // position: "relative",
                    // position: 'absolute',
                    // bottom: '8px',
                    right: "30px",
                    // marginLeft: 30
                  }}
                  onClick={handleOpenConfirm}
                />
              </div>
            </Grid>

            <DeletePopup
              isOpen={modalLimit}
              handleClose={() => setModalLimit(false)}
              // onContinue={handleBerhasil}
              title="Konfirmasi"
              message="Anda Yakin Menyetujui Data?"
            />
            {/* <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}

              <img src={check} alt="check" style={{ marginTop: 25 }} />
              <Typography
                style={{
                  // color: '#374062',
                  fontSize: "18px",
                  fontWeight: "bold",
                }}
              >
                {message}
              </Typography>{" "}
              <br />
              <Typography style={{ lineHeight: 0 }}>
              Atur Limit Rekening
              </Typography>
              <br />
              <br />
              <GeneralButton
                label="OK"
                width="370px"
                height="40px"
                onClick={handleClose}

              />
            </div> */}
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

AturLimitRekening.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

AturLimitRekening.defaultProps = {
  title: "Transfer",
  message: "Atur Limit Rekening",
};

export default AturLimitRekening;
