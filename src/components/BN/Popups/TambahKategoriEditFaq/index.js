// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ButtonOutlined from "../../Button/ButtonOutlined";
// components
import GeneralButton from "../../Button/GeneralButton";
import RadioGroup from "../../Radio/RadioGroup";
import TextField from "../../TextField/AntdTextField";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 502,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: "normal",
    marginBottom: 68,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    marginTop: 71,
  },
}));

const TambahKategoriEditFaq = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  setOpenModalSucces,
  rowData,
}) => {
  console.warn("handleClose:", handleClose);
  const classes = useStyles();
  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState(isOpen);

  const { editGeneralData } = useSelector(({ generalParam }) => ({
    editGeneralData: generalParam?.editGeneralData,
    isLoadingSubmit: generalParam?.isLoadingSubmit,
  }));

  const [namaModule, setNamaModule] = useState(null);
  const [kode, setKode] = useState(null);
  const [value, setValue] = useState(null);
  const [deskripsi, setDeskripsi] = useState(null);
  const [enkripsiValue, setEnkripsiValue] = useState("Ya");

  useEffect(() => {
    if (editGeneralData) {
      setNamaModule(editGeneralData.module);
      setKode(editGeneralData.name);
      setValue(editGeneralData.value);
      setDeskripsi(editGeneralData.description);
      setEnkripsiValue(editGeneralData.encrypt ? "Ya" : "Tidak");
    }
  }, [editGeneralData]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>

            <div style={{ marginBottom: 10 }}>
              <p style={{ marginBottom: 2 }}>Nama Kategori :</p>
              <TextField
                value=""
                onChange={(e) => console.warn(e)}
                placeholder="Masukan nama kategori"
                style={{ width: "100%" }}
              />
            </div>
            <div style={{ marginBottom: 24 }}>
              <p style={{ marginBottom: 2 }}>Pilih Kategori :</p>
              <SelectWithSearch
                value="admin"
                options={[
                  {
                    label: "admin",
                    value: "admin",
                  },
                  {
                    label: "cs",
                    value: "cs",
                  },
                ]}
                onChange={(e) => console.warn(e)}
                placeholder="admin"
                style={{ width: "100%" }}
              />
            </div>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={() => {
                  console.warn("testing saja");

                  setOpenModalSucces();

                  console.warn("selesai");

                  // onContinue({
                  //   idParameter: editGeneralData?.id,
                  //   namaModule,
                  //   kode,
                  //   value,
                  //   deskripsi,
                  //   enkripsiValue,
                  // });
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

TambahKategoriEditFaq.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
};

TambahKategoriEditFaq.defaultProps = {
  onContinue: () => {},
  title: "Tambah Kategori",
};

export default TambahKategoriEditFaq;
