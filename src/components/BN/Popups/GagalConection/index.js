// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import GeneralButton from "../../Button/GeneralButton";

// assets
// import times from '../../../../assets/icons/BN/times-circle.svg';

import times from "../../../../assets/icons/BN/wifiGagal.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 420,
    minHeight: 470,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 30,
  },
}));

const GagalConection = ({ isOpen, handleClose, title }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => {
        handleClose();
      }, 10000);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              {/* {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))} */}
              <img
                src={times}
                alt="Times"
                style={{
                  marginTop: 10,
                  marginBottom: 20,
                  width: "160px",
                  height: "160px",
                }}
              />{" "}
              <br />
              <Typography
                style={{
                  color: "rgba(55, 64, 98, 1)",
                  fontSize: "15px",
                  fontWeight: "normal",
                  fontFamily: "Futura Bk BT",
                  marginBottom: "65px",
                }}
              >
                Sistem sedang offline. Mohon coba kembali dalam beberapa saat.
              </Typography>
              <GeneralButton
                // ini data user
                label="OK"
                width="370px"
                height="40px"
                onClick={handleClose}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

GagalConection.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
};

GagalConection.defaultProps = {
  title: "Data sudah Telah Ditolak",
};

export default GagalConection;
