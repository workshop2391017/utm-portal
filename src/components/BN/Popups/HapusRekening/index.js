// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";
import { Checkbox, Typography } from "antd";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
// import SuccessConfirmation from '../SuccessConfirmation';
import SuccessHapusRekening from "../SuccesHapusRekening";

import check from "../../../../assets/images/BN/illustrationyellow.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    // paddingTop: 10,
  },
  paper: {
    width: 480,
    minHeight: 524,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
}));

const HapusRekening = ({
  isOpen,
  handleClose,
  onContinue,
  label,
  message,
  title,
}) => {
  const classes = useStyles();
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [successHapusRekening, setSuccessHapusRekening] = useState(false);
  const [checkList, setCheckList] = useState([]);
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  const handleCheckbox = (item) => {
    if (!checkList.includes(item)) {
      setCheckList([...checkList, item]);
    } else {
      const arr = checkList.filter((n) => n !== item);
      setCheckList(arr);
    }
  };

  const handleCloseModal = () => {
    handleClose();
    setCheckList([]);
  };

  const handleOpenConfirm = () => {
    handleCloseModal();
    setSuccessHapusRekening(true);
  };

  return (
    <div>
      <SuccessHapusRekening
        isOpen={successHapusRekening}
        handleClose={() => setSuccessHapusRekening(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            {/* <h1 className={classes.title}>{label}</h1> */}
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}
              {/* <SvgIcon component={check} alt='check' style={{marginTop: 30}} /> */}
              <img src={check} alt="check" style={{ marginTop: 25 }} />
              <Typography
                style={{
                  // color: '#374062',
                  fontSize: "24px",
                  fontWeight: "400",
                  fontFamily: "FuturaMdBt",
                }}
              >
                {message}
              </Typography>{" "}
              <br />
              <Typography style={{ lineHeight: 0 }}>
                Anda tidak dapat membatalkan tindakan ini.
              </Typography>
              <br />
              <br />
              {/* <GeneralButton
                label="OK"
                width="370px"
                height="40px"
                onClick={handleClose}
                // paddingTop= '30'
              /> */}
            </div>
            <div className={classes.button} style={{ left: 40 }}>
              <ButtonOutlined
                label="Batal"
                width="158px"
                height="40px"
                color="#0061A7"
                onClick={handleCloseModal}
              />
            </div>
            <div className={classes.button} style={{ right: 40 }}>
              <GeneralButton
                label="Hapus"
                width="158px"
                height="40px"
                onClick={handleOpenConfirm}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

HapusRekening.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

HapusRekening.defaultProps = {
  title: "Hapus Rekening",
  message: "Apakah Anda yakin ingin \n  menghapus rekening ini ?",
};

export default HapusRekening;
