// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Backdrop,
  Fade,
  Box,
  Typography,
  Modal,
} from "@material-ui/core";
import { Statistic } from "antd";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";

// assets
import logo from "../../../../assets/icons/BN/session-timeout.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",

    "& ::selection": {
      background: "#137EE1",
    },
  },
  titleModal: {
    marginTop: 20,
    fontFamily: "FuturaHvBT",
    textAlign: "center",
    color: "#374062",
    fontWeight: "900px",
    fontSize: "20px",
    lineHeight: "23.97px",
  },
  paper: {
    width: 480,
    height: 544,
    backgroundColor: "white",
    border: "1px solid #BCC8E7",
    padding: "30px 26px",
    alignItems: "center",
    borderRadius: 20,
  },
  logo: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 53.5,
  },
  timer: {
    display: "inline-block",
    "& .ant-statistic-content": {
      fontSize: 12,
      color: "#374062",
    },
  },
}));

// eslint-disable-next-line react/prop-types
const PopupUpSessionTime = ({ isOpen, onCancel, onContinue, title }) => {
  const classes = useStyles();
  const [timerValue, setTimerValue] = useState(0);
  const [isStartTimer, setIsStartTimer] = useState(false);
  const [isStillLogin, setIsStillLogin] = useState(true);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => setTimerValue(Date.now() + 60000), 1000);
      setIsStartTimer(true);
    } else {
      setTimerValue(0);
    }
  }, [isOpen]);

  const continueAction = () => {
    if (isStillLogin) {
      onContinue();
      setIsStartTimer(false);
    } else {
      window.location.href = `${process.env.PUBLIC_URL}/`;
    }
  };

  useEffect(() => {
    if (isStartTimer && timerValue === 0) {
      setIsStillLogin(false);
    }
  }, [timerValue]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <img src={logo} alt="logo" className={classes.logo} />
            <div>
              <Typography className={classes.titleModal}>
                Session Timeout
              </Typography>
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <div style={{ width: 376 }}>
                <Box
                  display="flex"
                  justifyContent="flex-start"
                  mt={3}
                  style={{ marginTop: 10 }}
                >
                  <Box>
                    <Typography
                      style={{
                        color: "#374062",
                        fontSize: "15px",
                        textAlign: "center",
                        lineHeight: "16px",
                        marginBottom: 20,
                      }}
                    >
                      Your time for this session is over, do you want to
                      continue this session?
                    </Typography>
                  </Box>
                </Box>
                <Box>
                  <Typography
                    style={{
                      color: "#374062",
                      fontSize: "15px",
                      textAlign: "center",
                      lineHeight: "16px",
                    }}
                  >
                    Please select within
                    <b>
                      <Statistic.Countdown
                        value={timerValue}
                        format="mm:ss"
                        className={classes.timer}
                      />
                    </b>{" "}
                    <b>seconds</b>, before the system automatically logout
                  </Typography>
                </Box>
              </div>
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <div style={{ width: 376 }}>
                <Box display="flex" justifyContent="flex-start" mt={10.2}>
                  <Box flexGrow={1}>
                    <ButtonOutlined
                      label="Cancel"
                      width={152.5}
                      height="44px"
                      color="#E31C23"
                      onClick={onCancel}
                    />
                  </Box>
                  <Box>
                    <GeneralButton
                      label="Next"
                      width={152.5}
                      height="44px"
                      variant="contained"
                      onClick={continueAction}
                      className={classes.Button}
                    />
                  </Box>
                </Box>
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupUpSessionTime.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onContinue: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default PopupUpSessionTime;
