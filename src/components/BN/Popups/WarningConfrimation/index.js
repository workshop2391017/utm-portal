/* eslint-disable import/order */
// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import GeneralButton from "../../Button/GeneralButton";

// assets
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import warning from "assets/images/BN/illustrationyellow.png";

const WarningConfirmation = ({
  isOpen,
  handleClose,
  title,
  message,
  submessage,
  height,
  img,
  closeModal,
}) => {
  const useStyles = makeStyles(() => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      width: 480,
      height: 544,
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      padding: closeModal ? 30 : 0,
      alignItems: "center",
      borderRadius: 20,
      position: "relative",
      textAlign: "center",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontSize: 36,
      fontWeight: 900,
      color: Colors.dark.hard,
    },
    xContainer: {
      display: "flex",
      justifyContent: "flex-end",
      "& .close": {
        "& :hover": {
          cursor: "pointer",
        },
      },
    },
  }));

  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  // useEffect(() => {
  //   if (isOpen) {
  //     setTimeout(() => {
  //       handleClose();
  //     }, 2500);
  //   }
  // }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 900,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper} style={{ height }}>
            {closeModal ? (
              <div className={classes.xContainer}>
                <XIcon className="close" onClick={handleClose} />
              </div>
            ) : null}
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}

              <img
                src={img || warning}
                alt="warning"
                style={{ marginTop: 43 }}
                width="160"
              />
              <Typography
                style={{
                  fontSize: 24,
                  marginTop: 20,
                  fontFamily: "FuturaMdBT",
                  color: Colors.dark.hard,
                }}
              >
                {message}
              </Typography>
              <div
                style={{
                  fontFamily: "FuturaBkBT",
                  fontSize: 15,
                  color: Colors.dark.hard,
                }}
              >
                {submessage}
              </div>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-between",
                  marginTop: 63,
                }}
              >
                <ButtonOutlined
                  label="Ya"
                  width="152.5px"
                  height="44px"
                  onClick={handleClose}
                />
                <GeneralButton
                  label="Tidak"
                  width="152.5px"
                  height="44px"
                  onClick={handleClose}
                />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

WarningConfirmation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string,
  height: PropTypes.string,
  img: PropTypes.node,
  closeModal: PropTypes.bool,
};

WarningConfirmation.defaultProps = {
  title: "Berhasil",
  message: "Aktivitas Berhasil Disetujui",
  submessage: "Silahkan Menunggu Persetujuan",
  height: 495,
  img: warning,
  closeModal: true,
};

export default WarningConfirmation;
