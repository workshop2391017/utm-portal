// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  createTheme,
  ThemeProvider,
  Grid,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Radio,
} from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import AddComment from "../AddComment";
import SuccessConfirmation from "../SuccessConfirmation";
import RadioGroup from "../../Radio/RadioGroup";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 590,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
}));

const EditSecurity = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [inputModul, setInputModul] = useState(null);
  const [inputCode, setInputCode] = useState(null);
  const [inputValue, setInputValue] = useState(null);
  const [inputDesc, setInputDesc] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [enkripsiValue, setEnkripsiValue] = useState("Ya");

  const handleOpenConfirm = () => {
    setAddComment(false);
    setConfirmSuccess(true);
  };

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };

  return (
    <div>
      <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(true)}
        onContinue={handleOpenConfirm}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={6}>
                <p style={{ marginBottom: 2 }}>Nama Modul :</p>
                <TextField
                  value={inputModul}
                  onChange={(e) => setInputModul(e.target.value)}
                  placeholder="Modul 1"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={6}>
                <p style={{ marginBottom: 2 }}>Kode :</p>
                <TextField
                  value={inputCode}
                  onChange={(e) => setInputCode(e.target.value)}
                  placeholder="1231"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={6}>
                <p style={{ marginBottom: 2 }}>Value :</p>
                <TextField
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Tambahkan Value"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={6}>
                <p style={{ marginBottom: 2 }}>Deskripsi :</p>
                <TextField
                  value={inputDesc}
                  onChange={(e) => setInputDesc(e.target.value)}
                  placeholder="Lorem ipsum dolor sit amet"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={6}>
                <p style={{ marginBottom: 15, PaddingLeft: 10 }}>
                  Enkripsi Value :
                </p>
                <RadioGroup
                  value={enkripsiValue}
                  onChange={(e) => setEnkripsiValue(e.target.value)}
                  options={["Ya", "Tidak"]}
                />
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={handleOpenComment}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditSecurity.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditSecurity.defaultProps = {
  onContinue: () => {},
};

export default EditSecurity;

const roleOptions = [
  {
    value: 1,
    label: "Ya",
  },
  {
    value: 2,
    label: "Tidak",
  },
];
