// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import GeneralDatePicker from "components/BN/DatePicker/GeneralDatePicker";
import Gap from "components/BN/Gap";
import InputImage from "components/BN/Inputs/InputImage";

import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  addDataHandlingOfficer,
  editDataHandlingOfficer,
} from "stores/actions/handlingOfficer";
import ButtonOutlined from "../../Button/ButtonOutlined";
// components
import GeneralButton from "../../Button/GeneralButton";
import RadioGroup from "../../Radio/RadioGroup";
import TextField from "../../TextField/AntdTextField";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    opacity: 0.19,
  },
  paper: {
    marginTop: "10%",
    width: 480,
    height: "353px",

    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  subtitle: {
    fontFamily: "FuturaBkBT",
    fontSize: 17,
    color: "#374062",
    marginBottom: 25,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: "normal",
    marginBottom: 24.28,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    marginTop: 71,
  },
  card: {
    display: "flex",
    marginBottom: "20px",
  },
  titleCard: {
    fontSize: "13px",
    fontWeight: 700,
    fontFamily: "Futura Md BT",
    color: "#374062",
    marginBottom: "10px",
  },
  subTitleCard: {
    color: "#374062",
    fontSize: "15px",
    fontFamily: "Futura Md BT",
  },
  cardContainer: {
    width: 750 / 2,
  },
}));

const EditCompanyCharge = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  rowData,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [openModal, setOpenModal] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState(false);

  const [formData, setFormData] = useState({
    email: "",
    location: "",
    officerId: "",
    phoneNumber: "",
    username: "",
  });

  const onSaveData = () => {
    console.warn("saving data");
  };

  return (
    <div>
      <SuccessConfirmation
        isOpen={successConfirmModal}
        handleClose={() => setSuccessConfirmModal(false)}
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>

            <div
              style={{
                width: "100%",
              }}
            >
              <span>Upload Dokumen :</span>
              <InputImage
                width="400px"
                onChange={() => console.warn("testing")}
                placeholder="Upload Dokumen"
              />
            </div>
            <div
              style={{
                marginTop: "10px",
              }}
            >
              <span>Expired Date :</span>

              <div
                style={{
                  display: "flex",
                }}
              >
                <div>
                  <GeneralDatePicker
                    placeholder="02/12/2021"
                    onChange={() => console.warn("testing")}
                    style={{
                      marginRight: "10px",
                    }}
                  />
                </div>

                <GeneralDatePicker placeholder="02/12/2021" />
              </div>
            </div>

            <Gap height="37px" />
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="92px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="76px"
                height="40px"
                onClick={() => console.warn("testing")}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditCompanyCharge.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
};

EditCompanyCharge.defaultProps = {
  onContinue: () => {},
  title: "Edit Company Charge",
};

export default EditCompanyCharge;
