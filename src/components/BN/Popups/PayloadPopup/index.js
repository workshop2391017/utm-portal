// main
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  CircularProgress,
  Typography,
} from "@material-ui/core";
import { useSelector } from "react-redux";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";

import { ReactComponent as CloseIcon } from "assets/icons/BN/close-blue.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 500,
    height: 500,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "15px 20px",
    display: "flex",
    flexDirection: "column",
    borderRadius: 8,
    gap: "5px",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
  },
  closeButtonContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
  },
  titleContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    marginBottom: "10px",
  },
  payloadContainer: {
    width: "100%",
    height: "100%",
    overflowX: "hidden",
    overflowY: "auto",
    border: "1px solid #DAE1ED",
    borderRadius: "10px",
    padding: "10px",
    marginBottom: "20px",
  },
  payloadChildContainer: {
    width: "100%",
  },
  value: {
    wordWrap: "break-word", // Make text wrap when it exceeds container width
  },
  buttonContainer: {
    width: "100%",
  },
}));

const PayloadPopup = ({
  isOpen,
  handleClose,
  title,
  inputRef,
  loading,
  value,
  formLabel,
}) => {
  const classes = useStyles();

  // state

  const closeModal = () => {
    handleClose();
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={closeModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.closeButtonContainer}>
              <div style={{ cursor: "pointer" }} onClick={handleClose}>
                <CloseIcon />
              </div>
            </div>

            <div className={classes.titleContainer}>
              <Typography className={classes.title}>{title}</Typography>
            </div>
            <div className={classes.payloadContainer}>
              <div className={classes.payloadChildContainer}>
                <Typography className={classes.value}>{value}</Typography>
              </div>
            </div>

            <div className={classes.buttonContainer}>
              <GeneralButton
                onClick={handleClose}
                label={"Close"}
                style={{ width: "100%" }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PayloadPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  loading: PropTypes.bool,
  formLabel: PropTypes.string,
};

PayloadPopup.defaultProps = {
  title: "Comment Change",
  formLabel: "Add Comment",
  loading: false,
};

export default PayloadPopup;
