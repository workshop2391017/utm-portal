// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import Switch from "../../Switch/GeneralSwitch";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 400,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
}));

const EditBillerKategori = ({ isOpen, handleClose, onContinue }) => {
  const classes = useStyles();
  const [kodeTran, setKodeTran] = useState(null);
  const [kode, setKode] = useState(null);
  const [categoryEn, setCategoryEn] = useState(null);
  const [categoryId, setCategoryId] = useState(null);
  const [checked, setChecked] = useState(false);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Ubah Biller</h1>
            <div style={{ display: "flex" }}>
              <div style={{ width: "50%", paddingRight: 10 }}>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Kode Tran :</p>
                  <TextField
                    value={kodeTran}
                    onChange={(e) => setKodeTran(e.target.value)}
                    placeholder="200192812"
                    style={{ width: "100%" }}
                  />
                </div>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Kategori Name (Inggris) :</p>
                  <TextField
                    value={categoryEn}
                    onChange={(e) => setCategoryEn(e.target.value)}
                    placeholder="Kategori"
                    style={{ width: "100%" }}
                  />
                </div>
                <div style={{ marginBottom: 24 }}>
                  <span style={{ fontFamily: "FuturaMdBT" }}>
                    Tambahkan Kode Prefix ?
                  </span>
                  <span style={{ float: "right" }}>
                    <Switch
                      checked={checked}
                      onChange={(e) => setChecked(e.target.checked)}
                    />
                  </span>
                </div>
              </div>
              <div style={{ width: "50%", paddingLeft: 10 }}>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Kode :</p>
                  <TextField
                    value={kode}
                    onChange={(e) => setKode(e.target.value)}
                    placeholder="BILLPAY_DONATION"
                    style={{ width: "100%" }}
                  />
                </div>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Kategori Name (Indonesia) :</p>
                  <TextField
                    value={categoryId}
                    onChange={(e) => setCategoryId(e.target.value)}
                    placeholder="Kategori"
                    style={{ width: "100%" }}
                  />
                </div>
              </div>
            </div>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={onContinue}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditBillerKategori.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
};

EditBillerKategori.defaultProps = { onContinue: () => {} };

export default EditBillerKategori;
