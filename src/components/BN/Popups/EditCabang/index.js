// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import SuccessConfirmation from "../SuccessConfirmation";
import AddComment from "../AddComment";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 600,
    minHeight: 490,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
}));

const EditCabang = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [kodeCabang, setKodeCabang] = useState(null);
  const [namaCabang, setNamaCabang] = useState(null);
  const [kota, setKota] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [addComment, setAddComment] = useState(false);

  const handleOpenConfirm = () => {
    setAddComment(false);
    setConfirmSuccess(true);
  };

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };
  return (
    <div>
      <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={4}>
                <p style={{ marginBottom: 2 }}>Kode Cabang :</p>
                <TextField
                  value={kodeCabang}
                  onChange={(e) => setKodeCabang(e.target.value)}
                  placeholder="Cth. 011"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Nama Cabang :</p>
                <TextField
                  value={namaCabang}
                  onChange={(e) => setNamaCabang(e.target.value)}
                  placeholder="Cth. KC Senayan"
                  style={{ width: "100%" }}
                />
                <p style={{ marginBottom: 2 }}>Alphanumeric</p>
              </Grid>
              <Grid item xs={12}>
                <p style={{ marginBottom: 2 }}>Kota :</p>
                <TextField
                  value={kota}
                  onChange={(e) => setKota(e.target.value)}
                  placeholder="Cth. Jakarta"
                  style={{ width: "100%" }}
                />
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={handleOpenComment}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditCabang.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditCabang.defaultProps = { onContinue: () => {} };

export default EditCabang;

const roleOptions = [
  {
    value: 1,
    label: "Ya",
  },
  {
    value: 2,
    label: "Tidak",
  },
];
