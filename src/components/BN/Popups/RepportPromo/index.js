// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";

import Grid from "@material-ui/core/Grid";

import TextField from "../../TextField/AntdTextField";

// import Button from "@material-ui/core/Button";

import ButtonOutlined from "../../Button/ButtonOutlined";
import GeneralButton from "../../Button/GeneralButton";
import Search from "../../Search/SearchWithoutDropdown";
import DeletePopup from "../Delete";

import SuccessConfirm from "../BerhasilRekeningKelompok";
// assets

import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";
import frametag from "../../../../assets/icons/BN/frametag.svg";
import frameeye from "../../../../assets/icons/BN/frameeye.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    fontFamily: "FuturaBkBt",
    fontWeight: "400",
    border: "1px solid #BCC8E7",
    borderRadius: "10px",
    height: 76,
    fontSize: 13,
    width: "400px",
    marginBottom: 15,
    textAlign: "left",
  },
  paper: {
    width: 480,
    height: 408,
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const RepportPromo = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();

  const [modalLimit, setModalLimit] = useState(false);

  const [successConfirm, setSuccessConfirm] = useState(false);
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  // useEffect(() => {
  //   if (isOpen) {
  //     // setTimeout(() => {
  //     //   handleClose();
  //     // }, 2500);
  //   }
  // }, [handleClose, isOpen]);

  const handleCloseModal = () => {
    handleClose();
    // setCheckList([]);
  };

  const handleOpenConfirm = () => {
    handleCloseModal();
    setSuccessConfirm(true);
  };

  return (
    <div>
      <SuccessConfirm
        isOpen={successConfirm}
        handleClose={() => setSuccessConfirm(false)}
        message="Report Promo Ok"
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 2500,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <Grid item xs={12}>
              <Typography
                style={{
                  color: "#374062",
                  fontSize: "36px",
                  fontWeight: "400",
                  fontFamily: "FuturaMdBt",
                  textAlign: "center",
                  marginBottom: "30px",
                }}
              >
                Report Promo
              </Typography>
              <div>
                <div
                  style={{
                    fontFamily: "FuturaBkBt",
                    fontWeight: "400",
                    border: "1px solid #BCC8E7",
                    borderRadius: "10px",
                    height: 76,
                    fontSize: 13,
                    width: "400px",
                    marginBottom: 15,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    marginLeft: "15px",
                    // marginTop: 15,
                    background: "#ffff",
                    boxSizing: "border-box",
                    flex: "none",
                    order: "0",
                    flexGrow: "0",
                    cursor: "pointer",
                  }}
                  // onClick={() => setModal(true)}
                >
                  <img
                    src={frameeye}
                    style={{
                      display: "flex",
                      width: "50px",
                      height: "50px",
                      alignContent: "left",
                      marginTop: "10px",
                      marginRight: "auto",
                      marginLeft: 10,
                    }}
                    alt="NPWP"
                  />

                  <p
                    style={{
                      fontFamily: "FuturaHvBt",
                      fontWeight: "400",
                      color: "#374062",
                      fontSize: "13",
                      marginLeft: "70px",
                      marginRight: "auto",
                      marginTop: "-45px",
                      textAlign: "left",
                    }}
                  >
                    Promo Di Lihat
                  </p>
                  <p
                    style={{
                      fontFamily: "FuturaBkBt",
                      fontWeight: "400",
                      color: "#7B87AF",
                      fontSize: "13",
                      marginLeft: "70px",
                      marginRight: "auto",
                      // marginTop: "-50px",
                    }}
                  >
                    13.000.345 Kali
                  </p>
                </div>
                <div
                  style={{
                    fontFamily: "FuturaBkBt",
                    fontWeight: "400",
                    border: "1px solid #BCC8E7",
                    borderRadius: "10px",
                    height: 76,
                    fontSize: 13,
                    width: "400px",
                    marginBottom: 15,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    marginLeft: "15px",
                    marginTop: 15,
                    background: "#ffff",
                    boxSizing: "border-box",
                    flex: "none",
                    order: "0",
                    flexGrow: "0",
                    cursor: "pointer",
                  }}
                  // onClick={() => setModal(true)}
                >
                  <img
                    src={frametag}
                    style={{
                      display: "flex",
                      width: "50px",
                      height: "50px",
                      alignContent: "left",
                      marginTop: "10px",
                      marginRight: "auto",
                      marginLeft: 10,
                    }}
                    alt="NPWP"
                  />

                  <p
                    style={{
                      fontFamily: "FuturaHvBt",
                      fontWeight: "400",
                      color: "#374062",
                      fontSize: "13",
                      marginLeft: "70px",
                      marginRight: "auto",
                      marginTop: "-45px",
                      textAlign: "left",
                    }}
                  >
                    Promo Di Buka
                  </p>
                  <p
                    style={{
                      fontFamily: "FuturaBkBt",
                      fontWeight: "400",
                      color: "#7B87AF",
                      fontSize: "13",
                      marginLeft: "70px",
                      marginRight: "auto",
                      // marginTop: "-50px",
                    }}
                  >
                    13.000.345 Kali
                  </p>
                </div>
                <div>
                  <GeneralButton
                    label="OK"
                    width="380px"
                    height="40px"
                    onClick={handleClose}
                    style={{
                      marginTop: 45,
                      fontFamily: "FuturaMdBT",
                      fontSize: 15,
                      marginBottom: "10px",
                      position: "relative",
                      bottom: "15px",
                    }}
                  />
                </div>
              </div>
            </Grid>

            <DeletePopup
              isOpen={modalLimit}
              handleClose={() => setModalLimit(false)}
              // onContinue={handleBerhasil}
              title="Konfirmasi"
              message="Anda Yakin Menyetujui Data?"
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

RepportPromo.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

RepportPromo.defaultProps = {
  title: "Comment",
  message: "Repport Promo New",
};

export default RepportPromo;
