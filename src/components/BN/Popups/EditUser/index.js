// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

// libraries

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import AddComment from "../AddComment";
import SuccessConfirmation from "../SuccessConfirmation";
import Select from "../../Select/AntdSelect";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 590,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
  error: {
    fontSize: 13,
    color: "#FF6F6F",
    position: "absolute",
    left: 8,
    transition: "0.3s ease-in-out",
  },
}));

const EditUser = ({
  isOpen,
  handleClose,
  onContinue,
  label,
  rowData,
  tableData,
}) => {
  const classes = useStyles();

  const [roleList, setRoleList] = useState([
    "Admin Blokir",
    "Staff Approver",
    "Staff Input",
    "Staff",
    "Staff Admin",
    "Buka Blokir",
  ]);
  const [branchList, setBranchList] = useState([
    "Kantor Pusat",
    "Wirosaban",
    "Banguntapan",
  ]);

  const [inputName, setInputName] = useState(null);
  const [inputNIP, setInputNIP] = useState(null);
  const [inputEmail, setInputEmail] = useState(null);
  const [inputUser, setInputUser] = useState(null);
  const [addComment, setAddComment] = useState(false);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [branch, setBranch] = useState("Kantor Pusat");
  const [role, setRole] = useState("Admin Blokir");
  const [title, setTitle] = useState("");
  const [error, setError] = useState(false);
  const [commentInput, setCommentInput] = useState(null);
  // console.log('LOOK NIP', inputNIP);
  // console.log('LOOK USER', inputUser);
  // console.log('LOOK COMMENT', commentInput);

  const handleOpenConfirm = () => {
    // addEditUser({
    //   idUser: rowData.idUser,
    //   name: inputName,
    //   username: inputUser,
    //   phoneNumber: null,
    //   email: inputEmail,
    //   role: role,
    //   branch: branch,
    //   password: null,
    //   requestComment: commentInput
    // }).then(res => console.log('LOOK RES', res))
    //   .catch(err => alert(err));
    setAddComment(false);
    setConfirmSuccess(true);
  };

  const handleOpenComment = (username) => {
    if (tableData.map((item) => item.user).includes(username)) {
      setError(true);
    } else {
      handleClose();
      setAddComment(true);
      setTimeout(() => {
        setError(false);
        setInputUser(null);
      }, 500);
    }
  };

  const handleCloseModal = () => {
    handleClose();
    setTimeout(() => {
      setError(false);
      setInputUser(null);
    }, 500);
  };

  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    label.toLowerCase().includes("ubah")
      ? setTitle("Rejection Comment")
      : setTitle("Komentar Penambahan");
  }, [label]);

  useEffect(() => {
    if (rowData !== undefined && rowData !== null) {
      setInputName(rowData.name);
      setInputNIP(rowData.nip);
      setInputEmail(rowData.email);
      setInputUser(rowData.username);
      setRole(
        roleList.find(
          (item) => item.toLowerCase() === rowData.role.toLowerCase()
        ) ?? roleList[0]
      );
      setBranch(
        branchList.find(
          (item) => item.toLowerCase() === rowData.branch.toLowerCase()
        ) ?? branchList[0]
      );
    }
  }, [rowData]);

  return (
    <div>
      <AddComment
        title={title}
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
        comment={commentInput}
        setComment={setCommentInput}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Nama :</p>
                <TextField
                  value={inputName}
                  onChange={(e) => setInputName(e.target.value)}
                  placeholder="Nama"
                  style={{ width: "100%" }}
                  disabled
                />
              </Grid>
              <Grid item xs={4}>
                <p style={{ marginBottom: 2 }}>NIP :</p>
                <TextField
                  value={inputNIP}
                  disabled
                  onChange={(e) => setInputNIP(e.target.value)}
                  placeholder="NIP"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Email :</p>
                <TextField
                  disabled
                  value={inputEmail}
                  onChange={(e) => setInputEmail(e.target.value)}
                  placeholder="Email"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4} style={{ position: "relative" }}>
                <p style={{ marginBottom: 2 }}>Username :</p>
                <TextField
                  disabled={rowData && rowData.user !== ""}
                  value={inputUser}
                  onChange={(e) => setInputUser(e.target.value)}
                  placeholder="Username"
                  style={{ width: "100%", position: "relative", zIndex: 1 }}
                />
                <div
                  className={classes.error}
                  style={{
                    bottom: error ? -10 : 10,
                    opacity: error ? 1 : 0,
                  }}
                >
                  Username telah ditambahkan
                </div>
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Cabang :</p>
                <Select
                  value={branch}
                  onChange={(value) => setBranch(value)}
                  placeholder="Cabang"
                  options={branchList}
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <p style={{ marginBottom: 2 }}>Peran :</p>
                <Select
                  value={role}
                  onChange={(value) => setRole(value)}
                  placeholder="Peran"
                  options={roleList}
                  style={{ width: "100%" }}
                />
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleCloseModal}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={() => handleOpenComment(inputUser)}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditUser.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditUser.defaultProps = {
  onContinue: () => {},
};

export default EditUser;
