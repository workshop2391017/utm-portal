// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  createTheme,
  ThemeProvider,
  CircularProgress,
} from "@material-ui/core";

// assets
// import trash from '../../../../assets/icons/BN/trash-alt.svg';
import logo from "../../../../assets/images/BN/illustration.png";

import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 420,
    minHeight: 450,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
}));

const buttonRedTheme = createTheme({
  palette: {
    primary: {
      // light: 'yellow',
      main: "#0061A7",
      // dark: 'green',
      contrastText: "#fff",
    },
  },
});

const DeleteConfirmation = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  loading,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}
              <img src={logo} alt="Trash" style={{ marginTop: 25 }} />
              <Typography
                style={{
                  // color: '#374062',
                  fontSize: "18px",
                  fontWeight: "bold",
                }}
              >
                Berhasil Menghapus Data
              </Typography>
              <p>Silahkan menunggu persetujuan</p>
              <div className={classes.button} style={{ right: 30 }}>
                <GeneralButton
                  label={loading ? <CircularProgress size={20} /> : "Tidak"}
                  width="150px"
                  height="40px"
                  onClick={handleClose}
                  disabled={!!loading}
                />
              </div>
              <div className={classes.button} style={{ left: 30 }}>
                <ThemeProvider theme={buttonRedTheme}>
                  <ButtonOutlined
                    label="Ya"
                    width="150px"
                    height="40px"
                    color="#0061A7"
                    onClick={onContinue}
                    disabled={!!loading}
                  />
                </ThemeProvider>
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

DeleteConfirmation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
};

DeleteConfirmation.defaultProps = {
  title: "Berhasil",
};

export default DeleteConfirmation;
