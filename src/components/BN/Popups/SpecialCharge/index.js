import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import moment from "moment";

import { blobToBase64 } from "utils/helpers/imageToBase64";
import Colors from "helpers/colors";

import DatePicker from "containers/BN/Transaction-Maintanance/CompanyCharge/components/Date";

import ButtonOutlined from "components/BN/Button/ButtonOutlined";
import InputImage from "components/BN/Inputs/InputImage";
import FormField from "components/BN/Form/InputGroupValidation";

import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";

import { saveSpecialCharge } from "stores/actions/companyCharge";

import GeneralButton from "../../Button/GeneralButton";

import check from "../../../../assets/images/BN/illustration.png";

const SpecialCharge = ({
  isOpen,
  handleClose,
  title,
  height,
  closeModal,
  detail,
  setSpecialChargeImg = () => {},
}) => {
  const useStyles = makeStyles({
    modal: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    paper: {
      position: "relative",
      textAlign: "center",
      alignItems: "center",
      width: "auto",
      height: 495,
      borderRadius: 20,
      padding: closeModal ? 30 : 0,
      backgroundColor: "white",
      border: "1px solid #BCC8E7",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontSize: 36,
      fontWeight: 900,
      color: Colors.dark.hard,
    },
    xContainer: {
      display: "flex",
      justifyContent: "flex-end",
      "& .close": {
        "& :hover": {
          cursor: "pointer",
        },
      },
    },
  });

  const classes = useStyles();
  const dispatch = useDispatch();

  const dateNow = moment();
  const dates = dateNow.format("DD/MM/YYYY");

  const [titles, setTitles] = useState([]);
  const [validateFile, setValidateFile] = useState({});
  const [disabledSave, setDisabledSave] = useState({
    isFormatPdf: true,
    isMaksFile: false,
  });
  const [formData, setFormData] = useState({
    fileName: "",
    fileDocument: "",
    startEffectiveDate: "",
    endEffectiveDate: "",
    startTime: "",
    id: "",
  });
  const [errUpload, setErrorUpload] = useState(false);

  useEffect(() => {
    if (Object.values(detail).length > 0) {
      setFormData((prevData) => ({
        ...prevData,
        id: detail?.companyCharge !== null ? detail.companyCharge.id : null,
        fileName: detail?.document?.split("/")[3],
        fileDocument: detail?.document,
        startEffectiveDate: !detail?.startEffectiveDate
          ? dates
          : moment(detail?.startEffectiveDate).format("DD/MM/YYYY"),
        endEffectiveDate: !detail?.endEffectiveDate
          ? dates
          : moment(detail?.endEffectiveDate).format("DD/MM/YYYY"),
      }));
    }
  }, [detail]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  const uploadBase64 = async (file, type, fileType) => {
    const toBase64 = await blobToBase64(file);

    setFormData((prevData) => ({
      ...prevData,
      fileName: file.name,
      fileDocument: toBase64.split(",")[1],
    }));
  };

  const handleFile = (e) => {
    if (e?.target?.files?.length > 0) {
      const file = e?.target?.files[0];
      setSpecialChargeImg(file);
      setValidateFile(file);
      uploadBase64(file, file?.type?.split("/")[1].toUpperCase());
    }
  };

  useEffect(() => {
    if (
      validateFile?.type !== undefined &&
      validateFile?.type !== "application/pdf" &&
      formData.fileName?.split(".")[1] !== "pdf"
    ) {
      setDisabledSave({ isMaksFile: false, isFormatPdf: false });
      setErrorUpload(true);
    } else if (
      validateFile?.size > 1 * 1024 * 1024 &&
      formData.fileName !== undefined
    ) {
      setDisabledSave({ isFormatPdf: true, isMaksFile: true });
      setErrorUpload(true);
    } else {
      setErrorUpload(false);
      setDisabledSave((prevData) => ({
        ...prevData,
        isMaksFile: false,
        isFormatPdf: true,
      }));
    }
  }, [validateFile, formData.fileName]);

  const handleLengthFileName = () => {
    const data = formData.fileName;
    if (data?.length > 45) {
      return `${(data ?? "").substring(0, 45)}...`;
    }
    return data;
  };

  return (
    <Modal
      className={classes.modal}
      open={isOpen}
      onClose={() => {
        handleClose();
        setDisabledSave((prevData) => ({
          ...prevData,
          isMaksFile: false,
          isFormatPdf: true,
        }));
        setFormData((prevData) => ({
          ...prevData,
          fileName: detail?.document?.split("/")[3],
          fileDocument: detail?.document,
          startEffectiveDate: !detail?.startEffectiveDate
            ? dates
            : moment(detail.startEffectiveDate).format("DD/MM/YYYY"),
          endEffectiveDate: !detail?.endEffectiveDate
            ? dates
            : moment(detail.endEffectiveDate).format("DD/MM/YYYY"),
        }));
      }}
      closeAfterTransition
      BackdropComponent={Backdrop}
    >
      <Fade in={isOpen}>
        <div className={classes.paper} style={{ height }}>
          {closeModal ? (
            <div className={classes.xContainer}>
              <XIcon
                className="close"
                onClick={() => {
                  handleClose();
                  setFormData((prevData) => ({
                    ...prevData,
                    fileName: detail?.document?.split("/")[3],
                    fileDocument: detail?.document,
                    startEffectiveDate: !detail?.startEffectiveDate
                      ? dates
                      : moment(detail.startEffectiveDate).format("DD/MM/YYYY"),
                    endEffectiveDate: !detail?.endEffectiveDate
                      ? dates
                      : moment(detail.endEffectiveDate).format("DD/MM/YYYY"),
                    // moment(new Date(detail?.endEffectiveDate)) ===
                    //   moment(new Date(dates)) ||
                    // moment(new Date(detail?.endEffectiveDate)) <=
                    //   moment(new Date(dates))
                    //   ? moment(new Date(dates)).format("DD/MM/YYYY")
                    //   : moment(new Date(detail?.endEffectiveDate)).format(
                    //       "DD/MM/YYYY"
                    //     ),
                  }));
                  setDisabledSave((prevData) => ({
                    ...prevData,
                    isMaksFile: false,
                    isFormatPdf: true,
                  }));
                }}
              />
            </div>
          ) : null}
          <div className={classes.content}>
            {titles.map((item) => (
              <p className={classes.title}>{item}</p>
            ))}

            <div
              style={{
                fontFamily: "FuturaBkBT",
                fontSize: 15,
                color: Colors.dark.hard,
                display: "flex",
                justifyContent: "space-around",
                flexDirection: "column",
                alignItems: "flex-start",
                width: "100%",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  marginTop: "24px",
                }}
              >
                <span>Upload Documents : </span>
                <FormField
                  error={!disabledSave?.isFormatPdf || disabledSave?.isMaksFile}
                >
                  <InputImage
                    width="400px"
                    onChange={handleFile}
                    id="file"
                    placeholder="Upload Documents"
                    value={
                      formData.fileName !== null ? handleLengthFileName() : null
                    }
                    accept="application/pdf"
                  />
                </FormField>
                {!disabledSave?.isFormatPdf || disabledSave?.isMaksFile ? (
                  <span style={{ color: "#D14848", marginRight: "2px" }}>
                    PDF file format,maximum file size 1 MB
                  </span>
                ) : null}
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-start",
                  marginTop: "28px",
                  justifyContent: "space-between",
                  width: "399px",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                  }}
                >
                  <span>Starting from : </span>
                  <DatePicker
                    width={195}
                    format="DD/MM/YYYY"
                    placeholder={dates}
                    value={moment(formData.startEffectiveDate, "DD/MM/YYYY")}
                    onChange={(date) => {
                      setFormData((prevData) => ({
                        ...prevData,
                        startEffectiveDate: date
                          ? moment(date).format("DD/MM/YYYY")
                          : null,
                      }));
                    }}
                    disabledDate={(current) => moment() >= current}
                    // disabledDate={(current) =>
                    //   current < moment(formData.startEffectiveDate) ||
                    //   current > moment(dates).add(23, "hours")
                    // }
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                  }}
                >
                  <span>Ends on : </span>
                  <DatePicker
                    format="DD/MM/YYYY"
                    placeholder={dates}
                    width={195}
                    value={moment(formData.endEffectiveDate, "DD/MM/YYYY")}
                    onChange={(date) => {
                      setFormData((prevData) => ({
                        ...prevData,
                        endEffectiveDate: date
                          ? moment(date).format("DD/MM/YYYY")
                          : null,
                      }));
                    }}
                    disabledDate={(current) =>
                      formData?.startEffectiveDate
                        ? current <
                          moment(formData?.startEffectiveDate, "DD/MM/YYYY")
                        : current < moment(dates, "DD/MM/YYYY")
                    }
                    // disabledDate={(current) =>
                    //   moment(new Date(detail?.endEffectiveDate)) ===
                    //     moment(new Date(dates)) ||
                    //   moment(new Date(detail?.endEffectiveDate)) <=
                    //     moment(new Date(dates))
                    //     ? current < moment(dates)
                    //     : current < moment(formData.startEffectiveDate)
                    // }
                  />
                </div>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
                marginTop: 50,
              }}
            >
              <ButtonOutlined
                label="Cancel"
                width="82px"
                onClick={() => {
                  handleClose();
                  setDisabledSave((prevData) => ({
                    ...prevData,
                    isMaksFile: false,
                    isFormatPdf: true,
                  }));
                  setFormData((prevData) => ({
                    ...prevData,
                    fileName: detail?.document?.split("/")[3],
                    fileDocument: detail?.document,
                    startEffectiveDate: !detail?.startEffectiveDate
                      ? dates
                      : moment(detail.startEffectiveDate).format("DD/MM/YYYY"),
                    endEffectiveDate: !detail?.endEffectiveDate
                      ? dates
                      : moment(detail.endEffectiveDate).format("DD/MM/YYYY"),
                    // detail?.startEffectiveDate !== "" &&
                    // detail?.startEffectiveDate !== null &&
                    // detail?.startEffectiveDate === dates
                    //   ? moment(dates).add(1, "days")
                    //   : moment(new Date(dates)).format("DD/MM/YYYY"),
                  }));
                }}
              />

              <GeneralButton
                label="Save"
                disabled={
                  formData.fileName === null ||
                  formData.fileDocument === null ||
                  formData.startEffectiveDate === null ||
                  formData.endEffectiveDate === null ||
                  formData.startEffectiveDate === "Invalid date" ||
                  formData.endEffectiveDate === "Invalid date" ||
                  moment(formData.endEffectiveDate, "DD/MM/YYYY").isBefore(
                    moment(formData.startEffectiveDate, "DD/MM/YYYY")
                  ) ||
                  errUpload
                }
                width="100px"
                height="40px"
                onClick={() => {
                  dispatch(
                    saveSpecialCharge({
                      corporateProfileId: detail?.corporateProfileId,
                      endEffectiveDate: moment(
                        formData.endEffectiveDate,
                        "DD/MM/YYYY"
                      )
                        .format("YYYY-MM-DD")
                        .concat(" 23:59:59"),
                      starEffectiveDate: moment(
                        formData.startEffectiveDate,
                        "DD/MM/YYYY"
                      )
                        .format("YYYY-MM-DD")
                        .concat(" 00:00:00"),
                      file: formData?.fileDocument,
                      id: formData?.id,
                    })
                  );
                  handleClose();
                }}
                style={{
                  fontFamily: "FuturaMdBT",
                  fontSize: 15,
                }}
              />
            </div>
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

SpecialCharge.defaultProps = {
  title: "Special Charge",
  message: "Aktivitas Berhasil Disetujui",
  submessage: "Silahkan Menunggu Persetujuan",
  height: 433,
  img: check,
  closeModal: true,
};

export default SpecialCharge;
