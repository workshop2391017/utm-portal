// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";
import { Checkbox } from "antd";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import SuccessConfirmation from "../SuccessConfirmation";
import AddComment from "../AddComment";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 430,
    minHeight: 200,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 15,
    color: "#374062",
    "& .ant-checkbox": {
      width: 20,
      height: 20,
      borderRadius: 3,
      "& .ant-checkbox-inner": {
        width: 20,
        height: 20,
        borderRadius: 3,
        border: "1px solid #BCC8E7",
        "&::after": {
          width: 6.5,
          height: 11.17,
          top: "42%",
        },
      },
    },
    "& .ant-checkbox-checked": {
      "& .ant-checkbox-inner": {
        border: "1px solid #0061A7 !important",
      },
    },
  },
}));

const BlastAppNew = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [androidVersion, setAndroidVersion] = useState("");
  const [iosVersion, setIosVersion] = useState("");
  const [inputPesan, setInputPesan] = useState(
    "Silahkan perbarui aplikasi Bank anda ke versi terbaru untuk mendapatkan pengalaman terbaik."
  );
  const [addComment, setAddComment] = useState(false);

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };
  const handleOpenConfirm = () => {
    setAddComment(false);
    setConfirmSuccess(true);
  };

  return (
    <div>
      <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 5 }}>
              <Grid item xs={3}>
                <p style={{ marginBottom: 2 }}>Versi Lama </p>
              </Grid>
              <Grid item xs={1}>
                <p style={{ marginBottom: 2 }}>:</p>
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>
                  Android 1.12.345 & IOS 1.12.345
                </p>
              </Grid>
            </Grid>
            <Grid container spacing={2} style={{ marginBottom: 5 }}>
              <Grid item xs={3}>
                <p style={{ marginBottom: 2 }}>Pesan </p>
              </Grid>
              <Grid item xs={1}>
                <p style={{ marginBottom: 2 }}>:</p>
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>
                  Silahkan perbarui aplikasi Bank anda ke versi terbaru untuk
                  mendapatkan pengalaman terbaik.
                </p>
              </Grid>
            </Grid>
            <Grid container spacing={2} style={{ marginBottom: 20 }}>
              <div
                style={{
                  borderBottom: "1px solid #AEB3C6",
                  marginTop: "12px",
                  width: "100%",
                }}
              />
            </Grid>
            <Grid container spacing={2} style={{ marginBottom: 10 }}>
              <Grid item xs={4}>
                <Checkbox className={classes.check}>IOS</Checkbox>
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Versi Baru :</p>
                <TextField
                  value={iosVersion}
                  onChange={(e) => setIosVersion(e.target.value)}
                  placeholder="1231"
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4}>
                <Checkbox className={classes.check}>Android</Checkbox>
              </Grid>
              <Grid item xs={8}>
                <p style={{ marginBottom: 2 }}>Versi Baru :</p>
                <TextField
                  value={androidVersion}
                  onChange={(e) => setAndroidVersion(e.target.value)}
                  placeholder="1231"
                  style={{ width: "100%" }}
                />
              </Grid>
            </Grid>
            <Grid item xs={12} style={{ marginBottom: 50 }}>
              <p style={{ marginBottom: 2 }}>Pesan:</p>
              <TextField
                value={inputPesan}
                type="textArea"
                onChange={(e) => setInputPesan(e.target.value)}
                placeholder="Super Admin"
                style={{ width: "100%", height: "80px", marginBottom: "20px" }}
              />
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Jadwalkan"
                width="92px"
                height="40px"
                onClick={handleOpenComment}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

BlastAppNew.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

BlastAppNew.defaultProps = { onContinue: () => {} };

export default BlastAppNew;
