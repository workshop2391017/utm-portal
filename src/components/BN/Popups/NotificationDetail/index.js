/* eslint-disable react/destructuring-assignment */
// main
import React, { useState, useEffect, useContext } from "react";
import { makeStyles, Backdrop, Fade, Modal } from "@material-ui/core";
import PropTypes from "prop-types";
import moment from "moment";
import { RootContext } from "../../../../router";

// components
import GeneralButton from "../../Button/GeneralButton";
import TableScroll from "../../Table/TableScroll";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& ::selection": {
      background: "#137EE1",
    },
  },
  paper: {
    width: "610px",
    height: "auto",
    backgroundColor: "white",
    border: "1px solid #BCC8E7",
    padding: "30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    minWidth: 796,
  },
  title: {
    fontFamily: "Futura",
    fontSize: 20,
  },
}));

const tableConfig = [
  {
    title: "Transaksi ID",
    headerAlign: "left",
    align: "left",
    render: (rowData) => <span>{rowData.transactionId}</span>,
  },
  {
    title: "Tgl & Waktu",
    headerAlign: "left",
    align: "left",
    render: (rowData) => (
      <span>
        {moment(rowData.transactionDate).format("DD-MM-YYYY | HH:mm:ss")}
      </span>
    ),
  },
  {
    title: "Aktivitas",
    headerAlign: "left",
    align: "left",
    render: (rowData) => {
      if (rowData.activity.toLowerCase().includes("transfer")) {
        if (rowData.activity.toLowerCase().includes("oa")) {
          return <span>PINDAH DANA</span>;
        }
        if (rowData.activity.toLowerCase().includes("on")) {
          return <span>TRANSFER SESAMA Bank</span>;
        }
        if (rowData.activity.toLowerCase().includes("off")) {
          return <span>TRANSFER BANK LAIN</span>;
        }
        return <span>TRANSFER</span>;
      }
      return <span>{rowData.activity.split("_").join(" ")}</span>;
    },
  },
  {
    title: "Nasabah ID",
    headerAlign: "left",
    align: "left",
    render: (rowData) => <span>{rowData.userProfileId}</span>,
  },
  {
    title: "Keterangan",
    headerAlign: "left",
    align: "left",
    render: (rowData) => <span>{rowData.transactionDescription}</span>,
  },
];

/* --------------------------------- START --------------------------------- */
const NotificationDetail = (props) => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const { userLogin } = useContext(RootContext);

  useEffect(() => {
    if (props.isRowSelected) {
      setLoading(true);
    }
  }, [props.isRowSelected, props.selectedRow]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={props.isOpen}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={props.isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Detail Notifikasi</h1>
            <div style={{ margin: "30px 0 20px" }}>
              <TableScroll
                cols={tableConfig}
                data={data}
                maxHeight={358}
                loading={loading}
              />
            </div>
            <div>
              <GeneralButton
                label="Tutup"
                width="77px"
                height="40px"
                variant="contained"
                onClick={props.handleClose}
                style={{ float: "right" }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

NotificationDetail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  isRowSelected: PropTypes.bool,
  selectedRow: PropTypes.object,
  // title: PropTypes.string.isRequired,
};

NotificationDetail.defaultProps = {
  selectedRow: {},
  isRowSelected: false,
};

export default NotificationDetail;
