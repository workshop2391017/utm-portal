import React, { useState } from "react";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Box, CircularProgress, Typography, Grid } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import moment from "moment";
import Toast from "components/BN/Toats";
import { setHandleClearError } from "stores/actions/dashboard/activityDetail";
import { useDispatch } from "react-redux";

import "./index.css";
import ScrollCustom from "components/BN/ScrollCustom";
import GeneralButton from "../../Button/GeneralButton";
import Badge from "../../Badge";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",

    "& ::selection": {
      background: "#137EE1",
    },
  },
  buttonCancel: {
    color: "#E31C23",
    fontFamily: "FuturaMdBT",
    fontSize: "13px",
    textTransform: "none",
    borderRadius: "6px",
  },
  button: {
    fontFamily: "FuturaMdBT",
    backgroundColor: "#374062",
  },
  titleModal: {
    fontFamily: "FuturaBQ",
    color: "#374062",
    fontWeight: 700,
    fontSize: "20px",
    lineHeight: "23.97px",
  },
  close: {
    textAlign: "right",
    cursor: "pointer",
  },
  paper: {
    width: "680px",
    height: "auto",
    backgroundColor: "white",
    border: "1px solid #BCC8E7",
    padding: "30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  subtitle: {
    width: 100,
    height: 16,
  },
  subtitle1: {
    fontFamily: "FuturaBQ",
  },
  logo: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    top: "5.56%",
  },
  status: {
    display: "flex",
    justifyContent: "flex-end",
    width: "100%",
    height: 23,
    fontFamily: "FuturaBQ",
  },
  tbody: {
    fontFamily: "FuturaMdBT",
    "& tr:hover": {
      backgroundColor: "#F4F7FB",
    },
  },
  grid: {
    fontFamily: "FuturaMdBT",
    fontSize: "12px",
    color: "#374062",
    backgroundColor: "#fff",
    // marginTop: 10,
    // "&::-webkit-scrollbar": {
    //   display: "none",
    // },
  },
}));

// eslint-disable-next-line react/prop-types
const PopupActivityDetail = ({
  activityDetail,
  isOpen,
  handleClose,
  type = "logAktivitas",
  title,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <div>
      <Toast
        open={activityDetail?.error?.isError}
        message={activityDetail?.error?.message ?? null}
        handleClose={() => dispatch(setHandleClearError())}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            {activityDetail?.detailActivity?.status === 0 ||
            activityDetail?.detailActivity?.status === 1 ? (
              <div className={classes.status}>
                <Badge
                  label={
                    activityDetail?.detailActivity?.status === 0
                      ? "Transaction"
                      : activityDetail?.detailActivity?.status === 1
                      ? "Error"
                      : "Anomaly"
                  }
                  type={
                    activityDetail?.detailActivity?.status === 0
                      ? "green"
                      : activityDetail?.detailActivity?.status === 1
                      ? "red"
                      : "orange"
                  }
                  styleBadge={{ marginRight: "5px" }}
                />
              </div>
            ) : null}
            <div>
              <Typography className={classes.titleModal}>
                Detail Activity
              </Typography>
            </div>

            {activityDetail?.isLoading ? (
              <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
                <div style={{ margin: "auto" }}>
                  <CircularProgress color="primary" size={40} />
                </div>
              </div>
            ) : (
              <div>
                <Box display="flex" justifyContent="flex-start" mt={3}>
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Name :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        height: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.userProfileName ?? "-"}
                    </Typography>
                  </Box>
                  {/* <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Tipe Nasabah :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        height: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.userProfileType ?? "-"}
                    </Typography>
                  </Box> */}
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Customer ID :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        height: 16,
                      }}
                    >
                      {/* 210001929218 */}
                      {activityDetail?.detailActivity?.userProfileId ?? "-"}
                    </Typography>
                  </Box>
                </Box>
                <Box display="flex" justifyContent="flex-start" mt={3}>
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Transaction ID :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.transactionId ?? "-"}
                    </Typography>
                  </Box>
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Ref Number :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",

                        minHeight: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.refNumber ?? "-"}
                    </Typography>
                  </Box>
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Channel :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.channel === "IB"
                        ? "Web"
                        : activityDetail?.detailActivity?.channel === "MB"
                        ? "Mobile"
                        : "-"}
                    </Typography>
                  </Box>
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Activity :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {/* Transfer Other */}
                      {activityDetail?.detailActivity?.activityName ?? "-"}
                    </Typography>
                  </Box>
                </Box>
                <Box display="flex" justifyContent="flex-start" mt={3}>
                  {/* <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Device Unique Id :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.deviceUniqueId ?? "-"}
                    </Typography>
                  </Box> */}
                  {/* <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      OS :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.os ?? "-"}
                    </Typography>
                  </Box> */}
                  <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      IP Address :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {/* 21.0199.1821 */}
                      {activityDetail?.detailActivity?.ipAddress ?? "-"}
                    </Typography>
                  </Box>
                  {/* <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 100,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Phone Type :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 100,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                        overflowWrap: "break-word",
                      }}
                    >
                      {activityDetail?.detailActivity?.phoneType ?? "-"}
                    </Typography>
                  </Box> */}
                  {/* <Box
                    style={{
                      marginRight: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        height: 16,
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Phone Brand :
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      className={classes.subtitle1}
                      style={{
                        color: "#374062",
                        fontSize: "13px",
                        lineHeight: "16px",
                        width: 120,
                        fontWeight: 900,
                        fontFamily: "FuturaHvBT",
                        minHeight: 16,
                      }}
                    >
                      {activityDetail?.detailActivity?.phoneBrand ?? "-"}
                    </Typography>
                  </Box> */}
                </Box>
                {type === "logAktivitas" ? (
                  <div>
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#AEB3C6",
                        fontSize: "12px",
                        textAlign: "left",
                        lineHeight: "12px",
                        marginTop: "22px",
                        fontWeight: "Bold",
                        fontFamily: "FuturaBQ",
                        paddingBottom: 4,
                        borderBottom: "1px solid #AEB3C6",
                        marginBottom: 24,
                      }}
                    >
                      Log Activity
                    </Typography>
                    <ScrollCustom height="260px">
                      <div
                        style={{
                          borderLeft: "2px solid #E6EAF3",
                          paddingLeft: 24,
                          marginLeft: 10,
                        }}
                      >
                        <Grid container spacing={3} className={classes.grid}>
                          {(activityDetail?.logActivityDtoList ?? []).map(
                            (elm, key) => (
                              <React.Fragment key={key}>
                                <Grid item sm={5}>
                                  {elm?.activityName ?? "-"}
                                </Grid>
                                <Grid item sm={4}>
                                  {moment(elm?.activityDate).format(
                                    "DD-MM-YYYY | HH:mm:ss"
                                  ) ?? "-"}
                                </Grid>
                                <Grid item sm={3}>
                                  {elm?.resultCode ?? "-"}
                                </Grid>
                              </React.Fragment>
                            )
                          )}
                        </Grid>
                      </div>
                    </ScrollCustom>
                    <div
                      style={{
                        borderBottom: "1px solid #BCC8E7",
                        marginTop: 27,
                      }}
                    />
                    {/* </ScrollCustom> */}
                  </div>
                ) : (
                  <Box>
                    {/* SEPERTINYA TIDAK DIPAKE */}
                    <Typography
                      variant="subtitle2"
                      style={{
                        color: "#AEB3C6",
                        fontSize: "12px",
                        textAlign: "left",
                        lineHeight: "12px",
                        marginTop: "22px",
                        fontWeight: "Bold",
                        fontFamily: "FuturaBQ",
                      }}
                    >
                      Detail Transaksi
                    </Typography>
                    <hr
                      style={{
                        border: "1px solid #AEB3C6",
                        width: "560px",
                        marginBottom: 1,
                      }}
                    />
                    <Box
                      style={{
                        border: "1px solid #BCC8E7",
                        boxSizing: "border-box",
                        width: "557px",
                        height: "273px",
                      }}
                    >
                      <table
                        className={classes.td}
                        style={{
                          borderCollapse: "collapse",
                          borderSpacing: "10px 10px",
                          width: "100%",
                        }}
                      >
                        <tbody style={{}}>
                          <tr style={{ width: "525px", height: "26px" }}>
                            <td style={{ paddingLeft: "20px" }}>SoF</td>
                            <td style={{ paddingLeft: "310px" }}>
                              70219299121
                            </td>
                          </tr>
                          <tr
                            style={{
                              width: "525px",
                              height: "26px",
                              marginTop: "15px",
                            }}
                          >
                            <td style={{ paddingLeft: "20px" }}>DOF</td>
                            <td style={{ paddingLeft: "310px" }}>
                              10019019821
                            </td>
                          </tr>
                          <tr
                            style={{
                              width: "525px",
                              height: "26px",
                              marginTop: "15px",
                            }}
                          >
                            <td style={{ paddingLeft: "20px" }}>
                              Nama Penerima Dana
                            </td>
                            <td style={{ paddingLeft: "292px" }}>
                              Beneficiary Name
                            </td>
                          </tr>
                          <tr
                            style={{
                              width: "525px",
                              height: "26px",
                              marginTop: "15px",
                            }}
                          >
                            <td style={{ paddingLeft: "20px" }}>
                              Bank Penerima Dana
                            </td>
                            <td style={{ paddingLeft: "375px" }}>BRI</td>
                          </tr>
                          <tr
                            style={{
                              width: "525px",
                              height: "26px",
                              marginTop: "15px",
                            }}
                          >
                            <td style={{ paddingLeft: "20px" }}>Fee</td>
                            <td style={{ paddingLeft: "320px" }}>
                              Rp 6.500,00
                            </td>
                          </tr>
                          <tr
                            style={{
                              width: "525px",
                              height: "26px",
                              marginTop: "15px",
                            }}
                          >
                            <td style={{ paddingLeft: "20px" }}>Amount</td>
                            <td style={{ paddingLeft: "295px" }}>
                              Rp 1.000.000,00
                            </td>
                          </tr>
                          <tr
                            style={{
                              width: "525px",
                              height: "26px",
                              marginTop: "15px",
                            }}
                          >
                            <td style={{ paddingLeft: "20px" }}>Total</td>
                            <td style={{ paddingLeft: "295px" }}>
                              Rp 1.006.500,00
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </Box>
                  </Box>
                )}
              </div>
            )}

            <Box display="flex" justifyContent="flex-end" mt={3}>
              <Box>
                <GeneralButton
                  label="Close"
                  width="77px"
                  height="40px"
                  variant="contained"
                  onClick={handleClose}
                />
              </Box>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupActivityDetail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default PopupActivityDetail;
