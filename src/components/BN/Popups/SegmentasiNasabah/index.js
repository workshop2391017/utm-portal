// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";

import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import ButtonOutlined from "../../Button/ButtonOutlined";
import Search from "../../Search/SearchWithoutDropdown";

// components
import GeneralButton from "../../Button/GeneralButton";
import RadioGroup from "../../Radio/RadioGroup";
import TextField from "../../TextField/AntdTextField";
// import CheckboxGroup from "../../Checkbox/ChexboxGroup";
import RadioOption from "../../Radio/RadioSingle";
// assets

import { ReactComponent as SearchIcon } from "../../../../assets/icons/BN/search.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 580,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  subtitle: {
    fontFamily: "FuturaBkBT",
    fontSize: 17,
    color: "#374062",
    marginBottom: 25,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: "normal",
    marginBottom: 24.28,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    marginTop: 71,
  },
  inputGroup: {
    width: "100%",
  },
  noResult: {
    fontSize: "17px",
    fontFamily: "FuturaMdBT",
    color: "#7B87AF",
    textAlign: "center",
    marginTop: "20px",
    padding: "0 50px",
  },
  listCompany: {
    display: "flex",
    marginTop: "16px",
    marginLeft: "15px",
  },
}));

const CustomerSegmentation = ({ isOpen, handleClose, onContinue, title }) => {
  const classes = useStyles();

  const { editGeneralData } = useSelector(({ generalParam }) => ({
    editGeneralData: generalParam?.editGeneralData,
    isLoadingSubmit: generalParam?.isLoadingSubmit,
  }));

  const [namaModule, setNamaModule] = useState(null);
  const [kode, setKode] = useState(null);
  const [value, setValue] = useState(null);
  const [deskripsi, setDeskripsi] = useState(null);
  const [enkripsiValue, setEnkripsiValue] = useState("Ya");

  useEffect(() => {
    if (editGeneralData) {
      setNamaModule(editGeneralData.module);
      setKode(editGeneralData.name);
      setValue(editGeneralData.value);
      setDeskripsi(editGeneralData.description);
      setEnkripsiValue(editGeneralData.encrypt ? "Ya" : "Tidak");
    }
  }, [editGeneralData]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>
            <div className={classes.inputGroup}>
              <div style={{ marginBottom: 2 }}>Cari Nama Perusahaan :</div>
              <TextField
                placeholder="pencarian..."
                style={{ width: "100%" }}
                // dataSearch={dataSearch}
                // setDataSearch={setDataSearch}
                suffix={
                  <SearchIcon style={{ width: "16px", height: "16px" }} />
                }
              />
            </div>
            <div>
              <div className={classes.noResult}>
                Tidak ada data ditampilkan. Mulai Pencarian
              </div>
              <div className={classes.listCompany}>
                <RadioOption />
                <div>PT. Sampoerna Tbk</div>
              </div>
              <div className={classes.button} style={{ left: 30 }}>
                <ButtonOutlined
                  label="Batal"
                  width="76px"
                  height="40px"
                  color="#0061A7"
                  onClick={handleClose}
                />
              </div>
              <div className={classes.button} style={{ right: 30 }}>
                <GeneralButton
                  label="Simpan"
                  width="92px"
                  height="40px"
                  onClick={() => {
                    onContinue({
                      idParameter: editGeneralData?.id,
                      namaModule,
                      kode,
                      value,
                      deskripsi,
                      enkripsiValue,
                    });
                  }}
                />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

CustomerSegmentation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
};

CustomerSegmentation.defaultProps = {
  onContinue: () => {},
  title: "Tambahkan Perusahaan",
};

export default CustomerSegmentation;
