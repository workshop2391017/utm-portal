import React, { useEffect, useState } from "react";

import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { handleClearError } from "stores/actions/errorGeneral";
import illustrationX from "assets/images/BN/illustrationred.png";
import { useHistory, useLocation } from "react-router-dom";

import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";

import {
  Backdrop,
  Fade,
  makeStyles,
  Modal,
  Typography,
} from "@material-ui/core";
import Colors from "helpers/colors";

// assets
import check from "../../../../assets/images/BN/illustration.png";
import GeneralButton from "../../Button/GeneralButton";

const PopupsError = ({
  isOpen,
  handleClose,
  title,
  message,
  submessage,
  height,
  img,
  closeModal,
  button,
  titlefontSize,
  widthMessage,
}) => {
  const useStyles = makeStyles(() => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      width: 480,
      minHeight: 345,
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      padding: 25,
      alignItems: "center",
      borderRadius: 20,
      position: "relative",
      textAlign: "center",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontSize: 28,
      fontWeight: 900,
      color: Colors.dark.hard,
    },
    xContainer: {
      display: "flex",
      justifyContent: "flex-end",
      "& .close": {
        "& :hover": {
          cursor: "pointer",
        },
      },
    },
  }));

  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        // onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            {closeModal ? (
              <div className={classes.xContainer}>
                <XIcon className="close" onClick={handleClose} />
              </div>
            ) : null}
            <div className={classes.content}>
              {titles
                ? titles.map((item) => (
                    <p
                      className={classes.title}
                      style={{
                        ...(titlefontSize && { fontSize: titlefontSize }),
                      }}
                    >
                      {item}
                    </p>
                  ))
                : null}
              <img
                src={img || check}
                alt="check"
                style={{ marginTop: titles ? 25 : 0, marginBottom: 15 }}
                width="160"
              />
              <Typography
                style={{
                  fontSize: 24,
                  marginTop: 15,
                  fontFamily: "FuturaMdBT",
                  color: Colors.dark.hard,
                  width: widthMessage,
                  letterSpacing: "1%",
                  fontWeight: 400,
                }}
              >
                {message}
              </Typography>
              <div
                style={{
                  fontFamily: "FuturaBkBT",
                  fontSize: 15,
                  color: Colors.dark.hard,
                }}
              >
                {submessage}
              </div>

              <GeneralButton
                label="OK"
                width="440px"
                height="44px"
                onClick={handleClose}
                style={{
                  marginTop: 20,
                  fontFamily: "FuturaMdBT",
                  fontSize: 15,
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

const GeneralErrorPopups = () => {
  const location = useLocation();
  const activePath = location?.pathname;

  const { errorAlreadyUsed } = useSelector(
    (store) => ({
      errorAlreadyUsed: store?.errorGeneral.errorAlreadyUsed,
    }),
    shallowEqual
  );

  const dispatch = useDispatch();

  const history = useHistory();

  if (errorAlreadyUsed?.code === "TKNAJ-04") {
    return (
      <PopupsError
        title={errorAlreadyUsed?.title}
        isOpen={errorAlreadyUsed.isError}
        handleClose={() => {
          dispatch(handleClearError());
        }}
        message=""
        submessage={errorAlreadyUsed.message}
        img={illustrationX}
      />
    );
  }

  return (
    <PopupsError
      isOpen={errorAlreadyUsed.isError}
      handleClose={() => {
        if (errorAlreadyUsed?.redirect) history.goBack();
        dispatch(handleClearError());
      }}
      message="Execute Failed"
      submessage={errorAlreadyUsed.message}
      title={errorAlreadyUsed?.title}
      img={illustrationX}
    />
  );
};

export default GeneralErrorPopups;
