// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  Grid,
  CircularProgress,
} from "@material-ui/core";
import { Checkbox } from "antd";
import { useSelector } from "react-redux";

// helpers
import { getPrevilegedata } from "utils/helpers";

// components
import RenderMenuAccess, {
  getIdsLIne,
  getNames,
} from "containers/BN/Security/PengaturanPeran/RenderMenuAccess";
import ScrollCustom from "components/BN/ScrollCustom";
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 520,
    minHeight: 657,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  access: {
    border: "1px solid #BCC8E7",
    minHeight: 200,
    maxHeight: 370,
    borderRadius: 10,
    padding: "10px 10px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  accessTitle: {
    fontSize: 13,
    color: "#7B87AF",
    borderBottom: "1px solid #7B87AF",
    paddingBottom: 2,
    margin: "10px 0 16px",
    fontFamily: "FuturaMdBT",
  },
  check: {
    fontSize: 15,
    color: "#374062",
    "& .ant-checkbox": {
      width: 20,
      height: 20,
      borderRadius: 3,
      "& .ant-checkbox-inner": {
        width: 20,
        height: 20,
        borderRadius: 3,
        border: "1px solid #BCC8E7",
        "&::after": {
          width: 6.5,
          height: 11.17,
          top: "42%",
        },
      },
    },
    "& .ant-checkbox-checked": {
      "& .ant-checkbox-inner": {
        border: "1px solid #0061A7 !important",
      },
    },
    "& .ant-checkbox-indeterminate": {
      "& .ant-checkbox-inner::after": {
        width: "10px !important",
        height: "10px !important",
        top: "50% !important",
      },
    },
  },
}));

const EditRole = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();

  const { privilege, isLoadingSubmit, isLoadingDuplicate, duplicateData } =
    useSelector(({ managementRole }) => ({
      ...managementRole,
    }));

  const [inputRole, setInputRole] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [privilegeLength, setPrivilegeLength] = useState(0);
  const [checkListIds, setCheckListIds] = useState([]);

  useEffect(() => {
    const ids = [];
    getIdsLIne(privilege?.privilegeList ?? [], ids);
    setPrivilegeLength(ids.length);
  }, [privilege?.privilegeList]);

  useEffect(() => {
    if (Object.keys(duplicateData).length) {
      setInputRole(duplicateData.roleName);
      setCheckListIds(getPrevilegedata("privilegeId", duplicateData));
    } else {
      setInputRole(null);
      setCheckListIds([]);
    }
  }, [duplicateData]);

  const handleCheckbox = (ids, currentIdClick) => {
    if (!checkListIds.find((a) => a === currentIdClick)) {
      setCheckListIds([...checkListIds, ...ids]);
    } else {
      const arr = checkListIds.filter((n) => n !== currentIdClick);
      setCheckListIds(arr);
    }
  };

  const handleClickAll = (e) => {
    const ids = [];
    const names = [];
    getIdsLIne(privilege?.privilegeList ?? [], ids);

    if (e.target.checked) {
      setCheckListIds(ids);
    } else {
      setCheckListIds([]);
    }
  };

  const handleCloseModal = () => {
    handleClose();

    setCheckListIds([]);
  };

  return (
    <div>
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>
              {duplicateData?.roleName
                ? `Duplicate ${duplicateData?.roleName}`
                : label}
            </h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              {isLoadingDuplicate ? (
                <div style={{ width: "100%", minHeight: 496, display: "flex" }}>
                  <div style={{ margin: "auto" }}>
                    <CircularProgress color="primary" size={40} />
                  </div>
                </div>
              ) : (
                <React.Fragment>
                  <Grid item xs={12}>
                    <p style={{ marginBottom: 2 }}>Nama Peran :</p>
                    <TextField
                      value={inputRole}
                      onChange={(e) => setInputRole(e.target.value)}
                      placeholder="Super Admin"
                      style={{ width: "100%" }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <div className={classes.access}>
                      <div style={{ paddingLeft: 8 }}>
                        <Checkbox
                          indeterminate={
                            checkListIds.length > 0 &&
                            checkListIds.length < privilegeLength
                          }
                          checked={
                            privilegeLength > 0 &&
                            checkListIds.length === privilegeLength
                          }
                          onChange={handleClickAll}
                          className={classes.check}
                          disabled={!inputRole}
                        >
                          Izinkan Semua Akses
                        </Checkbox>
                      </div>
                      <ScrollCustom height={372}>
                        <RenderMenuAccess
                          peran={inputRole}
                          menuAccess={privilege?.privilegeList ?? []}
                          checkList={checkListIds}
                          handleCheckbox={handleCheckbox}
                        />
                      </ScrollCustom>
                    </div>
                  </Grid>
                </React.Fragment>
              )}
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="157.4px"
                height="40px"
                color="#0061A7"
                disabled={isLoadingSubmit}
                onClick={handleCloseModal}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="157.4px"
                height="40px"
                disabled={isLoadingSubmit}
                loading={isLoadingSubmit}
                onClick={() => onContinue(true, { inputRole, checkListIds })}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditRole.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditRole.defaultProps = { onContinue: () => {} };

export default EditRole;
