// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import checksvg from "../../../../assets/icons/BN/clipboard.svg";
import check from "../../../../assets/images/BN/illustration.png";
import profilchecklist from "../../../../assets/images/BN/success-message.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 563,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const SuccessEndSessionUser = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 4500,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              {titles.map((item) => (
                <p className={classes.title}>{item}</p>
              ))}
              {/* <SvgIcon component={check} alt='check' style={{marginTop: 30}} /> */}
              <img
                src={profilchecklist}
                alt="check"
                style={{ marginTop: 25 }}
              />
              <Typography
                style={{
                  // color: '#374062',
                  fontSize: "24px",
                  fontFamily: "Futura Md BT",
                  fontWeight: "400",
                  width: "300px",
                  marginLeft: "60px",
                }}
              >
                {message}
              </Typography>{" "}
              <br />
              {/* <Typography style={{ lineHeight: 0 }}>
                Berhasil Menambahkan Pengguna Pengguna berhasil ditambahkan
                Silakan tunggu persetujuan dari admin approver Content Kembali
              </Typography> */}
              <br />
              <br />
              <GeneralButton
                label="OK"
                width="380px"
                height="40px"
                onClick={handleClose}
                // paddingTop= '30'
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

SuccessEndSessionUser.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

SuccessEndSessionUser.defaultProps = {
  title: "Saved Successfully",
  message: "End Session Success",
};

export default SuccessEndSessionUser;
