// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import ButtonOutlined from "../../Button/ButtonOutlined";
// components
import GeneralButton from "../../Button/GeneralButton";
import RadioGroup from "../../Radio/RadioGroup";
import TextField from "../../TextField/AntdTextField";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 420,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 36px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 40,
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 112,
  },
}));

const EditBillerKategori = ({ isOpen, handleClose, onContinue }) => {
  const classes = useStyles();

  const { editGeneralData } = useSelector(({ generalParam }) => ({
    editGeneralData: generalParam?.editGeneralData,
    isLoadingSubmit: generalParam?.isLoadingSubmit,
  }));

  const [namaModule, setNamaModule] = useState(null);
  const [kode, setKode] = useState(null);
  const [value, setValue] = useState(null);
  const [deskripsi, setDeskripsi] = useState(null);
  const [enkripsiValue, setEnkripsiValue] = useState("Yes");

  useEffect(() => {
    setNamaModule(editGeneralData.module);
    setKode(editGeneralData.name);
    setValue(editGeneralData.value);
    setDeskripsi(editGeneralData.description);
    setEnkripsiValue(editGeneralData.isEncrypted ? "Yes" : "No");
  }, [editGeneralData]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>Edit Parameter</h1>
            <div style={{ display: "flex" }}>
              <div style={{ width: "50%", paddingRight: 10 }}>
                <div style={{ marginBottom: 15 }}>
                  <p style={{ marginBottom: 2 }}>Module Name :</p>
                  <TextField
                    value={namaModule}
                    onChange={(e) => setNamaModule(e.target.value)}
                    placeholder="EntertainEster"
                    style={{
                      width: "100%",
                      fontFamily: "FuturaBkBT",
                      fontSize: "15px",
                    }}
                    disabled
                  />
                </div>
                <div style={{ marginBottom: 15 }}>
                  <p style={{ marginBottom: 2 }}>Value :</p>
                  <TextField
                    type="textArea"
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                    placeholder="On Account | On Account"
                    style={{
                      width: "100%",
                      fontFamily: "FuturaBkBT",
                      fontSize: "15px",
                      height: 113,
                    }}
                  />
                </div>
                <div style={{ marginBottom: 15 }}>
                  <p style={{ marginBottom: 10 }}>Encryption Value :</p>
                  <RadioGroup
                    value={enkripsiValue}
                    onChange={(e) => setEnkripsiValue(e.target.value)}
                    options={["Yes", "No"]}
                    style={{ width: "100%" }}
                  />
                </div>
              </div>
              <div style={{ width: "50%", paddingLeft: 10 }}>
                <div style={{ marginBottom: 15 }}>
                  <p style={{ marginBottom: 2 }}>Code :</p>
                  <TextField
                    value={kode}
                    onChange={(e) => setKode(e.target.value)}
                    placeholder="OA"
                    style={{
                      width: "100%",
                      fontFamily: "FuturaBkBT",
                      fontSize: "15px",
                    }}
                    disabled
                  />
                </div>
                <div style={{ marginBottom: 15 }}>
                  <p style={{ marginBottom: 2 }}>Description :</p>
                  <TextField
                    type="textArea"
                    value={deskripsi}
                    onChange={(e) => setDeskripsi(e.target.value)}
                    placeholder="Description"
                    style={{
                      width: "100%",
                      fontFamily: "FuturaBkBT",
                      fontSize: "15px",
                      height: 113,
                    }}
                  />
                </div>
              </div>
            </div>
            <div className={classes.button}>
              <ButtonOutlined
                label="Cancel"
                fontSize="15px"
                width="157.5px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
                style={{ fontSize: 15, fontFamily: "FuturaMdBT" }}
              />

              <GeneralButton
                label="Save"
                width="157.5px"
                fontSize="15px"
                height="40px"
                disabled={!value || !namaModule}
                style={{ fontFamily: "FuturaMdBT", fontSize: 15 }}
                onClick={() => {
                  onContinue({
                    idParameter: editGeneralData?.id,
                    namaModule,
                    kode,
                    value,
                    deskripsi,
                    enkripsiValue,
                  });
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditBillerKategori.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
};

EditBillerKategori.defaultProps = { onContinue: () => {} };

export default EditBillerKategori;
