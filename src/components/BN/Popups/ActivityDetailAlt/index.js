import React, { useState } from "react";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Box, Typography } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

import GeneralButton from "../../Button/GeneralButton";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& ::selection": {
      background: "#137EE1",
    },
  },
  titleModal: {
    fontFamily: "Futura",
    color: "#374062",
    fontWeight: 700,
    fontSize: "20px",
    lineHeight: "23.97px",
  },
  paper: {
    width: 700,
    minHeight: 330,
    height: "auto",
    backgroundColor: "white",
    border: "1px solid #BCC8E7",
    padding: "30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  subtitle: {
    width: 100,
    height: 16,
  },
  subtitle1: {
    fontFamily: "FuturaHvBT",
  },
  status: {
    position: "absolute",
    top: 30,
    right: 30,
  },
  reason: {
    backgroundColor: "#F4F7FB",
    border: "1px solid #BCC8E7",
    width: "100%",
    minHeight: 53,
    color: "#7B87AF",
    marginTop: 28,
    borderRadius: 8,
    padding: 10,
  },
}));

// eslint-disable-next-line react/prop-types
const PopupActivityDetail = ({
  isOpen,
  handleClose,
  children,
  reason,
  title,
}) => {
  const classes = useStyles();
  const [status, setStatus] = useState(0);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.status}>{children}</div>
            <div>
              <Typography className={classes.titleModal}>
                Detail Aktivitas
              </Typography>
            </div>
            <Box display="flex" justifyContent="flex-start" mt={3}>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  Nama :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  Jowo Darwono
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  Channel :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  Mobile Apps
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  ID Nasabah :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  210001929218
                </Typography>
              </Box>
            </Box>
            <Box display="flex" justifyContent="flex-start" mt={3}>
              <Box
                style={{
                  marginRight: "10px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 110,
                    height: 16,
                  }}
                >
                  Device Unique Id :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  ****21cc
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  OS :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  iOS 10
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  IP Address :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  21.0199.1821
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  Phone Type :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  iPhone X
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  Phone Brand :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  iPhone
                </Typography>
              </Box>
            </Box>
            <Box display="flex" justifyContent="flex-start" mt={3}>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  Status :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  Success
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: "20px",
                }}
              >
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    height: 16,
                  }}
                >
                  Remark :
                </Typography>
                <Typography
                  variant="subtitle2"
                  className={classes.subtitle1}
                  style={{
                    color: "#374062",
                    fontSize: "13px",
                    lineHeight: "16px",
                    width: 100,
                    fontWeight: 900,
                    height: 16,
                  }}
                >
                  -
                </Typography>
              </Box>
            </Box>
            {reason && (
              <div className={classes.reason}>
                <p style={{ fontFamily: "FuturaHvBT", fontWeight: 900 }}>
                  Reason :
                </p>
                <p>Fraud System</p>
              </div>
            )}
            <Box display="flex" justifyContent="flex-end" mt={5}>
              <Box>
                <GeneralButton
                  label="Tutup"
                  width="77px"
                  height="40px"
                  variant="contained"
                  onClick={handleClose}
                />
              </Box>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupActivityDetail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  reason: PropTypes.bool,
};

PopupActivityDetail.defaultProps = {
  reason: false,
};

export default PopupActivityDetail;
