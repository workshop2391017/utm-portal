// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import DeleteModal from "../DeleteConfirmation";
import GridItemField from "../../GridItemField";
import RadioGroup from "../../Radio/RadioGroup";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 470,
    minHeight: 400,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  check: {
    fontSize: 50,
  },
  radioItem: {
    marginLeft: 20,
  },
}));

const EditBankBulk = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [inputBank, setInputBank] = useState(null);
  const [inputSwift, setInputSwift] = useState(null);
  const [onlineSwitching, setOnlineSwitching] = useState("Jalin");

  //   const [value, setValue] = useState(null);

  //   const onChange = e => {
  //     setValue(e.target.value);
  //   };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid item xs={24} style={{ marginBottom: 60 }}>
              <p style={{ marginBottom: 15 }}>Jenis Online Switching :</p>
              <RadioGroup
                value={onlineSwitching}
                onChange={(e) => setOnlineSwitching(e.target.value)}
                options={["Jalin", "Alto", "Artajasa", "Prima"]}
                direction="horizontal"
              />
            </Grid>
            <span
              style={{
                margin: "0 auto 80px auto",
                width: 410,
                minHeight: 56,
                background: "#FFE4E4",
                fontSize: 15,
                border: "1px solid #FF6F6F",
                display: "inline-block",
                textAlign: "center",
                borderRadius: 8,
                color: "#FF6F6F",
                fontFamily: "FuturaBkBT",
                padding: 5,
              }}
            >
              Ketika melakukan perubahan online switching secara bulk, maka
              seluruh bank yang terpilih akan terjadi perubahan
            </span>
            ;
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Simpan"
                width="92px"
                height="40px"
                onClick={onContinue}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditBankBulk.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

EditBankBulk.defaultProps = { onContinue: () => {} };

export default EditBankBulk;

const roleOptions = [
  {
    value: 1,
    label: "Jalin",
  },
  {
    value: 2,
    label: "Alto",
  },
];
