// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  createTheme,
  ThemeProvider,
  CircularProgress,
} from "@material-ui/core";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import Colors from "helpers/colors";

// components
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";

// assets
import logo from "../../../../assets/images/BN/illustrationyellow.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 419,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "36px 26px 22px 26px",
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 28,
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    bottom: 30,
    marginTop: 20,
  },
  desc: {
    fontFamily: "FuturaBkBT",
    fontSize: 15,
  },
  message: {
    fontSize: 24,
    fontFamily: "FuturaMdBT",
    color: Colors.dark.hard,
    marginTop: 20,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
}));

// ubah button disini
const buttonRedTheme = createTheme({
  palette: {
    primary: {
      // light: 'yellow',
      main: "#0061A7",
      // dark: 'green',
      contrastText: "#fff",
    },
  },
});

const DeletePopup = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  loading,
  message,
  submessage,
  height,
  imageIcon,
}) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);
  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        // onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.xContainer}>
              <XIcon
                className="close"
                onClick={handleClose}
                width={12}
                height={12}
              />
            </div>
            {titles.map((item) => (
              <h1 className={classes.title}>{item}</h1>
            ))}

            <img
              src={imageIcon || logo}
              style={{ paddingTop: 13 }}
              alt="Exclamation Triangle"
            />
            <Typography className={classes.message}>{message}</Typography>
            <p className={classes.desc}>{submessage}</p>
            <div className={classes.button}>
              <ThemeProvider theme={buttonRedTheme}>
                <ButtonOutlined
                  label={loading ? <CircularProgress size={20} /> : "Yes"}
                  width={197.5}
                  height="40px"
                  color="#0061A7"
                  onClick={onContinue}
                  disabled={!!loading}
                  withHoverColor
                />
              </ThemeProvider>

              <ButtonOutlined
                label="No"
                width={197.5}
                height="40px"
                color="#0061A7"
                onClick={handleClose}
                disabled={!!loading}
                withHoverColor
              />
              {/* <GeneralButton
                withHoverColor
                label="No"
                width={157.5}
                height="40px"
                color="#0061A7"
                onClick={handleClose}
                disabled={!!loading}
              /> */}
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

DeletePopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string,
  height: PropTypes.string,
  imageIcon: PropTypes.node,
};

DeletePopup.defaultProps = {
  title: "Confirmation",
  message: "Are You Sure You Are Deleting Data?",
  submessage: "You can't undo this action",
  onContinue: () => {},
  height: 495,
  imageIcon: null,
};

export default DeletePopup;
