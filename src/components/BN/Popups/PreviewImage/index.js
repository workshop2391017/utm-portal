// main
import React from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  IconButton,
} from "@material-ui/core";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import imageFile from "assets/images/BN/fileBg.png";

// assets
import { ReactComponent as ArrowLeft } from "assets/icons/BN/arrow-left-white.svg";
import minuspopup from "assets/icons/BN/iconminus.svg";
import pluspopup from "assets/icons/BN/iconplus.svg";
import { DocType } from "utils/helpers/docType";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  zoom: {
    width: 89,
    background: "#374062",
    height: "50px",
    borderRadius: "5px",
    color: "#fff",
    display: "flex",
    position: "relative",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginTop: 25,
  },
  paperlr: {
    marginRight: 15,
    "& path": {
      fill: "white",
    },
  },
  textlr: {
    width: "auto",
    color: "#fff",
    fontFamily: "FuturaMdBT",
    fontSize: 20,
  },
  paper: {
    // backgroundColor: "white",
    // border: "1px solid #BCC8E7",
    // alignItems: "center",
    // position: "relative",
    // textAlign: "center",
    // display: "flex",
    // justifyContent: "center",
    // height: "600px",
  },
  content: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  zoomButtonContainer: {
    display: "flex",
    justifyContent: "center",
  },
  buttonTitleGroup: {
    position: "absolute",
    left: "70px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: 30,
    "&:hover": {
      boxShadow: "none",
    },
  },
}));

const PreviewImage = ({ isOpen, handleClose, title, image }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        // className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div>
            <IconButton
              className={classes.buttonTitleGroup}
              onClick={handleClose}
            >
              <div className={classes.paperlr}>
                <ArrowLeft />
              </div>
              <div>
                <p className={classes.textlr}>{title}</p>
              </div>
            </IconButton>
            <div style={{ marginTop: 150 }}>
              <TransformWrapper defaultScale={4}>
                {({ zoomIn, zoomOut, ...rest }) => (
                  <React.Fragment>
                    <div className={classes.paper}>
                      <div className={classes.content}>
                        <TransformComponent
                          contentStyle={{ maxHeight: 650, maxWidth: 650 }}
                          wrapperStyle={{ maxHeight: 650, maxWidth: 650 }}
                          // {...rest}
                        >
                          {image && (
                            <img
                              src={image}
                              alt="KTP"
                              style={{ maxHeight: 650, maxWidth: 650 }}
                            />
                          )}
                        </TransformComponent>
                      </div>
                    </div>

                    <div className={classes.zoomButtonContainer}>
                      <div className={classes.zoom}>
                        <IconButton onClick={() => zoomOut()}>
                          <img src={minuspopup} alt="minus" />
                        </IconButton>
                        <IconButton onClick={() => zoomIn()}>
                          <img src={pluspopup} alt="plus" />
                        </IconButton>
                      </div>
                    </div>
                  </React.Fragment>
                )}
              </TransformWrapper>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PreviewImage.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  image: PropTypes.string.isRequired,
};

PreviewImage.defaultProps = {
  title: "KTP.jpg",
};

export default PreviewImage;
