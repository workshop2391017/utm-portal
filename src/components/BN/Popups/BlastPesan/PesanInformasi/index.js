import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";

import TextField from "components/BN/TextField/AntdTextField";
import Date from "components/BN/DatePicker/GeneralDatePicker";
import InputImage from "components/BN/Inputs/InputImage";
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";

const PesanInformasi = (props) => {
  const [state, setState] = useState(null);
  const [checkboxGroupA, setCheckboxGroupA] = useState(null);
  const [checkboxGroupB, setCheckboxGroupB] = useState(null);

  return (
    <div
      style={{ display: "flex", flexDirection: "column", position: "relative" }}
    >
      <div>
        <p style={{ marginBottom: 2 }}>Judul :</p>
        <TextField
          value=""
          // onChange={(e) => setInputJudul(e.target.value)}
          placeholder="Judul"
          style={{ width: "100%" }}
        />
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: "24px",
        }}
      >
        <div>
          <p style={{ marginBottom: 2 }}>Tanggal :</p>
          <Date
            placeholder="Pilih tanggal Blast"
            height="40px"
            // value={dateValue}
            // onChange={(e) => setDateValue(e)}
            style={{ borderRadius: 10, width: 283 }}
          />
        </div>
        <div>
          <p style={{ marginBottom: 2 }}>Upload Gambar :</p>
          <InputImage
            height="40px"
            style={{ width: 283 }}
            placeholder="gambarpromo.jpg"
          />
        </div>
      </div>
      <div style={{ marginTop: "24px" }}>
        <p style={{ marginBottom: 2 }}>Pesan:</p>
        <TextField
          // value={inputPesan}
          type="textArea"
          //  onChange={(e) => setInputPesan(e.target.value)}
          placeholder="Pesan"
          style={{
            width: "100%",
            height: "80px",
            marginBottom: "20px",
            resize: "none",
          }}
        />
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <p style={{ marginBottom: 2 }}>Pilih Media Promo :</p>
        <CheckboxGroup
          value={checkboxGroupA}
          options={["Aplikasi", "SMS"]}
          onChange={(e) => setCheckboxGroupA(e)}
          margin="20px"
          width="250px"
        />
        <CheckboxGroup
          value={checkboxGroupB}
          options={["Email", "Whatsapp"]}
          onChange={(e) => setCheckboxGroupB(e)}
          margin="20px 20px 20px 20px"
          width="250px"
        />
      </div>
    </div>
  );
};

PesanInformasi.propTypes = {};

export default PesanInformasi;
