// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

import { Checkbox } from "antd";
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";

// components
import Tabs from "components/BN/Tabs/GeneralTabs";
import PopupFilter from "containers/BN/ConsolidationReport/Detail/PopupFilter";
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
import SuccessConfirmation from "../SuccessConfirmation";
import Date from "../../DatePicker/GeneralDatePicker";
import AddComment from "../AddComment";

// assets
import Trash from "../../../../assets/icons/BN/trash-blue.svg";
import PesanInformasi from "./PesanInformasi";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 200,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    bottom: 30,
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
  scrolledDate: {
    border: "1px solid #D3DCF4",
    borderRadius: 8,
    minHeight: 200,
    maxHeight: 220,
    padding: "10px 0px",
    marginTop: "20px",
    marginBottom: "20px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  scrolledSelect: {
    border: "1px solid #D3DCF4",
    borderRadius: 8,
    minHeight: 200,
    maxHeight: 280,
    padding: "10px 10px",
    marginBottom: "20px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  check: {
    fontSize: 15,
    color: "#374062",
    "& .ant-checkbox": {
      width: 20,
      height: 20,
      borderRadius: 3,
      "& .ant-checkbox-inner": {
        width: 20,
        height: 20,
        borderRadius: 3,
        border: "1px solid #BCC8E7",
        "&::after": {
          width: 6.5,
          height: 11.17,
          top: "42%",
        },
      },
    },
    "& .ant-checkbox-checked": {
      "& .ant-checkbox-inner": {
        border: "1px solid #0061A7 !important",
      },
    },
  },
}));

const BlastPesan = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [checkboxGroupA, setCheckboxGroupA] = useState(null);
  const [checkboxGroupB, setCheckboxGroupB] = useState(null);
  const [filter, setFilter] = useState({ tabs: 0 });
  const [dateValue, setDateValue] = useState(null);
  const [inputJudul, setInputJudul] = useState("Tokopedia");
  const [inputPesan, setInputPesan] = useState(
    "Payment successful Payment to PREPAID TELKOMSEL with amount IDR **,***.** is successful"
  );
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [addComment, setAddComment] = useState(false);

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };
  const handleOpenConfirm = () => {
    setAddComment(false);
    setConfirmSuccess(true);
  };

  console.warn("filterrr =", filter?.tabs);

  return (
    <div>
      {/* <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
      /> */}
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
        message="Blast Pesan Dijadwalkan"
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Tabs
                onChange={(event, newValue) => {
                  setFilter({
                    ...filter,
                    tabs: newValue,
                  });
                }}
                value={filter?.tabs}
                tabWidth={110}
                tabs={["Pesan Informasi", "Pesan Promo"]}
                style={{ height: "30px" }}
              />
            </div>

            {filter?.tabs === 1 ? (
              <Grid container spacing={2} style={{ marginTop: "12px" }}>
                <Grid item xs={6}>
                  <p style={{ marginBottom: 2 }}>Tgl Blast :</p>
                  <Date
                    placeholder="Pilih tanggal Blast"
                    value={dateValue}
                    onChange={(e) => setDateValue(e)}
                    style={{ width: "100%" }}
                  />
                  <div className={classes.scrolledDate}>
                    <div>
                      <div>
                        {[
                          "12/12/2021, 10:00 AM",
                          "11/12/2021, 22:10 AM",
                          "11/12/2021, 10:20 AM",
                          "10/12/2021, 12:00 AM",
                          "9/12/2021, 11:12 AM",
                          "8/12/2021, 13:15 AM",
                          "7/12/2021, 22:00 AM",
                          "7/12/2021, 10:00 AM",
                          "6/12/2021, 02:20 AM",
                          "06/12/2021, 04:31 AM",
                          "30/11/2021, 22:00 AM",
                          "12/12/2021, 10:00 AM",
                          "12/12/2021, 10:00 AM",
                          "12/12/2021, 10:00 AM",
                        ].map((item, i) => (
                          <div style={{ marginTop: i === 0 ? 0 : 12 }}>
                            <p
                              style={{
                                display: "inline-block",
                                marginLeft: 20,
                              }}
                            >
                              {item}
                            </p>
                            <img
                              src={Trash}
                              alt="trash-icon"
                              style={{
                                cursor: "pointer",
                                display: "inline-block",
                                verticalAlign: "top",
                                float: "right",
                                marginRight: 10,
                              }}
                            />
                            <div
                              style={{
                                borderBottom: "1px solid #D3DCF4",
                                marginTop: "12px",
                                width: "100%",
                              }}
                            />
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <p style={{ marginBottom: 2 }}>Pilih Segmentasi :</p>
                  <div className={classes.scrolledSelect}>
                    <div>
                      <div>
                        {[
                          "Silver",
                          "Platinum",
                          "Gold",
                          "Bronze",
                          "Gold",
                          "Gold",
                          "Platinum",
                          "Bronze",
                        ].map((item, i) => (
                          <div
                            style={{
                              paddingLeft: 8,
                              marginTop: i === 0 ? 0 : 10,
                            }}
                          >
                            <Checkbox className={classes.check}>
                              {item}
                            </Checkbox>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <p style={{ marginBottom: 2 }}>Judul :</p>
                  <TextField
                    value={inputJudul}
                    onChange={(e) => setInputJudul(e.target.value)}
                    placeholder="Judul"
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <p style={{ marginBottom: 2 }}>Pesan:</p>
                  <TextField
                    value={inputPesan}
                    type="textArea"
                    onChange={(e) => setInputPesan(e.target.value)}
                    placeholder="Pesan"
                    style={{
                      width: "100%",
                      height: "80px",
                      marginBottom: "20px",
                    }}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  style={{ display: "flex", flexDirection: "column" }}
                >
                  <p style={{ marginBottom: 2 }}>Pilih Media Promo :</p>
                  <CheckboxGroup
                    value={checkboxGroupA}
                    options={["Aplikasi", "SMS"]}
                    onChange={(e) => setCheckboxGroupA(e)}
                    margin="20px"
                    width="250px"
                  />
                  <CheckboxGroup
                    value={checkboxGroupB}
                    options={["Email", "Whatsapp"]}
                    onChange={(e) => setCheckboxGroupB(e)}
                    margin="20px 20px 20px 20px"
                    width="250px"
                  />
                </Grid>
              </Grid>
            ) : (
              <PesanInformasi />
            )}

            <div
              className={classes.button}
              style={{ marginTop: 20, paddingTop: "10px" }}
            >
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
              <GeneralButton
                label="Jadwalkan"
                width="92px"
                height="40px"
                onClick={() => {
                  handleClose();
                  setConfirmSuccess(true);
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

BlastPesan.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

BlastPesan.defaultProps = { onContinue: () => {} };

export default BlastPesan;
