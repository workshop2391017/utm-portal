// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";

// libraries
import { makeStyles, Modal, Fade, Backdrop } from "@material-ui/core";
import GeneralButton from "../../Button/GeneralButton";

// assets
// import times from '../../../../assets/icons/BN/times-circle.svg';

import offline from "../../../../assets/images/BN/offlinephone.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 420,
    minHeight: 470,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 30,
  },
}));

const FailedConfirmation = ({ isOpen, handleClose, title }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => {
        handleClose();
      }, 2000);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div className={classes.content}>
              <img src={offline} alt="Offline" style={{ marginTop: 25 }} />
              <br />
              <Typography
                style={{
                  // color: '#374062',
                  fontSize: "18px",
                  fontWeight: "bold",
                }}
              >
                Data Offline
              </Typography>
              <p>
                Sistem sedang offline. Mohon coba kembali dalam beberapa saat.
              </p>
              <br />
              <GeneralButton
                // ini popup offline
                label="OK"
                width="370px"
                height="40px"
                onClick=""
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

FailedConfirmation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
};

FailedConfirmation.defaultProps = {
  title: "Internet Anda Sedang Offline",
};

export default FailedConfirmation;
