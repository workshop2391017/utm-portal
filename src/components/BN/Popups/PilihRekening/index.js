// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { Typography } from "antd";
import ScrollCustom from "components/BN/ScrollCustom";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";
import Colors from "helpers/colors";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { getDataManagmentRekeningPopUp } from "stores/actions/managementPerusahaan/managmentCompanyRekening";

import ButtonOutlined from "../../Button/ButtonOutlined";
import GeneralButton from "../../Button/GeneralButton";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 996,
    height: 610,
    backgroundColor: "#fff",
    // border: "1px solid #BCC8E7",
    padding: 30,
    borderRadius: 20,
    position: "relative",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
  card: {
    width: 439,
    height: 315,
    padding: "20px 0 20px 20px",
    border: "1px solid #BCC8E7",
    borderRadius: 20,
    overflow: "hidden",
  },
  checkboxGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 18,
    "& .side": {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      "& .check": {
        marginRight: 15,
      },
      "& .nomor": {
        color: Colors.dark.hard,
        fontSize: 15,
        fontFamily: "FuturaHvBT",
      },
      "& .type": {
        color: Colors.dark.medium,
        fontSize: 13,
        fontDisplay: "FuturaMdBT",
      },
    },

    "& .nominal": {
      color: Colors.dark.hard,
      fontSize: 15,
      fontFamily: "FuturaHvBT",
      "& span": {
        fontSize: 11,
        verticalAlign: "super",
        marginRight: 5,
      },
    },
  },
}));

const dummyRekening = [
  {
    id: 1,
    nomor: 123123123321,
    nominal: 72500000000000,
    type: "Giro",
    star: true,
  },
  {
    id: 2,
    nomor: 123123123321,
    nominal: 72500000000000,
    type: "Giro",
  },
  {
    id: 3,
    nomor: 123123123321,
    nominal: 72500000000000,
    type: "Giro",
  },
  {
    id: 4,
    nomor: 123123123321,
    nominal: 72500000000000,
    type: "Giro",
  },
  {
    id: 5,
    nomor: 123123123321,
    nominal: 72500000000000,
    type: "Giro",
  },
  {
    id: 6,
    nomor: 123123123321,
    nominal: 72500000000000,
    type: "Giro",
  },
];

const dummyUserGroup = [
  {
    id: 1,
    name: "Maker 1",
    divisi: "Finance",
    count: 124,
  },
  {
    id: 2,
    name: "Maker 1",
    divisi: "Finance",
    count: 124,
  },
  {
    id: 3,
    name: "Maker 1",
    divisi: "Finance",
    count: 124,
  },
  {
    id: 4,
    name: "Maker 1",
    divisi: "Finance",
    count: 124,
  },
  {
    id: 5,
    name: "Maker 1",
    divisi: "Finance",
    count: 124,
  },
  {
    id: 6,
    name: "Maker 1",
    divisi: "Finance",
    count: 124,
  },
];

const PilihRekening = ({ isOpen, handleClose, title, message, onContinue }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [titles, setTitles] = useState([]);
  const [formData, setFormData] = useState({ rekeningList: [], groupList: [] });
  const { dataDetail, dataRekening, dataKelRek } = useSelector(
    (state) => state.managmentCompanyRekening
  );

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    getDataManagmentRekeningPopUp()(dispatch);
  }, []);
  useEffect(() => {
    setFormData({
      accounts: dataKelRek?.corporateAccounts,

      menuGroup: dataKelRek?.corporateMenuGroups,
    });
  }, [dataKelRek]);

  // useEffect(() => {
  //   setFormData({ rekeningList: dummyRekening, groupList: dummyUserGroup });
  // }, [dummyUserGroup, dummyRekening]);

  const handleGroupChange = (elm, index) => {
    const arr = [...formData.menuGroup];

    arr[index] = {
      ...arr[index],
      ...elm,
      checked: !arr[index]?.checked,
    };

    setFormData({ ...formData, menuGroup: arr });
  };

  const handleRekeningChange = (elm, index) => {
    const arr = [...formData.accounts];

    arr[index] = {
      ...arr[index],
      ...elm,
      checked: !arr[index]?.checked,
    };

    setFormData({ ...formData, accounts: arr });
  };

  console.warn("dataKelRek:", dataKelRek);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div>
              <Grid item xs={12}>
                <Typography
                  style={{
                    color: "#374062",
                    fontSize: "32px",
                    fontWeight: "400",
                    fontFamily: "FuturaHvBt",
                    textAlign: "center",
                  }}
                >
                  Rekening / Kel. User
                </Typography>
                <Typography
                  style={{
                    color: "#374062",
                    fontSize: "15px",
                    fontWeight: "400",
                    fontFamily: "FuturaMdBT",
                    marginTop: 15,
                    textAlign: "center",
                  }}
                >
                  Pilih rekening untuk dimasukan kedalam kelompok ini
                </Typography>
              </Grid>

              <div
                style={{
                  display: "flex",
                  marginTop: 41,
                  justifyContent: "center",
                }}
              >
                <div style={{ marginRight: 10 }}>
                  <span style={{ marginBottom: 10 }}>Rekening :</span>
                  <div className={classes.card}>
                    <ScrollCustom height={315}>
                      {(formData?.accounts ?? []).map((elm, i) => (
                        <div className={classes.checkboxGroup} key={i}>
                          <div className="side">
                            <div className="check">
                              <CheckboxSingle
                                checked={elm?.checked}
                                onChange={() => handleRekeningChange(elm, i)}
                              />
                            </div>
                            <div>
                              <div className="nomor">{elm.accountNumber}</div>
                              <div className="type">{elm.accountType}</div>
                            </div>
                          </div>
                          <div className="nominal">
                            <span>Rp</span>
                            {elm.totalBalance}
                          </div>
                        </div>
                      ))}
                    </ScrollCustom>
                  </div>
                </div>
                <div style={{ marginLeft: 10 }}>
                  <span style={{ marginBottom: 10 }}>Kelompok User :</span>
                  <div className={classes.card}>
                    <ScrollCustom height={315}>
                      {(formData?.menuGroup ?? []).map((elm, i) => (
                        <div className={classes.checkboxGroup} key={i}>
                          <div className="side">
                            <div className="check">
                              <CheckboxSingle
                                checked={elm?.checked}
                                onChange={() => handleGroupChange(elm, i)}
                              />
                            </div>
                            <div>
                              <div className="nomor">{elm.name}</div>
                              <div className="type">{elm.division}</div>
                            </div>
                          </div>
                          <div className="nominal">{elm.totalUser} User</div>
                        </div>
                      ))}
                    </ScrollCustom>
                  </div>
                </div>
              </div>
            </div>

            <div
              style={{
                height: 88,
                display: "flex",
                justifyContent: "space-between",
                padding: "46px 20px 20px 20px",
                width: "100%",
              }}
            >
              <ButtonOutlined label="Batal" width="157,5px" height="40px" />
              <GeneralButton
                label="Simpan"
                width="157,5px"
                height="40px"
                onClick={() =>
                  onContinue({
                    accounts: (formData?.accounts ?? []).filter(
                      ({ checked }) => checked
                    ),
                    menuGroup: (formData?.menuGroup ?? []).filter(
                      ({ checked }) => checked
                    ),
                  })
                }
                closeAfterTransition
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PilihRekening.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onContinue: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

PilihRekening.defaultProps = {
  title: "Pilih Rek",
  message: "Pilihan Rekening",
};

export default PilihRekening;
