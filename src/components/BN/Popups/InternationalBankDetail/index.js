// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";

import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ButtonOutlined from "../../Button/ButtonOutlined";
// components
import GeneralButton from "../../Button/GeneralButton";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    border: "1px solid #BCC8E7",
    width: 800,
    minHeight: 480,
    borderRadius: 20,
    padding: "50px 40px 20px 40px",
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: 400,
    marginBottom: 40,
    textAlign: "center",
  },
  detailContainer: {
    display: "flex",
    marginBottom: 20,
  },
  halfDetailContainer: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
    gap: 10,
  },
  titleCard: {
    fontFamily: "FuturaMdBT",
    fontSize: 13,
    fontWeight: 700,
  },
  subtitleCard: {
    fontFamily: "FuturaMdBT",
    fontSize: 15,
    paddingRight: 30,
    wordWrap: "break-word",
  },
  button: {
    position: "absolute",
    bottom: 30,
    right: 30,
  },
});

const InternationalBankDetail = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  rowData,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [openModal, setOpenModal] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState(false);

  const onSaveData = () => {
    console.warn("saving data");
  };

  return (
    <div>
      <SuccessConfirmation
        isOpen={successConfirmModal}
        handleClose={() => setSuccessConfirmModal(false)}
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>
            <div className={classes.detailContainer}>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>Bank Code</h3>
                <h5 className={classes.subtitleCard}>
                  {rowData?.onlineBankCode}
                </h5>
              </div>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>City</h3>
                <h5 className={classes.subtitleCard}>{rowData?.city}</h5>
              </div>
            </div>
            <div className={classes.detailContainer}>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>Bank Name</h3>
                <h5 className={classes.subtitleCard}>{rowData?.bankName}</h5>
              </div>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>Currency</h3>
                <h5 className={classes.subtitleCard}>
                  {rowData?.currencyCode}
                </h5>
              </div>
            </div>
            <div className={classes.detailContainer}>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>Bank Alias Name</h3>
                <h5 className={classes.subtitleCard}>
                  {rowData?.bankShortName}
                </h5>
              </div>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>BIC</h3>
                <h5 className={classes.subtitleCard}>{rowData?.swiftCode}</h5>
              </div>
            </div>
            <div className={classes.detailContainer}>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>Country</h3>
                <h5 className={classes.subtitleCard}>{rowData?.country}</h5>
              </div>
              <div className={classes.halfDetailContainer}>
                <h3 className={classes.titleCard}>Status</h3>
                <h5 className={classes.subtitleCard}>
                  {rowData.isDeleted ? "Inactive" : "Active"}
                </h5>
              </div>
            </div>
            <div className={classes.button}>
              <GeneralButton
                label="Back"
                width="76px"
                height="40px"
                onClick={handleClose}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

InternationalBankDetail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
};

InternationalBankDetail.defaultProps = {
  onContinue: () => {},
  title: "",
};

export default InternationalBankDetail;
