// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import GeneralButton from "../../Button/GeneralButton";

// assets
import check from "../../../../assets/icons/BN/user-blue-x.png";

const BlockedPopup = ({
  isOpen,
  handleClose,
  message,
  submessage,
  height,
  button,
}) => {
  const useStyles = makeStyles(() => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      width: 480,
      height: 495,
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      padding: 0,
      alignItems: "center",
      borderRadius: 20,
      position: "relative",
      textAlign: "center",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      paddingTop: 49,
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontSize: 36,
      fontWeight: 900,
      color: Colors.dark.hard,
    },
    xContainer: {
      display: "flex",
      justifyContent: "flex-end",
      "& .close": {
        "& :hover": {
          cursor: "pointer",
        },
      },
    },
  }));

  const classes = useStyles();

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paper} style={{ height }}>
            <div className={classes.content}>
              <img src={check} alt="check" width="160" />
              <Typography
                style={{
                  fontSize: 24,
                  marginTop: 20,
                  fontFamily: "FuturaMdBT",
                  color: Colors.dark.hard,
                }}
              >
                {message}
              </Typography>
              <div
                style={{
                  fontFamily: "FuturaBkBT",
                  fontSize: 15,
                  color: Colors.dark.hard,
                }}
              >
                {submessage}
              </div>
              {button ? (
                <GeneralButton
                  label="OK"
                  width="380px"
                  height="40px"
                  onClick={handleClose}
                  style={{
                    marginTop: 45,
                    fontFamily: "FuturaMdBT",
                    fontSize: 15,
                  }}
                />
              ) : null}
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

BlockedPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  message: PropTypes.string,
  submessage: PropTypes.string,
  height: PropTypes.string,
  button: PropTypes.bool,
};

BlockedPopup.defaultProps = {
  message: "Account Blocked",
  submessage: "Your account has been blocked",
  height: 423,
  button: true,
};

export default BlockedPopup;
