// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import GeneralButton from "../../Button/GeneralButton";

// assets
import check from "../../../../assets/images/BN/illustration.png";

const SystemMaintenance = ({
  isOpen,
  handleClose,
  title,
  message,
  submessage,
  height,
  img,
  closeModal,
  button,
  titlefontSize,
  widthMessage,
}) => {
  const useStyles = makeStyles(() => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      width: 480,
      minHeight: 300,
      backgroundColor: "#fff",
      border: "1px solid #BCC8E7",
      padding: closeModal ? 30 : 0,
      alignItems: "center",
      borderRadius: 20,
      position: "relative",
      textAlign: "center",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontSize: 28,
      fontWeight: 900,
      color: Colors.dark.hard,
    },
    xContainer: {
      display: "flex",
      justifyContent: "flex-end",
      "& .close": {
        "& :hover": {
          cursor: "pointer",
        },
      },
    },
  }));

  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            {closeModal ? (
              <div className={classes.xContainer}>
                <XIcon className="close" onClick={handleClose} />
              </div>
            ) : null}
            <div className={classes.content}>
              {titles
                ? titles.map((item) => (
                    <p
                      className={classes.title}
                      style={{
                        ...(titlefontSize && { fontSize: titlefontSize }),
                      }}
                    >
                      {item}
                    </p>
                  ))
                : null}
              <img
                src={img || check}
                alt="check"
                style={{ marginTop: titles ? 25 : 0, marginBottom: 15 }}
                width="160"
              />
              <Typography
                style={{
                  fontSize: 24,
                  marginTop: 15,
                  fontFamily: "FuturaMdBT",
                  color: Colors.dark.hard,
                  width: widthMessage,
                  letterSpacing: "1%",
                  fontWeight: 400,
                }}
              >
                {message}
              </Typography>
              <div
                style={{
                  fontFamily: "FuturaBkBT",
                  fontSize: 15,
                  color: Colors.dark.hard,
                }}
              >
                {submessage}
              </div>
              {button ? (
                <GeneralButton
                  label="OK"
                  width="440px"
                  height="44px"
                  onClick={handleClose}
                  style={{
                    marginTop: 20,
                    fontFamily: "FuturaMdBT",
                    fontSize: 15,
                    position: "aboslute",
                    bottom: 0,
                    marginBottom: 15,
                  }}
                />
              ) : null}
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

SystemMaintenance.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  submessage: PropTypes.string || PropTypes.node,
  height: PropTypes.string,
  img: PropTypes.node,
  closeModal: PropTypes.bool,
  button: PropTypes.bool,
  titlefontSize: PropTypes.number,
  widthMessage: PropTypes.number,
};

SystemMaintenance.defaultProps = {
  title: "Please Wait For Approval",
  message: "Saved Successfully",
  submessage: "",
  widthMessage: null,
  height: 495,
  img: check,
  closeModal: true,
  button: true,
  titlefontSize: 28,
};

export default SystemMaintenance;
