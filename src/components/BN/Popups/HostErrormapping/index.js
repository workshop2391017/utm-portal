// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { parseTextNumFirstSpace, validateMinMax } from "utils/helpers";
import { useValidateLms } from "utils/helpers/validateLms";
import { confValidateMenuName } from "stores/actions/validatedatalms";
import FormField from "components/BN/Form/InputGroupValidation";
import ButtonOutlined from "../../Button/ButtonOutlined";
// components
import GeneralButton from "../../Button/GeneralButton";
import RadioGroup from "../../Radio/RadioGroup";
import TextField from "../../TextField/AntdTextField";

import {
  addDataHostErrorMapping,
  editDataHostErrorMapping,
  setTypeOpenModal,
  setTypeSuccess,
} from "../../../../stores/actions/hostErrorMapping";
import DeleteConfirmation from "../Delete";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 502,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: "normal",
    marginBottom: 68,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    marginTop: 71,
  },
}));

const ErrorModal = ({
  isOpen,
  handleClose,
  setDataRow,
  onContinue,
  title,
  rowData,
}) => {
  const {
    data,
    isLoading,
    isLoadingExcute,
    error,
    isDelete,
    isOpen: isOpenStore,
  } = useSelector((state) => state.hostErrorMapping);
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isAlreadyExist } = useSelector((e) => e.validatedatalms);
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [openModalConfirmationEdit, setOpenModalConfirmationEdit] =
    useState(false);

  const [formData, setFormData] = useState({
    id: null,
    code: "",
    engMessage: "",
    idnMessage: "",
    sourceSystem: "",
  });

  const [formDataReadOnly, setFormDataReadOnly] = useState({
    id: null,
    code: "",
    engMessage: "",
    idnMessage: "",
    sourceSystem: "",
  });

  // validate char
  const vErrCode = !validateMinMax(formData.code, 2, 10);
  const vSourceSystem = !validateMinMax(formData.sourceSystem, 2, 20);
  const vIdn = !validateMinMax(formData.idnMessage, 1, 200);
  const vEng = !validateMinMax(formData.engMessage, 1, 200);

  useEffect(() => {
    if (rowData) {
      setFormData({
        ...formData,
        id: rowData?.id,
        code: rowData?.code,
        engMessage: rowData?.engMessage,
        idnMessage: rowData?.idnMessage,
        sourceSystem: rowData?.sourceSystem,
      });
      setFormDataReadOnly({
        sourceSystem: rowData?.sourceSystem,
        code: rowData?.code,
      });
    } else {
      setFormData({
        id: null,
        code: "",
        engMessage: "",
        idnMessage: "",
        sourceSystem: "",
      });
    }
  }, [rowData]);

  const onChangeHandler = (field, e) => {
    let value = e.target.value;
    if (field === "code") value = parseTextNumFirstSpace(value);
    setFormData({ ...formData, [field]: value });
  };

  const handleNext = () => {
    setOpenModalConfirmation(false);
  };

  const clearForm = () => {
    setDataRow({});
    setOpenModalConfirmation(false);
    setOpenModalConfirmationEdit(false);
    setFormData({
      id: null,
      code: "",
      engMessage: "",
      idnMessage: "",
      sourceSystem: "",
    });
  };
  const handleSaveClear = () => {
    clearForm();
    handleClose();
  };

  const addData = () => {
    if (rowData?.id) {
      dispatch(editDataHostErrorMapping(formData, { handleSaveClear }));
    } else {
      dispatch(
        addDataHostErrorMapping(formData, { handleNext, handleSaveClear })
      );
    }
  };

  // validate already exist
  useValidateLms(
    {
      name: formData?.sourceSystem,
      code: formData?.code,
      nameCode: confValidateMenuName.HOST_ERROR_CODE,
    },
    true,
    {
      pause:
        formDataReadOnly.sourceSystem && formDataReadOnly.code
          ? formData?.sourceSystem === formDataReadOnly.sourceSystem &&
            formData?.code === formDataReadOnly.code
          : false,
    }
  );

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>
            <FormField
              marginTopLabel={-20}
              error={isAlreadyExist?.nameAndCode}
              errorMessage="Name and Source System already exist"
              absolute
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <div style={{ marginBottom: 24 }}>
                  <FormField
                    label={<p style={{ marginBottom: 2 }}>Error Code:</p>}
                    error={vErrCode}
                    errorMessage="Min 2 and Max 10"
                  >
                    <TextField
                      name="code"
                      value={formData.code}
                      onChange={(e) => onChangeHandler("code", e)}
                      placeholder="#2121222"
                      style={{ width: "187.5px" }}
                    />
                  </FormField>
                </div>
                <div style={{ marginBottom: 24 }}>
                  <FormField
                    label={<p style={{ marginBottom: 2 }}>Source System :</p>}
                    error={vSourceSystem}
                    errorMessage="Min 2 and Max 20"
                  >
                    <TextField
                      value={formData.sourceSystem}
                      onChange={(e) => onChangeHandler("sourceSystem", e)}
                      placeholder="SBD_ID"
                      style={{ width: "187.5px" }}
                    />
                  </FormField>
                </div>
              </div>
            </FormField>
            <div style={{ marginBottom: 24 }}>
              <FormField
                label={<p style={{ marginBottom: 2 }}>In Indonesian:</p>}
                error={vIdn}
                errorMessage="Max 200 Characters"
              >
                <TextField
                  value={formData.idnMessage}
                  onChange={(e) => onChangeHandler("idnMessage", e)}
                  placeholder="Saldo Tidak Mencukupi"
                  style={{ width: "100%" }}
                />
              </FormField>
            </div>
            <div style={{ marginBottom: 24 }}>
              <FormField
                label={<p style={{ marginBottom: 2 }}>In English :</p>}
                error={vEng}
                errorMessage="Max 200 Characters"
              >
                <TextField
                  value={formData.engMessage}
                  onChange={(e) => onChangeHandler("engMessage", e)}
                  placeholder="Not enough Balance"
                  style={{ width: "100%" }}
                />
              </FormField>
            </div>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Cancel"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={() => {
                  clearForm();
                  handleClose();
                }}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Save"
                width="92px"
                height="40px"
                disabled={
                  !formData.code ||
                  !formData.engMessage ||
                  !formData.idnMessage ||
                  !formData.sourceSystem ||
                  isAlreadyExist?.nameAndCode ||
                  vErrCode ||
                  vSourceSystem ||
                  vIdn ||
                  vEng
                }
                onClick={() => {
                  dispatch(setTypeOpenModal(false));
                  if (rowData?.id) {
                    setOpenModalConfirmationEdit(true);
                  } else {
                    setOpenModalConfirmation(true);
                  }
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>

      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Add Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          clearForm();
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          addData();
        }}
      />

      <DeleteConfirmation
        title="Confirmation"
        message="Are You Sure To Edit Your Data?"
        submessage="You cannot undo this action"
        isOpen={openModalConfirmationEdit}
        loading={isLoadingExcute}
        handleClose={() => {
          clearForm();
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          addData();
        }}
      />

      <SuccessConfirmation
        isOpen={isOpenStore}
        handleClose={() => {
          dispatch(setTypeSuccess(false));
          clearForm();
        }}
      />
    </div>
  );
};

ErrorModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
};

ErrorModal.defaultProps = {
  onContinue: () => {},
  title: "Buat Pesan Error Baru",
};

export default ErrorModal;
