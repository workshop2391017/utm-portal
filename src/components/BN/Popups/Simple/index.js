// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  Backdrop,
  Fade,
  Box,
  Typography,
  Modal,
  makeStyles,
} from "@material-ui/core";

// components
import GeneralButton from "../../Button/GeneralButton";

// assets
import logo from "../../../../assets/icons/BN/exclamation-triangle.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& ::selection": {
      background: "#137EE1",
    },
  },
  paper: {
    width: 335,
    height: "auto",
    backgroundColor: "#fff",
    border: "none",
    padding: 20,
    alignItems: "center",
    borderRadius: 10,
  },
}));

// eslint-disable-next-line react/prop-types
const PopupNoData = ({
  isOpen,
  handleClose,
  label = "Tidak ada data yang tersedia di tabel",
  title,
}) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <p style={{ marginBottom: 20, textAlign: "center" }}>{label}</p>
            <GeneralButton
              label="Close"
              width="100%"
              height="40px"
              variant="contained"
              onClick={handleClose}
              className={classes.Button}
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupNoData.propTypes = {
  isOpen: PropTypes.bool,
  handleClose: PropTypes.func,
  title: PropTypes.string,
};

PopupNoData.defaultProps = {
  isOpen: false,
  handleClose: () => {},
  title: "",
};

export default PopupNoData;
