// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, SvgIcon } from "@material-ui/core";
import { Typography } from "antd";
// import Button from "@material-ui/core/Button";
import GeneralButton from "../../Button/GeneralButton";

// assets
import tabungan from "../../../../assets/images/BN/tabungan.png";
import chevronleft from "../../../../assets/icons/BN/chevron-left-white.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 490,
    minHeight: 490,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    // borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  paperlr: {
    width: "30px",
    position: "absolute",
    top: "30px",
    left: "30px",
  },
  textlr: {
    width: "30px",
    position: "absolute",
    top: "30px",
    left: "70px",
    color: "#fff",
    fontFamily: "FuturaHvBT",
    fontSize: 20,
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 40,
  },
}));

const TabunganPopup = ({ isOpen, handleClose, title, message }) => {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);

  useEffect(() => {
    if (title) {
      const temp = title.split("\\n");
      setTitles(temp);
    }
  }, [title]);

  useEffect(() => {
    if (isOpen) {
      setTimeout(() => {
        handleClose();
      }, 2500);
    }
  }, [handleClose, isOpen]);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div>
            <img className={classes.paperlr} src={chevronleft} alt="NPWP.jpg" />
            <p className={classes.textlr}>Tabungan.jpg</p>
            <div className={classes.paper}>
              <div className={classes.content}>
                <img
                  src={tabungan}
                  style={{
                    display: "flex",
                    width: "240px",
                    height: "320px",
                    alignContent: "center",
                    margin: "0px 0px 0px 90px",
                    //   marginTop: "40px",
                  }}
                  alt="tabungan"
                />
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

TabunganPopup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
};

TabunganPopup.defaultProps = {
  title: "Tabungan.jpg",
  message: "Preview Tabungan",
};

export default TabunganPopup;
