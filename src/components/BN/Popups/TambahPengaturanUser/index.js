// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  CircularProgress,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";

import useDebounce from "utils/helpers/useDebounce";

// Redux
import {
  getGroupByBranchId,
  handleGetAllBranch,
  handlegetAllRole,
  handleLdapUser,
  setClearUserData,
  setUserLdap,
  submitSettingUser,
} from "stores/actions/managementuser";
import { checkOnline } from "stores/actions/errorGeneral";
import CheckboxSingle from "components/SC/Checkbox/CheckboxSingle";

// components
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import { ipAddressInput, parseNumber, validateMinMax } from "utils/helpers";
import Select from "../../Select/SelectGroup";
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import TextField from "../../TextField/AntdTextField";
// import AddComment from "../AddComment";
import DeletePopup from "../Delete";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 696,
    minHeight: 420,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 40,
  },
  button: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 110,
  },
}));

const EditBillerKategori = ({ isOpen, handleClose, onContinue }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    isLoadingSubmit,
    isErrorLdap,
    isLoadingLdap,
    userLdap,
    roleData,
    branchData,
    dataGroupBranchId,
    userData,
    isLoadingGroupBranch,
  } = useSelector((data) => ({
    olistBranch: data.managementuser?.userAdminPaging?.olistBranch ?? [],
    olistRole: data.managementuser?.userAdminPaging?.olistRole ?? [],
    isLoadingSubmit: data.managementuser?.isLoadingSubmit,
    userLdap: data.managementuser?.userLdap,
    isLoadingLdap: data.managementuser?.isLoadingLdap,
    isErrorLdap: data.managementuser?.isErrorLdap,
    roleData: data.managementuser?.role,
    branchData: data.managementuser?.branch,
    dataGroupBranchId: data.managementuser?.dataGroupBranchId,
    isLoadingGruoupBranch: data.managementuser?.isLoadingGruoupBranch,
    userData: data?.managementuser?.userData,
    isLoadingGroupBranch: data?.managementuser?.isLoadingGroupBranch,
  }));

  const offices = (branchData?.olistBranch ?? [])
    .sort((a, b) => {
      const ba = a.branchName?.toLowerCase()?.trim();
      const bb = b.branchName?.toLowerCase()?.trim();
      if (ba < bb) {
        return -1;
      }
      if (ba > bb) {
        return 1;
      }
      return 0;
    })
    .map((elm, i) => ({
      id: i,
      key: i,
      value: elm?.branchCode,
      label: `${elm?.branchCode} - ${elm?.branchName?.trim()} - ${
        elm?.officeType
      }`,
      ...elm,
    }));

  const [username, setUsername] = useState(null);
  const [nameUser, setNameUser] = useState(null);
  const [email, setEmail] = useState(null);
  const [nip, setNip] = useState(null);
  const [branch, setBranch] = useState(null);
  const [role, setRole] = useState(null);
  const [group, setGroup] = useState(null);
  const [ipAddress, setIpAddress] = useState("");
  const [isIpCheck, setIsIpCheck] = useState(false);
  // const [modalComm, setModalComm] = useState(false);
  const [comment, setComment] = useState(null);
  const [modalConf, setModalConf] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);

  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);

  const [officeType, setOfficetype] = useState(null);
  const isEdit = userData?.idUser;

  const fullNameSearch = useDebounce(username, 1000);

  const selectedOffices = offices?.find((e) => e.branchCode === branch);

  useEffect(() => {
    if (Object.values(userData).filter((n) => n).length > 0) {
      setUsername(userData?.username);
      setNameUser(userData?.name);
      setEmail(userData?.email);
      setNip(userData?.nip);
      setBranch(userData?.branchCode);
      setRole(userData?.roleId);
      setGroup(userData?.portalGroupId);
      setIpAddress(userData?.ipAddress);
      setIsIpCheck(userData?.isIpCheck);
      setOfficetype(
        (branchData?.olistBranch ?? []).find(
          (a) => a?.branchCode === userData?.branchCode
        )?.officeType
      );
    }
  }, [userData]);

  const handleClearState = () => {
    setUsername(null);
    setNameUser(null);
    setEmail(null);
    setNip(null);
    setBranch(null);
    setRole(null);
    setGroup(null);
    setIpAddress(null);
    setIsIpCheck(false);
  };

  const handleNext = () => {
    dispatch(setClearUserData());
    handleClearState();
    // setModalComm(false);
    setComment(null);
    setModalConf(false);
    handleClose();
  };

  const handleSuccess = () => {
    setModalSuccess(true);
  };

  const handleCloseOnError = () => {
    setModalConf(false);
    handleClose();
  };

  const handleNewUser = async () => {
    await dispatch(checkOnline(dispatch));
    const payload = {
      ...(isEdit && {
        idUser: userData?.idUser,
        requestComment: comment,
      }),
      username,
      name: nameUser,
      email,
      nip: nip || "",
      branchCode: branch,
      role,
      portalGroupId: group,
      isIpCheck,
      ipAddress,
    };

    dispatch(
      submitSettingUser(
        payload,
        {
          name: isEdit ? null : username,
          id: isEdit ? userData?.idUser : null,
        },
        handleNext,
        handleSuccess,
        handleCloseOnError
      )
    );
  };

  useEffect(() => {
    if (fullNameSearch && !isEdit) dispatch(handleLdapUser(fullNameSearch));
  }, [fullNameSearch]);

  useEffect(() => {
    if (userLdap) {
      setEmail(userLdap?.email);
      setNameUser(userLdap?.fullName);
    }
  }, [userLdap]);

  useEffect(() => {
    dispatch(
      handlegetAllRole({
        roleName: "admin",
      })
    );
    dispatch(handleGetAllBranch());
  }, []);

  useEffect(() => {
    if (officeType) dispatch(getGroupByBranchId({ officeType }));
  }, [officeType]);

  const handleChangeOffice = (e) => {
    const fi = offices.find((fe) => fe.value === e);

    setOfficetype(fi?.officeType);
    setBranch(fi?.branchCode);
    setGroup(null);
  };

  const handleUpperCase = ([a, ...rest]) => [a.toUpperCase(), ...rest].join("");

  const vIpAddress = !validateMinMax(ipAddress, 7, 15);

  return (
    <div>
      {/* <AddComment
        isOpen={modalComm}
        value={comment}
        handleClose={() => {
          setModalComm(false);
          setComment(null);
        }}
        onContinue={(a, b) => {
          setComment(b);
          setModalConf(true);
        }}
        title="Comment Edit"
        loading={isLoadingSubmit}
      /> */}

      <DeletePopup
        isOpen={modalConf}
        handleClose={() => {
          setModalConf(false);
          // setModalComm(false);
        }}
        onContinue={handleNewUser}
        loading={isLoadingSubmit}
        title="Confirmation"
        message="Are You Sure To Edit Data?"
      />

      <SuccessConfirmation
        isOpen={modalSuccess}
        handleClose={() => {
          setModalConf(false);
          setModalSuccess(false);
          handleClearState();
          dispatch(setClearUserData());
        }}
      />

      <DeletePopup
        isOpen={openModalConfirmation}
        handleClose={() => setOpenModalConfirmation(false)}
        onContinue={() => {
          handleNewUser();
          setOpenModalConfirmation(false);
        }}
        loading={isLoadingSubmit}
        title="Confirmation"
        message="Are You Sure to Add the Data?"
      />

      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={() => {
          handleClearState();
          handleClose();
          dispatch(setClearUserData());
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
        disableEnforceFocus
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{isEdit ? "Edit" : "Add"} User</h1>
            <div style={{ display: "flex" }}>
              <div style={{ width: "50%", paddingRight: 10 }}>
                <div style={{ marginBottom: 24, position: "relative" }}>
                  <p
                    style={{
                      marginBottom: 2,
                      color: isErrorLdap ? "#D14848" : "",
                    }}
                  >
                    Username :
                  </p>
                  <TextField
                    disabled={isEdit}
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="Username"
                    style={{
                      width: 300,
                      height: 40,
                      borderColor: isErrorLdap ? "#D14848" : "",
                    }}
                    prefix={
                      isLoadingLdap && (
                        <CircularProgress size={20} color="primary" />
                      )
                    }
                  />
                  {isErrorLdap ? (
                    <span
                      style={{
                        color: "#D14848",
                        display: "block",
                        position: "absolute",
                      }}
                    >
                      Username not found
                    </span>
                  ) : (
                    ""
                  )}
                </div>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Email :</p>
                  <TextField
                    value={email}
                    disabled={email || isEdit}
                    placeholder="xyz@example.com"
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div className={classes.input} style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 3 }}>Office :</p>

                  <SelectWithSearch
                    value={selectedOffices}
                    placeholder="Select Office"
                    options={offices}
                    searchKey="branchName"
                    onChange={handleChangeOffice}
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div className={classes.input}>
                  <p style={{ marginBottom: 3 }}>Role :</p>
                  <Select
                    disabled={isLoadingGroupBranch}
                    value={role}
                    onChange={(value) => setRole(value)}
                    placeholder="Select Role"
                    options={[
                      {
                        name: "Select Role",
                        options: (roleData?.olistRole ?? []).map((elm) => ({
                          name:
                            elm?.roleName
                              ?.replaceAll("_", " ")
                              ?.toLowerCase()
                              ?.split(" ")
                              ?.map((m) => handleUpperCase(m))
                              ?.join(" ") || "-",
                          id: elm?.roleId,
                        })),
                      },
                    ]}
                    style={{ width: 300, height: 40 }}
                  />
                </div>
              </div>
              <div style={{ width: "50%", paddingLeft: 10 }}>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>Name :</p>
                  <TextField
                    value={nameUser}
                    disabled={nameUser || isEdit}
                    placeholder="Full Name"
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 2 }}>NIP :</p>
                  <TextField
                    value={nip}
                    disabled={isEdit}
                    onChange={(e) => {
                      const value = e.target.value;
                      setNip(parseNumber(value));
                    }}
                    placeholder="192019928812"
                    style={{ width: 300, height: 40 }}
                    type="text"
                    maxLength={25}
                  />
                </div>
                <div className={classes.input} style={{ marginBottom: 24 }}>
                  <p style={{ marginBottom: 3 }}>Group :</p>
                  <Select
                    disabled={!officeType}
                    value={group}
                    onChange={(value) => setGroup(value)}
                    placeholder="Select Group"
                    options={[
                      {
                        name: "Select Group",
                        options: (dataGroupBranchId?.portalGroupList ?? []).map(
                          (elm) => ({
                            name: elm?.name,
                            id: elm?.id,
                          })
                        ),
                      },
                    ]}
                    style={{ width: 300, height: 40 }}
                  />
                </div>
                <div className={classes.input}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "flex-end",
                      justifyContent: "space-between",
                    }}
                  >
                    <div>
                      <CheckboxSingle
                        checked={isIpCheck}
                        label="Ip Checking"
                        onChange={() => {
                          setIsIpCheck(!isIpCheck);
                          if (isIpCheck) setIpAddress(null);
                        }}
                      />
                    </div>
                    <div>
                      <p style={{ marginBottom: 3 }}>IP Address :</p>
                      <TextField
                        disabled={!isIpCheck}
                        value={ipAddress}
                        onChange={(e) =>
                          setIpAddress(ipAddressInput(e.target.value))
                        }
                        placeholder="182."
                        style={{
                          width: 180,
                          height: 40,
                          borderColor: vIpAddress ? "#D14848" : "",
                        }}
                        e
                      />
                      {vIpAddress ? (
                        <span
                          style={{
                            color: "#D14848",
                            display: "block",
                            position: "absolute",
                          }}
                        >
                          Min 7 and Max 15 Characters
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.button}>
              <ButtonOutlined
                label="Cancel"
                width={157.5}
                height="40px"
                color="#0061A7"
                onClick={() => {
                  handleClose();
                  handleClearState();
                  dispatch(setClearUserData());
                  dispatch(setUserLdap({}));
                }}
                disabled={isLoadingSubmit}
              />

              <GeneralButton
                label={
                  isLoadingSubmit ? (
                    <CircularProgress size={20} color="primary" />
                  ) : (
                    "Save"
                  )
                }
                width={157.5}
                height="40px"
                onClick={() => {
                  if (isEdit) {
                    // setModalComm(true);
                    setModalConf(true);
                    handleClose();
                  } else setOpenModalConfirmation(true);
                }}
                disabled={
                  isLoadingSubmit ||
                  isLoadingLdap ||
                  isErrorLdap ||
                  !username ||
                  !branch ||
                  !role ||
                  !nip ||
                  !group ||
                  vIpAddress ||
                  (isIpCheck && !ipAddress)
                }
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

EditBillerKategori.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
};

EditBillerKategori.defaultProps = { onContinue: () => {} };

export default EditBillerKategori;
