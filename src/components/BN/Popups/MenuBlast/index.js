// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Modal,
  Fade,
  Backdrop,
  SvgIcon,
  Paper,
} from "@material-ui/core";
import { Typography } from "antd";
import Colors from "helpers/colors";
import { ReactComponent as XIcon } from "assets/icons/BN/close-blue.svg";
import { ReactComponent as FrameTag } from "assets/icons/BN/frametag.svg";
import { ReactComponent as FrameMessage } from "assets/icons/BN/framemessage.svg";
import ScrollCustom from "components/BN/ScrollCustom";
import GeneralButton from "../../Button/GeneralButton";

// assets
import check from "../../../../assets/images/BN/illustration.png";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    height: 495,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
    textAlign: "center",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "FuturaMdBT",
    fontSize: 36,
    fontWeight: 900,
    color: Colors.dark.hard,
  },
  xContainer: {
    display: "flex",
    justifyContent: "flex-end",
    "& .close": {
      "& :hover": {
        cursor: "pointer",
      },
    },
  },
}));

const MenuBlast = ({
  isOpen,
  handleClose,
  handleBlastPesan,
  handleBlastPromo,
}) => {
  const classes = useStyles();
  const [click, setClick] = useState(true);
  const handleClick = () => {
    setClick(true);
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        // BackdropProps={{
        //   timeout: 900,
        // }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div>
              <h1
                style={{
                  fontFamily: "FuturaHvBT",
                  fontWeight: 400,
                  fontSize: 32,
                  letterSpacing: "0.03em",
                  color: "#374062",
                }}
              >
                Menu Blast
              </h1>
            </div>
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: " center",
                marginTop: "auto",
                height: 305,
                alignItems: "center",
                flexDirection: "column",
              }}
            >
              <div
                style={{
                  width: 400,
                  height: 75,
                  display: "flex",
                  border: "1px solid #BCC8E7",
                  borderRadius: 10,
                  padding: "6px 6px",
                  justifyContent: "space-between",
                  background: click ? "#BCC8E7" : "#FFFFFF",
                }}
              >
                <FrameTag
                  style={{ cursor: "pointer" }}
                  onClick={() => setClick(true)}
                />
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "100%",
                    alignItems: "flex-start",
                    paddingLeft: "12px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "FuturaHvBT",
                      fontWeight: 400,
                      fontSize: 13,
                      letterSpacing: "0.01em",
                      color: "#44495B",
                    }}
                  >
                    Blast Promo
                  </span>
                  <p
                    style={{
                      fontFamily: "FuturaBKBT",
                      fontWeight: 400,
                      fontSize: 11,
                      letterSpacing: "0.03em",
                      color: "#44495B",
                      textAlign: "start",
                    }}
                  >
                    Menu untuk melakukan blast promo kepada nasabah <br /> dari
                    list promo yang sudah tersedia
                  </p>
                </div>
              </div>
              <div
                style={{
                  width: 400,
                  height: 75,
                  display: "flex",
                  border: "1px solid #BCC8E7",
                  borderRadius: 10,
                  padding: "6px 6px",
                  justifyContent: "space-between",
                  margin: "24px auto",
                  background: click ? "#FFFFFF" : "#BCC8E7",
                }}
              >
                <FrameMessage
                  style={{ cursor: "pointer" }}
                  onClick={() => setClick(false)}
                />
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "100%",
                    alignItems: "flex-start",
                    paddingLeft: "12px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "FuturaHvBT",
                      fontWeight: 400,
                      fontSize: 13,
                      letterSpacing: "0.01em",
                      color: "#44495B",
                    }}
                  >
                    Blast Pesan
                  </span>
                  <p
                    style={{
                      fontFamily: "FuturaBKBT",
                      fontWeight: 400,
                      fontSize: 11,
                      letterSpacing: "0.03em",
                      color: "#44495B",
                      textAlign: "start",
                    }}
                  >
                    Menu untuk melakukan blast pesan kepada nasabah,
                    <br /> terdiri dari pesan informasi dan pesan promo
                  </p>
                </div>
              </div>
            </div>
            <GeneralButton
              onClick={() => {
                if (click) {
                  handleBlastPromo();
                } else {
                  handleBlastPesan();
                }
                handleClose();
              }}
              label="OK"
              width="380px"
              height="44px"
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

MenuBlast.propTypes = {};

MenuBlast.defaultProps = {
  title: "Berhasil",
  message:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem faucibus odio massa pellentesque tellus augue enim. Mattis id iaculis ullamcorper erat sit. Fames elementum leo vitae habitant quam posuere potenti purus lectus. Pellentesque leo convallis sed mattis. Aliquam aliquam morbi vel elit cursus. Convallis aliquam id turpis vitae id felis feugiat laoreet amet. Libero, varius aliquet arcu nisi. Lacus volutpat, id mi tincidunt suscipit nullam egestas penatibus velit. In accumsan, aliquet enim felis diam scelerisque tellus. Mauris ornare scelerisque curabitur gravida enim quis molestie nulla arcu. Aliquam, vitae convallis cras habitant tellus. Etiam pellentesque pellentesque lectus cursus nunc.In ipsum elit aliquet interdum orci tincidunt. Velit phasellus sed interdum augue in egestas. Magna cras in commodo facilisis. Nulla egestas quam sit id risus sapien pharetra tristique. Cras massa elit.",
  height: 495,
};

export default MenuBlast;
