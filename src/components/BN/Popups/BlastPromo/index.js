// main
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Modal, Fade, Backdrop, Grid } from "@material-ui/core";

// components
import CheckboxGroup from "components/BN/Checkbox/ChexboxGroup";
import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import SuccessConfirmation from "../SuccessConfirmation";
import Select from "../../Select/AntdSelect";
import Date from "../../DatePicker/GeneralDatePicker";
import AddComment from "../AddComment";

// assets
import Trash from "../../../../assets/icons/BN/trash-blue.svg";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 800,
    minHeight: 200,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "28px 30px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 20,
    fontWeight: 900,
    marginBottom: 18,
  },
  button: {
    position: "absolute",
    bottom: 30,
  },
  scrolled: {
    border: "1px solid #D3DCF4",
    borderRadius: 8,
    minHeight: 200,
    maxHeight: 270,
    padding: "10px 0px",
    marginBottom: "20px",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
}));

const BlastPromo = ({ isOpen, handleClose, onContinue, label }) => {
  const classes = useStyles();
  const [checkboxGroupA, setCheckboxGroupA] = useState(null);
  const [checkboxGroupB, setCheckboxGroupB] = useState(null);
  const [judulPromo, setJudulPromo] = useState("Promo Kemerdekaan");
  const [dateValue, setDateValue] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  const [addComment, setAddComment] = useState(false);

  const handleOpenComment = () => {
    handleClose(true);
    setAddComment(true);
  };
  const handleOpenConfirm = () => {
    // setAddComment(false);
    // setConfirmSuccess(true);
  };

  return (
    <div>
      {/* <AddComment
        isOpen={addComment}
        handleClose={() => setAddComment(false)}
        onContinue={handleOpenConfirm}
      /> */}
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
        message="Blast Promo Dijadwalkan"
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{label}</h1>
            <Grid container spacing={2} style={{ marginBottom: 40 }}>
              <Grid item xs={6} style={{ paddingRight: 16 }}>
                <p style={{ marginBottom: 2 }}>Pilih Judul Promo :</p>
                <Select
                  value={judulPromo}
                  onChange={(value) => setJudulPromo(value)}
                  placeholder="Judul Promo"
                  options={[
                    "Promo Kemerdekaan",
                    "Promo Akhir Tahun",
                    "Promo Ulang Tahun",
                  ]}
                  style={{ width: "100%" }}
                />
              </Grid>
              <Grid item xs={4} style={{ paddingLeft: 16 }}>
                <p style={{ marginBottom: 2 }}>Tgl Blast :</p>
                <Date
                  placeholder="Pilih tanggal Blast"
                  value={dateValue}
                  onChange={(e) => setDateValue(e)}
                  style={{ width: "100%", borderRadius: 10 }}
                />
              </Grid>
              <Grid item xs={2} style={{ paddingTop: "30px" }}>
                <GeneralButton
                  label="Tambah"
                  width="100%"
                  height="40px"
                  onClick={handleOpenConfirm}
                />
              </Grid>
              <Grid item xs={6} style={{ paddingRight: 16 }}>
                <p style={{ marginBottom: 2 }}>Jadwal Blast Promo :</p>
                <div className={classes.scrolled}>
                  <div>
                    <div>
                      {[
                        "12/12/2021, 10:00 AM",
                        "11/12/2021, 22:10 AM",
                        "11/12/2021, 10:20 AM",
                        "10/12/2021, 12:00 AM",
                        "9/12/2021, 11:12 AM",
                        "8/12/2021, 13:15 AM",
                        "7/12/2021, 22:00 AM",
                        "7/12/2021, 10:00 AM",
                        "6/12/2021, 02:20 AM",
                        "06/12/2021, 04:31 AM",
                        "30/11/2021, 22:00 AM",
                        "12/12/2021, 10:00 AM",
                        "12/12/2021, 10:00 AM",
                        "12/12/2021, 10:00 AM",
                      ].map((item, i) => (
                        <div style={{ marginTop: i === 0 ? 0 : 12 }}>
                          <p
                            style={{ display: "inline-block", marginLeft: 20 }}
                          >
                            {item}
                          </p>
                          <img
                            src={Trash}
                            alt="trash-icon"
                            style={{
                              cursor: "pointer",
                              display: "inline-block",
                              verticalAlign: "top",
                              float: "right",
                              marginRight: 10,
                            }}
                          />
                          <div
                            style={{
                              borderBottom: "1px solid #D3DCF4",
                              marginTop: "12px",
                              width: "100%",
                            }}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </Grid>
              <Grid item xs={6} style={{ paddingLeft: 16 }}>
                <p style={{ marginBottom: 2 }}>
                  Blast Promo Pada Waktu Yang Dipilih:
                </p>

                <div className={classes.scrolled}>
                  <div>
                    <div>
                      {[
                        "12/12/2021, 10:00 AM",
                        "11/12/2021, 22:10 AM",
                        "11/12/2021, 10:20 AM",
                        "10/12/2021, 12:00 AM",
                        "9/12/2021, 11:12 AM",
                        "8/12/2021, 13:15 AM",
                        "7/12/2021, 22:00 AM",
                        "7/12/2021, 10:00 AM",
                        "6/12/2021, 02:20 AM",
                        "06/12/2021, 04:31 AM",
                        "30/11/2021, 22:00 AM",
                        "12/12/2021, 10:00 AM",
                        "12/12/2021, 10:00 AM",
                        "12/12/2021, 10:00 AM",
                      ].map((item, i) => (
                        <div style={{ marginTop: i === 0 ? 0 : 12 }}>
                          <p style={{ marginLeft: 20 }}>Promo Lebaran</p>
                          <p style={{ color: "#AEB3C6", marginLeft: 20 }}>
                            {item}
                          </p>
                          <div
                            style={{
                              borderBottom: "1px solid #D3DCF4",
                              marginTop: "12px",
                              width: "100%",
                            }}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </Grid>
              <Grid item xs={12}>
                <p style={{ marginBottom: 2 }}>Pilih Media Promo :</p>
                <CheckboxGroup
                  value={checkboxGroupA}
                  options={["Aplikasi", "SMS"]}
                  onChange={(e) => setCheckboxGroupA(e)}
                  margin="20px"
                  width="250px"
                />
                <CheckboxGroup
                  value={checkboxGroupB}
                  options={["Email", "Whatsapp"]}
                  onChange={(e) => setCheckboxGroupB(e)}
                  margin="20px 20px 20px 20px"
                  width="250px"
                />
              </Grid>
            </Grid>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Batal"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={handleClose}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Jadwalkan"
                width="92px"
                height="40px"
                onClick={() => {
                  handleClose();
                  setConfirmSuccess(true);
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

BlastPromo.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  label: PropTypes.string.isRequired,
};

BlastPromo.defaultProps = { onContinue: () => {} };

export default BlastPromo;
