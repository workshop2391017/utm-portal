// main
// libraries
import { Backdrop, Fade, makeStyles, Modal } from "@material-ui/core";
import FormField from "components/BN/Form/InputGroupValidation";
import SelectWithSearch from "components/BN/Select/SelectWithSearch";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  addDataHandlingOfficer,
  editDataHandlingOfficer,
  handleGetAllBranch,
  setTypeIsOpen,
  setTypeOpenModal,
} from "stores/actions/handlingOfficer";
import { confValidateMenuName } from "stores/actions/validatedatalms";
import {
  emailRegex,
  formatPhoneNumber,
  parseFullName,
  parseNum,
  parseTextNumFirstSpace,
  parseUsernameHandlingOfficer,
  validateMinMax,
} from "utils/helpers";
import { useValidateLms } from "utils/helpers/validateLms";
import ButtonOutlined from "../../Button/ButtonOutlined";
// components
import GeneralButton from "../../Button/GeneralButton";
import RadioGroup from "../../Radio/RadioGroup";
import TextField from "../../TextField/AntdTextField";
import DeleteConfirmation from "../Delete";
import SuccessConfirmation from "../SuccessConfirmation";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 480,
    minHeight: 580,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: "30px 40px",
    alignItems: "center",
    borderRadius: 8,
    position: "relative",
  },
  subtitle: {
    fontFamily: "FuturaBkBT",
    fontSize: 17,
    color: "#374062",
    marginBottom: 25,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 32,
    fontWeight: "normal",
    marginBottom: 24.28,
    textAlign: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    marginTop: 71,
  },
  errorInput: {
    height: "auto",
    fontFamily: "FuturaBkBT",
    fontWeight: 400,
    fontSize: "13px",
    color: "#FF6F6F",
    width: "320px",
    transition: "0.3s",
  },
}));

const HandlingOfficer = ({
  isOpen,
  handleClose,
  onContinue,
  title,
  rowData,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openModalConfirmation, setOpenModalConfirmation] = useState(false);
  const [validationEmail, setValidationEmail] = useState(false);
  const { isAlreadyExist, isLoadingValidate } = useSelector(
    (e) => e.validatedatalms
  );
  const {
    data,
    isLoading,
    error,
    isOpenModal,
    openModal,
    isLoadingExcute,
    branch,
  } = useSelector((state) => state.handlingOfficer);

  const offices = (branch?.olistBranch ?? [])
    .sort((a, b) => {
      const ba = a.branchName?.toLowerCase()?.trim();
      const bb = b.branchName?.toLowerCase()?.trim();
      if (ba < bb) {
        return -1;
      }
      if (ba > bb) {
        return 1;
      }
      return 0;
    })
    .map((elm, i) => ({
      id: i,
      key: i,
      value: elm?.branchName,
      label: `${elm?.branchCode} - ${elm?.branchName?.trim()} - ${
        elm?.officeType
      }`,
      ...elm,
    }));

  const [formData, setFormData] = useState({
    email: "",
    location: "",
    officerId: "",
    phoneNumber: "",
    username: "",
  });
  const [formDataReadOnly, setFormDataReadOnly] = useState({
    officerId: "",
  });

  // validate already exist
  useValidateLms(
    {
      code: formData.officerId,
      codeCode: confValidateMenuName.HANDLING_OFFICER_ID_CODE,
    },
    null,
    { pause: formDataReadOnly?.officerId === formData.officerId }
  );

  const validateOfficerId = validateMinMax(formData?.officerId, 2, 8);
  const vUserName = !validateMinMax(formData?.username, 2, 50);
  const vPhone = !validateMinMax(formData?.phoneNumber, 10, 13);
  const vEmail = !validateMinMax(formData?.email, 1, 50);

  const selectedOffices = offices?.find(
    (e) => e.branchName === formData?.location
  );

  const clearForm = () => {
    setFormDataReadOnly({ officerId: null });
    setFormData({
      id: null,
      email: null,
      location: null,
      officerId: null,
      phoneNumber: null,
      username: null,
    });
  };
  useEffect(() => {
    if (rowData) {
      setFormDataReadOnly({ officerId: rowData?.officerId });
      setFormData({
        id: rowData?.id,
        email: rowData?.email,
        location: rowData?.location,
        officerId: rowData?.officerId,
        phoneNumber: rowData?.phoneNumber,
        username: rowData?.username,
      });
    }

    dispatch(handleGetAllBranch());
  }, [rowData]);

  const handleNext = () => {
    handleClose();
    clearForm();
    setOpenModalConfirmation(false);
  };

  const onSaveData = () => {
    if (rowData?.id) {
      dispatch(
        editDataHandlingOfficer(
          formData,
          { id: rowData?.id, name: null, code: null },
          handleNext
        )
      );
    } else {
      dispatch(
        addDataHandlingOfficer(
          formData,
          { code: formData.officerId, id: null },
          handleNext
        )
      );
    }
  };

  useEffect(() => {
    if (formData.email !== "") {
      const regex = RegExp("[a-z0-9]+@[a-z]+[.]+[a-z]{2,3}");
      const validation = regex.test(formData.email);
      if (validation) {
        setValidationEmail(false);
      } else {
        setValidationEmail(true);
      }
    }
  }, [formData.email]);

  return (
    <div>
      <DeleteConfirmation
        title="Confirmation"
        message={
          rowData !== null
            ? "Are You Sure To Edit Your Data?"
            : "Are You Sure To Add Data?"
        }
        submessage="You cannot undo this action"
        isOpen={openModalConfirmation}
        loading={isLoadingExcute}
        handleClose={() => {
          setOpenModalConfirmation(false);
        }}
        onContinue={() => {
          handleClose();
          clearForm();
          onSaveData();
        }}
      />

      <SuccessConfirmation
        isOpen={isOpenModal}
        handleClose={() => {
          dispatch(setTypeIsOpen(false));
          setOpenModalConfirmation(false);
          setFormData({
            id: null,
            email: "",
            location: "",
            officerId: "",
            phoneNumber: "",
            username: "",
          });
        }}
      />
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 900,
        }}
        disableEnforceFocus
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <h1 className={classes.title}>{title}</h1>
            <div className={classes.subtitle}>New Officer</div>
            <div style={{ marginBottom: validateOfficerId ? 25 : 4.5 }}>
              <FormField
                label={<p style={{ marginBottom: 2 }}>Officer ID :</p>}
                error={isAlreadyExist.code || !validateOfficerId}
                errorMessage={
                  isAlreadyExist.code
                    ? "Officer ID already exist"
                    : "Min 2 and Max 8 Characters"
                }
              >
                <TextField
                  value={formData.officerId}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      officerId: parseTextNumFirstSpace(e.target.value),
                    })
                  }
                  placeholder="12345678"
                  style={{ width: "100%" }}
                />
              </FormField>
            </div>
            <div
              style={{
                marginTop:
                  !validateOfficerId || isAlreadyExist.code ? 25 : null,
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <div style={{ marginBottom: 25 }}>
                <FormField
                  label={<p style={{ marginBottom: 2 }}>Username :</p>}
                  error={vUserName}
                  errorMessage="Min 2 and Max 50 Characters"
                >
                  <TextField
                    value={formData.username}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        username: parseUsernameHandlingOfficer(e.target.value),
                      })
                    }
                    placeholder="emanual_11928"
                    style={{ width: "187.5px" }}
                  />
                </FormField>
              </div>
              <div style={{ marginBottom: 25 }}>
                <p style={{ marginBottom: 2 }}>Branch :</p>
                <SelectWithSearch
                  value={selectedOffices || null}
                  placeholder="Select Branch"
                  options={offices}
                  searchKey="branchName"
                  onChange={(e) => setFormData({ ...formData, location: e })}
                  style={{ width: 187.5, height: 40 }}
                />
                {/* <TextField
                  value={formData.location}
                  onChange={(e) =>
                    setFormData({ ...formData, location: e.target.value })
                  }
                  placeholder="Jakarta Selatan"
                  style={{ width: "187.5px" }}
                /> */}
              </div>
            </div>
            <div style={{ marginBottom: 25 }}>
              <FormField
                label={<p style={{ marginBottom: 2 }}>Email :</p>}
                error={
                  (formData.email && !emailRegex(formData.email)) || vEmail
                }
                errorMessage={
                  vEmail
                    ? "Max 50 Characters"
                    : `${formData.email} is not valid`
                }
              >
                <TextField
                  value={formData.email}
                  onChange={(e) =>
                    setFormData({ ...formData, email: e.target.value })
                  }
                  placeholder="emanuel_12@gmail.com"
                  style={{ width: "100%" }}
                />
              </FormField>
            </div>
            <div style={{ marginBottom: 25 }}>
              <FormField
                label={<p style={{ marginBottom: 2 }}>Phone :</p>}
                error={vPhone}
                errorMessage="Min 10 and Max 13 Characters"
              >
                <TextField
                  value={
                    formData.phoneNumber
                      ? formatPhoneNumber(
                          formData.phoneNumber.replace(/[^0-9]/g, "") || "",
                          Infinity
                        )
                      : formData.phoneNumber
                  }
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      phoneNumber: parseNum(e.target.value),
                    })
                  }
                  placeholder="081234567890"
                  style={{ width: "100%" }}
                />
              </FormField>
            </div>
            <div className={classes.button} style={{ left: 30 }}>
              <ButtonOutlined
                label="Cancel"
                width="76px"
                height="40px"
                color="#0061A7"
                onClick={() => {
                  handleClose();
                  clearForm();
                }}
              />
            </div>
            <div className={classes.button} style={{ right: 30 }}>
              <GeneralButton
                label="Save"
                width="92px"
                height="40px"
                disabled={
                  !formData.email ||
                  !formData.location ||
                  !formData.officerId ||
                  !formData.phoneNumber ||
                  !formData.username ||
                  !validateOfficerId ||
                  !emailRegex(formData.email) ||
                  vEmail ||
                  vUserName ||
                  vPhone ||
                  isAlreadyExist.code ||
                  isLoadingValidate
                }
                onClick={() => {
                  setOpenModalConfirmation(true);
                }}
              />
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

HandlingOfficer.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onContinue: PropTypes.func,
  title: PropTypes.string,
};

HandlingOfficer.defaultProps = {
  onContinue: () => {},
  title: "Add Officer",
};

export default HandlingOfficer;
