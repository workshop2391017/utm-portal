import { Modal, Typography, Box } from "@material-ui/core";

const CustomComponent = (props) => {
  const { namaDepan, namaBelakang, hobi, isOpen, handleClose } = props;

  return (
    <Modal open={isOpen} onClose={handleClose}>
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 500,
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          borderRadius: "10px",
          gap: "10px",
        }}
      >
        <Typography
          id="modal-modal-title"
          variant="h6"
          component="h2"
          color="primary"
        >
          Attention
        </Typography>
        <Typography id="modal-modal-description" sx={{ mt: 5 }}>
          {`Data dengan nama ${namaDepan} ${namaBelakang} dan hobi ${hobi}`}
        </Typography>
        <Typography
          id="modal-modal-title"
          variant="h6"
          component="h2"
          sx={{ mt: 5 }}
          color="green"
        >
          Berhasil Disimpan
        </Typography>
      </Box>
    </Modal>
  );
};

export default CustomComponent;
