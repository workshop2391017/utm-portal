// main
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

// libraries
import {
  makeStyles,
  IconButton,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { pathnameCONFIG } from "configuration";

import { useDispatch } from "react-redux";
import {
  setIdCompany,
  setSelectedCompany,
} from "stores/actions/managementPerusahaan/managmentCompany";
import { setLocalStorage } from "utils/helpers";
import SuccessConfirmation from "../../Popups/SuccessConfirmation";

// assets
import rotate from "../../../../assets/icons/BN/rotate-ccw.svg";
import menu from "../../../../assets/icons/BN/ellipsis-horizontal.svg";
import edit from "../../../../assets/icons/BN/edit-2.svg";
import eye from "../../../../assets/icons/BN/eye.svg";

const menuOptions = [
  {
    name: "Company Details",
    icon: eye,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.DETAIL_COMPANY,
  },
  {
    name: "Group Settings",
    icon: edit,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.PENGATURAN_KELOMPOK,
  },
  {
    name: "User Settings",
    icon: edit,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.USER_SETTING,
  },
  {
    name: "Financial Matrix",
    icon: edit,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.PENGATURAN_MATRIX_FINANCIAL.LIST,
  },
  {
    name: "Non-Financial Matrix",
    icon: edit,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.PENGATURAN_MATRIX_NON_FINANCIAL.LIST,
  },
  {
    name: "Level Setting",
    icon: edit,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.PENGATURAN_LEVEL.LIST,
  },
];

const menuOptionsSingle = [
  {
    name: "Company Details",
    icon: eye,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.DETAIL_COMPANY,
  },
  {
    name: "User Settings",
    icon: edit,
    url: pathnameCONFIG.MANAGEMENT_COMPANY.USER_SETTING_DETAIL_ADMIN,
  },
];

const MenuTable = (props) => {
  const { dataDetail } = props;

  const [menuOptionState, setMenuOptions] = useState([]);

  useEffect(() => {
    if (dataDetail?.otorisasi === "SINGLE") {
      setMenuOptions(menuOptionsSingle);
    } else {
      setMenuOptions(menuOptions);
    }
  }, [dataDetail]);

  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    rotateButton: {
      padding: 7,
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            // position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      // position: "absolute",
      // right: 10,
      // top: "50%",
      // transform: "translateY(-50%)",
    },
    MenuItem: {
      width: "340px",
    },
    //   didnt use
  }));

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const [successConfirmModal, setSuccessConfirmModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDetail = (url, dataDetail) => {
    dispatch(setIdCompany(dataDetail?.id));

    const objectData = {
      id: dataDetail?.id,
      otorisasi: dataDetail?.otorisasi,
      corporateAdmin: dataDetail?.corporateAdmin,
      userProfileOwnerId: dataDetail?.userProfileOwnerId,
    };

    const dataLocal = JSON.stringify(objectData);
    setLocalStorage("idPerusahaan", dataLocal);

    dispatch(setSelectedCompany(dataDetail));
    history.push(url);
    document.scrollingElement.scrollTop = 0;
  };

  const status = 2;

  return (
    <div className={classes.menu}>
      <SuccessConfirmation
        isOpen={successConfirmModal}
        handleClose={() => setSuccessConfirmModal(false)}
      />

      {status === 1 ? (
        <IconButton
          className={classes.rotateButton}
          color="primary"
          onClick={() => setSuccessConfirmModal(true)}
        >
          <img src={rotate} alt="Rotate Icon" />
        </IconButton>
      ) : (
        <div>
          <Button
            startIcon={<img src={menu} alt="Menu" />}
            className={classes.menuButton}
            color="primary"
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleClick}
          />

          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            className={classes.menuPopup}
          >
            {menuOptionState.map((elm) => (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => clickDetail(elm.url, dataDetail)}
              >
                <span
                  style={{
                    color: "#0061A7",
                    marginRight: 8,
                  }}
                >
                  {elm.name}
                </span>
                <img src={elm.icon} alt="eye" className={classes.menuIcon} />
              </MenuItem>
            ))}
          </Menu>
        </div>
      )}
    </div>
  );
};

MenuTable.propTypes = {};

MenuTable.defaultProps = {};

export default MenuTable;
