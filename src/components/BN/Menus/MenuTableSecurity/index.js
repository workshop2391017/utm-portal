// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  IconButton,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";

// components
import Delete from "../../Popups/Delete";
import DeleteConfirmation from "../../Popups/DeleteConfirmation";
import Modal from "../../Popups/EditSecurity";
import ModalUser from "../../Popups/EditUser";
import ModalCabang from "../../Popups/EditCabang";
// assets
import rotate from "../../../../assets/icons/BN/rotate-ccw.svg";
import menu from "../../../../assets/icons/BN/ellipsis-horizontal.svg";
import edit from "../../../../assets/icons/BN/edit-2.svg";
import trash from "../../../../assets/icons/BN/trash-2.svg";

const MenuTableSecurity = ({ status, rowData = null }) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    rotateButton: {
      padding: 7,
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();
  const [deleteModal, setDeleteModal] = useState(null);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(null);
  const [modal, setModal] = useState(false);
  const [modalUser, setModalUser] = useState(false);
  const [modalCabang, setModalCabang] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    setDeleteConfirmModal(true);
  };

  return (
    <div className={classes.menu}>
      <Delete
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={clickDelete}
      />
      <DeleteConfirmation
        isOpen={deleteConfirmModal}
        handleClose={() => setDeleteConfirmModal(false)}
      />
      <Modal
        label="Ubah Keamanan"
        isOpen={modal}
        handleClose={() => setModal(false)}
      />
      <ModalUser
        label="Ubah Pengguna"
        isOpen={modalUser}
        handleClose={() => setModalUser(false)}
        rowData={rowData}
      />
      <ModalCabang
        label="Ubah Cabang"
        isOpen={modalCabang}
        handleClose={() => setModalCabang(false)}
      />
      {status === 1 ? (
        <IconButton className={classes.rotateButton} color="primary">
          <img src={rotate} alt="Rotate Icon" />
        </IconButton>
      ) : status === 2 ? (
        <div>
          <Button
            startIcon={<img src={menu} alt="Menu" />}
            className={classes.menuButton}
            color="primary"
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleClick}
          />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            className={classes.menuPopup}
          >
            <MenuItem
              style={{ color: "rgba(0, 97, 167, 0.3)" }}
              onClick={() => setModal(true)}
            >
              <span style={{ color: "#0061A7" }}>Ubah</span>
              <img src={edit} alt="Edit" className={classes.menuIcon} />
            </MenuItem>
            <MenuItem
              style={{ color: "rgba(0, 97, 167, 0.3)" }}
              onClick={() => setDeleteModal(true)}
            >
              <span style={{ color: "#E31C23" }}>Hapus</span>
              <img src={trash} alt="Trash" className={classes.menuIcon} />
            </MenuItem>
          </Menu>
        </div>
      ) : status === 3 ? (
        <div>
          <Button
            startIcon={<img src={menu} alt="Menu" />}
            className={classes.menuButton}
            color="primary"
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleClick}
          />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            className={classes.menuPopup}
          >
            <MenuItem
              style={{ color: "rgba(0, 97, 167, 0.3)" }}
              onClick={() => setModalUser(true)}
            >
              <span style={{ color: "#0061A7" }}>Ubah</span>
              <img src={edit} alt="Edit" className={classes.menuIcon} />
            </MenuItem>
            <MenuItem
              style={{ color: "rgba(0, 97, 167, 0.3)" }}
              onClick={() => setDeleteModal(true)}
            >
              <span style={{ color: "#E31C23" }}>Hapus</span>
              <img src={trash} alt="Trash" className={classes.menuIcon} />
            </MenuItem>
          </Menu>
        </div>
      ) : (
        <div>
          <Button
            startIcon={<img src={menu} alt="Menu" />}
            className={classes.menuButton}
            color="primary"
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleClick}
          />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            className={classes.menuPopup}
          >
            <MenuItem
              style={{ color: "rgba(0, 97, 167, 0.3)" }}
              onClick={() => setModalCabang(true)}
            >
              <span style={{ color: "#0061A7" }}>Ubah</span>
              <img src={edit} alt="Edit" className={classes.menuIcon} />
            </MenuItem>
            <MenuItem
              style={{ color: "rgba(0, 97, 167, 0.3)" }}
              onClick={() => setDeleteModal(true)}
            >
              <span style={{ color: "#E31C23" }}>Hapus</span>
              <img src={trash} alt="Trash" className={classes.menuIcon} />
            </MenuItem>
          </Menu>
        </div>
      )}
    </div>
  );
};

MenuTableSecurity.propTypes = {};

MenuTableSecurity.defaultProps = {};

export default MenuTableSecurity;
