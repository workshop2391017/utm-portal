// main
import React, { useState } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";

// libraries
import { makeStyles, Button, Menu, MenuItem } from "@material-ui/core";

// assets
import menu from "assets/icons/BN/ellipsis-horizontal.svg";
import edit from "assets/icons/BN/edit-2.svg";
import trash from "assets/icons/BN/trash-2.svg";
import arrowRight from "assets/icons/BN/arrow-right.svg";

import copy from "assets/icons/BN/copy.svg";
import Star from "assets/icons/BN/star_blue.svg";

const TapContactSupport = ({
  status,
  onDetails,
  onDelete,
  onEdit,
  options,
  onMain,
  rowData,
  close,
  openModalCharge,
  closeCharge,

  openModal,
}) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        padding: 0,
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();
  const history = useHistory();
  const [deleteModal, setDeleteModal] = useState(null);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(null);
  const [modalMerchant, setModalMerchant] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);
  // const [modalDetail, setModalDetail] = useState(openModal);

  const clickPush = () => {
    document.scrollingElement.scrollTop = 0;
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    setDeleteConfirmModal(true);
  };

  return (
    <div className={classes.menu}>
      <div>
        <Button
          startIcon={<img src={menu} alt="Menu" />}
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          {(options ?? []).map((elm) =>
            elm === "detail" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={onDetails}
              >
                <span style={{ color: "#0061A7" }}>Detail</span>
                <img
                  src={arrowRight}
                  alt="Trash"
                  className={classes.menuIcon}
                />
                {/* Waiting Icon */}
              </MenuItem>
            ) : elm === "edit" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={onEdit}
              >
                <span style={{ color: "#0061A7" }}>Ubah</span>
                <img src={edit} alt="Edit" className={classes.menuIcon} />
              </MenuItem>
            ) : elm === "delete" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={onDelete}
              >
                <span style={{ color: "#0061A7" }}>Hapus</span>
                <img src={trash} alt="Trash" className={classes.menuIcon} />
              </MenuItem>
            ) : null
          )}
        </Menu>
      </div>
    </div>
  );
};

TapContactSupport.propTypes = {
  onDetails: PropTypes.func,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
  options: PropTypes.array.isRequired,
  onMain: PropTypes.func,
};

TapContactSupport.defaultProps = {
  onDetails: () => {},
  onDelete: () => {},
  onEdit: () => {},
  onMain: () => {},
};

export default TapContactSupport;
