// main
import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  Button,
  Menu,
  MenuItem,
  CircularProgress,
} from "@material-ui/core";

// assets
import menu from "assets/icons/BN/ellipsis-horizontal.svg";
import edit from "assets/icons/BN/edit-2.svg";
import trash from "assets/icons/BN/trash-21.svg";
import reload from "assets/icons/BN/reload.png";
import arrowRight from "assets/icons/BN/arrow-right.svg";
import Star from "assets/icons/BN/star_blue.svg";
import EditCompanyCharge from "components/BN/Popups/EditCompanyCharge";
import flagUs from "assets/icons/BN/en-flag-rounded.svg";
import flagID from "assets/icons/BN/id-flag-rounded.svg";

const LoadingAction = ({ title }) => (
  <div
    style={{
      width: "100%",
      display: "flex",
      justifyContent: "space-between",
    }}
  >
    {title}
    <div>
      <CircularProgress color="primary" size={15} />
    </div>
  </div>
);

const GeneralMenu = ({
  onDetails,
  onDelete,
  onEdit,
  onActive,
  options,
  onMain,
  onId,
  onEn,
  rowData,
  openModalCharge,
  closeCharge,
  isLoading,
  labels,
  customIcon,
}) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        padding: 0,
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 190,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 400,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.menu}>
      <EditCompanyCharge
        isOpen={openModalCharge}
        handleClose={closeCharge}
        title="Edit Company Charge"
        rowData={rowData}
      />
      <div>
        <Button
          startIcon={
            <img
              src={customIcon || menu}
              alt="Menu"
              // eslint-disable-next-line react/jsx-props-no-spreading
              {...(customIcon && { width: 22, height: 22 })}
            />
          }
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          {(options ?? []).map((elm) =>
            elm === "detail" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => {
                  onDetails();
                  handleClose();
                }}
              >
                <span style={{ color: "#0061A7" }}>Detail</span>
                <img
                  src={arrowRight}
                  alt="Trash"
                  className={classes.menuIcon}
                />
              </MenuItem>
            ) : elm === "edit" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => {
                  onEdit();
                  handleClose();
                }}
              >
                <span style={{ color: "#0061A7" }}>Edit</span>
                <img src={edit} alt="Edit" className={classes.menuIcon} />
              </MenuItem>
            ) : elm === "delete" ? (
              <MenuItem
                style={{ color: "#FF6F6F" }}
                onClick={() => {
                  onDelete();
                  handleClose();
                }}
              >
                {isLoading[elm] ? (
                  <LoadingAction />
                ) : (
                  <Fragment>
                    <span style={{ color: "#FF6F6F" }}>
                      {(labels && labels[elm]) || "Delete"}
                    </span>
                    <img
                      src={trash}
                      alt="Trash"
                      className={classes.menuIcon}
                      color="#FF6F6F"
                    />
                  </Fragment>
                )}
              </MenuItem>
            ) : elm === "inactive" ? (
              <MenuItem
                style={{ color: "#FF6F6F" }}
                onClick={() => {
                  onDelete();
                  handleClose();
                }}
              >
                {isLoading[elm] ? (
                  <LoadingAction />
                ) : (
                  <Fragment>
                    <span style={{ color: "#FF6F6F" }}>
                      {(labels && labels[elm]) || "Inactive"}
                    </span>
                    <img
                      src={trash}
                      alt="Trash"
                      className={classes.menuIcon}
                      color="#FF6F6F"
                    />
                  </Fragment>
                )}
              </MenuItem>
            ) : elm === "utama" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => {
                  onMain();
                  handleClose();
                }}
              >
                <span style={{ color: "#0061A7" }}>Main</span>
                <img src={flagUs} alt="Trash" className={classes.menuIcon} />
              </MenuItem>
            ) : elm === "id-flag" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => {
                  onMain();
                  onId();
                  handleClose();
                }}
              >
                <img src={flagID} alt="Trash" className={classes.menuIcon} />
                <span style={{ color: "#0061A7" }}>IDN</span>
              </MenuItem>
            ) : elm === "en-flag" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => {
                  onMain();
                  onEn();
                  handleClose();
                }}
              >
                <img src={flagUs} alt="Trash" className={classes.menuIcon} />
                <span style={{ color: "#0061A7" }}>ENG</span>
              </MenuItem>
            ) : elm === "reset-password" ? (
              <MenuItem
                style={{ color: "rgba(0, 97, 167, 0.3)" }}
                onClick={() => {
                  onDelete();
                  handleClose();
                }}
              >
                {isLoading[elm] ? (
                  <LoadingAction />
                ) : (
                  <Fragment>
                    <span style={{ color: "#0061A7" }}>
                      {(labels && labels[elm]) || "Reset Password"}
                    </span>
                    <img
                      src={reload}
                      alt="Trash"
                      className={classes.menuIcon}
                      color="#0061A7"
                    />
                  </Fragment>
                )}
              </MenuItem>
            ) : null
          )}
        </Menu>
      </div>
    </div>
  );
};

GeneralMenu.propTypes = {
  onDetails: PropTypes.func,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
  onActive: PropTypes.func,
  options: PropTypes.array.isRequired,
  onMain: PropTypes.func,
  onId: PropTypes.func,
  onEn: PropTypes.func,
  isLoading: PropTypes.object,
  labels: PropTypes.object,
};

GeneralMenu.defaultProps = {
  onDetails: () => {},
  onDelete: () => {},
  onEdit: () => {},
  onMain: () => {},
  onId: () => {},
  onEn: () => {},
  onActive: () => {},
  isLoading: {
    delete: false,
  },
  labels: {},
};

export default GeneralMenu;
