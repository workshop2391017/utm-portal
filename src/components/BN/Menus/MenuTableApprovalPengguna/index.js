// main
// libraries
import { Button, makeStyles, Menu, MenuItem } from "@material-ui/core";

// components
import DeletePopup from "components/BN/Popups/Delete";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// redux
import {
  deleteUserAdmin,
  setUserData,
  submitSettingUser,
} from "stores/actions/managementuser";

import edit from "../../../../assets/icons/BN/edit-2.svg";
import menu from "../../../../assets/icons/BN/ellipsis-horizontal.svg";
import trash from "../../../../assets/icons/BN/trash-blue.svg";
import AddComment from "../../Popups/AddComment";
import Delete from "../../Popups/DeleteConfirmation";
import EditPengaturanUser from "../../Popups/EditPengaturanUser";
import FailedConfirmationData from "../../Popups/FailedConfirmationData";
import SuccessConfirmation from "../../Popups/SuccessConfirmation";

const MenuTable = ({ disabled, rowData }) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    rotateButton: {
      padding: 7,
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();
  const dispatch = useDispatch();

  const { isLoadingSubmit, userData } = useSelector(
    ({ managementuser }) => managementuser
  );

  const [deleteModal, setDeleteModal] = useState(false);
  const [failedConfirmDataModal, setFailedConfirmDataModal] = useState({
    action: null,
    isOpen: false,
  });
  const [successConfirmModal, setSuccessConfirmModal] = useState({
    isOpen: false,
    message: null,
  });
  const [commentModal, setCommentModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [addCommentModal, setAddCommentModal] = useState(false);
  const [deleteConfirmation, setDeleteConfirmation] = useState(false);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    setCommentModal(true);
  };

  const hanldeDeleteUser = async () => {
    const deleteUser = await deleteUserAdmin({
      ...rowData,
      branch: rowData?.branchId,
      role: rowData?.roleId,
      password: null,
      phoneNumber: null,
      requestComment: null,
      ipAddress: null,
    })(dispatch);
    setDeleteConfirmation(false);
    if (deleteUser) {
      setSuccessConfirmModal({
        isOpen: true,
        message: "Data Deleted Successfully",
        title: "Deleted Successfully",
      });
    } else {
      setFailedConfirmDataModal({
        action: "delete",
        isOpen: true,
      });
    }
  };

  const onContinue = async (onContinue, comment) => {
    if (!onContinue) {
      setAddCommentModal(false);
    }

    const { branchId, ...rest } = userData;
    const payloadUserData = {
      ...rest,
      requestComment: comment,
    };

    const submitting = await submitSettingUser(payloadUserData)(dispatch);

    if (submitting) {
      setAddCommentModal(false);
      setSuccessConfirmModal({
        isOpen: true,
        message: "Addition of Saved Data",
      });
    } else {
      setFailedConfirmDataModal({
        action: "edit",
        isOpen: true,
      });
    }
  };

  return (
    <div className={classes.menu}>
      <Delete
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={clickDelete}
        title="Confirmation"
      />
      <DeletePopup
        isOpen={deleteConfirmation}
        handleClose={() => setDeleteConfirmation(false)}
        onContinue={hanldeDeleteUser}
        loading={isLoadingSubmit}
        title="Confirmation"
        message="Are You Sure to Delete the Data?"
      />

      <FailedConfirmationData
        isOpen={failedConfirmDataModal?.isOpen}
        handleClose={() =>
          setFailedConfirmDataModal({
            ...failedConfirmDataModal,
            isOpen: false,
          })
        }
        title="Fail"
        message="Changes Failed to Save"
      />

      <SuccessConfirmation
        isOpen={successConfirmModal?.isOpen}
        handleClose={() =>
          setSuccessConfirmModal({ isOpen: false, message: null })
        }
        // title="Data telah\ndisimpan, Menunggu\nPersetujuan"
        title={successConfirmModal?.title || "Saved Successfully"}
        submessage="Please wait for approval"
        message={successConfirmModal?.message}
      />

      <EditPengaturanUser
        isOpen={commentModal}
        handleClose={() => setCommentModal(false)}
        onContinue={() => {
          setAddCommentModal(true);
          setCommentModal(false);
        }}
        title="Edit User"
      />
      <AddComment
        isOpen={addCommentModal}
        handleClose={() => setAddCommentModal(false)}
        onContinue={onContinue}
        title="Comment Edit"
        loading={isLoadingSubmit}
      />

      <div>
        <Button
          startIcon={<img src={menu} alt="Menu" />}
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          disabled={disabled}
          style={{ opacity: disabled ? 0.5 : 1 }}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={() => {
              setCommentModal(true);
              dispatch(setUserData(rowData));
            }}
          >
            {/* dipakai pengaturan user */}
            <span
              style={{
                color: "#0061A7",
                fontFamily: "FuturaMdBT",
                fontSize: 13,
              }}
            >
              Edit
            </span>
            <img
              src={edit}
              alt="Approve"
              className={classes.menuIcon}
              width={14.87}
              height={14.87}
            />
          </MenuItem>

          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={() => setDeleteConfirmation(true)}
          >
            <span
              style={{
                color: "#0061A7",
                fontFamily: "FuturaMdBT",
                fontSize: 13,
              }}
            >
              Delete
            </span>
            <img
              src={trash}
              alt="Reject"
              className={classes.menuIcon}
              width={13.5}
              height={15}
            />
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};

MenuTable.propTypes = {
  disabled: PropTypes.bool,
};

MenuTable.defaultProps = {
  disabled: false,
};

export default MenuTable;
