// main
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import Delete from "../MenuTableApprovalPengguna";
// ganti delete
import DeleteConfirmation from "../../Popups/DeleteConfirmation";
import SuccessConfirmation from "../../Popups/SuccessConfirmation";

const MenuTable = ({ rowData }) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    rotateButton: {
      padding: 7,
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();

  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const clickDelete = () => {
    setDeleteModal(false);
    setDeleteConfirmModal(true);
  };

  return (
    <div className={classes.menu}>
      <Delete
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={clickDelete}
        rowData={rowData}
      />
      <DeleteConfirmation
        isOpen={deleteConfirmModal}
        handleClose={() => setDeleteConfirmModal(false)}
      />

      <SuccessConfirmation
        isOpen={successConfirmModal}
        handleClose={() => setSuccessConfirmModal(false)}
      />
    </div>
  );
};

MenuTable.propTypes = {};

MenuTable.defaultProps = {};

export default MenuTable;
