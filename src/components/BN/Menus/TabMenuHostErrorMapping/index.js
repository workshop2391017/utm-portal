// main
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  IconButton,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";

// components

// ganti delete
import { useDispatch, useSelector } from "react-redux";
import {
  deleteConfirm,
  DeleteDataHostErrorMapping,
} from "stores/actions/hostErrorMapping";

import DeleteConfirmation from "../../Popups/DeleteConfirmation";
import SuccessConfirmation from "../../Popups/SuccessConfirmation";
import ModalHostErrormapping from "../../Popups/HostErrormapping";
import DeletePopup from "../../Popups/Delete";

// assets
import rotate from "../../../../assets/icons/BN/rotate-ccw.svg";
import menu from "../../../../assets/icons/BN/ellipsis-horizontal.svg";
import edit from "../../../../assets/icons/BN/edit-2.svg";
import trash from "../../../../assets/icons/BN/trash-2.svg";

const MenuTable = ({ rowData }) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    rotateButton: {
      padding: 7,
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { data, isLoading, error, isDelete, isOpen, isLoadingExcute } =
    useSelector((state) => state.hostErrorMapping);

  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [dataDetail, setDataDetail] = useState({
    id: "",
    code: "",
    description: null,
    engMessage: "",
    idnMessage: "",
    sourceSystem: "",
  });

  useEffect(() => {
    setDataDetail({
      ...dataDetail,
      id: rowData?.id,
      code: rowData?.code,
      description: rowData?.description,
      engMessage: rowData?.engMessage,
      idnMessage: rowData?.idnMessage,
      sourceSystem: rowData?.sourceSystem,
    });
  }, [rowData]);

  useEffect(() => {
    setDeleteConfirmModal(false);
  }, [isOpen]);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    // setCommentModal(true);
  };

  const clickPush = () => {
    history.push("/setting/biller-kategori/edit");
    document.scrollingElement.scrollTop = 0;
  };

  const onDeleteHandler = () => {
    if (isOpen) {
      setDeleteConfirmModal(false);
    }
  };

  return (
    <div className={classes.menu}>
      <div>
        <Button
          startIcon={<img src={menu} alt="Menu" />}
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          style={{ opacity: 1 }}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={() => {
              setOpenModal(true);
            }}
          >
            <span style={{ color: "#0061A7" }}>Edit</span>
            <img src={edit} alt="Approve" className={classes.menuIcon} />
          </MenuItem>
          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={() => setDeleteConfirmModal(true)}
          >
            <span style={{ color: "#E31C23" }}>Delete</span>
            <img src={trash} alt="Reject" className={classes.menuIcon} />
          </MenuItem>
        </Menu>
      </div>
      <ModalHostErrormapping
        rowData={rowData}
        title="Edit Error Message"
        isOpen={openModal}
        handleClose={() => setOpenModal(false)}
      />
      <DeletePopup
        isOpen={deleteConfirmModal}
        handleClose={() => setDeleteConfirmModal(false)}
        onContinue={onDeleteHandler}
        loading={isLoadingExcute}
        title="Confirmation"
        message="Are you Sure to Delete Data?"
      />

      <SuccessConfirmation
        isOpen={successConfirmModal}
        handleClose={() => setSuccessConfirmModal(false)}
      />
    </div>
  );
};

MenuTable.propTypes = {};

MenuTable.defaultProps = {};

export default MenuTable;
