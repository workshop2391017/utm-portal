// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, Button, Menu, MenuItem } from "@material-ui/core";

// components
import Delete from "../../Popups/Delete";
import DeleteConfirmation from "../../Popups/DeleteConfirmation";

// assets
import menu from "../../../../assets/icons/BN/ellipsis-horizontal.svg";
import edit from "../../../../assets/icons/BN/edit-2.svg";
import trash from "../../../../assets/icons/BN/trash-2.svg";

const useStyles = makeStyles(() => ({
  menu: {
    width: 40,
    textAlign: "center",
    display: "inline-block",
  },
  menuButton: {
    minWidth: 40,
    padding: 0,
    "& .MuiButton-label": {
      padding: 0,
      "& .MuiButton-startIcon": {
        margin: 0,
      },
    },
  },
  menuPopup: {
    "& .MuiMenu-paper": {
      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
      borderRadius: 8,
      backgroundColor: "rgba(255,255,255, 0.95)",
      transform: "translate(-35px, 18px) !important",
      width: 118,
      "& .MuiMenu-list": {
        padding: 0,
        "& .MuiListItem-button": {
          fontWeight: 600,
          position: "relative",
          height: 33,
          "&:hover": {
            backgroundColor: "rgba(0, 97, 167, 0.05)",
          },
        },
      },
    },
  },
  menuIcon: {
    position: "absolute",
    right: 10,
    top: "50%",
    transform: "translateY(-50%)",
  },
}));

const MenuSegmentasiUserDetail = ({ handleEdit, dataDetail }) => {
  const classes = useStyles();
  const [deleteModal, setDeleteModal] = useState(null);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    setDeleteConfirmModal(true);
  };

  return (
    <div className={classes.menu}>
      <Delete
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={clickDelete}
      />
      <DeleteConfirmation
        isOpen={deleteConfirmModal}
        handleClose={() => setDeleteConfirmModal(false)}
      />
      <div>
        <Button
          startIcon={<img src={menu} alt="Menu" />}
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={() => {
              handleClose();
              handleEdit();
            }}
          >
            <span style={{ color: "#0061A7" }}>Edit</span>
            <img src={edit} alt="Edit" className={classes.menuIcon} />
          </MenuItem>
          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={() => setDeleteModal(true)}
          >
            <span style={{ color: "#E31C23" }}>Hapus</span>
            <img src={trash} alt="Trash" className={classes.menuIcon} />
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};

MenuSegmentasiUserDetail.propTypes = {
  dataDetail: PropTypes.object.isRequired,
};

MenuSegmentasiUserDetail.defaultProps = {};

export default MenuSegmentasiUserDetail;
