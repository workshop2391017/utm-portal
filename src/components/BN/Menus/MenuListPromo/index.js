// main
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

// libraries
import {
  makeStyles,
  IconButton,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";

// components

// ganti delete
import DeleteConfirmation from "components/BN/Popups/DeleteConfirmation";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import ModalHostErrormapping from "components/BN/Popups/HostErrormapping";
import DeletePopup from "components/BN/Popups/Delete";

// assets
import rotate from "assets/icons/BN/rotate-ccw.svg";
import menu from "assets/icons/BN/ellipsis-horizontal.svg";
import edit from "assets/icons/BN/edit-2.svg";
import trash from "assets/icons/BN/trash-2.svg";

const MenuTable = ({ rowData, onEdit, onDelete }) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      minWidth: 40,
      padding: 0,
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    rotateButton: {
      padding: 7,
    },
    menuPopup: {
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255, 0.95)",
        transform: "translate(-35px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));
  const classes = useStyles();
  const history = useHistory();

  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(false);
  const [successConfirmModal, setSuccessConfirmModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [openModal, setOpenModal] = useState(false);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    // setCommentModal(true);
  };
  const clickDetail = () => {
    history.push("/portal/management-promo/change-promo/");
    document.scrollingElement.scrollTop = 0;
  };

  const clickComment = () => {
    // setCommentModal(false);
    // setFailedConfirmDataModal({title:"Gagal", action:"delete", isOpen:false});
    // setAddCommentModal(true);
  };

  const clickAddComment = () => {
    // setAddCommentModal(false);
    //
  };

  const clickPush = () => {
    history.push("/setting/biller-kategori/edit");
    document.scrollingElement.scrollTop = 0;
  };

  return (
    <div className={classes.menu}>
      <div>
        <Button
          startIcon={<img src={menu} alt="Menu" />}
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          //   disabled={disabled}
          //   style={{ opacity: disabled ? 0.5 : 1 }}
          style={{ opacity: 1 }}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          <MenuItem style={{ color: "rgba(0, 97, 167, 0.3)" }} onClick={onEdit}>
            {/* dipakai pengaturan user */}
            <span style={{ color: "#0061A7" }}>Edit</span>
            <img src={edit} alt="Approve" className={classes.menuIcon} />
            {/* onClick={clickDetail} */}
          </MenuItem>
          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={onDelete}
          >
            <span style={{ color: "#E31C23" }}>Delete</span>
            <img src={trash} alt="Reject" className={classes.menuIcon} />
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};

MenuTable.propTypes = {
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
};

MenuTable.defaultProps = {
  onEdit: () => {},
  onDelete: () => {},
};

export default MenuTable;
