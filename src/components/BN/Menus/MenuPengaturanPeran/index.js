// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles, IconButton, Menu, MenuItem } from "@material-ui/core";

// components
import Delete from "../../Popups/Delete";
import DeleteConfirmation from "../../Popups/DeleteConfirmation";
import SuccessConfirmation from "../../Popups/SuccessConfirmation";

// assets
import menuBlue from "../../../../assets/icons/BN/ellipsis-vertical.svg";
import menuWhite from "../../../../assets/icons/BN/ellipsis-vertical-white.svg";
import trash from "../../../../assets/icons/BN/trash-blue.svg";
import copy from "../../../../assets/icons/BN/copy.svg";
import iconedit from "../../../../assets/icons/BN/edit-2.svg";

const MenuPengaturanPeran = ({
  color,
  handleDuplicate,
  handleDeleteRole,
  handleEdit,
}) => {
  const useStyles = makeStyles(() => ({
    menu: {
      width: 40,
      textAlign: "center",
      display: "inline-block",
    },
    menuButton: {
      paddingLeft: 6,
      paddingTop: 6,
      paddingBottom: 6,
      paddingRight: "0 !important",
      transform: "rotate(90deg)",
      "& .MuiButton-label": {
        "& .MuiButton-startIcon": {
          margin: 0,
        },
      },
    },
    menuPopup: {
      marginTop: 15,
      marginLeft: 35,
      "& .MuiMenu-paper": {
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.3)",
        borderRadius: 8,
        backgroundColor: "rgba(255,255,255)",
        transform: "translate(-75px, 18px) !important",
        width: 118,
        "& .MuiMenu-list": {
          padding: 0,
          "& .MuiListItem-button": {
            fontWeight: 600,
            position: "relative",
            height: 33,
            "&:hover": {
              backgroundColor: "rgba(0, 97, 167, 0.05)",
            },
          },
        },
      },
    },
    menuIcon: {
      position: "absolute",
      right: 10,
      top: "50%",
      transform: "translateY(-50%)",
    },
  }));

  const classes = useStyles();
  const [deleteModal, setDeleteModal] = useState(null);
  const [deleteConfirmModal, setDeleteConfirmModal] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const [confirmSuccess, setConfirmSuccess] = useState(false);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const clickDelete = () => {
    setDeleteModal(false);
    setDeleteConfirmModal(true);
  };

  return (
    <div className={classes.menu}>
      <Delete
        isOpen={deleteModal}
        handleClose={() => setDeleteModal(false)}
        onContinue={clickDelete}
      />
      <DeleteConfirmation
        isOpen={deleteConfirmModal}
        handleClose={() => setDeleteConfirmModal(false)}
      />
      <SuccessConfirmation
        isOpen={confirmSuccess}
        handleClose={() => setConfirmSuccess(false)}
      />
      <div>
        <IconButton
          className={classes.menuButton}
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <img src={color === "blue" ? menuBlue : menuWhite} alt="Menu" />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menuPopup}
        >
          {/* <MenuItem style={{color:'rgba(0, 97, 167, 0.3)'}} onClick={() => setConfirmSuccess(true)}>
              <span style={{color:'#0061A7'}}>Duplicate</span>
              <img src={copy} alt='Edit' className={classes.menuIcon} />
            </MenuItem> */}

          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={handleDuplicate}
          >
            <span style={{ color: "#0061A7" }}>Duplicate</span>
            <img src={copy} alt="Edit" className={classes.menuIcon} />
          </MenuItem>

          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={handleEdit}
          >
            <span style={{ color: "#0061A7" }}>Ubah</span>
            <img src={iconedit} alt="Edit" className={classes.menuIcon} />
          </MenuItem>

          <MenuItem
            style={{ color: "rgba(0, 97, 167, 0.3)" }}
            onClick={handleDeleteRole}
          >
            <span style={{ color: "#0061A7" }}>Hapus</span>
            <img src={trash} alt="Trash" className={classes.menuIcon} />
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};

MenuPengaturanPeran.propTypes = {
  color: PropTypes.string,
  handleDuplicate: PropTypes.func,
  handleDeleteRole: PropTypes.func,
  handleEdit: PropTypes.func,
};

MenuPengaturanPeran.defaultProps = {
  color: "blue",
  handleDuplicate: () => {},
  handleDeleteRole: () => {},
  handleEdit: () => {},
};

export default MenuPengaturanPeran;
