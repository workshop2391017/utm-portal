import React, { useState, useRef } from "react";
import IdleTimer from "react-idle-timer";
import PropTypes from "prop-types";
import { Layout } from "antd";
import { handleLogout } from "stores/actions/login";
import { useDispatch } from "react-redux";

import SessionTimeout from "../Popups/SessionTimeout";

const { Content } = Layout;

const IdleContainerTimer = () => {
  const dispatch = useDispatch();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const idleTimerRef = useRef(null);
  const sessionTimeoutRef = useRef(null);

  const logOut = () => {
    sessionStorage.clear();
    dispatch(handleLogout());
  };

  const onIdle = () => {
    setModalIsOpen(true);
    sessionTimeoutRef.current = setTimeout(() => logOut(), 60000);
  };

  const stayActive = () => {
    setModalIsOpen(false);
    clearTimeout(sessionTimeoutRef.current);
  };

  return (
    <div>
      <SessionTimeout
        isOpen={modalIsOpen}
        onContinue={stayActive}
        onCancel={logOut}
      />
      <IdleTimer
        ref={idleTimerRef}
        timeout={600000}
        // timeout={1000}
        onIdle={onIdle}
      />
    </div>
  );
};

IdleContainerTimer.propTypes = {};

IdleContainerTimer.defaultProps = {};

export default IdleContainerTimer;
