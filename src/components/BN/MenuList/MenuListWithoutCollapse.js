import React, { useState } from "react";
import { Menu } from "antd";
import { CircularProgress, makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";
import PropTypes from "prop-types";
import ScrollCustom from "../ScrollCustom";
import AntdTextField from "../TextField/AntdTextField";
import SearchWithDropdown from "../Search/SearchWithoutDropdown/index";

const { SubMenu } = Menu;

const useStyles = makeStyles((menuOptions) => ({
  menuContainer: {
    backgroundColor: "#fff",
    padding: 20,
    borderRadius: 10,
    "& .ant-menu": {
      borderRight: "none",
      "& .ant-menu-submenu-title": {
        color: Colors.dark.hard,
        fontFamily: "FuturaHvBT",
        fontWeight: 400,
        lineHeight: "44px",
        fontSize: "15px !important",
        borderBottom: "1px solid #E6EAF3",
        marginLeft: "15px !important",
        padding: "0 !important",
        marginRight: "15px !important",
      },
      "& .ant-menu-item": {
        margin: 0,
        minHeight: 44,
        fontSize: 15,
        color: "#374062",
        fontFamily: "FuturaHvBT",
        fontWeight: 400,
        "&::after": {
          borderRight: "none",
        },
        "&.ant-menu-item-selected": {
          ...((menuOptions?.children ?? []).length < 1
            ? {
                backgroundColor: Colors.primary.hard,
                color: "white",
              }
            : { backgroundColor: "#fff" }),
          "&:hover": {
            backgroundColor: "#0061A7",
          },
        },
        "&:hover": {
          backgroundColor: "#fff",
        },
      },

      "& .ant-menu-submenu": {
        minHeight: 44,
        "& .ant-menu-item-only-child": {
          color: Colors.dark.hard,
          fontFamily: "FuturaBkBT",
          fontWeight: 400,
          lineHeight: "44px",
          fontSize: 15,
          padding: "0 !important",
          margin: 0,
        },
        "& .ant-menu-submenu-title": {
          margin: 0,
          minHeight: 44,
          lineHeight: "44px",
        },

        // submenus
        "& .ant-menu-sub.ant-menu-inline": {
          backgroundColor: "#fff",
          marginTop: 15,
          "& li": {
            paddingLeft: "25px !important",
            "&::after": {
              borderRight: "none",
            },
            "&.ant-menu-item-selected": {
              backgroundColor: Colors.primary.hard,
              color: "white",
            },
          },
        },
        "&.ant-menu-submenu-open": {
          backgroundColor: "#fff",
          "&.ant-menu-submenu-active .ant-menu-submenu-title:hover": {
            backgroundColor: "#fff",
          },
        },
        "&.ant-menu-submenu-active": {
          "& .ant-menu-submenu-title": {
            "&:hover": {
              backgroundColor: "#fff",
            },
          },
        },
        "&.ant-menu-submenu-selected": {
          backgroundColor: "#fff",
        },
      },
    },
  },
  noDataMessage: {
    textAlign: "center",
    fontFamily: "FuturaMdBT",
    fontSize: 21,
    color: Colors.dark.medium,
    paddingTop: 30,
  },
  header: {
    height: 93,
    width: "100%",
    color: Colors.dark.hard,
    fontSize: 15,
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
  },
}));

const MenuListWithoutCollapse = ({
  onClick,
  menuOptions,
  headerTitle,
  defaultOpenKeys,
  search,
  height,
  setSearch,
  width,
  loading,
  selectedKeys,
  placeholder,
  placement,
  trigger,
  inputPlaceholder,
}) => {
  const [defaultSelectedKeys, setDefaultSelectedKeys] = useState([]);
  const classes = useStyles(menuOptions);

  return (
    <div className={classes.menuContainer}>
      {headerTitle ? (
        <div className={classes.header}>
          <div style={{ marginBottom: 15 }}>{headerTitle}</div>
          <SearchWithDropdown
            style={{ width: 242 }}
            placeholder={placeholder}
            placement={placement}
            dataSearch={search}
            setDataSearch={setSearch}
            trigger={trigger}
            inputPlaceholder={inputPlaceholder}
          />
        </div>
      ) : null}

      {!menuOptions?.length ? (
        <div className={classes.noDataMessage} style={{ height }}>
          No Data
        </div>
      ) : (
        <ScrollCustom height={height} width={width}>
          {loading ? (
            <div style={{ display: "flex", marginTop: 50 }}>
              <CircularProgress style={{ margin: "auto" }} />
            </div>
          ) : null}
          <div style={{ position: "relative" }}>
            <Menu
              onClick={onClick}
              selectedKeys={selectedKeys}
              style={{ height: 459 }}
              defaultSelectedKeys={defaultSelectedKeys}
              openKeys={menuOptions?.map((elm) => elm?.key) ?? []}
              expandIcon={() => null}
              mode="inline"
              items={menuOptions}
            />
          </div>
        </ScrollCustom>
      )}
    </div>
  );
};

MenuListWithoutCollapse.propTypes = {
  onClick: PropTypes.func.isRequired,
  menuOptions: PropTypes.array.isRequired,
  setSearch: PropTypes.func,
  search: PropTypes.string,
  headerTitle: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.number,
  loading: PropTypes.bool,
  placement: PropTypes.string,
  trigger: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  inputPlaceholder: PropTypes.string,
};

MenuListWithoutCollapse.defaultProps = {
  headerTitle: "",
  setSearch: () => {},
  search: "",
  width: "250px",
  height: 381,
  loading: false,
  placement: "bottom",
  trigger: "none",
  inputPlaceholder: "Search...",
};

export default MenuListWithoutCollapse;
