import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { makeStyles, Paper, CircularProgress } from "@material-ui/core";
import ScrollCustom from "components/BN/ScrollCustom";
import SearchWithDropdown from "../Search/SearchWithoutDropdown";

const useStyles = makeStyles({
  loading: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    height: "100%",
    alignItems: "center",
  },
  cardMenu: {
    boxSizing: "border-box",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "10px",
    marginBottom: "40px",
    "& > :first-child": {
      borderWidth: "1px 0px !important",
      borderStyle: "solid !important",
      borderColor: "#EAF2FF !important",
    },
  },
  cardMenuTile: {
    fontSize: "17px",
    color: "#374062",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
    width: "100%",
    height: "55px",
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    backgroundColor: "#fff",
    padding: "10px",
  },
  cardTitle: {
    width: "100%",
    fontSize: "15px",
    fontFamily: "FuturaHvBT",
    fontWeight: 900,
    padding: "16px 10px",
    "&:hover": {
      backgroundColor: "#0061A7 !important",
      color: "#fff !important",
    },
    "&:active": {
      backgroundColor: "#0061A7 !important",
      color: "#fff !important",
    },
  },
});

const MenuListOptional = ({
  titleMenu,
  listData,
  selectedItem,
  onClick,
  childKey,
  isLoading,
  maxHeight,
  isSearch,
  search,
  setSearch,
  placeholder,
  placement,
  trigger,
  inputPlaceholder,
}) => {
  const classes = useStyles();
  return (
    <Fragment>
      <div className={classes.cardMenuTile}>{titleMenu}</div>
      {isSearch ? (
        <div style={{ paddingLeft: 10, marginBottom: 8 }}>
          <SearchWithDropdown
            style={{ width: "300px" }}
            placeholder={placeholder}
            placement={placement}
            dataSearch={search}
            setDataSearch={setSearch}
            trigger={trigger}
            inputPlaceholder={inputPlaceholder}
          />
        </div>
      ) : null}
      {isLoading ? (
        <div className={classes.loading}>
          <CircularProgress />
        </div>
      ) : (
        <Fragment>
          {maxHeight ? (
            <ScrollCustom height={maxHeight}>
              <div className={classes.cardMenu}>
                {listData?.map((item, index) => (
                  <Paper
                    style={{
                      backgroundColor:
                        item?.id !== undefined && item?.id !== null
                          ? `${
                              selectedItem?.data?.id === item?.id
                                ? "#0061A7"
                                : "#fff"
                            }`
                          : `${
                              selectedItem?.index === index ? "#0061A7" : "#fff"
                            }`,
                      color:
                        item?.id !== undefined && item?.id !== null
                          ? `${
                              selectedItem?.data?.id === item?.id
                                ? "#fff"
                                : "#0061A7"
                            }`
                          : `${
                              selectedItem?.index === index ? "#fff" : "#0061A7"
                            }`,
                      borderBottom: "1px solid #EAF2FF",
                      cursor: "pointer",
                    }}
                    elevation={0}
                    className={classes.cardTitle}
                    onClick={() => {
                      onClick({ index, data: item });
                    }}
                  >
                    {childKey ? item[childKey] : item?.name_en}
                  </Paper>
                ))}
              </div>
            </ScrollCustom>
          ) : (
            <div className={classes.cardMenu}>
              {listData?.map((item, index) => (
                <Paper
                  style={{
                    backgroundColor: `${
                      selectedItem?.index === index ? "#0061A7" : "#fff"
                    }`,
                    color: `${
                      selectedItem?.index === index ? "#fff" : "#0061A7"
                    }`,
                    borderBottom: "1px solid #EAF2FF",
                    cursor: "pointer",
                  }}
                  elevation={0}
                  className={classes.cardTitle}
                  onClick={() => {
                    onClick({ index, data: item });
                  }}
                >
                  {childKey ? item[childKey] : item?.name_en}
                </Paper>
              ))}
            </div>
          )}
        </Fragment>
      )}
    </Fragment>
  );
};

MenuListOptional.propTypes = {
  titleMenu: PropTypes.string,
  listData: PropTypes.array,
  selectedItem: PropTypes.bool,
  onClick: PropTypes.func,
  setSearch: PropTypes.func,
  childKey: PropTypes.string,
  isLoading: PropTypes.bool,
  isSearch: PropTypes.bool,
  maxHeight: PropTypes.string,
  placement: PropTypes.string,
  trigger: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  inputPlaceholder: PropTypes.string,
};

MenuListOptional.defaultProps = {
  titleMenu: "",
  listData: [],
  selectedItem: {},
  onClick: () => {},
  setSearch: () => {},
  childKey: "",
  isLoading: false,
  isSearch: false,
  maxHeight: "",
  placement: "bottom",
  trigger: "none",
  inputPlaceholder: "Search...",
};

export default MenuListOptional;
