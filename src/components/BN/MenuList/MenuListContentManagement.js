import React, { Fragment, useState } from "react";
import { Col, Menu } from "antd";
import { CircularProgress, makeStyles } from "@material-ui/core";
import Colors from "helpers/colors";
import PropTypes from "prop-types";
import ScrollCustom from "../ScrollCustom";

const { SubMenu } = Menu;

const useStyles = makeStyles((theme) => ({
  menuContainer: {
    backgroundColor: "#fff",
    minWidth: 280,
    [theme.breakpoints.up("md")]: {
      minWidth: 230,
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: 260,
    },
    [theme.breakpoints.up("xl")]: {
      minWidth: 260,
    },
    "& .ant-menu": {
      borderRight: "none",
      "& .ant-menu-title-content": {
        color: Colors.primary.hard,
      },
      "& .ant-menu-submenu-title": {
        fontWeight: 400,
        fontFamily: "FuturaHvBT",
        lineHeight: "44px",
        fontSize: 14,
        padding: "0 20px !important",
        [theme.breakpoints.up("md")]: {
          fontSize: 12,
        },
        [theme.breakpoints.up("lg")]: {
          fontSize: 14,
        },
        [theme.breakpoints.up("xl")]: {
          fontSize: 14,
        },
      },
      "& .ant-menu-item": {
        margin: 0,
        minHeight: 44,
        fontSize: 12,

        [theme.breakpoints.up("md")]: {
          fontSize: 11,
        },
        [theme.breakpoints.up("lg")]: {
          fontSize: 12,
        },
        [theme.breakpoints.up("xl")]: {
          fontSize: 12,
        },
        "& span": {
          // marginLeft: 10,
        },
        "&::after": {
          borderRight: "none",
        },

        "& .ant-menu-item-selected": {
          backgroundColor: Colors.primary.hard,
          // color: "#fff !important",
          "&:hover": {
            // backgroundColor: Colors.primary.hard,
            // color: "#fff",
          },
        },
        "&:hover": {
          // backgroundColor: Colors.primary.hard,
          // color: "#fff",
          "& .ant-menu-title-content": {
            // color: "#fff",
          },
        },
      },
      "& .ant-menu-submenu": {
        minHeight: 44,
        borderBottom: "1px solid #E8EEFF",
        "& .ant-menu-submenu-arrow::before": {
          backgroundColor: Colors.primary.hard,
        },
        "& .ant-menu-submenu-arrow::after": {
          backgroundColor: Colors.primary.hard,
        },
        "& .ant-menu-item-only-child": {
          fontWeight: 400,
          fontFamily: "FuturaHvBT",
          lineHeight: "44px",
          fontSize: 13,
          color: Colors.dark.hard,
          [theme.breakpoints.up("md")]: {
            fontSize: 11,
          },
          [theme.breakpoints.up("lg")]: {
            fontSize: 13,
          },
          [theme.breakpoints.up("xl")]: {
            fontSize: 13,
          },
        },
        "& .ant-menu-submenu-title": {
          margin: 0,
          minHeight: 44,
          lineHeight: "44px",
          "& .ant-menu-submenu-arrow": {
            right: "10px",
          },
        },
        "& .ant-menu-sub.ant-menu-inline": {
          backgroundColor: "#fff",
          borderTop: "1px solid #E8EEFF",
          "& li": {
            paddingLeft: "58px !important",
            "&::after": {
              borderRight: "none",
            },
            "&.ant-menu-item-selected": {
              backgroundColor: Colors.primary.hard,
            },
          },
        },
        "&.ant-menu-submenu-open": {
          backgroundColor: "#fff",
          "& .ant-menu-item-selected": {
            "& .ant-menu-title-content": {
              color: "#fff]",
            },
          },
          "& .ant-menu-submenu-title": {
            backgroundColor: Colors.info.soft,
            "& .ant-menu-title-content": {
              color: Colors.primary.hard,
            },
          },
        },
        "&.ant-menu-submenu-selected": {
          backgroundColor: Colors.primary.hard,
          "& .ant-menu-submenu-title": {
            "& .ant-menu-title-content": {
              color: Colors.primary.hard,
            },
          },
          "& .ant-menu-submenu-arrow::before": {
            backgroundColor: "#red !important",
          },
          "& .ant-menu-submenu-arrow::after": {
            backgroundColor: "#red !important",
          },
        },
      },
    },
  },
  header: {
    backgroundColor: "white",
    color: Colors.dark.hard,
    width: "100%",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    fontSize: 17,
    fontFamily: "FuturaHvBT",
    fontWeight: 400,
    display: "flex",
    alignItems: "center",
    padding: "20px 10px 20px 20px",
    borderBottom: "1px solid #EAF2FF",
    [theme.breakpoints.up("md")]: {
      fontSize: 14,
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: 17,
    },
    [theme.breakpoints.up("xl")]: {
      fontSize: 17,
    },
  },
  headerDokumen: {
    fontWeight: 400,
    fontFamily: "FuturaMdBT",
    letterSpacing: "0.01em",
    fontSize: 20,
    background: "#0061A7",
    padding: "10px 0px 0px 20px",
    width: "100%",
    // margin: '20px',
    height: "50px",
    borderRadius: "20px 20px 0px 0px",
    color: "#fff",
  },
}));

const MenuListCM = ({
  isLoading,
  onTitleClick,
  onClick,
  isApprovalWorkflow,
  menuOptions,
  mode,
  withHeader,
  headerTitle,
  children,
  onSubClick,
  selectedKeys,
  expandIcon,
  defaultSelectedKeys,
  defaultOpenKeys,
  openKeys,
  disabled,
  scrollHeight = 500,
  minWidth,
  borderRadius,
  contentManagement,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.menuContainer} style={{ borderRadius }}>
      {withHeader ? (
        <div className={classes.headerDokumen}>{headerTitle}</div>
      ) : null}
      {isLoading ? (
        <div style={{ display: "flex", height: "100%", marginTop: 50 }}>
          <CircularProgress style={{ margin: "auto" }} />
        </div>
      ) : null}
      {!isLoading ? (
        <Fragment>
          <ScrollCustom height={scrollHeight} style={{ minWidth }}>
            {children || (
              <Menu
                style={{ width: "100%", height: 459, borderRadius }}
                mode={mode}
                expandIcon={expandIcon}
                openKeys={openKeys}
                selectedKeys={selectedKeys}
                disabled={disabled}
              >
                {(menuOptions ?? []).map((elm, index) => (
                  <SubMenu
                    title={elm.name}
                    key={elm.key}
                    onTitleClick={() => onTitleClick(elm, [elm.key], index)}
                    style={{
                      backgroundColor:
                        selectedKeys.includes(elm.key) && Colors.info.soft,
                    }}
                  >
                    {(elm?.submenu ?? []).map((submenu, subIndex) => (
                      <Menu.Item
                        key={submenu.key}
                        onClick={() =>
                          onSubClick(submenu, index, subIndex, [
                            elm.key,
                            submenu.key,
                          ])
                        }
                      >
                        {submenu.name}
                      </Menu.Item>
                    ))}
                  </SubMenu>
                ))}
              </Menu>
            )}
          </ScrollCustom>
        </Fragment>
      ) : null}
    </div>
  );
};

MenuListCM.propTypes = {
  onTitleClick: PropTypes.func, // key onClick callback
  onSubClick: PropTypes.func,
  onClick: PropTypes.func.isRequired,
  menuOptions: PropTypes.array,
  mode: PropTypes.string,
  headerTitle: PropTypes.string,
  withHeader: PropTypes.bool,
  children: PropTypes.node,
  isApprovalWorkflow: PropTypes.bool,
  selectedKeys: PropTypes.number,
  expandIcon: PropTypes.bool,
  isLoading: PropTypes.bool,
  defaultSelectedKeys: PropTypes.array,
  defaultOpenKeys: PropTypes.array,
  disabled: PropTypes.bool,
};

MenuListCM.defaultProps = {
  onTitleClick: () => {},
  onSubClick: () => {},
  menuOptions: [],
  mode: "inline",
  headerTitle: "",
  withHeader: false,
  children: null,
  isApprovalWorkflow: false,
  selectedKeys: null,
  expandIcon: false,
  isLoading: false,
  defaultSelectedKeys: [],
  defaultOpenKeys: [],
  disabled: false,
};

export default MenuListCM;
