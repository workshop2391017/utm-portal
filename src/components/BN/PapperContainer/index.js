import React from "react";
import PropTypes from "prop-types";
import { CircularProgress, makeStyles } from "@material-ui/core";
import ButtonOutlined from "../Button/ButtonOutlined";
import GeneralButton from "../Button/GeneralButton";

const useStyles = (className) =>
  makeStyles({
    title: {
      fontSize: "24px",
      fontFamily: "FuturaMdBT",
      width: "100%",
      height: "59px",
      backgroundColor: "#0061A7",
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: "50px",
    },
    mainContainer: {
      width: 850,
      height: "auto",
      borderRadius: "20px",
      overflow: "hidden",
      position: "relative",
      backgroundColor: "#fff",
      boxShadow: "0px 3px 10px rgba(188, 200, 231, 0.2)",
      ...className,
    },
    mainContent: {
      padding: "50px",
    },
    buttonAction: {
      marginTop: 40,
    },
    buttonGroup: {
      width: 850,
      display: "flex",
      justifyContent: "space-between",
    },
    loadingContainer: {
      height: "400px",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  });

const PapperContainer = ({
  children,
  title,
  className,
  onCancel,
  withButton,
  onContinue,
  style,
  prevTitle,
  prevButton,
  nextButton,
  nextTitle,
  isLoading,
}) => {
  const classes = useStyles(className)();

  return (
    <div style={style}>
      <div className={classes.mainContainer}>
        <div className={classes.title}>
          <span className="text">{title}</span>
        </div>
        <div className={classes.mainContent}>
          {isLoading ? (
            <div className={classes.loadingContainer}>
              <CircularProgress color="primary" size={40} />
            </div>
          ) : (
            children
          )}
        </div>
      </div>
      {withButton ? (
        <div className={classes.buttonAction}>
          <div className={classes.buttonGroup}>
            {prevButton ? (
              <ButtonOutlined
                label={prevTitle}
                width="83px"
                style={{ marginRight: 11 }}
                onClick={onCancel}
              />
            ) : (
              <div />
            )}

            {nextButton && (
              <GeneralButton
                label={nextTitle}
                width="118px"
                onClick={onContinue}
              />
            )}
          </div>
        </div>
      ) : null}
    </div>
  );
};

PapperContainer.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  className: PropTypes.object,
  withButton: PropTypes.bool,
  onContinue: PropTypes.func,
  onCancel: PropTypes.func,
  style: PropTypes.object,
  prevTitle: PropTypes.string,
  nextTitle: PropTypes.string,
  prevButton: PropTypes.bool,
  nextButton: PropTypes.bool,
  isLoading: PropTypes.bool,
};
PapperContainer.defaultProps = {
  className: {},
  withButton: false,
  onContinue: () => {},
  onCancel: () => {},
  style: {},
  prevTitle: "Kembali",
  nextTitle: "Setujui",
  prevButton: true,
  nextButton: true,
  isLoading: false,
};

export default PapperContainer;
