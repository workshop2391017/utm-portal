import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import moment from "moment";

// libraries
import { makeStyles, ButtonGroup } from "@material-ui/core";
import SegmentationTypeSelect from "components/BN/Select/SegmentationTypeSelect";
import { Tooltip } from "antd";

// components
import Colors from "helpers/colors";
import { ReactComponent as SearchIcon } from "assets/icons/BN/search.svg";
import DropdownSelect from "components/BN/Select/AntdSelectObjectItem";
import { ReactComponent as ArrowUp } from "assets/icons/BN/chevron-up-alt.svg";
import { ReactComponent as ArrowDown } from "assets/icons/BN/chevron-down-alt.svg";
import Button from "../../Button/GeneralButton";
import Tabs from "../../Tabs/GeneralTabs";
// import DatePicker from "../../DatePicker/RangeDatePicker";
import RangeDatePicker from "../../DatePicker/RangeDatePicker";
import Date from "../../DatePicker/GeneralDatePicker";
import Search from "../../Button/ButtonDropDown/ButtonDropDown";
import TextField from "../../TextField/AntdTextField";
import SearchWithoutDropdown from "../../Search/SearchWithoutDropdown";

const GeneralFilter = (props) => {
  const {
    options,
    optionsAdvance,
    className,
    align,
    dataFilter,
    setDataFilter,
    onClickButton,
    page,
    withTitle,
    style,
    title,
    pg,
    isDashboard,
    isAdvance,
    disabled,
    dropdownAdvance,
  } = props;
  const useStyles = makeStyles((theme) => ({
    filter: {
      width: "100%",
      minHeight: 70,
      backgroundColor: "#fff",
      borderRadius: 8,
      display: "flex",
      flexDirection: "column",
    },
    row: {
      display: "flex",
    },
    title: {
      fontFamily: "FuturaHvBT",
      fontWeight: 600,
      width: 114,
      lineHeight: "73px",
      fontSize: 15,
      paddingLeft: 20,
      [theme.breakpoints.up("md")]: {
        fontSize: 14,
      },
      [theme.breakpoints.up("lg")]: {
        fontSize: 15,
      },
      [theme.breakpoints.up("xl")]: {
        fontSize: 15,
      },
    },
    button: {
      display: "flex",
      justifyContent: "flex-end",
      alignItems: "center",
      [theme.breakpoints.up("md")]: {
        width: 90,
      },
      [theme.breakpoints.up("lg")]: {
        width: 104,
      },
      [theme.breakpoints.up("xl")]: {
        width: 104,
      },
      paddingRight: 23,
    },
    advanceButton: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginLeft: "20px",
      [theme.breakpoints.up("md")]: {
        width: 110,
      },
      [theme.breakpoints.up("lg")]: {
        width: 124,
      },
      [theme.breakpoints.up("xl")]: {
        width: 124,
      },
      paddingRight: 23,
    },
    container: {
      display: "flex",
      flex: "auto",
      justifyContent:
        align === "center"
          ? "center"
          : align === "between"
          ? "space-between"
          : "flex-start",
    },
    containerPaper: {
      display: "flex",
      width: "100%",
      justifyContent: "space-between",
    },
    inputContainer: {
      display: "flex",
      lineHeight: "73px",
      alignItems: "center",
      margin:
        align === "center"
          ? "0 10px"
          : align === "left"
          ? "0 10px"
          : "0 0 0 -363px",
    },
    dropdownAdvance: {
      display: "flex",
      paddingLeft: 117,
      gap: "30px",
    },
    lebar: {
      display: "flex",
      lineHeight: "73px",
      alignItems: "center",
      margin: align === "center" ? "0 10px" : "0 0 0 20px",
      marginRight: 390,
    },
  }));
  const classes = useStyles();

  const [tabValue, setTabValue] = useState({ tabValue1: 0 });
  const [tabValueAdvance, setTabValueAdvance] = useState({
    tabValueAdvance: 0,
  });
  const [dateValue, setDateValue] = useState(null);
  const [dataDropdown, setDataDropdown] = useState(null);
  const [dateValueAdvance, setDateValueAdvance] = useState(null);
  const [dropdownValueAdvance, setDropdownValueAdvance] = useState(null);
  const [disabledDate, setDisabledDate] = useState(null);
  const [searchValue, setsearchValue] = useState(null);
  const [searchVal, setSearchVal] = useState("");
  const [disableSideDateFilter, setDisableDateSideFilter] = useState(false);
  const [showAdvance, setShowAdvance] = useState(false);
  const [segmentationType, setSegmentationType] = useState(null);

  const handleFilter = () => {
    setDisableDateSideFilter(false);
    onClickButton();
    setDataFilter({
      tabs: tabValue,
      date: dateValue,
      ...(segmentationType && { segmentation: segmentationType }),
      search: searchVal,
      dateAdvance: dateValueAdvance,
      dropdownAdvanceVal: dropdownValueAdvance,
      tabsAdvance: tabValueAdvance,
      dropdown: dataDropdown,
    });
  };

  const handleTabsChange = (event, newValue, id) => {
    if (tabValue) {
      setTabValue({ ...tabValue, [`tabValue${id}`]: newValue });
    }
    if (options && dateValue && isDashboard) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };

  const handleTabsChangeTwo = (event, newValue, id) => {
    if (tabValueAdvance) {
      setTabValueAdvance({
        ...tabValueAdvance,
        [`tabValueAdvance${id}`]: newValue,
      });
    }
    if (optionsAdvance && dateValueAdvance && isDashboard) {
      const obj = {};
      optionsAdvance?.map((item) => {
        if (item.type === "date") {
          obj[`dateValueAdvance${item.id}`] = null;
        }
        return item;
      });
      setDateValueAdvance(obj);
    }
  };

  const handleDisabled = (current) =>
    current > disabledDate[0].startOf("day") &&
    current < disabledDate[1].endOf("day");

  useEffect(() => {
    if (options.length > 0) {
      const objTabs = {};
      const objDate = {};
      const objDrop = {};

      // const objPlace = {};
      options.map((item) => {
        if (item.type === "tabs") {
          objTabs[`tabValue${item.id}`] = dataFilter
            ? dataFilter.tabs[`tabValue${item.id}`]
            : item?.defaultValue || 0;
        } else if (item.type === "dropdown") {
          objDrop[`dropdown${item?.id}`] =
            dataFilter && dataFilter?.dropdown?.[`dropdown${item.id}`];
        } else {
          objDate[`dateValue${item.id}`] =
            dataFilter && dataFilter.date[`dateValue${item.id}`];
          // objPlace[`placeholderState${item.id}`] = true;
        }
        return item;
      });

      setTabValue(objTabs);
      setDateValue(objDate);
      setDataDropdown(objDrop);
    }
    if (optionsAdvance.length > 0) {
      const objTabsAdvance = {};
      const objDateAdvance = {};
      const objDropdownAdvance = {};
      optionsAdvance?.map((item) => {
        if (item.type === "tabs") {
          objTabsAdvance[`tabValueAdvance${item.id}`] = dataFilter
            ? dataFilter.tabsAdvance[`tabValueAdvance${item.id}`]
            : 0;
        }
        if (item.type === "dropdown") {
          objDropdownAdvance[`dropdown${item?.id}`] =
            dataFilter &&
            dataFilter?.dropdownAdvanceVal?.[`dropdownValueAdvance${item.id}`];
          objDropdownAdvance.type = dataFilter?.dropdownAdvanceVal?.type;
          objDropdownAdvance.category =
            dataFilter?.dropdownAdvanceVal?.category;
        } else {
          objDateAdvance[`dateValueAdvance${item.id}`] =
            dataFilter && dataFilter.dateAdvance[`dateValueAdvance${item.id}`];
          // objPlace[`placeholderState${item.id}`] = true;
        }

        return item;
      });

      setTabValueAdvance(objTabsAdvance);
      setDateValueAdvance(objDateAdvance);
      setDropdownValueAdvance(objDropdownAdvance);
    }
  }, [dataFilter, options, optionsAdvance]);

  useEffect(() => {
    // reset tabs filter when date change
    if (options.length > 0) {
      const arr = [];
      options.forEach((item) => item.type === "date" && arr.push(item.id));
      options.forEach(
        (item) => item.type === "rangePicker" && arr.push(item.id)
      );
      const newArr = arr.map((item) => dateValue?.[`dateValue${item}`]);
      if (newArr.findIndex((item) => item?.startDate || item?.endDate) !== -1) {
        const objTabs = {};
        options.map((item) => {
          if (item.type === "tabs") {
            objTabs[`tabValue${item.id}`] = null;
          }
          return item;
        });
        setTabValue(objTabs);
      }
    }
    if (optionsAdvance?.length > 0) {
      const arr = [];
      optionsAdvance?.forEach(
        (item) => item.type === "date" && arr.push(item.id)
      );
      optionsAdvance?.forEach(
        (item) => item.type === "rangePicker" && arr.push(item.id)
      );
      const newArr = arr.map((item) => dateValue?.[`dateValueAdvance${item}`]);
      if (newArr.findIndex((item) => item?.startDate || item?.endDate) !== -1) {
        const objTabs = {};
        optionsAdvance?.map((item) => {
          if (item.type === "tabs") {
            objTabs[`tabValueAdvance${item.id}`] = null;
          }
          return item;
        });
        setTabValueAdvance(objTabs);
      }
    }

    // disable date on rangepicker2 when rangepicker1 is empty date
    if (dateValue) {
      const arr = options.filter((item) => item.type === "date");
      arr.map((item, i) => {
        if (i === 0) {
          if (dateValue[`dateValue${item.id}`]) {
            setDisabledDate(dateValue[`dateValue${item.id}`]);
          } else {
            setDisabledDate(null);
          }
        }
        return item;
      });
    }
  }, [dateValue, options, optionsAdvance]);

  // clear date filter and clear disable tabs when date filter is empty
  const handleClearDateValue = () => {
    // const obj = {};
    // options.map((item) => {
    //   if (item.type === "rangePicker") {
    //     obj[`dateValue${item.id}`] = null;
    //   }
    //   return item;
    // });
    // setDateValue(obj);
    // setDisableSideTabsFilter(false);
    if (!disabledDate) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date" || item.type === "rangePicker") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };

  const handleAdvance = () => {
    setShowAdvance(!showAdvance);
  };

  const handleDateApply = () => {
    if (page === "dashboard") {
      const dashboard = Object.values(dateValue?.dateValue2 ?? {});
      const dashboard2 = Object.values(dateValue?.dateValue3 ?? {});
      if (dashboard?.length) {
        if (
          dashboard.filter((e) => e).length === 2 &&
          dashboard2.filter((e) => e).length === 2
        ) {
          return false;
        }
        return true;
      }
      return false;
    }

    const date1 = Object.values(dateValue?.dateValueAdvance ?? {});
    const dateAdvance = Object.values(dateValue?.dateValue3 ?? {});

    const datePicker = Object.values(dateValue ?? {});

    if (datePicker.length > 1) {
      if (datePicker[0] && !datePicker[1]) return true;
      if (datePicker[1] && !datePicker[0]) return true;
      return false;
    }

    if (
      date1.filter((e) => e).length === 2 ||
      dateAdvance.filter((e) => e).length === 2
    ) {
      if (
        date1.filter((e) => e).length === 2 &&
        dateAdvance.filter((e) => e).length === 2
      ) {
        return false;
      }
      return true;
    }
    return false;
  };

  return (
    <div
      className={className ? `${className} ${classes.filter}` : classes.filter}
    >
      <div className={classes.row}>
        {withTitle && <div className={classes.title}>{title}</div>}
        <div className={classes.containerPaper}>
          <div className={classes.container}>
            {options.map((item, i) =>
              item.type === "tabs" ? (
                <div className={classes.inputContainer}>
                  <Tabs
                    value={tabValue && tabValue[`tabValue${item.id}`]}
                    onChange={(event, newValue) => {
                      handleTabsChange(event, newValue, item.id);
                      if (page === "dashboard") {
                        setDisableDateSideFilter(true);
                      }
                    }}
                    tabs={item.options}
                    style={{ height: "30px" }}
                  />
                </div>
              ) : item.type === "dropdown" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div style={{ marginLeft: item.title ? 10 : 0 }}>
                    <DropdownSelect
                      placeholder={pg ? "Tingkatan" : `${item.placeholder}`}
                      options={item.options}
                      style={{ width: item?.width || 180 }}
                      value={
                        (dataDropdown && dataDropdown[`dropdown${item.id}`]) ||
                        null
                      }
                      onChange={
                        dateValue &&
                        ((event) =>
                          setDataDropdown({
                            ...dateValue,
                            [`dropdown${item.id}`]: event,
                          }))
                      }
                    />
                  </div>
                </div>
              ) : item.type === "date" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div style={{ marginLeft: item.title ? 10 : 0 }}>
                    <Date
                      value={dateValue && dateValue[`dateValue${item.id}`]}
                      onChange={
                        dateValue &&
                        ((date, dateString) =>
                          setDateValue({
                            ...dateValue,
                            [`dateValue${item.id}`]: date,
                          }))
                      }
                      placeholder={item.placeholder}
                      style={{
                        fontFamily: "FuturaMdBT",
                        padding: "4px 0px 4px 14px",
                      }}
                    />
                  </div>
                  {item.dash && (
                    <div className={classes.range}>{item.dash}</div>
                  )}
                </div>
              ) : item.type === "datePicker" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div className={classes.input}>
                    <Date
                      value={dateValue && dateValue[`dateValue${item.id}`]}
                      format="DD/MM/YYYY"
                      onChange={
                        dateValue &&
                        ((date, dateString) => {
                          const filterOpt = options.filter(
                            (e) => e.type === "datePicker"
                          );
                          setDateValue({
                            ...dateValue,
                            [`dateValue${item.id}`]: date,

                            //  rest 2 date
                            ...(filterOpt.length > 1 &&
                              item.id === filterOpt[0]?.id && {
                                [`dateValue${item.id + 1}`]: undefined,
                              }),
                          });
                        })
                      }
                      placeholder={item.placeholder}
                      width={190.5}
                      style={{
                        fontFamily: "FuturaMdBT",
                        padding: "4px 0px 4px 14px",
                      }}
                      disabledDate={(current) =>
                        (item.id === 2 &&
                          item.disabled &&
                          current <
                            moment(
                              moment(
                                dateValue[`dateValue${item.id - 1}`]
                              ).format("YYYY-MM-DD")
                            )) ||
                        current > moment().endOf("day")
                      }
                    />
                  </div>
                  {item.dash && (
                    <div className={classes.range}>{item.dash}</div>
                  )}
                </div>
              ) : item.type === "select" ? (
                <div className={classes.inputContainer}>
                  <div>
                    <Search
                      style={{ width: item?.width || "auto" }}
                      value={
                        searchValue && searchValue[`searchValue${item.id}`]
                      }
                      onChange={(event, newValue) =>
                        handleTabsChange(event, newValue, item.id)
                      }
                      options={item.options}
                    />
                  </div>
                </div>
              ) : item.type === "rangePicker" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div className={classes.input}>
                    <RangeDatePicker
                      disabled={
                        page === "dashboard"
                          ? false
                          : (item.id === 3 &&
                              !dateValue?.dateValueAdvance?.startDate &&
                              !dateValue?.dateValueAdvance?.endDate) ||
                            disableSideDateFilter
                      }
                      value={
                        dateValue && [
                          dateValue[`dateValue${item.id}`]?.startDate,
                          dateValue[`dateValue${item.id}`]?.endDate,
                        ]
                      }
                      onChange={
                        dateValue &&
                        ((date, dateString) => {
                          if (!date?.[0] && !date?.[1]) {
                            handleClearDateValue();
                          } else {
                            setDateValue({
                              ...dateValue,
                              [`dateValue${item.id}`]: {
                                startDate: date[0],
                                endDate: date[1],
                              },
                            });
                          }
                        })
                      }
                      placeholder={item.placeholder}
                      style={{
                        fontFamily: "FuturaMdBT",
                        padding: "4px 0px 4px 14px",
                        // width: "223px !important",
                      }}
                      disabledDate={(current) =>
                        page === "dashboard"
                          ? (item.id === 3 &&
                              current.isBetween(
                                moment(
                                  dateValue?.dateValue2?.startDate
                                ).subtract(1, "days"),
                                moment(dateValue?.dateValue2?.endDate).add(
                                  1,
                                  "days"
                                ),
                                "days"
                              )) ||
                            current > moment().endOf("day")
                          : item.id === 3 &&
                            current.isBetween(
                              moment(
                                dateValue?.dateValueAdvance?.startDate
                              ).subtract(1, "days"),
                              moment(dateValue?.dateValueAdvance?.endDate).add(
                                1,
                                "days"
                              ),
                              "days"
                            )
                      }
                    />
                  </div>
                  {item.dash && (
                    <div className={classes.range}>{item.dash}</div>
                  )}
                </div>
              ) : item.type === "input" ? (
                <div className={classes.inputContainer}>
                  <Tooltip
                    placement={item.tooltipPlacement}
                    title={
                      <span style={{ color: Colors.dark.hard }}>
                        {item.tooltipPlaceholder}
                      </span>
                    }
                    color="#E6EAF3"
                    trigger={["hover", "focus", "contextMenu"]}
                  >
                    <TextField
                      placeholder={item.placeholder}
                      value={dateValue && dateValue[`dateValue${item.id}`]}
                      onChange={(e) =>
                        setDateValue({
                          ...dateValue,
                          [`dateValue${item.id}`]: e.target.value,
                        })
                      }
                      style={{ width: "230px" }}
                      suffix={
                        <SearchIcon style={{ width: "24px", height: "24px" }} />
                      }
                    />
                  </Tooltip>
                </div>
              ) : null
            )}
            {isAdvance ? (
              <div className={classes.advanceButton}>
                <Button
                  label="Advance"
                  width="115px"
                  height="40px"
                  onClick={handleAdvance}
                  variant="text"
                  buttonIcon={showAdvance ? <ArrowUp /> : <ArrowDown />}
                  iconPosition="endIcon"
                />
              </div>
            ) : null}
          </div>

          <div className={classes.button}>
            <Button
              label="Apply"
              width="115px"
              height="40px"
              style={{ borderRadius: 10 }}
              onClick={handleFilter}
              disabled={handleDateApply()}
            />
          </div>
        </div>
      </div>

      {showAdvance ? (
        !dropdownAdvance ? (
          optionsAdvance?.map((item, i) => (
            <div
              style={{
                display: "flex",
                margin: "0px 20px 20px 115px",
              }}
            >
              <Tabs
                value={
                  tabValueAdvance &&
                  tabValueAdvance[`tabValueAdvance${item.id}`]
                }
                onChange={(event, newValue) => {
                  handleTabsChangeTwo(event, newValue, item.id);
                  if (page === "dashboard") {
                    setDisableDateSideFilter(true);
                  }
                }}
                tabs={item.options}
                style={{ height: "30px" }}
              />
            </div>
          ))
        ) : (
          <div className={classes.dropdownAdvance}>
            {optionsAdvance?.map((item, i) => (
              <div>
                <div>
                  {item.title === "" || item.title === undefined
                    ? ""
                    : `${item.title} :`}
                </div>
                <div style={{ marginLeft: item.title ? 10 : 0 }}>
                  <DropdownSelect
                    placeholder={pg ? "Tingkatan" : `${item.placeholder}`}
                    options={item.options}
                    style={{ width: item?.width || 180 }}
                    value={
                      item.id === 4
                        ? dropdownValueAdvance.type &&
                          dropdownValueAdvance.type[`dropdown${item.id}`]
                        : dropdownValueAdvance.category &&
                          dropdownValueAdvance.category[`dropdown${item.id}`]
                    }
                    onChange={
                      dateValue &&
                      ((event) =>
                        setDropdownValueAdvance((prev) => ({
                          ...prev,
                          [item.id === 4 ? "type" : "category"]: event,
                        })))
                    }
                  />
                </div>
              </div>
            ))}
          </div>
        )
      ) : null}
    </div>
  );
};

GeneralFilter.propTypes = {
  options: PropTypes.array,
  className: PropTypes.string,
  align: PropTypes.string,
  dataFilter: PropTypes.object.isRequired,
  setDataFilter: PropTypes.func.isRequired,
  onClickButton: PropTypes.func,
  page: PropTypes.string,
  withTitle: PropTypes.bool,
  style: PropTypes.string,
  title: PropTypes.string,
  isAdvance: PropTypes.bool,
  disabled: PropTypes.bool,
  optionsAdvance: PropTypes.array,
  dropdownAdvance: PropTypes.bool,
};

GeneralFilter.defaultProps = {
  style: "",
  options: [],
  className: "",
  align: "left",
  page: "",
  withTitle: true,
  onClickButton: () => {},
  title: "Period :",
  isAdvance: false,
  disabled: false,
  optionsAdvance: [],
  dropdownAdvance: false,
};

export default GeneralFilter;
