// main
import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import Button from "../../Button/GeneralButton";
import Tabs from "../../Tabs/GeneralTabs";
import DatePicker from "../../DatePicker/RangeDatePicker";
import Date from "../../DatePicker/GeneralDatePicker";
import Search from "../../Button/ButtonDropDown/ButtonDropDown";

const GeneralFilter = (props) => {
  const {
    options,
    className,
    align,
    dataFilter,
    setDataFilter,
    onClickButton,
  } = props;
  const useStyles = makeStyles({
    filter: {
      width: "100%",
      minHeight: 70,
      backgroundColor: "#fff",
      borderRadius: 8,
      display: "flex",
      position: "relative",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontWeight: 600,
      marginLeft: 15,
      lineHeight: "73px",
      fontSize: 16,
    },
    button: {
      lineHeight: "73px",
      position: "absolute",
      right: 20,
      top: 0,
    },
    container: {
      display: "flex",
      flex: "auto",
      // justifyContent: 'space-evenly',
      paddingRight: 130,
      justifyContent: align === "center" ? "center" : "flex-start",
    },
    inputContainer: {
      display: "flex",
      lineHeight: "73px",
      alignItems: "center",
      margin: align === "center" ? "0 10px" : "0 0 0 20px",
    },
  });
  const classes = useStyles();

  const [tabValue, setTabValue] = useState(null);
  const [dateValue, setDateValue] = useState(null);
  const [disabledDate, setDisabledDate] = useState(null);
  const [searchValue, setsearchValue] = useState(null);

  // const [openCalendar, setOpenCalendar] = useState(null);
  // const [placeholderState, setPlaceholderState] = useState(null);
  // console.log('LOOK VALUE', dateValue);
  // console.log('LOOK OPEN', openCalendar);
  // console.log('LOOK PLACE', placeholderState);

  const handleFilter = () => {
    onClickButton();
    setDataFilter({
      tabs: tabValue,
      date: dateValue,
    });
  };

  const handleTabsChange = (event, newValue, id) => {
    if (tabValue) {
      setTabValue({ ...tabValue, [`tabValue${id}`]: newValue });
    }
    if (options && dateValue) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };

  const handleDisabled = (current) =>
    current > disabledDate[0].startOf("day") &&
    current < disabledDate[1].endOf("day");

  useEffect(() => {
    if (options.length > 0) {
      const objTabs = {};
      const objDate = {};
      // const objPlace = {};
      options.map((item) => {
        if (item.type === "tabs") {
          objTabs[`tabValue${item.id}`] = dataFilter
            ? dataFilter.tabs[`tabValue${item.id}`]
            : 0;
        } else {
          objDate[`dateValue${item.id}`] =
            dataFilter && dataFilter.date[`dateValue${item.id}`];
          // objPlace[`placeholderState${item.id}`] = true;
        }
        return item;
      });
      setTabValue(objTabs);
      setDateValue(objDate);
      // setPlaceholderState(objPlace);
    }
  }, [dataFilter, options]);

  useEffect(() => {
    // reset tabs filter when date change
    if (options.length > 0) {
      const arr = [];
      options.forEach((item) => item.type === "date" && arr.push(item.id));
      const newArr = arr.map((item) => dateValue?.[`dateValue${item}`]);
      if (newArr.findIndex((item) => item) !== -1) {
        const objTabs = {};
        options.map((item) => {
          if (item.type === "tabs") {
            objTabs[`tabValue${item.id}`] = null;
          }
          return item;
        });
        setTabValue(objTabs);
      }
    }

    // disable date on rangepicker2 when rangepicker1 is empty date
    if (dateValue) {
      const arr = options.filter((item) => item.type === "date");
      arr.map((item, i) => {
        if (i === 0) {
          if (dateValue[`dateValue${item.id}`]) {
            setDisabledDate(dateValue[`dateValue${item.id}`]);
          } else {
            setDisabledDate(null);
          }
        }
        return item;
      });
    }
  }, [dateValue, options]);

  // clear date on rangepicker2 when rangepicker1 is empty date
  useEffect(() => {
    if (!disabledDate) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  }, [disabledDate, options]);

  return (
    <div
      className={className ? `${className} ${classes.filter}` : classes.filter}
    >
      <div className={classes.title}> </div>
      {/* <div className={classes.title}> Tampilakan </div> */}
      <div className={classes.container}>
        {options.map((item, i) =>
          item.type === "tabs" ? (
            <div className={classes.inputContainer}>
              <Tabs
                value={tabValue && tabValue[`tabValue${item.id}`]}
                onChange={(event, newValue) =>
                  handleTabsChange(event, newValue, item.id)
                }
                tabs={item.options}
              />
            </div>
          ) : item.type === "date" ? (
            <div className={classes.inputContainer}>
              <div>
                {item.title === "" || item.title === undefined
                  ? ""
                  : `${item.title} :`}
              </div>
              <div style={{ marginLeft: item.title ? 10 : 0 }}>
                <DatePicker
                  // openCalendar={openCalendar}
                  // setOpenCalendar={setOpenCalendar}
                  // placeholderState={placeholderState}
                  // setPlaceholderState={setPlaceholderState}
                  datePickerID={item.id}
                  value={dateValue && dateValue[`dateValue${item.id}`]}
                  onChange={
                    dateValue &&
                    ((dates, dateStrings) =>
                      setDateValue({
                        ...dateValue,
                        [`dateValue${item.id}`]: dates,
                      }))
                  }
                  placeholder={item.placeholder}
                  disabled={item.disabled && !disabledDate}
                  disabledDate={
                    item.disabledDate && disabledDate && handleDisabled
                  }
                />
              </div>
              {item.dash && <div className={classes.range}>{item.dash}</div>}
            </div>
          ) : item.type === "datePicker" ? (
            <div className={classes.inputContainer}>
              <div>
                {item.title === "" || item.title === undefined
                  ? ""
                  : `${item.title} :`}
              </div>
              <div className={classes.input}>
                <Date
                  value={dateValue && dateValue[`dateValue${item.id}`]}
                  onChange={
                    dateValue &&
                    ((date, dateString) =>
                      setDateValue({
                        ...dateValue,
                        [`dateValue${item.id}`]: date,
                      }))
                  }
                  placeholder={item.placeholder}
                  style={{
                    fontFamily: "FuturaMdBT",
                    padding: "4px 0px 4px 14px",
                  }}
                />
              </div>
              {item.dash && <div className={classes.range}>{item.dash}</div>}
            </div>
          ) : item.type === "select" ? (
            <div className={classes.inputContainer}>
              <div>
                <Search
                  value={searchValue && searchValue[`searchValue${item.id}`]}
                  onChange={(event, newValue) =>
                    handleTabsChange(event, newValue, item.id)
                  }
                  options={item.options}
                />
              </div>
            </div>
          ) : null
        )}
      </div>

      {/* <div className={classes.button}>
        <Button
          label='Terapkan'
          width='104px'
          height='40px'
          onClick={handleFilter}
        />
      </div> */}
    </div>
  );
};

GeneralFilter.propTypes = {
  options: PropTypes.array,
  className: PropTypes.string,
  align: PropTypes.string,
  dataFilter: PropTypes.object.isRequired,
  setDataFilter: PropTypes.func.isRequired,
  onClickButton: PropTypes.func,
};

GeneralFilter.defaultProps = {
  options: [],
  className: "",
  align: "left",
  onClickButton: () => {},
};

export default GeneralFilter;
