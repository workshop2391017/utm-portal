import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import moment from "moment";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import DropdownSelect from "components/BN/Select/AntdSelectWithCheck";
import DropdownSelectNoBorder from "components/BN/Select/AntdNoBorder";
import Button from "../../Button/GeneralButton";
import Tabs from "../../Tabs/GeneralTabs";
// import DatePicker from "../../DatePicker/RangeDatePicker";
import RangeDatePicker from "../../DatePicker/RangeDatePicker";
import Date from "../../DatePicker/GeneralDatePicker";
import Search from "../../Button/ButtonDropDown/ButtonDropDown";
import TextField from "../../TextField/AntdTextField";

const GeneralFilterTransaksi = (props) => {
  const {
    options,
    setDataPayload,
    dataPayload,
    className,
    align,
    dataFilter,
    setDataFilter,
    onClickButton,
    page,
    withTitle,
    style,
    styleButton,
    value,
    onChange,
    tabs,
    tabWidth,
    onClick,
  } = props;
  const useStyles = makeStyles({
    filter: {
      width: "1054px",
      minHeight: 70,
      backgroundColor: "#fff",
      borderRadius: 8,
      display: "flex",
      marginLeft: "28px",
    },
    filter2: {
      minHeight: 70,
      backgroundColor: "#fff",
      borderRadius: 8,
      display: "flex",
      marginLeft: "28px",
    },
    title: {
      fontFamily: "FuturaHvBT",
      fontWeight: 600,
      width: 114,
      lineHeight: "73px",
      fontSize: 15,
      paddingLeft: 20,
    },
    button: {
      display: "flex",
      justifyContent: "flex-end",
      alignItems: "center",
      marginTop: "-3px",
    },
    container: {
      display: "flex",
      flex: "auto",
      justifyContent:
        align === "center"
          ? "center"
          : align === "between"
          ? "space-between"
          : "flex-start",

      alignItems: "center",
    },
    containerPaper: {
      display: "flex",
      width: "100%",
      justifyContent: "space-between",
    },
    inputContainer: {
      display: "flex",
      lineHeight: "73px",
      alignItems: "center",
      margin: align === "center" ? "0 10px" : "0 0 0 20px",
    },
    lebar: {
      display: "flex",
      lineHeight: "73px",

      margin: align === "center" ? "0 10px" : "0 0 0 20px",
    },
  });
  const classes = useStyles();

  const [tabValue, setTabValue] = useState({ tabValue1: 0 });
  const [dateValue, setDateValue] = useState(null);
  const [disabledDate, setDisabledDate] = useState(null);
  const [searchValue, setsearchValue] = useState(null);
  const [disableSideDateFilter, setDisableDateSideFilter] = useState(false);

  const handleFilter = () => {
    setDisableDateSideFilter(false);
    onClickButton();
    setDataFilter({
      tabs: tabValue,
      date: dateValue,
    });
  };

  const handleTabsChange = (event, newValue, id) => {
    if (tabValue) {
      setTabValue({ ...tabValue, [`tabValue${id}`]: newValue });
    }
    if (options && dateValue) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };

  const handleDisabled = (current) =>
    current > disabledDate[0].startOf("day") &&
    current < disabledDate[1].endOf("day");

  const [advance, setAdvance] = useState(false);

  useEffect(() => {
    if (options.length > 0) {
      const objTabs = {};
      const objDate = {};
      // const objPlace = {};
      options.map((item) => {
        if (item.type === "tabs") {
          objTabs[`tabValue${item.id}`] = dataFilter
            ? dataFilter.tabs[`tabValue${item.id}`]
            : 0;
        } else {
          objDate[`dateValue${item.id}`] =
            dataFilter && dataFilter.date[`dateValue${item.id}`];
          // objPlace[`placeholderState${item.id}`] = true;
        }
        return item;
      });
      setTabValue(objTabs);
      setDateValue(objDate);
      // setPlaceholderState(objPlace);
    }
  }, [dataFilter, options]);

  useEffect(() => {
    // reset tabs filter when date change
    if (options.length > 0) {
      const arr = [];
      options.forEach((item) => item.type === "date" && arr.push(item.id));
      options.forEach(
        (item) => item.type === "rangePicker" && arr.push(item.id)
      );
      const newArr = arr.map((item) => dateValue?.[`dateValue${item}`]);
      if (newArr.findIndex((item) => item) !== -1) {
        const objTabs = {};
        options.map((item) => {
          if (item.type === "tabs") {
            objTabs[`tabValue${item.id}`] = null;
          }
          return item;
        });
        setTabValue(objTabs);
      }
    }

    // disable date on rangepicker2 when rangepicker1 is empty date
    if (dateValue) {
      const arr = options.filter((item) => item.type === "date");
      arr.map((item, i) => {
        if (i === 0) {
          if (dateValue[`dateValue${item.id}`]) {
            setDisabledDate(dateValue[`dateValue${item.id}`]);
          } else {
            setDisabledDate(null);
          }
        }
        return item;
      });
    }
  }, [dateValue, options]);

  // clear date filter and clear disable tabs when date filter is empty
  const handleClearDateValue = () => {
    // const obj = {};
    // options.map((item) => {
    //   if (item.type === "rangePicker") {
    //     obj[`dateValue${item.id}`] = null;
    //   }
    //   return item;
    // });
    // setDateValue(obj);
    // setDisableSideTabsFilter(false);
    if (!disabledDate) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date" || item.type === "rangePicker") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };
  const dateNow = moment();
  const dates = dateNow.format("YYYY-MM-DD");
  useEffect(() => {
    if (dataFilter !== null) {
      setDataPayload({
        ...dataPayload,
        dateFrom:
          dataFilter?.date?.dateValue1 !== null
            ? moment(dataFilter?.date?.dateValue1).format("YYYY-MM-DD")
            : dates,
        dateTo:
          dataFilter?.date?.dateValue2 !== null
            ? moment(dataFilter?.date?.dateValue2).format("YYYY-MM-DD")
            : dates,
        transactionStatus:
          dataFilter?.tabs?.tabValue1 !== undefined
            ? dataFilter?.tabs?.tabValue1
            : null,
        transactionGroup:
          dataFilter.date?.dropdown3 === "Semua"
            ? null
            : dataFilter.date?.dropdown3 === "Transfer"
            ? "TRANSFER"
            : dataFilter.date?.dropdown3 === "Pembayaran"
            ? "BILLPAYMENT"
            : dataFilter.date?.dropdown3 === "Pembelian"
            ? "PURCHASE"
            : null,
        toBtnTransfer:
          dataFilter?.date?.dropdown4 === "Sesama Bank"
            ? true
            : dataFilter?.date?.dropdown4 === "Bank Lain"
            ? false
            : null,
        transferOA: dataFilter?.date?.dropdown4 !== "Pindah Dana" ? null : true,

        page: 1,
        size: 10,
      });
    }
  }, [dataFilter]);

  // clear date on rangepicker2 when rangepicker1 is empty date
  useEffect(() => {
    // handleClearDateValue();
  }, [disabledDate, options]);

  return (
    <div
      className={className ? `${className} ${classes.filter2}` : classes.filter}
    >
      <div className={classes.containerPaper}>
        <div>
          <div className={classes.container}>
            {options.map((item, i) =>
              item.type === "tabs" ? (
                <div className={classes.inputContainer}>
                  <Tabs
                    value={tabValue && tabValue[`tabValue${item.id}`]}
                    onChange={(event, newValue) => {
                      handleTabsChange(event, newValue, item.id);
                      if (page === "dashboard") {
                        setDisableDateSideFilter(true);
                      }
                    }}
                    tabs={item.options}
                    style={{ height: "30px" }}
                  />
                </div>
              ) : item.type === "dropdown" ? (
                <div className={classes.lebar}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div style={{ color: "#0061A7" }}>
                    <DropdownSelect
                      placeholder={item.placeholder}
                      options={item.options}
                      style={{ width: 200, color: "#0061A7" }}
                      value={dateValue && dateValue[`dropdown${item.id}`]}
                      onChange={
                        dateValue &&
                        ((event, dateString) =>
                          setDateValue({
                            ...dateValue,
                            [`dropdown${item.id}`]: event,
                          }))
                      }
                    />
                  </div>
                </div>
              ) : item.type === "date" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div style={{ marginLeft: item.title ? 10 : 0 }}>
                    <Date
                      value={dateValue && dateValue[`dateValue${item.id}`]}
                      onChange={
                        dateValue &&
                        ((date, dateString) =>
                          setDateValue({
                            ...dateValue,
                            [`dateValue${item.id}`]: date,
                          }))
                      }
                      placeholder={item.placeholder}
                      style={{
                        width: "162.5px",
                        fontFamily: "FuturaMdBT",
                        padding: "4px 0px 4px 14px",
                        color: "#BCC8E7",
                      }}
                    />
                  </div>
                  {item.dash && (
                    <div className={classes.range}>{item.dash}</div>
                  )}
                </div>
              ) : item.type === "datePicker" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div style={{ color: "#BCC8E7" }} className={classes.input}>
                    <Date
                      value={dateValue && dateValue[`dateValue${item.id}`]}
                      onChange={
                        dateValue &&
                        ((date, dateString) =>
                          setDateValue({
                            ...dateValue,
                            [`dateValue${item.id}`]: date,
                          }))
                      }
                      placeholder={item.placeholder}
                      width={162.5}
                      style={{
                        fontFamily: "FuturaMdBT",
                        padding: "4px 0px 4px 14px",
                        color: "red !important",
                      }}
                    />
                  </div>
                  {item.dash && (
                    <div className={classes.range}>{item.dash}</div>
                  )}
                </div>
              ) : item.type === "select" ? (
                <div className={classes.inputContainer}>
                  <div>
                    <Search
                      value={
                        searchValue && searchValue[`searchValue${item.id}`]
                      }
                      onChange={(event, newValue) =>
                        handleTabsChange(event, newValue, item.id)
                      }
                      options={item.options}
                    />
                  </div>
                </div>
              ) : item.type === "rangePicker" ? (
                <div className={classes.inputContainer}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div className={classes.input}>
                    <RangeDatePicker
                      disabled={
                        (item.id === 3 &&
                          !dateValue?.dateValue2?.startDate &&
                          !dateValue?.dateValue2?.endDate) ||
                        disableSideDateFilter
                      }
                      value={
                        dateValue && [
                          dateValue[`dateValue${item.id}`]?.startDate,
                          dateValue[`dateValue${item.id}`]?.endDate,
                        ]
                      }
                      onChange={
                        dateValue &&
                        ((date, dateString) => {
                          if (!date?.[0] && !date?.[1]) {
                            handleClearDateValue();
                          } else {
                            setDateValue({
                              ...dateValue,
                              [`dateValue${item.id}`]: {
                                startDate: date[0],
                                endDate: date[1],
                              },
                            });
                          }
                        })
                      }
                      placeholder={item.placeholder}
                      style={{
                        fontFamily: "FuturaMdBT",
                        padding: "4px 0px 4px 14px",
                        // width: "223px !important",
                      }}
                      disabledDate={(current) =>
                        item.id === 3 &&
                        current.isBetween(
                          moment(dateValue?.dateValue2?.startDate).subtract(
                            1,
                            "days"
                          ),
                          moment(dateValue?.dateValue2?.endDate).add(1, "days"),
                          "days"
                        )
                      }
                    />
                  </div>
                  {item.dash && (
                    <div className={classes.range}>{item.dash}</div>
                  )}
                </div>
              ) : item.type === "input" ? (
                <div className={classes.inputContainer}>
                  <TextField
                    placeholder={item.placeholder}
                    value={dateValue && dateValue[`dateValue${item.id}`]}
                    onChange={(e) =>
                      setDateValue({ ...dateValue, [`dateValue${item.id}`]: e })
                    }
                    style={{
                      width: "230px",
                    }}
                  />
                </div>
              ) : item.type === "dropdown2" ? (
                <div className={classes.lebar}>
                  <div>
                    {item.title === "" || item.title === undefined
                      ? ""
                      : `${item.title} :`}
                  </div>
                  <div style={{ color: "#0061A7", width: "100px" }}>
                    <DropdownSelectNoBorder
                      placeholder={item.placeholder}
                      options={item.options}
                      style={{
                        width: 99,
                        color: "#0061A7",
                        marginLeft: "-10px",
                        border: "#d9d9d9 !important",
                        outline: "none",
                      }}
                      value={dateValue && dateValue[`dropdown${item.id}`]}
                      onClick={() => {
                        if (advance) {
                          setAdvance(false);
                        } else {
                          setAdvance(true);
                        }
                      }}
                    />
                  </div>
                </div>
              ) : null
            )}

            <div className={classes.button} style={styleButton}>
              <Button
                label="Terapkan"
                width="115px"
                height="40px"
                onClick={handleFilter}
              />
            </div>
          </div>
          {advance ? (
            <div>
              {" "}
              <span
                style={{
                  marginLeft: "20px",
                  color: "#374062",
                  fontSize: "13px",
                  letterSpacing: "0.03em",
                  fontWeight: "400",
                  lineHeight: "16px",
                }}
              >
                Status Transaksi :{" "}
              </span>
              <Tabs
                value={value}
                tabs={[
                  "Semua",
                  "Berhasil",
                  "Menunggu Persetujuan",
                  "Gagal",
                  "Diproses",
                  "Ditolak",
                ]}
                onChange={onChange}
                indicatorColor="primary"
                textColor="primary"
                left
                style={{
                  marginLeft: 20,
                  marginBottom: "12px",
                  fontFamily: "FuturaMdBT",
                  fontStyle: "normal",
                  fontWeight: 400,
                  letterSpacing: "0.03em",
                  fontSize: "13px",
                }}
                selectedIndex="1"
                onClick={onClick}
              />
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

GeneralFilterTransaksi.propTypes = {
  options: PropTypes.array,
  className: PropTypes.string,
  align: PropTypes.string,
  dataFilter: PropTypes.object.isRequired,
  setDataFilter: PropTypes.func.isRequired,
  onClickButton: PropTypes.func,
  page: PropTypes.string,
  withTitle: PropTypes.bool,
  style: PropTypes.string,
};

GeneralFilterTransaksi.defaultProps = {
  style: "",
  options: [],
  className: "",
  align: "left",
  page: "",
  withTitle: true,
  onClickButton: () => {},
};

export default GeneralFilterTransaksi;
