// main
import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import moment from "moment";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import { TextStyle } from "components/BN/Atoms";
import Button from "../../Button/GeneralButton";
import Tabs from "../../Tabs/GeneralTabs";
// import DatePicker from "../../DatePicker/RangeDatePicker";
import RangeDatePicker from "../../DatePicker/RangeDatePicker";
import Date from "../../DatePicker/GeneralDatePicker";
import Search from "../../Button/ButtonDropDown/ButtonDropDown";
import TextField from "../../TextField/AntdTextField";
import { ReactComponent as SearchIcon } from "../../../../assets/icons/BN/search.svg";

const GeneralFilter = (props) => {
  const {
    options,
    className,
    align,
    dataFilter,
    setDataFilter,
    onClickButton,
    page,
    withTitle,
    style,
    input,
    onChange,
  } = props;
  const useStyles = makeStyles({
    filter: {
      width: "100%",
      minHeight: 70,
      backgroundColor: "#fff",
      borderRadius: 8,
      display: "flex",
      position: "relative",
    },
    title: {
      fontFamily: "FuturaMdBT",
      fontWeight: 600,
      marginLeft: 15,
      lineHeight: "73px",
      fontSize: 16,
    },
    button: {
      lineHeight: "73px",
      position: "absolute",
      right: 20,
      top: 0,
    },
    container: {
      display: "flex",
      flex: "auto",
      // justifyContent: 'space-evenly',
      paddingRight: 130,
      justifyContent: align === "center" ? "center" : "flex-start",
    },
    inputContainer: {
      display: "flex",
      lineHeight: "73px",
      alignItems: "center",
      margin: align === "center" ? "0 10px" : "0 0 0 20px",
    },
    inputContainerBlock: {
      display: "block",
      // lineHeight: "73px",
      alignItems: "center",
      margin: align === "center" ? "0 10px" : "0 0 0 20px",
    },
  });
  const classes = useStyles();

  const [tabValue, setTabValue] = useState({ tabValue1: 0 });
  const [dateValue, setDateValue] = useState(null);
  const [disabledDate, setDisabledDate] = useState(null);
  const [searchValue, setsearchValue] = useState(null);
  const [disableSideDateFilter, setDisableDateSideFilter] = useState(false);

  // const [openCalendar, setOpenCalendar] = useState(null);
  // const [placeholderState, setPlaceholderState] = useState(null);
  // console.log('LOOK VALUE', dateValue);
  // console.log('LOOK OPEN', openCalendar);
  // console.log('LOOK PLACE', placeholderState);

  const handleFilter = () => {
    setDisableDateSideFilter(false);
    onClickButton();
    setDataFilter({
      tabs: tabValue,
      date: dateValue,
    });
  };

  const handleTabsChange = (event, newValue, id) => {
    if (tabValue) {
      setTabValue({ ...tabValue, [`tabValue${id}`]: newValue });
    }
    if (options && dateValue) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };

  const handleDisabled = (current) =>
    current > disabledDate[0].startOf("day") &&
    current < disabledDate[1].endOf("day");

  useEffect(() => {
    if (options.length > 0) {
      const objTabs = {};
      const objDate = {};
      // const objPlace = {};
      options.map((item) => {
        if (item.type === "tabs") {
          objTabs[`tabValue${item.id}`] = dataFilter
            ? dataFilter.tabs[`tabValue${item.id}`]
            : 0;
        } else {
          objDate[`dateValue${item.id}`] =
            dataFilter && dataFilter.date[`dateValue${item.id}`];
          // objPlace[`placeholderState${item.id}`] = true;
        }
        return item;
      });
      setTabValue(objTabs);
      setDateValue(objDate);
      // setPlaceholderState(objPlace);
    }
  }, [dataFilter, options]);

  useEffect(() => {
    // reset tabs filter when date change
    if (options.length > 0) {
      const arr = [];
      options.forEach((item) => item.type === "date" && arr.push(item.id));
      options.forEach(
        (item) => item.type === "rangePicker" && arr.push(item.id)
      );
      const newArr = arr.map((item) => dateValue?.[`dateValue${item}`]);
      if (newArr.findIndex((item) => item) !== -1) {
        const objTabs = {};
        options.map((item) => {
          if (item.type === "tabs") {
            objTabs[`tabValue${item.id}`] = null;
          }
          return item;
        });
        setTabValue(objTabs);
      }
    }

    // disable date on rangepicker2 when rangepicker1 is empty date
    if (dateValue) {
      const arr = options.filter((item) => item.type === "date");
      arr.map((item, i) => {
        if (i === 0) {
          if (dateValue[`dateValue${item.id}`]) {
            setDisabledDate(dateValue[`dateValue${item.id}`]);
          } else {
            setDisabledDate(null);
          }
        }
        return item;
      });
    }
  }, [dateValue, options]);

  // clear date filter and clear disable tabs when date filter is empty
  const handleClearDateValue = () => {
    // const obj = {};
    // options.map((item) => {
    //   if (item.type === "rangePicker") {
    //     obj[`dateValue${item.id}`] = null;
    //   }
    //   return item;
    // });
    // setDateValue(obj);
    // setDisableSideTabsFilter(false);
    if (!disabledDate) {
      const obj = {};
      options.map((item) => {
        if (item.type === "date" || item.type === "rangePicker") {
          obj[`dateValue${item.id}`] = null;
        }
        return item;
      });
      setDateValue(obj);
    }
  };

  // clear date on rangepicker2 when rangepicker1 is empty date
  useEffect(() => {
    // handleClearDateValue();
  }, [disabledDate, options]);

  return (
    <div
      className={className ? `${className} ${classes.filter}` : classes.filter}
    >
      {withTitle && <div className={classes.title}>Period :</div>}
      <div className={classes.container}>
        {options.map((item, i) =>
          item.type === "tabs" ? (
            <div className={classes.inputContainer}>
              <Tabs
                value={tabValue && tabValue[`tabValue${item.id}`]}
                onChange={(event, newValue) => {
                  handleTabsChange(event, newValue, item.id);
                  if (page === "dashboard") {
                    setDisableDateSideFilter(true);
                  }
                }}
                tabs={item.options}
                style={{ height: "30px" }}
              />
            </div>
          ) : item.type === "date" ? (
            <div className={classes.inputContainer}>
              {/* {withTitle && <div className={classes.title}>Periode :</div>} */}
              <div>
                {/* {withTitle && <div className={classes.title}>Periodse :</div>} */}
                {item.title === "" || item.title === undefined
                  ? ""
                  : `${item.title} :`}
              </div>
              <div style={{ marginLeft: item.title ? 10 : 0 }}>
                <Date
                  value={dateValue && dateValue[`dateValue${item.id}`]}
                  onChange={
                    dateValue &&
                    ((date, dateString) =>
                      setDateValue({
                        ...dateValue,
                        [`dateValue${item.id}`]: date,
                      }))
                  }
                  placeholder={item.placeholder}
                  style={{
                    fontFamily: "FuturaMdBT",
                    padding: "4px 0px 4px 14px",
                  }}
                />
              </div>
              {item.dash && <div className={classes.range}>{item.dash}</div>}
            </div>
          ) : item.type === "datePicker" ? (
            <div
              className={
                item.dBlok
                  ? classes.inputContainerBlock
                  : classes.inputContainer
              }
            >
              <div>
                {item.title === "" || item.title === undefined
                  ? ""
                  : `${item.title} :`}
              </div>
              <div className={classes.input}>
                <Date
                  value={dateValue && dateValue[`dateValue${item.id}`]}
                  onChange={
                    dateValue &&
                    ((date, dateString) =>
                      setDateValue({
                        ...dateValue,
                        [`dateValue${item.id}`]: date,
                      }))
                  }
                  placeholder={item.placeholder}
                  style={{
                    fontFamily: "FuturaMdBT",
                    padding: "4px 0px 4px 14px",
                    marginLeft: item.dBlok ? 0 : 20,
                    marginRight: item.dBlok ? 20 : 0,
                  }}
                />
              </div>
              {item.dash && <div className={classes.range}>{item.dash}</div>}
            </div>
          ) : item.type === "select" ? (
            <div className={classes.inputContainer}>
              <div>
                <Search
                  value={searchValue && searchValue[`searchValue${item.id}`]}
                  onChange={(event, newValue) =>
                    handleTabsChange(event, newValue, item.id)
                  }
                  options={item.options}
                />
              </div>
            </div>
          ) : item.type === "rangePicker" ? (
            <div className={classes.inputContainer}>
              <div>
                {item.title === "" || item.title === undefined
                  ? ""
                  : `${item.title} :`}
              </div>
              <div className={classes.input}>
                <RangeDatePicker
                  // placeholder={["Star Date", "End Date"]}
                  disabled={
                    (item.id === 3 &&
                      !dateValue?.dateValue2?.startDate &&
                      !dateValue?.dateValue2?.endDate) ||
                    disableSideDateFilter
                  }
                  value={
                    dateValue && [
                      dateValue[`dateValue${item.id}`]?.startDate,
                      dateValue[`dateValue${item.id}`]?.endDate,
                    ]
                  }
                  onChange={
                    dateValue &&
                    ((date, dateString) => {
                      if (!date?.[0] && !date?.[1]) {
                        handleClearDateValue();
                      } else {
                        setDateValue({
                          ...dateValue,
                          [`dateValue${item.id}`]: {
                            startDate: date[0],
                            endDate: date[1],
                          },
                        });
                      }
                    })
                  }
                  placeholder={item.placeholder}
                  style={{
                    fontFamily: "FuturaMdBT",
                    padding: "4px 0px 4px 14px",
                  }}
                  disabledDate={(current) =>
                    item.id === 3 &&
                    current.isBetween(
                      moment(dateValue?.dateValue2?.startDate).subtract(
                        1,
                        "days"
                      ),
                      moment(dateValue?.dateValue2?.endDate).add(1, "days"),
                      "days"
                    )
                  }
                />
              </div>
              {item.dash && <div className={classes.range}>{item.dash}</div>}
            </div>
          ) : item.type === "input" ? (
            <div className={classes.inputContainer}>
              <TextField
                placeholder={item.placeholder}
                value={dateValue && dateValue[`dateValue${item.id}`]}
                onChange={(e) =>
                  setDateValue({ ...dateValue, [`dateValue${item.id}`]: e })
                }
                style={{
                  width: "230px",
                }}
              />
            </div>
          ) : item.type === "search" ? (
            <div className={classes.inputContainer}>
              <TextField
                placeholder={item.placeholder}
                value={input}
                onChange={(e) => onChange(e)}
                style={{
                  width: "345px",
                }}
                suffix={
                  <SearchIcon style={{ width: "16px", height: "16px" }} />
                }
              />
            </div>
          ) : null
        )}
      </div>
      {/* <div>
        {withTitle && (
          <p
            style={{
              marginLeft: "-910px",
              marginTop: -3,
            }}
          >
            Mulai dari :
          </p>
        )}{" "}
      </div>
      <div>
        {withTitle && (
          <p
            style={{
              marginLeft: "-680px",
              marginTop: -3,
            }}
          >
            Berakhir Pada :
          </p>
        )}
      </div> */}
      <div className={classes.button}>
        <Button
          label="Apply"
          width="104px"
          height="40px"
          onClick={handleFilter}
        />
      </div>
    </div>
  );
};

GeneralFilter.propTypes = {
  options: PropTypes.array,
  className: PropTypes.string,
  align: PropTypes.string,
  dataFilter: PropTypes.object.isRequired,
  setDataFilter: PropTypes.func.isRequired,
  onClickButton: PropTypes.func,
  page: PropTypes.string,
  withTitle: PropTypes.bool,
  style: PropTypes.string,
  input: PropTypes.string,
  onChange: PropTypes.func,
};

GeneralFilter.defaultProps = {
  style: "",
  options: [],
  className: "",
  align: "left",
  page: "",
  withTitle: true,
  onClickButton: () => {},
  input: "",
  onChange: () => {},
};

export default GeneralFilter;
