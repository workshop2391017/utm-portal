// main
import React, { useState } from "react";
import PropTypes from "prop-types";

// libraries
import { makeStyles } from "@material-ui/core";

// components
import TextField from "../TextField/AntdTextField";

// assets
import arrow from "../../../assets/icons/BN/chevron-down-white.svg";
import Title from "../Title";

import plus62 from "../../../assets/icons/BN/p62.svg";

const TextFieldPhone = (props) => {
  const {
    width,
    trx,
    dataSearch,
    setDataSearch,
    placeholder,
    name,
    onChange,
    value,
    onBlur,
    tlp,
    mt,
    maxLength,
    disabled,
  } = props;
  const useStyles = makeStyles({
    container: {
      marginTop: mt ? -8 : null,
    },
    box: {
      height: "44px",
      // marginBottom: '50px',
      display: "inline-block",
      width: 10,
      // marginTop: 3,
      position: "relative",
      fontFamily: "Futura",
      borderRadius: "8px 0 0 8px",
      border: "none",
      // width: 110,
      fontSize: 13,
      fontWeight: 700,
      color: "#fff",
      paddingLeft: "20px",
      backgroundColor: "#0061A7",
    },
    iconplus62: {
      height: "40px",
      width: tlp ? "103px" : "52px",
      fontFamily: "FuturaMdBT",
      // position: "relative",
      // left: "-px",
      display: "flex",
      fontSize: "15px",
      // justifyContent: "center",
      alignContent: "center",
      alignItems: "center",
      backgroundColor: "#0061A7",
      borderTopLeftRadius: "10px",
      borderBottomLeftRadius: "10px",
      padding: "0px 0px 0px 10px",
    },

    search: {
      height: "44px",
      display: "inline-block",
      "& .ant-input-affix-wrapper": {
        boxShadow: "none",
        marginTop: 8,
        "&:hover": {
          boxShadow: "none",
        },
        "& input": {
          color: "#0061A7",
          "&::placeholder": {
            color: "#BCC8E7",
          },
        },
        "&.ant-input:placeholder-shown": {
          fontSize: 15,
        },
        "&.ant-input-affix-wrapper-focused": {
          boxShadow: "none",
        },
      },
    },
  });

  const classes = useStyles();
  // const [input, setInput] = useState(null);

  // const clickSearch = () => {
  //   setDataSearch({
  //     input,
  //   });
  // };

  return (
    <div className={classes.container} style={{ display: "flex" }}>
      <div
        className={classes.iconplus62}
        style={{
          color: "white",
        }}
      >
        {trx ? "Rp" : "+62"}
      </div>
      <TextField
        placeholder={placeholder}
        value={value}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        icon={plus62}
        maxLength={maxLength}
        disabled={disabled}
        style={{
          width,
          height: "40px",
          borderRadius: "0px 10px 10px 0px ",
          border: "1px solid #BCC8E7",
          fontSize: "15px",
          marginLeft: "-1px",
          fontFamily: "FuturaBkBT",
        }}
      />
    </div>
  );
};

TextFieldPhone.propTypes = {
  dataSearch: PropTypes.object.isRequired,
  setDataSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  name: PropTypes.string,
  width: PropTypes.string,
  mt: PropTypes.bool,
  disabled: PropTypes.bool,
};

TextFieldPhone.defaultProps = {
  placeholder: "Masukan no telepon genggam Anda",
  value: "",
  name: "",
  width: "350px",
  mt: false,
  disabled: false,
};

export default TextFieldPhone;
