import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { Radio } from "antd";

const useStyles = makeStyles({
  radio: {
    fontSize: 15,
    minWidth: (props) => props.width,
    color: "#374062",
    display: (props) =>
      props.direction === "horizontal" ? "inline-block" : "block",
    marginBottom: 10,
    "& .ant-radio": {
      "& .ant-radio-inner": {
        width: 20,
        height: 20,
        border: "1px solid #BCC8E7",
        "&::after": {
          width: 10,
          height: 10,
          top: 4,
          left: 4,
          margin: "0px !important",
          transform: "scale(1) !important",
        },
      },
      "& .ant-radio-input": {
        width: 20,
        height: 20,
      },
    },
    "& .ant-radio-checked": {
      "& .ant-radio-inner": {
        border: "1px solid #0061A7 !important",
      },
    },
  },
});

const RadioGroup = ({ value, onChange, options, direction, width }) => {
  const classes = useStyles({ width, direction });

  return (
    <Radio.Group value={value} onChange={onChange} size="large">
      {options.map((item) => (
        <Radio value={item} key={item} className={classes.radio}>
          {item}
        </Radio>
      ))}
    </Radio.Group>
  );
};

RadioGroup.propTypes = {
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  direction: PropTypes.string,
  width: PropTypes.number,
};

RadioGroup.defaultProps = {
  options: [],
  direction: "horizontal",
  width: 140,
};

export default RadioGroup;
