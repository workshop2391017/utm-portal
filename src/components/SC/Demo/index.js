import { Box, CircularProgress, IconButton } from "@material-ui/core";
import ImageList from "@material-ui/core/ImageList";
import React, { useState } from "react";
import RichText from "components/BN/RichText";
import TableICBB from "components/BN/TableIcBB/collapse";
import Button from "../Button";
import { ReactComponent as Plus } from "../../../assets/icons/BN/plus.svg";
import Label from "../Label";
import CheckboxSingle from "../Checkbox/CheckboxSingle";
import CheckboxGroup from "../Checkbox/ChexboxGroup";
import DateTimePicker from "../DatePicker/DateTimePicker";
import GeneralDatePicker from "../DatePicker/GeneralDatePicker";
import RangeDatePicker from "../DatePicker/RangeDatePicker";
import DropdownAntd from "../Dropdown/DropdownAntd";
import DropdownMUI from "../Dropdown/DropdownMUI";
import OptionsItem from "../Dropdown/DropdownMUI/OptionsItem";
import RadioGroup from "../Radio/RadioGroup";
import BasicPopUp from "../BasicPopUp";
import TextfieldDate from "../../BN/TextfieldDate";
import TextfieldPhone from "../../BN/TextFieldPhone";
import CardSyarat from "../../BN/Cards/CardSyarat";

import calendar from "../../../assets/icons/BN/calendar.svg";
import checkCircle from "../../../assets/icons/BN/check-circle.svg";
import clipboard from "../../../assets/images/BN/clipboard.png";

const DemoSC = () => {
  const [checkboxSingle, setCheckboxSingle] = useState(false);
  const [checkboxGroup, setCheckboxGroup] = useState(["Uno"]);
  const [dateTimeValue, setDateTimeValue] = useState(null);
  const [dateValue, setDateValue] = useState(null);
  const [rangedateValue, setRangeDateValue] = useState(null);
  const [dropdownValue, setDropdownValue] = useState("Cuatro");
  const [radioValue, setRadioValue] = useState("Dos");
  const [successPopup, setSuccessPopup] = useState(false);

  // assets
  const dataDummyTableCheckingAccount = [
    {
      id: 1,
      name: "Local - To Local",
      child: [
        {
          corporateId: 1,
          dateTime: "12/10/2021",
          registration_id: "64282",
          corporateName: "PT. Indofood Tbk",
          status: "Waiting Approval",
          other: "Korporasi Besar",
          segmentation: "1",
          oleh: "ibblogin1",
        },
        {
          corporateId: 1,
          dateTime: "12/10/2021",
          registration_id: "64282",
          corporateName: "PT. Indofood Tbk",
          status: "Waiting Approval",
          other: "Korporasi Besar",
          segmentation: "1",
          oleh: "ibblogin1",
        },
        {
          corporateId: 1,
          dateTime: "12/10/2021",
          registration_id: "64282",
          corporateName: "PT. Indofood Tbk",
          status: "Waiting Approval",
          other: "Korporasi Besar",
          segmentation: "1",
          oleh: "ibblogin1",
        },
      ],
    },

    {
      id: 2,
      name: "Local",
      child: [
        {
          corporateId: 1,
          dateTime: "12/10/2021",
          registration_id: "64282",
          corporateName: "PT. Indofood Tbk",
          status: "Waiting Approval",
          other: "Korporasi Besar",
          segmentation: "1",
          oleh: "ibblogin1",
        },
        {
          corporateId: 1,
          dateTime: "12/10/2021",
          registration_id: "64282",
          corporateName: "PT. Indofood Tbk",
          status: "Waiting Approval",
          other: "Korporasi Besar",
          segmentation: "1",
          oleh: "ibblogin1",
        },
        {
          corporateId: 1,
          dateTime: "12/10/2021",
          registration_id: "64282",
          corporateName: "PT. Indofood Tbk",
          status: "Waiting Approval",
          other: "Korporasi Besar",
          segmentation: "1",
          oleh: "ibblogin1",
        },
      ],
    },
  ];

  const dataHeader = () => [
    {
      title: "Tanggal & Waktu",
      key: "dateTime",
    },
    {
      title: "Company ID",
      key: "corporateId",
    },
    {
      title: "Perusahaan",
      key: "corporateName",
    },
    {
      title: "Status",
      key: "status",
    },
    {
      title: "Segmentasi",
      key: "segmentation",
    },
    {
      title: "Oleh",
      key: "oleh",
    },
  ];

  return (
    <div>
      <h1>Shared components</h1>

      {/* --------------------------------- BUTTON --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<Button />"}</h2>
        <Button onClick={() => console.warn("Hola!")}>Simpan</Button>
        <Button outlined>Batal</Button>
        <Button color="#FF6F6F">Hapus</Button>
        <Button color="#75D37F">Setujui Perubahan</Button>
        <Button startIcon={<Plus />}>Pengguna Baru</Button>
        <Button endIcon={<Plus />}>Pilih Provinsi</Button>
        <Button disabled>Disabled</Button>
        <Button disabled>
          <CircularProgress size={20} />
        </Button>
        <IconButton>
          <img src={checkCircle} alt="Logo" />
        </IconButton>

        <Button text>Text Button</Button>
        <Box mt={2}>
          <Button fullWidth>Fullwidth Button</Button>
        </Box>
      </Box>

      {/* --------------------------------- LABEL --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<Label />"}</h2>
        <Label font="Futura" size={70} weight={700}>
          Login
        </Label>
        <Label font="Futura" weight={900} size={20} align="center">
          Perubahan Disimpan
        </Label>
        <Label font="Futura" weight={900} size={20} align="center">
          Menunggu Persetujuan
        </Label>
        <Label italic color="#FF6F6F">
          Simpan perubahan berfungsi untuk menyimpan seluruh perubahan di menu
          segmentasi
        </Label>
        <IconButton>
          <img src={clipboard} alt="Logo" />
        </IconButton>
      </Box>

      {/* --------------------------------- CHECKBOX --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<CheckboxSingle />"}</h2>
        <CheckboxSingle />
        <CheckboxSingle
          checked={checkboxSingle}
          label="Izinkan Semua Akses"
          onChange={() => setCheckboxSingle(!checkboxSingle)}
        />
        <CheckboxSingle color="purple" stroke="yellow" size={45} />

        <h2>{"<CheckboxGroup />"}</h2>
        <CheckboxGroup
          value={checkboxGroup}
          options={["Uno", "Dos", "Tres"]}
          onChange={(e) => setCheckboxGroup(e)}
        />
      </Box>

      {/* --------------------------------- DATE PICKER --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<DateTimePicker />"}</h2>
        <DateTimePicker
          placeholder="Pilih waktu"
          value={dateTimeValue}
          onChange={(e) => setDateTimeValue(e)}
          icon={calendar}
        />
        <DateTimePicker
          placeholder="With dateFormat"
          value={dateTimeValue}
          onChange={(e) => setDateTimeValue(e)}
          icon={calendar}
          dateFormat="DD-MM-YY"
        />

        <h2>{"<GeneralDatePicker />"}</h2>
        <GeneralDatePicker
          placeholder="Pilih tanggal"
          value={dateValue}
          onChange={(e) => setDateValue(e)}
          icon={calendar}
        />
        <GeneralDatePicker
          placeholder="With format"
          value={dateValue}
          onChange={(e) => setDateValue(e)}
          icon={calendar}
          format="DD/MM/YYYY"
        />

        <h2>{"<RangeDatePicker />"}</h2>
        <RangeDatePicker
          placeholder={["Dari tanggal", "Hingga tanggal"]}
          value={rangedateValue}
          onChange={(e) => setRangeDateValue(e)}
          icon={calendar}
        />
        <TextfieldDate />
        <TextfieldPhone />
      </Box>

      {/* --------------------------------- DROPDOWN --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<DropdownAntd />"}</h2>
        <DropdownAntd
          value={dropdownValue}
          options={["Uno", "Dos", "Tres", "Cuatro", "Cinco"]}
          onChange={(e) => setDropdownValue(e)}
        />

        <h2>{"<DropdownMUI />"}</h2>
        <DropdownMUI
          value={dropdownValue}
          onChange={(e) => setDropdownValue(e.target.value)}
          fullWidth={false}
        >
          <OptionsItem value="Uno">Uno</OptionsItem>
          <OptionsItem value="Dos">Dos</OptionsItem>
          <OptionsItem value="Tres">Tres</OptionsItem>
          <OptionsItem value="Cuatro">Cuatro</OptionsItem>
          <OptionsItem value="Cinco">Cinco</OptionsItem>
        </DropdownMUI>
      </Box>

      {/* --------------------------------- RADIO --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<RadioGroup />"}</h2>
        <RadioGroup
          value={radioValue}
          options={["Uno", "Dos", "Tres"]}
          onChange={(e) => setRadioValue(e.target.value)}
          direction="vertical"
        />
      </Box>

      {/* --------------------------------- POPUP --------------------------------- */}
      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <h2>{"<BasicPopUp />"}</h2>
        <Button onClick={() => setSuccessPopup(true)}>Success</Button>
        <BasicPopUp
          open={successPopup}
          title={
            <Label size={20}>Perubahan Disimpan Menunggu Persetujuan</Label>
          }
          image={checkCircle}
          width={320}
          onCancel={() => setSuccessPopup(false)}
        />

        <CardSyarat />
      </Box>

      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <div style={{ maxWidth: 636 }}>
          <RichText />
        </div>
      </Box>

      <Box width="75%" marginX="auto" border="1px solid black" p={3}>
        <TableICBB
          headerContent={dataHeader()}
          dataContent={dataDummyTableCheckingAccount}
          // page={page}
          // setPage={setPage}
          // totalData={totalPages}
          // isLoading={isLoading}
          selecable={false} // hide selecable
          collapse={false} // hide icon collapse
          defaultOpenCollapseBreakdown // open collapse default
          handleSelect={(e) => console.warn("EE =>", e)}
          handleSelectAll={(e) => console.warn("HandleSelectAll =>", e)}
          selectedValue={[]}
        />
      </Box>
    </div>
  );
};

export default DemoSC;
