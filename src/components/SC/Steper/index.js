/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
// Toggle Component
// --------------------------------------------------------

import React from "react";
import { styled } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector, {
  stepConnectorClasses,
} from "@material-ui/core/StepConnector";
import { Icon, Colors, TextStyle } from "../../BN/Atoms";

console.warn(stepConnectorClasses, "<<<< stepperConnector C;asses");

const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 22,
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      background: "#0061A7",
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      background: "#0061A7",
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    height: 5,
    border: 0,
    backgroundColor: "#E6EAF3",
    borderRadius: 1,
  },
}));

const ColorlibStepIconRoot = styled("div")(({ theme, ownerState }) => ({
  backgroundColor: "#F4F7FB",
  zIndex: 1,
  color: "#fff",
  width: 50,
  height: 50,
  display: "flex",
  borderRadius: "50%",
  justifyContent: "center",
  alignItems: "center",
  border: "1px solid #BCC8E7",
  ...(ownerState.active && {
    border: "1px solid #0061A7",
    background: "#EAF2FF",
    ".icon": {
      path: {
        // fill: "#0061A7",
        stroke: "#0061A7",
      },
    },
  }),
  ...(ownerState.completed && {
    background: "#EAF2FF",
    border: "1px solid #0061A7",
    ".icon": {
      path: {
        stroke: "#0061A7",
      },
    },
  }),
}));

function ColorlibStepIcon(props) {
  const { active, completed, className, icon } = props;
  // console.warn(className, "<<<< className");
  const iconName = icon || "grid";
  const icons = {
    grid: <Icon className="icon" name="grid" type="stroke" color="#BCC8E7" />,
    fileText: (
      <Icon type="stroke" name="file-text" className="icon" color="#BCC8E7" />
    ),
    tasks: (
      <Icon type="stroke" className="icon" name="clipboard" color="#BCC8E7" />
    ),
    thLarge: <Icon className="icon" type="stroke" />,
    upload: (
      <Icon
        className="icon"
        type="stroke"
        name="upload-stroke"
        color="#BCC8E7"
      />
    ),
    procuration: (
      <Icon className="icon" name="file" color="#BCC8E7" type="stroke" />
    ),
    mapPin: (
      <Icon className="icon" type="stroke" color="#BCC8E7" name="map-pin" />
    ),
  };

  return (
    <ColorlibStepIconRoot
      ownerState={{ completed, active }}
      className={className}
    >
      {icons[iconName]}
    </ColorlibStepIconRoot>
  );
}

const Mandatory = styled("span")(() => ({
  color: "#d14848",
  marginLeft: "3px",
}));

const StepperC = ({ steps, activeStep }) => (
  <Stepper
    alternativeLabel
    activeStep={activeStep}
    connector={<ColorlibConnector />}
  >
    {steps.map((el, index) => (
      <Step
        className={`step--${index}`}
        key={index}
        sx={{
          ".css-qivjh0-MuiStepLabel-label.MuiStepLabel-alternativeLabel": {
            marginTop: "5px",
          },
        }}
      >
        <StepLabel
          StepIconComponent={(prop) =>
            ColorlibStepIcon({ ...prop, icon: el.icon })
          }
        >
          <TextStyle
            size="13px"
            weight={500}
            sx={{ lineHeight: "17.27px", color: Colors.primary.hard }}
          >
            {el.label}
          </TextStyle>
          {el.isMandatory && <Mandatory>*</Mandatory>}
        </StepLabel>
      </Step>
    ))}
  </Stepper>
);

StepperC.propTypes = {
  steps: PropTypes.array,
  activeStep: PropTypes.number,
};

StepperC.defaultProps = {
  steps: [],
  activeStep: 0,
};

export default StepperC;
