import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Box, IconButton } from "@material-ui/core";
import Label from "../Label";
import Btn from "../Button";
import warning from "./assets/alert.png";
import { ReactComponent as Close } from "./assets/x.svg";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "#fff",
    width: (props) => (props.width ? props.width : 420),
    boxShadow: "0px 5px 15px rgba(188, 200, 231, 0.12)",
    padding: "40px 35px 30px",
    borderRadius: 10,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    position: "relative",
  },
}));

const BasicPopUp = ({
  open = false,
  title = "Perhatian!",
  image = warning,
  message,
  continueBtn,
  disableContinue,
  cancelBtn,
  onContinue,
  onCancel,
  closeBtn,
  children,
  width,
}) => {
  const classes = useStyles({ width });

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={onCancel}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 900,
      }}
    >
      <Fade in={open}>
        <div className={classes.paper}>
          {closeBtn && (
            <IconButton
              onClick={onCancel}
              size="small"
              style={{ position: "absolute", top: 10, right: 10 }}
            >
              <Close />
            </IconButton>
          )}
          <Label size={24} weight={600} align="center">
            {title}
          </Label>

          <img src={image} alt="" style={{ width: 120, margin: "20px 0" }} />
          {children || (
            <Label align="center" size={15}>
              {message}
            </Label>
          )}
          <Box
            display="flex"
            width="100%"
            justifyContent={cancelBtn ? "space-between" : "center"}
            mt={3}
          >
            {cancelBtn && (
              <Btn outlined onClick={onCancel}>
                {cancelBtn}
              </Btn>
            )}
            {continueBtn && (
              <Btn disabled={disableContinue} onClick={onContinue}>
                {continueBtn}
              </Btn>
            )}
          </Box>
        </div>
      </Fade>
    </Modal>
  );
};

export default BasicPopUp;
