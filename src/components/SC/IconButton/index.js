import React from "react";
import { makeStyles, IconButton } from "@material-ui/core";

const useStylesBN = makeStyles({
  primary: {
    fontFamily: "Futura",
    fontSize: 13,
    fontWeight: 700,
    borderRadius: 8,
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    "&:hover": {
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
  },
  disabled: {
    fontFamily: "Futura",
    fontSize: 13,
    fontWeight: 700,
    borderRadius: 8,
    backgroundColor: "#BCC8E7",
    boxShadow: "0px 6px 6px rgba(188, 200, 231, 0.2)",
    "&:hover": {
      boxShadow: "0px 6px 6px rgba(188, 200, 231, 0.2)",
      cursor: "not-allowed",
      backgroundColor: "#BCC8E7",
    },
  },
  disabledOutline: {
    fontFamily: "Futura",
    fontSize: 13,
    fontWeight: 700,
    borderRadius: 8,
    backgroundColor: "#F5F5F5 !important",
    "&:hover": {
      cursor: "not-allowed",
      border: "1px solid #BCC8E7",
    },
  },
  outlined: {
    fontFamily: "Futura",
    fontSize: 13,
    fontWeight: 700,
    borderRadius: 8,
    border: "1px solid #BCC8E7",
    backgroundColor: "transparent !important",
    boxShadow: "none",
  },
});

const ButtonIcon = ({
  disabled,
  outlined,
  text,
  icon,
  fullWidth,
  onClick,
  type,
  color,
  height,
  width,
  style,
  className,
}) => {
  const classes = useStylesBN();

  return (
    <IconButton
      type={type}
      variant={text ? null : outlined ? "outlined" : "contained"}
      disableElevation
      fullWidth={fullWidth}
      color={color}
      className={`${
        text
          ? null
          : disabled && outlined
          ? classes.disabledOutline
          : disabled && !outlined
          ? classes.disabled
          : outlined
          ? classes.outlined
          : classes.primary
      } ${className}`}
      onClick={onClick}
      style={{
        backgroundColor: color,
        pointerEvents: disabled ? "none" : null,
        textTransform: "none",
        height,
        width,
        ...style,
      }}
    >
      {icon}
    </IconButton>
  );
};

export default ButtonIcon;
