import { FormControlLabel } from "@material-ui/core";
import Check from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/styles";
import { Checkbox } from "antd";
import React, { memo } from "react";
import Label from "../../Label";

const CheckboxSingle = ({
  style,
  name,
  checked,
  onChange,
  label,
  color,
  stroke,
  size,
  disabled,
  indeterminate,
  defaultChecked,
}) => {
  const useStylesBN = makeStyles({
    check: {
      "&.ant-checkbox-wrapper": {
        fontSize: 15,
        color: "#374062",
        marginLeft: 0,
        textAlign: "left",
        "& .ant-checkbox": {
          width: 20,
          height: 20,
          borderRadius: 3,
          "& .ant-checkbox-inner": {
            width: 20,
            height: 20,
            borderRadius: 3,
            border: "1px solid #BCC8E7",
            "&::after": {
              width: 6.5,
              height: 11.17,
              top: "42%",
            },
          },
        },
        "& .ant-checkbox-checked": {
          "& .ant-checkbox-inner": {
            border: "1px solid #0061A7 !important",
          },
        },
        "& .ant-checkbox-indeterminate": {
          "& .ant-checkbox-inner::after": {
            width: "10px !important",
            height: "10px !important",
            top: "50% !important",
          },
        },
      },
    },
  });

  const classes = useStylesBN();

  return (
    <Checkbox
      style={style}
      name={name}
      checked={checked}
      onChange={onChange}
      className={classes.check}
      disabled={disabled}
      indeterminate={indeterminate}
      defaultChecked={defaultChecked}
    >
      {label}
    </Checkbox>
  );
};

export default memo(CheckboxSingle);
