import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { DatePicker } from "antd";

const useStyles = makeStyles({
  datePicker: {
    height: 40,
    width: 200,
    borderRadius: 6,
    backgroundColor: "#0061A7",
    border: "1px solid #0061A7",
    "&:hover": {
      boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
      border: "1px solid #0061A7",
    },
    "& .ant-picker-input": {
      cursor: "pointer",
      "& input": {
        color: "#0061A7",
        "&::placeholder": {
          color: "#0061A7",
        },
      },
      "& .ant-picker-suffix": {
        cursor: "pointer",
      },
      "& .ant-picker-clear": {
        width: 21,
        textAlign: "center",
        lineHeight: "22px",
      },
    },
  },
  dropdown: {
    zIndex: 1300,
    "& .ant-picker-panel-container": {
      borderRadius: 12,
      padding: 20,
      boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
      "& .ant-picker-panel": {
        border: "none",
        "& .ant-picker-date-panel": {
          "& .ant-picker-header": {
            border: "none",
            color: "#374062",
            "& .ant-picker-header-view": {
              fontFamily: "Futura",
              fontSize: 16,
            },
          },
          "& .ant-picker-body": {
            "& .ant-picker-content": {
              "& thead": {
                "& tr th": {
                  color: "#374062",
                },
              },
              "& tbody": {
                "& tr td": {
                  "&.ant-picker-cell": {
                    "& .ant-picker-cell-inner": {
                      borderRadius: "50%",
                    },
                  },
                  "&.ant-picker-cell-in-view": {
                    "& .ant-picker-cell-inner": {
                      color: "#06377B",
                      "&::before": {
                        display: "none",
                      },
                    },
                  },
                  "&.ant-picker-cell-selected": {
                    "& .ant-picker-cell-inner": {
                      borderRadius: "50%",
                      position: "absolute",
                      top: "50%",
                      left: "50%",
                      transform: "translate(-50%, -50%)",
                      lineHeight: "28px",
                      minWidth: 28,
                      height: 28,
                      color: "#F2F5FD !important",
                      "&::before": {
                        display: "none",
                      },
                    },
                  },
                },
              },
            },
          },
        },
        "& .ant-picker-month-panel": {
          "& .ant-picker-header": {
            border: "none",
            color: "#374062",
            "& .ant-picker-header-view": {
              fontFamily: "Futura",
              fontSize: 16,
            },
          },
          "& .ant-picker-body": {
            "& .ant-picker-content": {
              "& tbody": {
                "& tr td": {
                  "&.ant-picker-cell-in-view": {
                    "& .ant-picker-cell-inner": {
                      color: "#06377B",
                      "&::before": {
                        display: "none",
                      },
                    },
                  },
                  "&.ant-picker-cell-selected": {
                    "& .ant-picker-cell-inner": {
                      color: "#F2F5FD !important",
                    },
                  },
                },
              },
            },
          },
        },
        "& .ant-picker-year-panel": {
          "& .ant-picker-header": {
            border: "none",
            color: "#374062",
            "& .ant-picker-header-view": {
              fontFamily: "Futura",
              fontSize: 16,
            },
          },
          "& .ant-picker-body": {
            "& .ant-picker-content": {
              "& tbody": {
                "& tr td": {
                  "&.ant-picker-cell-in-view": {
                    "& .ant-picker-cell-inner": {
                      color: "#06377B",
                      "&::before": {
                        display: "none",
                      },
                    },
                  },
                  "&.ant-picker-cell-selected": {
                    "& .ant-picker-cell-inner": {
                      color: "#F2F5FD !important",
                    },
                  },
                },
              },
            },
          },
        },
        "& .ant-picker-decade-panel": {
          "& .ant-picker-header": {
            border: "none",
            color: "#374062",
            "& .ant-picker-header-view": {
              fontFamily: "Futura",
              fontSize: 16,
            },
          },
          "& .ant-picker-body": {
            "& .ant-picker-content": {
              "& tbody": {
                "& tr td": {
                  "&.ant-picker-cell-in-view": {
                    "& .ant-picker-cell-inner": {
                      color: "#06377B",
                      "&::before": {
                        display: "none",
                      },
                    },
                  },
                  "&.ant-picker-cell-selected": {
                    "& .ant-picker-cell-inner": {
                      color: "#F2F5FD !important",
                    },
                  },
                },
              },
            },
          },
        },
        "& .ant-picker-footer": {
          border: "none",
          "& .ant-picker-today-btn": {
            color: "#06377B",
          },
        },
      },
    },
  },
});

const DateTimePicker = ({
  value,
  onChange,
  style,
  placeholder,
  icon,
  dateFormat = "DD/MM/YYYY",
}) => {
  const classes = useStyles();

  return (
    <DatePicker
      showTime={{ format: "HH:mm A" }}
      format={`${dateFormat}, HH:mm A`}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      className={classes.datePicker}
      style={style}
      suffixIcon={<img alt="icon" src={icon} style={{ cursor: "pointer" }} />}
      dropdownClassName={classes.dropdown}
    />
  );
};

DateTimePicker.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};

DateTimePicker.defaultProps = {
  value: null,
  onChange: () => console.warn("click onChange"),
  style: null,
  placeholder: "Placeholder",
};

export default DateTimePicker;
