import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { DatePicker } from "antd";

const { RangePicker } = DatePicker;

const useStyles = makeStyles({
  datePicker: {
    height: 40,
    width: 280,
    borderRadius: 6,
    backgroundColor: "#0061A7",
    border: "1px solid #0061A7",
    cursor: "pointer",
    transition: "0.25s",
    "&:hover": {
      boxShadow: "0 0 0 2px rgb(87 168 233 / 20%)",
      border: "1px solid #0061A7",
    },
    "& .ant-picker-input": {
      "& input": {
        color: "#0061A7",
        "&::placeholder": {
          color: "#0061A7",
        },
      },
    },
    "& .ant-picker-suffix": {
      cursor: "pointer",
    },
    "& .ant-picker-clear": {
      width: 21,
      textAlign: "center",
      lineHeight: "22px",
    },
  },
  dropdown: {
    zIndex: 1300,
    "& .ant-picker-range-wrapper": {
      "& .ant-picker-panel-container": {
        borderRadius: 12,
        padding: 20,
        boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
        "& .ant-picker-panels": {
          "& .ant-picker-panel": {
            "&:last-child": {
              display: "none",
            },
            "&:first-child": {
              display: "block",
              border: "none",
              "& .ant-picker-date-panel": {
                "& .ant-picker-header": {
                  color: "#374062",
                  border: "none",
                  "& .ant-picker-header-view": {
                    fontFamily: "Futura",
                    fontSize: 16,
                  },
                  "& .ant-picker-header-next-btn": {
                    visibility: "visible !important",
                  },
                  "& .ant-picker-header-super-next-btn": {
                    visibility: "visible !important",
                  },
                },
                "& .ant-picker-body": {
                  "& .ant-picker-content": {
                    "& thead": {
                      "& tr th": {
                        color: "#374062",
                      },
                    },
                    "& tbody": {
                      "& tr": {
                        "& td": {
                          position: "relative",
                          "&.ant-picker-cell-today": {
                            "& .ant-picker-cell-inner::before": {
                              border: "none",
                            },
                          },
                          "&.ant-picker-cell-range-start": {
                            "& .ant-picker-cell-inner": {
                              borderRadius: "50%",
                              position: "absolute",
                              top: "50%",
                              left: "50%",
                              transform: "translate(-50%, -50%)",
                              lineHeight: "28px",
                              minWidth: 28,
                              height: 28,
                              color: "#F2F5FD",
                            },
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                            "&.ant-picker-cell-range-start-single::before": {
                              backgroundColor: "transparent",
                            },
                          },
                          "&.ant-picker-cell-range-end": {
                            "& .ant-picker-cell-inner": {
                              borderRadius: "50%",
                              position: "absolute",
                              top: "50%",
                              left: "50%",
                              transform: "translate(-50%, -50%)",
                              lineHeight: "28px",
                              minWidth: 28,
                              height: 28,
                              color: "#F2F5FD",
                            },
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                          },
                          "&.ant-picker-cell-in-range": {
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                          },
                          "&.ant-picker-cell-in-view": {
                            "& .ant-picker-cell-inner": {
                              color: "#06377B",
                            },
                          },
                          "&.ant-picker-cell": {
                            "& .ant-picker-cell-inner": {
                              borderRadius: "50%",
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
              "& .ant-picker-month-panel": {
                "& .ant-picker-header": {
                  color: "#374062",
                  border: "none",
                  "& .ant-picker-header-view": {
                    fontFamily: "Futura",
                    fontSize: 16,
                  },
                },
                "& .ant-picker-body": {
                  "& .ant-picker-content": {
                    "& thead": {
                      "& tr th": {
                        color: "#374062",
                      },
                    },
                    "& tbody": {
                      "& tr": {
                        "& td": {
                          "&.ant-picker-cell-range-start": {
                            "& .ant-picker-cell-inner": {
                              color: "#F2F5FD",
                            },
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                            "&.ant-picker-cell-range-start-single::before": {
                              backgroundColor: "transparent",
                            },
                          },
                          "&.ant-picker-cell-range-end": {
                            "& .ant-picker-cell-inner": {
                              color: "#F2F5FD",
                            },
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                          },
                          "&.ant-picker-cell-in-range": {
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                          },
                          "&.ant-picker-cell-in-view": {
                            "& .ant-picker-cell-inner": {
                              color: "#06377B",
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
              "& .ant-picker-year-panel": {
                "& .ant-picker-header": {
                  color: "#374062",
                  border: "none",
                  "& .ant-picker-header-view": {
                    fontFamily: "Futura",
                    fontSize: 16,
                  },
                },
                "& .ant-picker-body": {
                  "& .ant-picker-content": {
                    "& thead": {
                      "& tr th": {
                        color: "#374062",
                      },
                    },
                    "& tbody": {
                      "& tr": {
                        "& td": {
                          "&.ant-picker-cell-range-start": {
                            "& .ant-picker-cell-inner": {
                              color: "#F2F5FD",
                            },
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                            "&.ant-picker-cell-range-start-single::before": {
                              backgroundColor: "transparent",
                            },
                          },
                          "&.ant-picker-cell-range-end": {
                            "& .ant-picker-cell-inner": {
                              color: "#F2F5FD",
                            },
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                          },
                          "&.ant-picker-cell-in-range": {
                            "&::before": {
                              backgroundColor: "#E6EAF3",
                            },
                          },
                          "&.ant-picker-cell-in-view": {
                            "& .ant-picker-cell-inner": {
                              color: "#06377B",
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
              "& .ant-picker-decade-panel": {
                "& .ant-picker-header": {
                  color: "#374062",
                  border: "none",
                  "& .ant-picker-header-view": {
                    fontFamily: "Futura",
                    fontSize: 16,
                  },
                },
                "& .ant-picker-body": {
                  "& .ant-picker-content": {
                    "& thead": {
                      "& tr th": {
                        color: "#374062",
                      },
                    },
                    "& tbody": {
                      "& tr": {
                        "& td": {
                          "&.ant-picker-cell-in-view": {
                            "& .ant-picker-cell-inner": {
                              color: "#06377B",
                            },
                          },
                          "&.ant-picker-cell-selected": {
                            "& .ant-picker-cell-inner": {
                              color: "#F2F5FD !important",
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  placeholder: {
    fontSize: 15,
    color: "#0061A7",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    lineHeight: "40px",
    borderRadius: 6,
    textAlign: "center",
    cursor: "pointer",
    display: "flex",
    border: "1px solid #0061A7",
    overflow: "hidden",
  },
  container: {
    width: "fit-content",
    height: 40,
    position: "relative",
  },
});

const RangeDatePicker = ({
  value,
  onChange,
  style,
  placeholder,
  datePickerID,
  icon,
  format,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <RangePicker
        value={value}
        onChange={onChange}
        className={`picker${datePickerID} ${classes.datePicker}`}
        style={style}
        suffixIcon={<img alt="icon" src={icon} style={{ cursor: "pointer" }} />}
        dropdownClassName={`rangeDropdown${datePickerID} ${classes.dropdown}`}
        placeholder={placeholder}
        format={format}
      />
    </div>
  );
};

RangeDatePicker.propTypes = {
  value: PropTypes.array,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.array,
  datePickerID: PropTypes.number,
  format: PropTypes.string,
};

RangeDatePicker.defaultProps = {
  value: null,
  onChange: () => console.warn("onChange"),
  style: null,
  placeholder: ["Start", "End"],
  datePickerID: 0,
  format: "DD/MM/YYYY",
};

export default RangeDatePicker;
