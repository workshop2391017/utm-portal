import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { Select } from "antd";
import arrow from "../../../../assets/icons/BN/chevron-down-light.svg";

const useStyles = makeStyles(() => ({
  select: {
    "& .ant-select-selector": {
      minWidth: 200,
      height: "40px !important",
      border: "1px solid #BCC8E7 !important",
      borderRadius: "5px !important",
      "& .ant-select-selection-search input": {
        height: "40px !important",
        lineHeight: "40px",
      },
      "& .ant-select-selection-item": {
        lineHeight: "40px",
        fontSize: 15,
        color: "#BCC8E7",
      },
    },
    "& .ant-select-arrow": {
      width: 20,
      height: 20,
      top: "41%",
      transition: "0.3s",
    },
    "&.ant-select-open": {
      "& .ant-select-arrow": {
        top: "41%",
        transform: "rotate(-180deg)",
      },
    },
  },
  dropdown: {
    zIndex: 1300,
    boxShadow: "0px 6px 16px rgba(179, 202, 244, 0.6)",
    "& .ant-select-item-option-active": {
      "&:hover": {
        backgroundColor: "#F4F7FB",
      },
    },
    "& .ant-select-item-option-selected": {
      backgroundColor: "#cfe0e6 !important",
    },
  },
  arrow: {
    height: 20,
    width: 20,
    objectFit: "cover",
    objectPosition: "center",
  },
}));

const DropdownAntd = ({ value, onChange, options, style, placeholder }) => {
  const classes = useStyles();

  return (
    <Select
      value={value}
      onChange={onChange}
      className={classes.select}
      style={style}
      placeholder={placeholder}
      dropdownClassName={classes.dropdown}
      suffixIcon={<img alt="arrow" src={arrow} className={classes.arrow} />}
    >
      {options.map((option) => (
        <Select.Option value={option}>{option}</Select.Option>
      ))}
    </Select>
  );
};

DropdownAntd.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  style: PropTypes.object,
  placeholder: PropTypes.string,
};

DropdownAntd.defaultProps = {
  options: [],
  style: null,
  placeholder: "",
};

export default DropdownAntd;
