import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { ReactComponent as ChevronDown } from "./assets/chevron-down.svg";
import Label from "../../Label";

const useStyles = makeStyles((theme) => ({
  select: {
    marginTop: 5,

    "& .MuiOutlinedInput-input": {
      padding: "10px 44px 10px 10px",
    },
    "& .MuiInputBase-input": {
      fontSize: 15,
      backgroundColor: "#FFF",
    },
    "& .MuiSelect-icon": {
      color: "#BC0000",
    },
    "& .MuiOutlinedInput-notchedOutline": {
      border: "1px solid #BCC8E7",
    },
    "&.MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline": {
      borderColor: "#BCC8E7",
    },
    "&.MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: "#6889FF",
      borderWidth: 1,
    },

    "& .MuiSelect-select:focus": {
      backgroundColor: "#fff",
    },
  },
}));

const DropdownMUI = ({
  label,
  value,
  onChange,
  children,
  fullWidth = true,
  icon,
}) => {
  const classes = useStyles();

  return (
    <FormControl variant="outlined" fullWidth={fullWidth}>
      {label && <Label>{label}</Label>}
      <Select
        value={value}
        onChange={onChange}
        displayEmpty
        IconComponent={icon || ChevronDown}
        className={classes.select}
      >
        {children}
      </Select>
    </FormControl>
  );
};

export default DropdownMUI;
