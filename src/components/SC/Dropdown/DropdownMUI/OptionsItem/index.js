import React from "react";
import { MenuItem } from "@material-ui/core";

const OptionsItem = React.forwardRef((props, ref) => (
  <MenuItem innerRef={ref} {...props} style={{ fontSize: 15 }}>
    {props.children}
  </MenuItem>
));

export default OptionsItem;
