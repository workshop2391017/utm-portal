import React from "react";
import { BrowserRouter } from "react-router-dom";
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import ReactDOM from "react-dom";
// Redux
import { Provider } from "react-redux";
import store from "./stores/store";
import * as serviceWorker from "./serviceWorker";
import Route from "./router";
import "./utils/translation";
import UserService from "services/UserService";
import App from "./App";

// import AuthRoute from './containers/AuthRoute';
// import PrivateRoute from './containers/PrivateRoute';
// import LoginScreen from './containers/BN/LoginScreen';
// import Ringkasan from './containers/BN/Ringkasan';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <Route />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
