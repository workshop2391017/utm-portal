import { createTheme } from "@material-ui/core/styles";
import * as Colors from "./colors";

const themeBN = createTheme({
  containedButton: {
    color: "white",
    borderRadius: 10,
    "&.MuiButton-containedPrimary.Mui-disabled": {
      backgroundColor: "#BCC8E7",
    },
    "&:hover": {
      boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    },
  },
  typography: {
    fontFamily: "FuturaBkBT",
    fontWeight: "normal",
    gray: {
      fontFamily: "FuturaBQ",
      fontWeight: 500,
      fontSize: 13,
      color: "#7B87AF",
    },
    // Card Header
    h1: {
      fontFamily: "FuturaMdBT",
      fontWeight: 400,
      fontSize: 24,
      letterSpacing: "0.01em",
      color: "white",
    },
    // Page Header
    h2: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: 20,
      letterSpacing: "0.03em",
    },
    h6: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: 15,
      letterSpacing: "0.03em",
    },
    body1: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: 15,
      letterSpacing: "0.03em",
    },
    body2: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: 13,
      letterSpacing: "0.03em",
    },
    subtitle1: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: 13,
      letterSpacing: "0.01em",
    },
    subtitle2: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: 15,
      letterSpacing: "0.01em",
    },
    // Back Button Header
    backButton: {
      textTransform: "none",
      fontFamily: "FuturaMdBT",
      fontWeight: 700,
      fontSize: 13,
      letterSpacing: "0.03em",
      color: "#0061A7",
    },
    // Save, Cancel
    actionButton: {
      textTransform: "none",
      fontFamily: "FuturaMdBT",
      fontWeight: 700,
      fontSize: 15,
      letterSpacing: "0.03em",
    },
    noDataMessage: {
      fontFamily: "FuturaHvBT",
      fontWeight: 400,
      fontSize: 21,
      letterSpacing: "0.01em",
      color: "#7B87AF",
    },
    noDataSubmessage: {
      fontFamily: "FuturaBkBT",
      fontWeight: 400,
      fontSize: 13,
      letterSpacing: "0.03em",
      color: "#7B87AF",
    },
  },
  palette: {
    text: { primary: Colors.Dark },
    primary: {
      light: Colors.PrimarySoft,
      main: Colors.PrimaryMedium,
      dark: Colors.PrimaryDark,
      contrastText: Colors.White,
    },
    secondary: {
      main: Colors.Dark,
    },
    success: { main: Colors.SecondaryMedium },
    error: { main: Colors.RedMedium },
    background: {
      default: Colors.GrayUltrasoft,
    },
  },
  status: {
    active: Colors.SecondaryMedium,
    hold: Colors.RedMedium,
  },
});

const theme = themeBN;

export default theme;
