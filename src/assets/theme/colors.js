// collor assets
export const PrimaryDark = "#004a80";
export const PrimaryMedium = "#0061A7";
export const PrimarySoft = "#207dc0";
export const SecondaryMedium = "#99CC00";
export const GrayMedium = "#B0BFD3";
export const Dark = "#374062";
export const GrayUltrasoft = "#F6FAFF";
export const RedHard = "#E31C23";
export const RedMedium = "#FF8970";
export const BlueHard = "#137EE1";
export const BlueMedium1 = "#0061A7";
export const White = "#fff";
