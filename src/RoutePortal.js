import React from "react";
import { Redirect, Route } from "react-router-dom";
import { getLocalStorage } from "utils/helpers";

// LIST ROLE
// "ADMIN_MAKER"
// "ADMIN_APPROVER"
// "ADMIN_MAKER_APPROVER"

const isValidateRoute = false;

const isAuthenticated = (userRole, menuId) => {
  if (!isValidateRoute) {
    return true;
  }

  const lsData = getLocalStorage("portalUserLogin");
  const userData = lsData ? JSON.parse(lsData) : null;
  console.warn(userData, "ini user data", userRole, menuId);

  if (userData === null) {
    return false;
  }

  if (!userRole.includes(userData.role)) {
    return false;
  }

  let isValidMenu = false;
  if (userData?.portalGroupDetailDtoList[0]) {
    const listMasterMenu = userData.portalGroupDetailDtoList[0].masterMenu;
    listMasterMenu.forEach((itemMenu) => {
      if (itemMenu.id === menuId) {
        isValidMenu = true;
        return false;
      }
      if (itemMenu.menuAccessChild.lenght > 0) {
        itemMenu.menuAccessChild.forEach((childMenu) => {
          if (childMenu.id === menuId) {
            isValidMenu = true;
            return false;
          }
        });
      }
    });
  }

  return isValidMenu;
};

const RoutePortal = ({ component: Component, userRole, menuId, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated(userRole || [], menuId) ? <Component {...props} /> : null
    }
  />
);

export default RoutePortal;
