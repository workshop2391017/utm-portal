// main
import React, { useState, useEffect, createContext } from "react";
import {
  Redirect,
  Route,
  Switch,
  useHistory,
  useLocation,
} from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";
import {
  Box,
  CircularProgress,
  Dialog,
  DialogContent,
  makeStyles,
} from "@material-ui/core";

// Keycloak
// import { useKeycloak } from "@react-keycloak/web";

import offline from "assets/icons/BN/offline.svg";
import PopUpErrorBE from "assets/images/BN/PopUpErrorBE.png";
import DeviceLogin from "assets/icons/BN/device-login.svg";

// Redux
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import {
  setPopUpOffline,
  setPopUpErrorBE,
  setPopUpExpired,
  setIsReloaded,
  handleClearError,
} from "stores/actions/errorGeneral";
import SuccessConfirmation from "components/BN/Popups/SuccessConfirmation";
import { handleLogout, handleLogoutDoubleTab } from "stores/actions/login";
import ProtectedRoute from "ProtectedRoute";
import RoutePortal from "RoutePortal";
import SystemMaintenance from "components/BN/Popups/SystemMaintenance";

// containers
import { pathnameCONFIG } from "configuration";

import ProductRekening from "containers/BN/Transaction-Maintanance/ProductRekening";
import MenuGlobal from "containers/BN/Transaction-Maintanance/MenuGlobal";
import MenuGlobalEdit from "containers/BN/Transaction-Maintanance/MenuGlobal/Edit";
import CompanyLimit from "containers/BN/Transaction-Maintanance/CompanyLimit";
import CompanyLimitDetail from "containers/BN/Transaction-Maintanance/CompanyLimit/Details";
import PersetujuanMatrixAdminBank from "containers/BN/PersetujuanMatriks/AdminBank";
import EditAdminBank from "containers/BN/PersetujuanMatriks/AdminBank/EditAdminBank";
import EditNasabah from "containers/BN/PersetujuanMatriks/Nasabah/EditNasabah";
import ContentAccessDetail from "containers/BN/Utilitas/ContentAccess/details";
import ContentAccessEdit from "containers/BN/Utilitas/ContentAccess/details/Ubah";
import TambahResponseSetting from "containers/BN/ResponseSetting/Tambah";
import Toast from "components/BN/Toats";
import DetailApprovalWorkflowBillerManager from "containers/BN/ApprovalWorkflow/Detail/PaymentSetup/DetailBillerManager";
import DetailApprovalWorkflowBillersCategory from "containers/BN/ApprovalWorkflow/Detail/PaymentSetup/DetailBillersCategory";
import DetailContactSupportResponse from "containers/BN/ApprovalWorkflow/Detail/Utility/DetailContactSupportResponse";
import DetailEventCalendar from "containers/BN/ApprovalWorkflow/Detail/Utility/DetailEventCalendar";
import DetailContentManagement from "containers/BN/ApprovalWorkflow/Detail/Utility/DetailContentManagement";
import DetailProductInformation from "containers/BN/ApprovalWorkflow/Detail/Utility/DetailProductInformation";
import TypeSegment from "containers/BN/Transaction-Maintanance/TypeSegment";
import GeneralErrorPopups from "components/BN/Popups/GeneralErrorPoups";
import DetailUserSetting from "containers/BN/ApprovalWorkflow/Detail/Utility/DetailUserSetting";
import NotifInformation from "containers/BN/NotifInformation";
import {
  getLocalStorage,
  getSessionStorage,
  makeId,
  setLocalStorage,
  setSessionStorage,
} from "utils/helpers";

import CompanyLoginDetail from "containers/BN/ConsolidationReport/Detail/CompanyLoginDetail";
import DetailBlastNotification from "containers/BN/ApprovalWorkflow/Detail/DetailBlastNotification";
import DetailFaq from "containers/BN/ApprovalWorkflow/Detail/DetailFaq";
import ContainerBN from "./layout/BN";
import LoginScreen from "./containers/BN/LoginScreen";
import Ringkasan from "./containers/BN/Ringkasan";
import BisnisPengaturanUserSetting from "./containers/BN/BisnisPengaturan/UserSetting";
import BisnisPengaturanBillerKategori from "./containers/BN/BisnisPengaturan/BillerKategori";

import BisnisPengaturanGeneralParameter from "./containers/BN/BisnisPengaturan/GeneralParameter";
import Merchant from "./containers/BN/Merchant";
// import BisnisPengaturanNotifikasi from "./containers/BN/BisnisPengaturan/LogNotifikasi";
import SecurityApprovalPengguna from "./containers/BN/Security/ApprovalPengguna";

import SecurityApprovalConfig from "./containers/BN/Security/ApprovalConfig";
import SecurityDetailApproval from "./containers/BN/Security/ApprovalConfig/DetailApproval";
import DetailApproval from "./containers/BN/Security/ApprovalPengguna/DetailApproval";
import PengaturanPeran from "./containers/BN/Security/PengaturanPeran";
import LoginKategori from "./containers/BN/LoginKategori";
import DemoSC from "./components/SC/Demo";
import MaintananceUmum from "./containers/BN/MaintananceUmum/HostErrormapping";
import DomesticBank from "./containers/BN/MaintananceUmum/DomesticBank";
import ExchangeRate from "./containers/BN/MaintananceUmum/ExchangeRate";
import HandlingOfficer from "./containers/BN/MaintananceUmum/HandlingOfficer";
import InstitusiVa from "./containers/BN/MaintananceUmum/InstitusiVa";
import DomesticBankDetails from "./containers/BN/MaintananceUmum/DomesticBank/Details";
import OrganizationUnit from "./containers/BN/MaintananceUmum/OrganizationUnit";
import InstitusiVaDetail from "./containers/BN/MaintananceUmum/InstitusiVa/Detail";
import AddNewOrganizationUnit from "./containers/BN/MaintananceUmum/OrganizationUnit/Detail/addNew";
import PendingTaskRegistration from "./containers/BN/PendingTask/Registrasi";
import PendingTaskChangeAuthorization from "./containers/BN/PendingTask/UbahOtorisasi";
import PendingTaskActivationVa from "./containers/BN/PendingTask/PengaktivanVa";
import PendingTaskRegistrationSoftToken from "./containers/BN/PendingTask/PendaftaranSoftToken";
import PendingTaskRegistrationDetail from "./containers/BN/PendingTask/Registrasi/Detail";
import PendingTaskDetailVa from "./containers/BN/PendingTask/PengaktivanVa/Detail";
import PendingTaskChangeAuthorizationDetail from "./containers/BN/PendingTask/UbahOtorisasi/Detail";
import InternationalBank from "./containers/BN/MaintananceUmum/InternationalBank";
import InternationalBankUbah from "./containers/BN/MaintananceUmum/InternationalBank/Ubah";
import InternationalBankTambah from "./containers/BN/MaintananceUmum/InternationalBank/Tambah";
import GlobalLimitTrx from "./containers/BN/Transaction-Maintanance/GlobalLimitTrx";
import PendingTaskAutoCollection from "./containers/BN/PendingTask/AutoCollection";
import DetailPendingTaskAutoCollection from "./containers/BN/PendingTask/AutoCollection/Detail";
import TransactionStatus from "./containers/BN/StatusTransaksi";

import StatusNonTransaksi from "./containers/BN/StatusNonTransaksi";
// assets
import theme from "./assets/theme/theme";

import CompanyCharge from "./containers/BN/Transaction-Maintanance/CompanyCharge";
import CompanyChargeDetail from "./containers/BN/Transaction-Maintanance/CompanyCharge/Details";
import TimeDepositeSpecialRate from "./containers/BN/Transaction-Maintanance/TimeDepositeSpecialRate";
import TambahTimeDepositeSpecialRate from "./containers/BN/Transaction-Maintanance/TimeDepositeSpecialRate/Tambah";
import CutOffTime from "./containers/BN/PengaturanCuttTime";
import CutOffTimeForm from "./containers/BN/PengaturanCuttTime/Form/Form";
// ! Nanti ga perlu 2 page
// import PengaturanCutOffTimeEdit from "./containers/BN/PengaturanCuttTime/Edit";
import ApprovalWorkflow from "./containers/BN/ApprovalWorkflow";
// import DetailApprovalWorkflow from "./containers/BN/ApprovalWorkflow/Detail/DetailTemplate";
import ListPromo from "./containers/BN/ListPromo";
import ListPromoApprovalWf from "./containers/BN/ApprovalWorkflow/Detail/ManagementPromo/ListPromo";

import TambahPromo from "./containers/BN/ListPromo/TambahPromo";
import AddMerchant from "./containers/BN/Merchant/AddMerchant";

import PromoCategory from "./containers/BN/PromoCategory";
import PromoCategoryWorkflow from "./containers/BN/ApprovalWorkflow/ManagementPromo/PromoCategory";
// import AddMerchant from "./containers/BN/BisnisPengaturan/Merchant/AddMerchant";
import ListJenisPromo from "./containers/BN/ListJenisPromo";
import AddSubCategory from "./containers/BN/ListJenisPromo/AddSubCategory";
import CalendarEvent from "./containers/BN/Utilitas/CalendarEvent";
import OnlineSubmissionReport from "./containers/BN/OnlineSubmissionReport/VirtualAccount";
import OnlineSubmissionReportDetail from "./containers/BN/OnlineSubmissionReport/VirtualAccount/detail";
import AutoCollectionReport from "./containers/BN/OnlineSubmissionReport/AutoCollection";
import AutoCollectionDetail from "./containers/BN/OnlineSubmissionReport/AutoCollection/detail";
import VaActivation from "./containers/BN/OnlineSubmissionReport/VirtualAccount/VaActivation";
import SegmentationReport from "./containers/BN/ConsolidationReport";
import SegmentasiReportDetail from "./containers/BN/ConsolidationReport/Detail";

import UtilitasManagemenrUser from "./containers/BN/Utilitas/ManagementUser";
import UtilitasManagemenrUserAddNew from "./containers/BN/Utilitas/ManagementUser/addUser";
import UtilitasManagementUserDetail from "./containers/BN/Utilitas/ManagementUser/detail";
import UtilitasProductInformation from "./containers/BN/Utilitas/ProductInformation";
import ContentAccess from "./containers/BN/Utilitas/ContentAccess";
import ResponseSetting from "./containers/BN/ResponseSetting";
import BillerManager from "./containers/BN/PaymentSetup/BillerManager";
import BillerManagerForm from "./containers/BN/PaymentSetup/BillerManager/Form";
import BillerKategori from "./containers/BN/PaymentSetup/BillerKategori";
import BillerKategoriEdit from "./containers/BN/PaymentSetup/BillerKategori/edit";

import GLobalLimitTrxDetails from "./containers/BN/Transaction-Maintanance/GlobalLimitTrx/Details";
import DetailApprovalWorkflowMenuGlobal from "./containers/BN/ApprovalWorkflow/Detail/GlobalMaintenance/menuGlobal";
import DetailApprovalWorkProdukRekening from "./containers/BN/ApprovalWorkflow/Detail/GlobalMaintenance/produkRekening";
import DetailApprovalWorkLimtTransaksi from "./containers/BN/ApprovalWorkflow/Detail/GlobalMaintenance/limitTransaksi";
import DetailApprovalWorkService from "./containers/BN/ApprovalWorkflow/Detail/GlobalMaintenance/service";
import DetailApprovalWorkflowhostErrormapping from "./containers/BN/ApprovalWorkflow/Detail/MaintenanceUmum/hostErrormapping";
import DetailApprovalWorkflowDomesticBank from "./containers/BN/ApprovalWorkflow/Detail/MaintenanceUmum/domesticBank";
import DetailApprovalWorkflowInternationalBank from "./containers/BN/ApprovalWorkflow/Detail/MaintenanceUmum/internationalBank";
import DetailApprovalWorkflowHandlingOfficer from "./containers/BN/ApprovalWorkflow/Detail/MaintenanceUmum/handlingOfficer";
import DetailApprovalWorkflowChargeList from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/chargeList";
import DetailApprovalWorkflowChargePackage from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/chargePackage";
import DetailApprovalWorkflowDaftarSegmentasi from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/daftarSegmentasi";
import DetailApprovalWorkflowJenisRekeningProduk from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/jenisRekeningProduk";
import DetailApprovalWorkflowLimitPackage from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/limitPackage";
import DetailApprovalWorkflowMenuPackage from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/menuPackage";
import DetailApprovalWorkflowSegmenCabang from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/segmenCabang";
import DetailApprovalWorkflowServiceChargeList from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/serviceChargeList";

import DetailApprovalWorkflowTieringSlab from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/tieringSlab";

import DetailApprovalWorkflowMenuKhusus from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/menuKhusus";
import DetailApprovalWorkflowTypeParameter from "./containers/BN/ApprovalWorkflow/Detail/SegmentasiNasabah/typeParameter";
import DetailApprovalWorkflowCompanyLimit from "./containers/BN/ApprovalWorkflow/Detail/CompanyLimitCharge/CompanyLimit";
import DetailApprovalWorkflowCompanyCharge from "./containers/BN/ApprovalWorkflow/Detail/CompanyLimitCharge/CompanyCharge";
import PeriodicalChargeList from "./containers/BN/PeriodicalChargeList";
import ContentManagement from "./containers/BN/Utilitas/ContentManagement";
import ContentManagementEdit from "./containers/BN/Utilitas/ContentManagement/edit";

import PersetujuanMatrixNasabah from "./containers/BN/PersetujuanMatriks/Nasabah";

import PersetujuanMatrix from "./containers/BN/Security/PersetujuanMatrix";
import PersetujuanMatrixDetail from "./containers/BN/Security/PersetujuanMatrix/detail";
import PengaturanHakAkses from "./containers/BN/Pengaturan/PengaturanHakAkses";
import RoleUser from "./containers/BN/Pengaturan/PeranPengguna";

import PersetujuanHakAkses from "./containers/BN/Security/privilegeApproval";
import PersetujuanHakAksesDetail from "./containers/BN/Security/privilegeApproval/detail";

import Notifikasi from "./containers/BN/Notifikasi";
import AuditTrailAdminBank from "./containers/BN/AuditTrail/AdminBank";
import AuditTrailCustomer from "./containers/BN/AuditTrail/Customer";
import ServiceLimitTransaction from "./containers/BN/Transaction-Maintanance/ServiceLimitTransaction";
import DetailApprovalWorkflowPromoCategory from "./containers/BN/ApprovalWorkflow/Detail/ManagementPromo/PromoCategory";
import VirtualAccount from "./containers/BN/VirtualAccount";
import HardToken from "containers/BN/HardToken";
import HardTokenBind from "containers/BN/HardTokenBind";

import WorkshopPage from "containers/BN/Workshop";
import WorkshopCustomPage from "containers/BN/WorkshopCustomPage";
import CustomPage from "containers/BN/CustomPage";

// New UTM
import UserManager from "containers/BN/UserManagement/UserManager";
import AddNewUser from "containers/BN/UserManagement/UserManager/AddUser";
import TransactionHistory from "containers/BN/TransactionManagement/TransactionHistory";
import TransactionDetail from "containers/BN/TransactionManagement/TransactionHistory/TransactionDetail";
import TransactionStepDetail from "containers/BN/TransactionManagement/TransactionHistory/TransactionStepDetail";
import FlowDetail from "containers/BN/TransactionManagement/TransactionHistory/FlowDetail";
import RoleManager from "containers/BN/UserManagement/RoleManager";
// End New UTM

import { ReactKeycloakProvider } from "@react-keycloak/web";
import keycloak from "utils/helpers/keycloak";

const useStyles = makeStyles(() => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 645,
    minHeight: 720,
    backgroundColor: "#fff",
    border: "1px solid #BCC8E7",
    padding: 30,
    alignItems: "center",
    borderRadius: 20,
    position: "relative",
    textAlign: "center",
  },
  content: {
    position: "absolute",
    left: 30,
    right: 30,
    top: "50%",
    transform: "translateY(-50%)",
    paddingBottom: 30,
  },
  title: {
    fontFamily: "FuturaHvBT",
    fontSize: 30,
    fontWeight: 900,
    paddingTop: 30,
  },
}));

export const RootContext = createContext();

// Channel ID
// const channelID = process.env.REACT_APP_CHANNEL_ID;

const AppIndex = () => {
  const classes = useStyles();
  const location = useLocation().pathname;
  const history = useHistory();
  const dispatch = useDispatch();
  const [loc, setLoc] = useState("/portal/");
  const [userLogin, setUserLogin] = useState({
    role: "admin",
    username: "christian",
  });
  const {
    isPopUpOffline,
    isPopUpBE,
    isPopUpExpired,
    errorAlreadyUsed,
    gobackAlreadyUsed,
    error,
  } = useSelector(
    ({ errorGeneral }) => ({
      isPopUpOffline: errorGeneral.isPopUpOffline,
      isPopUpBE: errorGeneral.isPopUpBE,
      isPopUpExpired: errorGeneral.isPopUpExpired,
      error: errorGeneral.error,
      errorAlreadyUsed: errorGeneral.errorAlreadyUsed,
      gobackAlreadyUsed: errorGeneral.gobackAlreadyUsed,
    }),
    shallowEqual
  );

  // Keycloak
  // const [keycloak, initialized] = useKeycloak();

  // loading validate
  const { isLoadingValidate } = useSelector((e) => e.validateTaskPortal);
  const { isLoadingGlobal } = useSelector((e) => e.pageContainer);
  const lsData = getLocalStorage("portalUserLogin");

  useEffect(() => {
    if (
      location === "/portal/" ||
      location === "/portal/login" ||
      location === "/portal/login/"
    ) {
      setLoc(location);
    }
  }, [location]);

  useEffect(() => {
    const pageAccessedByReload =
      (window.performance.navigation &&
        window.performance.navigation.type === 1) ||
      window.performance
        .getEntriesByType("navigation")
        .map((nav) => nav.type)
        .includes("reload");

    if (pageAccessedByReload) {
      dispatch(setIsReloaded(true));
    } else {
      dispatch(setIsReloaded(false));
    }
  }, []);

  useEffect(() => {
    if (sessionStorage) {
      setUserLogin(JSON.parse(sessionStorage.getItem("user")));
    }
  }, []);

  useEffect(() => {
    if (lsData) {
      const stringRandom = makeId(12);
      const tabValue = getSessionStorage("tabportal");
      const tabValueLocal = getLocalStorage("tabportal");
      if (tabValueLocal && tabValue !== tabValueLocal) {
        dispatch(handleLogoutDoubleTab());
      } else {
        setSessionStorage("tabportal", stringRandom);
        setLocalStorage("tabportal", stringRandom);
      }
    }
  }, [dispatch, lsData]);

  const handleCloseOffline = () => {
    dispatch(setPopUpOffline(false));
  };

  const handleCloseErrorBE = () => {
    dispatch(setPopUpErrorBE(false));
  };

  const handleCloseExpired = () => {
    dispatch(setPopUpExpired(false));
    window.location.href = `${process.env.PUBLIC_URL}/`;
  };

  // BN ROUTER
  const switchBN = (
    <Switch>
      {/* Sprint 1 */}
      <Route exact path={"/"} component={LoginScreen} />
      <Route exact path="/portal/shared-components" component={DemoSC} />
      <Route exact path="/demosc" component={DemoSC} />
      <Route
        exact
        path={pathnameCONFIG.SELECT_CATEGORY}
        component={LoginKategori}
      />
      <ProtectedRoute>
        <ContainerBN>
          <RoutePortal
            exact
            path={pathnameCONFIG.DASHBOARD}
            component={Ringkasan}
            userRole={["ADMIN_MAKER", "ADMIN_APPROVER", "ADMIN_MAKER_APPROVER"]}
            menuId={69}
          />
          {/* Route New UTM */}
          <RoutePortal
            exact
            path={pathnameCONFIG.USER_MANAGEMENT.USER_MANAGER}
            component={UserManager}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.USER_MANAGEMENT.ADD_USER_MANAGER}
            component={AddNewUser}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_HISTORY}
            component={TransactionHistory}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_DETAIL}
            component={TransactionDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_STEP_DETAIL}
            component={TransactionStepDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MANAGEMENT.TRANSACTION_FLOW_DETAIL}
            component={FlowDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.USER_MANAGEMENT.ROLE_MANAGER}
            component={RoleManager}
          />
          {/* End Route New UTM */}
          <RoutePortal
            exact
            path={pathnameCONFIG.NOTIF}
            component={NotifInformation}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.SETTING.GENERAL_PARAMETER}
            userRole={["ADMIN_MAKER", "ADMIN_MAKER_APPROVER"]}
            component={BisnisPengaturanGeneralParameter}
            menuId={71}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.SETTING.USER_SETTING}
            component={BisnisPengaturanUserSetting}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.SETTING.ROLE_SETTING}
            component={PengaturanPeran}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.SETTING.BRANCH_SETTING}
            component={BisnisPengaturanBillerKategori}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.USER_APPROVAL}
            component={SecurityApprovalPengguna}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.MATRIX}
            component={PersetujuanMatrix}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.MATRIX_DETAIL}
            component={PersetujuanMatrixDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.CONFIG_APPROVAL}
            component={SecurityApprovalConfig}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.DETAIL_APPROVAL_CONFIG}
            component={SecurityDetailApproval}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.DETAIL_APPROVAL_USER}
            component={DetailApproval}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.HAK_AKSES}
            component={PersetujuanHakAkses}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL.HAK_AKSES_DETAIL}
            component={PersetujuanHakAksesDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.REGISTRATION}
            component={PendingTaskRegistration}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.REGISTRATION_DETAIL}
            component={PendingTaskRegistrationDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION}
            component={PendingTaskChangeAuthorization}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.CHANGE_AUTHORIZATION_DETAIL}
            component={PendingTaskChangeAuthorizationDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.SOFT_TOKEN_REGISTRATION}
            component={PendingTaskRegistrationSoftToken}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.SETTING.PERAN_PENGGUNA}
            component={RoleUser}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.PERSETUJUAN_MATRIX.NASABAH.LIST}
            component={PersetujuanMatrixNasabah}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PERSETUJUAN_MATRIX.ADMIN_BANK.INSERT}
            component={EditAdminBank}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PERSETUJUAN_MATRIX.NASABAH.INSERT}
            component={EditNasabah}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PERSETUJUAN_MATRIX.ADMIN_BANK.LIST}
            component={PersetujuanMatrixAdminBank}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.SETTING.PENGATURAN_HAK_AKSES}
            component={PengaturanHakAkses}
          />
          {/* Sprint 2 */}
          <RoutePortal
            exact
            path={pathnameCONFIG.AUDIT_TRAIL.ADMIN_BANK}
            component={AuditTrailAdminBank}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.AUDIT_TRAIL.CUSTOMER}
            component={AuditTrailCustomer}
          />
          {/* // financial */}

          <RoutePortal
            exact
            path={
              pathnameCONFIG.TRANSACTION_MAINTENANCE.GLOBAL_PRODUCT_REKENING
            }
            component={ProductRekening}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MAINTENANCE.TYPE_SEGEMENT}
            component={TypeSegment}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.GLOBAL_MAINTENANCE.GLOBAL_LIMIT_TRANSACTION}
            component={GlobalLimitTrx}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GLOBAL_MAINTENANCE.SERVICE_LIMIT_TRANSACTION}
            component={ServiceLimitTransaction}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT}
            component={CompanyLimit}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_LIMIT_DETAIL}
            component={CompanyLimitDetail}
          />
          {/* Tunggu Fixing Global Limit */}
          <RoutePortal
            exact
            path={
              pathnameCONFIG.GLOBAL_MAINTENANCE.GLOBAL_LIMIT_TRANSACTION_DETAIL
            }
            component={GLobalLimitTrxDetails}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE}
            component={CompanyCharge}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSACTION_MAINTENANCE.COMPANY_CHARGE_DETAIL}
            component={CompanyChargeDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GLOBAL_MAINTENANCE.MENU_GLOBAL}
            component={MenuGlobal}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GLOBAL_MAINTENANCE.MENU_GLOBAL_EDIT}
            component={MenuGlobalEdit}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.HOST_ERROR_MAPPING}
            component={MaintananceUmum}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK_DETAIL}
            component={DomesticBankDetails}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.DOMESTIC_BANK}
            component={DomesticBank}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.INTERNATIONAL_BANK}
            component={InternationalBank}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.INTERNATIONAL_BANK_EDIT}
            component={InternationalBankUbah}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.INTENRATIONAL_BANK_ADD}
            component={InternationalBankTambah}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.HANDLING_OFFICER}
            component={HandlingOfficer}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.INSTITUSI_VA}
            component={InstitusiVa}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.INSTITUSI_VA_DETAIL}
            component={InstitusiVaDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.ORGANIZATION_UNIT}
            component={OrganizationUnit}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.ORGANIZATION_UNIT_ADD}
            component={AddNewOrganizationUnit}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL_WORKFLOW.BASE_URL}
            component={ApprovalWorkflow}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.GLOBAL_MAINTENANCE_DETAIL
                .MENU_GLOBAL
            }
            component={DetailApprovalWorkflowMenuGlobal}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.GLOBAL_MAINTENANCE_DETAIL
                .PRODUK_REKENING
            }
            component={DetailApprovalWorkProdukRekening}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.GLOBAL_MAINTENANCE_DETAIL
                .LIMIT_TRANSAKSI
            }
            component={DetailApprovalWorkLimtTransaksi}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.GLOBAL_MAINTENANCE_DETAIL.SERVICE
            }
            component={DetailApprovalWorkService}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.MAINTENANCE_UMUM_DETAIL
                .HOST_ERROR_MAPPING
            }
            component={DetailApprovalWorkflowhostErrormapping}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.MAINTENANCE_UMUM_DETAIL
                .DOMESTIC_BANK
            }
            component={DetailApprovalWorkflowDomesticBank}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.MAINTENANCE_UMUM_DETAIL
                .INTERNATIONAL_BANK
            }
            component={DetailApprovalWorkflowInternationalBank}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.MAINTENANCE_UMUM_DETAIL
                .HANDLING_OFFICER
            }
            component={DetailApprovalWorkflowHandlingOfficer}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .CHARGE_LIST
            }
            component={DetailApprovalWorkflowChargeList}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .CHARGE_PACKAGE
            }
            component={DetailApprovalWorkflowChargePackage}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .DAFTAR_SEGMENTASI
            }
            component={DetailApprovalWorkflowDaftarSegmentasi}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .JENIS_REKENING_PRPODUK
            }
            component={DetailApprovalWorkflowJenisRekeningProduk}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .LIMIT_PACKAGE
            }
            component={DetailApprovalWorkflowLimitPackage}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .MENU_PACKAGE
            }
            component={DetailApprovalWorkflowMenuPackage}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .SEGMEN_CABANG
            }
            component={DetailApprovalWorkflowSegmenCabang}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .SERVICE_CHARGE_LIST
            }
            component={DetailApprovalWorkflowServiceChargeList}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .TIERING_SLAB
            }
            component={DetailApprovalWorkflowTieringSlab}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .MENU_KHUSUS
            }
            component={DetailApprovalWorkflowMenuKhusus}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.SEGMENTASI_NASABAH_DETAIL
                .TYPE_PARAMETER
            }
            component={DetailApprovalWorkflowTypeParameter}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.COMPANY_LIMIT_CHARGE_DETAIL
                .COMPANY_LIMIT
            }
            component={DetailApprovalWorkflowCompanyLimit}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.COMPANY_LIMIT_CHARGE_DETAIL
                .COMPANY_CHARGE
            }
            component={DetailApprovalWorkflowCompanyCharge}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.MANAGEMENT_PERUHSAAN_DETAIL
                .PENGATURAN_TEMPLATE
            }
            component={DetailApprovalWorkflowCompanyCharge}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.UTILITY_DETAIL
                .CONTACT_SUPPORT_RESPONSE
            }
            component={DetailContactSupportResponse}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.UTILITY_DETAIL.EVENT_CALENDAR
            }
            component={DetailEventCalendar}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL_WORKFLOW.UTILITY_DETAIL.USER_SETTINGS}
            component={DetailUserSetting}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.UTILITY_DETAIL.CONTENT_MANAGEMENT
            }
            component={DetailContentManagement}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.UTILITY_DETAIL
                .PRODUCT_INFORMATION
            }
            component={DetailProductInformation}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL_WORKFLOW.BLAST_NOTIFICATION.PROMO}
            component={DetailBlastNotification}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL_WORKFLOW.FAQ.DETAIL}
            component={DetailFaq}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.ACTIVATION_VA}
            component={PendingTaskActivationVa}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.ACTIVATION_VA_DETAIL}
            component={PendingTaskDetailVa}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION}
            component={PendingTaskAutoCollection}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.PENDING_TASK.AUTO_COLLECTION_DETAIL}
            component={DetailPendingTaskAutoCollection}
          />
          {/* Sprint 3 */}
          {/* Example  Biller Manager Sprint3 */}
          {/* <RoutePortal
          exact
          path="/portal/bisnis-pengaturan/biller-manager/edit"
          component={BisnisPengaturanBillerManagerEdit}
          // contoh ubah user
        /> */}
          {/* <RoutePortal
            exact
            path={pathnameCONFIG.TRANSAKSI.STATUS_TRANSAKSI}
            component={StatusTransaksi}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSAKSI.RESI_TRANSAKSI}
            component={ResiTransaksi}
          /> */}
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL_WORKFLOW.PAYMENT_SETUP.BILLER_MANAGER}
            component={DetailApprovalWorkflowBillerManager}
            userRole={["ADMIN_APPROVER", "ADMIN_MAKER_APPROVER"]}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.PAYMENT_SETUP.BILLER_KATEGORI
            }
            component={DetailApprovalWorkflowBillersCategory}
            userRole={["ADMIN_APPROVER", "ADMIN_MAKER_APPROVER"]}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.TRANSAKSI.STATUS_NON_TRANSAKSI}
            component={StatusNonTransaksi}
          />

          {/* Example List Merchant Sprint 3 */}
          <RoutePortal
            exact
            path={pathnameCONFIG.MERCHANT.LIST_MERCHANT}
            component={Merchant}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.MERCHANT.ADD_MERCHANT}
            component={AddMerchant}
          />

          {/* <RoutePortal
            exact
            path={pathnameCONFIG.PREFERENSI.LIST_PREFERENSI}
            component={ListPreferensi}
          /> */}
          {/* Example Blast Promo Sprint 3 */}
          {/* <RoutePortal
          exact
          path="/portal/bisnis-pengaturan/log-notifikasi"
          component={BisnisPengaturanNotifikasi}
        /> */}
          <RoutePortal
            exact
            path={pathnameCONFIG.ONLINE_SUBMISSION_REPORT.VA}
            component={OnlineSubmissionReport}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.ONLINE_SUBMISSION_REPORT.VA_DETAIL}
            component={OnlineSubmissionReportDetail}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.ONLINE_SUBMISSION_REPORT.VA_ACTIVATION}
            component={VaActivation}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION}
            component={AutoCollectionReport}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.ONLINE_SUBMISSION_REPORT.AUTO_COLLECTION_DETAIL
            }
            component={AutoCollectionDetail}
          />
          <RoutePortal
            path={pathnameCONFIG.CONSOLIDATION_REPORT.LIST}
            component={SegmentationReport}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.CONSOLIDATION_REPORT.DETAIL}
            component={SegmentasiReportDetail}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.CONSOLIDATION_REPORT.COMPANY_LOGIN_DETAIL}
            component={CompanyLoginDetail}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.INQUIRY.PERIODICAL_CHARGE_LIST}
            component={PeriodicalChargeList}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.INQUIRY.TRANSACTION_STATUS}
            component={TransactionStatus}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.INQUIRY.NON_TRANSACTION_STATUS}
            component={StatusNonTransaksi}
            exact
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.UTILITAS.CALENDAR_EVENT}
            component={CalendarEvent}
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.MANAGEMENT_USER_DETAIL}
            component={UtilitasManagementUserDetail}
            exact
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.CUT_OFF_TIME_SETTINGS.BASE_URL}
            component={CutOffTime}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.CUT_OFF_TIME_SETTINGS.FORM}
            component={CutOffTimeForm}
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.MANAGEMENT_USER}
            component={UtilitasManagemenrUser}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.ADD_MANAGEMENT_USER}
            component={UtilitasManagemenrUserAddNew}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.PRODUCT_INFORMATION}
            component={UtilitasProductInformation}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.CONTENT_ACCESS}
            component={ContentAccess}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.CONTENT_ACCESS_DETAIL}
            component={ContentAccessDetail}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.CONTENT_ACCESS_EDIT}
            component={ContentAccessEdit}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.RESPONSE_SETTING}
            component={ResponseSetting}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.RESPONSE_SETTING_ADD}
            component={TambahResponseSetting}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER}
            component={BillerManager}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER_ADD}
            component={BillerManagerForm}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.PAYMENT_SETUP.BILLER_MANAGER_EDIT}
            component={BillerManagerForm}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI}
            component={BillerKategori}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.PAYMENT_SETUP.BILLER_KATEGORI_EDIT}
            component={BillerKategoriEdit}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT}
            component={ContentManagement}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.UTILITAS.CONTENT_MANAGEMENT_EDIT}
            component={ContentManagementEdit}
            exact
          />
          <RoutePortal
            path={pathnameCONFIG.NOTIFICATION}
            component={Notifikasi}
            exact
          />
          {/* Sprint 4 */}
          <RoutePortal
            exact
            path={pathnameCONFIG.GENERAL_MAINTENANCE.EXCHANGE_RATE}
            component={ExchangeRate}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.TRANSACTION_MAINTENANCE.TIME_DEPOSIT_SPECIAL_RATE
            }
            component={TimeDepositeSpecialRate}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.TRANSACTION_MAINTENANCE
                .TIME_DEPOSIT_SPECIAL_RATE_TAMBAH
            }
            component={TambahTimeDepositeSpecialRate}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.MANAGEMENT_PROMO.LIST_PROMO}
            component={ListPromo}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.APPROVAL_WORKFLOW.MANAGEMENT_PROMO.PROMO}
            component={ListPromoApprovalWf}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.MANAGEMENT_PROMO.PROMO_CATEGORY}
            component={PromoCategory}
          />
          <RoutePortal
            exact
            path={
              pathnameCONFIG.APPROVAL_WORKFLOW.MANAGEMENT_PROMO.PROMO_CATEGORY
            }
            component={DetailApprovalWorkflowPromoCategory}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.VIRTUAL_ACCOUNT}
            component={VirtualAccount}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.HARD_TOKEN}
            component={HardToken}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.HARD_TOKEN + "/bind"}
            component={HardTokenBind}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.WORKSHOP}
            component={WorkshopPage}
          />
          <RoutePortal
            exact
            path={pathnameCONFIG.WORKSHOP_CUSTOM}
            component={WorkshopCustomPage}
          />

          <RoutePortal
            exact
            path={pathnameCONFIG.CUSTOM_PAGE}
            component={CustomPage}
          />

          {/* <RoutePortal
            exact
            path={pathnameCONFIG.MANAGEMENT_PROMO}
            component={TambahPromo}
          /> */}
          {/* Sprint 5 */}
        </ContainerBN>
      </ProtectedRoute>
    </Switch>
  );

  return (
    <ThemeProvider theme={theme}>
      <RootContext.Provider
        value={{
          userLogin,
        }}
      >
        {switchBN}
      </RootContext.Provider>

      <Dialog open={isLoadingValidate}>
        <DialogContent style={{ padding: "20px" }}>
          <Box sx={{ display: "flex" }}>
            <CircularProgress />
          </Box>
        </DialogContent>
      </Dialog>

      <Dialog open={isLoadingGlobal}>
        <DialogContent style={{ padding: "20px" }}>
          <Box sx={{ display: "flex" }}>
            <CircularProgress />
          </Box>
        </DialogContent>
      </Dialog>

      <Toast
        open={error?.isError}
        message={error?.message ?? null}
        handleClose={() => dispatch(handleClearError())}
      />

      <SystemMaintenance
        isOpen={isPopUpOffline}
        handleClose={handleCloseOffline}
        message=""
        title=""
        submessage={
          <div style={{ width: 376 }}>
            The system is offline. Please try again in a few moments.
          </div>
        }
        img={offline}
        height="320px"
        closeModal={false}
      />

      {/* <SuccessConfirmation
        isOpen={1}
        handleClose={handleCloseOffline}
        message="IP Checking Active"
        title=""
        submessage="You cannot login. Please use the registered IP"
        img={IpLock}
        height="390px"
        closeModal={false}
      /> */}

      <SystemMaintenance
        isOpen={isPopUpBE}
        handleClose={handleCloseErrorBE}
        submessage={
          <div style={{ width: 376 }}>
            Sorry, the system is currently under maintenance. Please try again
            in a moment
          </div>
        }
        message="Maintenance Activity"
        title=""
        closeModal={false}
        img={PopUpErrorBE}
      />

      <SuccessConfirmation
        height="500px"
        title="Session End"
        message="Please login again to start the session"
        submessage=""
        isOpen={isPopUpExpired}
        handleClose={handleCloseExpired}
        img={DeviceLogin}
      />

      <GeneralErrorPopups />
    </ThemeProvider>
  );
};

export default AppIndex;
