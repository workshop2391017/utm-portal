/* eslint-disable import/prefer-default-export */
const pathnameCONFIG = {
  ROOT_URL: "/portal/",
  LOGIN: "/portal/login",
  SELECT_CATEGORY: "/portal/kategori",
  DASHBOARD: "/portal/dashboard",
  USER_MANAGEMENT: {
    USER_MANAGER: "/portal/user-manager",
    ADD_USER_MANAGER: "/portal/user-manager/add",
    ROLE_MANAGER: "/portal/role-manager",
  },
  TRANSACTION_MANAGEMENT: {
    TRANSACTION_HISTORY: "/portal/transaction-management/transaction-history",
    TRANSACTION_DETAIL: "/portal/transaction-management/transaction-detail",
    TRANSACTION_STEP_DETAIL:
      "/portal/transaction-management/transaction-step-detail",
    TRANSACTION_FLOW_DETAIL:
      "/portal/transaction-management/transaction-flow-detail",
  },
  SETTING: {
    BASE_URL: "/portal/setting",
    GENERAL_PARAMETER: "/portal/setting/general-parameter",
    USER_SETTING: "/portal/setting/user-setting",
    ROLE_SETTING: "/portal/setting/role-setting",
    ROLE_SETTING_DETAIL: "/portal/setting/role-setting/detail",
    BRANCH_SETTING: "/portal/setting/branch-setting",
    PENGATURAN_HAK_AKSES: "/portal/setting/pengaturan-hak-akses",
    PERAN_PENGGUNA: "/portal/peran-pengguna",
    DETAIL_PERAN_PENGGUNA: "/portal/peran-pengguna/detail",
  },
  APPROVAL: {
    BASE_URL: "/portal/persetujuan",
    USER_APPROVAL: "/portal/persetujuan/user-approval",
    DETAIL_APPROVAL_USER: "/portal/persetujuan/user-approval/detail",
    CONFIG_APPROVAL: "/portal/config-persetujuan",
    DETAIL_APPROVAL_CONFIG: "/portal/config-persetujuan/detail",
    MATRIX: "/portal/persetujuan/matrix",
    MATRIX_DETAIL: "/portal/persetujuan/matrix/detail",
    HAK_AKSES: "/portal/persetujuan/hak-akses",
    HAK_AKSES_DETAIL: "/portal/persetujuan/hak-akses/detail",
  },
  MANAGEMENT_COMPANY: {
    BASE_URL: "/portal/management-company",
    REGISTRATION: "/portal/management-company/registration",
    REGISTRATION_NEW: "/portal/management-company/registration/new",
    REGISTRATION_DETAIL: "/portal/management-company/registration/detail",
    WORKFLOW_SETTING: "/portal/management-company/pangaturan-alur-kerja",
    TEMPLATE_SETTING: "/portal/management-company/pangaturan-template",
    CHANGE_AUTHORIZATION: "/portal/management-company/change-authorization",
    CHANGE_AUTHORIZATION_CHANGE:
      "/portal/management-company/change-authorization/change",
    DETAIL_CHANGE_AUTHORIZATION:
      "/portal/management-company/change-authorization/detail",
    EDIT_CHANGE_AUTHORIZATION_DETAIL:
      "/portal/management-company/change-authorization/edit-detail",
    EDIT_CHANGE_AUTHORIZATION:
      "/portal/management-company/change-authorization/edit",
    COMPANY: "/portal/management-company/company",

    DETAIL_COMPANY: "/portal/management-company/company/detail/",
    DETAIL_ADD_GROUP: "/portal/detailtambahkelompok",
    USER_SETTING_SINGLE: "/portal/management-company/user-setting-single",
    USER_SETTING: "/portal/management-company/user-setting",
    USER_SETTING_ADD: "/portal/management-company/user-setting/add",
    USER_SETTING_EDIT: "/portal/management-company/user-setting/edit",
    USER_SETTING_EDIT_ADMIN:
      "/portal/management-company/user-setting/edit-admin",
    USER_SETTING_DETAIL: "/portal/management-company/user-setting/detail",
    USER_SETTING_DETAIL_ADMIN:
      "/portal/management-company/user-setting/detail-admin",
    PENGATURAN_KELOMPOK: "/portal/management-company/pengaturan-kelompok",
    PENGELOMPOKAN_REKENING_ADD:
      "/portal/management-company/pengelompkan_rekening/add",
    PENGELOMPOKAN_REKENING_EDIT:
      "/portal/management-company/pengelompkan_rekening/edit",

    PENGATURAN_KELOMPOK_UBAH:
      "/portal/management-company/pengaturan-kelompok/ubah",
    PENGATURAN_LEVEL: {
      LIST: "/portal/management-company/level-setting/list",
    },

    PENGATURAN_MATRIX_NON_FINANCIAL: {
      LIST: "/portal/management-company/pengaturan-matrix/non-finansial/list",
      ADD: "/portal/management-company/pengaturan-matrix/non-finansial/add",
    },
    PENGATURAN_MATRIX_FINANCIAL: {
      LIST: "/portal/management-company/pengaturan-matrix/finansial/list",
      ADD: "/portal/management-company/pengaturan-matrix/finansial/add",
    },
  },
  SEGMENTATION_USER: {
    BASE_URL: "/portal/segmentasi-nasabah",
    LIMIT_MENU_PACKAGE: "/portal/segmentasi-nasabah/limit-menu",
    CHANGE_PACKAGE: "/portal/segmentasi-nasabah/change",

    SERVICE_PACKAGE: "/portal/segmentasi-nasabah/service",
    REPORT: "",
    REPORT_DETAIL: "",
    CREATE_NEW: "/portal/segmentasi-nasabah/create",
    EDIT: "/portal/segmentasi-nasabah/edit",
    MENU_KHUSUS: "/portal/segmentasi-package/menu-khusus",
    MENU_KHUSUS_DETAILS: "/portal/segmentasi-package/menu-khusus/details",
    MENU_KHUSUS_EDIT: "/portal/segmentasi-package/menu-khusus/edit",
    MENU_KHUSUS_ADD: "/portal/segmentasi-package/menu-khusus/add",
  },
  CONSOLIDATION_REPORT: {
    LIST: "/portal/segmentation/report",
    DETAIL: "/portal/segmentation/report/detail",
    COMPANY_LOGIN_DETAIL: "/portal/segmentation/report/detail/company-login",
  },
  INQUIRY: {
    PERIODICAL_CHARGE_LIST: "/portal/inquiry/periodical-charge-list",
    TRANSACTION_STATUS: "/portal/inquiry/transaction-status",
    NON_TRANSACTION_STATUS: "/portal/inquiry/non-transaction-status",
  },
  SEGMENTATION_PACKAGE: {
    CHARGE_LIST: "/portal/segmentasi-package/charge-list",
    CHARGE_PACKAGE: "/portal/segmentasi-package/charge-package",
    CHARGE_PACKAGE_DETAIL: "/portal/segmentasi-package/charge-package/detail",
    CHARGE_PACKAGE_ADD_NEW: "/portal/segmentasi-package/charge-package/new",
    JENIS_REKENING: "/portal/segmentasi-package/jenis-rekening-produk",
    JENIS_REKENING_DETAIL:
      "/portal/segmentasi-package/jenis-rekening-produk/detail",
    JENIS_REKENING_ADD_NEW:
      "/portal/segmentasi-package/jenis-rekening-produk/add-new",
    LIMIT_PACKAGE: "/portal/segmentasi-package/limit-package",
    LIMIT_PACKAGE_DETAIL: "/portal/segmentasi-package/limit-package/detail",
    LIMIT_PACKAGE_ADD_NEW: "/portal/segmentasi-package/limit-package/add-new",
    MENU_PACKAGE: "/portal/segmentasi-package/menu-package",
    MENU_PACKAGE_DETAIL: "/portal/segmentasi-package/menu-package/detail",
    MENU_PACKAGE_ADD_NEW: "/portal/segmentasi-package/menu-package/add-new",
    SEGMEN_CABANG: "/portal/segmentasi-package/segmen-cabang",
    SEGMEN_CABANG_DETAILS: "/portal/segmentasi-package/segmen-cabang/details",
    SEGMEN_CABANG_EDIT: "/portal/segmentasi-package/segmen-cabang/edit",
    SEGMEN_CABANG_ADD: "/portal/segmentasi-package/segmen-cabang/add",
    SERVICE_CHARGE_PACKAGE: "/portal/segmentasi-package/service-charge-list",
    SERVICE_CHARGE_PACKAGE_ADD_EDIT:
      "/portal/segmentasi-package/service-charge-list/add-edit",
    TEIRING_SLAB: "/portal/segmentasi-package/teiring-slab",
    TEIRING_SLAB_DETAIL: "/portal/segmentasi-package/teiring-slab/detail",
    TEIRING_SLAB_EDIT: "/portal/segmentasi-package/teiring-slab/edit",
    TEIRING_SLAB_ADD: "/portal/segmentasi-package/teiring-slab/add",
    PARAMETER_SETTING: "/portal/segmentasi-package/parameter-setting",
    PARAMETER_SETTING_INSERT:
      "/portal/segmentasi-package/parameter-setting/insert",
    MENU_KHUSUS: "/portal/segmentasi-package/menu-khusus",
  },
  GENERAL_MAINTENANCE: {
    BASE_URL: "/portal/general-maintenance",
    HOST_ERROR_MAPPING: "/portal/general-maintenance/host-error-mapping",
    DOMESTIC_BANK: "/portal/general-maintenance/domestic-bank",
    DOMESTIC_BANK_DETAIL: "/portal/maintenance-umum/domestic-bank-details",
    DOMESTIC_BANK_EDIT: "/portal/maintenance-umum/domestic-bank-details-edit",
    EXCHANGE_RATE: "/portal/general-maintenance/exchange-rate",
    HANDLING_OFFICER: "/portal/general-maintenance/handling-officer",
    INSTITUSI_VA: "/portal/general-maintenance/institusi-va",
    INSTITUSI_VA_DETAIL: "/portal/general-maintenance/institusi-va/detail",
    ORGANIZATION_UNIT: "/portal/general-maintenance/organization-unit",
    ORGANIZATION_UNIT_ADD: "/portal/maintenance-umum/organization-unit/add",
    INTERNATIONAL_BANK: "/portal/general-maintenance/international-bank",
    INTENRATIONAL_BANK_ADD: "/portal/maintenance-umum/international-bank/add",
    INTERNATIONAL_BANK_EDIT: "/portal/maintenance-umum/international-bank/edit",
  },
  TRANSACTION_MAINTENANCE: {
    BASE_URL: "/portal/transaction-maintenance",

    GLOBAL_PRODUCT_REKENING: "/portal/transaction-maintenance/product-rekening",
    TYPE_SEGEMENT: "/portal/transaction-maintenance/type-segement",

    GLOBAL_FOREX_USAGE: "/portal/transaction-maintenance/global-forex-usage",
    COMPANY_LIMIT: "/portal/transaction-maintenance/company-limit",
    COMPANY_LIMIT_DETAIL:
      "/portal/transaction-maintenance/company-limit/details",
    FOREX_SPECIAL_RATE: "/portal/transaction-maintenance/forex-special-rate",
    COMPANY_CHARGE: "/portal/transaction-maintenance/company-charge",
    COMPANY_CHARGE_TAMBAH:
      "/portal/transaction-maintenance/company-charge-tambah",
    // COMPANY_CHARGE_LANDING: "/portal/transaction-maintenance/company-charge",
    COMPANY_CHARGE_EDIT: "/portal/transaction-maintenance/company-charge-edit",
    COMPANY_CHARGE_DETAIL:
      "/portal/transaction-maintenance/company-charge-detail",
    TIME_DEPOSIT_SPECIAL_RATE:
      "/portal/transaction-maintenance/time-desposite-special-rate",
    TIME_DEPOSIT_SPECIAL_RATE_TAMBAH:
      "/portal/transaction-maintenance/time-desposite-special-rate-tambah",
  },

  GLOBAL_MAINTENANCE: {
    MENU_GLOBAL: "/portal/transaction-maintenance/menu-global",
    MENU_GLOBAL_EDIT: "/portal/transaction-maintenance/menu-global-edit",
    SERVICE_LIMIT_TRANSACTION:
      "/portal/transaction-maintenance/service-limit-transaction",
    GLOBAL_LIMIT_TRANSACTION:
      "/portal/transaction-maintenance/global-limit-transaction",
    GLOBAL_LIMIT_TRANSACTION_DETAIL:
      "/portal/transaction-maintenance/statistik-transaksi-details",
  },
  CUT_OFF_TIME_SETTINGS: {
    BASE_URL: "/portal/cut-off-time",
    FORM: "/portal/cut-off-time/form",
  },
  PENDING_TASK: {
    BASE_URL: "/portal/pending-task/",
    REGISTRATION: "/portal/pending-task/registration",
    // REGISTRATION_DETAIL: "/portal/pending-task/registration/detail",
    CHANGE_AUTHORIZATION: "/portal/pending-task/change-authorization",
    CHANGE_AUTHORIZATION_DETAIL:
      "/portal/pending-task/change-authorization/detail",
    ACTIVATION_VA: "/portal/pending-task/activation-va",
    ACTIVATION_VA_DETAIL: "/portal/pending-task/activation-va/detail",
    SOFT_TOKEN_REGISTRATION: "/portal/pending-task/soft-token-registration",
    REGISTRATION_DETAIL: "/portal/pending-task/registration/detail",
    AUTO_COLLECTION: "/portal/pending-task/auto-collection",
    AUTO_COLLECTION_DETAIL: "/portal/pending-task/auto-collection/detail",
  },
  PAYMENT_SETUP: {
    TIME_DEPOSIT: "/portal/payment-setup/time-deposit",
    BILLER_MANAGER: "/portal/payment-setup/biller-manager",
    BILLER_MANAGER_ADD: "/portal/payment-setup/biller-manager/add",
    BILLER_MANAGER_EDIT: "/portal/payment-setup/biller-manager/edit",
    BILLER_KATEGORI: "/portal/payment-setup/biller-kategori",
    BILLER_KATEGORI_EDIT: "/portal/payment-setup/biller-kategori/edit",
  },
  ONLINE_SUBMISSION_REPORT: {
    VA: "/portal/online-submission-report/va",
    VA_DETAIL: "/portal/online-submission-report/va-detail",
    VA_ACTIVATION: "/portal/online-submission-report/va-activation",
    AUTO_COLLECTION: "/portal/online-submission-report/autocollection",
    AUTO_COLLECTION_DETAIL:
      "/portal/online-submission-report/autocollection/detail",
  },
  APPROVAL_WORKFLOW: {
    BASE_URL: "/portal/approval-workflow",
    UTILITY_DETAIL: {
      CONTACT_SUPPORT_RESPONSE:
        "/portal/approval-workflow/utility/contact-support-response",
      EVENT_CALENDAR: "/portal/approval-workflow/utility/event-calendar",
      CONTENT_MANAGEMENT:
        "/portal/approval-workflow/utility/content-management",
      PRODUCT_INFORMATION:
        "/portal/approval-workflow/utility/product-information",
      USER_SETTINGS: "/portal/approval-workflow/utility/user-setting",
    },
    GLOBAL_MAINTENANCE_DETAIL: {
      MENU_GLOBAL: "/portal/approval-workflow/global-maintenance/menu-global",
      PRODUK_REKENING:
        "/portal/approval-workflow/galobal-maintenance/produk-rekening",
      LIMIT_TRANSAKSI:
        "/portal/approval-workflow/galobal-maintenance/limit-transaksi",
      SERVICE: "/portal/approval-workflow/galobal-maintenance/service",
    },
    MAINTENANCE_UMUM_DETAIL: {
      DOMESTIC_BANK: "/portal/approval-workflow/maintenance-umum/domestic-bank",
      INTERNATIONAL_BANK:
        "/portal/approval-workflow/maintenance-umum/international-bank",
      HOST_ERROR_MAPPING:
        "/portal/approval-workflow/maintenance-umum/host-error-mapping",
      HANDLING_OFFICER:
        "/portal/approval-workflow/maintenance-umum/handling-officer",
    },

    SEGMENTASI_NASABAH_DETAIL: {
      CHARGE_LIST: "/portal/approval-workflow/segmentasi-nasabah/charge-list",
      CHARGE_PACKAGE:
        "/portal/approval-workflow/segmentasi-nasabah/charge-package",
      DAFTAR_SEGMENTASI:
        "/portal/approval-workflow/segmentasi-nasabah/daftar-segmentasi",
      JENIS_REKENING_PRPODUK:
        "/portal/approval-workflow/segmentasi-nasabah/jenis-rekening-produk",
      LIMIT_PACKAGE:
        "/portal/approval-workflow/segmentasi-nasabah/limit-package",
      MENU_PACKAGE: "/portal/approval-workflow/segmentasi-nasabah/menu-package",
      SEGMEN_CABANG:
        "/portal/approval-workflow/segmentasi-nasabah/segmen-cabang",
      SERVICE_CHARGE_LIST:
        "/portal/approval-workflow/segmentasi-nasabah/service-charge-list",
      TIERING_SLAB: "/portal/approval-workflow/segmentasi-nasabah/tiered-slab",
      MENU_KHUSUS: "/portal/approval-workflow/segmentasi-nasabah/menu-khusus",
      TYPE_PARAMETER:
        "/portal/approval-workflow/segmentasi-nasabah/type-parameter",
    },
    COMPANY_LIMIT_CHARGE_DETAIL: {
      COMPANY_LIMIT:
        "/portal/approval-workflow/company-limit-charge/company-limit",
      COMPANY_CHARGE:
        "/portal/approval-workflow/company-limit-charge/company-charge",
    },
    MANAGEMENT_PERUHSAAN_DETAIL: {
      INFORMASI_PERUHSAAN:
        "/portal/approval-workflow/management-perusahaan/informasi-perusahaan",
      PENGELOMPOKAN_REKENING:
        "/portal/approval-workflow/management-perusahaan/pengelompokan-rekening",
      KONFIGURASI_REKENING:
        "/portal/approval-workflow/management-perusahaan/konfigurasi-rekening",
      PENGATURAN_PENERIMA_TRANSFER:
        "/portal/approval-workflow/management-perusahaan/pengaturan-penerima-transfer",
      PENGATURAN_SEGMENTASI:
        "/portal/approval-workflow/management-perusahaan/pengaturan-segmentasi",
      PENGATURAN_ALUR_KERJA:
        "/portal/approval-workflow/management-perusahaan/pengaturan-alur-kerja",
      PENGATURAN_TEMPLATE:
        "/portal/approval-workflow/management-perusahaan/pengaturan-template",
      PENGATURAN_PENGGUNA:
        "/portal/approval-workflow/management-perusahaan/pengaturan-pengguna",
      PENGATURAN_KELOMPOK:
        "/portal/approval-workflow/management-perusahaan/pengaturan-kelompok",
      MATRIK_FINANSIAL:
        "/portal/approval-workflow/management-perusahaan/matrix-finansial",
      MATRIX_NONFINANSIAL:
        "/portal/approval-workflow/management-perusahaan/matrix-nonfinansial",
      PENGATURAN_LEVEL:
        "/portal/approval-workflow/management-perusahaan/pengaturan-level",
    },

    PAYMENT_SETUP: {
      BILLER_MANAGER: "/portal/approval-workflow/payment-setup/biller-manager",
      BILLER_KATEGORI:
        "/portal/approval-workflow/payment-setup/biller-kategori",
    },
    MANAGEMENT_PROMO: {
      PROMO: "/portal/approval-workflow/promo",
      PROMO_CATEGORY: "/portal/approval-workflow/promo-category",
    },
    BLAST_NOTIFICATION: {
      PROMO: "/portal/approval-workflow/blast-promo",
    },
    FAQ: {
      DETAIL: "/portal/approval-workflow/faq",
    },
  },
  UTILITAS: {
    MANAGEMENT_USER: "/portal/utilitas/management-user",
    ADD_MANAGEMENT_USER: "/portal/utilitas/management-user/add-user",
    MANAGEMENT_USER_DETAIL: "/portal/utilitas/management-user/block",
    CALENDAR_EVENT: "/portal/utilitas/calendar-event",
    CONTENT_ACCESS: "/portal/utilitas/content-access",
    CONTENT_ACCESS_DETAIL: "/portal/utilitas/content-access/detail",
    CONTENT_ACCESS_EDIT: "/portal/utilitas/content-access/edit",
    RESPONSE_SETTING: "/portal/utilitas/response-setting",
    RESPONSE_SETTING_ADD: "/portal/utilitas/response-setting/add",
    CONTENT_MANAGEMENT: "/portal/utilitas/content-management",
    CONTENT_MANAGEMENT_EDIT: "/portal/utilitas/content-management/edit",
    CONTENT_MANAGEMENT_DETAIL: "/portal/utilitas/content-management/detail",
    PRODUCT_INFORMATION: "/portal/utilitas/product-information",
    HELP_DESK: {
      INBOX: "/portal/utilitas/help-desk",
      DETAIL: "/portal/utilitas/help-desk/detail",
    },
  },

  // ?? gak nemu menunya
  // PROMO: {
  //   LIST_PROMO: "/portal/management-promo/list-promo",
  // },
  // JENIS_PROMO: {
  //   LIST_JENIS_PROMO: "/portal/list-jenis-promo/",
  //   ADD_SUB_CATEGORY: "/portal/list-jenis-promo/add",
  // },
  // PROMO_CATEGORY: "/portal/promo-category",

  MANAGEMENT_PROMO: {
    LIST_PROMO: "/portal/management-promo/list-promo",
    PROMO_CATEGORY: "/portal/management-promo/promo-category",
    LIST_JENIS_PROMO: "/portal/management-promo/list-jenis-promo",
    LIST_MERCHANT: "/portal/management-promo/list-merchant",
  },
  MERCHANT: {
    LIST_MERCHANT: "/portal/management-promo/merchant",
    ADD_MERCHANT: "/portal/management-promo/merchant/add",
  },
  TRANSAKSI: {
    STATUS_TRANSAKSI: "/portal/status-transaksi",
    STATUS_NON_TRANSAKSI: "/portal/status-non-transaksi/",
  },

  FAQ_DOWNLOAD: {
    CATEGORY: "/portal/faq-download/management",
    LIST: "/portal/faq-download/bank-admin",
    FAQ_ADD: "/portal/faq-download/add",
    FAQ_EDIT: "/portal/faq-download/edit",
  },
  KPRKPA: "/portal/kprkpa",
  PERSETUJUAN_MATRIX: {
    ADMIN_BANK: {
      LIST: "/portal/persetujuan-matrix/admin-bank/list",
      INSERT: "/portal/persetujuan-matrix/admin-bank/list/insert",
    },
    NASABAH: {
      LIST: "/portal/persetujuan-matrix/nasabah/list",
      INSERT: "/portal/persetujuan-matrix/nasabah/list/insert",
    },
  },

  PENGATURAN: {
    PERAN_PENGGUNA: "/portal/peran-pengguna",
  },

  NOTIFICATION: "/portal/blast-promo",
  AUDIT_TRAIL: {
    BASE: "/portal/audit-trail",
    ADMIN_BANK: "/portal/audit-trail/admin-bank",
    CUSTOMER: "/portal/audit-trail/customer",
  },
  NOTIF: "/portal/notif",
  VIRTUAL_ACCOUNT: "/virtual-account",
  HARD_TOKEN: "/hard-token",
  WORKSHOP: "/workshop",
  WORKSHOP_CUSTOM: "/workshoptwo",
  CUSTOM_PAGE: "/custompage",
};

export { pathnameCONFIG };
