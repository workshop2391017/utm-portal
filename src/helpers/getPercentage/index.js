const getPercentage = (number1, number2) => {
  if (number2 === 0) {
    return "-";
  }
  const number = (number1 / number2) * 100;
  return `${number.toFixed(2)}%`;
};

export default getPercentage;
