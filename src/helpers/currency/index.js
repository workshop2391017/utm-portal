// Add here if there is new currency added

export const Currency = (input) => {
  if (input === "IDR") return "Indonesian Rupiah";
  if (input === "USD") return "Unites States Dollar";
  if (input === "SGD") return "Singapore Dollar";
  if (input === "AUD") return "Australian Dollar";
  if (input === "SAR") return "Saudi Arabia Riyal";
  if (input === "EUR") return "Euro";
  if (input === "JPY") return "Japanese Yen";
  if (input === "GBP") return "Pound Sterling";
  if (input === "HKD") return "Hongkong Dollar";
  if (input === "MYR") return "Malaysian Ringgit";
  if (input === "CNY") return "Chinese Yuan";
  if (input === "CAD") return "Canadian Dollar";
  if (input === "CHF") return "Swiss Franc";
};
