import * as yup from 'yup';

const validateForm = () => ({
  max: yup.object({
    max: yup.array(
      yup.object({
        limitType: yup.string(),
        pindahDanaAmount: yup.string().required('cannot be empty'),
        sameBankAmount: yup.string().required('cannot be empty'),
        otherBankAmount: yup.string().required('cannot be empty'),
        sknAmount: yup.string().required('cannot be empty'),
        rtgsAmount: yup.string().required('cannot be empty'),
        paymentAmount: yup.string().required('cannot be empty'),
        cashDepositAmount: yup.string().required('cannot be empty'),
        cashWithdrawalAmount: yup.string().required('cannot be empty'),
      })
    ),
  }),
  min: yup.object({
    min: yup.array(
      yup.object({
        limitType: yup.string(),
        pindahDanaAmount: yup.string().required('cannot be empty'),
        sameBankAmount: yup.string().required('cannot be empty'),
        otherBankAmount: yup.string().required('cannot be empty'),
        sknAmount: yup.string().required('cannot be empty'),
        rtgsAmount: yup.string().required('cannot be empty'),
        paymentAmount: yup.string().required('cannot be empty'),
        cashDepositAmount: yup.string().required('cannot be empty'),
        cashWithdrawalAmount: yup.string().required('cannot be empty'),
      })
    ),
  }),
});

export default validateForm;
