export const validPassword = new RegExp(
  "^(?=.*?[A-Za-z])(?=.*?[0-9])(?!.*[ ]).{8,10}$"
);

export const validUsername = new RegExp(
  "(?=^.{10,30}$)(?=.*[a-z])(?!.*[A-Z])(?!.*[ ])(?!.*[~.,<>?/;':|!@#$%^&*()_+=-])"
);

export const validNumber = /^(\s*|\d+)$/;

export const validEmail =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
