export default function timeFormatHandler(userInput) {
  const value = userInput.replace(/\D/g, "");

  return value.replace(/\B(?=(\d{2})+(?!\d))/g, ":");
}
