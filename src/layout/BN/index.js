// main
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "BNindex.scss";

// libraries
import { Layout } from "antd";

// components
import SideBar from "components/BN/TemplateLayout/SideBar";
import Header from "components/BN/TemplateLayout/Header";
import IdleTimerContainer from "components/BN/IdleContainerTimer";
import HeaderContext from "helpers/context/header";

const { Content } = Layout;

const Container = ({ children }) => {
  const history = useHistory();
  const [isRefreshed, setIsRefreshed] = useState(false);

  const onRefresh = () => {
    setIsRefreshed(!isRefreshed);
  };

  const hiddenSidebar = () => {
    const path = history?.location?.pathname;

    if (
      [
        "/portal/management-company/registration/new",
        "/portal/management-company/change-authorization/change",
      ].includes(path)
    ) {
      return true;
    }

    return false;
  };

  return (
    <HeaderContext.Provider
      value={{
        isRefreshed,
      }}
    >
      <Layout
        style={{
          minHeight: "100vh",
          background: "#fff",
          fontFamily: "FuturaBkBT",
        }}
      >
        {hiddenSidebar() ? null : <SideBar />}
        <Layout style={{ backgroundColor: "#F9FAFF" }}>
          <Content
            style={{
              // width: '100%',
              margin: hiddenSidebar() ? "60px 0" : "60px 0 0 200px",
              // paddingRight: 200
            }}
          >
            <Header onRefresh={onRefresh} />
            {children}
            <IdleTimerContainer />
          </Content>
        </Layout>
      </Layout>
    </HeaderContext.Provider>
  );
};

export default Container;
