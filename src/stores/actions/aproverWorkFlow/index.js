// aproverWorkFlow Actions

import { setLocalStorage } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import {
  serviceRequestApprovelMatrixFinanceAllService,
  serviceRequestGetWorkFlow,
  serviceRequestGetWorkFlowBulkExcute,
  serviceRequestGetWorkFlowDetail,
  serviceRequestGetWorkFlowExcute,
} from "utils/api";
// const {
//   serviceRequestGetWorkFlow,
//   serviceRequestGetWorkFlowDetail,
//   serviceRequestGetWorkFlowExcute,
// } = require("utils/api");
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "aproverWorkFlow/SET_LOADING";
export const CLEAR_ERROR = "aproverWorkFlow/CLEAR_ERROR";
export const SET_ERROR = "aproverWorkFlow/SET_ERROR";
export const INIT_DATA = "aproverWorkFlow/INIT_DATA";
export const SET_DATA = "aproverWorkFlow/SET_DATA";
export const SET_PAGE = "aproverWorkFlow/SET_PAGE";
export const SET_DATA_DETAIL = "aproverWorkFlow/SET_DATA_DETAIL";
export const SET_CONFIRMATION = "aproverWorkFlow/SET_CONFIRMATION ";
export const SET_DATA_FILTER = "aproverWorkFlow/SET_DATA_FILTER";
export const SET_DATA_EMPATY_FILTER = "aproverWorkFlow/SET_DATA_EMPATY_FILTER";
export const SET_DATA_REFF_NUMBER = "aproverWorkFlow/SET_DATA_REFF_NUMBER";
export const SET_DATA_BULK_REFF_NUMBER =
  "aproverWorkFlow/SET_DATA_BULK_REFF_NUMBER";
export const SET_DOUBLE_SUBMIT = "aproverWorkFlow/SET_DOUBLE_SUBMIT";
export const SET_SELECTED_APPROVE = "aproverWorkFlow/SET_SELECTED_APPROVE";
export const GET_IS_DETAIL_APPROVAL = "aproverWorkFlow/GET_IS_DETAIL_APPROVAL";
export const SET_SIDEBAR_MENU = "aproverWorkFlow/SET_SIDEBAR_MENU";
export const SET_ACTIVATED_MENU = "aproverWorkFlow/SET_ACTIVATED_MENU ";
export const SET_INDIKASI_REJECT_OR_APPROVE =
  "aproverWorkFlow/SET_INDIKASI_REJECT_OR_APPROVE";
export const CLEAN_FILTER = "aproverWorkFlow/CLEAN_FILTER";
export const ALL_SERVICES_MATRIX = "aproverWorkFlow/ALL_SERVICES_MATRIX";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const cleanFilter = () => ({
  type: CLEAN_FILTER,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setPageAction = (payload) => ({
  type: SET_PAGE,
  payload,
});

export const setSelectedApprove = (payload) => ({
  type: SET_SELECTED_APPROVE,
  payload,
});

export const setTypeErrorHandling = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeClearError = () => ({
  type: CLEAR_ERROR,
});

export const setTypeDataFilter = (payload) => ({
  type: SET_DATA_FILTER,
  payload,
});

export const setTypeEmpatyFilter = (payload) => ({
  type: SET_DATA_EMPATY_FILTER,
  payload,
});

export const setTypeDataDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setTypeSuccesConfirmation = (payload) => ({
  type: SET_CONFIRMATION,
  payload,
});

export const setTypeReffNumber = (payload) => ({
  type: SET_DATA_REFF_NUMBER,
  payload,
});
export const setTypeBulkReffNumber = (payload) => ({
  type: SET_DATA_BULK_REFF_NUMBER,
  payload,
});

export const setGetIsDetail = (payload) => ({
  type: GET_IS_DETAIL_APPROVAL,
  payload,
});

export const setBarMenuApprovelWorkFlow = (payload) => ({
  type: SET_SIDEBAR_MENU,
  payload,
});

export const setTypeActivatedMenu = (payload) => ({
  type: SET_ACTIVATED_MENU,
  payload,
});

export const setIndikasiRejectOrApprove = (payload) => ({
  type: SET_INDIKASI_REJECT_OR_APPROVE,
  payload,
});

export const setAllServicesMatrix = (payload) => ({
  type: ALL_SERVICES_MATRIX,
  payload,
});

export const getDataApprovelWorkFlowManagmentPerusahaan =
  (payload) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const res = await serviceRequestGetWorkFlow(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(setData(res?.data));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeErrorHandling(err?.message));
    }
  };

export const getDataApprovelWorkFlow =
  ({ searchValue }) =>
  async (dispatch, getState) => {
    try {
      const dataFilter = getState().aproverWorkFlow.dataFilter;
      // const pageSelected = getState().aproverWorkFlow.pageSelected;
      const approveSelected = getState().aproverWorkFlow.approveSelected;
      const { fromDate, toDate, workFlowStatus, pageSelected } = dataFilter;
      const payload = {
        fromDate: fromDate || null,
        menuName: approveSelected.workflowName,
        page: pageSelected - 1,
        size: 10,
        toDate: toDate || null,
        searchValue,
        workFlowStatus,
        id: null,
        isMatrixApproval: false,
      };
      dispatch(setLoading(true));
      const res = await serviceRequestGetWorkFlow(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(setData(res?.data));
      } else {
        dispatch(setData({}));
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeErrorHandling(err?.message));
    }
  };

export const getDataApprovelWorkFlowManagmentPerusahaanDetail =
  (payload) => async (dispatch) => {
    try {
      setLocalStorage("reffNumber", payload.reffNo);
      dispatch(setTypeReffNumber(payload.reffNo));
      dispatch(setLoading(true));
      // dispatch(setTypeDataDetail({}));

      const res = await serviceRequestGetWorkFlowDetail(payload);
      dispatch(setLoading(false));
      dispatch(setGetIsDetail(true));
      if (res.status === 200) {
        dispatch(
          setTypeDataDetail({
            metadata: JSON.parse(res?.data?.metadata),
            additionalData: JSON.parse(res?.data?.additionalData),
            userDataApprovalWorkflowDto: res?.data?.userDataApprovalWorkflowDto,
          })
        );
      } else {
        dispatch(setGetIsDetail(false));
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setGetIsDetail(false));
      dispatch(setTypeErrorHandling(err?.message));
    }
  };

export const ExcuteDataApprovelWorkFlowManagmentPerusahaan =
  (payload) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const res = await serviceRequestGetWorkFlowExcute(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(setTypeSuccesConfirmation(true));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeErrorHandling(err?.message));
    }
  };

export const ExcuteDataApprovelWorkFlow =
  (typeApproval, comment = "") =>
  async (dispatch, getState) => {
    try {
      const action = typeApproval === "approve" ? 5 : 6;
      const approveSelected = getState().aproverWorkFlow.approveSelected;
      const reffNo = getState().aproverWorkFlow.reffNo;
      const payload = {
        referenceNumber: reffNo,
        workFlowMenu: approveSelected.workflowName,
        action,
        modifiedReason: comment,
      };
      dispatch(setLoading(true));
      const res = await serviceRequestGetWorkFlowExcute(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(
          setTypeSuccesConfirmation({
            isSuccess: true,
            isToReleaser: res?.data?.isToReleaser,
          })
        );
        dispatch(setIndikasiRejectOrApprove(action));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeErrorHandling(err?.message));
    }
  };
export const ExcuteDataBulkApprovelWorkFlow =
  (typeApproval, comment = "") =>
  async (dispatch, getState) => {
    try {
      const action = typeApproval === "approve" ? 5 : 6;
      const approveSelected = getState().aproverWorkFlow.approveSelected;
      const reffNoBulk = getState().aproverWorkFlow.reffNoBulk;

      const result = reffNoBulk.map((item) => ({
        workFlowMenu: approveSelected.workflowName,
        referenceNumber: item.referenceNumber.toString(),
      }));
      const payload = {
        approvalList: result,
        action,
        modifiedReason: comment,
      };
      dispatch(setLoading(true));
      const res = await serviceRequestGetWorkFlowBulkExcute(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(
          setTypeSuccesConfirmation({
            isSuccess: true,
            isToReleaser: res?.data?.isToReleaser,
          })
        );
        dispatch(setIndikasiRejectOrApprove(action));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeErrorHandling(err?.message));
    }
  };

export const getAllService = (payload) => async (dispatch, getState) => {
  try {
    const res = await serviceRequestApprovelMatrixFinanceAllService(payload);

    if (res.status === 200) {
      dispatch(setAllServicesMatrix(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErrorHandling(error?.message));
  }
};
