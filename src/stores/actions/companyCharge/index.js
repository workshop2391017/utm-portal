// companyCharge Actions
// --------------------------------------------------------

import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import {
  serviceRequestGetCompanyCharge,
  serviceRequestGetCompanyChargeDetail,
  serviceRequestAddCompanyCharge,
  serviceRequestAllCurrencyCompanyLimit,
  serviceRequestGetTearingSlab,
  serviceRequestSaveCompanyCharge,
  serviceRequestDownloadFileCompanyCharge,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";

export const SET_LOADING = "companyCharge/SET_LOADING";
export const CLEAR_ERROR = "companyCharge/CLEAR_ERROR";
export const SET_ERROR = "companyCharge/SET_ERROR";
export const INIT_DATA = "companyCharge/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "companyCharge/SET_DOUBLE_SUBMIT";
export const SET_DATA = "companyCharge/SET_DATA";
export const SET_DATA_FORM = "companyCharge/SET_DATA_FORM";
export const SET_DETAIL = "companyCharge/SET_DETAIL";
export const SET_LIST_TIERING = "companyCharge/SET_LIST_TIERING";
export const SET_LIST_CURRENCY = "companyCharge/SET_CURRENCY_LIMIT";
export const SET_SUCCESS_CONFIRMATION =
  "companyCharge/SET_SUCCESS_CONFIRMATION";
export const SET_HANDLE_CLEAR_ERROR = "companyCharge/SET_HANDLE_CLEAR_ERROR";
export const SET_SAVE_SPECIAL_CHARGE = "companyCharge/SET_SAVE_SPECIAL_CHARGE";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});
export const setFormDataPayload = (payload) => ({
  type: SET_DATA_FORM,
  payload,
});
export const setPopUpSuccess = (payload) => ({
  type: SET_SUCCESS_CONFIRMATION,
  payload,
});
export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});
export const setListCurrency = (payload) => ({
  type: SET_LIST_CURRENCY,
  payload,
});
export const setListTiering = (payload) => ({
  type: SET_LIST_TIERING,
  payload,
});
export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});
export const setSaveSpecialCharge = (payload) => ({
  type: SET_SAVE_SPECIAL_CHARGE,
  payload,
});

export const getDataChargeCompany = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetCompanyCharge(payload);

    if (res.status === 200) {
      dispatch(setData(res?.data));
      dispatch(setLoading(false));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getDataChargeCompanyDetail = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const res = await serviceRequestGetCompanyChargeDetail(payload);
    if (res.status === 200) {
      dispatch(setDetail(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }

    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setHandleError(error.message));
    dispatch(setLoading(false));
  }
};

export const saveSpecialCharge = (payload) => async (dispatch) => {
  dispatch(setLoading(true));

  try {
    const res = await serviceRequestSaveCompanyCharge(payload);

    if (res.status === 200) {
      dispatch(setSaveSpecialCharge(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setHandleError(err?.data?.message));
  }
};

export const addDataCompanyCharge = (payload) => async (dispatch) => {
  dispatch(setLoading(true));

  try {
    const res = await serviceRequestAddCompanyCharge(payload);

    if (res.status === 200) {
      dispatch(setPopUpSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setHandleError(err?.data?.message));
  }
};

export const getListCurrency = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestAllCurrencyCompanyLimit(payload);

    if (res.status === 200) {
      dispatch(setListCurrency(res?.data?.currencyList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandleError(err?.data?.message));
  }
};
export const getListTiering = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestGetTearingSlab(payload);

    if (res.status === 200) {
      dispatch(
        setListTiering(
          (res.data?.tieringList ?? []).map((elm) => ({
            label: elm?.name,
            value: [elm.name, elm.id],
          }))
        )
      );
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandleError(err?.data?.message));
  }
};

export const getDataDownloadFile =
  (payload, fileName = "document") =>
  async (dispatch) => {
    try {
      await dispatch(checkOnline());
      dispatch(setLoading(true));
      const response = await serviceRequestDownloadFileCompanyCharge({
        file: payload,
      });
      const { status, data } = response;
      dispatch(setLoading(false));

      const d = new Date();
      if (status === 200) {
        const getFileName = `${fileName}-${d?.getMilliseconds()}.pdf`;
        const url = window.URL.createObjectURL(data);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", getFileName);
        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(status)) {
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          let data = response.data;
          reader.onload = () => {
            data = JSON.parse(reader.result);
            resolve(Promise.reject(data));
          };
          reader.onerror = () => {
            reject(data);
          };
          reader.readAsText(data);
        })
          .then((e) => {
            dispatch(setHandleError(e?.data?.message));
          })
          .catch((e) => {
            dispatch(setHandleError(e?.data?.message));
          });
        dispatch(setLoading(false));
      }
    } catch {
      dispatch(setLoading(false));
    }
  };
