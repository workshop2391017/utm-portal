// bankUserSetting Actions
// --------------------------------------------------------

import {
  serviceBlockedDetail,
  serviceBlockUser,
  serviceEndSession,
  serviceManagamentUser,
  serviceUnBlockUser,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE } from "../errorGeneral";

export const SET_LOADING = "bankUserSetting/SET_LOADING";
export const CLEAR_ERROR = "bankUserSetting/CLEAR_ERROR";
export const SET_ERROR = "bankUserSetting/SET_ERROR";
export const INIT_DATA = "bankUserSetting/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "bankUserSetting/SET_DOUBLE_SUBMIT";
export const SET_DETAIL_DATA = "bankUserSetting/SET_DETAIL_DATA";
export const SET_USERID = "bankUserSetting/SET_USERID";
export const SET_BLOCKUNBLOCK = "bankUserSetting/SET_BLOCKUNBLOCK";
export const SET_ACTION_LOADING = "bankUserSetting/SET_ACTION_LOADING";
export const SET_SUCCESS = "bankUserSetting/SET_SUCCESS";

export const actionUserBlockUnBlockConf = {
  BLOCK: "BLOCK",
  UNBLOCK: "UNBLOCK",
  ENDSESSION: "ENDSESSION",
};

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitialData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setDetailData = (payload) => ({
  type: SET_DETAIL_DATA,
  payload,
});

export const setUserId = (payload) => ({
  type: SET_USERID,
  payload,
});

export const setBlockUnBlock = (payload) => ({
  type: SET_BLOCKUNBLOCK,
  payload,
});

export const setLoadingAction = (payload) => ({
  type: SET_ACTION_LOADING,
  payload,
});

export const handleUserData = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceManagamentUser(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setInitialData(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setError(e.message));
  }
};

export const handleBlockedDetail = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceBlockedDetail(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setDetailData(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setError(e.message));
  }
};

export const handleBlockedUser =
  (payload, setOpenConfirmation) => async (dispatch) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceBlockUser(payload);
      dispatch(setLoading(false));
      setOpenConfirmation((prev) => ({ action: prev.action, isOpen: false }));

      if (response.status === 200) {
        dispatch(
          setBlockUnBlock({
            action: actionUserBlockUnBlockConf.BLOCK,
          })
        );
        dispatch(setSuccess(true));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
      }
      // dispatch(setLoadingAction({ [actionUserBlockUnBlockConf.BLOCK]: false }));
    } catch (e) {
      // dispatch(setLoadingAction({ [actionUserBlockUnBlockConf.BLOCK]: false }));
      dispatch(setError(e.message));
    }
  };

export const handleUnBlockedUser =
  (payload, setOpenConfirmation) => async (dispatch) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceUnBlockUser(payload);
      dispatch(setLoading(false));
      setOpenConfirmation((prev) => ({ action: prev.action, isOpen: false }));

      if (response.status === 200) {
        dispatch(
          setBlockUnBlock({
            action: actionUserBlockUnBlockConf.UNBLOCK,
          })
        );
        dispatch(setSuccess(true));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
      }

      // dispatch(setLoadingAction({ [actionUserBlockUnBlockConf.UNBLOCK]: false }));
    } catch (e) {
      // dispatch(setLoadingAction({ [actionUserBlockUnBlockConf.UNBLOCK]: false }));
      dispatch(setError(e.message));
    }
  };

export const handleEndSession = (payload) => async (dispatch) => {
  try {
    // dispatch(
    //   setLoadingAction({ [actionUserBlockUnBlockConf.ENDSESSION]: true })
    // );
    const response = await serviceEndSession(payload);

    if (response.status === 200) {
      dispatch(setSuccess(true));
    } else {
      throw response;
    }
    // dispatch(
    //   setLoadingAction({ [actionUserBlockUnBlockConf.ENDSESSION]: false })
    // );
  } catch (error) {
    // dispatch(setLoadingAction({ [actionUserBlockUnBlockConf.UNBLOCK]: false }));
    dispatch(handleErrorBE(error));
  }
};
