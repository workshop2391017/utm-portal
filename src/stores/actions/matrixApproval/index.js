// matrixApproval Actions
// --------------------------------------------------------

import {
  serviceApprovalMatrixAdd,
  serviceDeleteApprovalMatrix,
  serviceApprovalMatrixAllMenuType,
  serviceGetGroupByBranchId,
  serviceGetPrivilegeHakaksesGroup,
  serviceMenuGlobalApprovalMatrix,
  serviceMenuGlobalApprovalMatrixGetSquence,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "matrixApproval/SET_LOADING";
export const CLEAR_ERROR = "matrixApproval/CLEAR_ERROR";
export const SET_ERROR = "matrixApproval/SET_ERROR";
export const INIT_DATA_MENU = "matrixApproval/INIT_DATA_MENU";
export const SET_DOUBLE_SUBMIT = "matrixApproval/SET_DOUBLE_SUBMIT";
export const SET_MATRIX_SQUENCE = "matrixApproval/SET_MATRIX_SQUENCE";
export const SET_SELECTED_MENU = "matrixApproval/SET_SELECTED_MENU";
export const SET_LOADING_SQUENCE = "matrixApproval/SET_LOADING_SQUENCE";
export const SET_SUCCESS_SAVE = "matrixApproval/SET_SUCCESS_SAVE";
export const SET_LOADING_SAVE = "matrixApproval/SET_LOADING_SAVE";
export const SET_LOADING_DELETE = "matrixApproval/SET_LOADING_DELETE";
export const SET_SUCCESS_DELETE = "matrixApproval/SET_SUCCESS_DELETE";
export const SET_CLEAR = "matrixApproval/SET_CLEAR";
export const SET_ALL_LAYANAN = "matrixApproval/SET_ALL_LAYANAN";
export const SET_CLEAR_ERROR = "matrixApproval/SET_CLEAR_ERROR";
export const SET_SELECTED_KEYS = "matrixApproval/SET_SELECTED_KEYS";

export const SET_ACTION_APPROVAL_MATRIX_CUSTOMER =
  "matrixApproval/SET_ACTION_APPROVAL_MATRIX_CUSTOMER";
export const SET_ACTION_APPROVAL_MATRIX_ADMIN =
  "matrixApproval/SET_ACTION_APPROVAL_MATRIX_ADMIN";
export const SET_MATRIX_APPROVAL_TARGET_GRP =
  "matrixApproval/SET_MATRIX_APPROVAL_TARGET_GRP";

export const ADD_MATRIX_APPROVAL = "Add";
export const EDIT_MATRIX_APPROVAL = "Change";

export const ADD_CUSTOMER_MATRIX_APPROVAL =
  "matrixApproval/ADD_CUSTOMER_MATRIX_APPROVAL";
export const EDIT_CUSTOMER_MATRIX_APPROVAL =
  "matrixApproval/EDIT_CUSTOMER_MATRIX_APPROVAL";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setMatrixDetailList = (payload) => ({
  type: SET_MATRIX_SQUENCE,
  payload,
});

export const setSelectedMenu = (payload) => ({
  type: SET_SELECTED_MENU,
  payload,
});

export const setSelectedKeys = (payload) => ({
  type: SET_SELECTED_KEYS,
  payload,
});

export const setInitDataMenu = (payload) => ({
  type: INIT_DATA_MENU,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setLoadingSquence = (payload) => ({
  type: SET_LOADING_SQUENCE,
  payload,
});

export const setMatrixApprovalActionInsert = (payload) => ({
  type: SET_ACTION_APPROVAL_MATRIX_ADMIN,
  payload,
});

export const setMatrixApprovalTargetGrp = (payload) => ({
  type: SET_MATRIX_APPROVAL_TARGET_GRP,
  payload,
});

export const setisLoadingSave = (payload) => ({
  type: SET_LOADING_SAVE,
  payload,
});

export const setSuccessSave = (payload) => ({
  type: SET_SUCCESS_SAVE,
  payload,
});

export const setLoadingDelete = (payload) => ({
  type: SET_LOADING_DELETE,
  payload,
});

export const setSuccessDelete = (payload) => ({
  type: SET_SUCCESS_DELETE,
  payload,
});

export const setClear = (payload) => ({
  type: SET_CLEAR,
  payload,
});

export const setAllLayanan = (payload) => ({
  type: SET_ALL_LAYANAN,
  payload,
});

export const setClearError = (payload) => ({
  type: SET_CLEAR_ERROR,
  payload,
});

export const renderMenuOption = (menuAccessList) =>
  (menuAccessList ?? [])
    .map((e, i) => ({
      ...e,
      key: `sub${i}`,
      label: e.name_en || e.name_id,
      children:
        e.menuAccessChild.map((c, ci) => ({
          ...c,
          key: `sub${ci}-${i}`,
          label: c.name_en || c.name_id,
        })) || [],
    }))
    .map((e) => {
      delete e.menuAccessChild;
      return e;
    });

export const handleApprovalMatrixMenu = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await serviceMenuGlobalApprovalMatrix(dataParams);
    if (response.status === 200) {
      const data = [];
      response?.data?.menuAccessList
        .filter((em) => !em.name_en.includes("Trade"))
        .forEach((el) => {
          data.push({
            ...el,
            menuAccessChild: el.menuAccessChild.sort((a, b) =>
              a.order > b.order ? 1 : -1
            ),
          });
        });
      dispatch(setInitDataMenu(renderMenuOption(data)));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setInitDataMenu([]));
      dispatch(setLoading(false));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setInitDataMenu([]));
    dispatch(setLoading(false));
    dispatch(setError(e?.message));
  }
};

export const handleApprovalMatrixAllMenu = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoading(true));

  try {
    const response = await serviceApprovalMatrixAllMenuType(dataParams);
    if (response.status === 200) {
      dispatch(
        setAllLayanan(
          response?.data?.workflowApprovalMatrixAllServiceDtos?.map((elm) => ({
            ...elm,
            checked: elm?.parentMenuId !== null,
          }))
        )
      );
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setAllLayanan([]));
      dispatch(setLoading(false));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setAllLayanan([]));
    dispatch(setLoading(false));
    dispatch(setError(e?.message));
  }
};

export const handleApprovalMatrixSquence = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoadingSquence(true));
    const response = await serviceMenuGlobalApprovalMatrixGetSquence(
      dataParams
    );
    if (response.status === 200) {
      dispatch(setMatrixDetailList(response?.workflowGroupPortalDtoList));

      const sq =
        response.data.workflowApprovalMatrixPortalDetailDtoList?.[0] ?? {};

      dispatch(
        setMatrixDetailList(
          !Object.keys(sq).length
            ? {}
            : {
                squenceHeader: sq,
                squence: sq?.workflowGroupPortalDtoList?.map((elm) => ({
                  seq: elm.totalSequence,
                  kelompok: elm.groupType,
                  pengguna: elm.totalApproval,
                  targetKelompok: elm?.portalGroupId, // for save
                  targetKelompokName: elm?.portalGroupName, // for save
                })),
              }
        )
      );
      dispatch(setLoadingSquence(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setMatrixDetailList([]));
      dispatch(setLoadingSquence(false));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setMatrixDetailList([]));
    dispatch(setLoadingSquence(false));
    dispatch(setError(e?.message));
  }
};

export const matrixApprovalTargetGrp = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  // dispatch(setLoading(true));
  try {
    const response = await serviceGetGroupByBranchId(payload);
    if (response.status === 200) {
      dispatch(setMatrixApprovalTargetGrp(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
    // dispatch(setLoading(false));
  } catch (err) {
    // dispatch(setLoading(false));
    dispatch(setError(err?.message));
  }
};

export const matrixApprovalSave =
  (payload, ifEditWithChild) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setisLoadingSave(true));

    const { allLayananMenu, actionAdmin, selectedMenu, matrixSquence } =
      getState()?.matrixApproval;

    const actionStatus =
      actionAdmin === EDIT_MATRIX_APPROVAL
        ? { isEdited: true, isAdded: false, isDeleted: false }
        : { isAdded: true, isEdited: false, isDeleted: false };

    const selectedMenuPayload = [
      {
        workflowApprovalMatrix: {
          ...(actionAdmin === EDIT_MATRIX_APPROVAL && {
            id: matrixSquence?.squenceHeader?.workflowApprovalMatrix?.id,
          }),
          masterMenuId: selectedMenu?.id,
          totalSequence: payload?.noOfSquence,
        },
        workflowGroupPortalDtoList: payload?.squenceList.map((elm) => ({
          groupType: elm?.kelompok,
          portalGroupId: elm?.targetKelompok ?? null,
          totalApproval: Number(elm?.pengguna),
          totalSequence: Number(elm?.seq),
        })),
      },
    ];

    const payloadData = {
      menuName: selectedMenu?.label,
      approvalMatrixType: payload?.menuType,
      ...(ifEditWithChild && {
        allMenuId: selectedMenu?.id,
        parentMenuId: selectedMenu?.parent_id,
      }),
      workflowApprovalMatrixPortalDetailDtoList: allLayananMenu.filter(
        (e) => e.checked
      )?.length
        ? [
            ...allLayananMenu
              ?.filter((e) => e.checked)
              .map((elm, i) => ({
                workflowApprovalMatrix: {
                  ...(elm?.workflowApprovalMatrixId && {
                    id: elm.workflowApprovalMatrixId,
                  }),
                  masterMenuId: elm?.masterMenuId,
                  totalSequence: payload?.noOfSquence,
                },
                workflowGroupPortalDtoList: payload?.squenceList.map((se) => ({
                  groupType: se?.kelompok,
                  portalGroupId: se?.targetKelompok,
                  totalApproval: Number(se?.pengguna),
                  totalSequence: Number(se?.seq),
                })),
              })),
            ...selectedMenuPayload,
          ]
        : selectedMenuPayload,
      ...actionStatus,
    };

    try {
      const response = await serviceApprovalMatrixAdd(payloadData);
      if (response.status === 200) {
        dispatch(setSuccessSave(response.data));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
      }
      dispatch(setisLoadingSave(false));
    } catch (err) {
      dispatch(setisLoadingSave(false));
      dispatch(setError(err?.message));
    }
  };

export const matrixApprovalDelete =
  (payload, ifEditWithChild) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoadingDelete(true));

    const { selectedMenu, allLayananMenu, matrixSquence } =
      getState()?.matrixApproval;

    const selectedMenuPayload = [
      {
        workflowApprovalMatrix: {
          isDelete: true,
          id: matrixSquence?.squenceHeader?.workflowApprovalMatrix?.id,
          masterMenuId: selectedMenu?.id,
          totalSequence:
            matrixSquence?.squenceHeader?.workflowGroupPortalDtoList?.length,
        },
        workflowGroupPortalDtoList:
          matrixSquence?.squenceHeader?.workflowGroupPortalDtoList.map(
            ({ portalGroupName, ...rest }) => rest
          ),
      },
    ];

    const payloadData = {
      menuName: selectedMenu?.label,
      approvalMatrixType: payload?.menuType,
      isDeleted: true,
      ...(ifEditWithChild && {
        allMenuId: selectedMenu?.id,
        parentMenuId: selectedMenu?.parent_id,
      }),
      workflowApprovalMatrixPortalDetailDtoList: allLayananMenu.filter(
        (e) => e.checked
      )?.length
        ? [
            ...allLayananMenu
              ?.filter((e) => e.checked)
              .map((elm, i) => ({
                workflowApprovalMatrix: {
                  isDelete: true,
                  id: elm?.workflowApprovalMatrixId,
                  masterMenuId: elm?.masterMenuId,
                  totalSequence:
                    matrixSquence?.squenceHeader?.workflowGroupPortalDtoList
                      ?.length,
                },
                workflowGroupPortalDtoList:
                  matrixSquence?.squenceHeader?.workflowGroupPortalDtoList.map(
                    ({ portalGroupName, ...rest }) => rest
                  ),
              })),
            ...selectedMenuPayload,
          ]
        : selectedMenuPayload,
    };

    try {
      const response = await serviceDeleteApprovalMatrix(payloadData);
      if (response.status === 200) {
        dispatch(setSuccessDelete(true));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
      }
      dispatch(setLoadingDelete(false));
    } catch (err) {
      dispatch(setLoadingDelete(false));
      dispatch(setError(err?.message));
    }
  };
