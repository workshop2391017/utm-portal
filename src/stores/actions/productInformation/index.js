// productInformation Actions
// --------------------------------------------------------

import {
  serviceAccountProductGet,
  serviceGetAllProductList,
  serviceGetAllProductCodeList,
  serviceGetInfoCurrency,
  serviceAccountProductAdd,
  serviceAccountProductEdit,
  serviceAccountProductDetail,
  serviceInquiryProduct,
  serviceAccountProductDelete,
  serviceRequestGetProductAccountType,
} from "utils/api";
import { handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "productInformation/SET_LOADING";
export const SET_ADD_LOADING = "productInformation/SET_ADD_LOADING";
export const CLEAR_ERROR = "productInformation/CLEAR_ERROR";
export const SET_ERROR = "productInformation/SET_ERROR";
export const INIT_DATA = "productInformation/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "productInformation/SET_DOUBLE_SUBMIT";
export const SET_DATA = "productInformation/SET_DATA";
export const SET_DETAIL = "productInformation/SET_DETAIL";
export const SET_PRODUCTS = "productInformation/SET_PRODUCTS";
export const SET_PRODUCT_CODES = "productInformation/SET_PRODUCT_CODES";
export const SET_PRODUCT_NAME_ACCOUNT_TYPE =
  "productInformation/SET_PRODUCT_NAME_ACCOUNT_TYPE";
export const SET_CURRENCIES = "productInformation/SET_CURRENCIES";
export const SET_SUCCESS = "productInformation/SET_SUCCESS";
export const SET_ADD_SUCCESS = "productInformation/SET_ADD_SUCCESS";
export const SET_IS_EDIT = "productInformation/SET_IS_EDIT";

export const setIsEdit = (payload) => ({
  type: SET_IS_EDIT,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setAddLoading = (payload) => ({
  type: SET_ADD_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setProducts = (payload) => ({
  type: SET_PRODUCTS,
  payload,
});

export const setProductCodes = (payload) => ({
  type: SET_PRODUCT_CODES,
  payload,
});

export const setProductNameAccountType = (payload) => ({
  type: SET_PRODUCT_NAME_ACCOUNT_TYPE,
  payload,
});

export const setCurrencies = (payload) => ({
  type: SET_CURRENCIES,
  payload,
});

export const setSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});

export const setAddSuccess = (payload) => ({
  type: SET_ADD_SUCCESS,
  payload,
});
export const setInitData = () => ({
  type: INIT_DATA,
});

export const actionAccountProductGet = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceAccountProductGet(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setData(res.data));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionInquiryProduct = (payload) => async (dispatch) => {
  try {
    const res = await serviceInquiryProduct(payload);

    if (res.status === 200) {
      const data = { ...res.data.data[0] };

      dispatch(setProductNameAccountType(data));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionGetAccountType = () => async (dispatch) => {
  try {
    const res = await serviceRequestGetProductAccountType();

    if (res.status === 200) {
      const data = res.data.accountTypeList.map((product) => ({
        value: product,
        label: product,
        key: product,
      }));

      dispatch(setProducts(data));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionAccountProductDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setIsEdit(true));
    dispatch(setLoading(true));
    const res = await serviceAccountProductDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDetail(res.data));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

// export const actionGetAllProductList = () => async (dispatch) => {
//   try {
//     const res = await serviceGetAllProductList();

//     if (res.status === 200) {
//       dispatch(setProducts(res.data.productEsbDtoList));
//     } else {
//       throw res;
//     }
//   } catch (error) {
//     dispatch(handleErrorBE(error));
//   }
// };

export const actionGetAllProductCodeList = (payload) => async (dispatch) => {
  try {
    const res = await serviceGetAllProductCodeList(payload);
    if (res.status === 200) {
      const data = res.data.accountProductDtoList.map(({ productCode }) => ({
        value: productCode,
        label: productCode,
        key: productCode,
      }));

      dispatch(setProductCodes(data));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionGetInfoCurrency = () => async (dispatch) => {
  try {
    const res = await serviceGetInfoCurrency();

    if (res.status === 200) {
      dispatch(setCurrencies(res.data.currencyList));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionAccountProductAdd =
  (payload, closeConfirmationHandler) => async (dispatch) => {
    try {
      dispatch(setAddLoading(true));
      const res = await serviceAccountProductAdd(payload);
      dispatch(setAddLoading(false));
      closeConfirmationHandler();

      if (res.status === 200) {
        dispatch(setAddSuccess(true));
      } else {
        throw res;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    } finally {
      dispatch(setInitData());
    }
  };

export const actionAccountProductEdit =
  (payload, closeConfirmationHandler) => async (dispatch) => {
    try {
      dispatch(setAddLoading(true));
      const res = await serviceAccountProductEdit(payload);
      dispatch(setAddLoading(false));
      closeConfirmationHandler();

      if (res.status === 200) {
        dispatch(setAddSuccess(true));
      } else {
        throw res;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionAccountProductDelete =
  (payload, closeConfirmationHandler) => async (dispatch) => {
    try {
      dispatch(setAddLoading(true));
      const res = await serviceAccountProductDelete(payload);
      dispatch(setAddLoading(false));
      closeConfirmationHandler();
      if (res.status === 200) {
        dispatch(setSuccess(true));
      } else {
        throw res;
      }
    } catch (error) {
      dispatch(handleErrorBE(error, false));
    }
  };
