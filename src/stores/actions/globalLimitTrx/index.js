// globalLimitTrx Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

const {
  serviceRequestGetServiceGlobalLimitTrx,
  serviceRequestGetTransactionCategoryGlobalLimitTrx,
  serviceRequestGetTransactionCurrencyMatrixGlobalLimitTrx,
  serviceRequestAddGlobalLimitTrx,
  serviceRequestGetGloalLimit,
  serviceRequestGetTransactionCurrencyMatrix,
  serviceRequestAddGloalLimit,
  serviceRequestGetCurrency,
  serviceRequestValidationLimitManagementService,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "globalLimitTrx/SET_LOADING";
export const SET_LOADING_CARD = "globalLimitTrx/SET_LOADING_CARD";
export const CLEAR_ERROR = "globalLimitTrx/CLEAR_ERROR";
export const SET_ERROR = "globalLimitTrx/SET_ERROR";
export const INIT_DATA = "globalLimitTrx/INIT_DATA";
export const INIT_DATA_CURRENCY = "globalLimitTrx/INIT_DATA_CURRENCY";
export const SET_HANDLE_CLEAR_ERROR = "globalLimitTrx/SET_HANDLE_CLEAR_ERROR";
export const GET_DATA_SERVICE = "globalLimitTrx/GET_DATA_SERVICE";
export const GET_DATA_TRANSACTION_CATEGORY =
  "globalLimitTrx/GET_DATA_TRANSACTION_CATEGORY";
export const SET_DOUBLE_SUBMIT = "globalLimitTrx/SET_DOUBLE_SUBMIT";
export const GET_DATA_CURRENCY_MATRIX =
  "globalLimitTrx/GET_DATA_CURRENCY_MATRIX";

export const GET_DATA_MATRIX = "globalLimitTrx/GET_DATA_MATRIX";

export const GET_DATA_GLOBAL_LIMIT = "globalLimitTrx/GET_DATA_GLOBAL_LIMIT";

export const SET_SUCCESS_CONFRIMATION =
  "globalLimitTrx/SET_SUCCESS_CONFRIMATION ";

export const SET_SUCCESS_CONFRIMATION_GLOBAL_LIMIT =
  "globalLimitTrx/SET_SUCCESS_CONFRIMATION_GLOBAL_LIMIT ";
export const SET_IS_ADD_SERVICE = "globalLimitTrx/SET_IS_ADD_SERVICE";
export const SET_GLOBAL_LIMIT_TEMP = "globalLimitTrx/SET_GLOBAL_LIMIT_TEMP";
export const SET_IS_ALREADY_EXIST = "globalLimitTrx/SET_IS_ALREADY_EXIST";
export const SET_LOADING_SUBMIT = "globalLimitTrx/SET_LOADING_SUBMIT";

export const DATA_CARD = "globalLimitTrx/DATA_CARD";

export const setLoadingSubmit = (payload) => ({
  type: SET_LOADING_SUBMIT,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingCard = (payload) => ({
  type: SET_LOADING_CARD,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setDataService = (payload) => ({
  type: GET_DATA_SERVICE,
  payload,
});

export const setDataTransactionCategory = (payload) => ({
  type: GET_DATA_TRANSACTION_CATEGORY,
  payload,
});

export const setDataCurrencyMatrix = (payload) => ({
  type: GET_DATA_CURRENCY_MATRIX,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setSuccessConfirmation = (payload) => ({
  type: SET_SUCCESS_CONFRIMATION,
  payload,
});

export const setSuccessConfirmationGlobalLimit = (payload) => ({
  type: SET_SUCCESS_CONFRIMATION_GLOBAL_LIMIT,
  payload,
});

export const setGlobalLimit = (payload) => ({
  type: GET_DATA_GLOBAL_LIMIT,
  payload,
});

export const setGlobalLimitTemp = (payload) => ({
  type: SET_GLOBAL_LIMIT_TEMP,
  payload,
});

export const setGetDataMatrix = (payload) => ({
  type: GET_DATA_MATRIX,
  payload,
});

export const setCurrency = (payload) => ({
  type: INIT_DATA_CURRENCY,
  payload,
});

export const setIsAdd = (payload) => ({
  type: SET_IS_ADD_SERVICE,
  payload,
});

export const setDataCard = (payload) => ({
  type: DATA_CARD,
  payload,
});
export const setServiceAlready = (payload) => ({
  type: SET_IS_ALREADY_EXIST,
  payload,
});

export const getDataService = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetServiceGlobalLimitTrx(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      const resultFilter = res?.data?.serviceList;
      dispatch(setDataService(resultFilter));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getDataServiceTransactionCategory =
  (payload) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const res = await serviceRequestGetTransactionCategoryGlobalLimitTrx(
        payload
      );

      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(
          setDataTransactionCategory(res?.data?.transactionCategoryList)
        );
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setHandleError(error?.message));
    }
  };

export const getDataCurrencyMatrix = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetTransactionCurrencyMatrixGlobalLimitTrx(
      payload
    );
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDataCurrencyMatrix(res?.data?.currencyMatrixList));
    } else {
      dispatch(handleErrorBE(res));
    }

    // console.warn("res:", res);
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const addDataGlobalLimitTrx = (payload) => async (dispatch) => {
  dispatch(
    validateTask(
      {
        menuName: validateTaskConstant.SERVICE_GLOBAL_LIMIT,
        code: null,
        id: null,
        name: payload?.nameService,
        validate:
          payload?.serviceId?.id === null ||
          payload?.serviceId?.id === undefined,
        loader: setLoading,
      },
      {
        async onContinue() {
          try {
            const res = await serviceRequestAddGlobalLimitTrx(payload);

            if (res.status === 200) {
              dispatch(setSuccessConfirmation(true));
            } else {
              dispatch(handleErrorBE(res));
            }
          } catch (error) {
            dispatch(setHandleError(error?.message));
          }
        },
        onError() {},
      },
      {
        redirect: false,
      }
    )
  );
};

export const getDataCurrencyTransactionMatrix =
  (payload) => async (dispatch) => {
    try {
      const res = await serviceRequestGetTransactionCurrencyMatrix(payload);

      if (res.status === 200) {
        dispatch(setGetDataMatrix(res?.data));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setHandleError(error?.message));
    }
  };

export const getDataGlobalLimit = (payload) => async (dispatch) => {
  try {
    dispatch(setLoadingCard(true));
    const res = await serviceRequestGetGloalLimit(payload);
    dispatch(setLoadingCard(false));

    if (res.status === 200) {
      dispatch(setGlobalLimit(res?.data?.globalLimitList));
      dispatch(setGlobalLimitTemp(res?.data?.globalLimitList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const addDataGlobalLimitDetail = (payload) => async (dispatch) => {
  dispatch(setLoadingSubmit(true));
  try {
    const res = await serviceRequestAddGloalLimit(payload);
    dispatch(setLoadingSubmit(false));
    if (res.status === 200) {
      dispatch(setSuccessConfirmationGlobalLimit(true));
    } else {
      dispatch(handleErrorBE(res, false));
    }
  } catch (error) {
    dispatch(setLoadingSubmit(false));
    dispatch(setHandleError(error?.message));
  }
};

export const getCurrency = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestGetCurrency(payload);

    if (res.status === 200) {
      dispatch(setCurrency(res?.data?.currencyList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};
export const getValidationService = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestValidationLimitManagementService(payload);

    if (res.status === 200) {
      dispatch(setServiceAlready(!res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};
