import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { handleExceptionError } from "utils/helpers";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

const {
  serviceRequestBankDomestic,
  serviceRequestBankDomesticCreate,
  serviceRequestBankDomesticChangeStatus,
  serviceRequestBankDomesticDelete,
  requestDownloadCsvDomesticBank,
  serviceRequestBankDomesticCreateValidation,
} = require("utils/api");

export const SET_BANK_DOMESTIC_TYPE = "dashboard/SET_BANK_DOMESTIC_TYPE";
export const SET_BANK_DOMESTIC_DETAIL_TYPE =
  "dashboard/SET_BANK_DOMESTIC_DETAIL_TYPE";
export const SET_BANK_DOMESTIC_ERROR_TYPE =
  "dashboard/SET_BANK_DOMESTIC_ERROR_TYPE";
export const SET_LOADING = "dashboard/SET_BANK_DOMESTIC_LOADING";
export const SET_ERROR = "dashboard/SET_BANK_DOMESTIC_SET_ERROR";
export const CLEAR_ERROR = "dashboard/SET_BANK_DOMESTIC_CLEAR_ERROR";
export const SET_SUCCESS = "dashboard/SET_BANK_DOMESTIC_SET_SUCCESS";
export const SET_DATA_DETAIL = "dashboard/SET_BANK_DOMESTIC_SET_DATA_DETAIL";
export const SET_VALIDATE_CREATE = "dashboard/SET_VALIDATE_CREATE";
export const SET_LOADING_EXCUTE = "dashboard/SET_BANK_DOMESTIC_LOADING_EXCUTE";

export const setTypeBankDomestic = (payload) => ({
  type: SET_BANK_DOMESTIC_TYPE,
  payload,
});

export const setTypeBankDomesticDetail = (payload) => ({
  type: SET_BANK_DOMESTIC_DETAIL_TYPE,
  payload,
});

export const setTypeLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeClearError = () => ({
  type: CLEAR_ERROR,
});

export const setTypeDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setTypeSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});
export const setValidate = (payload) => ({
  type: SET_VALIDATE_CREATE,
  payload,
});

export const setTypeIsLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const getValidateCreate = (payload) => async (dispatch) => {
  try {
    dispatch(setTypeLoading(true));
    const res = await serviceRequestBankDomesticCreateValidation({
      ...payload,
      isInternational: false,
    });
    dispatch(setTypeLoading(false));

    if (res.status === 200) {
      dispatch(setValidate([]));
    } else {
      dispatch(setValidate(res.data?.message.split(" ")));
    }
  } catch (err) {
    dispatch(setTypeError(err.message));
  }
};
export const getDataBankDomestic = (payload) => async (dispatch) => {
  try {
    dispatch(setTypeLoading(true));
    const res = await serviceRequestBankDomestic(payload);
    dispatch(setTypeLoading(false));

    if (res.status === 200) {
      dispatch(setTypeBankDomestic(res.data.bankListPage));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err.message));
  }
};

export const addDataBankDomestic =
  (payload, codeValidateTask, handleNext) => async (dispatch, getState) => {
    const { dataDetail } = getState().domesticBank;
    if (dataDetail !== null) {
      try {
        dispatch(setTypeIsLoadingExcute(true));
        const res = await serviceRequestBankDomesticCreate(payload);
        dispatch(setTypeIsLoadingExcute(false));

        if (res.status === 200) {
          dispatch(setTypeSuccess(true));
        } else {
          dispatch(handleErrorBE(res));
        }
      } catch (err) {
        dispatch(setTypeError(err.message));
      }
    } else {
      dispatch(
        validateTask(
          {
            menuName: validateTaskConstant.BANK_DOMESTIC,
            code: codeValidateTask,
            id: null,
            name: payload?.bankAlias,
            validate: payload?.id === null || payload?.id === undefined, // karena udah divalidasi pas klik button edit
            loader: setTypeLoading,
          },
          {
            async onContinue() {
              try {
                dispatch(setTypeIsLoadingExcute(true));
                const res = await serviceRequestBankDomesticCreate(payload);
                dispatch(setTypeIsLoadingExcute(false));

                if (res.status === 200) {
                  dispatch(setTypeSuccess(true));
                } else {
                  dispatch(handleErrorBE(res));
                }
              } catch (err) {
                dispatch(setTypeError(err.message));
              }
            },
            onError() {
              // handle callback error
              // ! popups callback error sudah dihandle !
              // ex. clear form, redirect, etc
              handleNext();
            },
          }
        )
      );
    }
  };

export const changeStatusDataBankDomesticDetail =
  (payload) => async (dispatch) => {
    try {
      dispatch(setTypeIsLoadingExcute(true));
      const res = await serviceRequestBankDomesticChangeStatus(payload);
      dispatch(setTypeIsLoadingExcute(false));

      if (res.status === 200) {
        dispatch(setTypeSuccess(true));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeError(err.message));
    }
  };

export const deleteDataBankDomesticDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setTypeIsLoadingExcute(true));
    const res = await serviceRequestBankDomesticDelete(payload);
    dispatch(setTypeIsLoadingExcute(false));

    if (res.status === 200) {
      dispatch(setTypeSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err.message));
  }
};

export const dowloadCsvBankDomestic =
  (payload, fileName = "Bank-Domestic") =>
  async (dispatch) => {
    try {
      await dispatch(checkOnline());
      // dispatch(setLoadingUnduh(true));
      const response = await requestDownloadCsvDomesticBank(payload);
      const { status, data } = response;
      let today = new Date();
      const dd = String(today.getDate()).padStart(2, "0");
      const mm = String(today.getMonth() + 1).padStart(2, "0"); // January is 0!
      const yyyy = today.getFullYear();

      today = `${mm}-${dd}-${yyyy}`;
      // document.write(today);

      // dispatch(setLoadingUnduh(false));

      const d = new Date();
      if (status === 200) {
        const getFileName = `${fileName}-${today}-${d?.getMilliseconds()}.csv`;
        const url = window.URL.createObjectURL(data);
        const link = document.createElement("a");
        link.href = url;

        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(status)) {
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          let data = response.data;
          reader.onload = () => {
            data = JSON.parse(reader.result);
            resolve(Promise.reject(data));
          };
          reader.onerror = () => {
            reject(data);
          };
          reader.readAsText(data);
        })
          .then((err) => {
            dispatch(
              setTypeError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          })
          .catch((err) => {
            dispatch(
              setTypeError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          });
        // dispatch(setLoadingUnduh(false));
      }
    } catch {
      // dispatch(setLoadingUnduh(false));
    }
  };
