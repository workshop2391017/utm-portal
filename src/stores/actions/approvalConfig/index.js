// login Actions
// --------------------------------------------------------

import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceApprovalConfig,
  ServiceApprovalConfigDetail,
  ServiceConfigApprove,
} from "utils/api";
import { firstUpperCase, getLocalStorage } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "approvalconfig/SET_LOADING";
export const SET_ALL_CONFIG_DATA = "approvalconfig/SET_ALL_CONFIG_DATA";
export const SET_APPROVAL_DETAIL = "approvalconfig/SET_APPROVAL_DETAIL";
export const SET_CONIFG_ID = "approvalconfig/SET_CONIFG_ID";
export const SET_ERROR_APPROVE = "approvalconfig/SET_ERROR_APPROVE";
export const SET_CLEAR_ERROR = "pprovalconfig/SET_CLEAR_ERROR";
export const SET_LOADING_APPROVE = "approvalconfig/SET_LOADING_APPROVE";
export const SET_HANDLE_ERROR = "approvalconfig/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "approvalconfig/SET_HANDLE_CLEAR_ERROR";

// approval action types
export const TOLAK_PENGGUNA = 2;
export const SETUJUI_PENGGUNA = 1;

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setAllApprovalData = (payload) => ({
  type: SET_ALL_CONFIG_DATA,
  payload,
});

export const setApprovalDetail = (payload) => ({
  type: SET_APPROVAL_DETAIL,
  payload,
});

export const setConfigId = (payload) => ({
  type: SET_CONIFG_ID,
  payload,
});

export const setErrorApprove = (payload) => ({
  type: SET_ERROR_APPROVE,
  payload,
});

export const setClearError = () => ({
  type: SET_CLEAR_ERROR,
});

export const setLoadingApprove = (payload) => ({
  type: SET_LOADING_APPROVE,
  payload,
});

export const handleConfigData = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceApprovalConfig(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setAllApprovalData(response.data));
    } else {
      dispatch(handleErrorBE(response));
      dispatch(setAllApprovalData([]));
      dispatch(setLoading(false));
    }
  } catch (e) {
    dispatch(setAllApprovalData([]));
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};

// const dynamicConfigData = (data, key, forDefaultKey) => {
//   // checking if data is array or object
//   const dataMapping = Array.isArray(data[key]) ? data[key][0] : data[key];

//   // set default key if data is empty
//   const defaultMapping = forDefaultKey
//     ? Array.isArray(forDefaultKey[key])
//       ? forDefaultKey[key][0]
//       : forDefaultKey[key]
//     : null;

//   // mapping dynamic key value

//   return dataMapping && !forDefaultKey
//     ? Object.keys(dataMapping).map((item) => ({
//         value: dataMapping[item] || "-",
//         label: firstUpperCase(item)
//           .match(/[A-Z][a-z]+/g)
//           .join(" "),
//       }))
//     : Object.keys(defaultMapping).map((item) => ({
//         value: "-",
//         label: firstUpperCase(item)
//           .match(/[A-Z][a-z]+/g)
//           .join(" "),
//       }));
// };

// const handleDefinedConfigData = (data) => {
//   const key = Object.keys(data[0].newData);
//   const newData = data[0].newData;
//   const oldData = data[0]?.oldData ?? {};

//   switch (key[0]) {
//     case "bankList":
//       return {
//         ...data[0],
//         newData: dynamicConfigData(newData, "bankList"),
//         oldData: dynamicConfigData(oldData, "bankList", newData),
//       };
//     case "role":
//       return {
//         ...data[0],
//         newData: dynamicConfigData(newData, "role"),
//         oldData: dynamicConfigData(oldData, "role", newData),
//       };
//     case "card":
//       return {
//         ...data[0],
//         newData: dynamicConfigData(newData, "card"),
//         oldData: dynamicConfigData(oldData, "card", newData),
//       };
//     case "newBankList":
//       return {
//         ...data[0],
//         newData: dynamicConfigData(newData, "newBankList"),
//         oldData: dynamicConfigData(oldData, "newBankList", newData),
//       };
//     case "systemParameterList":
//       return {
//         ...data[0],
//         newData: dynamicConfigData(newData, "systemParameterList"),
//         oldData: dynamicConfigData(oldData, "systemParameterList"),
//       };
//     default:
//       return {};
//   }
// };

export const handleApprovalConfigDetail = (payload) => async (dispatch) => {
  const requestPayload = {
    approvalIds: payload,
  };

  try {
    dispatch(setLoading(true));
    const response = await ServiceApprovalConfigDetail(requestPayload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      const data = response.data.map((item) => ({
        ...item,
        newData: item.newData && JSON.parse(item.newData),
        oldData: item.oldData && JSON.parse(item.oldData),
      }));

      dispatch(setApprovalDetail(data[0]));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandleError(e.message));
  }
};

export const submitApprovalConfig = (payload) => async (dispatch) => {
  const { checkList, data, status, comment } = payload;

  const filterChecklist = (data ?? [])
    .map((elm) => {
      if (checkList.includes(elm.id)) {
        return {
          approvalComment: comment || null,
          approvalStatus: payload.status,
          idApproval: elm?.id,
          typeApproval: "GENERAL_PARAMETER",
        };
      }
      return {};
    })
    .filter((elm) => elm.idApproval);

  const requestPayload = {
    approvalList: filterChecklist,
  };

  try {
    dispatch(setLoadingApprove(true));
    const response = await ServiceConfigApprove(
      requestPayload,
      privilegeCONFIG.APPROVAL_CONFIG.EXECUTE
    );
    dispatch(setLoadingApprove(false));

    const error = response.data.errorCode;

    if (response.status === 200) {
      return response;
    }

    if (error === "0052") {
      dispatch(
        setErrorApprove({
          isError: true,
          message: response.data.message,
        })
      );

      return false;
    }
    dispatch(handleErrorBE(response));
  } catch (e) {
    dispatch(setHandleError(e.message));
  }
};
