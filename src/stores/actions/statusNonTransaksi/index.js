import {
  serviceGetNonTransactionStatus,
  serviceGetNonTransactionStatusDetail,
  serviceGetNonTransactionStatusTaskInformation,
} from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import moment from "moment";
import { handleExceptionError } from "utils/helpers";

export const SET_LOADING = "statusNonTransaksi/SET_LOADING";
export const CLEAR_ERROR = "statusNonTransaksi/CLEAR_ERROR";
export const SET_ERROR = "statusNonTransaksi/SET_ERROR";
export const INIT_DATA = "statusNonTransaksi/INIT_DATA";
export const SET_PAGES = "statusNonTransaksi/SET_PAGES";
export const SET_DATA_NON_TRANSAKSI =
  "statusNonTransaksi/SET_DATA_NON_TRANSAKSI";
export const SET_DATA_NON_TRANSAKSI_DETAIL =
  "statusNonTransaksi/SET_DATA_NON_TRANSAKSI_DETAIL";
export const SET_TASK_INFORMATION = "statusNonTransaksi/SET_TASK_INFORMATION";

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});
export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDataNonTransaksi = (payload) => ({
  type: SET_DATA_NON_TRANSAKSI,
  payload,
});

export const setDataNonTransaksiDetail = (payload) => ({
  type: SET_DATA_NON_TRANSAKSI_DETAIL,
  payload,
});

export const setTaskInformation = (payload) => ({
  type: SET_TASK_INFORMATION,
  payload,
});
export const setStepPages = (payload) => ({
  type: SET_PAGES,
  payload,
});

export const getDataNonTransaksi = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetNonTransactionStatus(payload);
    if (response.status === 200) {
      dispatch(setDataNonTransaksi(response.data));
      dispatch(setLoading(false));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};

export const getDataNonTransaksiDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetNonTransactionStatusDetail(payload);
    if (response.status === 200) {
      dispatch(setDataNonTransaksiDetail(response.data));
      dispatch(setLoading(false));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};

export const getDataNonTransaksiDetailTaskInfo =
  (payload) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await serviceGetNonTransactionStatusTaskInformation(
        payload
      );
      if (response.status === 200) {
        dispatch(setTaskInformation(response.data));
        dispatch(setLoading(false));
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setHandlingError(e?.data?.message));
    } finally {
      setLoading(false);
    }
  };
