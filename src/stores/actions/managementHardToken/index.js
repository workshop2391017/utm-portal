import {
  serviceBindToken,
  serviceCorporateList,
  serviceCorporateTokenList,
  serviceCorporateUserList,
  serviceUnbindToken,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
/* eslint-disable space-before-function-paren */
export const SET_LOADING = "managementHardToken/SET_LOADING";
export const SET_LOADING_SUBMIT = "managementHardToken/SET_LOADING_SUBMIT";
export const SET_HANDLE_ERROR = "managementHardToken/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR =
  "managementHardToken/SET_HANDLE_CLEAR_ERROR";
export const SET_CORPORATE_LIST = "managementHardToken/SET_CORPORATE_LIST";
export const SET_CORPORATE_TOKEN_LIST =
  "managementHardToken/SET_CORPORATE_TOKEN_LIST";
export const SET_CORPORATE_USER_LIST =
  "managementHardToken/SET_CORPORATE_USER_LIST";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingSubmit = (payload) => ({
  type: SET_LOADING_SUBMIT,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setCorporateList = (payload) => ({
  type: SET_CORPORATE_LIST,
  payload,
});

export const setCorporateTokenList = (payload) => ({
  type: SET_CORPORATE_TOKEN_LIST,
  payload,
});

export const setCorporateUserList = (payload) => ({
  type: SET_CORPORATE_USER_LIST,
  payload,
});

export const getCorporateList = (payloadRequest) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoading(true));
    const response = await serviceCorporateList(payloadRequest);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setCorporateList(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};

export const getCorporateTokenList = (payloadRequest) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoading(true));
    const response = await serviceCorporateTokenList(payloadRequest);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setCorporateTokenList(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};

export const getCorporateUserList = (payloadRequest) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoading(true));
    const response = await serviceCorporateUserList(payloadRequest);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setCorporateUserList(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(setCorporateUserList([]));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};

export const bindToken = (payloadRequest, onSuccess) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoadingSubmit(true));
    const response = await serviceBindToken(payloadRequest);
    dispatch(setLoadingSubmit(false));

    if (response.status === 200) {
      onSuccess && onSuccess();
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoadingSubmit(false));
    dispatch(setHandleError(e.message));
  }
};

export const unBindToken = (payloadRequest, onSuccess) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoadingSubmit(true));
    const response = await serviceUnbindToken(payloadRequest);
    dispatch(setLoadingSubmit(false));

    if (response.status === 200) {
      onSuccess && onSuccess();
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoadingSubmit(false));
    dispatch(setHandleError(e.message));
  }
};
