// approvelMatrixNonFinantial Actions
// --------------------------------------------------------

import {
  serviceGetMenuGlobal,
  serviceGetApprovalMatrix,
  serviceGetAllService,
  serviceGetWorkflowGroup,
  serviceDeleteNonFinancialMatrixApproval,
  serviceAddNonFinancialMatrixApproval,
} from "utils/api";
import { handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING_SIDEBAR =
  "approvelMatrixNonFinantial/SET_LOADING_SIDEBAR";
export const SET_LOADING_CARD = "approvelMatrixNonFinantial/SET_LOADING_CARD";
export const SET_LOADING_ALL_SERVICE =
  "approvelMatrixNonFinantial/SET_LOADING_ALL_SERVICE";
export const CLEAR_ERROR = "approvelMatrixNonFinantial/CLEAR_ERROR";
export const SET_ERROR = "approvelMatrixNonFinantial/SET_ERROR";
export const INIT_DATA = "approvelMatrixNonFinantial/INIT_DATA";
export const SET_MENU_SIDEBAR = "approvelMatrixNonFinantial/SET_MENU_SIDEBAR";
export const SET_DOUBLE_SUBMIT = "approvelMatrixNonFinantial/SET_DOUBLE_SUBMIT";
export const SET_DATA_CARD = "approvelMatrixNonFinantial/SET_DATA_CARD";
export const SET_DATA_DROPDOWN = "approvelMatrixNonFinantial/SET_DATA_DROPDOWN";
export const SET_SUCCESS = "approvelMatrixNonFinantial/SET_SUCCESS";
export const SET_MENU_SELECTED = "approvelMatrixNonFinantial/SET_MENU_SELECTED";
export const SET_ALL_SERVICE = "approvelMatrixNonFinantial/SET_ALL_SERVICE";
export const SET_IDCOMPANY = "approvelMatrixNonFinantial/SET_IDCOMPANY";
export const SET_IS_CHECK_SERVICE =
  "approvelMatrixNonFinantial/SET_IS_CHECK_SERVICE";

export const setLoadingSideBar = (payload) => ({
  type: SET_LOADING_SIDEBAR,
  payload,
});

export const setLoadingCard = (payload) => ({
  type: SET_LOADING_CARD,
  payload,
});

export const setLoadingAllService = (payload) => ({
  type: SET_LOADING_ALL_SERVICE,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeSideBar = (payload) => ({
  type: SET_MENU_SIDEBAR,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeDataCard = (payload) => ({
  type: SET_DATA_CARD,
  payload,
});

export const setDropDown = (payload) => ({
  type: SET_DATA_DROPDOWN,
  payload,
});

export const setTypeSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});
export const setDataMenuSelected = (payload) => ({
  type: SET_MENU_SELECTED,
  payload,
});
export const setAllService = (payload) => ({
  type: SET_ALL_SERVICE,
  payload,
});
export const setDataServiceIsCheck = (payload) => ({
  type: SET_IS_CHECK_SERVICE,
  payload,
});

export const setIdCompany = (payload) => ({
  type: SET_IDCOMPANY,
  payload,
});

export const nonFinancialAllService =
  (payload) => async (dispatch, getState) => {
    try {
      dispatch(setLoadingAllService(true));
      const { idPerusahaan } = getState().managmentCompany;
      const res = await serviceGetAllService({
        ...payload,
        corporateProfileId: idPerusahaan || payload.corporateProfileId,
      });

      if (res.status === 200) {
        dispatch(setLoadingAllService(false));
        dispatch(
          setAllService(res?.data?.workflowApprovalMatrixAllServiceDtos)
        );
      } else {
        dispatch(setLoadingAllService(false));
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setLoadingAllService(false));
      console.error("ERR ", error);
    }
  };

export const nonFinancialMenuGlobal =
  (payload) => async (dispatch, getState) => {
    try {
      dispatch(setLoadingSideBar(true));
      const { idPerusahaan } = getState().managmentCompany;
      const res = await serviceGetMenuGlobal({
        ...payload,
        corporateProfileId: idPerusahaan || payload.corporateProfileId,
      });

      if (res.status === 200) {
        dispatch(setLoadingSideBar(false));
        dispatch(setTypeSideBar(res?.data?.menuAccessList));
      } else {
        dispatch(setLoadingSideBar(false));
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setLoadingSideBar(false));
      console.error("ERR ", error);
    }
  };

export const nonFinancialMenuGlobalDetails =
  (payload) => async (dispatch, getState) => {
    try {
      dispatch(setLoadingCard(true));
      const { idPerusahaan } = getState().managmentCompany;
      const res = await serviceGetApprovalMatrix({
        ...payload,
        corporateProfileId: idPerusahaan || payload.corporateProfileId,
      });

      if (res.status === 200) {
        let data = res?.data?.getApprovalMatrixByMenuResponse?.approvalMatrix;

        data = data?.length ? data[0] : null;

        dispatch(setLoadingCard(false));
        dispatch(setTypeDataCard(data));
      } else {
        dispatch(setLoadingCard(false));
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setLoadingCard(false));
      console.error("ERR ", error);
    }
  };

export const nonFinancialTargetGroupIndex = (payload) => async (dispatch) => {
  try {
    const res = await serviceGetWorkflowGroup(payload);

    if (res.status === 200) {
      dispatch(setDropDown(res?.data?.getGroupDtoList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};

export const nonFinancialAdd = (payload) => async (dispatch) => {
  try {
    const res = await serviceAddNonFinancialMatrixApproval(payload);

    if (res.status === 200) {
      dispatch(setTypeSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};

export const nonFinancialDelete = (payload) => async (dispatch) => {
  try {
    const res = await serviceDeleteNonFinancialMatrixApproval(payload);

    if (res.status === 200) {
      dispatch(setTypeSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};
