// companyLimit Actions
// --------------------------------------------------------
import { rest } from "lodash";
import {
  handleErrorBE,
  checkOnline,
  setErrorAlreadyUsed,
} from "stores/actions/errorGeneral";

const {
  serviceRequestGetCompanyLimit,
  serviceRequestGetCompanyLimitDetail,
  serviceRequestAddCompanyLimit,
  serviceRequestAllCurrencyCompanyLimit,
  serviceRequestGetValidationCompanyLimit,
  hanldeValidationDataLms,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "companyLimit/SET_LOADING";
export const SET_LOADING_EXCUTE = "companyLimit/SET_LOADING_EXCUTE";
export const CLEAR_ERROR = "companyLimit/CLEAR_ERROR";
export const SET_ERROR = "companyLimit/SET_ERROR";
export const INIT_DATA = "companyLimit/INIT_DATA";
export const SET_DATA = "companyLimit/SET_DATA";
export const SET_POP_UP = "companyLimit/SET_POP_UP";
export const SET_DATA_DETAIL = "companyLimit/SET_DATA_DETAIL";
export const SET_DOUBLE_SUBMIT = "companyLimit/SET_DOUBLE_SUBMIT";
export const SET_CURRENCY = "companyLimit/SET_CURRENCY";
export const SET_PAYLOAD_DETAIL = "companyLimit/SET_PAYLOAD_DETAIL";
export const SET_VALIDATION_LIMIT = "companyLimit/SET_VALIDATION_LIMIT";
export const SET_CLEAR_VALIDATION_LIMIT =
  "companyLimit/SET_CLEAR_VALIDATION_LIMIT";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const setDetil = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setPopUp = (payload) => ({
  type: SET_POP_UP,
  payload,
});

export const setTypeCurrency = (payload) => ({
  type: SET_CURRENCY,
  payload,
});

export const setTypePayloadDetail = (payload) => ({
  type: SET_PAYLOAD_DETAIL,
  payload,
});
export const setValidationLimit = (payload) => ({
  type: SET_VALIDATION_LIMIT,
  payload,
});
export const setClearValidationLimit = () => ({
  type: SET_CLEAR_VALIDATION_LIMIT,
});

// export const setEditData = (payload) => ({
//   type: SET_DATA_EDIT,
//   payload,
// });
// export const clearDataDetail = () => ({
//   type: CLEAR_DATA_DETAIL,
// });

export const getValidationLimit = (payload) => async (dispatch) => {
  const { id, data, ...rest } = payload;
  const obj = {};

  if (rest?.numberOfTransaction === 0) {
    obj[`numberOfTransaction${id}`] = false;
  }

  if (rest?.maxTransaction === 0) {
    obj[`maxTransaction${id}`] = false;
  }
  dispatch(setValidationLimit(obj));

  try {
    const res = await serviceRequestGetValidationCompanyLimit(rest);

    if (res.status === 200) {
      if (rest?.numberOfTransaction && id) {
        obj[`numberOfTransaction${id}`] = !res?.data;
      }
      if (rest?.maxTransaction && id) {
        obj[`maxTransaction${id}`] = !res?.data;
      }
      dispatch(setValidationLimit(obj));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};
export const getDataCompanyLimit = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestGetCompanyLimit(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setTypeData(res?.data));
    } else {
      const emptyFunction = () => dispatch(setTypeData([]));
      dispatch(handleErrorBE(res, null, null, emptyFunction));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};

export const getAllCurrencyCompanyLimit = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestAllCurrencyCompanyLimit(payload);

    if (res.status === 200) {
      dispatch(setTypeCurrency(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};

export const addDataCompanyLimit = (payload) => async (dispatch) => {
  try {
    dispatch(setLoadingExcute(true));

    const res = await serviceRequestAddCompanyLimit(payload);
    dispatch(setLoadingExcute(false));

    if (res.status === 200) {
      dispatch(setPopUp(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};

export const getDataCompanyLimitDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestGetCompanyLimitDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(
        setDetil({
          ...res?.data,
          companyLimitDetail: res.data.companyLimitDetail.filter(
            (e) => e?.serviceName?.toLowerCase() !== "annual"
          ),
        })
      );
    } else {
      dispatch(handleErrorBE(res));
    }

    console.warn("res:", res);
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};

export const handleValidateEditDataCompanyLimit =
  (payload, callback) => async (dispatch) => {
    try {
      const response = await hanldeValidationDataLms(payload);

      if (response.status === 200) {
        if (response.data) callback.onContinue();
        if (!response.data)
          dispatch(
            setErrorAlreadyUsed({
              isError: true,
              message: "Data is being used",
            })
          );
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      console.error("ERR ", err);
    }
  };
