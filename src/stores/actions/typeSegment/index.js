// typeSegment Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "typeSegment/SET_LOADING";
export const CLEAR_ERROR = "typeSegment/CLEAR_ERROR";
export const SET_ERROR = "typeSegment/SET_ERROR";
export const INIT_DATA = "typeSegment/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "typeSegment/SET_DOUBLE_SUBMIT";

const {
  serviceRequestGetTearingSlab,
  serviceRequestGetTearingSlabDetail,
  serviceRequestAddTearingSlab,
} = require("utils/api");

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});
