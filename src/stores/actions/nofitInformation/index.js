// notifInformation Actions
// --------------------------------------------------------
import ErrorList from "antd/lib/form/ErrorList";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

import {
  serviceNotifInformation,
  serviceNotifInformationDetail,
} from "utils/api";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "notifInformation/SET_LOADING";
export const SET_LOADING_EXCUTE = "notifInformation/SET_LOADING_EXCUTE";
export const CLEAR_ERROR = "notifInformation/CLEAR_ERROR";
export const SET_ERROR = "notifInformation/SET_ERROR";
export const INIT_DATA = "notifInformation/INIT_DATA";
export const SET_DATA = "notifInformation/SET_DATA";
export const SET_DETAIL = "notifInformation/SET_DETAIL";
export const SET_NOTIFID = "notifInformation/SET_NOTIFID";

export const setNotifId = (payload) => ({
  type: SET_NOTIFID,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const handleGetNotifInformation =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceNotifInformation(payload);
      dispatch(setLoading(false));
      if (response.status === 200) {
        dispatch(setData(response?.data));
      } else {
        dispatch(setData([]));
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      console.error("ERR ", err);
      dispatch(setLoading(false));
    }
  };

export const handleGetNotifInformationDetail =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceNotifInformationDetail(payload);
      dispatch(setLoading(false));
      if (response.status === 200) {
        dispatch(setDetail(response.data));
      } else {
        dispatch(setData([]));
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      console.error("ERR ", err);
      dispatch(setLoading(false));
    }
  };
