// login Actions
// --------------------------------------------------------
import { privilegeCONFIG } from "configuration/privilege";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import {
  serviceApprovalConfig,
  ServiceApprovalConfigDetail,
  ServiceConfigApprove,
} from "utils/api";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "userapproval/SET_LOADING";
export const SET_ALL_APPROVAL_DATA = "userapproval/SET_ALL_APPROVAL_DATA";
export const SET_APPROVAL_DETAIL = "userapproval/SET_APPROVAL_DETAIL";
export const SET_USERS_ID = "userapproval/SET_USERS_ID";
export const SET_LOADING_APPROVE = "userapproval/SET_LOADING_APPROVE";
export const SET_ERROR = "userapproval/SET_ERROR";
export const SET_ERROR_APPROVE = "userapproval/SET_ERROR_APPROVE";
export const SET_CLEAR_ERROR = "userapproval/SET_CLEAR_ERROR";
export const SET_HANDLE_ERROR = "userapproval/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "userapproval/SET_HANDLE_CLEAR_ERROR";

// approval action types
export const TOLAK_PENGGUNA = 2;
export const SETUJUI_PENGGUNA = 1;

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setAllApprovalData = (payload) => ({
  type: SET_ALL_APPROVAL_DATA,
  payload,
});

export const setApprovalDetail = (payload) => ({
  type: SET_APPROVAL_DETAIL,
  payload,
});
export const setUsersId = (payload) => ({
  type: SET_USERS_ID,
  payload,
});
export const setLoadingApprove = (payload) => ({
  type: SET_LOADING_APPROVE,
  payload,
});
export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});
export const setErrorApprove = (payload) => ({
  type: SET_ERROR_APPROVE,
  payload,
});
export const setClearError = () => ({
  type: SET_CLEAR_ERROR,
});

export const handleUserApproval = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceApprovalConfig(
      payload,
      privilegeCONFIG.APPROVAL_USER.LIST
    );

    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setAllApprovalData(response.data));
    } else {
      dispatch(handleErrorBE(response));
      dispatch(setAllApprovalData([]));
      dispatch(setLoading(false));
    }
  } catch (e) {
    dispatch(setHandleError(e.message));
    dispatch(setAllApprovalData([]));
    dispatch(setLoading(false));
  }
};

export const handleApprovalDetail = (payload) => async (dispatch) => {
  const requestPayload = {
    approvalIds: payload,
  };
  try {
    dispatch(setLoading(true));
    const response = await ServiceApprovalConfigDetail(
      requestPayload,
      privilegeCONFIG.APPROVAL_USER.LIST
    );
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setApprovalDetail(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandleError(e.message));
  }
};

export const submitApprovalUser = (payload) => async (dispatch) => {
  const payloadUserApproval = {
    approvalList: [payload],
  };

  try {
    dispatch(setLoadingApprove(true));
    const response = await ServiceConfigApprove(
      payloadUserApproval,
      privilegeCONFIG.APPROVAL_USER.LIST
    );
    dispatch(setLoadingApprove(false));
    if (response.status === 200) {
      return response;
    }
    dispatch(handleErrorBE(response));
    return false;
  } catch (e) {
    dispatch(setHandleError(e.message));
  }
};

export const submitMultipleApprovalUser = (payload) => async (dispatch) => {
  const { checkList, data, status, comment } = payload;

  const filterChecklist = (data ?? [])
    .map((elm) => {
      if (checkList.includes(elm.id)) {
        return {
          approvalComment: comment || null,
          approvalStatus: status,
          idApproval: elm?.id,
          typeApproval: "PORTAL_USER",
        };
      }
      return {};
    })
    .filter((elm) => elm.idApproval);

  const requestPayload = {
    approvalList: filterChecklist,
  };

  try {
    dispatch(setLoadingApprove(true));
    const response = await ServiceConfigApprove(
      requestPayload,
      privilegeCONFIG.APPROVAL_USER.LIST
    );
    dispatch(setLoadingApprove(false));
    if (response.status === 200) {
      return true;
    }

    const error = response.data.errorCode;
    if (error === "0052") {
      dispatch(
        setErrorApprove({
          isError: true,
          message: response.data.message,
        })
      );
    } else dispatch(handleErrorBE(response));
  } catch (e) {
    dispatch(setHandleError(e.message));
  }
};
