// progressBarAction Actions
// --------------------------------------------------------

import { serviceMerchantList, serviceSyncMerchantList } from "utils/api";
import { checkOnline, handleErrorBE } from "../errorGeneral";

export const SET_LOADING = "merchant/SET_LOADING";
export const INIT_DATA = "merchant/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "merchant/SET_DOUBLE_SUBMIT";
export const SET_LOADING_SYNC = "merchant/SET_DOUBLE_SUBMIT";

export const setLoadingSync = (payload) => ({
  type: SET_LOADING_SYNC,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const getMerchantList = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoading(true));

  try {
    const response = await serviceMerchantList(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setInitData(response.data));
    } else {
      dispatch(setInitData());
      if (response.status !== 500) {
        dispatch(handleErrorBE(response));
      }
    }
  } catch (e) {
    dispatch(setInitData());
    console.error(e);
  }
};

export const syncMerchantList = () => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingSync(true));

  try {
    const response = await serviceSyncMerchantList({
      merchantId: "",
    });
    dispatch(setLoadingSync(false));

    if (response.status === 200) {
      dispatch(setLoadingSync(false));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    console.error(e);
  }
};
