// approvalPersetujuanMatrix Actions
// --------------------------------------------------------

import {
  serviceApprovalWorkflowDetail,
  serviceApprovalWorkflowExecute,
  serviceApprovalMatrixAllMenuType,
  servicePersetujuanApprovalMatrix,
  serviceApprovalWorkflowBulkExecute,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "approvalPersetujuanMatrix/SET_LOADING";
export const SET_CLEAR = "approvalPersetujuanMatrix/SET_CLEAR";
export const SET_ERROR = "approvalPersetujuanMatrix/SET_ERROR";
export const INIT_DATA = "approvalPersetujuanMatrix/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "approvalPersetujuanMatrix/SET_DOUBLE_SUBMIT";
export const SET_INITIAL_DATA = "approvalPersetujuanMatrix/SET_INITIAL_DATA";
export const SET_DETAIL_DATA = "approvalPersetujuanMatrix/SET_DETAIL_DATA";
export const SET_LOADING_EXECUTE =
  "approvalPersetujuanMatrix/SET_LOADING_EXECUTE";
export const SET_SUCCESS_REJECT =
  "approvalPersetujuanMatrix/SET_SUCCESS_REJECT";
export const SET_SUCCESS_APPROVE =
  "approvalPersetujuanMatrix/SET_SUCCESS_APPROVE";
export const SET_ALL_MENU = "approvalPersetujuanMatrix/SET_ALL_MENU";
export const SET_LOADING_ALL_MENU =
  "approvalPersetujuanMatrix/SET_LOADING_ALL_MENU";
export const SET_DATA_REFF = "approvalPersetujuanMatrix/SET_DATA_REFF";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitialData = (payload) => ({
  type: SET_INITIAL_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setDetailData = (payload) => ({
  type: SET_DETAIL_DATA,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setSuccessReject = (payload) => ({
  type: SET_SUCCESS_REJECT,
  payload,
});

export const setSuccessApprove = (payload) => ({
  type: SET_SUCCESS_APPROVE,
  payload,
});

export const setClear = (payload) => ({
  type: SET_CLEAR,
  payload,
});

export const setAllMenu = (payload) => ({
  type: SET_ALL_MENU,
  payload,
});

export const setLoadingAllMenu = (payload) => ({
  type: SET_LOADING_ALL_MENU,
  payload,
});

export const setTypeBulkReffNumber = (payload) => ({
  type: SET_DATA_REFF,
  payload,
});

export const handleGetPersetujuanApprovalMatrix =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await servicePersetujuanApprovalMatrix(payload);
      if (response.status === 200) {
        dispatch(setInitialData(response.data));
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setInitialData([]));
        dispatch(setLoading(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setInitialData([]));
      dispatch(setLoading(false));
      dispatch(setError(e?.message));
    }
  };

export const handleGetPersetujuanApprovalMatrixDetail =
  (payload) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceApprovalWorkflowDetail(payload);
      if (response.status === 200) {
        dispatch(
          setDetailData({
            ...response.data,
            metadata: JSON.parse(response.data?.metadata),
            additionalData: JSON.parse(response.data?.additionalData),
          })
        );
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setDetailData([]));
        dispatch(setLoading(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setDetailData([]));
      dispatch(setLoading(true));
      dispatch(setError(e?.message));
    }
  };

export const handleApprovePersetujuanApprovalMatrixExecute =
  (payload) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoadingExecute(true));
    try {
      const response = await serviceApprovalWorkflowExecute(payload);
      if (response.status === 200) {
        if (payload.action === 5) {
          dispatch(
            setSuccessApprove({
              isSuccess: true,
              releaser: response.data.isToReleaserList,
            })
          );
        }
        if (payload.action === 6) {
          dispatch(setSuccessReject(true));
        }
        dispatch(setLoadingExecute(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoadingExecute(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setLoadingExecute(true));
      dispatch(setError(e?.message));
    }
  };
export const handleApprovePersetujuanApprovalMatrixBulkExecute =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoadingExecute(true));

    const reffNoBulk = getState().approvalPersetujuanMatrix.dataReffBulk;

    const result = reffNoBulk.map((item) => ({
      workFlowMenu: payload?.workFlowMenu,
      referenceNumber: item.referenceNumber.toString(),
    }));

    const finalPayload = {
      approvalList: result,
      action: payload?.action,
      modifiedReason: payload?.modifiedReason,
    };

    try {
      const response = await serviceApprovalWorkflowBulkExecute(finalPayload);
      if (response.status === 200) {
        if (payload.action === 5) {
          dispatch(
            setSuccessApprove({
              isSuccess: true,
              releaser: response.data.isToReleaserList,
            })
          );
        }
        if (payload.action === 6) {
          dispatch(setSuccessReject(true));
        }
        dispatch(setLoadingExecute(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoadingExecute(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setLoadingExecute(true));
      dispatch(setError(e?.message));
    }
  };

export const handleApprovalMatrixAllMenu = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingAllMenu(true));

  try {
    const response = await serviceApprovalMatrixAllMenuType(dataParams);
    if (response.status === 200) {
      dispatch(
        setAllMenu(response?.data?.workflowApprovalMatrixAllServiceDtos)
      );
      dispatch(setLoadingAllMenu(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setAllMenu([]));
      dispatch(setLoadingAllMenu(false));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setAllMenu([]));
    dispatch(setLoadingAllMenu(false));
    dispatch(setError(e?.message));
  }
};
