// statusTransaksi Actions
// --------------------------------------------------------

import {
  serviceGetAllTransactionStatus,
  serviceRequestDownloadStatusTransaksi,
  serviceRequestStatusTransaksi,
  serviceRequestBulkStatusTransaksi,
  serviceRequestStatusTransaksiDetail,
  serviceGetAllTransactionCategory,
  serviceGetAllTransactionGroup,
} from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import moment from "moment";
import { handleExceptionError } from "utils/helpers";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "statusTransaksi/SET_LOADING";
export const CLEAR_ERROR = "statusTransaksi/CLEAR_ERROR";
export const SET_ERROR = "statusTransaksi/SET_ERROR";
export const INIT_DATA = "statusTransaksi/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "statusTransaksi/SET_DOUBLE_SUBMIT";
export const SET_DATA_TRANSAKSI = "statusTransaksi/SET_DATA_TRANSAKSI";
export const SET_DATA_BULK_TRANSAKSI =
  "statusTransaksi/SET_DATA_BULK_TRANSAKSI";
export const SET_DATA_DOWNLOAD_TRANSAKSI =
  "statusTransaksi/SET_DATA_DOWNLOAD_TRANSAKSI";
export const SET_USER_PROFILE_ID = "statusTransaksi/SET_USER_PROFILE_ID";
export const SET_TRANSACTION_GROUP = "statusTransaksi/SET_TRANSACTION_GROUP";
export const SET_DATA_TRANSAKSI_DETAIL =
  "statusTransaksi/SET_DATA_TRANSAKSI_DETAIL";
export const SET_PAGES = "statusTransaksi/SET_PAGES";
export const SET_ALL_TRANSACTION_STATUS =
  "statusTransaksi/GET_TRANSACTION_STATUS";
export const SET_ALL_TRANSACTION_CATEGORY =
  "statusTransaksi/SET_ALL_TRANSACTION_CATEGORY";
export const SET_ALL_TRANSACTION_GROUP =
  "statusTransaksi/SET_ALL_TRANSACTION_GROUP";

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});
export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setDataTransaksi = (payload) => ({
  type: SET_DATA_TRANSAKSI,
  payload,
});

export const setUserProfileId = (payload) => ({
  type: SET_USER_PROFILE_ID,
  payload,
});

export const setTransactionGroup = (payload) => ({
  type: SET_TRANSACTION_GROUP,
  payload,
});

export const setDataTransaksiBulk = (payload) => ({
  type: SET_DATA_BULK_TRANSAKSI,
  payload,
});

export const setDataTransaksiDetail = (payload) => ({
  type: SET_DATA_TRANSAKSI_DETAIL,
  payload,
});

export const setAllTransactionStatus = (payload) => ({
  type: SET_ALL_TRANSACTION_STATUS,
  payload,
});

export const setAllTransactionCategory = (payload) => ({
  type: SET_ALL_TRANSACTION_CATEGORY,
  payload,
});

export const setAllTransactionGroup = (payload) => ({
  type: SET_ALL_TRANSACTION_GROUP,
  payload,
});

export const setDownloadResi = (payload) => ({
  type: SET_DATA_DOWNLOAD_TRANSAKSI,
  payload,
});
export const setStepPages = (payload) => ({
  type: SET_PAGES,
  payload,
});

export const getDataStatusTransaksi = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceRequestStatusTransaksi(payload);
    if (response.status === 200) {
      dispatch(setDataTransaksi(response?.data));
      dispatch(setLoading(false));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
export const getDataBulkStatusTransaksi = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceRequestBulkStatusTransaksi(payload);
    if (response.status === 200) {
      dispatch(setDataTransaksiBulk(response.data));
      dispatch(setLoading(false));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
export const getDataStatusTransaksiDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceRequestStatusTransaksiDetail(payload);
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setDataTransaksiDetail(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
export const getTransactionStatus = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetAllTransactionStatus(payload);
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setAllTransactionStatus(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
export const getTransactionCategory = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetAllTransactionCategory(payload);
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setAllTransactionCategory(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
export const getTransactionGroup = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetAllTransactionGroup(payload);
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setAllTransactionGroup(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
// export const getDataDownloadResi = (payload) => async (dispatch) => {
//   try {
//     dispatch(setLoading(true));

//     const response = await serviceRequestDownloadStatusTransaksi(payload);
//     dispatch(setLoading(false));
//     if (response.status === 200) {
//       dispatch(setDownloadResi(response.data));
//     } else {
//       dispatch(handleErrorBE(response));
//     }
//   } catch (e) {
//     dispatch(setHandlingError(e?.data?.message));
//   }
// };
export const getDataDownloadResi =
  (payload, fileName = "Resi") =>
  async (dispatch) => {
    try {
      await dispatch(checkOnline());
      dispatch(setLoading(true));
      const response = await serviceRequestDownloadStatusTransaksi(payload);
      const { status, data } = response;
      dispatch(setLoading(false));

      const d = new Date();
      if (status === 200) {
        const getFileName = `${fileName}-${d?.getMilliseconds()}.pdf`;
        const url = window.URL.createObjectURL(data);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", getFileName);
        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(status)) {
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          let data = response.data;
          reader.onload = () => {
            data = JSON.parse(reader.result);
            resolve(Promise.reject(data));
          };
          reader.onerror = () => {
            reject(data);
          };
          reader.readAsText(data);
        })
          .then((e) => {
            dispatch(setHandlingError(e?.data?.message));
          })
          .catch((e) => {
            dispatch(setHandlingError(e?.data?.message));
          });
        dispatch(setLoading(false));
      }
    } catch {
      dispatch(setLoading(false));
    }
  };
