// menuPackage Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

const {
  serviceRequestGetMenuPackage,
  serviceRequestGetMenuPackageDetail,
  serviceRequestGetMenuPackageMaster,
  serviceRequestAddMenuPackage,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "menuPackage/SET_LOADING";
export const CLEAR_ERROR = "menuPackage/CLEAR_ERROR";
export const SET_ERROR = "menuPackage/SET_ERROR";
export const INIT_DATA = "menuPackage/INIT_DATA";
export const SET_DATA = "menuPackage/SET_DATA";
export const SET_DATA_DETAIL = "menuPackage/SET_DATA_DETAIL";
export const SET_DOUBLE_SUBMIT = "menuPackage/SET_DOUBLE_SUBMIT";
export const SET_RESULT_FILTER = "menuPackage/SET_RESULT_FILTER";
export const SET_DATA_MASTER = "menuPackage/SET_DATA_MASTER";
export const SET_POP_UP = "menuPackage/SET_POP_UP";
export const SET_ERROR_VALIDASI = "menuPackage/SET_ERROR_VALIDASI";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setDataMaster = (payload) => ({
  type: SET_DATA_MASTER,
  payload,
});

export const setDataDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});
export const setResultFilter = (payload) => ({
  type: SET_RESULT_FILTER,
  payload,
});

export const setTypePopUp = (payload) => ({
  type: SET_POP_UP,
  payload,
});

export const setTypeErrorValidasi = (payload) => ({
  type: SET_ERROR_VALIDASI,
  payload,
});
export const getMenuDataMenuPackaged = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetMenuPackage(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setData(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getMenuDataMenuPackagedDetail = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetMenuPackageDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDataDetail(res?.data?.menuPrivilegeDto));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getMenuDataMenuPackageMaster = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    const res = await serviceRequestGetMenuPackageMaster(payload);

    if (res.status === 200) {
      const result = res?.data?.menuAccessList?.filter(
        (val) =>
          val?.name_en?.trim().toLowerCase() !== "manajemen cek" &&
          val?.name_en?.toLowerCase().trim() !== "profile" &&
          val?.name_en?.toLowerCase().trim() !== "summary" &&
          val?.name_en?.toLowerCase().trim() !== "account" &&
          val?.name_en?.toLowerCase().split(" ")[0] !== "cash"
      );
      dispatch(setDataMaster(result));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const AddMenuDataMenuPackage =
  (payload, payloadValidation, handleNext) => async (dispatch) => {
    dispatch(checkOnline());

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.MENU_PACKAGE,
          ...payloadValidation,
          validate: true,
          loader: setLoading,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoading(true));
              const res = await serviceRequestAddMenuPackage(payload);
              dispatch(setLoading(false));
              if (res.status === 200) {
                dispatch(setTypePopUp(true));
              } else {
                dispatch(handleErrorBE(res));
              }
            } catch (error) {
              dispatch(setHandleError(error?.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };
