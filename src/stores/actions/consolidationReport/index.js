import {
  serviceConsolidationCompanyLogin,
  serviceConsolidationCompanyLoginDetail,
  serviceConsolidationCorporate,
  serviceConsolidationCorporateDetail,
  serviceConsolidationFinancial,
  serviceConsolidationNonFinancial,
  serviceConsolidationSofFilter,
  serviceDownloadConsolidationCmpnyLogin,
  serviceDownloadConsolidationFinancial,
  serviceDownloadConsolidationNonFinancial,
  serviceReportFinancialDetail,
  serviceReportLimitUsage,
  serviceWorkflowGroup,
  serviceGetAllTransactionGroup,
  serviceDownloadConsolidationLimitUsage,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { checkOnline, handleErrorBE } from "../errorGeneral";

export const SET_LOADING = "consolidationReport/SET_LOADING";
export const CLEAR_ERROR = "consolidationReport/CLEAR_ERROR";
export const SET_ERROR = "consolidationReport/SET_ERROR";
export const INIT_DATA = "consolidationReport/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "consolidationReport/SET_DOUBLE_SUBMIT";
export const SET_CONSOLIASTION_FINANCIAL =
  "consolidationReport/SET_CONSOLIASTION_FINANCIAL";
export const SET_CONSOLIASTION_NONFINANCIAL =
  "consolidationReport/SET_CONSOLIASTION_NONFIAL";
export const SET_CONSOLIASTION_NONFIAL =
  "consolidationReport/SET_CONSOLIASTION_NONFIAL";
export const SET_CORPORATE_ID = "consolidationReport/SET_CORPORATE_ID";
export const SET_FULL_NAME = "consolidationReport/SET_FULL_NAME";
export const SET_CORPORATE_DETAIL = "consolidationReport/SET_CORPORATE_DETAIL";
export const SET_NONFINANCIAL_DATA =
  "consolidationReport/SET_NONFINANCIAL_DATA";
export const SET_FINANCIAL_DATA = "consolidationReport/SET_FINANCIAL_DATA";
export const SET_COMPANY_LOGIN = "consolidationReport/SET_COMPANY_LOGIN";
export const SET_COMPANY_LOGIN_ID = "consolidationReport/SET_COMPANY_LOGIN_ID";
export const SET_COMPANY_LOGIN_DETAIL =
  "consolidationReport/SET_COMPANY_LOGIN_DETAIL";
export const SET_LIMIT_USAGE = "consolidationReport/SET_LIMIT_USAGE";
export const SET_FINANCIAL_DETAIL = "consolidationReport/FINANCIAL_DETAIL";
export const SET_DETAIL_LOADING = "consolidationReport/SET_DETAIL_LOADING";
export const SET_SOF_DATA = "consolidationReport/SET_SOF_DATA";
export const SET_SOF_LOADING = "consolidationReport/SET_SOF_LOADING";
export const SET_AUTHORIZATION = "consolidationReport/SET_AUTHORIZATION";
export const SET_WORKFLOW_GROUP = "consolidationReport/SET_WORKFLOW_GROUP";
export const SET_TRANSACTION_GROUP =
  "consolidationReport/SET_TRANSACTION_GROUP";
export const SET_LOADING_WORKFLOWGROUP =
  "consolidationReport/SET_LOADING_WORKFLOWGROUP";

export const setTransactionTypeConf = [
  null,
  "TRANSFER",
  "BILLPAYMENT",
  "PURCHASE",
];

export const setWorkflowGroup = (payload) => ({
  type: SET_WORKFLOW_GROUP,
  payload,
});

export const setWorkflowGroupLoading = (payload) => ({
  type: SET_LOADING_WORKFLOWGROUP,
  payload,
});

export const setTransactionGroup = (payload) => ({
  type: SET_TRANSACTION_GROUP,
  payload,
});

export const setSoftLoading = (payload) => ({
  type: SET_SOF_LOADING,
  payload,
});

export const setSofData = (payload) => ({
  type: SET_SOF_DATA,
  payload,
});

export const setDetailLoading = (payload) => ({
  type: SET_DETAIL_LOADING,
  payload,
});

export const setLimitUsage = (payload) => ({
  type: SET_LIMIT_USAGE,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setFinancialDetail = (payload) => ({
  type: SET_FINANCIAL_DETAIL,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitialData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setCorporateid = (payload) => ({
  type: SET_CORPORATE_ID,
  payload,
});

export const setAuthorization = (payload) => ({
  type: SET_AUTHORIZATION,
  payload,
});

export const setCorporatedetail = (payload) => ({
  type: SET_CORPORATE_DETAIL,
  payload,
});

export const setNonFinancialData = (payload) => ({
  type: SET_NONFINANCIAL_DATA,
  payload,
});

export const setFinancialData = (payload) => ({
  type: SET_FINANCIAL_DATA,
  payload,
});

export const setCompanyLogin = (payload) => ({
  type: SET_COMPANY_LOGIN,
  payload,
});

export const setFullName = (payload) => ({
  type: SET_FULL_NAME,
  payload,
});

export const setCompanyLoginId = (payload) => ({
  type: SET_COMPANY_LOGIN_ID,
  payload,
});

export const setCompanyLoginDetail = (payload) => ({
  type: SET_COMPANY_LOGIN_DETAIL,
  payload,
});

export const handleConsolidation = (payload) => async (dispatch, getState) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceConsolidationCorporate(payload);

    if (res.status === 200) {
      dispatch(setInitialData(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setError({ isError: true, message: err?.message }));
  }
};

export const handleConsolidationDetail =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const res = await serviceConsolidationCorporateDetail(payload);

      if (res.status === 200) {
        dispatch(
          setCorporatedetail({
            name: res?.data?.corporateProfile?.[0]?.corporateName,
            address: res?.data?.corporateProfile?.[0]?.corporateAddress,
            contact: res?.data?.corporateProfile?.[0]?.corporateContact,
            accountNumber: res?.data?.corporateProfile?.[0]?.accountNumber,
            authorization:
              res?.data?.corporateProfile?.[0]?.corporateAuthorization,
            category: "",
          })
        );
      } else {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setError({ isError: true, message: err?.message }));
    }
  };

export const handleConsolidationFinancial =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const res = await serviceConsolidationFinancial(payload);

      if (res.status === 200) {
        dispatch(setFinancialData(res?.data));
      } else {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setError({ isError: true, message: err?.message }));
    }
  };

export const handleConsolidationNonFinancial =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const res = await serviceConsolidationNonFinancial(payload);

      if (res.status === 200) {
        dispatch(setNonFinancialData(res?.data));
      } else {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setError({ isError: true, message: err?.message }));
    }
  };

export const handleConsolidationCompanyLogin =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const res = await serviceConsolidationCompanyLogin(payload);

      if (res.status === 200) {
        dispatch(setCompanyLogin(res?.data));
      } else {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setError({ isError: true, message: err?.message }));
    }
  };

export const handleConsolidationCompanyLoginDetail =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const res = await serviceConsolidationCompanyLoginDetail(payload);
      if (res.status === 200) {
        dispatch(setCompanyLoginDetail(res?.data?.detailCompanyLogin));
      } else {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setError({ isError: true, message: err?.message }));
    }
  };

export const downloadPagesConf = {
  COMPANY_LOGIN: "COMPANY_LOGIN",
  FINANCIAL_REPORT: "FINANCIAL_REPORT",
  NONFINANCIAL_REPORT: "NONFINANCIAL_REPORT",
  LIMITUSAGE_REPORT: "LIMITUSAGE_REPORT",
};

const downloadConsolidationReport = (pages, payload) => {
  switch (pages) {
    case downloadPagesConf.COMPANY_LOGIN:
      return serviceDownloadConsolidationCmpnyLogin(payload);
    case downloadPagesConf.FINANCIAL_REPORT:
      return serviceDownloadConsolidationFinancial(payload);
    case downloadPagesConf.NONFINANCIAL_REPORT:
      return serviceDownloadConsolidationNonFinancial(payload);
    case downloadPagesConf.LIMITUSAGE_REPORT:
      return serviceDownloadConsolidationLimitUsage(payload);
    default:
      return null;
  }
};

export const handleConsolidationReportDownload =
  (payload, { pages, fileName = "Template" }) =>
  async (dispatch) => {
    try {
      await dispatch(checkOnline());
      // dispatch(setLoadingUnduh(true));
      const response = await downloadConsolidationReport(pages, payload);
      const { status, data } = response;
      // dispatch(setLoadingUnduh(false));

      const d = new Date();
      if (status === 200) {
        const getFileName = `${fileName}-${d?.getMilliseconds()}.${
          payload?.format
        }`;
        const url = window.URL.createObjectURL(data);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", getFileName);
        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(status)) {
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          let data = response.data;
          reader.onload = () => {
            data = JSON.parse(reader.result);
            resolve(Promise.reject(data));
          };
          reader.onerror = () => {
            reject(data);
          };
          reader.readAsText(data);
        })
          .then((err) => {
            dispatch(
              setError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          })
          .catch((err) => {
            dispatch(
              setError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          });
        // dispatch(setLoadingUnduh(false));
      }
    } catch {
      // dispatch(setLoadingUnduh(false));
    }
  };

export const handleLimitUsage = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await serviceReportLimitUsage(payload);
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setLimitUsage(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    dispatch(setError({ isError: true, message: error?.message }));
  }
};

export const handleFinancialDetail = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setDetailLoading(true));
    const response = await serviceReportFinancialDetail(payload);
    dispatch(setDetailLoading(false));

    if (response.status) {
      dispatch(setFinancialDetail(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    dispatch(setError({ isError: true, message: error?.message }));
  }
};

export const handleSoftGet = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setSoftLoading(true));
    const response = await serviceConsolidationSofFilter(payload);
    dispatch(setSoftLoading(false));
    if (response.status === 200) {
      dispatch(setSofData(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (err) {
    dispatch(setError({ isError: true, message: err?.message }));
  }
};

export const handleWorkflowGroup = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setWorkflowGroupLoading(true));
    const response = await serviceWorkflowGroup(payload);
    if (response.status === 200) {
      dispatch(setWorkflowGroup(response.data));
    } else {
      handleErrorBE(response);
    }
  } catch (err) {
    dispatch(setError({ isError: true, message: err?.message }));
  }
};

export const handleTransactionGroup = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setWorkflowGroupLoading(true));
    const response = await serviceGetAllTransactionGroup(payload);
    if (response.status === 200) {
      dispatch(setTransactionGroup(response?.data));
    } else {
      handleErrorBE(response);
    }
  } catch (err) {
    dispatch(setError({ isError: true, message: err?.message }));
  }
};
