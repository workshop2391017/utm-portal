// uploadProgress Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const CLEAR_ERROR = "uploadProgress/CLEAR_ERROR";
export const SET_ERROR = "uploadProgress/SET_ERROR";
export const INIT_DATA = "uploadProgress/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "uploadProgress/SET_DOUBLE_SUBMIT";
export const SET_UPLOAD_PROGRESS = "uploadProgress/SET_UPLOAD_PROGRESS";

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setUploadProgress = (value, fileName) => ({
  type: SET_UPLOAD_PROGRESS,
  payload: { fileName, value },
});

export const setErrorUpload = (value, fileName) => ({
  type: SET_ERROR,
  payload: { fileName, value },
});
