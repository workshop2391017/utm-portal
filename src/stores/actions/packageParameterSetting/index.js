import {
  serviceGetbusinessfield,
  serviceGetSegmentasi,
  serviceParameterSetting,
  serviceParameterSettingDetail,
  serviceParameterSettingExecute,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";
import { validateTask } from "../validateTaskPortal";

export const SET_LOADING = "packageParameterSetting/SET_LOADING";
export const SET_LOADING_DETAIL = "packageParameterSetting/SET_LOADING_DETAIL";
export const SET_DATA = "packageParameterSetting/SET_DATA";
export const SET_ERROR = "productRekening/SET_ERROR";
export const CLEAR_ERROR = "productRekening/CLEAR_ERROR";
export const SET_DETAIL_DATA = "productRekening/SET_DETAIL_DATA";
export const SET_PARAMETER_SETTING_ID =
  "productRekening/SET_PARAMETER_SETTING_ID";
export const SET_LOADING_EXECUTE = "productRekening/SET_LOADING_EXECUTE";
export const SET_EXECUTE_SUCCESS = "productRekening/SET_EXECUTE_SUCCESS";
export const SET_CLEAR_DATA = "productRekening/SET_CLEAR_DATA";
export const SET_BUSINESS_FIELD = "productRekening/SET_BUSINESS_FIELD";
export const SET_SEGMENTATION = "productRekening/SET_SEGMENTATION";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingDetail = (payload) => ({
  type: SET_LOADING_DETAIL,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setClearError = () => ({
  type: CLEAR_ERROR,
});

export const setDetailData = (payload) => ({
  type: SET_DETAIL_DATA,
  payload,
});

export const setParameterSettingId = (payload) => ({
  type: SET_PARAMETER_SETTING_ID,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setExecuteSuccess = (payload) => ({
  type: SET_EXECUTE_SUCCESS,
  payload,
});

export const setClearParameterSetting = () => ({
  type: SET_CLEAR_DATA,
});

export const setBusinessField = (payload) => ({
  type: SET_BUSINESS_FIELD,
  payload,
});

export const setSegmentation = (payload) => ({
  type: SET_SEGMENTATION,
  payload,
});

export const getParameterSettingList = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceParameterSetting(payload);

    if (res.status === 200) {
      dispatch(setData(res.data));
    } else if (handleExceptionError(res.status)) {
      dispatch(handleErrorBE(res));
    }

    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setError(error?.message));
  }
};

export const getParameterSettingDetail = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoadingDetail(true));
    const res = await serviceParameterSettingDetail(payload);

    if (res.status === 200) {
      dispatch(setDetailData(res.data.parameterSettingDetail));
    } else if (handleExceptionError(res.status)) {
      dispatch(handleErrorBE(res));
    }

    dispatch(setLoadingDetail(false));
  } catch (error) {
    dispatch(setLoadingDetail(false));
    dispatch(setError(error?.message));
  }
};

export const handleExecuteParametersetting =
  (payload, payloadValidation, handeNext = () => {}) =>
  async (dispatch) => {
    dispatch(checkOnline());

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PARAMETER_SETTING,
          ...payloadValidation,
          loader: setLoadingExecute,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoadingExecute(true));
              const res = await serviceParameterSettingExecute(payload);

              if (res.status === 200) {
                dispatch(setExecuteSuccess(true));
              } else if (handleExceptionError(res.status)) {
                dispatch(handleErrorBE(res));
              }

              dispatch(setLoadingExecute(false));
            } catch (error) {
              dispatch(setLoadingDetail(false));
              dispatch(setError(error?.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handeNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const handleBusinessField = (payload) => async (dispatch) => {
  dispatch(checkOnline());

  try {
    // dispatch(setLoadingExecute(true));
    const res = await serviceGetbusinessfield(payload);

    if (res.status === 200) {
      dispatch(setBusinessField(res.data));
    } else if (handleExceptionError(res.status)) {
      dispatch(handleErrorBE(res));
    }

    // dispatch(setLoadingExecute(false));
  } catch (error) {
    dispatch(setLoadingDetail(false));
    dispatch(setError(error?.message));
  }
};

export const handleSegmentation = (payload) => async (dispatch) => {
  dispatch(checkOnline());

  try {
    // dispatch(setLoadingExecute(true));
    const res = await serviceGetSegmentasi(payload);

    if (res.status === 200) {
      dispatch(setSegmentation(res.data));
    } else if (handleExceptionError(res.status)) {
      dispatch(handleErrorBE(res));
    }

    // dispatch(setLoadingExecute(false));
  } catch (error) {
    dispatch(setLoadingDetail(false));
    dispatch(setError(error?.message));
  }
};
