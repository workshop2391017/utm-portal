// productRekening Actions
// --------------------------------------------------------

import { privilegeCONFIG } from "configuration/privilege";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

const {
  serviceRequestGetProductAccountType,
  serviceRequestGetProduct,
  serviceRequestAddProduct,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "productRekening/SET_LOADING";
export const CLEAR_ERROR = "productRekening/CLEAR_ERROR";
export const SET_ERROR = "productRekening/SET_ERROR";
export const INIT_DATA = "productRekening/INIT_DATA";
export const ADD_DATA = "productRekening/ADD_DATA";
export const PRODUCT_ACCUNT_TYPE = "productRekening/PRODUCT_ACCUNT_TYPE";
export const ACCOUNT_PRODUCT = "productRekening/ACCOUNT_PRODUCT";
export const ACCOUNT_PRODUCT_TEMP = "productRekening/ACCOUNT_PRODUCT_TEMP";
export const SET_DOUBLE_SUBMIT = "productRekening/SET_DOUBLE_SUBMIT";
export const SET_LOADINGSUBMIT = "productRekening/SET_LOADINGSUBMIT";

export const setLoadingSubmit = (payload) => ({
  type: SET_LOADINGSUBMIT,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitDataProductAccunt = (payload) => ({
  type: PRODUCT_ACCUNT_TYPE,
  payload,
});
export const setInitDataProduct = (payload) => ({
  type: ACCOUNT_PRODUCT,
  payload,
});

export const setInitDataProductTemp = (payload) => ({
  type: ACCOUNT_PRODUCT_TEMP,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setAddData = (payload) => ({
  type: ADD_DATA,
  payload,
});

export const getProductAccountType = (payload) => async (dispatch) => {
  try {
    setLoading(true);
    const res = await serviceRequestGetProductAccountType(
      payload,
      privilegeCONFIG.PRODUCT_REKENING.lIST
    );
    setLoading(false);

    if (res.status === 200) {
      dispatch(setInitDataProductAccunt(res?.data?.accountTypeList));
    } else {
      dispatch(handleErrorBE(res));
    }

    console.warn("res:", res);
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

const handleGrouping = (payload) => {
  const arr = [];

  payload.forEach((elm) => {
    const fIdx = arr.findIndex((e) => e.accountType === elm.accountType);

    if (fIdx >= 0) {
      arr[fIdx].child.push(elm);
    } else {
      const assign = {
        accountType: elm?.accountType,
        label:
          elm?.accountType === "SA"
            ? "Saving Account"
            : elm?.accountType === "CA"
            ? "Current Account"
            : elm?.accountType === "LN"
            ? "Loan Account"
            : elm?.accountType === "CD"
            ? "Deposit Account"
            : null,
        child: [],
      };
      assign.child.push(elm);
      arr.push(assign);
    }
  });

  return arr;
};

export const getProduct = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetProduct(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(
        setInitDataProduct(handleGrouping(res?.data?.accountProductDtoList))
      );
      dispatch(
        setInitDataProductTemp(handleGrouping(res?.data?.accountProductDtoList))
      );
    } else {
      dispatch(handleErrorBE(res));
    }

    console.warn("res:", res);
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const addProduct =
  (payload, payloadValidate, handleNext, handleResetData) =>
  async (dispatch) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.SAVE_ACCOUNT_PRODUCT,
          name: null,
          id: null,
          ...payloadValidate,
          validate: true,
          loader: setLoadingSubmit,
        },
        {
          async onContinue() {
            dispatch(setLoadingSubmit(true));
            try {
              const res = await serviceRequestAddProduct(
                payload,
                privilegeCONFIG.PRODUCT_REKENING.ADD
              );

              if (res.status === 200) {
                handleResetData();
                dispatch(setAddData(true));
              } else {
                dispatch(handleErrorBE(res));
              }
              dispatch(setLoadingSubmit(false));
            } catch (error) {
              dispatch(setHandleError(error?.message));
              dispatch(setLoadingSubmit(false));
            }
          },
          onError() {
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };
