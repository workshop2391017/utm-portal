// validatedatalms Actions
// --------------------------------------------------------

import { hanldeValidationDataLms } from "utils/api";
import { handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING_VALIDATE = "validatedatalms/SET_LOADING_VALIDATE";
export const SET_IS_ALREADY_EXIST = "validatedatalms/SET_IS_ALREADY_EXIST";
export const SET_LOADING_VALIDATE_BY_FIELD =
  "validatedatalms/SET_LOADING_VALIDATE_BY_FIELD";
export const SET_CLEAR_ALREADY_EXIST =
  "validatedatalms/SET_CLEAR_ALREADY_EXIST";

export const confValidateMenuName = {
  CHARGE: "2",
  CHARGE_NAME: "5",
  SERVICE_CHARGE_NAME: "6",
  SERVICE_CHARGE: "3",
  SEGMENTATION_NAME: "7",
  PRODUCT_NAME: "8",
  PRODUCT_CODE: "9",
  LIMIT_PACKAGE_NAME: "10",
  LIMIT_PACKAGE_CODE: "11",
  CHARGE_CODE: "12",
  MENU_PACKAGE_NAME: "13",
  MENU_PACKAGE_CODE: "14",
  TIERING_CODE: "15",
  TIERING: "1",
  TIERING_NAME: "4",
  CHARGE_PACKAGE_NAME: "16",
  CHARGE_PACKAGE_CODE: "17",
  BRANCH_SEGMENTATION_NAME: "18",
  BRANCH_SEGMENTATION_CODE: "19",
  PARAMETER_ADVANCE_TYPE: "20",
  ACCOUNT_PRODUCT_NAME: "21",
  ACCOUNT_PRODUCT_CODE: "22",
  HOST_ERROR_CODE: "23",
  BANK_INTERNATIONAL_NAME: "24",
  BANK_INTERNATIONAL_CODE: "25",
  BANK_INTERNATIONAL_SWIFT: "26",
  BANK_DOMESTIC_NAME: "27",
  BANK_DOMESTIC_CODE: "28",
  BANK_DOMESTIC_SWIFT: "29",
  HANDLING_OFFICER_ID_CODE: "30",
  SPECIAL_MENU_CODE: "31",
  SPECIAL_MENU_NAME: "32",
  PRIVILEGE_HEAD: "33",
  PRIVILEGE_BRANCH: "34",
  PARAMETER_SETTINGS_EDIT: "36",
  PARAMETER_SETTINGS_ADD: "37",
};

export const setLoading = (payload) => ({
  type: SET_LOADING_VALIDATE,
  payload,
});

export const setLoadingByField = (payload) => ({
  type: SET_LOADING_VALIDATE_BY_FIELD,
  payload,
});

export const setAlreadyExist = (payload) => ({
  type: SET_IS_ALREADY_EXIST,
  payload,
});

export const setClearAlreadyExist = () => ({
  type: SET_CLEAR_ALREADY_EXIST,
});

export const handlevalidationDataLms =
  ({ key, ...rest }) =>
  async (dispatch) => {
    const loading = rest?.loadingByField ? setLoadingByField : setLoading;

    dispatch(loading(rest?.loadingByField ? { [key]: true } : true));
    const payload = {
      id: null,
      ...rest,
    };

    try {
      const response = await hanldeValidationDataLms({
        ...payload,
        table: payload.table,
      });
      dispatch(loading(rest?.loadingByField ? { [key]: false } : false));

      if (response.status === 200) {
        dispatch(setAlreadyExist({ [key]: !response.data }));
        dispatch(loading(rest?.loadingByField ? { [key]: false } : false));
      } else if (response.data.errorCode === "EXCDLMT") {
        dispatch(setAlreadyExist({ [key]: response.data.message }));
        dispatch(loading(rest?.loadingByField ? { [key]: false } : false));
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      console.error("ERR ", e);
    }
  };

export const handleValidateOnSubmit =
  ({ keys, ...rest }, onContinue) =>
  async (dispatch, getState) => {
    const { isAlreadyExist } = getState()?.validatedatalms;
    dispatch(setLoading(true));
    try {
      const response = await hanldeValidationDataLms(rest);

      if (response.status === 200) {
        dispatch(setLoading(false));
        keys.forEach((key) => {
          dispatch(
            setAlreadyExist({
              [key]: !response.data,
            })
          );
        });

        if (response.data) {
          onContinue();
        }
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      console.error("ERR ", e);
    }
  };

export const handleClearErrorLms = (payload) => (dispatch) => {
  dispatch(setAlreadyExist(payload));
};
