// billerCategory Actions
// --------------------------------------------------------

import {
  serviceBillerCategoryList,
  serviceBillerCategoryDelete,
  serviceBillerCategoryEdit,
  serviceBillerCategoryGetCategories,
} from "utils/api";
import { handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "billerCategory/SET_LOADING";
export const CLEAR_ERROR = "billerCategory/CLEAR_ERROR";
export const SET_ERROR = "billerCategory/SET_ERROR";
export const INIT_DATA = "billerCategory/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "billerCategory/SET_DOUBLE_SUBMIT";
export const SET_DATA = "billerCategory/SET_DATA";
export const SET_SUCCESS = "billerCategory/SET_SUCCESS";
export const SET_CATEGORIES = "billerCategory/SET_CATEGORIES";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});

export const setCategories = (payload) => ({
  type: SET_CATEGORIES,
  payload,
});

export const actionBillerCategoryGet =
  (payload, setTableData) => async (dispatch) => {
    try {
      const response = await serviceBillerCategoryList(payload);

      if (response.status === 200) {
        dispatch(setData(response.data?.billerCategoryList));
      } else {
        setTableData([]);
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionBillerCategoryGetCategories = () => async (dispatch) => {
  try {
    const response = await serviceBillerCategoryGetCategories();

    if (response.status === 200) {
      // hide bancassurance
      const payload = response.data.filter(({ code }) => +code !== 5) || [];
      dispatch(setCategories(payload));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionBillerCategoryDelete =
  (payload, setOpenConfirmation) => async (dispatch) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceBillerCategoryDelete(payload);
      dispatch(setLoading(false));
      setOpenConfirmation({ open: false, type: undefined, id: null });

      if (response.status === 200) {
        dispatch(setSuccess(true));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error, false));
    }
  };

export const actionBillerCategoryEdit =
  (payload, setOpenConfirmation) => async (dispatch) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceBillerCategoryEdit(payload);
      dispatch(setLoading(false));
      setOpenConfirmation(false);

      if (response.status === 200) {
        dispatch(setSuccess(true));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error, false));
    }
  };
