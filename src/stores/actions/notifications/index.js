/* eslint-disable array-callback-return */
// notifications Actions
// --------------------------------------------------------
import {
  handleErrorBE,
  checkOnline,
  setErrorAlreadyUsed,
} from "stores/actions/errorGeneral";

import {
  serviceGetAllPromosTitle,
  serviceNotifications,
  serviceNotificationsDetail,
  serviceNotificationsSave,
} from "utils/api";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "notifications/SET_LOADING";
export const SET_LOADING_EXCUTE = "notifications/SET_LOADING_EXCUTE";
export const CLEAR_ERROR = "notifications/CLEAR_ERROR";
export const SET_ERROR = "notifications/SET_ERROR";
export const INIT_DATA = "notifications/INIT_DATA";
export const SET_DATA = "notifications/SET_DATA";
export const SET_DETAIL = "notifications/SET_DETAIL";
export const SET_NOTIF_ID = "notifications/SET_NOTIF_ID";
export const SET_SUCCESS = "notifications/SET_SUCCESS";
export const SET_PROMOTION_TITLES = "notifications/SET_PROMOTION_TITLES";

export const setNotifId = (payload) => ({
  type: SET_NOTIF_ID,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const setSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});

export const setPromotionTitles = (payload) => ({
  type: SET_PROMOTION_TITLES,
  payload,
});

export const promoMediaOptions = [
  { key: "isPushNotif", value: "Appplication" },
  { key: "isSms", value: "SMS" },
  { key: "isEmail", value: "Email" },
  { key: "isWA", value: "WhatsApp" },
];

export const actionNotificationGet =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceNotifications(payload);
      dispatch(setLoading(false));
      if (response.status === 200) {
        const { promoNotifications } = response.data;
        const dataTable = [];
        promoNotifications.map((item) => {
          const media = [];
          promoMediaOptions.map((itemPromo) => {
            Object.entries(item).map(([key, value]) => {
              if (key === itemPromo.key && value) {
                media.push(itemPromo.value);
              }
            });
          });

          dataTable.push({
            scheduleDate: item.scheduleDate,
            title: item.title,
            media,
            promoStatus: item.promoStatus,
          });
        });
        dispatch(setData({ ...response.data, dataTable }));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
      console.log("Ini error", error);
    }
  };

export const actionNotificationPromotionTitlesGet =
  (payload) => async (dispatch, getState) => {
    try {
      const response = await serviceGetAllPromosTitle(payload);
      // setTimeout(() => {
      //   dispatch(setPromotionTitles(dummyDataPromotionTitles));
      if (response.status === 200) {
        const { promotionList } = response.data;
        const titleList = [];
        promotionList.map((item) => {
          titleList.push({
            value: item.promotionCode,
            lable: item.promotionSubject,
            endDate: item.endDate,
          });
        });

        dispatch(setPromotionTitles(titleList));
      } else {
        throw response;
      }
      // }, 1000);
    } catch (error) {
      dispatch(handleErrorBE(error));
      console.log("Ini error", error);
    }
  };

export const actionNotificationSave =
  (payload, closeConfirmationHandler) => async (dispatch, getState) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceNotificationsSave(payload);
      dispatch(setLoading(false));
      if (response.status === 200) {
        closeConfirmationHandler();
        dispatch(setSuccess(true));
      } else {
        closeConfirmationHandler();
        dispatch(handleErrorBE(response));
        dispatch(
          setErrorAlreadyUsed({
            redirect: false,
          })
        );
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
      console.log("Ini error", error);
    }
  };

export const actionNotificationDelete =
  (payload, closeConfirmationHandler) => async (dispatch, getState) => {
    dispatch(setLoading(true));
    try {
      //   const response = await serviceNotificationsDelete(payload);
      // dispatch(setLoading(false));
      setTimeout(() => {
        dispatch(setLoading(false));
        dispatch(setSuccess(true));
        closeConfirmationHandler();
        // if (response.status === 200) {
        // closeConfirmationHandler();
        //   dispatch(setSuccess(true));
        // } else {
        // closeConfirmationHandler();
        // throw res
        // }
      }, 1000);
    } catch (error) {
      dispatch(handleErrorBE(error));
      console.log("Ini error", error);
    }
  };

export const handleGetnotificationsDetail =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceNotificationsDetail(payload);
      dispatch(setLoading(false));
      if (response.status === 200) {
        dispatch(setDetail(response.data));
      } else {
        dispatch(setData([]));
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      console.error("ERR ", err);
      dispatch(setLoading(false));
    }
  };
