// menuGlobal Actions
// --------------------------------------------------------

import { privilegeCONFIG } from "configuration/privilege";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

const {
  serviceRequestGetMenuGlobal,
  serviceRequestExcuteMenuGlobal,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "menuGlobal/SET_LOADING";
export const CLEAR_ERROR = "menuGlobal/CLEAR_ERROR";
export const SET_ERROR = "menuGlobal/SET_ERROR";
export const INIT_DATA = "menuGlobal/INIT_DATA";
export const ADD_SUCCESS = "menuGlobal/ADD_SUCCESS";
export const DATA_DETAIL = "menuGlobal/DATA_DETAIL";
export const SET_DATA_MENU_GELOBAL = "menuGlobal/SET_DATA_MENU_GELOBAL";
export const SET_DOUBLE_SUBMIT = "menuGlobal/SET_DOUBLE_SUBMIT";
export const SET_DATA_DROPDOWN = "menuGlobal/SET_DATA_DROPDOWN";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setDataMenuGlobal = (payload) => ({
  type: SET_DATA_MENU_GELOBAL,
  payload,
});
export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const setDataDetail = (payload) => ({
  type: DATA_DETAIL,
  payload,
});

export const setDataSucces = (payload) => ({
  type: ADD_SUCCESS,
  payload,
});

export const setTypeDataDropDown = (payload) => ({
  type: SET_DATA_DROPDOWN,
  payload,
});

export const getMenuDataGlobal = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetMenuGlobal(
      payload,
      privilegeCONFIG.MENU_GLOBAL.LIST
    );
    dispatch(setLoading(false));

    if (res.status === 200) {
      const result = res?.data?.menuAccessList?.filter(
        (val) =>
          val?.name_en?.trim().toLowerCase() !== "manajemen cek" &&
          val?.name_en?.toLowerCase().trim() !== "summary" &&
          val?.name_en?.toLowerCase().split(" ")[0] !== "cash"
      );
      dispatch(setDataMenuGlobal(result));
      const arr = [
        {
          name_en: "All Menu",
          id: "all-menu",
          index: "all-menu",
        },
      ];

      result.forEach((e) => {
        arr.push(e);
      });

      dispatch(setTypeDataDropDown(arr));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const addMenuDataGlobal = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestExcuteMenuGlobal(payload);

    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDataSucces(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};
