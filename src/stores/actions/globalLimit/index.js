import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

const { serviceRequestGlobalLimit } = require("utils/api");

export const SET_GLOBAL_LIMIT_TYPE = "dashboard/SET_GLOBAL_LIMIT_TYPE";
export const SET_GLOBAL_LIMIT_ERROR_TYPE =
  "dashboard/SET_GLOBAL_LIMIT_ERROR_TYPE";

export const setTypeGlobalLimit = (payload) => ({
  type: SET_GLOBAL_LIMIT_TYPE,
  payload,
});

export const setTypeErrorGlobalLimit = (payload) => ({
  type: SET_GLOBAL_LIMIT_ERROR_TYPE,
  payload,
});

export const getDataGlobalLimit = () => async (dispatch) => {
  try {
    const res = await serviceRequestGlobalLimit({
      currency: "",
      transactionCategory: "",
      transactionGroup: "",
    });

    if (res.status === 200) {
      dispatch(setTypeGlobalLimit(res.data.globalLimitList));
    } else {
      dispatch(handleErrorBE(res));
    }

    // console.warn("res:", res.data.globalLimitList);
  } catch (err) {
    // dispatch(
    //   setTypeErrorGlobalLimit({
    //     status: 500,
    //     msg: "Server tidak konek ke internet",
    //   })
    // );
    console.error("Catch Error ", err);
  }
};
