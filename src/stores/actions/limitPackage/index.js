// login Actions
// --------------------------------------------------------

import {
  editGlobalLimitPackage,
  hanldeValidationDataLms,
  serviceAddLimitPackage,
  serviceCurrencyMatrix,
  serviceLimitPackage,
  serviceLimitPackageDetail,
  serviceRequestGetCurrency,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import {
  handleErrorBE,
  checkOnline,
  setErrorAlreadyUsed,
} from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "limitPackage/SET_LOADING";
export const SET_DATA = "limitPackage/SET_DATA";
export const SET_ERROR = "limitPackage/SET_ERROR";
export const SET_DETAIL = "limitPackage/SET_DETAIL";
export const SET_ID = "limitPackage/SET_ID";
export const SET_LOADING_EXECUTE = "limitPackage/SET_LOADING_EXECUTE";
export const SET_SUCCESS_EXECUTE = "limitPackage/SET_SUCCESS_EXECUTE";
export const SET_CLEAR = "limitPackage/SET_CLEAR";
export const SET_CURR_MATRIX = "limitPackage/SET_CURR_MATRIX";
export const SET_ADD_LIMIT_PACKAGE = "limitPackage/SET_ADD_LIMIT_PACKAGE";
export const SET_SUCCESS_SUBMIT = "limitPackage/SET_SUCCESS_SUBMIT";
export const SET_LOADING_CURRENCY = "limitPackage/SET_LOADING_CURRENCY";
export const SET_CURRENCY = "limitPackage/SET_CURRENCY";

// global error
export const SET_HANDLE_ERROR = "limitPackage/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "limitPackage/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setId = (payload) => ({
  type: SET_ID,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setSucessExecute = (payload) => ({
  type: SET_SUCCESS_EXECUTE,
  payload,
});

export const setClear = () => ({
  type: SET_CLEAR,
});

export const setCurrMatrix = (payload) => ({
  type: SET_CURR_MATRIX,
  payload,
});

export const setAddLimitPaclage = (payload) => ({
  type: SET_ADD_LIMIT_PACKAGE,
  payload,
});

export const setLoadingCurrency = (payload) => ({
  type: SET_LOADING_CURRENCY,
  payload,
});

export const setCurrency = (payload) => ({
  type: SET_CURRENCY,
  payload,
});

export const handleGetLimitPackage = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceLimitPackage(payload);
    if (response.status === 200) {
      dispatch(setData(response.data));
    } else {
      dispatch(handleErrorBE(response));
      dispatch(setData([]));
    }
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setError(error.message));
    dispatch(setData([]));
    dispatch(setLoading(false));
  }
};
export const handleGetLimitPackageDetail = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceLimitPackageDetail(payload);
    if (response.status === 200) {
      dispatch(
        setDetail({
          ...response.data,
          limitPackage: response.data?.limitPackage.filter(
            (e) => e?.serviceName?.toLowerCase() !== "annual"
          ),
        })
      );
    } else {
      dispatch(handleErrorBE(response));
      dispatch(setDetail({}));
    }

    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setError(error.message));
    dispatch(setDetail({}));
    dispatch(setLoading(false));
  }
};

export const handleEditLimitPackage = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingExecute(true));
  try {
    const response = await serviceAddLimitPackage(payload);
    if (response.status === 200) {
      dispatch(setSucessExecute(true));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoadingExecute(false));
    }
  } catch (error) {
    dispatch(setError(error.message));
    dispatch(setLoadingExecute(false));
  }
};

export const getCurrency = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingCurrency(true));

  try {
    const response = await serviceRequestGetCurrency(payload);
    if (response.status === 200) {
      dispatch(
        setCurrency(
          (response.data?.currencyList ?? []).map((elm) => ({
            ...elm,
            name: `${elm?.currencyCode} - ${elm?.nameEng}`,
            id: elm?.currencyCode,
          }))
        )
      );
      dispatch(setLoadingCurrency(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoadingCurrency(false));
    }
  } catch (e) {
    dispatch(setError(e.message));
    dispatch(setLoadingCurrency(false));
  }
};

export const handleCurrencyMatrix = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoading(true));
  try {
    const response = await serviceCurrencyMatrix(payload);
    if (response.status === 200) {
      dispatch(
        setCurrMatrix(
          response.data.filter(
            (e) => e?.serviceName?.toLowerCase() !== "annual"
          )
        )
      );
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoading(false));
    }
  } catch (error) {
    dispatch(setError(error.message));
    dispatch(setLoading(false));
  }
};

export const handleSaveLimitPackage =
  (payload, payloadValidation) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoadingExecute(true));
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.LIMIT_PACKAGE,
          ...payloadValidation,
          id: null,
          validate: true,
          loader: setLoadingExecute,
        },
        {
          async onContinue() {
            try {
              const response = await serviceAddLimitPackage(payload);
              if (response.status === 200) {
                dispatch(setSucessExecute(true));
              } else if (handleExceptionError(response.status)) {
                dispatch(handleErrorBE(response));
                dispatch(setLoadingExecute(false));
              }

              dispatch(setLoadingExecute(false));
            } catch (error) {
              dispatch(setError(error.message));
              dispatch(setLoadingExecute(false));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
          },
        },
        {
          redirect: false,
        }
      )
    );
  };
export const handleValidateEditDataLimitPackage =
  (payload, callback) => async (dispatch) => {
    try {
      const response = await hanldeValidationDataLms(payload);

      if (response.status === 200) {
        if (response.data) callback.onContinue();
        if (!response.data)
          dispatch(
            setErrorAlreadyUsed({
              isError: true,
              message: "Data is being used",
            })
          );
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      console.error("ERR ", err);
    }
  };
