export const SET_COUNTER = 'exampleActions/SET_COUNTER';
export const SET_TIME = 'exampleActions/SET_TIME';

// example shorthand
export const setCounter = (payload) => ({
  type: SET_COUNTER,
  payload,
});

// example standard dispatch
export const setTime = (payload) => (dispatch) =>
  dispatch({
    type: SET_TIME,
    payload,
  });

  
