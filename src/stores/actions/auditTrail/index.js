// auditTrail Actions
// --------------------------------------------------------
import moment from "moment";
import {
  getAuditTrail,
  serviceDownloadExcelAuditTrail,
  getTransactionDetail,
  serviceSegmentationDetail,
  serviceRequestGetMenuPackageDetail,
} from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { setLoadingGlobal } from "stores/actions/pageContainer";
import {
  handleExceptionError,
  handleValueActivityDetailAudit,
} from "utils/helpers";
import { detailTransaksi } from "utils/helpers/transaksiHelperAudit";
import stringContainsArrayPattern from "utils/helpers/stringContainsArrayPattern";
import { dummyMulti } from "./dummyResponse";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "auditTrail/SET_LOADING";
export const CLEAR_ERROR = "auditTrail/CLEAR_ERROR";
export const SET_ERROR = "auditTrail/SET_ERROR";
export const INIT_DATA = "auditTrail/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "auditTrail/SET_DOUBLE_SUBMIT";
export const SET_PAYLOAD_GET_DATA = "auditTrail/SET_PAYLOAD_GET_DATA";
export const SET_CLEAR_PAYLOAD_GET_DATA =
  "auditTrail/SET_CLEAR_PAYLOAD_GET_DATA";
export const SET_ACTIVITY_NAME = "auditTrail/SET_ACTIVITY_NAME";
export const SET_DATA_TABLE_AUDIT = "auditTrail/SET_DATA_TABLE_AUDIT";
export const SET_DETAIL_ACTIVITY = "auditTrail/SET_DETAIL_ACTIVITY";
export const SET_OLD_DATA = "auditTrail/SET_OLD_DATA";
export const SET_NEW_DATA = "auditTrail/SET_NEW_DATA";
export const SET_TYPE_DETAIL = "auditTrail/SET_TYPE_DETAIL";
export const SET_DATA_SEARCH = "auditTrail/SET_DATA_SEARCH";
export const SET_DATA_RESI_DETAIL = "auditTrail/SET_DATA_RESI_DETAIL";
export const SET_DETAIL_RESI_TRANSFER = "auditTrail/SET_DETAIL_RESI_TRANSFER";
export const SET_MASTER_RESI_PAYMENT = "auditTrail/SET_MASTER_RESI_PAYMENT";
export const SET_DETAIL_RESI_PAYMENT = "auditTrail/SET_DETAIL_RESI_PAYMENT";
export const SET_PAGES_MASS_TRANSFER = "auditTrail/SET_PAGES_MASS_TRANSFER";
export const SET_SEGMENTATION_DETAIL = "auditTrail/SET_SEGMENTATION_DETAIL";
export const SET_MENU_PACKAGE_DETAIL = "auditTrail/SET_MENU_PACKAGE_DETAIL";
export const SET_DETAIL_OPENING_SECONDARY_ACCOUNT =
  "auditTrail/SET_DETAIL_OPENING_SECONDARY_ACCOUNT";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDataSearch = (payload) => ({
  type: SET_DATA_SEARCH,
  payload,
});

export const clearPayloadGetData = () => ({
  type: SET_CLEAR_PAYLOAD_GET_DATA,
});

export const setDataResiTransaksi = (payload) => ({
  type: SET_DATA_RESI_DETAIL,
  payload,
});

export const setMasterResiPayment = (payload) => ({
  type: SET_MASTER_RESI_PAYMENT,
  payload,
});

export const setDetailResiPayment = (payload) => ({
  type: SET_DETAIL_RESI_PAYMENT,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setActivityName = (payload) => ({
  type: SET_ACTIVITY_NAME,
  payload,
});

export const setPayloadGetData = (payload) => ({
  type: SET_PAYLOAD_GET_DATA,
  payload,
});

export const setDataTableAudit = (payload) => ({
  type: SET_DATA_TABLE_AUDIT,
  payload,
});

export const setDetailActivity = (payload) => ({
  type: SET_DETAIL_ACTIVITY,
  payload,
});

export const setNewData = (payload) => ({
  type: SET_NEW_DATA,
  payload,
});

export const setOldData = (payload) => ({
  type: SET_OLD_DATA,
  payload,
});

export const setTypeDetail = (payload) => ({
  type: SET_TYPE_DETAIL,
  payload,
});
export const setPagesMass = (payload) => ({
  type: SET_PAGES_MASS_TRANSFER,
  payload,
});

export const setDetailResiTransfer = (payload) => ({
  type: SET_DETAIL_RESI_TRANSFER,
  payload,
});

export const setDetailOpeningSecondaryAccount = (payload) => ({
  type: SET_DETAIL_OPENING_SECONDARY_ACCOUNT,
  payload,
});

export const setSegmentationDetail = (payload) => ({
  type: SET_SEGMENTATION_DETAIL,
  payload,
});

export const setMenuPackageDetail = (payload) => ({
  type: SET_MENU_PACKAGE_DETAIL,
  payload,
});

export const getTransactionDetailAction =
  (userData, handleSuccess) => async (dispatch, getState) => {
    const { userActivityDetail, activityName } = userData;
    const activity = handleValueActivityDetailAudit(activityName);
    // console.warn("Audit-trail", userData);
    try {
      dispatch(setLoadingGlobal(true));
      const payload = {
        transactionId:
          userActivityDetail?.transactionId || userActivityDetail?.oldData,
      };

      // if (!activity.includes("Login")) {
      // SKIP HIT DETAIL FOR LOGIN and EDIT
      if (!stringContainsArrayPattern(activity, ["Login", "Edit"])) {
        const response = await getTransactionDetail(payload);
        dispatch(setLoadingGlobal(false));
        // console.warn(response);
        const { status, data } = response;
        if (status === 200) {
          handleSuccess(userData);
          if (
            activity === "Execute Transfer" ||
            activity === "Execute Sweep" ||
            activity === "Execute Own" ||
            activity === "Execute Samebank" ||
            activity === "Execute SKN" ||
            activity === "Execute RTGS" ||
            activity === "Execute Online" ||
            activity === "Execute Payroll" ||
            activity === "Execute BI-FAST"
          ) {
            dispatch(setDetailResiTransfer(data?.transactionHistory));
          } else if (
            activity === "Execute Billpayment" ||
            activity === "Execute Purchase"
          ) {
            dispatch(
              setDetailResiPayment(detailTransaksi(data?.transactionHistory))
            );
            dispatch(setMasterResiPayment(data?.transactionHistory));
          } else if (activity === "Create Saving") {
            dispatch(
              setDetailOpeningSecondaryAccount(data?.transactionHistory)
            );
          }
        } else {
          dispatch(handleErrorBE(response, false));
        }
      } else {
        dispatch(setLoadingGlobal(false));
        handleSuccess(userData);
      }
    } catch (error) {
      console.error("ERR ", error);
    }
  };

export const getDataAudit = (userType) => async (dispatch, getState) => {
  try {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    const payloadGetData = getState().auditTrail.payloadGetData;
    const res = await getAuditTrail({
      ...payloadGetData,
      userType,
    });
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDataTableAudit(res.data));
      // HARDCODE DUMMY FOR MULTI (temp)
      // dispatch(
      //   setDataTableAudit({
      //     ...res.data,
      //     userActivitylist: [
      //       ...(userType === "customer" ? [...dummyMulti] : []),
      //       ...res.data.userActivitylist,
      //     ],
      //   })
      // );
      // console.warn(res, "ini response");
    } else {
      dispatch(setDataTableAudit({}));
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};

export const getSegmentationDetailAuditTrail =
  (payload) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const res = await serviceSegmentationDetail(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(setSegmentationDetail(res.data.listSegmentation[0]));
        // console.warn("ini response :", res);
      } else {
        dispatch(setSegmentationDetail({}));
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      console.error("ERR ", error);
    }
  };

export const getMenuPackageDetailAuditTrail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetMenuPackageDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setMenuPackageDetail(res.data.menuPrivilegeDto));
      // console.warn("ini response :", res);
    } else {
      dispatch(setMenuPackageDetail({}));
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};

export const handleDownloadAuditTrail =
  (type) => async (dispatch, getState) => {
    const payloadGetData = getState().auditTrail.payloadGetData;
    try {
      await dispatch(checkOnline());
      dispatch(setLoading(true));
      const response = await serviceDownloadExcelAuditTrail({
        userType: type,
        startDate: payloadGetData.startDate,
        endDate: payloadGetData.endDate,
      });
      const { status, data } = response;
      dispatch(setLoading(false));

      const d = new Date();
      if (status === 200) {
        const getFileName = `auditTrail ${moment(new Date()).format(
          "DD MMM YYYY HH_mm_ss"
        )}.xlsx`;
        const url = window.URL.createObjectURL(data);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", getFileName);
        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(status)) {
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          let data = response.data;
          reader.onload = () => {
            data = JSON.parse(reader.result);
            resolve(Promise.reject(data));
          };
          reader.onerror = () => {
            reject(data);
          };
          reader.readAsText(data);
        })
          .then((err) => {
            dispatch(
              setError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          })
          .catch((err) => {
            dispatch(
              setError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          });
        dispatch(setLoading(false));
      }
    } catch {
      dispatch(setLoading(false));
    }
  };
