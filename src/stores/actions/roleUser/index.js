// roleUser Actions
// --------------------------------------------------------

import { isRegExp, set, split } from "lodash";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

/* eslint-disable space-before-function-paren */

const {
  serviceGetListOfficeUserRole,
  serviceGetUserRole,
  serviceGetRoleUserDetails,
  serviceGetListMenuUserRole,
} = require("utils/api");

export const SET_LOADING = "roleUser/SET_LOADING";
export const CLEAR_ERROR = "roleUser/CLEAR_ERROR";
export const SET_ERROR = "roleUser/SET_ERROR";
export const INIT_DATA = "roleUser/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "roleUser/SET_DOUBLE_SUBMIT";
export const SET_DATA = "roleUser/SET_DATA";
export const SET_DATA_USER_ROLE = "roleUser/SET_DATA_USER_ROLE";
export const SET_DATA_USER_DETAIL = "roleUser/SET_DATA_USER_DETAIL";
export const SET_SAVE_PAYLOAD = "roleUser/SET_SAVE_PAYLOAD";
export const SET_DATA_LIST_MENU = "roleUser/SET_DATA_LIST_MENU";
export const SET_HANDLE_CLEAR_ERROR = "roleUser/SET_HANDLE_CLEAR_ERROR";
export const SET_PAGES = "roleUser/SET_PAGES";
export const SET_MENU_SELECTED = "roleUser/SET_MENU_SELECTED";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});
export const setGetListOffice = (payload) => ({
  type: SET_DATA,
  payload,
});
export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});
export const setGetUserRole = (payload) => ({
  type: SET_DATA_USER_ROLE,
  payload,
});
export const setGetUserDetails = (payload) => ({
  type: SET_DATA_USER_DETAIL,
  payload,
});
export const setSavePayload = (payload) => ({
  type: SET_SAVE_PAYLOAD,
  payload,
});
export const setGetListMenu = (payload) => ({
  type: SET_DATA_LIST_MENU,
  payload,
});
export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});
export const setPages = (payload) => ({
  type: SET_PAGES,
  payload,
});
export const setMenuSelect = (payload) => ({
  type: SET_MENU_SELECTED,
  payload,
});

export const getDataListOffice = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceGetListOfficeUserRole(payload);

    dispatch(setLoading(false));
    if (res.status === 200) {
      const data = res.data.listData;

      const dataMap = data.map((item) => ({
        key: item.branchCode,
        branchTypeCode: `${item.branchName.split("-")[0]}`,
        branchName: item.branchName.split("-")[1],
      }));

      dataMap.sort((first, second) => {
        const labelA = first.branchName.replace(/\s/g, "").toUpperCase();
        const labelB = second.branchName.replace(/\s/g, "").toUpperCase();
        if (labelA < labelB) {
          return -1;
        }

        if (labelA > labelB) {
          return 1;
        }

        return 0;
      });

      const result = dataMap.map((item) => ({
        key: item.key,
        label: `${item.key} - ${
          item.branchName
        } - ${item.branchTypeCode.replace("-", "")}`,
      }));

      dispatch(setGetListOffice(result));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.data?.message));
  }
};
export const getDataUserRole = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceGetUserRole(payload);
    dispatch(setLoading(false));
    if (res.status === 200) {
      const data = res.data?.response;
      const dataMap = [];
      data.forEach((item) => {
        if (item.child.length > 0) {
          dataMap.push({
            id: item.groupId,
            name: item.groupName,
            child: item.child.map((child) => ({
              roleId: child.roleId,
              roleName: child.roleName,
              totalUser: child.totalUser,
              groupName: item.groupName,
              groupId: item.groupId,
              branchCode: payload?.branchCode,
            })),
          });
        }
      });
      dispatch(setGetUserRole(dataMap));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.data?.message));
  }
};
export const getDataUserDetails = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceGetRoleUserDetails(payload);
    dispatch(setLoading(false));
    if (res.status === 200) {
      dispatch(setGetUserDetails(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.data?.message));
  }
};

export const getDataListMenu = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceGetListMenuUserRole(payload);
    dispatch(setLoading(false));
    if (res.status === 200) {
      dispatch(setGetListMenu(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.data?.message));
  }
};
