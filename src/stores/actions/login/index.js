// login Actions
// --------------------------------------------------------
import { serviceLogin, serviceLogout } from "utils/api";
import {
  setLocalStorage,
  getLocalStorage,
  removeAllLocalStorage,
  removeLocalStorage,
  removeAllSessionStorage,
} from "utils/helpers";
import { pathnameCONFIG } from "../../../configuration";
import { checkOnline, setPopUpExpired } from "../errorGeneral";
import UserService from "services/UserService";
/* eslint-disable space-before-function-paren */
export const SET_LOADING = "login/SET_LOADING";
export const CLEAR_ERROR = "login/CLEAR_ERROR";
export const SET_ERROR = "login/SET_ERROR";
export const INIT_DATA = "login/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "login/SET_DOUBLE_SUBMIT";
export const SET_ACTIVE_STEP = "login/SET_ACTIVE_STEP";
export const SET_USER_LOGIN = "login/SET_USER_LOGIN";
export const SET_IS_OPEN_OTP = "login/SET_IS_OPEN_OTP";
export const SET_DATA_FORM_LOGIN = "login/SET_DATA_FORM_LOGIN";
export const SET_DATA_USER_LOGIN = "login/SET_DATA_USER_LOGIN";
export const SET_ERROR_ALREADY_LOGIN = "login/SET_ERROR_ALREADY_LOGIN";
export const SET_IS_BLOCKIR_ACCOUNT = "login/SET_IS_BLOCKIR_ACCOUNT";
export const SET_IPCHECKING = "login/SET_IPCHECKING";

export const setIpChecking = (payload) => ({
  type: SET_IPCHECKING,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setActiveStep = (payload) => ({
  type: SET_ACTIVE_STEP,
  payload,
});

export const setErrorAlreadyLogin = (payload) => ({
  type: SET_ERROR_ALREADY_LOGIN,
  payload,
});

export const setIsBlockirAccount = (payload) => ({
  type: SET_IS_BLOCKIR_ACCOUNT,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setUserLogin = (payload) => ({
  type: SET_USER_LOGIN,
  payload,
});

export const setDataFormLogin = (payload) => ({
  type: SET_DATA_FORM_LOGIN,
  payload,
});
export const setIsOpenOTP = (payload) => ({
  type: SET_IS_OPEN_OTP,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const clearError = () => ({
  type: CLEAR_ERROR,
});

export const setDataUserLogin = (payload) => ({
  type: SET_DATA_USER_LOGIN,
  payload,
});

// "0026" invalid ip
export const handleSubmit = (dataUser, history) => async (dispatch) => {
  await dispatch(checkOnline());
  dispatch(setDataFormLogin(dataUser));
  const payloadRequest = {
    ip: "string",
    username: dataUser.username,
    password: dataUser.password,
    channel: "OP",
  };
  try {
    dispatch(setLoading(true));
    const response = await serviceLogin(payloadRequest);
    dispatch(setLoading(false));
    const { data, headers, status } = response;
    if (status === 200) {
      console.warn(response, "ini respon");
      const dataUserResp = {
        ...data,
        // role: mappingRoleUser(response.data.role),
      };
      dispatch(setDataUserLogin(dataUserResp));
      setLocalStorage("portalUserLogin", JSON.stringify(dataUserResp));
      setLocalStorage("portalAccessToken", headers.access_token);
      history.push(pathnameCONFIG.DASHBOARD);
    } else {
      const error = data.errorCode;

      if (error === "0039") {
        dispatch(
          setErrorAlreadyLogin({ isError: true, message: data.engMessage })
        );
      } else if (error === "0025") {
        dispatch(setIsBlockirAccount(true));
        dispatch(
          setError({
            isError: true,
            message: response.data.message,
          })
        );
      } else if (data.message === "0025") {
        dispatch(setIsBlockirAccount(true));
        dispatch(
          setError({
            isError: true,
          })
        );
      } else if (error === "0051") {
        dispatch(setIpChecking(true));
      } else {
        dispatch(
          setError({
            isError: true,
            message: response.data.message,
          })
        );
      }

      console.warn("Eror di else");
    }
  } catch (e) {
    console.warn("Ini Catch Error", e);
  }
};

export const handleKeycloakSubmit = (dataUser, history) => async (dispatch) => {
  await dispatch(checkOnline());
  dispatch(setDataFormLogin(dataUser));
  const payloadRequest = {};
  try {
    dispatch(setLoading(true));
    const response = await serviceLogin(payloadRequest);
    dispatch(setLoading(false));
    const { data, headers, status } = response;
    if (status === 200) {
      console.warn(response, "ini respon");
      const dataUserResp = {
        ...data,
        // role: mappingRoleUser(response.data.role),
      };
      dispatch(setDataUserLogin(dataUserResp));
      setLocalStorage("portalUserLogin", JSON.stringify(dataUserResp));
      setLocalStorage("portalAccessToken", headers.access_token);
      history.push(pathnameCONFIG.DASHBOARD);
    } else {
      const error = data.errorCode;

      if (error === "0039") {
        dispatch(
          setErrorAlreadyLogin({ isError: true, message: data.engMessage })
        );
      } else if (error === "0025") {
        dispatch(setIsBlockirAccount(true));
        dispatch(
          setError({
            isError: true,
            message: response.data.message,
          })
        );
      } else if (data.message === "0025") {
        dispatch(setIsBlockirAccount(true));
        dispatch(
          setError({
            isError: true,
          })
        );
      } else if (error === "0051") {
        dispatch(setIpChecking(true));
      } else {
        dispatch(
          setError({
            isError: true,
            message: response.data.message,
          })
        );
      }

      console.warn("Eror di else");
    }
  } catch (e) {
    console.warn("Ini Catch Error", e);
  }
};

export const handleLogout = () => async (dispatch) => {
  console.log("logout 2");
  await UserService.doLogout();
  window.location.href =
    "http://34.101.193.33/auth/realms/portal/protocol/openid-connect/auth?client_id=portal-client&redirect_uri=http%3A%2F%2F34.101.193.33%3A3000%2F&state=aa3ed1e3-2a8b-4cf9-b7c1-74e48ac641e1&response_mode=fragment&response_type=code&scope=openid&nonce=25d99d98-57a9-432c-8054-532352f95bc1&code_challenge=KoUS4RBySLCsH1jSeRMbco0-TJ8J9uDKblsttYOT5Ic&code_challenge_method=S256";

  // UserService.doLogin();
  // const user = getLocalStorage("portalUserLogin");
  // const userLogin = user ? JSON.parse(user) : null;
  // const reqPayload = {
  //   username: userLogin?.username,
  // };

  // try {
  //   const response = await serviceLogout(reqPayload);
  //   const { status, data } = response;
  //   console.warn(data, status, "ini data logout");
  //   if (status === 200) {

  //     removeLocalStorage("portalAccessToken");
  //     removeLocalStorage("portalUserLogin");
  //     removeLocalStorage("tabportal");
  //     removeAllSessionStorage();
  //     window.location.replace("google.com");
  //   }
  // } catch (error) {
  //   console.warn(error);
  // }
};

export const handleLogoutDoubleTab = () => async (dispatch) => {
  try {
    console.log("logout 1");
    const user = getLocalStorage("portalUserLogin");
    const userLogin = user ? JSON.parse(user) : null;
    const reqPayload = {
      username: userLogin?.username,
    };

    const response = await serviceLogout(reqPayload);
    const { status } = response;

    if (status === 200) {
      dispatch(setPopUpExpired(true));
      removeLocalStorage("portalAccessToken");
      removeLocalStorage("portalUserLogin");
      removeLocalStorage("tabportal");
      removeAllSessionStorage();
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};

// const dummyLoginNotFirst = {
//   username: "sarkawi",
//   fullName: "Sarkawi",
//   role: "Super Admin",
//   branch: "KANTOR PUSAT",
//   ip: "188.166.252.153",
//   status: 1,
//   listPrivilege: [
//     {
//       privilegeId: 5,
//       privilegeMenu: "Blokir MB",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 6,
//       privilegeMenu: "Buka Blokir MB",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 7,
//       privilegeMenu: "Tutup Akun",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 1,
//       privilegeMenu: "Administrasi Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 2,
//       privilegeMenu: "Pengaturan Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 3,
//       privilegeMenu: "Pengaturan Cabang",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 4,
//       privilegeMenu: "Profile",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 12,
//       privilegeMenu: "Pengaturan Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 19,
//       privilegeMenu: "Tambah",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 20,
//       privilegeMenu: "Ubah",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 21,
//       privilegeMenu: "Hapus",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 37,
//       privilegeMenu: "Approval Config",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 15,
//       privilegeMenu: "Penutupan Akun",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 14,
//       privilegeMenu: "Pengaturan Blokir",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 13,
//       privilegeMenu: "Pengaturan Cabang",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 12,
//       privilegeMenu: "Pengaturan Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 11,
//       privilegeMenu: "Administrasi Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 31,
//       privilegeMenu: "Add Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 32,
//       privilegeMenu: "Edit Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 33,
//       privilegeMenu: "Delete Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 30,
//       privilegeMenu: "Pengaturan Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 34,
//       privilegeMenu: "Add Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 35,
//       privilegeMenu: "Edit Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 36,
//       privilegeMenu: "Delete Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 41,
//       privilegeMenu: "Approval Pengguna",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 54,
//       privilegeMenu: "Card Setting",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 46,
//       privilegeMenu: "Statistik Transaksi",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 51,
//       privilegeMenu: "Notifikasi",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 53,
//       privilegeMenu: "Merchant",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 55,
//       privilegeMenu: "Add Card",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 56,
//       privilegeMenu: "Edit Card",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 57,
//       privilegeMenu: "Delete Card",
//       privilegeChild: null,
//     },
//   ],
//   lastLoginDatetime: 1637649364000,
//   firstLogin: true,
// };

// const response = {
//   username: "munaroh",
//   fullName: "Munaroh",
//   role: "Super Admin",
//   branch: "KANTOR PUSAT",
//   ip: "188.166.252.153",
//   status: 1,
//   listPrivilege: [
//     { privilegeId: 5, privilegeMenu: "Blokir MB", privilegeChild: null },
//     { privilegeId: 6, privilegeMenu: "Buka Blokir MB", privilegeChild: null },
//     { privilegeId: 7, privilegeMenu: "Tutup Akun", privilegeChild: null },
//     {
//       privilegeId: 1,
//       privilegeMenu: "Administrasi Pengguna",
//       privilegeChild: null,
//     },
//     { privilegeId: 2, privilegeMenu: "Pengaturan Peran", privilegeChild: null },
//     {
//       privilegeId: 3,
//       privilegeMenu: "Pengaturan Cabang",
//       privilegeChild: null,
//     },
//     { privilegeId: 4, privilegeMenu: "Profile", privilegeChild: null },
//     {
//       privilegeId: 12,
//       privilegeMenu: "Pengaturan Peran",
//       privilegeChild: null,
//     },
//     { privilegeId: 19, privilegeMenu: "Tambah", privilegeChild: null },
//     { privilegeId: 20, privilegeMenu: "Ubah", privilegeChild: null },
//     { privilegeId: 21, privilegeMenu: "Hapus", privilegeChild: null },
//     { privilegeId: 37, privilegeMenu: "Approval Config", privilegeChild: null },
//     { privilegeId: 15, privilegeMenu: "Penutupan Akun", privilegeChild: null },
//     {
//       privilegeId: 14,
//       privilegeMenu: "Pengaturan Blokir",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 13,
//       privilegeMenu: "Pengaturan Cabang",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 12,
//       privilegeMenu: "Pengaturan Peran",
//       privilegeChild: null,
//     },
//     {
//       privilegeId: 11,
//       privilegeMenu: "Administrasi Pengguna",
//       privilegeChild: null,
//     },
//     { privilegeId: 31, privilegeMenu: "Add Peran", privilegeChild: null },
//     { privilegeId: 32, privilegeMenu: "Edit Peran", privilegeChild: null },
//     { privilegeId: 33, privilegeMenu: "Delete Peran", privilegeChild: null },
//     {
//       privilegeId: 30,
//       privilegeMenu: "Pengaturan Pengguna",
//       privilegeChild: null,
//     },
//     { privilegeId: 34, privilegeMenu: "Add Pengguna", privilegeChild: null },
//     { privilegeId: 35, privilegeMenu: "Edit Pengguna", privilegeChild: null },
//     { privilegeId: 36, privilegeMenu: "Delete Pengguna", privilegeChild: null },
//     {
//       privilegeId: 41,
//       privilegeMenu: "Approval Pengguna",
//       privilegeChild: null,
//     },
//     { privilegeId: 54, privilegeMenu: "Card Setting", privilegeChild: null },
//     {
//       privilegeId: 46,
//       privilegeMenu: "Statistik Transaksi",
//       privilegeChild: null,
//     },
//     { privilegeId: 51, privilegeMenu: "Notifikasi", privilegeChild: null },
//     { privilegeId: 53, privilegeMenu: "Merchant", privilegeChild: null },
//     { privilegeId: 55, privilegeMenu: "Add Card", privilegeChild: null },
//     { privilegeId: 56, privilegeMenu: "Edit Card", privilegeChild: null },
//     { privilegeId: 57, privilegeMenu: "Delete Card", privilegeChild: null },
//   ],
//   lastLoginDatetime: 1637653950000,
//   firstLogin: true,
// };

// const error = {
//   errorCode: "0039",
//   sourceSystem: "Digital Banking",
//   message: "Already login",
//   transactionId: null,
//   engMessage: "User Already Login",
//   idnMessage: "Pengguna sedang login",
// };
