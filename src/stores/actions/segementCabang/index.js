// segementCabang Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "segementCabang/SET_LOADING";
export const CLEAR_ERROR = "segementCabang/CLEAR_ERROR";
export const SET_ERROR = "segementCabang/SET_ERROR";
export const INIT_DATA = "segementCabang/INIT_DATA";
export const SET_DATA = "segementCabang/SET_DATA ";
export const SET_DATA_EDIT = "segementCabang/SET_DATA_EDIT";
export const SET_DATA_DETAIL = "segementCabang/SET_DATA_DETAIL";
export const CLEAR_DATA_DETAIL = "segementCabang/CLEAR_DATA_DETAIL";
export const SUCCESS_CONFIRMATION = "segementCabang/SUCCESS_CONFIRMATION";
export const SET_DATA_BRANCH = "segementCabang/SET_DATA_BRANCH";
export const SET_DOUBLE_SUBMIT = "segementCabang/SET_DOUBLE_SUBMIT";
export const SET_LOADING_EXCUTE = "segementCabang/SET_LOADING_EXCUTE";
const {
  serviceRequestGetSegmentCabang,
  serviceRequestAddSegmentCabang,
  serviceRequestGetBranchWithoutPagination,
  serviceRequestGetBranchDetail,
} = require("utils/api");

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setTypeDataBranch = (payload) => ({
  type: SET_DATA_BRANCH,
  payload,
});

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const setPopUp = (payload) => ({
  type: SUCCESS_CONFIRMATION,
  payload,
});

export const setDetil = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setEditData = (payload) => ({
  type: SET_DATA_EDIT,
  payload,
});
export const clearDataDetail = () => ({
  type: CLEAR_DATA_DETAIL,
});

export const getDataSegmentCabnag = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestGetSegmentCabang(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setTypeData(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};

export const addDataSegmentCabnag =
  (payload, handleSave) => async (dispatch) => {
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.SEGMENT_BRANCH,
          code: payload?.segmentBranch?.code,
          id: null,
          name: payload?.segmentBranch?.name,
          validate:
            payload?.segmentBranch?.id === null ||
            payload?.segmentBranch?.id === undefined,
          loader: setLoading,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoadingExcute(true));

              const res = await serviceRequestAddSegmentCabang(payload);
              dispatch(setLoadingExcute(false));

              if (res.status === 200) {
                dispatch(setPopUp(true));
              } else {
                dispatch(handleErrorBE(res));
              }
            } catch (err) {
              dispatch(setHandlingError(err?.message));
            }
          },
          onError() {
            handleSave();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const getBranchWithoutPagination = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestGetBranchWithoutPagination(payload);

    dispatch(setLoading(false));

    console.warn("resWithoutPagination:", res);

    if (res.status === 200) {
      const resultMapping = res?.data?.branch?.map((item) => ({
        ...item,
        checked: false,
      }));
      dispatch(setTypeDataBranch(resultMapping));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};

export const getDataSegmentCabangDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestGetBranchDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDetil(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }

    console.warn("res:", res);
  } catch (err) {
    dispatch(setHandlingError(err?.message));
  }
};
