// privilrgeApproval Actions
// --------------------------------------------------------

import {
  serviceApprovalConfig,
  ServiceApprovalConfigDetail,
  ServiceConfigApprove,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { act } from "@testing-library/react";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "privilrgeApproval/SET_LOADING";
export const CLEAR_ERROR = "privilrgeApproval/CLEAR_ERROR";
export const SET_ERROR = "privilrgeApproval/SET_ERROR";
export const INIT_DATA = "privilrgeApproval/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "privilrgeApproval/SET_DOUBLE_SUBMIT";
export const SET_DETAIL_DATA = "privilrgeApproval/SET_DETAIL_DATA";
export const SET_LOADING_APPROVE = "privilrgeApproval/SET_LOADING_APPROVE";
export const SET_SUCCESS_APPROVE = "privilrgeApproval/SET_SUCCESS_APPROVE";
export const SET_SUCCESS_REJECT = "privilrgeApproval/SET_SUCCESS_REJECT";
export const SET_CLEAR = "privilrgeApproval/SET_CLEAR";
export const SET_DATA_ID_BULK = "privilrgeApproval/SET_DATA_ID_BULK";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitalData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setClearError = (payload) => ({
  type: CLEAR_ERROR,
});

export const setDetailData = (payload) => ({
  type: SET_DETAIL_DATA,
  payload,
});

export const setLoadingApprove = (payload) => ({
  type: SET_LOADING_APPROVE,
  payload,
});

export const setSuccessApprove = (payload) => ({
  type: SET_SUCCESS_APPROVE,
  payload,
});

export const setSuccessReject = (payload) => ({
  type: SET_SUCCESS_REJECT,
  payload,
});
export const setDataIdBulk = (payload) => ({
  type: SET_DATA_ID_BULK,
  payload,
});

export const setClear = (payload) => ({
  type: SET_CLEAR,
});

export const handlePrivilegeApproval =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceApprovalConfig(payload);
      if (response?.status === 200) {
        dispatch(setInitalData(response.data));
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setInitalData([]));
        dispatch(setLoading(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setInitalData([]));
      dispatch(setLoading(false));
      dispatch(setError(e?.message));
    }
  };

export const handlePrivilegeApprovalDetail =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await ServiceApprovalConfigDetail(payload);

      if (response.status === 200) {
        dispatch(
          setDetailData(
            response.data.map((elm) => ({
              ...elm,
              newData: JSON.parse(elm?.newData),
              oldData: JSON.parse(elm?.oldData),
            }))
          )
        );
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setDetailData([]));
        dispatch(setLoading(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setDetailData([]));
      dispatch(setLoading(false));
      dispatch(setError(e?.message));
    }
  };

export const handleApprove = (payload) => async (dispatch) => {
  dispatch(checkOnline());

  const { data, comment, approvalStatus } = payload;

  const filterChecklist = (data ?? []).map((elm) => ({
    approvalComment: comment || null,
    approvalStatus,
    idApproval: elm?.id,
    typeApproval: "PORTAL_GROUP",
  }));

  const requestPayload = {
    approvalList: filterChecklist,
  };

  try {
    dispatch(setLoadingApprove(true));
    const response = await ServiceConfigApprove(requestPayload);
    dispatch(setLoadingApprove(false));

    if (response.status === 200) {
      if (approvalStatus === 1) {
        dispatch(
          setSuccessApprove({
            isSuccess: true,
            releaser: response.data.isToReleaserList,
          })
        );
      } else if (approvalStatus === 2) {
        dispatch(setSuccessReject(true));
      }
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoadingApprove(false));
      dispatch(handleErrorBE(response));
      dispatch(setClear());
    }
  } catch (e) {
    dispatch(setError(e.message));
    setSuccessApprove({ isSuccess: false, data: null });
    dispatch(setClear());
  }
};

export const handleBulkApprove =
  (payload, comment) => async (dispatch, getState) => {
    dispatch(checkOnline());

    const { action } = payload;
    const idBulk = getState()?.privilrgeApproval?.idBulk;
    const data = idBulk?.map((item) => ({
      approvalComment: comment ?? null,
      approvalStatus: action !== 1 ? 2 : 1,
      idApproval: item?.idApproval,
      typeApproval: "PORTAL_GROUP",
    }));

    const requestPayload = {
      approvalList: data,
    };

    try {
      dispatch(setLoadingApprove(true));
      const response = await ServiceConfigApprove(requestPayload);
      dispatch(setLoadingApprove(false));

      if (response.status === 200) {
        if (action === 1) {
          dispatch(
            setSuccessApprove({
              isSuccess: true,
              releaser: response.data.isToReleaserList,
            })
          );
        } else if (action === 2) {
          dispatch(setSuccessReject(true));
        }
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoadingApprove(false));
        dispatch(handleErrorBE(response));
        dispatch(setClear());
      }
    } catch (e) {
      dispatch(setError(e.message));
      setSuccessApprove({ isSuccess: false, data: null });
      dispatch(setClear());
    }
  };
