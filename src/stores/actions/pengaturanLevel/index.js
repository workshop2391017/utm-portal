// pengaturanLevel Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

const {
  serviceRequestApprovelMatrixFinanceGetGroupSchemaList,
  serviceRequestApprovalMatrixFinanceGetCurrencye,
  serviceRequestAddLevelSettings,
  requestDeleteLimit,
  serviceGetInfoCurrency,
  serviceValidateCodeAndName,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "pengaturanLevel/SET_LOADING";
export const SET_DELETE_LOADING = "pengaturanLevel/SET_DELETE_LOADING";
export const CLEAR_ERROR = "pengaturanLevel/CLEAR_ERROR";
export const SET_ERROR = "pengaturanLevel/SET_ERROR";
export const INIT_DATA = "pengaturanLevel/INIT_DATA";
export const DATA_SIDEBAR = "pengaturanLevel/DATA_SIDEBAR";
export const SET_DOUBLE_SUBMIT = "pengaturanLevel/SET_DOUBLE_SUBMIT";
export const DATA_EDIT = "pengaturanLevel/DATA_EDIT";
export const DATA_CURRENCY = "pengaturanLevel/DATA_CURRENCY";
export const OPEN_SUCCESS = "pengaturanLevel/OPEN_SUCCESS";
export const SET_PAGES = "pengaturanLevel/SET_PAGES";
export const SET_IDCOMPANY = "pengaturanLevel/SET_IDCOMPANY";
export const SET_IS_VALID_NAME = "pengaturanLevel/SET_IS_VALID_NAME";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDeleteLoading = (payload) => ({
  type: SET_DELETE_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeSideBar = (payload) => ({
  type: DATA_SIDEBAR,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeEdit = (payload) => ({
  type: DATA_EDIT,
  payload,
});

export const setTypeCurrency = (payload) => ({
  type: DATA_CURRENCY,
  payload,
});

export const setTypeOpenSuccess = (payload) => ({
  type: OPEN_SUCCESS,
  payload,
});
export const setPages = (payload) => ({
  type: SET_PAGES,
  payload,
});
export const setIdCompany = (payload) => ({
  type: SET_IDCOMPANY,
  payload,
});

export const setIsNameValid = (payload) => ({
  type: SET_IS_VALID_NAME,
  payload,
});

export const actionValidateName = (payload) => async (dispatch, getState) => {
  const { idPerusahaan } = getState().managmentCompany;
  try {
    const res = await serviceValidateCodeAndName({
      ...payload,
      corporateProfileId: idPerusahaan || payload?.corporateProfileId,
    });

    if (res.status === 200) {
      dispatch(setIsNameValid(res.data));
    } else {
      throw res;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const getDataSidebarSettingLevel =
  (payload) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const res = await serviceRequestApprovelMatrixFinanceGetGroupSchemaList(
        payload
      );
      dispatch(setLoading(false));

      if (res.status === 200) {
        dispatch(setTypeSideBar(res?.data));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setTypeError(error.message));
    }
  };

export const getAllCurrency = (payload) => async (dispatch) => {
  try {
    const res = await serviceGetInfoCurrency(payload);

    if (res.status === 200) {
      dispatch(setTypeCurrency(res?.data?.currencyList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeError(error.message));
  }
};

export const AddExcuteSettingLevel =
  (payload, setConfirmationPopup) => async (dispatch, getState) => {
    dispatch(setLoading(true));
    const { idPerusahaan } = getState().managmentCompany;
    try {
      const res = await serviceRequestAddLevelSettings({
        ...payload,
        corporateProfileId: idPerusahaan || payload?.corporateProfileId,
      });
      dispatch(setLoading(false));
      setConfirmationPopup(false);

      if (res.status === 200) {
        dispatch(setTypeOpenSuccess(true));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setTypeError(error.message));
    }
  };

export const AddDeleteSettingLevel =
  (payload, setConfirmationPopup) => async (dispatch, getState) => {
    dispatch(setDeleteLoading(true));
    const { idPerusahaan } = getState().managmentCompany;
    try {
      const res = await requestDeleteLimit({
        ...payload,
        corporateProfileId: idPerusahaan || payload?.corporateProfileId,
      });
      dispatch(setDeleteLoading(false));
      setConfirmationPopup(false);

      if (res.status === 200) {
        dispatch(setTypeOpenSuccess(true));
      } else {
        dispatch(handleErrorBE(res, false));
      }
    } catch (error) {
      dispatch(setTypeError(error.message));
    }
  };
