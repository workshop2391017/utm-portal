// validateTaskPortal Actions
// --------------------------------------------------------

import { hanldeValidationDataLms, serviceValidateTaskPortal } from "utils/api";
import { handleErrorBE, setErrorAlreadyUsed } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING_VALIDTE = "validateTaskPortal/SET_LOADING_VALIDTE";
export const CLEAR_ERROR = "validateTaskPortal/CLEAR_ERROR";
export const SET_ERROR = "validateTaskPortal/SET_ERROR";
export const INIT_DATA = "validateTaskPortal/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "validateTaskPortal/SET_DOUBLE_SUBMIT";

export const setLoadingValidate = (payload) => ({
  type: SET_LOADING_VALIDTE,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

// example payload
// {
//   "code": "string",
//   "id": 0,
//   "menuName": "string",
//   "name": "string"
// }

// jika edit kirim id & menuName
// jika add name & code & menuName
// jika tidak ada code-nya kirim name & menuName

// menuName = validateTaskConstant [export dari validateTaskPortal/constantValidateTask.js]
// sesuaikan dengan menu nya

// sementara test audit
// reminder: jangan lupa dibalikin guys kalo update ini :D
const DISABLED_VALIDATE = false;

export const validateTask =
  (payload, callback, options) => async (dispatch, getState) => {
    if (DISABLED_VALIDATE) {
      callback?.onContinue();
      return;
    }

    const state = getState()?.validateTaskPortal;

    // sementara kalo mau di skip validasiTask-nya
    // const skip = true;

    // test
    // ketika loading
    // tidak bisa di klik berkalikali
    if (state?.isDoubleSubmit) return false;

    // must be boolean
    // typeof payload?.validate === "boolean" && !payload?.validate || skip
    if (typeof payload?.validate === "boolean" && !payload?.validate) {
      callback?.onContinue(); // exception
      return;
    }

    const { validate, loader, ...rest } = payload;

    const loaderValidate = payload?.loader ? loader : setLoadingValidate;
    dispatch(loaderValidate(true));
    dispatch(setDoubleSubmit(true));

    try {
      const response = await serviceValidateTaskPortal(rest);
      dispatch(setDoubleSubmit(false));
      dispatch(loaderValidate(false));

      if (response.status === 200) {
        callback?.onContinue();
      } else if (response?.data?.errorCode === "0403") {
        if (callback?.onError) callback?.onError(); // callback exception
        dispatch(
          setErrorAlreadyUsed({
            isError: true,
            message: response?.data?.engMessage,
            code: response?.data?.errorCode,
            redirect: options?.redirect,
          })
        );
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      dispatch(loaderValidate(false));
      dispatch(setDoubleSubmit(false));
    }
  };

export const validateTaskForLimitPackage =
  (payload, callback, options) => async (dispatch, getState) => {
    if (DISABLED_VALIDATE) {
      callback?.onContinue();
      return;
    }

    const state = getState()?.validateTaskPortal;

    // sementara kalo mau di skip validasiTask-nya
    // const skip = true;

    // test
    // ketika loading
    // tidak bisa di klik berkalikali
    if (state?.isDoubleSubmit) return false;

    // must be boolean
    // typeof payload?.validate === "boolean" && !payload?.validate || skip
    if (typeof payload?.validate === "boolean" && !payload?.validate) {
      callback?.onContinue(); // exception
      return;
    }

    const { validate, loader, ...rest } = payload;

    const loaderValidate = payload?.loader ? loader : setLoadingValidate;
    dispatch(loaderValidate(true));
    dispatch(setDoubleSubmit(true));

    try {
      const response = await hanldeValidationDataLms(rest);
      dispatch(setDoubleSubmit(false));
      dispatch(loaderValidate(false));

      if (response.status === 200) {
        callback?.onContinue();
      } else if (response?.data?.errorCode === "0403") {
        if (callback?.onError) callback?.onError(); // callback exception
        dispatch(
          setErrorAlreadyUsed({
            isError: true,
            message: response?.data?.engMessage,
            code: response?.data?.errorCode,
            redirect: options?.redirect,
          })
        );
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      dispatch(loaderValidate(false));
      dispatch(setDoubleSubmit(false));
    }
  };
