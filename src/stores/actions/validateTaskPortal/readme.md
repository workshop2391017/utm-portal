## validateTaskPortal This Function

// jika edit kirim id & menuName
// jika add name & code & menuName
// jika tidak ada code-nya kirim name & menuName

// menuName = validateTaskConstant [export dari validateTaskPortal/constantValidateTask.js]
// sesuaikan dengan menu nya

```
const payload = {
  "code": "string",
  "id": 0,
  "menuName": "string",
  "name": "string"
  "validate": lakukan validate atau lewati validate, true/false
}

   dispatch(
      validateTask(payload
        {
          onContinue() {
            // do something

            // ex. handle save, delete, or continue open modal edit or redirect to edit page etc
          },
          onError() {
            // do something

            // handle callback error
            // popups callback error sudah dihandle
            // ex. clear form input, redirect to list, etc
          },
        },
        {
         // handle error popups general butuh redirect atau tidak
          redirect: false,
        }
      )
    );
```
