// progressBarAction Actions
// --------------------------------------------------------

export const SET_LOADING = "menus/SET_LOADING";
export const CLEAR_ERROR = "menus/CLEAR_ERROR";
export const SET_ERROR = "menus/SET_ERROR";
export const INIT_DATA = "menus/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "menus/SET_DOUBLE_SUBMIT";
export const SET_MENU_ID = "menus/SET_MENU_ID";
// export const SET_ACTIVE_PATH = "menus/SET_ACTIVE_PATH";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setMenuId = (payload) => ({
  type: SET_MENU_ID,
  payload,
});

// export const setActivePath = (payload) => ({
//   type: SET_ACTIVE_PATH,
//   payload,
// });
