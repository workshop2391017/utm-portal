// cutOffTime Actions
// --------------------------------------------------------

import {
  serviceCutOffTimeGet,
  serviceCutOffTimeSave,
  serviceRequestGetCurrency,
} from "utils/api";
import { handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "cutOffTime/SET_LOADING";
export const CLEAR_ERROR = "cutOffTime/CLEAR_ERROR";
export const SET_ERROR = "cutOffTime/SET_ERROR";
export const SET_SUCCESS = "cutOffTime/SET_SUCCESS";
export const SET_DATA = "cutOffTime/SET_DATA";
export const SET_CURRENCIES = "cutOffTime/SET_CURRENCIES";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});

export const setCurrencies = (payload) => ({
  type: SET_CURRENCIES,
  payload,
});

export const actionCutOffTimeGet = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceCutOffTimeGet(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setData(response.data.cutOffTime));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionCutOffTimeGetCurrencies = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceRequestGetCurrency(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setCurrencies(response.data.currencyList));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionCutOffTimeSave = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceCutOffTimeSave(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setSuccess(true));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};
