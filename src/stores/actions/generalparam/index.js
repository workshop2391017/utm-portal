// login Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceGeneralParameter,
  serviceUpdateGeneralParameter,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";
import { validateTask } from "../validateTaskPortal";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "generalparam/SET_LOADING";
export const CLEAR_ERROR = "generalparam/CLEAR_ERROR";
export const SET_ERROR = "generalparam/SET_ERROR";
export const SET_GENERAL_PARAMS = "generalparam/SET_GENERAL_PARAMS";
export const SET_GENERALPARAMS_FORM = "generalparam/SET_GENERALPARAMS_FORM";
export const SET_EDIT_GENERALPARAMS = "generalparam/SET_EDIT_GENERALPARAMS";
export const SET_LOADING_SUBMIT = "approvalconfig/SET_LOADING_SUBMIT";
export const SET_HANDLE_ERROR = "approvalconfig/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "approvalconfig/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setGeneralParams = (payload) => ({
  type: SET_GENERAL_PARAMS,
  payload,
});

export const setGeneralParamsForm = (payload) => ({
  type: SET_GENERALPARAMS_FORM,
  payload,
});

export const setEditgeneralParams = (payload) => ({
  type: SET_EDIT_GENERALPARAMS,
  payload,
});

export const setLoadingSubmit = (payload) => ({
  type: SET_LOADING_SUBMIT,
  payload,
});

export const handleGetGeneralParams = (payload) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoading(true));
    const response = await serviceGeneralParameter(
      payload,
      privilegeCONFIG.GENERAL_PARAMETER.LIST
    );
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setGeneralParams(response.data?.systemParameterListPage ?? []));
    } else if (response.status === 404) {
      dispatch(setGeneralParams([]));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};

export const submitRequestUpdateParameter =
  (payload, { handleSuccess, handleNext }) =>
  async (dispatch) => {
    const requestPayload = {
      idParameter: payload.idParameter,
      module: payload?.namaModule,
      name: payload?.kode,
      description: payload?.deskripsi,
      isEncrypted: payload?.enkripsiValue === "Yes",
      requestComment: payload?.requestComment,
      value: payload?.value,
    };

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.GENERAL_PARAMETER,
          name: null,
          code: null,
          id: payload.idParameter,
          loader: setLoadingSubmit,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoadingSubmit(true));
              const response = await serviceUpdateGeneralParameter(
                requestPayload,
                privilegeCONFIG.GENERAL_PARAMETER.LIST
              );
              dispatch(setLoadingSubmit(false));
              if (response.status === 200) {
                handleSuccess();
                handleNext();
              } else {
                dispatch(handleErrorBE(response, false));
              }

              return;
            } catch (e) {
              dispatch(setLoading(false));
              dispatch(setHandleError(e.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };
