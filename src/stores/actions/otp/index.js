// otp Actions
// --------------------------------------------------------

import { serviceValidateOTP } from "utils/api";
import { encryptOTP } from "utils/encrypt";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "otp/SET_LOADING";
export const CLEAR_ERROR = "otp/CLEAR_ERROR";
export const SET_ERROR = "otp/SET_ERROR";
export const INIT_DATA = "otp/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "otp/SET_DOUBLE_SUBMIT";
export const SET_TIMER_OTP = "otp/SET_TIMER_OTP";
export const SET_DATA_OTP = "otp/SET_DATA_OTP";
export const SET_SECRET_TOKEN = "otp/SET_SECRET_TOKEN";
export const SET_DATA_FORM_OTP = "otp/SET_DATA_FORM_OTP";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTimer = (payload) => ({ type: SET_TIMER_OTP, payload });

export const setDataOTP = (payload) => ({ type: SET_DATA_OTP, payload });

export const setDataFormOTP = (payload) => ({
  type: SET_DATA_FORM_OTP,
  payload,
});

export const setSecretToken = (payload) => ({
  type: SET_SECRET_TOKEN,
  payload,
});

// export const handleSubmitValidateOTP =
//   (dataOTP) => async (dispatch, getState) => {
//     const { oneTimePasswordRequestId } = getState().otp.dataOTP;
//     const secretToken = getState().otp.secretToken;
//     console.warn(
//       dataOTP,
//       secretToken,
//       oneTimePasswordRequestId,
//       "ini data otp"
//     );
//     dispatch(setDataFormOTP(dataOTP));
//     try {
//       const payload = {
//         otp: encryptOTP(dataOTP.reqOtp),
//         secretToken,
//         oneTimePasswordRequestId,
//         channel: "MB",
//       };
//       const dataOtpAPI = await serviceValidateOTP(payload);
//       console.warn(dataOtpAPI);
//     } catch (error) {
//       console.warn(error, JSON.stringify({ ...error }));
//     }
//   };
