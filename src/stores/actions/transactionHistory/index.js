export const SET_SELECTED_DATA = "transactionhistory/setSelectedData";
export const SET_SELECTED_STEP_DATA = "transactionhistory/setSelectedStepData";
export const SET_DATA_CHANNEL = "transactionhistory/setDataChannel";
export const SET_DATA_MDW_LEFT = "transactionhistory/setDataMdwLeft";
export const SET_DATA_MDW_RIGHT = "transactionhistory/setDataMdwRight";
export const SET_DATA_CORE = "transactionhistory/setDataCore";
export const SET_DATA_SURROUNDING = "transactionhistory/setDataSurrounding";
export const SET_DATA_EDGES = "transactionhistory/setDataEdges";
export const SET_DATA_NODES = "transactionhistory/setDataNodes";
export const RESET_TRANSACTION_HISTORY =
  "transactionhistory/resetTransactionHistory";

import {
  dataFlowDetailTransaction,
  dataNodesFlowTransaction,
} from "containers/BN/TransactionManagement/TransactionHistory/index.dummy";

export const setSelectedData = (payload) => ({
  type: SET_SELECTED_DATA,
  payload,
});

export const setSelectedStepData = (payload) => ({
  type: SET_SELECTED_STEP_DATA,
  payload,
});

export const filterDataFlow = () => async (dispatch) => {
  const data = dataFlowDetailTransaction;
  const dataNodes = dataNodesFlowTransaction;
  const initEdges = [];
  const initNodes = [];

  const filterDataChannel = data.filter(
    (item) => item["from"] == "channel" || item["to"] == "channel"
  );
  const filterDataMiddlewareLeft = data.filter(
    (item) =>
      (item["from"] == "channel" && item["to"] == "middleware") ||
      (item["from"] == "middleware" && item["to"] == "channel")
  );

  const filterDataMiddlewareRight = data.filter(
    (item) =>
      (item["from"] !== "channel" && item["to"] == "middleware") ||
      (item["from"] == "middleware" && item["to"] !== "channel")
  );

  const filterDataCore = data.filter(
    (item) => item["from"] == "core" || item["to"] == "core"
  );

  const filterDataSurrounding = data.filter(
    (item) =>
      item["from"] != "channel" &&
      item["from"] != "core" &&
      item["to"] != "channel" &&
      item["to"] != "core"
  );

  let yOffset = 0;

  const newDataNodes = dataNodes.map((item, index) => {
    let newNodes = {};
    let yPosition = 0;
    if (item.nodeType == "channel") {
      newNodes = {
        id: item.nodeName,
        type: item.nodeType,
        data: {
          label: item.nodeName,
        },
        position: { x: 0, y: 100 },
      };
    } else if (item.nodeType == "middleware") {
      newNodes = {
        id: item.nodeName,
        type: item.nodeType,
        data: {
          label: item.nodeName,
        },
        position: { x: 250, y: 40 },
      };
    } else {
      yPosition = yOffset;
      newNodes = {
        id: item.nodeName,
        type: item.nodeType,
        data: {
          label: item.nodeName,
        },
        position: { x: 500, y: yPosition },
      };
      yOffset += 190;
    }
    return newNodes;
  });

  console.log("Data Nodes : ", newDataNodes);

  data.map((item, index) => {
    const newObj = {
      id: `flow${(index + 1).toString()}`,
      source: item.from,
      target: item.to,
      label: item.no.toString(),
      sourceHandle: item.no.toString(),
      targetHandle: item.no.toString(),
      type: "custom",
      data: {
        label: item.no.toString(),
      },
      animated: true,
    };
    initEdges.push(newObj);
  });

  // console.log("data new nodes : ", initEdges);

  await dispatch({
    type: SET_DATA_NODES,
    payload: newDataNodes,
  });

  await dispatch({
    type: SET_DATA_EDGES,
    payload: initEdges,
  });

  await dispatch({
    type: SET_DATA_CHANNEL,
    payload: filterDataChannel,
  });

  await dispatch({
    type: SET_DATA_MDW_LEFT,
    payload: filterDataMiddlewareLeft,
  });
  await dispatch({
    type: SET_DATA_MDW_RIGHT,
    payload: filterDataMiddlewareRight,
  });
  await dispatch({
    type: SET_DATA_CORE,
    payload: filterDataCore,
  });
  await dispatch({
    type: SET_DATA_SURROUNDING,
    payload: filterDataSurrounding,
  });
};
