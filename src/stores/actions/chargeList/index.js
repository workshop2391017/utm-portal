// chargeList Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */

import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { serviceChargeList, serviceAddChargesList } from "utils/api";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

// const { serviceChargeList, serviceAddChargesList } = require("utils/api");

export const SET_LOADING = "chargeList/SET_LOADING";
export const CLEAR_ERROR = "chargeList/CLEAR_ERROR";
export const SET_ERROR = "chargeList/SET_ERROR";
export const INIT_DATA = "chargeList/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "chargeList/SET_DOUBLE_SUBMIT";
export const SET_DATA = "chargeList/SET_DATA";
export const SET_DATA_EDIT = "chargesList/SET_DATA_EDIT";
export const CLEAR_DATA_DETAIL = "chargesList/CLEAR_DATA_DETAIL";
export const SUCCESS_CONFIRMATION = "chargesList/SUCCESS_CONFIRMATION";
export const SET_HANDLE_CLEAR_ERROR = "chargesList/SET_HANDLE_CLEAR_ERROR";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setGetChargeList = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setPopUpSuccess = (payload) => ({
  type: SUCCESS_CONFIRMATION,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const getDataChargeList = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceChargeList(payload);
    dispatch(setLoading(false));
    if (res.status === 200) {
      dispatch(setGetChargeList(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandlingError(err?.data?.message));
  }
};

export const addDataChargeList = (payload, handleNext) => async (dispatch) => {
  dispatch(
    validateTask(
      {
        menuName: validateTaskConstant.CHARGES_LIST,
        code: null,
        id: null,
        name: payload?.chargesList?.name,
        validate:
          payload?.chargesList?.id === null ||
          payload?.chargesList?.id === undefined, // karena udah divalidasi pas klik button edit
        loader: setLoading,
      },
      {
        async onContinue() {
          try {
            dispatch(setLoading(true));
            const res = await serviceAddChargesList(payload);
            handleNext();
            dispatch(setLoading(false));
            if (res.status === 200) {
              dispatch(setPopUpSuccess(true));
            } else {
              dispatch(handleErrorBE(res, false));
            }
          } catch (err) {
            dispatch(setHandlingError(err?.data?.message));
          }
        },
        onError() {
          // handle callback error
          // ! popups callback error sudah dihandle !
          // ex. clear form, redirect, etc
          handleNext();
        },
      },
      {
        redirect: false,
      }
    )
  );
};
