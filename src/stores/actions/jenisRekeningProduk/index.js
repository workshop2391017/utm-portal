// login Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

import {
  editGlobalLimitPackage,
  serviceAddLimitPackage,
  serviceCurrencyMatrix,
  serviceLimitPackage,
  serviceLimitPackageDetail,
  serviceRequestAddRekeningProduk,
  serviceRequestGetAccountType,
  serviceRequestGetProductAccountType,
  //   serviceRequestGetProdukRekening,
  serviceRequestGetRekeningProduk,
  serviceRequestProductDetailGet,
  serviceRequestProductGet,
  serviceRequestProductTypeGet,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "jenisProductRekening/SET_LOADING";
export const SET_DATA = "jenisProductRekening/SET_DATA";
export const SET_ERROR = "jenisProductRekening/SET_ERROR";
export const SET_DETAIL = "jenisProductRekening/SET_DETAIL";
export const SET_ID = "jenisProductRekening/SET_ID";
export const SET_LOADING_EXECUTE = "jenisProductRekening/SET_LOADING_EXECUTE";
export const SET_SUCCESS_EXECUTE = "jenisProductRekening/SET_SUCCESS_EXECUTE";
export const SET_CLEAR = "jenisProductRekening/SET_CLEAR";
export const SET_PRODUCT_REKENING = "jenisProductRekening/SET_PRODUCT_REKENING";
export const SET_ACCOUNT_TYPE = "jenisProductRekening/SET_ACCOUNT_TYPE";

export const SET_PRODUCT_TYPE = "jenisProductRekening/SET_PRODUCT_TYPE";
export const SET_LOADING_PRODUCT_ACCOUNTTYPE =
  "jenisProductRekening/SET_LOADING_PRODUCT_ACCOUNTTYPE";
export const SET_PRODUCT_ACCOUNTTYPE =
  "jenisProductRekening/SET_PRODUCT_ACCOUNTTYPE";

// global error
export const SET_HANDLE_ERROR = "jenisProductRekening/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR =
  "jenisProductRekening/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setId = (payload) => ({
  type: SET_ID,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setSucessExecute = (payload) => ({
  type: SET_SUCCESS_EXECUTE,
  payload,
});

export const setClear = () => ({
  type: SET_CLEAR,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setProductRekening = (payload) => ({
  type: SET_PRODUCT_REKENING,
  payload,
});

export const setAccountType = (payload) => ({
  type: SET_ACCOUNT_TYPE,
  payload,
});

export const setLoadingProductAccounttype = (payload) => ({
  type: SET_LOADING_PRODUCT_ACCOUNTTYPE,
  payload,
});

export const setProductAccountType = (payload) => ({
  type: SET_PRODUCT_ACCOUNTTYPE,
  payload,
});

export const handleGetRekeningProduk = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoading(true));
  try {
    const response = await serviceRequestGetRekeningProduk(payload);
    if (response.status === 200) {
      dispatch(setData(response.data));
      dispatch(setLoading(false));
    } else {
      dispatch(handleErrorBE(response));
      dispatch(setLoading(false));
    }
  } catch (error) {
    dispatch(setError(error.message));
    dispatch(setLoading(false));
  }
};

export const handleDataDetailJenisRekeningProduk =
  (productDetailGet) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceRequestProductDetailGet(productDetailGet);

      if (response.status === 200) {
        dispatch(setDetail(response?.data));
        dispatch(setLoading(false));
      } else {
        dispatch(setDetail({}));
        dispatch(handleErrorBE(response));
        dispatch(setLoading(false));
      }
    } catch (err) {
      dispatch(setError(err.message));
      dispatch(setLoading(false));
    }
  };

export const handleGetJenisProduk =
  (productGet) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));

    try {
      const response = await serviceRequestProductGet(productGet);

      if (response.status === 200) {
        dispatch(
          setProductRekening({
            ...response?.data,
            accountProducts: response?.data?.accountProductDtoList,
          })
        );
        dispatch(setLoading(false));
      } else {
        dispatch(handleErrorBE(response));
        dispatch(setLoading(false));
      }
    } catch (err) {
      dispatch(setError(err.message));
      dispatch(setLoading(false));
    }
  };

export const handleAddJenisRekeningProduk =
  (payload, payloadValidation, handleNext) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PRODUCT_TYPE_DETAIL,
          ...payloadValidation,
          loader: setLoadingExecute,
        },
        {
          async onContinue() {
            dispatch(setLoadingExecute(true));

            try {
              const response = await serviceRequestAddRekeningProduk(payload);
              handleNext();
              dispatch(setLoadingExecute(false));
              if (response.status === 200) {
                dispatch(setSucessExecute(response.data));
              } else {
                dispatch(handleErrorBE(response));
                dispatch(setLoadingExecute(false));
              }
            } catch (err) {
              dispatch(setError(err.message));
              dispatch(setLoadingExecute(false));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const handleProductType = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingProductAccounttype(true));

  try {
    const response = await serviceRequestGetProductAccountType(payload);

    if (response.status === 200) {
      // "DD", "S", "LN", "CD"
      const productAccountType = ["SA", "CA", "LN", "CD"];

      const data = {};
      response.data.accountTypeList
        .filter((e) => productAccountType.includes(e))
        .forEach((elm) => {
          switch (elm) {
            case "SA":
              data[elm] = {
                code: elm,
                name: "Saving Account",
                children: [],
              };
              break;
            case "CA":
              data[elm] = {
                code: elm,
                name: "Current Account",
                children: [],
              };
              break;
            // case "DD":
            //   data[elm] = {
            //     code: elm,
            //     name: "DD",
            //     children: [],
            //   };
            //   break;
            // case "S":
            //   data[elm] = {
            //     code: elm,
            //     name: "S",
            //     children: [],
            //   };
            //   break;
            case "LN":
              data[elm] = {
                code: elm,
                name: "Loan Account",
                children: [],
              };
              break;
            case "CD":
              data[elm] = {
                code: elm,
                name: "Deposit Account",
                children: [],
              };
              break;
            default:
              data[elm] = {};
          }
        });

      dispatch(setProductAccountType(data));
      dispatch(setLoadingProductAccounttype(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoadingProductAccounttype(false));
    }
  } catch (err) {
    dispatch(setError(err.message));
    dispatch(setLoadingExecute(true));
  }
};
