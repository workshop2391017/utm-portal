/* eslint-disable space-before-function-paren */
export const SET_DATA = "workshop/SET_DATA";

export const setWorkshopData = (payload) => ({
  type: SET_DATA,
  payload,
});
