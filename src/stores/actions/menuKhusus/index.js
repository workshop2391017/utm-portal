// menuKhusus Actions
// --------------------------------------------------------

import { privilegeCONFIG } from "configuration/privilege";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "menuKhusus/SET_LOADING";
export const CLEAR_ERROR = "menuKhusus/CLEAR_ERROR";
export const SET_ERROR = "menuKhusus/SET_ERROR";
export const INIT_DATA = "menuKhusus/INIT_DATA";
export const SET_DATA = "menuKhusus/SET_DATA";
export const SET_DATA_DETAIL = "menuKhusus/SET_DATA_DETAIL";
export const SET_DATA_GLOBAL = "menuKhusus/SET_DATA_GLOBAL";
export const SET_DATA_DETAIL_EDIT = "menuKhusus/SET_DATA_DETAIL_EDIT";
export const SET_POP_UP = "menuKhusus/SET_POP_UP";

export const SET_DOUBLE_SUBMIT = "menuKhusus/SET_DOUBLE_SUBMIT";

const {
  serviceRequestMenuKhusus,
  serviceRequestMenuKhususDetail,
  serviceRequestMenuKhususGlobal,
  serviceRequestMenuKhususAdd,
} = require("utils/api");

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeMenuKhusus = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setTypeMenuKhususDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setTypeMenuKhusuDetailEdit = (payload) => ({
  type: SET_DATA_DETAIL_EDIT,
  payload,
});

export const setTypeMenuKhususGlobal = (payload) => ({
  type: SET_DATA_GLOBAL,
  payload,
});

export const setTypePop = (payload) => ({
  type: SET_POP_UP,
  payload,
});

export const setTypeClearError = () => ({
  type: CLEAR_ERROR,
});

export const getMenuKhusus = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestMenuKhusus(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setTypeMenuKhusus(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getMenuKhususDetail = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestMenuKhususDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setTypeMenuKhususDetail(res?.data?.menuPrivilegeDto));
    } else {
      dispatch(handleErrorBE(res));
    }

    console.warn("res:", res);
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getMenuKhususMenuGlobal = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    const res = await serviceRequestMenuKhususGlobal(payload);

    if (res.status === 200) {
      const result = res?.data?.menuAccessList?.filter(
        (val) =>
          val?.name_en?.trim().toLowerCase() !== "manajemen cek" &&
          val?.name_en?.toLowerCase().trim() !== "profile" &&
          val?.name_en?.toLowerCase().trim() !== "summary" &&
          val?.name_en?.toLowerCase().trim() !== "account" &&
          val?.name_en?.toLowerCase().split(" ")[0] !== "cash"
      );
      dispatch(setTypeMenuKhususGlobal(result));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const AddMenuKhususMenuGlobal =
  (payload, payloadValidation, handleNext) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.SPECIAL_MENU,
          ...payloadValidation,
          validate: true,
          loader: setLoading,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoading(true));
              const res = await serviceRequestMenuKhususAdd(payload);
              dispatch(setLoading(false));

              if (res.status === 200) {
                dispatch(setTypePop(true));
              } else {
                dispatch(handleErrorBE(res));
              }
            } catch (error) {
              dispatch(setHandleError(error?.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };
