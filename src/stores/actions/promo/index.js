// listPromo Actions
// --------------------------------------------------------

import {
  hanldeValidationDataLms,
  serviceGetAllBranch,
  serviceGetPromoCategory,
  serviceGetSegmentasi,
  serviceManagementPromoCategory,
  serviceManagementPromoDelete,
  serviceManagementPromoDetail,
  serviceManagementPromoEdit,
  serviceManagementPromoGetMerchants,
  serviceManagementPromoList,
  serviceManagementPromoSave,
  serviceMerchantList,
  servicePromoCategory,
  servicePromoCategorySave,
} from "utils/api";
import { checkOnline, handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "listPromo/SET_LOADING";
export const SET_LOADING_MODAL = "listPromo/SET_LOADING_MODAL";
export const CLEAR_ERROR = "listPromo/CLEAR_ERROR";
export const SET_ERROR = "listPromo/SET_ERROR";
export const INIT_DATA = "listPromo/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "listPromo/SET_DOUBLE_SUBMIT";
export const SET_PAGES = "listPromo/SET_PAGES";
export const SET_DETAIL = "listPromo/SET_DETAIL";
export const SET_TABLE_PAGE = "listPromo/SET_TABLE_PAGE";
export const SET_PROMO_CATEGORY = "listPromo/SET_PROMO_CATEGORY";
export const SET_SEGMENTATION = "listPromo/SET_SEGMENTATION";
export const SET_BRANCH = "listPromo/SET_BRANCH";
export const SET_MERCHANTS = "listPromo/SET_MERCHANTS";
export const SET_ISEXECUTE = "listAcceptArray/SET_ISEXECUTE";
export const SET_PROMO_CODE = "listAcceptArray/SET_PROMO_CODE";
export const SET_CATEGORIES = "listAcceptArray/SET_CATEGORIES";
export const SET_IS_ALREADY_EXIST = "listAcceptArray/SET_IS_ALREADY_EXIST";

export const SET_CATEGORIES_FIELD = "listAcceptArray/SET_CATEGORIES_FIELD";
export const SET_POP_UP = "listAcceptArray/SET_POP_UP";

export const setPromoCode = (payload) => ({
  type: SET_PROMO_CODE,
  payload,
});

export const setMerchants = (payload) => ({
  type: SET_MERCHANTS,
  payload,
});

export const setCategories = (payload) => ({
  type: SET_CATEGORIES,
  payload,
});

export const setIsExecute = (payload) => ({
  type: SET_ISEXECUTE,
  payload,
});

export const setBranch = (payload) => ({
  type: SET_BRANCH,
  payload,
});

export const setSegmentation = (payload) => ({
  type: SET_SEGMENTATION,
  payload,
});

export const setPromoCategory = (payload) => ({
  type: SET_PROMO_CATEGORY,
  payload,
});

export const setLoadingModal = (payload) => ({
  type: SET_LOADING_MODAL,
  payload,
});

export const setTablePage = (payload) => ({
  type: SET_TABLE_PAGE,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});
export const setPages = (payload) => ({
  type: SET_PAGES,
  payload,
});

export const setInitData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setDetails = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setCategoriesField = (payload) => ({
  type: SET_CATEGORIES_FIELD,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setPopUp = (payload) => ({
  type: SET_POP_UP,
  payload,
});

export const setServiceAlready = (payload) => ({
  type: SET_IS_ALREADY_EXIST,
  payload,
});

export const handlePromoList = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await serviceManagementPromoList(payload);
    if (response.status === 200) {
      dispatch(setInitData(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.error("ERR ", error);
  } finally {
    dispatch(setLoading(false));
  }
};

export const actionGetMerchants = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    const response = await serviceMerchantList(payload);
    if (response.status === 200) {
      dispatch(setMerchants(response.data.content));
      // console.log("ini response.data.merchantList", response.data.merchantList);
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.error("ERR ", error);
  }
};

export const handlePromoLDetail = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await serviceManagementPromoDetail(payload);
    if (response.status === 200) {
      dispatch(setDetails(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setLoading(false));
  }
};

export const getPromoCategory = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await servicePromoCategory(payload);
    if (response.status === 200) {
      dispatch(setCategories(response.data?.categories || []));
      dispatch(setTablePage(response?.data || ""));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  } finally {
    dispatch(setLoading(false));
  }
};

export const getPromoCategoryAddScreen = (payload) => async (dispatch) => {
  try {
    const response = await servicePromoCategory(payload);
    if (response.status === 200) {
      dispatch(setCategories(response.data?.categories || []));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const handlePromoCategory = (payload, detail) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoadingModal(true));
    const response = await serviceManagementPromoCategory(payload);
    if (response.status === 200) {
      const data =
        response.data?.systemParameterListPage?.parameters?.[0]?.value;

      const jParse = data ? JSON.parse(data) : null;

      const newJparse = jParse?.promotionCategoryList?.map((elm) => {
        if (
          detail?.promotionCategory?.findIndex(
            (f) => elm.promotionCategoryNameEng === f
          ) >= 0
        ) {
          return {
            ...elm,
            checked: true,
            isSelected: true,
            isDetail: true,
          };
        }
        return {
          ...elm,
        };
      });
      dispatch(setPromoCategory({ promotionCategoryList: newJparse }));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setLoadingModal(false));
  }
};

export const handleSegmentationGet = (payload, detail) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    const response = await serviceGetSegmentasi(payload);
    if (response.status === 200) {
      const segmentasi = response.data?.listSegmentation?.map((e) => ({
        id: e.id,
        name: e.name,
        description: e.description,
      }));

      const newSegmentation = segmentasi?.map((elm) => {
        if (detail?.segmentation?.findIndex((f) => elm.id === f) >= 0) {
          return {
            ...elm,
            checked: true,
            isSelected: true,
            isDetail: true,
          };
        }
        return {
          ...elm,
        };
      });

      dispatch(setSegmentation(newSegmentation));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setLoadingModal(false));
  }
};

export const handleAllBranch = (detail) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    const response = await serviceGetAllBranch();
    if (response.status === 200) {
      const newBranch = response?.data?.olistBranch?.map((elm) => {
        if (detail?.location?.findIndex((f) => elm.branchName === f) >= 0) {
          return {
            ...elm,
            checked: true,
            isDetail: true,
            isSelected: true,
          };
        }
        return {
          ...elm,
        };
      });

      dispatch(setBranch({ olistBranch: newBranch }));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setLoadingModal(false));
  }
};

export const handlePromoSave = (payload, cb, cbEr) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setIsExecute(true));
  try {
    const response = await serviceManagementPromoSave(payload);
    if (response.status === 200) {
      if (cb) cb();
    } else if (response.status === 400 && response.data.errorCode === "2602") {
      if (cbEr) cbEr(response.data.message);
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setIsExecute(false));
  }
};

export const handlePromoSaveCategory = (payload, cb) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setIsExecute(true));
  try {
    const response = await servicePromoCategorySave(payload);
    if (response.status === 200) {
      if (cb) cb();
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setIsExecute(false));
  }
};

export const handlePromoDeleteCategory = (payload, cb) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setIsExecute(true));
  try {
    const response = await servicePromoCategorySave(payload);
    if (response.status === 200) {
      dispatch(setPopUp(true));
      if (cb) cb();
    } else if (response?.data?.engMessage === "Data is being used") {
      dispatch(handleErrorBE(response, false));
    } else dispatch(handleErrorBE(response, false));
  } catch (error) {
    dispatch(handleErrorBE(false));
  } finally {
    dispatch(setIsExecute(false));
  }
};

export const handlePromoEdit = (payload, cb, cbEr) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setIsExecute(true));
  try {
    const response = await serviceManagementPromoEdit(payload);

    if (response.status === 200) {
      if (cb) cb();
    } else if (response.status === 400 && response.data.errorCode === "2602") {
      if (cbEr) cbEr(response.data.message);
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setIsExecute(false));
  }
};

export const handlePromoDelete = (payload, cb) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setIsExecute(true));
  try {
    const response = await serviceManagementPromoDelete(payload);

    if (response.status === 200) {
      if (cb) cb();
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(setIsExecute(false));
  }
};

export const getValidationService = (payload) => async (dispatch) => {
  try {
    const res = await hanldeValidationDataLms(payload);

    if (res.status === 200) {
      dispatch(setServiceAlready(!res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setError(error?.message));
  }
};
