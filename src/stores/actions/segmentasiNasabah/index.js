/* eslint-disable no-param-reassign */
import moment from "moment";
import { menusSegmentasiCONF } from "stores/reducers/segmentasiNasabah";
import {
  serviceAddSegmentasi,
  serviceDetailChargePackage,
  serviceDownloadTemplate,
  serviceGetSegmentasi,
  serviceNewSeqGetChargePackage,
  serviceNewSeqGetLimitPackage,
  serviceNewSeqGetMenuPackage,
  serviceNewSeqGetPrpoductType,
  serviceNewSeqGetSegmentBranch,
  serviceRequestGetTearingSlab,
  serviceRequestGetTearingSlabDetail,
  serviceSeqmenBranchDetail,
  serviceSeqmenChargePackageDetail,
  serviceSeqmenLimitPackageDetail,
  serviceSeqmenMenuDetail,
  serviceSeqmenProductTypeDetail,
  serviceSeqParameterAdvanceUplaod,
} from "utils/api";
import { handleExceptionError, setLocalStorage } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";
import { validateTask } from "../validateTaskPortal";

export const SET_DATA_FORM = "customerSegmen/SET_DATA_FORM";
export const SET_SEGMENTATION_TABS_ACTIVE =
  "customerSegmen/SET_SEGMENTATION_TABS_ACTIVE";
export const SET_SEGMENTATION_FORM_ACTIVE =
  "customerSegmen/SET_SEGMENTATION_FORM_ACTIVE";

export const ADD_NEW_REKENING = "customerSegmen/ADD_NEW_REKENING";
export const ADD_NEW_PACKAGE = "customerSegmen/ADD_NEW_PACKAGE";
export const ADD_NEW_MENUPACKAGE = "customerSegmen/ADD_NEW_MENUPACKAGE";
export const ADD_NEW_CHARGE_PACKAGE = "customerSegmen/ADD_NEW_CHARGE_PACKAGE";
export const ADD_NEW_MENU_BRANCH = "customerSegmen/ADD_NEW_MENU_BRANCH";
export const ADD_MENU_PACKAGE_KHUSUS = "customerSegmen/ADD_MENU_PACKAGE_KHUSUS";
export const SET_ACTIVE_MENU = "customerSegmen/SET_ACTIVE_MENU";
export const SET_MENU_DATA = "customerSegmen/SET_MENU_DATA";
export const SET_MENU_KHUSUS_DATA = "customerSegmen/SET_MENU_KHUSUS_DATA";
export const SET_MENU_ADVANCE_DATA = "customerSegmen/SET_MENU_ADVANCE_DATA";
export const SET_LOADING = "customerSegmen/SET_LOADING";
export const SET_DATA_LIST = "customerSegmen/SET_DATA_LIST";
export const SET_ERROR = "customerSegmen/SET_ERROR";
export const SET_CLEAR_ERROR = "customerSegmen/SET_CLEAR_ERROR";
export const SET_LOADING_INSERT = "customerSegmen/SET_LOADING_INSERT";
export const SET_SUCCESS_INSERT = "customerSegmen/SET_SUCCESS_INSERT";
export const SET_LOADING_PACKAGE = "customerSegmen/SET_LOADING_PACKAGE";
export const SET_PACKAGE_DATA = "customerSegmen/SET_PACKAGE_DATA";
export const SET_LOADING_UPLOAD_FILE = "customerSegmen/SET_LOADING_UPLOAD_FILE";
export const SET_UPLOAD_FILE = "customerSegmen/SET_UPLOAD_FILE";
export const SET_LOADING_DETAIL = "customerSegmen/SET_LOADING_DETAIL";
export const SET_CLEAR_DATA = "customerSegmen/SET_CLEAR_DATA";
export const SET_CHARGE_PACKAGE_TABS = "customerSegmen/SET_CHARGE_PACKAGE_TABS";
export const SET_SEGMENTASI_ID = "customerSegmen/SET_SEGMENTASI_ID";
export const SET_LOADING_UNDUH = "customerSegmen/SET_LOADING_UNDUH";
export const SET_PREVIEW_ADVANCE = "customerSegmen/SET_PREVIEW_ADVANCE";
export const SET_ERROR_UPLOAD = "customerSegmen/SET_ERROR_UPLOAD";

export const setErrorUpload = (payload) => ({
  type: SET_ERROR_UPLOAD,
  payload,
});

export const SetSegmentationDataForm = (payload) => ({
  type: SET_DATA_FORM,
  payload,
});

export const setSegmentationTabs = (payload) => ({
  type: SET_SEGMENTATION_TABS_ACTIVE,
  payload,
});

export const setSegmentationFormActive = (payload) => ({
  type: SET_SEGMENTATION_FORM_ACTIVE,
  payload,
});

export const seActiveMenu = (payload) => ({
  type: SET_ACTIVE_MENU,
  payload,
});

export const setMenuData = (payload, key) => ({
  type: SET_MENU_DATA,
  payload: { key, payload },
});

export const setMenuKhususData = (payload, key) => ({
  type: SET_MENU_KHUSUS_DATA,
  payload: { key, payload },
});

export const setMenuAdvanceData = (payload, key) => ({
  type: SET_MENU_ADVANCE_DATA,
  payload: { key, payload },
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDataList = (payload) => ({
  type: SET_DATA_LIST,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setClearError = () => ({
  type: SET_CLEAR_ERROR,
});

export const setSuccessInsert = (payload) => ({
  type: SET_SUCCESS_INSERT,
  payload,
});

export const setLoadingpackage = (payload) => ({
  type: SET_LOADING_PACKAGE,
  payload,
});

export const setLoadingDetail = (payload) => ({
  type: SET_LOADING_DETAIL,
  payload,
});

export const setPackageData = (payload, key) => ({
  type: SET_PACKAGE_DATA,
  payload: { payload, key },
});

export const setLoadingInsert = (payload) => ({
  type: SET_LOADING_INSERT,
  payload,
});

export const setUploadLoadingFile = (payload) => ({
  type: SET_LOADING_UPLOAD_FILE,
  payload,
});

export const setUploadFile = (payload) => ({
  type: SET_UPLOAD_FILE,
  payload,
});

export const setClearData = (payload) => ({
  type: SET_CLEAR_DATA,
});

export const setTieringSlabTabsActive = (payload) => ({
  type: SET_CHARGE_PACKAGE_TABS,
  payload,
});

export const setSegmentasiId = (payload) => ({
  type: SET_SEGMENTASI_ID,
  payload,
});

export const setLoadingUnduh = (payload) => ({
  type: SET_LOADING_UNDUH,
  payload,
});

export const setPreviewAdance = (payload) => ({
  type: SET_PREVIEW_ADVANCE,
  payload,
});

const mappingDetailData = (data) => {
  let loopingData = data;
  if (data === null) {
    return [];
  }
  if (data[0]?.id === undefined) {
    loopingData = [data];
  }
  return (loopingData ?? []).map((elm) => ({
    id: elm?.id,
    title: elm?.name,
    subtitle: elm?.code,
  }));
};

export const onEditSegmentation = (elm) => (dispatch, getState) => {
  const { menuAdvance } = getState().customerSegmentation;

  const lsParams = {
    segmentasiId: elm.id,
  };
  setLocalStorage("segmentationEdit", JSON.stringify(lsParams));

  dispatch(setSegmentasiId(elm?.id));
  dispatch(
    SetSegmentationDataForm({
      name: elm?.name,
      description: elm?.description,
    })
  );
  Object.keys(elm).forEach((key) => {
    const currDataKey = elm[key];
    switch (key) {
      case "jenisProduct":
        return dispatch(
          setMenuData(
            {
              ...mappingDetailData(currDataKey)[0],
              jenisProductId: mappingDetailData(currDataKey)[0]?.id,
            },
            menusSegmentasiCONF.JENIS_PRODUK_REKENING
          )
        );
      case "limitPackage":
        return dispatch(
          setMenuData(
            {
              ...mappingDetailData(currDataKey)[0],
              limitPackageId: mappingDetailData(currDataKey)[0]?.id,
            },
            menusSegmentasiCONF.LIMIT_PACKAGE
          )
        );
      case "chargePackage":
        return dispatch(
          setMenuData(
            {
              ...mappingDetailData(currDataKey)[0],
              chargePackageId: mappingDetailData(currDataKey)[0]?.id,
            },
            menusSegmentasiCONF.CHARGE_PACKAGE
          )
        );
      case "menuPackage":
        return dispatch(
          setMenuData(
            {
              ...mappingDetailData(currDataKey)[0],
              menuPackageId: mappingDetailData(currDataKey)[0]?.id,
            },
            menusSegmentasiCONF.MENU_PACKGE
          )
        );

      case "menuKhusus":
        return dispatch(
          setMenuKhususData(
            {
              ...mappingDetailData(currDataKey)[0],
              menuKhususId: mappingDetailData(currDataKey)[0]?.id,
            },
            menusSegmentasiCONF.MENU_KHUSUS
          )
        );
      case "segmentBranch":
        return dispatch(
          setMenuKhususData(
            {
              ...mappingDetailData(currDataKey)[0],
              segmentBranchId: mappingDetailData(currDataKey)[0]?.id,
            },
            menusSegmentasiCONF.SEGMEN_CABANG
          )
        );
      case "parameterAdvance":
        if (elm?.parameterAdvance.length) {
          dispatch(setPreviewAdance(true));
          dispatch(
            setPackageData(
              {
                [menusSegmentasiCONF.UNGGAH_FILE]: {
                  ...menuAdvance[menusSegmentasiCONF.UNGGAH_FILE],
                  isUploaded: true,
                  fileUpload: {
                    ...menuAdvance[menusSegmentasiCONF.UNGGAH_FILE]?.fileUpload,
                    saveParameterAdvance: elm?.parameterAdvance?.map(
                      (elm, i) => ({
                        ...elm,
                        existing: true,
                        id: i + 1,
                        vId: elm?.id,
                      })
                    ),
                  },
                },
              },
              "menuAdvance"
            )
          );
          dispatch(
            setMenuAdvanceData({ id: true }, menusSegmentasiCONF.UNGGAH_FILE)
          );
        }
        break;

      default:
        return null;
    }
  });
};

const getServices = (payload, activeMenu, tabsForOptionalMenuActive) => {
  activeMenu = tabsForOptionalMenuActive || activeMenu;
  switch (activeMenu) {
    case menusSegmentasiCONF.CHARGE_PACKAGE:
      return serviceNewSeqGetChargePackage(payload);
    case menusSegmentasiCONF.CHARGE_PACKAGE_TIERINGSLAB:
      return serviceRequestGetTearingSlab({
        kode: null,
        pageNumber: 0,
        pageSize: 10,
      });
    case menusSegmentasiCONF.MENU_PACKGE:
      return serviceNewSeqGetMenuPackage({ ...payload, menuType: 0 });
    case menusSegmentasiCONF.LIMIT_PACKAGE:
      return serviceNewSeqGetLimitPackage(payload);
    case menusSegmentasiCONF.MENU_KHUSUS:
      return serviceNewSeqGetMenuPackage({ ...payload, menuType: 1 });
    case menusSegmentasiCONF.JENIS_PRODUK_REKENING:
      return serviceNewSeqGetPrpoductType(payload);
    case menusSegmentasiCONF.SEGMEN_CABANG:
      return serviceNewSeqGetSegmentBranch(payload);
    default:
      return null;
  }
};

const getDetailServices = (currSelectedData, activeMenu) => {
  switch (activeMenu) {
    case menusSegmentasiCONF.CHARGE_PACKAGE:
      return serviceDetailChargePackage({
        chargePackageCode: currSelectedData?.subtitle,
        chargePackageName: currSelectedData?.title,
      });
    case menusSegmentasiCONF.CHARGE_PACKAGE_TIERINGSLAB:
      return serviceRequestGetTearingSlabDetail({
        tiering: {
          id: currSelectedData?.id,
        },
      });
    case menusSegmentasiCONF.LIMIT_PACKAGE:
      return serviceSeqmenLimitPackageDetail({
        id: currSelectedData?.id,
        pageNumber: 0,
        pageSize: 0,
      });
    case menusSegmentasiCONF.MENU_PACKGE:
      return serviceSeqmenMenuDetail({
        code: currSelectedData?.code,
        menuPackageId: currSelectedData?.id,
        menuType: 0,
        pageNumber: 0,
        pageSize: 0,
      });
    case menusSegmentasiCONF.MENU_KHUSUS:
      return serviceSeqmenMenuDetail({
        menuPackageId: currSelectedData?.id,
      });
    case menusSegmentasiCONF.JENIS_PRODUK_REKENING:
      return serviceSeqmenProductTypeDetail({
        productType: {
          id: currSelectedData?.id,
        },
      });
    case menusSegmentasiCONF.SEGMEN_CABANG:
      return serviceSeqmenBranchDetail({
        segmentBranchId: currSelectedData?.id,
      });
    default:
      return null;
  }
};

export const handleDaftarSegmentasi = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceGetSegmentasi(payload);
    if (response.status === 200) {
      dispatch(setDataList(response.data));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoading(false));
    }
  } catch (e) {
    dispatch(setError(e.message));
    dispatch(setLoading(false));
  }
};

export const handleAddSegmentasi = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const response = await serviceGetSegmentasi(payload);

    if (response.status === 200) {
      dispatch(setDataList(response.data));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoading(false));
    }
  } catch (e) {
    dispatch(setError(e.message));
    dispatch(setLoading(false));
  }
};

const getMenuParent = (_, getState) => {
  const { activeMenu, menus, menuKhusus, menuAdvance } =
    getState()?.customerSegmentation;

  switch (activeMenu) {
    case menusSegmentasiCONF.MENU_KHUSUS:
      return { key: "menuKhusus", menu: menuKhusus };
    case menusSegmentasiCONF.SEGMEN_CABANG:
      return { key: "menuKhusus", menu: menuKhusus };
    case menusSegmentasiCONF.UNGGAH_FILE:
      return { key: "menuAdvance", menu: menuAdvance };
    default:
      return { key: "menus", menu: menus };
  }
};

export const handleNewSeqGetChargePackage =
  (payload, tabsForOptionalMenuActive) => async (dispatch, getState) => {
    dispatch(checkOnline());

    const { activeMenu, segmentasiId } = getState()?.customerSegmentation;
    const menu = dispatch(getMenuParent);

    try {
      dispatch(setLoadingpackage({ [activeMenu]: true }));
      const response = await getServices(
        payload,
        activeMenu,
        tabsForOptionalMenuActive
      );

      if (response)
        if (response && response?.status === 200) {
          dispatch(
            setPackageData(
              {
                [activeMenu]: {
                  ...menu?.menu[activeMenu],
                  dataList: response.data,
                  ...(segmentasiId &&
                    menu?.menu[activeMenu] && {
                      fetchListFullFill: true,
                    }),
                },
              },
              menu?.key
            )
          );

          dispatch(setLoadingpackage({ [activeMenu]: false }));
        } else if (handleExceptionError(response.status)) {
          dispatch(handleErrorBE(response));
          dispatch(setLoadingpackage({ [activeMenu]: false }));
        }
    } catch (e) {
      dispatch(setError(e.message));
      dispatch(setLoadingpackage(false));
    }
  };

export const handleGetSeqmentationDetail =
  (currSelectedData) => async (dispatch, getState) => {
    dispatch(checkOnline());
    const { activeMenu } = getState()?.customerSegmentation;
    const menu = dispatch(getMenuParent);

    if (!Object.values(currSelectedData)?.filter((e) => e)?.length) {
      dispatch(
        setPackageData(
          {
            [activeMenu]: {
              ...menu?.menu[activeMenu],
              detailData: [],
            },
          },
          menu?.key
        )
      );
      return;
    }

    dispatch(setLoadingDetail({ [activeMenu]: true }));
    try {
      const response = await getDetailServices(currSelectedData, activeMenu);

      if (response) {
        if (response && response?.status === 200) {
          dispatch(
            setPackageData(
              {
                [activeMenu]: {
                  ...menu?.menu[activeMenu],
                  detailData: response.data,
                },
              },
              menu?.key
            )
          );
          dispatch(setLoadingDetail({ [activeMenu]: false }));
        } else if (handleExceptionError(response.status)) {
          dispatch(handleErrorBE(response));
          dispatch(setLoadingDetail({ [activeMenu]: false }));
          dispatch(
            setPackageData(
              {
                [activeMenu]: {
                  ...menu?.menu[activeMenu],
                  detailData: [],
                },
              },
              menu?.key
            )
          );
        }
      }
      dispatch(setLoadingDetail({ [activeMenu]: false }));
    } catch (e) {
      dispatch(
        setPackageData(
          {
            [activeMenu]: {
              ...menu?.menu[activeMenu],
              detailData: [],
            },
          },
          menu?.key
        )
      );
      dispatch(setError(e.message));
      dispatch(setLoadingDetail({ [activeMenu]: false }));
    }
  };

export const handleAddNewSeqGetChargePackage =
  (formPayload, handleNext) => async (dispatch, getState) => {
    dispatch(checkOnline());
    const { menus, menuAdvance, activeMenu, menuKhusus, segmentasiId } =
      getState()?.customerSegmentation;

    const objMenu = {};
    Object.keys(menus).forEach((key) => {
      objMenu[menus[key].idKey] = {
        id: menus[key]?.data[menus[key].idKey],
        name: menus[key]?.data?.title,
        code: menus[key]?.data?.subtitle,
      };
    });

    const objMenuKhusus = {};
    Object.keys(menuKhusus).forEach((key) => {
      objMenuKhusus[menuKhusus[key].idKey] = {
        id: menuKhusus[key]?.data[menuKhusus[key].idKey],
        name: menuKhusus[key]?.data?.title,
        code: menuKhusus[key]?.data?.subtitle,
      };
    });

    const parameterAdvanceMap = (
      menuAdvance[activeMenu]?.fileUpload?.saveParameterAdvance ?? []
    ).map((elm) => ({
      ...elm,
      segmentationId: segmentasiId,
    }));

    const payload = {
      ...formPayload,
      ...objMenu,
      ...objMenuKhusus,
      ...(segmentasiId
        ? {
            idSegmentation: {
              ...formPayload,
              id: segmentasiId,
              segmentBranchId: 0,
            },
          }
        : { idSegmentation: null }),
      saveParameterAdvance: parameterAdvanceMap
        .map(({ vId, id, ...rest }) =>
          rest.existing ? { ...rest, id: vId } : { ...rest }
        )
        .filter((e) => (e.existing && e.id) || (!e.id && !e.isDeleted)),
    };

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.SEGMENTATION,
          code: null,
          id: segmentasiId || null,
          name: segmentasiId ? null : formPayload?.name,
          validate: true,
          loader: setLoadingInsert,
        },
        {
          async onContinue() {
            dispatch(setLoadingInsert(true));
            try {
              const response = await serviceAddSegmentasi(payload);
              if (response.status === 200) {
                dispatch(setSuccessInsert(true));
                dispatch(setLoadingInsert(false));
              } else if (handleExceptionError(response.status)) {
                dispatch(handleErrorBE(response));
                dispatch(setLoadingInsert(false));
              }
            } catch (e) {
              dispatch(setError(e.message));
              dispatch(setLoadingInsert(false));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc

            dispatch(setLoadingInsert(false));
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const handleUploadParameterSettings =
  () => async (dispatch, getState) => {
    dispatch(setUploadLoadingFile(true));
    const { activeMenu, menuAdvance, uploadFile, segmentasiId } =
      getState()?.customerSegmentation;

    const formData = new FormData();
    formData.append("file", uploadFile);

    try {
      const response = await serviceSeqParameterAdvanceUplaod(
        formData,
        segmentasiId
      );
      if (response.status === 200) {
        const dataAdvance = [
          ...(menuAdvance[activeMenu]?.fileUpload?.saveParameterAdvance ?? []),
        ];

        (response?.data?.saveParameterAdvance ?? []).forEach((elm, i) => {
          dataAdvance.push({
            ...elm,
            id: i + 1,
          });
        });

        const data = menuAdvance[menusSegmentasiCONF?.UNGGAH_FILE]?.fileUpload;

        const duplicateData = [];

        (data?.saveParameterAdvance ?? [])?.forEach(
          (
            {
              accountNumber,
              age,
              averageBalance,
              businessField,
              division,
              type,
            },
            i
          ) => {
            const findDuplicate = (
              response?.data?.saveParameterAdvance ?? []
            ).find(
              (dup) =>
                accountNumber === dup.accountNumber &&
                age === dup.age &&
                averageBalance === dup.averageBalance &&
                businessField === dup.businessField &&
                division === dup.division &&
                type === dup.type
            );

            if (findDuplicate !== undefined) {
              if (
                duplicateData?.find(
                  ({ accountNumber }) =>
                    accountNumber === findDuplicate?.findDuplicate
                ) === undefined
              ) {
                duplicateData.push(findDuplicate);
              }

              duplicateData.push({
                accountNumber,
                age,
                averageBalance,
                businessField,
                division,
                type,
              });
            }
          }
        );

        dispatch(
          setPackageData(
            {
              [activeMenu]: {
                ...menuAdvance[activeMenu],
                isUploaded: true,
                fileUpload: {
                  ...response?.data,
                  saveParameterAdvance: dataAdvance.filter((a) =>
                    duplicateData.map(
                      (e) => a?.accountNumber !== e?.accountNumber
                    )
                  ),
                  validParameterAdvance: (
                    response?.data?.saveParameterAdvance ?? []
                  ).filter((a) => {
                    const accs = duplicateData.map(
                      ({ accountNumber }) => accountNumber
                    );

                    if (!accs.length) return true;
                    return a.accountNumber?.includes(accs);
                  }),
                  ...(duplicateData?.length && {
                    duplicateParameterAdvance: duplicateData,
                  }),
                },
              },
            },
            "menuAdvance"
          )
        );
        dispatch(setUploadLoadingFile(false));
      } else {
        dispatch(setErrorUpload(true));
        dispatch(setUploadLoadingFile(false));
      }
    } catch (e) {
      console.error("ERR ", e);
      dispatch(setErrorUpload(true));
      dispatch(setUploadLoadingFile(false));
    }
  };

export const handleDownloadTemp =
  (payload, fileName = "Template") =>
  async (dispatch) => {
    try {
      await dispatch(checkOnline());
      dispatch(setLoadingUnduh(true));
      const response = await serviceDownloadTemplate({
        file: "public/template/Templete Upload File Advance.xlsx",
      });
      const { status, data } = response;
      dispatch(setLoadingUnduh(false));

      const d = new Date();
      if (status === 200) {
        const getFileName = `${fileName}-${d?.getTime()}.xlsx`;
        const url = window.URL.createObjectURL(data);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", getFileName);
        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(status)) {
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          let data = response.data;
          reader.onload = () => {
            data = JSON.parse(reader.result);
            resolve(Promise.reject(data));
          };
          reader.onerror = () => {
            reject(data);
          };
          reader.readAsText(data);
        })
          .then((err) => {
            dispatch(
              setError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          })
          .catch((err) => {
            dispatch(
              setError({
                isError: true,
                errorCode: err.status,
                message: err.message,
              })
            );
          });
        dispatch(setLoadingUnduh(false));
      }
    } catch {
      dispatch(setLoadingUnduh(false));
    }
  };
