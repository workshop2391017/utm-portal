// errorGeneral Actions
// --------------------------------------------------------

import { removeAllLocalStorage } from "utils/helpers";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "errorGeneral/SET_LOADING";
export const CLEAR_ERROR = "errorGeneral/CLEAR_ERROR";
export const SET_ERROR = "errorGeneral/SET_ERROR";
export const INIT_DATA = "errorGeneral/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "errorGeneral/SET_DOUBLE_SUBMIT";
export const SET_POP_UP_EXPIRED = "errorGeneral/SET_POP_UP_EXPIRED";
export const SET_POP_UP_OFFLINE = "errorGeneral/SET_POP_UP_OFFLINE";
export const SET_POP_UP_ERROR_BE = "errorGeneral/SET_POP_UP_ERROR_BE";
export const SET_IS_RELOADED = "errorGeneral/SET_IS_RELOADED";
export const SET_ERROR_ALREADY_USED = "errorGeneral/SET_ERROR_ALREADY_USED";
export const SET_GOBACK_ALREADY_USED = "errorGeneral/SET_GOBACK_ALREADY_USED";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setIsReloaded = (payload) => ({
  type: SET_IS_RELOADED,
  payload,
});

export const setPopUpExpired = (payload) => ({
  type: SET_POP_UP_EXPIRED,
  payload,
});

export const setPopUpOffline = (payload) => ({
  type: SET_POP_UP_OFFLINE,
  payload,
});

export const setPopUpErrorBE = (payload) => ({
  type: SET_POP_UP_ERROR_BE,
  payload,
});

export const setErrorAlreadyUsed = (payload) => ({
  type: SET_ERROR_ALREADY_USED,
  payload,
});

export const setNeedGoBack = (payload) => ({
  type: SET_GOBACK_ALREADY_USED,
  payload,
});

export const handleErrorBE =
  (
    response = { status: 400, data: {} },
    redirect = true,
    action,
    emptyAction = () => {}
  ) =>
  (dispatch) => {
    const { status, data } = response;

    // if (data.errorCode === "0326") {
    //   dispatch(
    //     setErrorAlreadyUsed({
    //       isError: true,
    //       message: data?.message ? data?.message : data?.engMessage,
    //       code: data?.errorCode,
    //       redirect,
    //     })
    //   );

    //   return;
    // }

    if (
      data?.errorCode === "0211" ||
      data?.errorCode === "0510" ||
      data?.errorCode === "0326" ||
      data?.errorCode === "0309" || // double approve
      data?.errorCode === "0426" || // approval not found
      data?.errorCode === "0230" ||
      data?.errorCode === "0090" ||
      data?.errorCode === "0993" || // approval matrix not found
      data?.errorCode === "TKNAJ-04"
    ) {
      dispatch(
        setErrorAlreadyUsed({
          isError: true,
          message: data?.engMessage,
          code: data?.errorCode,
          redirect,
          action,
          title: data?.errorCode === "TKNAJ-04" ? "" : "Failure",
        })
      );
      return;
    }
    if (data?.errorCode === "0033") {
      emptyAction();
    }
    if (
      data?.errorCode === "0033" ||
      status === 401 ||
      status === 502 ||
      status === 503 ||
      data?.message?.includes("I/O")
    ) {
      dispatch(setError({ isError: false, message: "", code: "" }));
    } else {
      dispatch(
        setError({
          isError: true,
          message: data?.engMessage ? data?.engMessage : data?.message,
          code: data?.errorCode,
        })
      );
    }
  };

export const handleClearError = () => (dispatch) => {
  dispatch(setError({ isError: false, message: "", code: "" }));
  dispatch(
    setErrorAlreadyUsed({ isError: false, message: "", code: "", title: "" })
  );
};

export const checkOnline = () => (dispatch) =>
  new Promise((resolve, reject) => {
    const connection = navigator.onLine;
    if (connection) {
      resolve();
    } else {
      dispatch(setPopUpOffline(true));
      reject(new Error("User Offline"));
    }
  });
