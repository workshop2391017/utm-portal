// approvalMatrixFinantial Actions
// --------------------------------------------------------

import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import {
  serviceRequestApprovelMatrixFinanceSidebar,
  serviceRequestApprovelMatrixFinanceDetail,
  serviceRequestApprovelMatrixFinanceAdd,
  serviceRequestApprovalMatrixFinanceGetCurrencye,
  serviceRequestApprovelMatrixFinanceGetGroup,
  serviceRequestApprovelMatrixFinanceGetGroupSchemaList,
  serviceRequestApprovelMatrixFinanceAllService,
  serviceRequestApprovelMatrixFinanceDelete,
  serviceGetInfoCurrency,
} from "utils/api";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "approvalMatrixFinantial/SET_LOADING";
export const SET_LOADING_DETAIL = "approvalMatrixFinantial/SET_LOADING_DETAIL";
export const SET_LOADING_ALL_SERVICES =
  "approvalMatrixFinantial/SET_LOADING_ALL_SERVICES";
export const CLEAR_ERROR = "approvalMatrixFinantial/CLEAR_ERROR";
export const SET_ERROR = "approvalMatrixFinantial/SET_ERROR";
export const INIT_DATA = "approvalMatrixFinantial/INIT_DATA";
export const SET_DATA_SIDBAR_MENU =
  "approvalMatrixFinantial/SET_DATA_SIDBAR_MENU";
export const SET_DOUBLE_SUBMIT = "approvalMatrixFinantial/SET_DOUBLE_SUBMIT";
export const SET_DATA_DETAIL = "approvalMatrixFinantial/SET_DATA_DETAIL";
export const SET_POP_UP = "approvalMatrixFinantial/SET_POP_UP";
export const SET_CURRENCY_DATA = "approvalMatrixFinantial/SET_CURRENCY_DATA";
export const SET_SELECT_CURRENCY =
  "approvalMatrixFinantial/SET_SELECT_CURRENCY";
export const SET_DATA_GROUP = "approvalMatrixFinantial/SET_DATA_GROUP";
export const SET_DATA_DROPDOWN_APPV =
  "approvalMatrixFinantial/SET_DATA_DROPDOWN_APPV";
export const SET_POP_UP_CURR = "approvalMatrixFinantial/SET_POP_UP_CURR";
export const SET_DATA_SIDEBAR_CARD =
  "approvalMatrixFinantial/SET_DATA_SIDEBAR_CARD";

export const SET_LOADING_EXCUTE = "approvalMatrixFinantial/SET_LOADING_EXCUTE";

export const SET_DATA_EDIT = "approvalMatrixFinantial/SET_DATA_EDIT";
export const SET_IDCOMPANY = "approvalMatrixFinantial/SET_IDCOMPANY";
export const SET_ALL_SERVICE = "approvalMatrixFinantial/SET_ALL_SERVICE";
export const SET_IS_CHECK = "approvalMatrixFinantial/SET_IS_CHECK";
export const SET_MENU_CHILD_ID = "approvalMatrixFinantial/SET_MENU_CHILD_ID";
export const SET_SELECTED_MENU = "approvalMatrixFinantial/SET_SELECTED_MENU";
export const SET_SELECT_ALL = "approvalMatrixFinantial/SET_SELECT_ALL";

export const setSelectAll = (payload) => ({
  type: SET_SELECT_ALL,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingDetail = (payload) => ({
  type: SET_LOADING_DETAIL,
  payload,
});

export const setLoadingAllServices = (payload) => ({
  type: SET_LOADING_ALL_SERVICES,
  payload,
});

export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeErrorHandling = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeDataMenuSideBar = (payload) => ({
  type: SET_DATA_SIDBAR_MENU,
  payload,
});

export const setTypeDataDetailCard = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setTypePopUpSuccess = (payload) => ({
  type: SET_POP_UP,
  payload,
});

export const setTypeCurrencyData = (payload) => ({
  type: SET_CURRENCY_DATA,
  payload,
});

export const setTypeSelectCurrency = (payload) => ({
  type: SET_SELECT_CURRENCY,
  payload,
});

export const setTypeDataGroup = (payload) => ({
  type: SET_DATA_GROUP,
  payload,
});

export const setTypeDropdownAppv = (payload) => ({
  type: SET_DATA_DROPDOWN_APPV,
  payload,
});

export const setTypeDataCardSideBar = (payload) => ({
  type: SET_DATA_SIDEBAR_CARD,
  payload,
});

export const setTypeDataEdit = (payload) => ({
  type: SET_DATA_EDIT,
  payload,
});
export const setIdCompany = (payload) => ({
  type: SET_IDCOMPANY,
  payload,
});
export const setAllService = (payload) => ({
  type: SET_ALL_SERVICE,
  payload,
});
export const setDataIsCheckService = (payload) => ({
  type: SET_IS_CHECK,
  payload,
});
export const setDataMenuChildId = (payload) => ({
  type: SET_MENU_CHILD_ID,
  payload,
});

export const setSelectedMenus = (payload) => ({
  type: SET_SELECTED_MENU,
  payload,
});

export const getAllService = (payload) => async (dispatch, getState) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoadingAllServices(true));
    const res = await serviceRequestApprovelMatrixFinanceAllService(payload);
    dispatch(setLoadingAllServices(false));
    if (res.status === 200) {
      const data = (res.data?.workflowApprovalMatrixAllServiceDtos ?? []).map(
        (item) => ({
          ...item,
          isChecked: item.status === "DONE",
        })
      );
      dispatch(setAllService(data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErrorHandling(error?.message));
  }
};
export const getMenuApprovelMatrixSideBar =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      const { serviceIsCheck } = getState().approvalMatrixFinantial;
      // dispatch(setIdCompany(idPerusahaan));
      if (serviceIsCheck !== null) {
        const res = await serviceRequestApprovelMatrixFinanceSidebar(payload);

        if (res.status === 200) {
          dispatch(setTypeDataMenuSideBar(res?.data?.menuAccessList));
        } else {
          dispatch(handleErrorBE(res));
        }
      }
    } catch (error) {
      dispatch(setTypeErrorHandling(error?.message));
    }
  };

export const getMenuApprovelMatrixDetail =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoadingDetail(true));
    try {
      const { dataCompanyId, selectedMenu } =
        getState().approvalMatrixFinantial;

      const res = await serviceRequestApprovelMatrixFinanceDetail({
        corporateProfileId: dataCompanyId,
        menuId: payload,
        pageSize: 10,
        pageNumber: 0,
      });

      if (res.status === 200) {
        dispatch(setTypeDataDetailCard(res?.data));

        if (
          res.data.getApprovalMatrixByMenuResponse?.approvalMatrix?.[0]
            ?.currencyCode ||
          res.data.getApprovalMatrixByMenuResponse?.approvalMatrix?.[0]
            ?.currencyName
        ) {
          dispatch(
            setSelectedMenus({
              ...selectedMenu,
              currency: {
                value:
                  res.data.getApprovalMatrixByMenuResponse?.approvalMatrix?.[0]
                    ?.currencyCode,
                label:
                  res.data.getApprovalMatrixByMenuResponse?.approvalMatrix?.[0]
                    ?.currencyName,
              },
            })
          );
        } else {
          dispatch(
            setSelectedMenus({
              ...selectedMenu,
              currency: {
                value: "IDR",
                label: "Indonesian Rupiah",
              },
            })
          );
        }
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setTypeErrorHandling(error?.message));
    } finally {
      dispatch(setLoadingDetail(false));
    }
  };

export const getDataGroup = (payload) => async (dispatch, getState) => {
  dispatch(checkOnline());
  try {
    const res = await serviceRequestApprovelMatrixFinanceGetGroup({
      corporateProfileId: payload?.corporateProfileId,
      code: null,
    });

    if (res.status === 200) {
      dispatch(setTypeDataGroup(res?.data?.getGroupDtoList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErrorHandling(error?.message));
  }
};

export const getDataGroupApprovel = (payload) => async (dispatch, getState) => {
  dispatch(checkOnline());
  try {
    const { dataCompanyId } = getState().approvalMatrixFinantial;

    const res = await serviceRequestApprovelMatrixFinanceGetGroupSchemaList({
      corporateProfileId: dataCompanyId,
      code: null,
    });

    if (res.status === 200) {
      dispatch(setTypeDropdownAppv(res?.data?.workflowLimitSchemeDtoList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErrorHandling(error?.message));
  }
};

export const handleDeleteMenuFinancial =
  (payload, onSuccess, onError) => async (dispatch) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoadingExcute(true));
      const response = await serviceRequestApprovelMatrixFinanceDelete(payload);
      dispatch(setLoadingExcute(false));
      if (response.status === 200) {
        onSuccess();
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (err) {
      console.error("ERR ", err);
      onError();
    }
  };

export const addMenuApprovelMatrix =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoadingExcute(true));

      const { dataCompanyId } = getState().approvalMatrixFinantial;

      const res = await serviceRequestApprovelMatrixFinanceAdd({
        corporateProfileId: dataCompanyId,
        ...payload,
      });

      dispatch(setLoadingExcute(false));

      if (res.status === 200) {
        dispatch(setTypePopUpSuccess(true));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setTypeErrorHandling(error?.message));
    }
  };

export const getMenuApprovelMatrixCurrency = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    const res = await serviceGetInfoCurrency(payload);

    if (res.status === 200) {
      dispatch(setTypeCurrencyData(res?.data?.currencyList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErrorHandling(error?.message));
  }
};
