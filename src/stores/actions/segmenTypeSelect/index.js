// segmenTypeSelect Actions
// --------------------------------------------------------

import { serviceSegmenTypeSelect } from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE } from "../errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "segmenTypeSelect/SET_LOADING";
export const CLEAR_ERROR = "segmenTypeSelect/CLEAR_ERROR";
export const SET_ERROR = "segmenTypeSelect/SET_ERROR";
export const INIT_DATA = "segmenTypeSelect/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "segmenTypeSelect/SET_DOUBLE_SUBMIT";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const handleSegmenTypeSelect =
  (payload) => async (dispatch, getState) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceSegmenTypeSelect(payload);
      if (response.status === 200) {
        dispatch(setInitData(response.data));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
      }

      dispatch(setLoading(false));
    } catch (e) {
      dispatch(setLoading(false));
      // dispatch(setError(e.message));
      console.error(e);
    }
  };
