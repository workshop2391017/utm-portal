import { checkOnline } from "../errorGeneral";
import { handleExceptionError } from "../../../utils/helpers";
import {
  serviceFaqSave,
  serviceGetFaq,
  serviceGetFaqAdminBank,
} from "../../../utils/api";

export const SET_LOADING = "faq/SET_LOADING";
export const SET_DATA = "faq/SET_DATA";
export const SET_ADMIN_BANK_DATA = "faq/SET_ADMIN_BANK_DATA";
export const SET_ERROR = "faq/SET_ERROR";
export const SET_IS_EDIT = "faq/SET_IS_EDIT";
export const SET_DETAIL_DATA = "faq/SET_DETAIL_DATA";
export const SET_ID = "faq/SET_ID";
export const SET_PAGES = "faq/SET_PAGES";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});
export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});
export const setAdminBankData = (payload) => ({
  type: SET_ADMIN_BANK_DATA,
  payload,
});
export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});
export const setIsEdit = (payload) => ({
  type: SET_IS_EDIT,
  payload,
});
export const setDetailData = (payload) => ({
  type: SET_DETAIL_DATA,
  payload,
});
export const setId = (payload) => ({
  type: SET_ID,
  payload,
});
export const setPages = (payload) => ({
  type: SET_PAGES,
  payload,
});

export const handleFaqList = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetFaq(payload);
    if (response.status === 200) {
      dispatch(setData(response?.data));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response?.status)) {
      dispatch(setLoading(false));
      dispatch(setData([]));
      // dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};

export const handleFaqAdminBankList = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceGetFaqAdminBank(payload);
    if (response.status === 200) {
      dispatch(setAdminBankData(response?.data));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response?.status)) {
      dispatch(setLoading(false));
      dispatch(setData([]));
      // dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};

export const handleFaqSave = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceFaqSave(payload);
    if (response.status === 200) {
      dispatch(setLoading(false));
    } else if (handleExceptionError(response?.status)) {
      dispatch(setLoading(false));
      dispatch(setData([]));
      // dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};
