// progressBarAction Actions
// --------------------------------------------------------

import moment from "moment";
import {
  hanldeValidationDataLms,
  serviceCalendaerEventList,
  serviceCalendarEventAdd,
  serviceCalendarEventDelete,
  serviceCalendarEventEdit,
} from "utils/api";
import { checkOnline, handleErrorBE } from "../errorGeneral";

export const SET_LOADING = "calendarEvent/SET_LOADING";
export const INIT_DATA = "calendarEvent/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "calendarEvent/SET_DOUBLE_SUBMIT";
export const SET_EVENT_BY_DATE = "calendarEvent/SET_EVENT_BY_DATE";
export const SET_ISSUBMIT = "calendarEvent/SET_ISSUBMIT";
export const SET_EVENT = "calendarEvent/SET_EVENT";
export const SET_IS_ALREADY_EXIST = "calendarEvent/SET_IS_ALREADY_EXIST";

export const setEvent = (payload) => ({
  type: SET_EVENT,
  payload,
});

export const setIsSubmit = (payload) => ({
  type: SET_ISSUBMIT,
  payload,
});

export const setEventByDate = (payload) => ({
  type: SET_EVENT_BY_DATE,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setEventAlreadyExist = (payload) => ({
  type: SET_IS_ALREADY_EXIST,
  payload,
});

export const getEventList = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoading(true));

  try {
    const response = await serviceCalendaerEventList(payload);
    dispatch(setLoading(false));

    if (response.status === 200) {
      const data = [];

      response.data.calendarEventList.forEach((e) => {
        e.calendarEventList.forEach((c) => {
          data.push({
            ...c,
            id: c.id,
            title: c?.name,
            start: moment(c.eventDate).toDate(),
            end: moment(c.eventDate).add(30, "minute").toDate(),
            allDay: true,
          });
        });
      });

      dispatch(setInitData(data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    console.error(e);
  }
};

export const handleSaveCalendarEvent = (payload, cb) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setIsSubmit(true));
    const response = await serviceCalendarEventAdd(payload);
    dispatch(setIsSubmit(false));
    if (response.status === 200) {
      cb();
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    console.log(e);
  }
};

export const handleEditCalendarEvent = (payload, cb) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setIsSubmit(true));
    const response = await serviceCalendarEventEdit(payload);
    dispatch(setIsSubmit(false));
    if (response.status === 200) {
      cb();
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    console.log(e);
  }
};

export const handleDeleteCalendarEvent = (payload, cb) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setIsSubmit(true));
    const response = await serviceCalendarEventDelete(payload);
    dispatch(setIsSubmit(false));
    if (response.status === 200) {
      cb();
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    console.log(e);
  }
};

export const getValidationEvent = (payload) => async (dispatch) => {
  try {
    const res = await hanldeValidationDataLms(payload);

    if (res.status === 200) {
      dispatch(setEventAlreadyExist(!res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.log(error);
  }
};
