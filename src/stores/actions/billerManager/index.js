/* eslint-disable array-callback-return */
// billerManager Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

import {
  serviceBillerCategoryList,
  serviceBillerManagerList,
  serviceSaveBillerManager,
  serviceSaveBillerManagerDelete,
  serviceBillerManagerCategoriesGet,
  serviceBillerManagerSpecGet,
  serviceBillerManagerTransactionCategoriesGet,
  serviceBillerManagerTransactionGroupGet,
  serviceBillerManagerGetByPayeeCode,
  serviceBillerManagerGetPrefixByPayeeCode,
  serviceBillerManagerGetDenomListByPayeeCode,
  serviceBillerManagerGetTemplateListByPayeeCode,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "billerManager/SET_LOADING";
export const SET_LOADING_EXCUTE = "billerManager/SET_LOADING_EXCUTE";
export const CLEAR_ERROR = "billerManager/CLEAR_ERROR";
export const SET_ERROR = "billerManager/SET_ERROR";
export const INIT_DATA = "billerManager/INIT_DATA";
export const SET_DATA = "billerManager/SET_DATA";
export const SET_DETAIL = "billerManager/SET_DETAIL";
export const SET_CATEGORIES = "billerManager/SET_CATEGORIES";
export const SET_TRANSACTION_CATEGORIES =
  "billerManager/SET_TRANSACTION_CATEGORIES";
export const SET_SPECS = "billerManager/SET_SPECS";
export const SET_TRANSACTION_GROUPS = "billerManager/SET_TRANSACTION_GROUPS";
export const SET_ID = "billerManager/SET_ID";
export const SET_SUCCESS = "billerManager/SET_SUCCESS";
export const SET_OPTION_LIST = "billerManager/SET_OPTION_LIST";
export const SET_BILLER_DENOM_LIST = "billerManager/SET_BILLER_DENOM_LIST";
export const SET_DENOM_FORM_MODE = "billerManager/SET_DENOM_FORM_MODE";
export const SET_BILLER_TEMPLATE_LIST =
  "billerManager/SET_BILLER_TEMPLATE_LIST";
export const SET_TEMPLATE_FORM_MODE = "billerManager/SET_TEMPLATE_FORM_MODE";
export const CLEAR_FORM = "billerManager/CLEAR_FORM";
export const SET_PREFIXES = "billerManager/SET_PREFIXES";
export const SET_DENOM_LIST = "billerManager/SET_DENOM_LIST";
export const SET_TEMPLATE_LIST = "billerManager/SET_TEMPLATE_LIST";
export const SET_DATA_LIST = "billerManager/SET_DATA_LIST";
export const SET_IS_LIST_FIELD = "billerManager/IS_LIST_FIELD";

export const setId = (payload) => ({
  type: SET_ID,
  payload,
});

export const setDenomList = (payload) => ({
  type: SET_DENOM_LIST,
  payload,
});

export const setTemplateList = (payload) => ({
  type: SET_TEMPLATE_LIST,
  payload,
});

export const setCategories = (payload) => ({
  type: SET_CATEGORIES,
  payload,
});

export const setTransactionCategories = (payload) => ({
  type: SET_TRANSACTION_CATEGORIES,
  payload,
});
export const setSpecs = (payload) => ({
  type: SET_SPECS,
  payload,
});
export const setTransactionGroups = (payload) => ({
  type: SET_TRANSACTION_GROUPS,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setClearError = () => ({
  type: CLEAR_ERROR,
});

export const setSuccess = (payload) => ({
  type: SET_SUCCESS,
  payload,
});

export const setOptionsList = (payload) => ({
  type: SET_OPTION_LIST,
  payload,
});

export const setBillerDenomList = (payload) => ({
  type: SET_BILLER_DENOM_LIST,
  payload,
});

export const setDenomFormMode = (payload) => ({
  type: SET_DENOM_FORM_MODE,
  payload,
});

export const setListField = (payload) => ({
  type: SET_BILLER_TEMPLATE_LIST,
  payload,
});

export const setTemplateFormMode = (payload) => ({
  type: SET_TEMPLATE_FORM_MODE,
  payload,
});

export const setPrefixes = (payload) => ({
  type: SET_PREFIXES,
  payload,
});

export const setDataList = (payload) => ({
  type: SET_DATA_LIST,
  payload,
});

export const clearForm = () => ({
  type: CLEAR_FORM,
});

export const setIsListField = (payload) => ({
  type: SET_IS_LIST_FIELD,
  payload,
});

export const actionBillerManagerGet =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));
    try {
      const response = await serviceBillerManagerList(payload);
      dispatch(setLoading(false));

      if (response.status === 200) {
        dispatch(setData(response?.data));
      } else if (handleExceptionError(response.status)) {
        dispatch(setData(null));
        throw response;
      }
    } catch (error) {
      // dispatch(handleErrorBE(error));
    }
  };

export const actionBillerManagerCategoriesGet = () => async (dispatch) => {
  try {
    const response = await serviceBillerManagerCategoriesGet();

    if (response.status === 200) {
      const data = response.data.map(({ value, label }) => ({
        value: value.toString(),
        label,
      }));
      dispatch(setCategories(data));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionBillerManagerTransactionCategoriesGet =
  () => async (dispatch) => {
    try {
      const response = await serviceBillerManagerTransactionCategoriesGet();

      if (response.status === 200) {
        const payload = response.data.map(({ value, labelEng }) => ({
          value: value.toString(),
          label: labelEng,
        }));
        dispatch(setTransactionCategories(payload));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionBillerManagerSpecGet = () => async (dispatch) => {
  try {
    const response = await serviceBillerManagerSpecGet();

    if (response.status === 200) {
      // console.log("ini specs", response.data);
      // const payload = response.data.map(({ index, value }) => ({
      //   value: index.toString(),
      //   label: value,
      // }));
      // dispatch(setTransactionCategories(payload));
    } else {
      throw response;
    }
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionBillerManagerTransactionGroupGet =
  () => async (dispatch) => {
    try {
      const response = await serviceBillerManagerTransactionGroupGet();

      if (response.status === 200) {
        const data = response.data.map(({ value, label }) => ({
          value: value.toString(),
          label,
        }));
        dispatch(setTransactionGroups(data));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionOptionsListGet = (payload) => async (dispatch, getState) => {
  try {
    const [categoriesRes] = await Promise.all([serviceBillerCategoryList()]);

    const categories = categoriesRes?.data?.billerCategoryDtoList || [];

    const mapCategories = categories.map((item) => ({
      label: item.code,
      value: item.id,
    }));

    dispatch(
      setOptionsList({
        categories: mapCategories || [],
      })
    );
  } catch (error) {
    dispatch(handleErrorBE(error));
  }
};

export const actionSaveBillerManager = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingExcute(true));
  try {
    const res = await serviceSaveBillerManager(payload);
    dispatch(setLoadingExcute(false));

    if (res.status === 200) {
      dispatch(setSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setError(err?.message));
  }
};

export const actionDeleteBiller =
  (payload, setDeleteConfirmation) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoadingExcute(true));
    try {
      const response = await serviceSaveBillerManagerDelete(payload);
      dispatch(setLoadingExcute(false));
      setDeleteConfirmation(false);

      if (response.status === 200) {
        dispatch(setSuccess(true));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionBillerManagerGetByPayeeCode =
  (payload) => async (dispatch) => {
    try {
      const response = await serviceBillerManagerGetByPayeeCode(payload);
      if (response.status === 200) {
        dispatch(setDetail(response.data));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionBillerManagerGetPrefixByPayeeCode =
  (payload) => async (dispatch) => {
    try {
      const response = await serviceBillerManagerGetPrefixByPayeeCode(payload);
      if (response.status === 200) {
        const payload = response.data.billerPrefix.map(
          ({ billerCategory, prefix, payeeCode }) => ({
            billerCategory,
            prefix,
            payeeCode,
          })
        );
        dispatch(setPrefixes(payload));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionBillerManagerGetDenomListByPayeeCode =
  (payload) => async (dispatch) => {
    try {
      const response = await serviceBillerManagerGetDenomListByPayeeCode(
        payload
      );
      if (response.status === 200) {
        const payload = response.data.billerDenom.map(
          ({
            noOrder,
            labelId,
            labelEn,
            denominationCategory,
            denominationValue,
            payeeCode,
          }) => ({
            noOrder,
            labelId,
            labelEn,
            denominationCategory,
            denominationValue,
            payeeCode,
          })
        );
        dispatch(setDenomList(payload));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const actionBillerManagerGetTemplateListByPayeeCode =
  (payload) => async (dispatch) => {
    try {
      const response = await serviceBillerManagerGetTemplateListByPayeeCode(
        payload
      );
      if (response.status === 200) {
        const payload = response.data.billerTemplate.map(
          ({
            payeeCode,
            lineNum,
            nameId,
            nameEn,
            placeHolderId,
            placeHolderEn,
            inputType,
            templateType,
            fieldType,
            maxFieldLength,
            minFieldLength,
          }) => ({
            payeeCode,
            lineNum,
            nameId,
            nameEn,
            placeHolderId,
            placeHolderEn,
            inputType,
            templateType,
            fieldType,
            maxFieldLength,
            minFieldLength,
          })
        );
        dispatch(setTemplateList(payload));
      } else {
        throw response;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };
