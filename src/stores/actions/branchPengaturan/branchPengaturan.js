// import { privilegeCONFIG } from "configuration/privilege";

import {
  serviceRequestBranchPengaturan,
  serviceRequestBranchPengaturanSync,
} from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_BRANCH_PENGATURAN_TYPE =
  "branchPengaturan/SET_BRANCH_PENGATURAN_TYPE";

export const SET_BRANCH_PENGATURAN_UPDATE_TYPE =
  "branchPengaturan/SET_BRANCH_PENGATURAN_UPDATE_TYPE";

export const SET_BRANCH_PENGATURAN_ERROR =
  "branchPengaturan/SET_BRANCH_PENGATURAN_ERROR";

export const SET_HANDLE_ERROR = "branchPengaturan/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "branchPengaturan/SET_HANDLE_CLEAR_ERROR";

export const SET_LOADING = "branchPengaturan/SET_LOADING";
export const SET_LOADINGSYNC = "branchPengaturan/SET_LOADINGSYNC";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setTypeBranchPengaturan = (payload) => ({
  type: SET_BRANCH_PENGATURAN_TYPE,
  payload,
});

export const setTypeErrorBranchPengaturan = (payload) => ({
  type: SET_BRANCH_PENGATURAN_ERROR,
  payload,
});
export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});
export const setLoadingSync = (payload) => ({
  type: SET_LOADINGSYNC,
  payload,
});

export const getDataBranchPengaturan = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestBranchPengaturan(payload);
    dispatch(setLoading(false));
    // console.warn("data BranchKategori:", res);
    if (res.status === 200) {
      dispatch(setTypeBranchPengaturan(res?.data));
    } else {
      dispatch(setTypeBranchPengaturan([]));
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setHandleError(err.message));
  }
};

export const getDataBranchPengaturanButtonSync = () => async (dispatch) => {
  dispatch(setLoadingSync(1));
  try {
    const res = await serviceRequestBranchPengaturanSync({});

    dispatch(setLoadingSync(0));

    if (res.status === 200) {
      return true;
    }
    dispatch(handleErrorBE(res));
  } catch (err) {
    dispatch(setHandleError(err.message));
  }
};
