// pengaturanKelompok Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
import {
  serviceRequestPengaturanKelompokDetail,
  serviceRequestPengaturanKelompok,
  serviceRequestPengaturanKelompokUserFeatures,
  serviceRequestPengaturanKelompokLimit,
  serviceRequestPengaturanKelompokDelete,
  serviceRequestPengaturanKelompokAdd,
  serviceGetKelompokRekening,
  serviceValidateCodeAndName,
} from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { setLoadingGlobal } from "stores/actions/pageContainer";
import { getLocalStorage } from "utils/helpers";

export const SET_LOADING = "pengaturanKelompok/SET_LOADING";
export const CLEAR_ERROR = "pengaturanKelompok/CLEAR_ERROR";
export const SET_ERROR = "pengaturanKelompok/SET_ERROR";
export const INIT_DATA = "pengaturanKelompok/INIT_DATA";
export const SET_COMPANY_ID = "pengaturanKelompok/SET_COMPANY_ID";
export const SET_DOUBLE_SUBMIT = "pengaturanKelompok/SET_DOUBLE_SUBMIT";
export const SET_DATA_GROUP = "pengaturankelompok/SET_DATA_GROUP";
export const SET_DATA_GROUP_DETAIL = "pengaturankelompok/SET_DATA_GROUP_DETAIL";
export const SET_DATA_FEATURES = "pengaturankelompok/SET_DATA_FEATURES";
export const SET_DATA_LIST_FEATURES =
  "pengaturankelompok/SET_DATA_LIST_FEATURES";
export const SET_DATA_LIMIT = "pengaturanKelompok/SET_DATA_LIMIT";
export const SET_EDIT_GROUP = "pengaturanKelompok/SET_EDIT_GROUP";
export const SET_PENGATURAN_KELOMPOK_CLEAR_ERROR =
  "pengaturanKelompok/ SET_PENGATURAN_KELOMPOK_CLEAR_ERROR";
export const SET_POP_UP_DELETE_SUCCESS =
  "pengaturanKelompok/SET_POP_UP_DELETE_SUCCESS";
export const SET_POP_UP_ADD_SUCCESS =
  "pengaturanKelompok/SET_POP_UP_ADD_SUCCESS";
export const SET_CORPORATE_SEGMENTATION_ID =
  "pengaturanKelompok/SET_CORPORATE_SEGMENTATION_ID";
export const SET_LIST_GROUP_REKENING =
  "pengaturanKelompok/SET_LIST_GROUP_REKENING";
export const SET_LOADING_LIMIT = "pengaturanKelompok/SET_LOADING_LIMIT";
export const SET_IS_VALID_CODE = "pengaturanKelompok/SET_IS_VALID_CODE";
export const SET_IS_VALID_NAME = "pengaturanKelompok/SET_IS_VALID_NAME";

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingLimit = (payload) => ({
  type: SET_LOADING_LIMIT,
  payload,
});

export const setListGroupRekening = (payload) => ({
  type: SET_LIST_GROUP_REKENING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setDataGroup = (payload) => ({
  type: SET_DATA_GROUP,
  payload,
});

export const setDataGroupDetail = (payload) => ({
  type: SET_DATA_GROUP_DETAIL,
  payload,
});

export const setDataUserFeatures = (payload) => ({
  type: SET_DATA_FEATURES,
  payload,
});

export const setDataListFeatures = (payload) => ({
  type: SET_DATA_LIST_FEATURES,
  payload,
});

export const setDataLimit = (payload) => ({
  type: SET_DATA_LIMIT,
  payload,
});

export const setCompanyId = (payload) => ({
  type: SET_COMPANY_ID,
  payload,
});

export const setPopUpDeleteSuccess = (payload) => ({
  type: SET_POP_UP_DELETE_SUCCESS,
  payload,
});

export const setPopUpAddSuccess = (payload) => ({
  type: SET_POP_UP_ADD_SUCCESS,
  payload,
});

export const setCorporateSegmentationId = (payload) => ({
  type: SET_CORPORATE_SEGMENTATION_ID,
  payload,
});

export const setDataEditGroup = (payload) => ({
  type: SET_EDIT_GROUP,
  payload,
});

export const setClearError = () => ({
  type: SET_PENGATURAN_KELOMPOK_CLEAR_ERROR,
});

export const setIsCodeValid = (payload) => ({
  type: SET_IS_VALID_CODE,
  payload,
});

export const setIsNameValid = (payload) => ({
  type: SET_IS_VALID_NAME,
  payload,
});

export const actionValidateCodeAndName =
  (payload) => async (dispatch, getState) => {
    const { idPerusahaan } = getState().managmentCompany;
    const isCode = payload.code !== "";
    const isName = payload.name !== "";
    try {
      const res = await serviceValidateCodeAndName({
        ...payload,
        corporateProfileId: idPerusahaan || payload?.corporateProfileId,
      });

      if (res.status === 200) {
        if (isCode) dispatch(setIsCodeValid(res.data));
        if (isName) dispatch(setIsNameValid(res.data));
      } else {
        throw res;
      }
    } catch (error) {
      dispatch(handleErrorBE(error));
    }
  };

export const fetchListGroup = (payload) => async (dispatch, getState) => {
  const { idPerusahaan } = getState().managmentCompany;
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestPengaturanKelompok({
      ...payload,
      corporateProfileId: idPerusahaan || payload?.corporateProfileId,
    });

    const data =
      res?.data?.getGroupDtoList?.map((item) => ({
        ...item,
        id: item.accountGroupId,
      })) || [];

    if (res.status === 200) {
      dispatch(setDataGroup(data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  } finally {
    dispatch(setLoading(false));
  }
};

export const deleteGroup = (payload) => async (dispatch, getState) => {
  const { idPerusahaan } = getState().managmentCompany;
  try {
    dispatch(setLoadingGlobal(true));
    const res = await serviceRequestPengaturanKelompokDelete({
      ...payload,
      // corporateProfileId: 5,
      corporateProfileId: idPerusahaan || payload?.corporateProfileId,
    });

    if (res.status === 200) {
      dispatch(setPopUpDeleteSuccess(true));
    } else {
      dispatch(handleErrorBE(res, false));
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  } finally {
    dispatch(setLoadingGlobal(false));
  }
};
export const addGroup = (payload) => async (dispatch, getState) => {
  const { idPerusahaan } = getState().managmentCompany;
  try {
    dispatch(setLoadingGlobal(true));
    const res = await serviceRequestPengaturanKelompokAdd({
      ...payload,
      // corporateProfileId: 5,
      corporateProfileId: idPerusahaan || payload?.corporateProfileId,
    });
    if (res.status === 200) {
      dispatch(setPopUpAddSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  } finally {
    dispatch(setLoadingGlobal(false));
  }
};
export const fetchListGroupDetail = (payload) => async (dispatch, getState) => {
  const { idPerusahaan } = getState().managmentCompany;
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestPengaturanKelompokDetail({
      ...payload,
      // corporateProfileId: 5,
      corporateProfileId: idPerusahaan || payload?.corporateProfileId,
    });

    if (res.status === 200) {
      dispatch(setDataGroupDetail(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  } finally {
    dispatch(setLoading(false));
  }
};

export const fetchListDataLimit = (payload) => async (dispatch, getState) => {
  const dataIdPrusahaan = getLocalStorage("idPerusahaan")
    ? JSON.parse(getLocalStorage("idPerusahaan")).id
    : "";
  try {
    dispatch(setLoadingLimit(true));
    if (dataIdPrusahaan !== null) {
      const res = await serviceRequestPengaturanKelompokLimit({
        corporateProfileId: dataIdPrusahaan,
      });

      if (res.status === 200) {
        dispatch(setDataLimit(res.data));
      } else {
        dispatch(handleErrorBE(res));
      }
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  } finally {
    dispatch(setLoadingLimit(false));
  }
};

export const fetchListGroupRekening = () => async (dispatch) => {
  try {
    const dataIdPrusahaan = getLocalStorage("idPerusahaan")
      ? JSON.parse(getLocalStorage("idPerusahaan")).id
      : "";
    dispatch(setLoading(true));
    if (dataIdPrusahaan) {
      const res = await serviceGetKelompokRekening({
        corporateProfileId: dataIdPrusahaan,
      });

      if (res.status === 200) {
        dispatch(setListGroupRekening(res.data?.groupAccounts));
      } else {
        dispatch(handleErrorBE(res));
      }
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  } finally {
    dispatch(setLoading(false));
  }
};

export const fethListDataUserFeatures = (payload) => async (dispatch) => {
  try {
    const dataIdPrusahaan = getLocalStorage("idPerusahaan")
      ? JSON.parse(getLocalStorage("idPerusahaan")).id
      : "";
    const res = await serviceRequestPengaturanKelompokUserFeatures({
      ...payload,
      // corporateProfileId: 18,
      corporateProfileId: dataIdPrusahaan,
    });

    const result = res.data.listMenu
      .filter((item) => item.parentId === "" || item.parentId === null)
      .map((item2) => ({
        title: item2.nameEn,
        menuId: item2.menuId,
        isApprovalMatrixMenu: item2.isApprovalMatrixMenu,
        isFinancialMenu: false,
        child: [],
      }));

    res.data.listMenu.forEach((item) => {
      result.forEach((item2) => {
        if (item.parentId === item2.menuId) {
          item2.child.push({ ...item, isFinancialMenu: false });
        }
      });
    });

    if (res.status === 200) {
      dispatch(setDataListFeatures(res.data));
      dispatch(setDataUserFeatures(result));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandlingError(error?.message));
  }
};
