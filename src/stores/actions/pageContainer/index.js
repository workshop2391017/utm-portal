// pageContainer Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "pageContainer/SET_LOADING";
export const CLEAR_ERROR = "pageContainer/CLEAR_ERROR";
export const SET_ERROR = "pageContainer/SET_ERROR";
export const INIT_DATA = "pageContainer/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "pageContainer/SET_DOUBLE_SUBMIT";

export const setLoadingGlobal = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});
