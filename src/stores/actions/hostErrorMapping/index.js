import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

const {
  serviceRequestHostErrorMapping,
  serviceRequestHostErrorMappingAdd,
  serviceRequestHostErrorMappingEdit,
  requestDeleteHostErrorMapping,
} = require("utils/api");

export const SET_HOST_ERROR_MAPPING_TYPE =
  "dashboard/SET_HOST_ERROR_MAPPING_TYPE";
export const SET_HOST_ERROR_MAPPING_LOADING =
  "dashboard/SET_HOST_ERROR_MAPPING_LOADING";

export const SET_HOST_ERROR_MAPPING_ERROR =
  "dashboard/SET_HOST_ERROR_MAPPING_ERROR";

export const SET_HOST_ERROR_MAPPING_CLEAR_ERROR =
  "dashboard/SET_HOST_ERROR_MAPPING_CLEAR_ERROR";
export const SET_HOST_ERROR_MAPPING_SUCCESS =
  "dashboard/SET_HOST_ERROR_MAPPING_SUCCESS";
export const SET_HOST_ERROR_MAPPING_DELETE_CONFIRM =
  "dashboard/SET_HOST_ERROR_MAPPING_DELETE_CONFIRM";

export const SET_HOST_ERROR_MAPPING_OPEN_MODAL =
  "dashboard/SET_HOST_ERROR_MAPPING_OPEN_MODAL";

export const SET_HOST_ERROR_MAPPING_LOADING_EXCUTE =
  "dashboard/SET_HOST_ERROR_MAPPING_LOADING_EXCUTE";

export const SET_HOST_ERROR_MAPPING_OPEN_DELETE =
  "dashboard/SET_HOST_ERROR_MAPPING_OPEN_DELETE";

export const setTypeHostErrorMapping = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_TYPE,
  payload,
});

export const setTypeLoading = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_LOADING,
  payload,
});

export const setTypeLoadingExcute = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_LOADING_EXCUTE,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_ERROR,
  payload,
});

export const setTypeClearError = () => ({
  type: SET_HOST_ERROR_MAPPING_CLEAR_ERROR,
});

export const setTypeSuccess = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_SUCCESS,
  payload,
});

export const deleteConfirm = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_DELETE_CONFIRM,
  payload,
});

export const setTypeOpenModal = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_OPEN_MODAL,
  payload,
});

export const setTypeOpenDelete = (payload) => ({
  type: SET_HOST_ERROR_MAPPING_OPEN_DELETE,
  payload,
});

export const getDataHostErrorMapping = (payload) => async (dispatch) => {
  try {
    dispatch(setTypeLoading(true));
    const res = await serviceRequestHostErrorMapping(payload);
    dispatch(setTypeLoading(false));

    if (res.status === 200) {
      dispatch(setTypeHostErrorMapping(res.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err?.message));
  }
};

export const addDataHostErrorMapping = (payload, opt) => async (dispatch) => {
  dispatch(
    validateTask(
      {
        menuName: validateTaskConstant.HOST_ERROR_MAPPING,
        code: payload?.code,
        id: null,
        name: payload?.sourceSystem,
        validate: payload?.id === null || payload?.id === undefined, // karena udah divalidasi pas klik button edit
        loader: setTypeLoading,
      },
      {
        async onContinue() {
          try {
            dispatch(setTypeLoadingExcute(true));
            const res = await serviceRequestHostErrorMappingAdd(payload);
            dispatch(setTypeLoadingExcute(false));

            if (res.status === 200) {
              if (opt?.handleSaveClear) opt?.handleSaveClear();
              dispatch(setTypeSuccess(true));
            } else {
              dispatch(handleErrorBE(res));
            }
          } catch (err) {
            dispatch(setTypeError(err?.message));
          }
        },
        onError() {
          opt?.handleNext();
        },
      },
      {
        redirect: false,
      }
    )
  );
};

export const editDataHostErrorMapping = (payload, opt) => async (dispatch) => {
  try {
    dispatch(setTypeLoadingExcute(true));
    const res = await serviceRequestHostErrorMappingEdit(payload);
    dispatch(setTypeLoadingExcute(false));

    if (res.status === 200) {
      if (opt?.handleSaveClear) opt?.handleSaveClear();
      dispatch(setTypeSuccess(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err?.message));
  }
};

export const DeleteDataHostErrorMapping = (payload) => async (dispatch) => {
  try {
    dispatch(setTypeLoadingExcute(true));
    const res = await requestDeleteHostErrorMapping(payload);
    dispatch(setTypeLoadingExcute(false));

    if (res.status === 200) {
      dispatch(setTypeOpenDelete(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err?.message));
  }
};
