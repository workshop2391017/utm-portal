// actions Role
// --------------------------------------------------------
import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceGetAllPrivilege,
  serviceGetRoleDetail,
  serviceManagementRole,
  serviceRequestDeleteRole,
  serviceRequestSaveRole,
} from "utils/api";
import { getLocalStorage } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "pengaturanperan/SET_LOADING";
export const SET_LOADING_PRIVILEGE = "pengaturanperan/SET_LOADING_PRIVILEGE";
export const SET_LOADING_DETAIL = "pengaturanperan/SET_LOADING_DETAIL";
export const SET_LOADING_SUBMIT = "pengaturanperan/SET_LOADING_SUBMIT";
export const SET_GET_ALL_ROLE = "pengaturanperan/SET_GET_ALL_ROLE";
export const SET_ALL_PRIVILEGE = "pengaturanperan/SET_ALL_PRIVILEGE";
export const SET_DETAIL_ROLE = "pengaturanperan/SET_DETAIL_ROLE";
export const SET_DUPLICATE = "pengaturanperan/SET_DUPLICATE";
export const SET_CLEAR_DETAIL = "pengaturanperan/SET_CLEAR_DETAIL";
export const SET_LOADING_DUPLICATE = "pengaturanperan/SET_LOADING_DUPLICATE";
export const SET_CLEAR_DUPLICATE = "pengaturanperan/SET_CLEAR_DUPLICATE";
export const SET_HANDLE_ERROR = "pengaturanperan/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "pengaturanperan/SET_HANDLE_CLEAR_ERROR";
export const SET_HANDLE_DUPLICATE_CLEAR =
  "pengaturanperan/SET_HANDLE_DUPLICATE_CLEAR";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setLoadingPrivilege = (payload) => ({
  type: SET_LOADING_PRIVILEGE,
  payload,
});

export const setGetAllRole = (payload) => ({
  type: SET_GET_ALL_ROLE,
  payload,
});

export const setAllPrivilegedata = (payload) => ({
  type: SET_ALL_PRIVILEGE,
  payload,
});

export const setLoadingDetail = (payload) => ({
  type: SET_LOADING_DETAIL,
  payload,
});

export const setDetailRole = (payload) => ({
  type: SET_DETAIL_ROLE,
  payload,
});

export const setLoadingSubmit = (payload) => ({
  type: SET_LOADING_SUBMIT,
  payload,
});

export const setDuplicate = (payload) => ({
  type: SET_DUPLICATE,
  payload,
});

export const setClearDetail = () => ({
  type: SET_CLEAR_DETAIL,
});

export const setLoadingDuplicate = (payload) => ({
  type: SET_LOADING_DUPLICATE,
  payload,
});

export const setClearDuplicate = () => ({
  type: SET_CLEAR_DUPLICATE,
});

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const handleGetAllRole = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceManagementRole(
      payload,
      privilegeCONFIG.MANAGEMENT_ROLE.LIST
    );
    dispatch(setLoading(false));
    if (response.status === 200) {
      dispatch(setGetAllRole(response?.data?.olistRole));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (err) {
    dispatch(setHandleError(err.message));
    console.warn("Error catch", err);
  }
};

export const handleGetAllPrivilege = () => async (dispatch) => {
  try {
    dispatch(setLoadingPrivilege(true));
    const response = await serviceGetAllPrivilege(
      privilegeCONFIG.MANAGEMENT_ROLE.LIST
    );
    dispatch(setLoadingPrivilege(false));
    if (response.status === 200) {
      dispatch(setAllPrivilegedata(response?.data));
    } else if (response.status === 400) {
      dispatch(setAllPrivilegedata([]));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (err) {
    dispatch(setHandleError(err.message));
  }
};

export const handleGetRoleDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoadingDetail(true));
    const response = await serviceGetRoleDetail(
      payload,
      privilegeCONFIG.MANAGEMENT_ROLE.LIST
    );
    dispatch(setLoadingDetail(false));
    if (response.status === 200) {
      dispatch(setDetailRole(response?.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (err) {
    dispatch(setHandleError(err.message));
  }
};

export const handleDuplicate = (payload) => async (dispatch) => {
  try {
    dispatch(setLoadingDuplicate(true));
    const response = await serviceGetRoleDetail(
      { roleId: payload.roleId },
      privilegeCONFIG.MANAGEMENT_ROLE.LIST
    );
    dispatch(setLoadingDuplicate(false));
    if (response.status === 200) {
      dispatch(
        setDuplicate({
          ...response?.data,
          userTotal: payload.userTotal,
          deleted: payload.deleted,
        })
      );
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (err) {
    dispatch(setHandleError(err.message));
  }
};

export const handleRequestSaveRole =
  (payload, checkListIds) => async (dispatch) => {
    const requestPayload = {
      roleId: payload?.roleId ?? null,
      roleName: payload?.roleName,
      requestComment: payload?.requestComment,
      privilegeId: [...new Set(checkListIds)],
    };

    try {
      dispatch(setLoadingSubmit(true));
      const response = await serviceRequestSaveRole(
        requestPayload,
        privilegeCONFIG.MANAGEMENT_ROLE.LIST
      );
      dispatch(setLoadingSubmit(false));
      if (response.status === 200) {
        dispatch(setClearDetail());
        dispatch(setClearDuplicate());
        return true;
      }
      dispatch(handleErrorBE(response));
      return false;
    } catch (err) {
      dispatch(setHandleError(err.message));
    }
  };

export const handleRequestDeleteRole =
  ({ deleted, ...rest }) =>
  async (dispatch) => {
    const payloadRequest = {
      roleId: rest.roleId,
    };
    try {
      dispatch(setLoadingSubmit(true));
      const response = await serviceRequestDeleteRole(
        payloadRequest,
        privilegeCONFIG.MANAGEMENT_ROLE.LIST
      );
      dispatch(setLoadingSubmit(false));
      if (response.status === 200) {
        return true;
      }
      dispatch(handleErrorBE(response));
      return false;
    } catch (err) {
      dispatch(setHandleError(err.message));
    }
  };
