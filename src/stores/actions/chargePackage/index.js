// chargePackage Actions
// --------------------------------------------------------

import {
  serviceChargeLIstWithoutpaging,
  serviceChargePackageSave,
  serviceDetailChargePackage,
  serviceExecuteChargePackage,
  serviceGetChargePackage,
  serviceRequestGetCurrency,
  serviceRequestGetTearingSlab,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";
import { validateTask } from "../validateTaskPortal";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "chargePackage/SET_LOADING";
export const CLEAR_ERROR = "chargePackage/CLEAR_ERROR";
export const SET_ERROR = "chargePackage/SET_ERROR";
export const INIT_DATA = "chargePackage/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "chargePackage/SET_DOUBLE_SUBMIT";
export const SET_INITIAL_DATA = "chargePackage/SET_INITIAL_DATA";
export const SET_CLEAR = "chargePackage/SET_CLEAR";
export const SET_DETAIL_DATA = "chargePackage/SET_DETAIL_DATA";
export const SET_DETAIL_DATA_TEMP = "chargePackage/SET_DETAIL_DATA_TEMP";
export const SET_LOADING_ECECUTE = "chargePackage/SET_LOADING_ECECUTE";
export const SET_SUCCESS_EXECUTE = "chargePackage/SET_SUCCESS_EXECUTE";
export const SET_LOADING_CURRENCY = "chargePackage/SET_LOADING_CURRENCY";
export const SET_LOADING_TIERINGSLAB = "chargePackage/SET_LOADING_TIERINGSLAB";
export const SET_LOADING_SERVICES = "chargePackage/SET_LOADING_SERVICES";
export const SET_CURRENCY = "chargePackage/SET_CURRENCY";
export const SET_TIERINGSLAB = "chargePackage/SET_TIERINGSLAB";
export const SET_SERVICES = "chargePackage/SET_SERVICES";
export const SET_FORMDATA = "chargePackage/SET_FORMDATA";
export const SET_SAVE_SUCCESS = "chargePackage/SET_SAVE_SUCCESS";
export const SET_UPDATE_DETAIL = "chargePackage/SET_UPDATE_DETAIL";
export const SET_EDIT_DATA = "chargePackage/SET_EDIT_DATA";

export const setFormData = (payload) => ({
  type: SET_FORMDATA,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitialData = (payload) => ({
  type: SET_INITIAL_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setClear = (payload) => ({
  type: SET_CLEAR,
  payload,
});

export const setDetailData = (payload) => ({
  type: SET_DETAIL_DATA,
  payload,
});

export const setDetailDataTemp = (payload) => ({
  type: SET_DETAIL_DATA_TEMP,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_ECECUTE,
  payload,
});

export const setSuccessExecute = (payload) => ({
  type: SET_SUCCESS_EXECUTE,
  payload,
});

export const setLoadingCurrency = (payload) => ({
  type: SET_LOADING_CURRENCY,
  payload,
});

export const setLoadingTieringSlab = (payload) => ({
  type: SET_LOADING_TIERINGSLAB,
  payload,
});

export const setLoadingServices = (payload) => ({
  type: SET_LOADING_SERVICES,
  payload,
});

export const setCurrency = (payload) => ({
  type: SET_CURRENCY,
  payload,
});

export const setTieringSlab = (payload) => ({
  type: SET_TIERINGSLAB,
  payload,
});

export const setServices = (payload) => ({
  type: SET_SERVICES,
  payload,
});

export const setUpdateDetail = (payload) => ({
  type: SET_UPDATE_DETAIL,
  payload,
});

export const setEditData = (payload) => ({
  type: SET_EDIT_DATA,
  payload,
});

export const handleSaveChargePackage =
  (payload, validatePayload, handleNext = () => {}) =>
  async (dispatch, getState) => {
    const { services, detail } = getState().chargePackage;

    const srvc = [];

    if (payload?.id) {
      detail?.transactionalList?.forEach((elm) => {
        elm?.child?.forEach((cChild) => {
          srvc.push({
            chargePackageId: cChild?.chargePackageId,
            currencyId: cChild?.currency,
            id: cChild?.serviceChargePackageId,
            serviceChargeId: cChild?.serviceChargeId,
            serviceId: elm?.id,
            ...(cChild?.radiofixedAmount
              ? { fee: Number(cChild?.fixedAmount), tieringId: null }
              : cChild?.radioTieringSlab
              ? { tieringId: cChild?.tieringOrSlabId, fee: null }
              : {
                  fee: Number(cChild?.fixedAmount),
                  tieringId: cChild?.tieringOrSlabId,
                }),
          });
        });
      });
      detail?.periodicalList?.forEach((elm) => {
        elm?.child?.forEach((cChild) => {
          srvc.push({
            chargePackageId: cChild?.chargePackageId,
            currencyId: cChild?.currency,
            id: cChild?.serviceChargePackageId,
            serviceChargeId: cChild?.serviceChargeId,
            serviceId: elm?.id,
            ...(cChild?.radiofixedAmount
              ? { fee: Number(cChild?.fixedAmount), tieringId: null }
              : cChild?.radioTieringSlab
              ? { tieringId: cChild?.tieringOrSlabId, fee: null }
              : {
                  fee: Number(cChild?.fixedAmount),
                  tieringId: cChild?.tieringOrSlabId,
                }),
          });
        });
      });
    } else
      services?.forEach((elm, i) => {
        elm?.chargesList?.forEach((cChild, ci) => {
          srvc.push({
            currencyId: cChild?.currencyId,
            fee: Number(cChild?.fee),
            id: null,
            serviceChargeId: cChild?.serviceChargeId,
            tieringId: cChild?.tieringId,
            serviceId: elm?.service?.id,
          });
        });
      });

    const payloadSave = {
      chargePackage: {
        ...payload,
        id: payload?.id || null,
      },
      serviceChargePackageList: srvc,
    };
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.CHARGE_PACKAGE,
          name: payload?.id ? null : payload?.name,
          code: payload?.id ? null : payload?.code,
          id: payload?.id ? payload?.id : null,
          ...validatePayload,
          loader: setLoadingExecute,
        },
        {
          async onContinue() {
            dispatch(setLoadingExecute(true));
            try {
              const response = await serviceChargePackageSave(payloadSave);
              if (response.status === 200) {
                dispatch(setSuccessExecute(true));
                dispatch(setLoadingExecute(false));
              } else if (handleExceptionError(response.status)) {
                dispatch(handleErrorBE(response));
                dispatch(setLoadingExecute(false));
              }
            } catch (e) {
              dispatch(setError(e.message));
              dispatch(setLoading(true));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const handleGetChargePackage =
  (payload) => async (dispatch, getState) => {
    dispatch(setLoading(true));

    try {
      const response = await serviceGetChargePackage(payload);
      if (response.status === 200) {
        dispatch(setInitialData(response.data));
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
        dispatch(setInitialData([]));
        dispatch(setLoading(false));
      }
    } catch (e) {
      dispatch(setError(e.message));
      dispatch(setLoading(false));
      dispatch(setInitialData([]));
    }
  };

export const handleDetailChargePackage =
  (payload) => async (dispatch, getState) => {
    dispatch(setLoading(true));

    try {
      const response = await serviceDetailChargePackage(payload);
      if (response.status === 200) {
        const arr = [];
        response?.data?.periodicalList?.forEach((elm) => {
          arr.push({
            name: elm?.serviceName,
            serviceId: elm?.serviceId,
            child: elm.childMenu?.map((cElm) => ({
              ...cElm,
              existingField:
                cElm.fixedAmount || cElm.fixedAmount === 0
                  ? "fixedAmount"
                  : cElm.tieringOrSlabId
                  ? "tieringOrSlabId"
                  : null,
              radiofixedAmount:
                cElm.fixedAmount === 0 ? true : !!cElm.fixedAmount,
              radioTieringSlab: !!cElm.tieringOrSlabId,
            })),
            id: elm?.serviceId,
          });
        });

        const trxArr = [];
        // transactionalList
        response?.data?.transactionalList?.forEach((elm) => {
          trxArr.push({
            name: elm?.serviceName,
            serviceId: elm?.serviceId,
            child: elm.childMenu?.map((cElm) => ({
              ...cElm,
              existingField:
                cElm.fixedAmount || cElm.fixedAmount === 0
                  ? "fixedAmount"
                  : cElm.tieringOrSlabId
                  ? "tieringOrSlabId"
                  : null,
              radiofixedAmount:
                cElm.fixedAmount === 0 ? true : !!cElm.fixedAmount,
              radioTieringSlab: !!cElm.tieringOrSlabId,
            })),
            id: elm?.serviceId,
          });
        });

        dispatch(
          setDetailData({
            ...response.data,
            periodicalList: arr,
            transactionalList: trxArr,
          })
        );
        dispatch(
          setDetailDataTemp({
            copyData: response?.data,
          })
        );
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
        dispatch(setLoading(false));
      }
    } catch (e) {
      dispatch(setError(e.message));
      dispatch(setLoading(true));
    }
  };

export const handleExecuteChargePackage =
  (payload) => async (dispatch, getState) => {
    dispatch(setLoadingExecute(true));

    try {
      const response = await serviceExecuteChargePackage(payload);
      if (response.status === 200) {
        dispatch(setSuccessExecute(true));
        dispatch(setLoadingExecute(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
        dispatch(setLoadingExecute(false));
      }
    } catch (e) {
      dispatch(setError(e.message));
      dispatch(setLoadingExecute(true));
    }
  };

export const getServices = (payload) => async (dispatch) => {
  dispatch(setLoadingServices(true));

  try {
    const response = await serviceChargeLIstWithoutpaging(payload);
    if (response.status === 200) {
      const services = (response.data?.serviceChargeDtoList ?? [])
        .map((elm, i) => ({
          ...elm,
          ...elm?.service,
          key: `key-${i}`,
          chargesList: (elm?.chargesList ?? []).map((elm) => ({
            ...elm,
            currencyId: "IDR",
          })),
        }))
        .reduce((a, elm) => {
          if (elm.name === "Annual") {
            return [elm, ...a];
          }

          if (elm.name === "Monthly") {
            return [elm, ...a];
          }
          return [...a, elm];
        }, []);
      dispatch(setServices(services));

      dispatch(setLoadingServices(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoadingServices(false));
    }
  } catch (e) {
    dispatch(setError(e.message));
    dispatch(setLoadingServices(true));
  }
};

export const getCurrency = (payload) => async (dispatch) => {
  dispatch(setLoadingCurrency(true));

  try {
    const response = await serviceRequestGetCurrency(payload);
    if (response.status === 200) {
      dispatch(
        setCurrency(
          (response.data?.currencyList ?? []).map((elm) => ({
            ...elm,
            label: `${elm?.currencyCode} - ${elm?.nameEng}`,
            value: elm?.currencyCode,
          }))
        )
      );
      dispatch(setLoadingCurrency(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoadingCurrency(false));
    }
  } catch (e) {
    dispatch(setError(e.message));
    dispatch(setLoadingCurrency(true));
  }
};

export const getTieringSlab = (payload) => async (dispatch) => {
  dispatch(setLoadingTieringSlab(true));

  try {
    const response = await serviceRequestGetTearingSlab(payload);
    if (response.status === 200) {
      dispatch(
        setTieringSlab(
          (response.data?.tieringList ?? []).map((elm) => ({
            name: elm?.name, // -
            id: elm?.id, // -
            label: elm?.name,
            value: elm?.id,
          }))
        )
      );
      dispatch(setLoadingTieringSlab(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
      dispatch(setLoadingTieringSlab(false));
    }
  } catch (e) {
    dispatch(setError(e.message));
    dispatch(setLoadingTieringSlab(true));
  }
};
