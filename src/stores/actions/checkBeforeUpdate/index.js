// checkBeforeUpdate Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "checkBeforeUpdate/SET_LOADING";
export const CLEAR_ERROR = "checkBeforeUpdate/CLEAR_ERROR";
export const SET_ERROR = "checkBeforeUpdate/SET_ERROR";
export const INIT_DATA = "checkBeforeUpdate/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "checkBeforeUpdate/SET_DOUBLE_SUBMIT";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const handleCheckService =
  (data, onNext = () => {}, onCancel = () => {}) =>
  (dispatch) => {
    // Cek data di API
    // ketika balikannya true
    // panggil si onNext
    // ketika balikannya false
    // kita panggil handle pop up error global tidak bisa edit / delete
    // bisa panggil onCancel sesuai dengan kebutuhan
  };
