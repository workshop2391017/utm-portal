export const SET_PAGE_PRIVILEGE = "authorization/SET_PAGE_PRIVILEGE";

export const setPagePrivilege = (payload) => ({
  type: SET_PAGE_PRIVILEGE,
  payload,
});
