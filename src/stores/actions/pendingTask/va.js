import {
  serviceChargeListExecute,
  servicePendingtaskDetail,
  servicePendingtaskVaList,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING = "pendingtaskva/SET_LOADING";
export const SET_INITIALDATA = "pendingtaskva/SET_INITIALDATA";
export const SET_ERROR = "pendingtaskva/SET_ERROR";
export const SET_CLEAR_ERROR = "pendingtaskva/SET_CLEAR_ERROR";
export const SET_PT_EXECUTE = "pendingtaskva/SET_PT_EXECUTE";
export const SET_PT_EXECUTE_LOADING = "pendingtaskva/SET_PT_EXECUTE_LOADING";
export const SET_PT_EXECUTE_ERROR = "pendingtaskva/SET_PT_EXECUTE_ERROR";
export const SET_DETAIL = "pendingtaskva/SET_DETAIL";
export const SET_LOADING_EXECUTE = "pendingtaskva/SET_LOADING_EXECUTE";
export const SET_SUCCESS_EXECUTE = "pendingtaskva/SET_SUCCESS_EXECUTE";
export const SET_USER_VA_DATA = "pendingtaskva/SET_USER_VA_DATA";
export const SET_SELECTED_COMPANY = "pendingtaskva/SET_SELECTED_COMPANY";
export const SET_CLEAR_EXECUTE = "pendingtaskva/SET_CLEAR_EXECUTE";
export const APPROVE = 5;
export const REJECT = 6;

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setInitialData = (payload) => ({
  type: SET_INITIALDATA,
  payload,
});

export const setSelectedCompany = (payload) => ({
  type: SET_SELECTED_COMPANY,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setPtExecute = (payload) => ({
  type: SET_PT_EXECUTE,
  payload,
});

export const setClearError = (payload) => ({
  type: SET_CLEAR_ERROR,
  payload,
});

export const setRegisVAData = (payload) => ({
  type: SET_USER_VA_DATA,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setSuccessExecute = (payload) => ({
  type: SET_SUCCESS_EXECUTE,
  payload,
});

export const setClearPtExecute = (payload) => ({
  type: SET_CLEAR_EXECUTE,
});

// const dataDummyGetVA = {
//   status: 200,
//   data: {
//     data: [
//       {
//         reffNo: "954961802992",
//         dateTime: "20 Jul 2022 03:42:24",
//         corporateName: "Corporate Name",
//         ownerName: "Alwan",
//         status: "Menunggu",
//         menu: "REGISTRATION_VA",
//         activityName: "Virtual Account Registration",
//       },
//     ],
//     totalPages: 1,
//     totalElements: 1,
//     numberOfElements: 1,
//     pageNumber: 1,
//   },
// };

// const detailDummyVA = {
//   status: 200,
//   data: {
//     registrationVaTaskDetail: {
//       corporateName: "Corporate Name",
//       address: "Jl.Merdeka No 98",
//       email: "corporate@gmail.com",
//       city: "Yogya",
//       province: "Jawa Timur",
//       zipCode: "65432",
//       businessFields: "Corporate 1",
//       ownerName: "Alwan",
//       npwp: "010141448",
//       position: "Head Of Director",
//       section: "Head Division",
//       phoneNumber: "6273664676776",
//       homePhoneNumber: "62736646767",
//       faxNumber: "62736646723",
//       lengthVa: 7,
//       lengthCustomerId: 7,
//       vaAccountNumber: {
//         accountNumber: "14013000150777",
//         ownerName: "NASABAH GIRO",
//       },
//       vaBillingPendingDto: [
//         {
//           billingCode: "001",
//           billingName: "Pembayaran Listrik",
//           billingType: "BILLING",
//         },
//         {
//           billingCode: "007",
//           billingName: "coa",
//           billingType: "NONBILLING",
//         },
//         {
//           billingCode: "007",
//           billingName: "coa",
//           billingType: "NONBILLING",
//         },
//       ],
//       registrationNo: 1,
//       branchVapendingDto: {
//         name: "JAKARTA PUSAT",
//         phone: "0216336789",
//         fax: "367326",
//         address: "JAKARTA PUSAT, JAKARTA",
//       },
//     },
//   },
// };

// const dummyExecuteVA = {
//   status: 200,
//   data: {
//     statusCode: "00",
//     message: "Success",
//     action: null,
//     transferExecuteResponse: null,
//     executeTaskResponse: null,
//     billpaymentTaskResponse: null,
//     blockCardResponse: null,
//     deleteOrEditWorkflowLimitSchemeResponse: null,
//     purchaseTaskResponse: null,
//   },
// };

export const handlePendingtaskVaList = (dataParams) => async (dispatch) => {
  console.warn(dataParams, "ini list payload");
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await servicePendingtaskVaList(dataParams);
    // const response = dataDummyGetVA;

    if (response.status === 200) {
      dispatch(setInitialData(response?.data));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoading(false));
      dispatch(setInitialData([]));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};

export const handlePandingTaskVaDetail = (dataParams) => async (dispatch) => {
  console.warn(dataParams, "ini detail payload");
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await servicePendingtaskDetail(dataParams);
    // const response = detailDummyVA;

    if (response.status === 200) {
      dispatch(setDetail(response?.data?.registrationVaTaskDetail));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoading(false));
      dispatch(setDetail({}));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};

export const handlePandingTaskVaExecute = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoadingExecute(true));
    const response = await serviceChargeListExecute(dataParams);
    // const response = dummyExecuteVA;

    if (response.status === 200) {
      dispatch(setSuccessExecute(true));
      dispatch(setLoadingExecute(false));

      dispatch(
        setPtExecute({
          action: dataParams?.action,
          isSuccess: true,
          isToReleaser: response.data.isToReleaser,
        })
      );
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoadingExecute(false));
      dispatch(setSuccessExecute(false));
      dispatch(setPtExecute({}));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoadingExecute(false));
  }
};
