import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceGetPendingTaskRegistration,
  serviceGetPendingTaskRegistrationDetail,
  servicePendingTaskSoftTokenBulkExecute,
  servicePendingTaskSoftTokenExecute,
  serviceSoftTokenRegistrationExecute,
  serviePendingTaskSoftToken,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING_SOFTTOKEN = "ptSoftToken/SET_LOADING_PT_SOFTTOKEN";
export const SET_PT_DATA_SOFTTOKEN = "ptSoftToken/SET_PT_DATA_SOFTTOKEN";
export const SET_PT_ERROR_SOFTTOKEN = "ptSoftToken/SET_PT_ERROR_SOFTTOKEN";
export const SET_PT_DETAIL_SOFTTOKEN = "ptSoftToken/SET_PT_DETAIL_SOFTTOKEN";
export const SET_LOADING_EXECUTE = "ptSoftToken/SET_LOADING_EXECUTE";
export const SET_SUCCESS_EXECUTE = "ptSoftToken/SET_SUCCESS_EXECUTE";
export const SET_HANDLE_ERROR = "ptSoftToken/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "ptSoftToken/SET_HANDLE_CLEAR_ERROR";
export const SET_DATA_REFF = "ptSoftToken/SET_DATA_REFF";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING_SOFTTOKEN,
  payload,
});

export const setPendingTaskData = (payload) => ({
  type: SET_PT_DATA_SOFTTOKEN,
  payload,
});

export const setPendingTaskError = (payload) => ({
  type: SET_PT_ERROR_SOFTTOKEN,
  payload,
});

export const setPendingTaskDetail = (payload) => ({
  type: SET_PT_DETAIL_SOFTTOKEN,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setSuccessExecute = (payload) => ({
  type: SET_SUCCESS_EXECUTE,
  payload,
});
export const setBulkReff = (payload) => ({
  type: SET_DATA_REFF,
  payload,
});

export const handleGetPendingTaskSoftToken =
  (dataParams) => async (dispatch) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const response = await serviePendingTaskSoftToken(
        dataParams,
        privilegeCONFIG.PENDING_TASK_CHANGE_SOFT_TOKEN.LIST
      );

      if (response.status === 200) {
        dispatch(setPendingTaskData(response?.data));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoading(false));
        dispatch(setPendingTaskData([]));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setHandleError(e?.message));
      dispatch(setLoading(false));
      dispatch(setPendingTaskData([]));
    }
  };

export const handlePendingTaskBulkExecute =
  (payload) => async (dispatch, getState) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoadingExecute(true));
      const reffNoBulk = getState().pendingTaskSoftToken.dataReffNo;

      const result = reffNoBulk.map((item) => ({
        workFlowMenu: payload?.workFlowMenu,
        referenceNumber: item.referenceNumber.toString(),
      }));

      const finalPayload = {
        approvalList: result,
        action: payload?.action,
        modifiedReason: payload?.modifiedReason,
      };
      const response = await servicePendingTaskSoftTokenBulkExecute(
        finalPayload
      );

      if (response.status === 200) {
        dispatch(setLoadingExecute(false));
        dispatch(
          setSuccessExecute({
            action: payload.action,
            isSuccess: true,
            isToReleaser: response?.data?.isToReleaser,
          })
        );
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoadingExecute(false));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setLoadingExecute(false));
      dispatch(setHandleError(e.message));
    }
  };
export const handlePendingTaskExecute = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoadingExecute(true));
    const response = await servicePendingTaskSoftTokenExecute(
      dataParams,
      privilegeCONFIG.PENDING_TASK_CHANGE_SOFT_TOKEN.LIST
    );

    if (response.status === 200) {
      dispatch(setLoadingExecute(false));
      dispatch(
        setSuccessExecute({
          action: dataParams.action,
          isSuccess: true,
          isToReleaser: response?.data?.isToReleaser,
        })
      );
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoadingExecute(false));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoadingExecute(false));
    dispatch(setHandleError(e.message));
  }
};
