import {
  serviceRequestGetWorkFlowExcute,
  serviceRequestGetWorkFlowDetail,
  serviceRequestGetWorkFlow,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING = "pendingAutoCollection/SET_LOADING";
export const SET_INITIALDATA = "pendingAutoCollection/SET_INITIALDATA";
export const SET_ERROR = "pendingAutoCollection/SET_ERROR";
export const SET_CLEAR_ERROR = "pendingAutoCollection/SET_CLEAR_ERROR";
export const SET_PT_EXECUTE = "pendingAutoCollection/SET_PT_EXECUTE";
export const SET_PT_EXECUTE_LOADING =
  "pendingAutoCollection/SET_PT_EXECUTE_LOADING";
export const SET_PT_EXECUTE_ERROR =
  "pendingAutoCollection/SET_PT_EXECUTE_ERROR";
export const SET_DETAIL = "pendingAutoCollection/SET_DETAIL";
export const SET_LOADING_EXECUTE = "pendingAutoCollection/SET_LOADING_EXECUTE";
export const SET_SUCCESS_EXECUTE = "pendingAutoCollection/SET_SUCCESS_EXECUTE";
export const SET_USER_AUTOCOLLECTION_DATA =
  "pendingAutoCollection/SET_USER_AUTOCOLLECTION_DATA";
export const SET_SELECTED_COMPANY =
  "pendingAutoCollection/SET_SELECTED_COMPANY";
export const SET_CLEAR_EXECUTE = "pendingAutoCollection/SET_CLEAR_EXECUTE";
export const APPROVE = 5;
export const REJECT = 6;

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setInitialData = (payload) => ({
  type: SET_INITIALDATA,
  payload,
});

export const setSelectedCompany = (payload) => ({
  type: SET_SELECTED_COMPANY,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setPtExecute = (payload) => ({
  type: SET_PT_EXECUTE,
  payload,
});

export const setClearError = (payload) => ({
  type: SET_CLEAR_ERROR,
  payload,
});

export const setRegisAutoCollectionData = (payload) => ({
  type: SET_USER_AUTOCOLLECTION_DATA,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setSuccessExecute = (payload) => ({
  type: SET_SUCCESS_EXECUTE,
  payload,
});

export const setClearPtExecute = (payload) => ({
  type: SET_CLEAR_EXECUTE,
});

export const handlePendingtaskAutoCollectionList =
  (dataParams) => async (dispatch) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const response = await serviceRequestGetWorkFlow(dataParams);

      if (response.status === 200) {
        const payload = response.data.workflowApprovalDtoList.map((item) => ({
          ...item,
          additionalData: JSON.parse(item.additionalData),
        }));

        dispatch(
          setInitialData({ ...response.data, workflowApprovalDtoList: payload })
        );
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoading(false));
        dispatch(setInitialData([]));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setError(e?.message));
      dispatch(setLoading(false));
    }
  };

export const handlePandingTaskAutoCollectionDetail =
  (dataParams) => async (dispatch) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const response = await serviceRequestGetWorkFlowDetail(dataParams);

      if (response.status === 200) {
        dispatch(
          setDetail({
            ...response?.data,
            metadata: JSON.parse(response?.data.metadata),
            additionalData: JSON.parse(response?.data.additionalData),
          })
        );
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoading(false));
        dispatch(setDetail({}));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setError(e?.message));
      dispatch(setLoading(false));
    }
  };

export const handlePandingTaskAutoCollectionExecute =
  (dataParams) => async (dispatch) => {
    await dispatch(checkOnline());
    try {
      dispatch(setLoadingExecute(true));
      const response = await serviceRequestGetWorkFlowExcute(dataParams);
      // const response = dummyExecuteVA;

      if (response.status === 200) {
        dispatch(setSuccessExecute(true));
        dispatch(setLoadingExecute(false));

        dispatch(
          setPtExecute({
            action: dataParams?.action,
            isSuccess: true,
            isToReleaser: response.data.isToReleaser,
          })
        );
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoadingExecute(false));
        dispatch(setSuccessExecute(false));
        dispatch(setPtExecute({}));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setError(e?.message));
      dispatch(setLoadingExecute(false));
    }
  };
