import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceSoftTokenRegistrationExecute,
  serviceSoftTokenUbahOtirisasi,
  serviceSoftTokenUbahOtirisasiDetail,
} from "utils/api";
import { handleErrorBE } from "stores/actions/errorGeneral";
import { compare, handleExceptionError } from "utils/helpers";
import { getLabelRegistrasi } from ".";

export const SET_PT_UBAH_OTORISASI = "ptUbahOtorisasi/SET_PT_UBAH_OTORISASI";
export const SET_PT_UO_LOADING = "ptUbahOtorisasi/SET_PT_UO_LOADING";
export const SET_PT_UO_ERROR = "ptUbahOtorisasi/SET_PT_UO_ERROR";
export const SET_PT_UO_DETAIL = "ptUbahOtorisasi/SET_PT_UO_DETAIL";
export const SET_PT_UO_DETAIL_LOADING =
  "ptUbahOtorisasi/SET_PT_UO_DETAIL_LOADING";
export const SET_HANDLE_ERROR = "ptUbahOtorisasi/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "ptUbahOtorisasi/SET_HANDLE_CLEAR_ERROR";
export const SET_DETAIL_DATA = "ptUbahOtorisasi/SET_DETAIL_DATA";
export const SET_LOADING_EXECUTE = "ptUbahOtorisasi/SET_LOADING_EXECUTE";
export const SET_EXECUTE_SUCCESS = "ptUbahOtorisasi/SET_EXECUTE_SUCCESS";
export const SET_CLEAR = "ptUbahOtorisasi/SET_CLEAR";
export const PT_UBAHOTORISASI = "ptUbahOtorisasi/PT_UBAHOTORISASI";
export const SET_REJECTED_LIST_OTORISASI =
  "ptUbahOtorisasi/SET_REJECTED_LIST_OTORISASI";

export const setRejectedList = (payload) => ({
  type: SET_REJECTED_LIST_OTORISASI,
  payload,
});
export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const setExecuteSuccess = (payload) => ({
  type: SET_EXECUTE_SUCCESS,
  payload,
});

export const setDetailData = (payload, key) => ({
  type: SET_DETAIL_DATA,
  payload: { payload, key },
});

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});
export const setLoading = (payload) => ({
  type: SET_PT_UO_LOADING,
  payload,
});

export const setPtUbahOtorisasi = (payload) => ({
  type: SET_PT_UBAH_OTORISASI,
  payload,
});

export const setPtUbahOtorisasiError = (payload) => ({
  type: SET_PT_UO_ERROR,
  payload,
});

export const setPtUbahOtorisasiDetail = (payload) => ({
  type: SET_PT_UO_DETAIL,
  payload,
});

// export const setLoading = (payload) => ({
//   type: SET_PT_UO_DETAIL_LOADING,
//   payload,
// });

export const setClear = () => ({
  type: SET_CLEAR,
});

export const setTemplate = (payload) => ({
  type: PT_UBAHOTORISASI,
  payload,
});

export const handleGetPendingTaskUbahOtorisasi =
  (dataParams) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await serviceSoftTokenUbahOtirisasi(
        dataParams,
        privilegeCONFIG.PENDING_TASK_CHANGE_ATHORIZATION.LIST
      );

      if (response.status === 200) {
        dispatch(setPtUbahOtorisasi(response?.data));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoading(false));
        dispatch(setPtUbahOtorisasi([]));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setHandleError(e?.message));
    }
  };
const mappingValue = (dataForm, el) => dataForm[el] || "-";

const detailFormData = (detail, activeCase) => (dispatch, getState) => {
  const { viewTemplate } = getState().pendingTaskChangeAutorization;

  const template = { ...viewTemplate };

  viewTemplate[activeCase].after.template.forEach((elm, index) => {
    const sectionData = detail?.metadata?.[elm.key] ?? {};
    const suratKuasa = [];
    const registeredAccount = [];

    //  additional account array[]
    if (elm.key === "registeredAccount") {
      registeredAccount.push(
        {
          id: 0,
          value: detail?.accountNumber || "-",
          originalKey: "accountNumber",
          label_en: `Account Number`,
          label_id: `Nomor Rekening`,
          checked: true,
        },
        {
          id: 1,
          value: detail?.accountType || "-",
          originalKey: "accountType",
          label_en: `Account Type`,
          label_id: `Type Rekening`,
          checked: true,
        }
      );
    }

    // others
    const combinedWithLabels = Object.keys(sectionData)
      .map((el, index) => ({
        id: index,
        value: mappingValue(sectionData, el),
        originalKey: el,
        ...getLabelRegistrasi(el, elm.key),
        objKeys: elm.key,
      }))
      .filter((f) => f.label_en)
      .sort(compare)
      .map((m) => ({
        ...m,
        checked: true,
      }));

    if (elm.key === "suratKuasa") {
      if (detail.metadata[elm.key]?.length)
        detail.metadata[elm.key]?.forEach((sk, si) => {
          const obj = { [sk.role]: [] };

          Object.keys(sk).forEach((skKey) => {
            if (["idCardNumber", "name"].includes(skKey)) {
              obj[sk.role].push({
                id: index,
                value: sk[skKey],
                originalKey: skKey,
                role: sk.role,
                ...getLabelRegistrasi(skKey, elm.key),
                checked: true,
              });
            }
          });

          suratKuasa.push(obj);
        });
    }

    template[activeCase].after.template[index] = {
      ...template[activeCase].after.template[index],
      fields: [...combinedWithLabels, ...suratKuasa, ...registeredAccount],
    };
  });

  dispatch(setTemplate(template[activeCase]));
};

export const handleGetPendingTaskUbahOtorisasiDetail =
  (dataParams) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await serviceSoftTokenUbahOtirisasiDetail(dataParams);

      if (response.status === 200) {
        const rejectedList = [];
        if (response.data?.additionalData) {
          const additionalData = response.data?.additionalData ?? {};

          Object.keys(additionalData)
            .filter((f) => additionalData[f])
            .forEach((e) => {
              if (
                [
                  "userProfileMaker",
                  "userProfileReleaser",
                  "userProfileApproval",
                  "userProfileAdminMaker",
                  "userProfileAdminApproval",
                ].includes(e)
              ) {
                Object.keys(additionalData[e]).forEach((key) => {
                  if (additionalData[e][key])
                    rejectedList.push([e, key].join("_"));
                });
              } else {
                Object.keys(additionalData[e]).forEach((key) => {
                  if (additionalData[e][key]) rejectedList.push(key);
                });
              }
            });
        }

        const isAdmin =
          response.data.metadata.corporateProfile.corporateAdmin ===
          "WITH_ADMIN";
        const sampeActive = isAdmin
          ? "multipleWithAdmin"
          : "multipleWithoutAdmin";

        dispatch(setRejectedList(rejectedList));
        dispatch(detailFormData(response?.data, sampeActive));
        dispatch(setPtUbahOtorisasiDetail(response?.data));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoading(false));
        dispatch(setPtUbahOtorisasi([]));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setHandleError(e?.message));
    }
  };

export const handleUbahOtorisasiExecute = (dataParams) => async (dispatch) => {
  try {
    dispatch(setLoadingExecute(true));
    const response = await serviceSoftTokenRegistrationExecute(dataParams);

    if (response.status === 200) {
      dispatch(setLoadingExecute(false));
      dispatch(
        setExecuteSuccess({
          action: dataParams?.status,
          isSuccess: true,
        })
      );
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoadingExecute(false));
      dispatch(handleErrorBE(response));
    } else {
      dispatch(setLoadingExecute(false));
    }
  } catch (e) {
    dispatch(setLoadingExecute(false));
    dispatch(setHandleError(e?.message));
  }
};
