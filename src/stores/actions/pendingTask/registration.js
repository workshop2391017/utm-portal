import { privilegeCONFIG } from "configuration/privilege";
import {
  downloadFileService,
  serviceBranchDetail,
  serviceGetPendingTaskRegistration,
  serviceGetPendingTaskRegistrationDetail,
  serviceRegisLimitPackageDetail,
  serviceRegisMenuPackageDetail,
  serviceSegmentationDetail,
  serviceSoftTokenRegistrationExecute,
} from "utils/api";
import { checkOnline, handleErrorBE } from "stores/actions/errorGeneral";
import {
  compare,
  formatPhoneNumberStripNoSpace,
  handleExceptionError,
} from "utils/helpers";
import { getLabelRegistrasi } from ".";

export const SET_REJECTED_LIST = "pendingTaskRegistration/SET_REJECTED_LIST";
export const SET_LOADING_PT_REGISTRATION =
  "pendingTaskRegistration/SET_LOADING_PT_REGISTRATION";
export const SET_LOADING_PT_REGISTRATION_DOWNLOAD =
  "pendingTaskRegistration/SET_LOADING_PT_REGISTRATION_DOWNLOAD";
export const SET_PT_DATA = "pendingTaskRegistration/SET_PT_DATA";
export const SET_PT_ERROR = "pendingTaskRegistration/SET_PT_ERROR";
export const SET_PT_DETAIL = "pendingTaskRegistration/SET_PT_DETAIL";
export const SET_REF_NUMBER = "pendingTaskRegistration/SET_REF_NUMBER";
export const SET_PT_EXECUTE = "pendingTaskRegistration/SET_PT_EXECUTE";
export const SET_PT_EXECUTE_LOADING =
  "pendingTaskRegistration/SET_PT_EXECUTE_LOADING";
export const SET_PT_EXECUTE_ERROR =
  "pendingTaskRegistration/SET_PT_EXECUTE_ERROR";

export const SET_CLEAR_EXECUTE = "pendingTaskRegistration/SET_CLEAR_EXECUTE";
export const APPROVE = "APPROVE";
export const REJECT = "REJECT";
export const SET_HANDLE_ERROR = "pendingTaskRegistration/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR =
  "pendingTaskRegistration/SET_HANDLE_CLEAR_ERROR";

export const SET_DATA_PROFILE_BIDANG =
  "pendingTaskRegistration/SET_DATA_PROFILE_BIDANG";
export const SET_DATA_PEMILIK_USAHA =
  "pendingTaskRegistration/SET_DATA_PEMILIK_USAHA";
export const SET_IMAGE_CHECKED = "pendingTaskRegistration/SET_IMAGE_CHECKED";
export const SET_OPEN_PREVIEW_IMAGE =
  "pendingTaskRegistration/SET_OPEN_PREVIEW_IMAGE";

export const SET_AFTER_LOGIN_USER_DATA =
  "pendingTaskRegistration/SET_AFTER_LOGIN_USER_DATA";

export const SET_BEFORE_LOGIN_USER_DATA =
  "pendingTaskRegistration/SET_BEFORE_LOGIN_USER_DATA";

export const PENDINGTASK_REGIS_TEMPLATE =
  "pendingTaskRegistration/PENDINGTASK_REGIS_TEMPLATE";
export const SET_PT_BRANCH = "pendingTaskRegistration/SET_PT_BRANCH";
export const SET_PT_SEGMENTATION =
  "pendingTaskRegistration/SET_PT_SEGMENTATION";
export const SET_V_LIMIT_PACAGE = "pendingTaskRegistration/SET_V_LIMIT_PACAGE";
export const SET_V_MENU_PACKAGE = "pendingTaskRegistration/SET_V_MENU_PACKAGE";

// new CR

export const setLimitPackageDetail = (payload) => ({
  type: SET_V_LIMIT_PACAGE,
  payload,
});

export const setMenuPackageDetail = (payload) => ({
  type: SET_V_MENU_PACKAGE,
  payload,
});

export const setBranch = (payload) => ({
  type: SET_PT_BRANCH,
  payload,
});

export const setSegmentation = (payload) => ({
  type: SET_PT_SEGMENTATION,
  payload,
});

export const setTemplate = (payload) => ({
  type: PENDINGTASK_REGIS_TEMPLATE,
  payload,
});
// end new CR

export const setAfterLoginUserData = (payload, key) => ({
  type: SET_AFTER_LOGIN_USER_DATA,
  payload: { payload, key },
});

export const setBeforeLoginData = (payload) => ({
  type: SET_BEFORE_LOGIN_USER_DATA,
  payload,
});

export const setOpenPreviewImage = (payload) => ({
  type: SET_OPEN_PREVIEW_IMAGE,
  payload,
});

export const setImageChecked = (payload) => ({
  type: SET_IMAGE_CHECKED,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});
export const setRegisterIdandFase = (payload) => ({
  type: SET_REF_NUMBER,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING_PT_REGISTRATION,
  payload,
});

export const setPendingTaskData = (payload) => ({
  type: SET_PT_DATA,
  payload,
});

export const setPendingTaskError = (payload) => ({
  type: SET_PT_ERROR,
  payload,
});

export const setPendingTaskDetail = (payload) => ({
  type: SET_PT_DETAIL,
  payload,
});

export const setPtExecute = (payload) => ({
  type: SET_PT_EXECUTE,
  payload,
});

export const setPtExecuteLoading = (payload) => ({
  type: SET_PT_EXECUTE_LOADING,
  payload,
});

export const setPtExecuteError = (payload) => ({
  type: SET_PT_EXECUTE_ERROR,
  payload,
});

export const setClearPtExecute = (payload) => ({
  type: SET_CLEAR_EXECUTE,
});

export const setRejectedList = (payload) => ({
  type: SET_REJECTED_LIST,
  payload,
});

export const setLoadingDownload = (payload) => ({
  type: SET_LOADING_PT_REGISTRATION,
  payload,
});

const getImageName = (name) => {
  switch (name) {
    case "ktp":
      return {
        label: "KTP/KITAS",
        label_en: "KTP/KITAS",
        label_id: "KTP/KITAS",
        value: "ID card.jpg",
      };
    case "tabungan":
      return {
        label: "Business Savings & Current Accounts",
        label_en: "Business Savings & Current Accounts",
        label_id: "Tabungan Bisnis & Giro",
        value: "SavingsBusiness.jpg",
      };

    default:
      return {
        label: "KTP/KITAS",
        label_en: "KTP/KITAS",
        label_id: "KTP/KITAS",
        value: "",
      };
  }
};

const mappingValue = (dataForm, el) => {
  if (el === "accountNumber") return dataForm.accountNumber;
  if (dataForm[el] === "M") return "Man";
  if (dataForm[el] === "F") return "Woman";
  if (el === "province") return dataForm?.provinceName;
  if (el === "district") return dataForm?.districtName;
  if (el === "phoneHome" && !dataForm[el]) return null;
  if (el === "numberOfTokens") return dataForm[el];

  return dataForm[el] || "-";
};

// grouping
const handleGrouping = (payload) => {
  const arr = [];

  payload?.forEach((elm) => {
    const fIdx = arr?.findIndex((e) => e.id === elm.id);

    if (fIdx >= 0) {
      arr[fIdx].child.push(elm);
    } else {
      const assign = { ...elm, child: [] };
      assign.child.push(elm);
      arr.push(assign);
    }
  });

  return arr;
};

const detailFormData = (detail, activeCase) => (dispatch, getState) => {
  const { registrastionCase, viewTemplate } =
    getState().pendingTaskRegistration;

  const template = { ...viewTemplate };

  // hide numberOfTokens on mapping view, issue hide field amount of tokens
  delete detail.metadata.corporateProfile.numberOfTokens;

  Object.keys(viewTemplate[activeCase]).forEach((key) => {
    viewTemplate[activeCase][key].template.forEach((elm, index) => {
      const sectionData = detail?.metadata?.[elm.key] ?? {};

      const additionalAccounts = [];
      const suratKuasa = [];
      const registeredAccount = [];

      //  additional account array[]
      if (elm.key === "registeredAccount") {
        registeredAccount.push(
          {
            id: 0,
            value: detail?.accountNumber || "-",
            originalKey: "accountNumber",
            label_en: `Account Number`,
            label_id: `Nomor Rekening`,
            checked: true,
          },
          {
            id: 1,
            value: detail?.accountType || "-",
            originalKey: "accountType",
            label_en: `Account Type`,
            label_id: `Type Rekening`,
            checked: true,
          }
        );
      }

      //  additional account array[]
      if (elm.key === "additionalAccounts") {
        if (detail.metadata[elm.key]?.length)
          detail.metadata[elm.key]?.forEach((addt, addi) => {
            additionalAccounts.push({
              id: index,
              value: addt,
              originalKey: addi,
              label_en: `Account Number ${addi + 1}`,
              label_id: `Nomor Rekening ${addi + 1}`,
              checked: true,
            });
          });
      }

      // multiple suratkuasa array[{}]
      if (elm.key === "suratKuasa") {
        if (detail.metadata[elm.key]?.length)
          detail.metadata[elm.key]?.forEach((sk, si) => {
            const obj = { [sk.role]: [] };

            Object.keys(sk).forEach((skKey) => {
              if (["idCardNumber", "name"].includes(skKey)) {
                obj[sk.role].push({
                  id: index,
                  value: sk[skKey],
                  originalKey: skKey,
                  role: sk.role,
                  ...getLabelRegistrasi(skKey, elm.key),
                  checked: true,
                });
              }
            });

            suratKuasa.push(obj);
          });
      }
      // others

      const combinedWithLabels = Object.keys(sectionData).map((el, index) => ({
        id: index,
        value: mappingValue(sectionData, el),
        originalKey: el,
        ...getLabelRegistrasi(el, elm.key),
        objKeys: elm.key,
      }));

      if (key === "after") {
        combinedWithLabels.push({
          id: -1,
          value: "",
          originalKey: "emptyKey",
          objKeys: "",
          label_en: "empty field",
          order: 2,
        });
      }

      template[activeCase][key].template[index] = {
        ...template[activeCase][key].template[index],
        fields: [
          ...combinedWithLabels
            .filter((f) => f.label_en)
            .sort(compare)
            .map((m) => ({
              ...m,
              checked: true,
            })),
          ...additionalAccounts,
          ...suratKuasa,
          ...registeredAccount,
        ],
      };
    });
  });

  const documents = detail.metadata.documents;
  const doc = [];

  if (documents) {
    const findNameKeys = Object?.keys(documents)?.filter(
      (elm) => !elm.includes("Image") && ["ktp", "tabungan"].includes(elm)
    );

    // general document
    findNameKeys.forEach((elm) => {
      doc.push({
        [elm]: documents[elm],
        label: getImageName(elm).label,
        label_en: getImageName(elm).label_en,
        label_id: getImageName(elm).label_id,
        value: getImageName(elm).value,
        image:
          elm === "ktp"
            ? documents?.ktpImage
            : elm === "tabungan"
            ? documents?.tabunganImage
            : null,
        key: elm,
        rejectPayload: "profilDokumenRejectedList",
        checked: true,
      });
    });

    // additional documents
    const additionalDocs = Object?.keys(documents)?.filter((elm) =>
      ["additionalSuratKuasa"].includes(elm)
    );
    if (additionalDocs) {
      documents[additionalDocs]?.forEach((elm, i) => {
        doc.push({
          [`Power of Attorney Account ${i + 1}`]: elm,
          label: `Power of Attorney Account ${i + 1}`,
          label_id: `Surat Kuasa ${i + 1}`,
          label_en: `Power of Attorney Account ${i + 1}`,
          value: elm,
          image: documents?.additionalSuratKuasaImage?.[i],
          key: `Power of Attorney Account ${i + 1}`,
          rejectPayload: "profilDokumenRejectedList",
          checked: true,
        });
      });
    }

    template[activeCase] = {
      ...template[activeCase],
      documents: {
        documents: doc,
        signatuerList: detail?.downloadSignatureDocumentUrlList,
      },
    };
  }

  dispatch(setTemplate(template[activeCase]));

  // keys.forEach((key) => {
  // const dataForm = detail?.metadata?.[key.key] ?? {};
  // const replacedKey = Object.keys(dataForm).map((el, index) => ({
  //   id: index,
  //   value: mappingValue(dataForm, el),
  //   originalKey: el,
  //   ...getLabelRegistrasi(el, key.key),
  // }));
  // dispatch(
  //   setBeforeLoginData({
  //     [key.key]: replacedKey
  // .filter((elm) => elm.label_en)
  // .sort(compare)
  // .map((elm) => ({
  //   ...elm,
  //   checked: true,
  //   unchecked: false,
  // })),
  //   })
  // );
  // });
};

export const handleCheckDocument = (name, data) => (dispatch) => {
  const documentUnChecked = [];

  const keyName = name.replace("Image", "");
  const selectedImg = data.find((elm) => elm.label === keyName);
  const withoutSelected = data.filter((elm) => elm.label !== keyName);

  documentUnChecked.push(...withoutSelected, {
    ...selectedImg,
    checked: !selectedImg.checked,
  });

  dispatch(setImageChecked(documentUnChecked));
};

export const handleGetPendingTaskRegistration =
  (dataParams) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await serviceGetPendingTaskRegistration(
        dataParams,
        privilegeCONFIG.PENDING_TASK_REGISTRATION.LIST
      );
      if (response.status === 200) {
        dispatch(setPendingTaskData(response?.data));
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoading(false));
        dispatch(setPendingTaskData(null));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setLoading(false));
      // dispatch(setHandleError(e?.message));
      dispatch(setPendingTaskData(null));
    }
  };

export const handleServiceSegmentationDetail =
  (payload) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await serviceSegmentationDetail(payload);

      if (response.status === 200) {
        dispatch(setLoading(false));
        dispatch(setSegmentation(response.data));
      } else {
        dispatch(setLoading(false));
        dispatch(handleErrorBE(response));
      }
      dispatch(setLoading(false));
    } catch (e) {
      dispatch(setLoading(false));
      dispatch(setHandleError(e?.message));
    }
  };

export const handleServiceBranchDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceBranchDetail(payload);

    if (response.status === 200) {
      dispatch(setLoading(false));
      dispatch(setBranch(response.data));
    } else {
      dispatch(setLoading(false));
      dispatch(handleErrorBE(response));
    }
    dispatch(setLoading(false));
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e?.message));
  }
};

export const handleServiceRegisMenuPackageDetail =
  (payload) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));

    try {
      const res = await serviceRegisMenuPackageDetail(payload);
      if (res.status === 200) {
        dispatch(
          setMenuPackageDetail(
            res.data?.menuPrivilegeDto?.masterMenuHierarchyDtoList?.map(
              (elm) => ({
                name_en: elm?.name_en,
                name: elm?.name_en,
                name_id: elm?.name_id,
                order: elm?.order,
                parent_id: elm?.parent_id,
                id: elm?.id,
                group: elm?.group,
                child: elm?.menuAccessChild,
              })
            )
          )
        );
      } else if (handleExceptionError(res.status)) {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (error) {
      dispatch(setHandleError(error?.message));
      dispatch(setLoading(false));
    }
  };

export const handleGetPendingTaskRegistrationDetail =
  (dataParams, options) => async (dispatch, getState) => {
    const { registrastionCase, viewTemplate } =
      getState().pendingTaskRegistration;

    // template
    //

    try {
      dispatch(setLoading(true));
      const response = await serviceGetPendingTaskRegistrationDetail(
        dataParams,
        privilegeCONFIG.PENDING_TASK_REGISTRATION.LIST
      );

      if (response?.status === 200) {
        // console.log("response -", response.data);
        // const template = [singleOtorisasi,multipleWithAdmin,multipleWithoutAdmin]
        const category = response.data.metadata.corporateProfile.otorisasi;
        const isAdmin =
          response.data.metadata.corporateProfile.corporateAdmin ===
          "WITH_ADMIN";
        // const settingType = response.data.corporateProfile.settingType;
        // const category = response.data.corporateProfile.corporateCategory
        const rejectedList = [];
        if (response.data?.additionalData) {
          const additionalData = response.data?.additionalData ?? {};

          Object.keys(additionalData)
            .filter((f) => additionalData[f])
            .forEach((e) => {
              if (
                [
                  "userProfileMaker",
                  "userProfileReleaser",
                  "userProfileApproval",
                  "userProfileAdminMaker",
                  "userProfileAdminApproval",
                ].includes(e)
              ) {
                Object.keys(additionalData[e]).forEach((key) => {
                  if (additionalData[e][key] !== null)
                    rejectedList.push([e, key].join("_"));
                });
              } else {
                Object.keys(additionalData[e]).forEach((key) => {
                  if (additionalData[e][key] !== null) rejectedList.push(key);
                });
              }
            });
        }

        const sampeActive =
          category === "SINGLE"
            ? "singleOtorisasi"
            : category === "MULTIPLE"
            ? isAdmin
              ? "multipleWithAdmin"
              : "multipleWithoutAdmin"
            : null;

        dispatch(setTemplate(viewTemplate[sampeActive]));
        dispatch(detailFormData(response?.data, sampeActive));
        dispatch(setPendingTaskDetail(response?.data));
        dispatch(setLoading(false));
        dispatch(setRejectedList(rejectedList));

        dispatch(
          setLimitPackageDetail({
            isEdited: false,
            data: handleGrouping(
              response?.data?.metadata?.segmentationLimits?.map((elm) => ({
                ...elm,
                name: elm?.serviceName,
              }))
            ),
          })
        );

        if (response.data?.metadata?.segmentationId)
          dispatch(
            handleServiceSegmentationDetail({
              segmentationId: response.data?.metadata?.segmentationId,
            })
          );

        if (response.data?.metadata?.branch?.kodebranch)
          dispatch(
            handleServiceBranchDetail({
              branchCode: response.data?.metadata?.branch.kodebranch,
            })
          );
      } else {
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setLoading(false));
      dispatch(setHandleError(e?.message));
    }
  };

export const handleRegistrationExecute = (dataParams) => async (dispatch) => {
  try {
    dispatch(setPtExecuteLoading(true));
    const response = await serviceSoftTokenRegistrationExecute(
      dataParams,
      privilegeCONFIG.PENDING_TASK_REGISTRATION.LIST
    );

    if (response.status === 200) {
      dispatch(setPtExecuteLoading(false));
      dispatch(
        setPtExecute({
          action: dataParams?.status,
          isSuccess: true,
          isToReleaser: response.data.approvedStatusSequence,
        })
      );
    } else if (handleExceptionError(response.status)) {
      dispatch(setPtExecuteLoading(false));
      dispatch(handleErrorBE(response));
    } else {
      dispatch(setPtExecuteLoading(false));
    }
  } catch (e) {
    dispatch(setPtExecuteLoading(false));
    dispatch(setHandleError(e?.message));
  }
};

export const handleServiceRegisLimitPackageDetail =
  (payload) => async (dispatch) => {
    dispatch(checkOnline());
    dispatch(setLoading(true));

    try {
      const res = await serviceRegisLimitPackageDetail(payload);
      if (res.status === 200) {
        dispatch(
          setLimitPackageDetail({
            isEdited: false,
            data: handleGrouping(
              res.data?.globalLimit?.map((elm) => ({
                ...elm,
                name: elm?.serviceName,
              }))
            ),
          })
        );
      } else if (handleExceptionError(res.status)) {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoading(false));
    } catch (error) {
      dispatch(setHandleError(error?.message));
      dispatch(setLoading(false));
    }
  };

export const downloadFileSignature =
  (payload, fileName = "document") =>
  async (dispatch) => {
    try {
      const res = await downloadFileService(payload);
      if (res.status === 200) {
        const getFileName = fileName;
        const url = window.URL.createObjectURL(res.data);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", getFileName);
        document.body.appendChild(link);
        link.click();
      } else if (handleExceptionError(res.status)) {
        dispatch(handleErrorBE(res));
      }
      dispatch(setLoadingDownload(false));
    } catch (error) {
      dispatch(setHandleError(error?.message));
      dispatch(setLoadingDownload(false));
    }
  };
