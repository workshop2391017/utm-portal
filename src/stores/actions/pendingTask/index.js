import { compare } from "utils/helpers";
import { handleGetPendingTaskRegistration } from "./registration";
import { handleGetPendingTaskSoftToken } from "./softToken";
import { handleGetPendingTaskUbahOtorisasi } from "./ubahOtorisasi";

export {
  handleGetPendingTaskRegistration,
  handleGetPendingTaskSoftToken,
  handleGetPendingTaskUbahOtorisasi,
};

// afterLogin/UbahOtorisasi
const afterAppliciant = {
  userId: { label_en: "User ID", label_id: "ID User", order: 1 },
  fullName: { label_en: "Full Name", label_id: "Nama Lengkap", order: 3 },
  identityNumber: {
    label_en: "KTP/KITAS Number",
    label_id: "No. KTP/KITAS",
    order: 4,
  },
  email: { label_en: "Email", label_id: "Email", order: 4 },
  phoneNumber: {
    label_en: "Mobile Phone Number",
    label_id: "No. Telepon Rumah (Opsional)",
    order: 5,
  },
};

const afterPowerAttenory = {
  name: { label_en: "Full Name", label_id: "Nama Lengkap", order: 2 },
  idCardNumber: {
    label_en: "KTP/KITAS Number",
    label_id: "No. KTP/KITAS",
    order: 7,
  },
};

export const getLabelRegistrasi = (key, keyObject) => {
  if (keyObject === "corporateProfile") {
    switch (key) {
      case "corporateId":
        return {
          label_en: "Corporate ID",
          label_id: "ID Perusahaan",
          order: 1,
        };

      case "corporateName":
        return { label_en: "Business Name", label_id: "Nama Usaha", order: 2 };
      case "corporateCategoryName":
        return {
          label_en: "Business fields",
          label_id: "Bidang Usaha",
          order: 3,
        };
      case "corporateAddress":
        return { label_en: "Address", label_id: "Alamat Lengkap", order: 4 };

      case "province":
        return {
          label_en: "Province",
          label_id: "Provinsi",
          order: 5,
        };

      case "district":
        return {
          label_en: "City/Regency",
          label_id: "Kabupaten/Kota",
          order: 6,
        };
      case "subDistrict":
        return { label_en: "District", label_id: "Kecamatan", order: 7 };
      case "village":
        return { label_en: "Sub District", label_id: "Desa", order: 8 };
      case "postalCode":
        return { label_en: "Postcode", label_id: "Kode Pos", order: 9 };
      case "corporatePhoneNumber":
        return {
          label_en: "Business Phone",
          label_id: "No. Telepon Usaha",
          order: 10,
        };
      case "numberOfTokens":
        return {
          label_en: "Amount of Token Requirement:",
          label_id: "Jumlah Kebutuhan Hard Token",
          order: 11,
        };
      default:
        return {};
    }
  } else if (keyObject === "userProfile") {
    switch (key) {
      case "userId":
        return { label_en: "User ID", label_id: "Id Pengguna", order: 1 };
      case "fullName":
        return { label_en: "Full name", label_id: "Nama Lengkap", order: 2 };
      case "jobPosition":
        return { label_en: "Position", label_id: "Jabatan", order: 3 };
      case "division":
        return { label_en: "Department", label_id: "Divisi", order: 4 };
      case "gender":
        return { label_en: "Gender", label_id: "Jenis Kelamin", order: 5 };
      case "identityNumber":
        return {
          label_en: "KTP/Kitas Number",
          label_id: "No. KTP/Kitas",
          order: 6,
        };
      case "npwp":
        return { label_en: "NPWP Number", label_id: "No. NPWP", order: 7 };

      case "phoneHome":
        return {
          label_en: "Home Phone Number",
          label_id: "No. Telepon Rumah (opsional)",
          order: 8,
        };
      case "phoneNumber":
        return {
          label_en: "Mobile Phone Number",
          label_id: "No. Hp",
          order: 9,
        };
      case "email":
        return { label_en: "Email", label_id: "Email", order: 12 };

      default:
        return {};
    }
  } else if (
    keyObject === "userProfileAdminMaker" ||
    keyObject === "userProfileAdminApproval" ||
    keyObject === "userProfileMaker" ||
    keyObject === "userProfileApproval" ||
    keyObject === "userProfileReleaser"
  ) {
    return afterAppliciant[key];
  } else if (keyObject === "suratKuasa") {
    return afterPowerAttenory[key];
  }
};

// export const getLabelRegistrasiAfterLogin = (key, keyObject) => {
//   if (
//     keyObject === "userProfileMaker" ||
//     keyObject === "userProfileApproval" ||
//     keyObject === "userProfileReleaser" ||
//     keyObject === "userProfileAdminMaker" ||
//     keyObject === "userProfileAdminApproval"
//   ) {
//     switch (key) {
//       case "email":
//         return;
//       case "fullName":
//         return { label_en: "Full Name", label_id: "Nama Lengkap", order: 2 };
//       case "jobPosition":
//         return { label_en: "Position", label_id: "Jabatan", order: 3 };
//       case "gender":
//         return { label_en: "Gender", label_id: "Jenis Kelamin", order: 4 };
//       case "address":
//         return {
//           label_en: "Complete address",
//           label_id: "Alamat Lengkap",
//           order: 5,
//         };
//       case "citizenship":
//         return {
//           label_en: "Nationality",
//           label_id: "Kewarganeragaan",
//           order: 6,
//         };
//       case "identityNumber":
//         return {
//           label_en: "KTP/KITAS Number",
//           label_id: "No. KTP/KITAS",
//           order: 7,
//         };
//       case "birthPlace":
//         return {
//           label_en: "Place of birth",
//           label_id: "Tempat Lahir",
//           order: 8,
//         };
//       case "birthdate":
//         return {
//           label_en: "Date of birth",
//           label_id: "Tanggal Lahir",
//           order: 9,
//         };
//       case "phoneHome":
//         return;
//       case "phoneNumber":
//         return { label_en: "No. Mobile Phone", label_id: "No. HP", order: 11 };
//       default:
//         return {};
//     }
//   } else if (keyObject === "userProfile") {
//     switch (key) {
//       case "fullName":
//         return { label_en: "Full Name", label_id: "Nama Lengkap", order: 1 };
//       case "jobPosition":
//         return { label_en: "Position", label_id: "Jabatan", order: 2 };
//       case "gender":
//         return { label_en: "Gender", label_id: "Jenis Kelamin", order: 3 };
//       case "identityNumber":
//         return { label_en: "No KTP/Kitas", label_id: "No KTP/Kitas", order: 4 };
//       case "citizenship":
//         return {
//           label_en: "Nationality",
//           label_id: "Kewarganegaraan",
//           order: 5,
//         };
//       case "birthPlace":
//         return {
//           label_en: "Place of birth",
//           label_id: "Tempat Lahir",
//           order: 6,
//         };
//       case "birthdate":
//         return {
//           label_en: "Date of birth",
//           label_id: "Tanggal Lahir",
//           order: 7,
//         };
//       case "phoneHome":
//         return {
//           label_en: "Mobile Phone Number",
//           label_id: "No Telepon Rumah",
//           order: 8,
//         };
//       case "phoneNumber":
//         return {
//           label_en: "No. Mobile Phone",
//           label_id: "No Telepon Genggam",
//           order: 9,
//         };
//       case "email":
//         return { label_en: "Email", label_id: "Email", order: 10 };
//       case "address":
//         return {
//           label_en: "Business Address",
//           label_id: "Alamat Usaha",
//           order: 11,
//         };
//       default:
//         return {};
//     }
//   } else if (keyObject === "corporateProfile") {
//     switch (key) {
//       case "corporateName":
//         return { label_en: "Business Name", label_id: "Nama Usaha", order: 1 };
//       case "corporateAddress":
//         return { label_en: "Address", label_id: "Alamat Usaha", order: 2 };
//       case "corporateCategoryName":
//         return {
//           label_en: "Business fields",
//           label_id: "Bidang Usaha",
//           order: 3,
//         };
//       case "corporatePhoneNumber":
//         return {
//           label_en: "Business Phone",
//           label_id: "No Telepon Usaha",
//           order: 4,
//         };
//       case "province":
//         return {
//           label_en: "Province",
//           label_id: "Provinsi",
//           order: 5,
//         };
//       case "numberOfTokens":
//         return {
//           label_en: "Amount of Token Requirement",
//           label_id: "Jumlah Kebutuhan Token",
//           order: 6,
//         };
//       case "district":
//         return {
//           label_en: "District",
//           label_id: "Kabupaten",
//           order: 7,
//         };

//       default:
//         return {};
//     }
//   }
// };

export const filterExecuteUnchacked = (data, type, detail) => {
  const uncheckedData = data.filter((elm) => !elm.checked);
  const objectData = {};

  if (type === "corporateProfile" && detail) {
    const forcorporateProfile = detail?.metadata.corporateProfile;

    if (
      uncheckedData.find(
        ({ originalKey }) => originalKey === "corporateCategoryName"
      )
    ) {
      objectData.corporateCategory = forcorporateProfile.corporateCategory;
    }
  }

  uncheckedData
    .filter((elm) => !elm.checked)
    .forEach((elm) => {
      objectData[type === "doc" ? elm.key : elm.originalKey] =
        type === "doc" ? elm[Object.keys(elm)[0]] : elm.value;
    });

  if (type === "additionalAccounts") {
    const objAddAcc = [];

    uncheckedData
      .filter((elm) => !elm.checked)
      .forEach((elm) => {
        objAddAcc.push(elm.value);
      });

    return objAddAcc;
  }
  return objectData;
};

export const handleChecklist =
  (i, data, setData, key, corporateAdmin) => (dispatch, getState) => {
    const arr = [...data];
    arr[i] = {
      ...arr[i],
      checked: !arr[i].checked,
    };

    if (corporateAdmin) {
      dispatch(setData({ [key]: arr }, corporateAdmin));
    } else dispatch(setData({ [key]: arr }));
  };

const mappingValueUbahOtorisasi = (dataForm, el, data) => {
  if (el === "accountNumber") return data.accountNumber;
  if (dataForm[el] === "M") return "Man";
  if (dataForm[el] === "F" || dataForm[el] === "P") return "Woman";
  if (el === "province") return dataForm?.provinceName;
  if (el === "district") return dataForm?.districtName;

  return dataForm[el] || "-";
};

export const detailFormData =
  (keys, setData, data, withAdminOrNot) => (dispatch) => {
    keys.forEach((key) => {
      const dataForm = data[key.key];

      if (dataForm) {
        const replacedKey = Object.keys(dataForm)
          .map((el, index) => ({
            id: index,
            value: mappingValueUbahOtorisasi(dataForm, el, data),
            originalKey: el,
            ...getLabelRegistrasi(el, key.key),
          }))
          .filter((f) => f.label_id);

        dispatch(
          setData(
            {
              [key.key]: replacedKey
                .filter((elm) => elm.label_id)
                .sort(compare)
                .map((elm) => ({
                  ...elm,
                  label: elm.label_en,
                  checked: true,
                  unchecked: false, // sepertinya ini udah tidak dipake
                })),
            },
            withAdminOrNot
          )
        );
      }
    });
  };
