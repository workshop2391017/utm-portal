import {
  serviceAutoCollectionReport,
  serviceAutoCollectionReportDetail,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING = "autoCollectionReport/SET_LOADING";
export const SET_INITIALDATA = "autoCollectionReport/SET_INITIALDATA";
export const SET_ERROR = "autoCollectionReport/SET_ERROR";
export const SET_SELECTED_COMPANY = "autoCollectionReport/SET_SELECTED_COMPANY";
export const SET_DETAIL = "autoCollectionReport/SET_DETAIL";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setInitialData = (payload) => ({
  type: SET_INITIALDATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setSelectedCompany = (payload) => ({
  type: SET_SELECTED_COMPANY,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const handleAutoCollectionReportData =
  (dataParams) => async (dispatch) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const response = await serviceAutoCollectionReport(dataParams);
      // const response = dataDummyGetVA;

      if (response.status === 200) {
        dispatch(setInitialData(response?.data));
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoading(false));
        dispatch(setInitialData([]));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setError(e?.message));
      dispatch(setLoading(false));
    }
  };

export const handleAutoCollectionReportDetail =
  (dataParams) => async (dispatch) => {
    dispatch(checkOnline());
    try {
      dispatch(setLoading(true));
      const response = await serviceAutoCollectionReportDetail(dataParams);
      // const response = detailDummyVA;
      if (response.status === 200) {
        dispatch(setDetail(response?.data));
        dispatch(setLoading(false));
      } else if (handleExceptionError(response.status)) {
        dispatch(setLoading(false));
        dispatch(setDetail({}));
        dispatch(handleErrorBE(response));
      }
    } catch (e) {
      dispatch(setError(e?.message));
      dispatch(setLoading(false));
    }
  };
