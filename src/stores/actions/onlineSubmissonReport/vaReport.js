import { serviceVAReportDetail, serviceVirtualAccountReport } from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING = "vaReport/SET_LOADING";
export const SET_INITIALDATA = "vaReport/SET_INITIALDATA";
export const SET_ERROR = "vaReport/SET_ERROR";
export const SET_SELECTED_COMPANY = "vaReport/SET_SELECTED_COMPANY";
export const SET_DETAIL = "vaReport/SET_DETAIL";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setInitialData = (payload) => ({
  type: SET_INITIALDATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setSelectedCompany = (payload) => ({
  type: SET_SELECTED_COMPANY,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const handleVAReportList = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await serviceVirtualAccountReport(dataParams);
    // const response = dataDummyGetVA;

    if (response.status === 200) {
      dispatch(setInitialData(response?.data));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoading(false));
      dispatch(setInitialData([]));
      // dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};

export const handleVAReportDetail = (dataParams) => async (dispatch) => {
  dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const response = await serviceVAReportDetail(dataParams);
    // const response = detailDummyVA;
    if (response.status === 200) {
      dispatch(setDetail(response?.data?.vaInstitutionDtoDetailList[0]));
      dispatch(setLoading(false));
    } else if (handleExceptionError(response.status)) {
      dispatch(setLoading(false));
      dispatch(setDetail({}));
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setError(e?.message));
    dispatch(setLoading(false));
  }
};
