// login Actions
// --------------------------------------------------------
import { privilegeCONFIG } from "configuration/privilege";
import {
  getAllWorkFlowRole,
  serviceDeleteUserAdmin,
  serviceGetAllBranch,
  serviceGetGroupByBranchId,
  serviceManagamentUser,
  serviceManagementRole,
  serviceSaveUserAdmin,
  servicesGetLdapUser,
} from "utils/api";
import { getLocalStorage, handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";
import { validateTask } from "../validateTaskPortal";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "managementuser/SET_LOADING";
export const SET_ALL_FORM_MANAGEMENTUSER =
  "managementuser/SET_ALL_FORM_MANAGEMENTUSER";
export const SET_USER_DATA = "managementuser/SET_USER_DATA";
export const SET_LOADING_SUBMIT = "managementuser/SET_LOADING_SUBMIT";
export const SET_CLEAR_USERDATA = "managementuser/SET_CLEAR_USERDATA";
export const SET_LOADING_USER_LDAP = "managementuser/SET_LOADING_USER_LDAP";
export const SET_USER_LDAP = "managementuser/SET_USER_LDAP";
export const SET_ERROR_LDAP = "managementuser/SET_ERROR_LDAP";
export const SET_CLEAR_ERROR_LDAP = "managementuser/SET_CLEAR_ERROR_LDAP";
export const SET_HANDLE_ERROR = "managementuser/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "managementuser/SET_HANDLE_CLEAR_ERROR";
export const SET_LOADING_ALLBRANCH = "managementuser/SET_LOADING_ALLBRANCH";
export const SET_LOADING_ALLROLE = "managementuser/SET_LOADING_ALLROLE";
export const SET_BRANCH_DATA = "managementuser/SET_BRANCH_DATA";
export const SET_ALLROLE = "managementuser/SET_ALLROLE";
export const SET_LOADING_GROUP_BRANCHID =
  "managementuser/SET_LOADING_GROUP_BRANCHID";
export const SET_DATA_GROUP_BRANCH_ID =
  "managementuser/SET_DATA_GROUP_BRANCH_ID";
export const SET_ERROR_USER_ALREADY_EXIST =
  "managementuser/SET_ERROR_USER_ALREADY_EXIST";

export const setLoadingBranch = (payload) => ({
  type: SET_LOADING_ALLBRANCH,
  payload,
});

export const setLoadingAllRole = (payload) => ({
  type: SET_LOADING_ALLROLE,
  payload,
});

export const setAllRole = (payload) => ({
  type: SET_ALLROLE,
  payload,
});

export const setAllBranch = (payload) => ({
  type: SET_BRANCH_DATA,
  payload,
});

export const setErrorLdapClear = () => ({
  type: SET_CLEAR_ERROR_LDAP,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDataManagementUser = (payload) => ({
  type: SET_ALL_FORM_MANAGEMENTUSER,
  payload,
});

export const setUserData = (payload) => ({
  type: SET_USER_DATA,
  payload,
});

export const setLoadingSubmit = (payload) => ({
  type: SET_LOADING_SUBMIT,
  payload,
});

export const setClearUserData = () => ({
  type: SET_CLEAR_USERDATA,
});

export const setLoadingUserLdap = (payload) => ({
  type: SET_LOADING_USER_LDAP,
  payload,
});

export const setUserLdap = (payload) => ({
  type: SET_USER_LDAP,
  payload,
});

export const setErrorLdap = (payload) => ({
  type: SET_ERROR_LDAP,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoadingGroupBranchId = (payload) => ({
  type: SET_LOADING_GROUP_BRANCHID,
  payload,
});

export const setDataGroupBranchId = (payload) => ({
  type: SET_DATA_GROUP_BRANCH_ID,
  payload,
});

export const setErrorUserAlreadyExist = (payload) => ({
  type: SET_ERROR_USER_ALREADY_EXIST,
  payload,
});

export const handleManagementUser = (payloadRequest) => async (dispatch) => {
  await dispatch(checkOnline(dispatch));
  try {
    dispatch(setLoading(true));
    const response = await serviceManagamentUser(payloadRequest);
    dispatch(setLoading(false));

    if (response.status === 200) {
      dispatch(setDataManagementUser(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};

export const submitSettingUser =
  (
    { roleId, ...rest },
    payloadValidate,
    handleNext,
    handleSuccess,
    handleCloseOnError
  ) =>
  async (dispatch) => {
    const user = JSON.parse(getLocalStorage("portalUserLogin"));

    const requestPayload = {
      ...rest,
      userMaker: user.username,
    };

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PORTAL_USER,
          code: null,
          ...payloadValidate,
          validate: true,
          loader: setLoadingSubmit,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoadingSubmit(true));
              const response = await serviceSaveUserAdmin(requestPayload);

              dispatch(setLoadingSubmit(false));
              if (response.status === 200) {
                dispatch(setClearUserData());
                handleNext();
                handleSuccess();
              } else if (response?.data?.errorCode === "0019") {
                dispatch(setErrorUserAlreadyExist(true));
              } else {
                handleCloseOnError();
                dispatch(handleErrorBE(response, false, "ADD_OR_EDIT"));
              }

              return false;
            } catch (e) {
              dispatch(setLoadingSubmit(false));
              dispatch(setHandleError(e.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
            dispatch(setLoadingSubmit(false));
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const deleteUserAdmin =
  (payload, payloadValidate, { handleNext, handleSuccess }) =>
  async (dispatch) => {
    const user = JSON.parse(getLocalStorage("portalUserLogin"));

    const requestPayload = {
      ...payload,
      userMaker: user.username,
      privilege: user?.privilegeList?.[0]?.privilegeId,
      ipAddress: user?.ip,
    };

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PORTAL_USER,
          ...payloadValidate,
          validate: true,
          loader: setLoadingSubmit,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoadingSubmit(true));
              const response = await serviceDeleteUserAdmin(
                requestPayload,
                privilegeCONFIG.MANAGEMENT_USER.LIST
              );
              dispatch(setLoadingSubmit(false));
              if (response.status === 200) {
                handleSuccess();
                handleNext();
                return;
              }
              if (handleExceptionError(response.status)) {
                dispatch(handleErrorBE(response));
              }
              return false;
            } catch (e) {
              dispatch(setLoadingSubmit(false));
              dispatch(setHandleError(e.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
            dispatch(setLoadingSubmit(false));
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const handleLdapUser = (payload) => async (dispatch) => {
  try {
    dispatch(setLoadingUserLdap(true));
    const response = await servicesGetLdapUser(
      { username: payload },
      privilegeCONFIG.MANAGEMENT_USER.LIST
    );
    dispatch(setLoadingUserLdap(false));
    if (response.status === 200) {
      dispatch(setUserLdap(response.data));
      dispatch(setErrorLdapClear());
    } else if (handleExceptionError(response.status)) {
      dispatch(setErrorLdap(true));
      dispatch(setUserLdap({}));
    }
  } catch (e) {
    dispatch(setLoadingUserLdap(false));
    dispatch(setHandleError(e.message));
    dispatch(setUserLdap({}));
  }
};

export const handleGetAllBranch = (payload) => async (dispatch) => {
  try {
    dispatch(setLoadingBranch(true));
    const response = await serviceGetAllBranch(
      privilegeCONFIG.MANAGEMENT_USER.LIST
    );
    dispatch(setLoadingBranch(false));
    if (response.status === 200) {
      dispatch(setAllBranch(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setLoadingBranch(false));
    dispatch(setHandleError(e.message));
  }
};

export const handlegetAllRole = (payload) => async (dispatch) => {
  try {
    dispatch(setAllRole(true));
    const response = await getAllWorkFlowRole(payload);
    if (response.status === 200) {
      dispatch(setAllRole(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setAllRole(false));
    dispatch(setHandleError(e.message));
  }
};

export const getGroupByBranchId = (payload) => async (dispatch) => {
  dispatch(setLoadingGroupBranchId(true));
  try {
    const response = await serviceGetGroupByBranchId(payload);

    if (response.status === 200) {
      dispatch(setDataGroupBranchId(response.data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
    dispatch(setLoadingGroupBranchId(false));
  } catch (err) {
    dispatch(setLoadingGroupBranchId(false));
    dispatch(setHandleError(err.message));
  }
};
