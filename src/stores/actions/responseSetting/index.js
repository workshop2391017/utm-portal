// responseSetting Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "responseSetting/SET_LOADING";
export const CLEAR_ERROR = "responseSetting/CLEAR_ERROR";
export const SET_ERROR = "responseSetting/SET_ERROR";
export const INIT_DATA = "responseSetting/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "responseSetting/SET_DOUBLE_SUBMIT";
export const ADD_ONE_DATA = "responseSetting/ADD_ONE_DATA";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setAddOneData = (payload) => ({
  type: ADD_ONE_DATA,
  payload,
});
