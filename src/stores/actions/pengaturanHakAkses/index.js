// pengaturanhakakses Actions
// --------------------------------------------------------

import {
  serviceDeletePrivilegeHakaksesGroup,
  serviceGetPrivilegeHakaksesGroup,
  serviceInsertPrivilegeHakaksesGroup,
  serviceRequestGetMenuGlobal,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";
import { validateTask } from "../validateTaskPortal";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "pengaturanhakakses/SET_LOADING";
export const CLEAR_ERROR = "pengaturanhakakses/CLEAR_ERROR";
export const SET_ERROR = "pengaturanhakakses/SET_ERROR";
export const INIT_DATA = "pengaturanhakakses/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "pengaturanhakakses/SET_DOUBLE_SUBMIT";
export const SET_LOADING_INSERT = "pengaturanhakakses/SET_LOADING_INSERT";
export const SET_SUCCESS_INSERT = "pengaturanhakakses/SET_SUCCESS_INSERT";
export const SET_CLEAR_DATA = "pengaturanhakakses/SET_CLEAR_DATA";
export const SET_MENU_GLOBAL = "pengaturanhakakses/SET_MENU_GLOBAL";
export const SET_LOADING_MENU_GLOBAL =
  "pengaturanhakakses/SET_LOADING_MENU_GLOBAL";
export const SET_SELECT_GROUP = "pengaturanhakakses/SET_SELECT_GROUP";
export const SET_LOADING_DELETE = "pengaturanhakakses/SET_LOADING_DELETE";
export const SET_DELETE_SUCCESS = "pengaturanhakakses/SET_DELETE_SUCCESS";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setClearError = () => ({
  type: CLEAR_ERROR,
});

export const setInitData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setLoadingInsert = (payload) => ({
  type: SET_LOADING_INSERT,
  payload,
});

export const setSuccessInsert = (payload) => ({
  type: SET_SUCCESS_INSERT,
  payload,
});

export const setClear = () => ({
  type: SET_CLEAR_DATA,
});

export const setLoadingMenuGlobal = (payload) => ({
  type: SET_LOADING_MENU_GLOBAL,
  payload,
});

export const setMenuGlobal = (payload) => ({
  type: SET_MENU_GLOBAL,
  payload,
});

export const setSelectedMenu = (payload) => ({
  type: SET_SELECT_GROUP,
  payload,
});

export const setLoadingDelete = (payload) => ({
  type: SET_LOADING_DELETE,
  payload,
});

export const setDeleteSuccess = (payload) => ({
  type: SET_DELETE_SUCCESS,
  payload,
});

export const hanleGetPrivilegeHakaksesGroup = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoading(true));
  try {
    const response = await serviceGetPrivilegeHakaksesGroup(payload);
    if (response.status === 200) {
      const data = { portalGroupDetailDto: [] };
      response?.data?.portalGroupDetailDto.forEach((el) => {
        data.portalGroupDetailDto.push({
          ...el,
          masterMenu: el.masterMenu?.map((em) =>
            em.menuAccessChild.length > 0
              ? {
                  ...em,
                  menuAccessChild: em.menuAccessChild
                    .sort((a, b) => (a.order > b.order ? 1 : -1))
                    .filter((em) => !em.name_en.includes("All")),
                }
              : em
          ),
        });
      });
      dispatch(setInitData(data));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(setError(err?.message));
  }
};

export const hanleInsertPrivilegeHakaksesGroup =
  (payload, payloadValidate, handleNext) => async (dispatch) => {
    dispatch(checkOnline());

    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.PORTAL_GROUP,
          ...payloadValidate,
          id: null,
          validate: true,
          loader: setLoadingInsert,
        },
        {
          async onContinue() {
            try {
              const response = await serviceInsertPrivilegeHakaksesGroup(
                payload
              );
              handleNext();
              if (response.status === 200) {
                dispatch(setSuccessInsert(true));
              } else if (handleExceptionError(response.status)) {
                dispatch(handleErrorBE(response));
              }
              dispatch(setLoadingInsert(false));
            } catch (err) {
              dispatch(setLoadingInsert(false));
              dispatch(setError(err?.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
            dispatch(setLoadingInsert(false));
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const handleGetAllMenuGlobal = (payload) => async (dispatch) => {
  dispatch(checkOnline());
  dispatch(setLoadingMenuGlobal(true));
  try {
    const response = await serviceRequestGetMenuGlobal(payload);

    if (response.status === 200) {
      const data = [];
      response?.data?.menuAccessList.forEach((e) => {
        data.push({
          ...e,
          menuAccessChild: e.menuAccessChild.sort((a, b) =>
            a.order > b.order ? 1 : -1
          ),
        });
      });
      dispatch(setMenuGlobal({ menuAccessList: data }));
    } else if (handleExceptionError(response.status)) {
      dispatch(handleErrorBE(response));
    }
    dispatch(setLoadingMenuGlobal(false));
  } catch (err) {
    dispatch(setLoadingMenuGlobal(false));
    dispatch(setError(err?.message));
  }
};

export const handleDeleteMenuGlobal =
  (payload, payloadValidate, handleNext) => async (dispatch) => {
    dispatch(checkOnline());

    try {
      const response = await serviceDeletePrivilegeHakaksesGroup(payload);

      if (response.status === 200) {
        dispatch(setDeleteSuccess(true));
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response, false));
      }
      dispatch(setLoadingDelete(false));
    } catch (err) {
      dispatch(setLoadingDelete(false));
      dispatch(setError(err?.message));
    }
  };
