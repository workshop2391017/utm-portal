// progressBarAction Actions
// --------------------------------------------------------

import {
  serviceContentManagementGet,
  serviceContentManagementSave,
} from "utils/api";
import { handleErrorBE } from "../errorGeneral";
import { setLoadingGlobal } from "../pageContainer";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "contentManagement/SET_LOADING";
export const CLEAR_ERROR = "contentManagement/CLEAR_ERROR";
export const SET_ERROR = "contentManagement/SET_ERROR";
export const INIT_DATA = "contentManagement/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "contentManagement/SET_DOUBLE_SUBMIT";
export const SET_SUCCESS_SAVE = "contentManagement/SET_SUCCESS_SAVE";
export const SET_CONTENT = "contentManagement/SET_CONTENT";

export const setContent = (payload) => ({
  type: SET_CONTENT,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setInitialData = (payload) => ({
  type: INIT_DATA,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setSuccessSave = (payload) => ({
  type: SET_SUCCESS_SAVE,
  payload,
});

export const handleManagementContentGet =
  (payload) => async (dispatch, getState) => {
    dispatch(setLoading(true));
    try {
      const response = await serviceContentManagementGet(payload);

      if (response.status === 200) {
        dispatch(setInitialData(response.data));
        dispatch(setLoading(false));
      } else {
        dispatch(handleErrorBE(response));
        dispatch(setLoading(false));
      }
    } catch (e) {
      dispatch(setError(e.message));
      dispatch(setLoading(false));
    }
  };

export const handleManagementContentSave =
  (payload, cb) => async (dispatch, getState) => {
    dispatch(setLoadingGlobal(true));
    try {
      const response = await serviceContentManagementSave(payload);
      if (response.status === 200) {
        cb();
      } else {
        dispatch(handleErrorBE(response));
        dispatch(setLoadingGlobal(false));
      }
    } catch (e) {
      dispatch(setError(e.message));
      dispatch(setLoadingGlobal(false));
    } finally {
      dispatch(setLoadingGlobal(false));
    }
  };
