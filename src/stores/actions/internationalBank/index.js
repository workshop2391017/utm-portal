// internationalBank Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceRequestInternationalBank,
  serviceRequestAllCurrencyInternationalBank,
  serviceRequestSaveInternationalBank,
  serviceRequestDeleteInternationalBank,
  serviceRequestActiveInternationalBank,
  serviceRequestBankDomesticCreateValidation,
} from "utils/api";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "internationalBank/SET_LOADING";
export const CLEAR_ERROR = "internationalBank/CLEAR_ERROR";
export const SET_ERROR = "internationalBank/SET_ERROR";
export const INIT_DATA = "internationalBank/INIT_DATA";
export const DETAIL_DATA = "internationalBank/DETAIL_DATA";
export const CURRENCY_LIST_DATA = "internationalBank/CURRENCY_LIST_DATA";
export const SET_DOUBLE_SUBMIT = "internationalBank/SET_DOUBLE_SUBMIT";
export const SET_IS_OPEN = "internationalBank/SET_IS_OPEN";
export const SET_VALIDATE_CREATE = "internationalBank/SET_VALIDATE_CREATE";

export const setTypeIsOpen = (payload) => ({
  type: SET_IS_OPEN,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

const setTypeInternationalBank = (payload) => ({
  type: INIT_DATA,
  payload,
});
const setTypeErorHandler = (payload) => ({
  type: SET_ERROR,
  payload,
});

const setTypeCurrencyList = (payload) => ({
  type: CURRENCY_LIST_DATA,
  payload,
});

export const setTypeDetailEdit = (payload) => ({
  type: DETAIL_DATA,
  payload,
});

export const setTypeClearError = () => ({
  type: CLEAR_ERROR,
});
export const setValidate = (payload) => ({
  type: SET_VALIDATE_CREATE,
  payload,
});

export const getValidateCreate = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestBankDomesticCreateValidation(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setValidate([]));
    } else {
      dispatch(setValidate(res.data?.message.split(" ")));
    }
  } catch (err) {
    dispatch(setTypeErorHandler(err.message));
  }
};

export const getAllDataInternationalBank = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const res = await serviceRequestInternationalBank(payload);

    dispatch(setLoading(false));

    console.warn("internationalBank:", res);

    if (res.status === 200) {
      dispatch(setTypeInternationalBank(res?.data?.bankListPage));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErorHandler(error?.message));
  }
};

export const getAllDataCurrencyInternationalBank =
  (payload) => async (dispatch) => {
    try {
      const res = await serviceRequestAllCurrencyInternationalBank(payload);

      // setTypeBankDomestic(res.bankListPage);

      if (res.status === 200) {
        dispatch(setTypeCurrencyList(res?.data?.currencyList));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setTypeErorHandler(error?.message));
    }
  };

export const AddSaveInternationalBank =
  (payload, codeValidateTask, handleNext) => async (dispatch, getState) => {
    const { dataDetail } = getState().internationalBank;
    if (dataDetail !== null) {
      try {
        const res = await serviceRequestSaveInternationalBank(payload);

        if (res.status === 200) {
          // dispatch(setTypeCurrencyList(res?.data?.currencyList));

          dispatch(setTypeIsOpen(true));
        } else {
          dispatch(handleErrorBE(res));
        }
      } catch (error) {
        dispatch(setTypeErorHandler(error?.message));
      }
    } else {
      dispatch(
        validateTask(
          {
            menuName: validateTaskConstant.INTERNATIONAL_BANK,
            code: codeValidateTask,
            id: null,
            name: payload?.bankAlias,
            validate: payload?.id === null || payload?.id === undefined, // karena udah divalidasi pas klik button edit
            loader: setLoading,
          },
          {
            async onContinue() {
              try {
                const res = await serviceRequestSaveInternationalBank(payload);

                if (res.status === 200) {
                  // dispatch(setTypeCurrencyList(res?.data?.currencyList));

                  dispatch(setTypeIsOpen(true));
                } else {
                  dispatch(handleErrorBE(res));
                }
              } catch (error) {
                dispatch(setTypeErorHandler(error?.message));
              }
            },
            onError() {
              // handle callback error
              // ! popups callback error sudah dihandle !
              // ex. clear form, redirect, etc
              handleNext();
            },
          }
        )
      );
    }
  };

export const ReloadActiveInternationalBank = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestActiveInternationalBank(payload);

    // setTypeBankDomestic(res.bankListPage);

    if (res.status === 200) {
      // dispatch(setTypeCurrencyList(res?.data?.currencyList));

      dispatch(setTypeIsOpen(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErorHandler(error?.message));
  }
};

export const DeleteBankInternationalBank = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestDeleteInternationalBank(payload);

    // setTypeBankDomestic(res.bankListPage);

    if (res.status === 200) {
      // dispatch(setTypeCurrencyList(res?.data?.currencyList));
      dispatch(setTypeIsOpen(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setTypeErorHandler(error?.message));
  }
};
