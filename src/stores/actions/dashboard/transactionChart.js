import { privilegeCONFIG } from "configuration/privilege";
import {
  serviceGetTransactionHistoryPage,
  serviceTransactionChart,
} from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING_CHART = "dashboard/SET_LOADING_CHART";
export const CLEAR_ERROR = "dashboard/CLEAR_ERROR";
export const SET_ERROR = "dashboard/SET_ERROR";
export const SET_TRANSACTION_CHART = "dashboard/SET_TRANSACTION_CHART";
export const SET_HANDLE_ERROR = "dashboard/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "dashboard/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING_CHART,
  payload,
});

export const setTransactionChart = (payload) => ({
  type: SET_TRANSACTION_CHART,
  payload,
});

export const setErrorChart = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const handleGetTransactionChart = (dataParams) => async (dispatch) => {
  try {
    const respData = {
      status: 200,
      data: {
        transactionSuccessBefore: [
          {
            monthTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-19, 2024-04-25",
                transactionMonth: 4,
                transactionYear: 2024,
              },
            ],
            dayTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-19",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-20",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-21",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-22",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-23",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-24",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-25",
                transactionMonth: 4,
                transactionYear: 2024,
              },
            ],
            weekTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-19, 2024-04-25",
                transactionWeek: 1,
                transactionMonth: null,
                transactionYear: null,
              },
            ],
          },
        ],
        transactionSuccessNow: [
          {
            monthTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-26, 2024-04-30",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-05-01, 2024-05-02",
                transactionMonth: 5,
                transactionYear: 2024,
              },
            ],
            dayTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-26",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-27",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-28",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-29",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-30",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-05-01",
                transactionMonth: 5,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-05-02",
                transactionMonth: 5,
                transactionYear: 2024,
              },
            ],
            weekTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-26, 2024-05-02",
                transactionWeek: 1,
                transactionMonth: null,
                transactionYear: null,
              },
            ],
          },
        ],
        transactionErrorBefore: [
          {
            monthTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-19, 2024-04-25",
                transactionMonth: 4,
                transactionYear: 2024,
              },
            ],
            dayTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-19",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-20",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-21",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-22",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-23",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-24",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-25",
                transactionMonth: 4,
                transactionYear: 2024,
              },
            ],
            weekTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-19, 2024-04-25",
                transactionWeek: 1,
                transactionMonth: null,
                transactionYear: null,
              },
            ],
          },
        ],
        transactionErrorNow: [
          {
            monthTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-26, 2024-04-30",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-05-01, 2024-05-02",
                transactionMonth: 5,
                transactionYear: 2024,
              },
            ],
            dayTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-26",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-27",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-28",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-29",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-04-30",
                transactionMonth: 4,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-05-01",
                transactionMonth: 5,
                transactionYear: 2024,
              },
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDate: "2024-05-02",
                transactionMonth: 5,
                transactionYear: 2024,
              },
            ],
            weekTransaction: [
              {
                transactionQty: 0,
                transactionAmount: 0,
                transactionDateRange: "2024-04-26, 2024-05-02",
                transactionWeek: 1,
                transactionMonth: null,
                transactionYear: null,
              },
            ],
          },
        ],
      },
    };
    dispatch(setLoading(true));
    // const response = await serviceTransactionChart(
    //   dataParams,
    //   privilegeCONFIG.DASHBOARD.ALL
    // );
    if (respData.status === 200) {
      dispatch(setTransactionChart(respData?.data));
    } else if (handleExceptionError(respData.status)) {
      dispatch(handleErrorBE(respData));
    }
    dispatch(setLoading(false));
  } catch (e) {
    dispatch(setLoading(false));
    dispatch(setHandleError(e.message));
  }
};
