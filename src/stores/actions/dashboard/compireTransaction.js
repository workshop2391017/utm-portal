import { privilegeCONFIG } from "configuration/privilege";
import { serviceCompareTransaction } from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING_COMPIRE = "dashboard/SET_LOADING_COMPIRE";
export const SET_ERROR_COMPIRE = "dashboard/SET_ERROR_COMPIRE";
export const SET_DATA_COMPIRE = "dashboard/SET_DATA_COMPIRE";
export const SET_HANDLE_ERROR = "dashboard/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "dashboard/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING_COMPIRE,
  payload,
});

export const setDataCompire = (payload) => ({
  type: SET_DATA_COMPIRE,
  payload,
});

export const setError = () => ({
  type: SET_ERROR_COMPIRE,
});

export const handleCompireTransaction = (payload) => async (dispatch) => {
  try {
    const respData = {
      status: 200,
      data: [
        {
          summaryType: "Transaksi Sukses",
          amountNow: 0,
          amountBefore: 0,
        },
        {
          summaryType: "Transaksi Gagal",
          amountNow: 0,
          amountBefore: 0,
        },
        {
          summaryType: "Total Aktivitas",
          amountNow: 0,
          amountBefore: 0,
        },
        {
          summaryType: "Proyeksi Pendapatan",
          amountNow: 0,
          amountBefore: 0,
        },
      ],
    };
    dispatch(setLoading(true));
    // const response = await serviceCompareTransaction(
    //   payload,
    //   privilegeCONFIG.DASHBOARD.ALL
    // );

    if (respData.status === 200) {
      dispatch(setDataCompire(respData.data));
    } else if (handleExceptionError(respData.status)) {
      dispatch(handleErrorBE(respData));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(setHandleError(err.message));
  }
};
