import { privilegeCONFIG } from "configuration/privilege";
import { serviceGetUserActifityDetails } from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { handleExceptionError } from "utils/helpers";

export const SET_LOADING_DETAIL = "dashboard/SET_LOADING_DETAIL";
export const CLEAR_ERROR = "dashboard/CLEAR_ERROR";
export const SET_ERROR_DETAIL = "dashboard/SET_ERROR_DETAIL";
export const SET_ACTIVITY_DETAILS = "dashboard/SET_ACTIVITY_DETAILS";
export const CLEAR_DETAIL_DATA = "dashboard/CLEAR_DETAIL_DATA";
export const SET_HANDLE_ERROR = "dashboard/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "dashboard/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING_DETAIL,
  payload,
});

export const setActivityDetails = (payload) => ({
  type: SET_ACTIVITY_DETAILS,
  payload,
});

export const setErrorhistoryPage = (payload) => ({
  type: SET_ERROR_DETAIL,
  payload,
});

export const setClearDetailData = () => ({
  type: CLEAR_DETAIL_DATA,
});

export const handleGetActivityDetails =
  (dataParams, dataTable) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await serviceGetUserActifityDetails(
        dataParams,
        privilegeCONFIG.DASHBOARD.ALL
      );
      if (response.status === 200) {
        dispatch(
          setActivityDetails({
            ...response?.data,
            detailActivity: { ...response?.data?.detailActivity },
          })
        );
      } else if (handleExceptionError(response.status)) {
        dispatch(handleErrorBE(response));
      }
      dispatch(setLoading(false));
    } catch (e) {
      dispatch(setLoading(false));
      dispatch(setHandleError(e.message));
    }
  };
