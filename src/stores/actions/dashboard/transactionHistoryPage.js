import { privilegeCONFIG } from "configuration/privilege";
import { serviceGetTransactionHistoryPage } from "utils/api";
import { handleExceptionError } from "utils/helpers";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

export const SET_LOADING = "dashboard/SET_LOADING";
export const CLEAR_ERROR = "dashboard/CLEAR_ERROR";
export const SET_ERROR = "dashboard/SET_ERROR";
export const SET_TRANSACTION_HISTORY_PAGE =
  "dashboard/SET_TRANSACTION_HISTORY_PAGE";
export const SET_HANDLE_ERROR = "dashboard/SET_HANDLE_ERROR";
export const SET_HANDLE_CLEAR_ERROR = "dashboard/SET_HANDLE_CLEAR_ERROR";

export const setHandleError = (payload) => ({
  type: SET_HANDLE_ERROR,
  payload,
});

export const setHandleClearError = () => ({
  type: SET_HANDLE_CLEAR_ERROR,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setTransactionHistoryPage = (payload) => ({
  type: SET_TRANSACTION_HISTORY_PAGE,
  payload,
});

export const handleGetTransactionHistoryPage =
  (dataParams) => async (dispatch) => {
    await dispatch(checkOnline(dispatch));
    try {
      const respData = {
        status: 200,
        data: {
          transactionHistoryPage: {
            totalPages: 0,
            totalElements: 0,
            numberOfElements: 0,
            pageNumber: 0,
            content: [],
          },
        },
      };
      dispatch(setLoading(true));
      // const response = await serviceGetTransactionHistoryPage(
      //   dataParams,
      //   privilegeCONFIG.DASHBOARD.ALL
      // );
      dispatch(setLoading(false));
      if (respData.status === 200) {
        dispatch(
          setTransactionHistoryPage(respData?.data?.transactionHistoryPage)
        );
      } else if (handleExceptionError(respData.status)) {
        dispatch(handleErrorBE(respData));
      }
    } catch (e) {
      dispatch(setLoading(false));
      dispatch(setHandleError(e.message));
    }
  };
