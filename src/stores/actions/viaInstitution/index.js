// viaInstitution Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

const {
  serviceRequestVirtualaccountinstitution,
  serviceRequestVirtualaccountinstitutionDetail,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "viaInstitution/SET_LOADING";
export const CLEAR_ERROR = "viaInstitution/CLEAR_ERROR";
export const SET_ERROR = "viaInstitution/SET_ERROR";
export const INIT_DATA = "viaInstitution/INIT_DATA";
export const SET_DOUBLE_SUBMIT = "viaInstitution/SET_DOUBLE_SUBMIT";
export const SET_DATA = "viaInstitution/SET_DATA";
export const SET_DATA_DETAIL = "viaInstitution/SET_DATA_DETAIL";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setTypeDataDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setTypeClearError = () => ({
  type: CLEAR_ERROR,
});

export const getDataInstitutionVIa = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestVirtualaccountinstitution(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      console.warn("res:", res);
      dispatch(setTypeData(res.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.warn("error:", error);

    dispatch(setTypeError(error.message));
  }
};

export const getDataInstitutionVIaDetail = (payload) => async (dispatch) => {
  try {
    const res = await serviceRequestVirtualaccountinstitutionDetail(payload);

    if (res.status === 200) {
      console.warn("res detail:", res);

      const branches = JSON.parse(res.data?.vaInstitutionDtoDetail?.branch);
      const data = {
        ...res.data?.vaInstitutionDtoDetail,
        branch: branches,
      };
      dispatch(setTypeDataDetail(data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    console.warn("error:", error);

    dispatch(setTypeError(error.message));
  }
};
