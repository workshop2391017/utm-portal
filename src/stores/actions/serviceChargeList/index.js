// serviceChargeList Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";

const {
  serviceRequestGetServiceCharges,
  serviceRequestAddServiceCharges,
  serviceRequestAllChargesDropdown,
  serviceChargeListNoPaging,
} = require("utils/api");

/* eslint-disable space-before-function-paren */
export const SET_LOADING = "serviceChargeList/SET_LOADING";
export const CLEAR_ERROR = "serviceChargeList/CLEAR_ERROR";
export const SET_ERROR = "serviceChargeList/SET_ERROR";
export const INIT_DATA = "serviceChargeList/INIT_DATA";
export const SET_DATA = "serviceChargeList/SET_DATA";
export const SET_DATA_DROPDOWN = "serviceChargeList/SET_DATA_DROPDOWN";
export const SET_POP_UP = "serviceChargeList/SET_POP_UP";
export const SET_DATA_DETAIL = "serviceChargeList/SET_DATA_DETAIL";
export const SET_DOUBLE_SUBMIT = "serviceChargeList/SET_DOUBLE_SUBMIT";
export const SET_LOADING_EXECUTE = "serviceChargeList/SET_LOADING_EXECUTE";
export const SET_USED_CHARGE_LIST = "serviceChargeList/SET_USED_CHARGE_LIST";

export const setUsedServiceChargeList = (payload) => ({
  type: SET_USED_CHARGE_LIST,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setTypeDataDropDown = (payload) => ({
  type: SET_DATA_DROPDOWN,
  payload,
});

export const setTypeDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setHandleClearError = () => ({
  type: CLEAR_ERROR,
});

export const setPopUp = (payload) => ({
  type: SET_POP_UP,
  payload,
});

export const setLoadingExecute = (payload) => ({
  type: SET_LOADING_EXECUTE,
  payload,
});

export const getAllDataServiceCharges = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetServiceCharges(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setTypeData(res?.data));
    } else {
      dispatch(setTypeData([]));
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setTypeData([]));
    dispatch(setTypeError(error?.message));
  }
};

export const addDataServiceCharges = (payload) => async (dispatch) => {
  dispatch(setLoadingExecute(true));
  try {
    const res = await serviceRequestAddServiceCharges(payload);

    if (res.status === 200) {
      dispatch(setPopUp(true));
    } else {
      dispatch(handleErrorBE(res));
    }

    dispatch(setLoadingExecute(false));
  } catch (error) {
    dispatch(setTypeError(error?.message));
    dispatch(setLoadingExecute(false));
  }
};

export const getAllDataServiceDropDownCharges =
  (payload, usedService) => async (dispatch) => {
    try {
      const res = await serviceRequestAllChargesDropdown(payload);

      if (res.status === 200) {
        dispatch(
          setTypeDataDropDown({
            chargesDropDown: res.data?.chargesDropDown,
          })
        );
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setTypeError(error?.message));
    }
  };

export const getAllDataServiceChargesNoPaging =
  (payload, menuService) => async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const res = await serviceChargeListNoPaging(payload);
      dispatch(setLoading(false));

      if (res.status === 200) {
        const usedChargeList = [];

        const selectedIds = menuService?.map((elm) => elm.id);
        res?.data?.serviceChargeDtoList?.forEach((e) => {
          if (selectedIds?.includes(e.id)) {
            e?.chargesList?.forEach((c) => {
              usedChargeList?.push(c?.chargeName);
            });
          }
        });

        const payload = { name: null };

        dispatch(getAllDataServiceDropDownCharges(payload, usedChargeList));
      } else {
        dispatch(setUsedServiceChargeList([]));
        dispatch(handleErrorBE(res));
      }
    } catch (error) {
      dispatch(setLoading(false));
      dispatch(setUsedServiceChargeList([]));
      dispatch(setTypeError(error?.message));
    }
  };
