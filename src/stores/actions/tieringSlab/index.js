// tieringSlab Actions
// --------------------------------------------------------
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

/* eslint-disable space-before-function-paren */

const {
  serviceRequestGetTearingSlab,
  serviceRequestGetTearingSlabDetail,
  serviceRequestAddTearingSlab,
} = require("utils/api");

export const SET_LOADING = "tieringSlab/SET_LOADING";
export const CLEAR_ERROR = "tieringSlab/CLEAR_ERROR";
export const SET_ERROR = "tieringSlab/SET_ERROR";
export const INIT_DATA = "tieringSlab/INIT_DATA";
export const SUCCESS_CONFIRMATION = "tieringSlab/SUCCESS_CONFIRMATION";
export const SET_DATA = "tieringSlab/SET_DATA";
export const SET_DATA_DETAIL = "tieringSlab/SET_DATA_DETAIL";
export const SET_DOUBLE_SUBMIT = "tieringSlab/SET_DOUBLE_SUBMIT";
export const SET_DATA_ID = "tieringSlab/SET_DATA_ID";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});

export const setDataTieringSlab = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setDataTieringSlabDetail = (payload) => ({
  type: SET_DATA_DETAIL,
  payload,
});

export const setHandleError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypePopConfirm = (payload) => ({
  type: SUCCESS_CONFIRMATION,
  payload,
});
export const setTypeDataIdValidate = (payload) => ({
  type: SET_DATA_ID,
  payload,
});

export const getDataTieringSlab = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetTearingSlab(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDataTieringSlab(res?.data));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const getDataTieringSlabDetail = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestGetTearingSlabDetail(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setDataTieringSlabDetail(res?.data?.tieringDetailList));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (error) {
    dispatch(setHandleError(error?.message));
  }
};

export const AddDataTieringSlab = (payload, handleSave) => async (dispatch) => {
  dispatch(
    validateTask(
      {
        menuName: validateTaskConstant.TIERING,
        code: payload?.tieringDetailList[0]?.tiering?.kode,
        id: null,
        name: payload?.tieringDetailList[0]?.tiering?.name,
        validate:
          payload?.tieringDetailList[0]?.tiering?.id === null ||
          payload?.tieringDetailList[0]?.tiering?.id === undefined,
        loader: setLoading,
      },
      {
        async onContinue() {
          try {
            dispatch(setLoading(true));
            const res = await serviceRequestAddTearingSlab(payload);
            dispatch(setLoading(false));

            if (res.status === 200) {
              dispatch(setTypePopConfirm(true));
            } else {
              dispatch(handleErrorBE(res));
            }
          } catch (error) {
            dispatch(setHandleError(error?.message));
          }
        },
        onError() {
          handleSave();
        },
      },
      {
        redirect: false,
      }
    )
  );
};
