import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { validateTask } from "../validateTaskPortal";
import { validateTaskConstant } from "../validateTaskPortal/constantValidateTask";

const {
  serviceRequestHandlingOfficer,
  serviceRequestHandlingOfficerAdd,
  serviceRequestHandlingOfficerEdit,
  requestDeleteHandlingOfficer,
  serviceGetAllBranch,
} = require("utils/api");

export const SET_HANDLING_OFFICER_TYPE =
  "handlingOfficer/SET_HOST_ERROR_MAPPING_TYPE";
export const SET_LOADING = "handlingOfficer/SET_HOST_ERROR_SET_LOADING";
export const SET_LOADING_EXCUTE =
  "handlingOfficer/SET_HOST_ERROR_SET_LOADING_EXCUTE";
export const SET_ERROR = "handlingOfficer/SET_HOST_ERROR_SET_ERROR";
export const SET_IS_OPEN = "handlingOfficer/SET_HOST_ERROR_SET_IS_OPEN";
export const SET_CLEAR = "handlingOfficer/SET_HOST_ERROR_SET_CLEAR";
export const SET_IS_OPEN_MODAL =
  "handlingOfficer/SET_HOST_ERROR_SET_IS_OPEN_MODAL";
export const SET_BRANCH = "handlingOfficer/SET_BRANCH";
export const SET_BRANCH_LOADING = "handlingOfficer/SET_BRANCH_LOADING";

export const SET_OPEN_MODAL_CONFIRM =
  "handlingOfficer/SET_HOST_ERROR_SET_OPEN_MODAL_CONFIRM ";

export const setBranch = (payload) => ({
  type: SET_BRANCH,
  payload,
});

export const setBranchLoading = (payload) => ({
  type: SET_BRANCH_LOADING,
  payload,
});

export const setTypeHandlingOfficer = (payload) => ({
  type: SET_HANDLING_OFFICER_TYPE,
  payload,
});

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});
export const setLoadingExcute = (payload) => ({
  type: SET_LOADING_EXCUTE,
  payload,
});

export const setTypeError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setTypeIsOpen = (payload) => ({
  type: SET_IS_OPEN,
  payload,
});

export const setHandleClearErrorOfficer = () => ({
  type: SET_CLEAR,
});

export const setTypeOpenModal = (payload) => ({
  type: SET_IS_OPEN_MODAL,
  payload,
});

export const setTypePopUpConfirm = (payload) => ({
  type: SET_OPEN_MODAL_CONFIRM,
  payload,
});

export const getDataHandlingOfficer = (payload) => async (dispatch) => {
  await dispatch(checkOnline());
  try {
    dispatch(setLoading(true));
    const res = await serviceRequestHandlingOfficer(payload);
    dispatch(setLoading(false));

    if (res.status === 200) {
      dispatch(setTypeHandlingOfficer(res?.data));
    } else if (res.status === 400) {
      dispatch(setTypeHandlingOfficer([]));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err?.message));
  }
};

export const addDataHandlingOfficer =
  (payload, payloadValidate, handleNext) => async (dispatch) => {
    await dispatch(checkOnline());
    dispatch(
      validateTask(
        {
          menuName: validateTaskConstant.HANDLING_OFFICER,
          ...payloadValidate,
          loader: setLoadingExcute,
        },
        {
          async onContinue() {
            try {
              dispatch(setLoadingExcute(true));
              const res = await serviceRequestHandlingOfficerAdd({
                actionWorkflow: "ADD",
                data: payload,
              });
              dispatch(setLoadingExcute(false));
              if (res.status === 200) {
                dispatch(setTypeIsOpen(true));
              } else {
                dispatch(handleErrorBE(res));
              }
            } catch (err) {
              dispatch(setTypeError(err?.message));
            }
          },
          onError() {
            // handle callback error
            // ! popups callback error sudah dihandle !
            // ex. clear form, redirect, etc
            handleNext();
            dispatch(setLoadingExcute(false));
          },
        },
        {
          redirect: false,
        }
      )
    );
  };

export const editDataHandlingOfficer =
  (payload, payloadValidate, handleNext) => async (dispatch) => {
    await dispatch(checkOnline());
    try {
      dispatch(setLoadingExcute(true));
      const res = await serviceRequestHandlingOfficerEdit({
        actionWorkflow: "EDIT",
        data: payload,
      });
      dispatch(setLoadingExcute(false));
      if (res.status === 200) {
        dispatch(setTypeIsOpen(true));
      } else {
        dispatch(handleErrorBE(res));
      }
    } catch (err) {
      dispatch(setTypeError(err?.message));
      dispatch(setLoadingExcute(false));
    }
  };

export const deleteDataHandlingOfficer = (payload) => async (dispatch) => {
  await dispatch(checkOnline());
  try {
    dispatch(setLoadingExcute(true));
    const res = await requestDeleteHandlingOfficer({
      actionWorkflow: "DELETE",
      data: payload,
    });
    dispatch(setLoadingExcute(false));

    if (res.status === 200) {
      dispatch(setTypePopUpConfirm(true));
    } else {
      dispatch(handleErrorBE(res));
    }
  } catch (err) {
    dispatch(setTypeError(err?.message));
  }
};

export const handleGetAllBranch = (payload) => async (dispatch) => {
  try {
    dispatch(setBranchLoading(true));
    const response = await serviceGetAllBranch();
    dispatch(setBranchLoading(false));
    if (response.status === 200) {
      dispatch(setBranch(response.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setBranchLoading(false));
    dispatch(setTypeError(e.message));
  }
};
