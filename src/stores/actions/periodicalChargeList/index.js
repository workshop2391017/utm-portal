import { serviceRequestPeriodicalChargeList } from "utils/api";
import { handleErrorBE, checkOnline } from "stores/actions/errorGeneral";
import { handleExceptionError } from "utils/helpers";

export const SET_LOADING = "periodicalChargeList/SET_LOADING";
export const SET_ERROR = "periodicalChargeList/SET_ERROR";
export const SET_PAGES = "periodicalChargeList/SET_PAGES";
export const INIT_DATA = "periodicalChargeList/INIT_PAGES";
export const GET_DATA_PERIODICAL_CHARGE_LIST =
  "periodicalChargeList/GET_DATA_PERIODICAL_CHARGE_LIST";
export const SET_DATA_PERIODICAL_CHARGE_LIST =
  "periodicalChargeList/SET_DATA_PERIODICAL_CHARGE_LIST";

export const setHandlingError = (payload) => ({
  type: SET_ERROR,
  payload,
});
export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});
export const setDataPeriodicalChargeList = (payload) => ({
  type: SET_DATA_PERIODICAL_CHARGE_LIST,
  payload,
});
export const setPages = (payload) => ({
  type: SET_PAGES,
  payload,
});

export const getPeriodicalChargeList = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await serviceRequestPeriodicalChargeList(payload);
    dispatch(setLoading(false));
    if (response?.status === 200) {
      dispatch(setDataPeriodicalChargeList(response?.data));
    } else {
      dispatch(handleErrorBE(response));
    }
  } catch (e) {
    dispatch(setHandlingError(e?.data?.message));
  }
};
