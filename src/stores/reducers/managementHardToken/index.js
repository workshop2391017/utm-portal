import {
  SET_LOADING,
  SET_LOADING_SUBMIT,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_CORPORATE_LIST,
  SET_CORPORATE_USER_LIST,
  SET_CORPORATE_TOKEN_LIST,
} from "stores/actions/managementHardToken";

const initialState = {
  isLoading: false,
  isLoadingSubmit: false,
  error: { isError: false, message: "" },
  corporateList: [],
  corporateTokenList: {},
  corporateUserList: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_LOADING_SUBMIT:
      return {
        ...state,
        isLoadingSubmit: payload,
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    case SET_CORPORATE_LIST:
      return {
        ...state,
        corporateList: payload,
      };
    case SET_CORPORATE_TOKEN_LIST:
      return {
        ...state,
        corporateTokenList: payload,
      };
    case SET_CORPORATE_USER_LIST:
      return {
        ...state,
        corporateUserList: payload,
      };
    default:
      return state;
  }
};
