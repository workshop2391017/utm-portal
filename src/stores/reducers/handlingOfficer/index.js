import {
  SET_HANDLING_OFFICER_TYPE,
  SET_ERROR,
  SET_IS_OPEN,
  SET_LOADING,
  SET_CLEAR,
  SET_IS_OPEN_MODAL,
  SET_LOADING_EXCUTE,
  SET_OPEN_MODAL_CONFIRM,
  SET_BRANCH,
  SET_BRANCH_LOADING,
} from "stores/actions/handlingOfficer";

const initialState = {
  data: [],
  isLoading: false,
  isLoadingExcute: false,
  error: {},
  isOpenModal: false,
  openModal: false,
  modalConfirm: false,
  branch: [],
  isLoadingBranch: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_HANDLING_OFFICER_TYPE:
      return {
        ...state,
        data: payload,
      };

    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_IS_OPEN:
      return {
        ...state,
        isOpenModal: payload,
      };

    case SET_CLEAR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };

    case SET_IS_OPEN_MODAL:
      return {
        ...state,
        openModal: payload,
      };

    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };

    case SET_OPEN_MODAL_CONFIRM:
      return {
        ...state,
        modalConfirm: payload,
      };

    case SET_BRANCH:
      return {
        ...state,
        branch: payload,
      };

    case SET_BRANCH_LOADING:
      return {
        ...state,
        isLoadingBranch: payload,
      };

    default:
      return state;
  }
};
