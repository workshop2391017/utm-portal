// globalLimitTrx Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_LOADING_SUBMIT,
  GET_DATA_SERVICE,
  GET_DATA_TRANSACTION_CATEGORY,
  GET_DATA_CURRENCY_MATRIX,
  SET_HANDLE_CLEAR_ERROR,
  SET_SUCCESS_CONFRIMATION,
  GET_DATA_MATRIX,
  GET_DATA_GLOBAL_LIMIT,
  SET_GLOBAL_LIMIT_TEMP,
  SET_SUCCESS_CONFRIMATION_GLOBAL_LIMIT,
  SET_LOADING_CARD,
  INIT_DATA_CURRENCY,
  SET_IS_ADD_SERVICE,
  DATA_CARD,
  SET_IS_ALREADY_EXIST,
} from "stores/actions/globalLimitTrx";

const initialState = {
  openSuccess: false,
  serviceCurrencyMatrixList: [],
  serviceList: null,
  transactionCategoryList: null,
  isLoading: true,
  isLoadingCard: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  dataMatrixDetail: {},
  dataGlobalLimit: [],
  dataGlobalLimitTemp: [],
  formData: {},
  openSuccessGLTRX: false,
  currencyList: [],
  isAddService: 0,
  dataCard: null,
  isAlreadyExist: false,
  isLoadingSubmit: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_LOADING_CARD:
      return {
        ...state,
        isLoadingCard: payload,
      };
    case SET_LOADING_SUBMIT:
      return {
        ...state,
        isLoadingSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case GET_DATA_SERVICE:
      return {
        ...state,
        serviceList: payload,
      };
    case GET_DATA_TRANSACTION_CATEGORY:
      return {
        ...state,
        transactionCategoryList: payload,
      };
    case GET_DATA_CURRENCY_MATRIX:
      return {
        ...state,
        serviceCurrencyMatrixList: payload,
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    case SET_SUCCESS_CONFRIMATION:
      return {
        ...state,
        openSuccess: payload,
      };

    case GET_DATA_MATRIX:
      return {
        ...state,
        dataMatrixDetail: payload,
      };
    case GET_DATA_GLOBAL_LIMIT:
      return {
        ...state,
        dataGlobalLimit: payload,
      };
    case SET_GLOBAL_LIMIT_TEMP:
      return {
        ...state,
        dataGlobalLimitTemp: payload,
      };
    case SET_SUCCESS_CONFRIMATION_GLOBAL_LIMIT: {
      return {
        ...state,
        openSuccessGLTRX: payload,
      };
    }
    case INIT_DATA_CURRENCY:
      return {
        ...state,
        currencyList: payload,
      };

    case SET_IS_ADD_SERVICE:
      return {
        ...state,
        isAddService: payload,
      };

    case DATA_CARD:
      return {
        ...state,
        dataCard: payload,
      };
    case SET_IS_ALREADY_EXIST:
      return {
        ...state,
        isAlreadyExist: payload,
      };

    default:
      return state;
  }
};
