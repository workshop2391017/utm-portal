import {
  SET_GLOBAL_LIMIT_ERROR_TYPE,
  SET_GLOBAL_LIMIT_TYPE,
} from "stores/actions/globalLimit";

const initialState = {
  data: [],
  error: {},
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_GLOBAL_LIMIT_TYPE:
      return {
        ...state,
        data: payload,
      };
    case SET_GLOBAL_LIMIT_ERROR_TYPE:
      return {
        ...state,
        error: payload,
      };

    default:
      return state;
  }
};
