// pengaturanKelompok Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA_GROUP,
  SET_DATA_GROUP_DETAIL,
  SET_DATA_FEATURES,
  SET_DATA_LIMIT,
  SET_EDIT_GROUP,
  SET_POP_UP_DELETE_SUCCESS,
  SET_POP_UP_ADD_SUCCESS,
  SET_CORPORATE_SEGMENTATION_ID,
  SET_PENGATURAN_KELOMPOK_CLEAR_ERROR,
  SET_LIST_GROUP_REKENING,
  SET_DATA_LIST_FEATURES,
  SET_COMPANY_ID,
  SET_LOADING_LIMIT,
  SET_IS_VALID_CODE,
  SET_IS_VALID_NAME,
} from "stores/actions/pengaturanKelompok";

const initialState = {
  isLoading: false,
  isLoadingLimit: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isSuccess: false,
  error: { isError: false, message: "" },
  dataGroup: [],
  dataDetailGroup: {},
  userFeatures: {},
  dataListFeatures: {},
  limit: [],
  popUpSuccessDelete: false,
  corporateSegmentationIdPayload: null,
  dataEditGroup: {},
  listRekeningGroup: [],
  dataCompanyId: 0,
  isValidCode: true,
  isValidName: true,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_IS_VALID_CODE:
      return {
        ...state,
        isValidCode: payload,
      };
    case SET_IS_VALID_NAME:
      return {
        ...state,
        isValidName: payload,
      };
    case SET_LOADING_LIMIT:
      return {
        ...state,
        isLoadingLimit: payload,
      };
    case SET_LIST_GROUP_REKENING:
      return {
        ...state,
        listRekeningGroup: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_COMPANY_ID:
      return {
        ...state,
        dataCompanyId: payload,
      };
    case SET_DATA_GROUP:
      return {
        ...state,
        dataGroup: payload,
      };
    case SET_DATA_GROUP_DETAIL:
      return {
        ...state,
        dataDetailGroup: payload,
      };

    case SET_DATA_FEATURES:
      return {
        ...state,
        userFeatures: payload,
      };
    case SET_DATA_LIST_FEATURES:
      return {
        ...state,
        dataListFeatures: payload,
      };
    case SET_DATA_LIMIT:
      return {
        ...state,
        limit: payload,
      };
    case SET_EDIT_GROUP:
      return {
        ...state,
        dataEditGroup: payload,
      };
    case SET_POP_UP_DELETE_SUCCESS:
      return {
        ...state,
        popUpSuccessDelete: payload,
      };
    case SET_POP_UP_ADD_SUCCESS:
      return {
        ...state,
        isSuccess: payload,
      };
    case SET_CORPORATE_SEGMENTATION_ID:
      return {
        ...state,
        corporateSegmentationIdPayload: payload,
      };
    case SET_PENGATURAN_KELOMPOK_CLEAR_ERROR: {
      return {
        ...state,
        error: {
          isError: false,
          message: "",
        },
      };
    }

    default:
      return state;
  }
};
