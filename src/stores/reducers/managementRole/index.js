//  Reducers Privilege
// --------------------------------------------------------
import {
  SET_LOADING,
  SET_GET_ALL_ROLE,
  SET_ALL_PRIVILEGE,
  SET_LOADING_PRIVILEGE,
  SET_LOADING_DETAIL,
  SET_DETAIL_ROLE,
  SET_DUPLICATE,
  SET_LOADING_SUBMIT,
  SET_CLEAR_DETAIL,
  SET_LOADING_DUPLICATE,
  SET_CLEAR_DUPLICATE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/managementRole";

const initialState = {
  isLoading: false,
  isLoadingPrivilege: false,
  isLoadingDetail: false,
  isLoadingSubmit: false,
  isLoadingDuplicate: false,
  data: [],
  privilege: {},
  detailRole: {},
  duplicateData: {},
  error: { isError: false, message: "" },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_GET_ALL_ROLE:
      return {
        ...state,
        data: payload,
      };
    case SET_LOADING_PRIVILEGE:
      return {
        ...state,
        isLoadingPrivilege: payload,
      };
    case SET_LOADING_DETAIL:
      return {
        ...state,
        isLoadingDetail: payload,
      };
    case SET_ALL_PRIVILEGE:
      return {
        ...state,
        privilege: payload,
      };
    case SET_DETAIL_ROLE:
      return {
        ...state,
        detailRole: payload,
      };

    case SET_LOADING_SUBMIT:
      return {
        ...state,
        isLoadingSubmit: payload,
      };
    case SET_DUPLICATE: {
      return {
        ...state,
        duplicateData: { ...payload, roleName: `${payload?.roleName} - Copy` },
      };
    }
    case SET_CLEAR_DETAIL:
      return {
        ...state,
        detailRole: {},
      };
    case SET_LOADING_DUPLICATE:
      return {
        ...state,
        isLoadingDuplicate: payload,
      };
    case SET_CLEAR_DUPLICATE:
      return {
        ...state,
        duplicateData: {},
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
