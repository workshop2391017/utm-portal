// login Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_USER_LOGIN,
  SET_DATA_FORM_LOGIN,
  SET_DATA_USER_LOGIN,
  SET_ERROR_ALREADY_LOGIN,
  SET_IS_BLOCKIR_ACCOUNT,
  SET_IPCHECKING,
} from "stores/actions/login";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isOpenOTP: false,
  error: { isError: false, message: "" },
  user: {},
  dataFormUser: {},
  errorAlreadyLogin: { isError: false, message: "" },
  isBlockirAccount: false,
  isIpChecking: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DATA_FORM_LOGIN:
      return {
        ...state,
        dataFormUser: payload,
      };
    case SET_ERROR_ALREADY_LOGIN:
      return {
        ...state,
        errorAlreadyLogin: payload,
      };
    case SET_IS_BLOCKIR_ACCOUNT:
      return {
        ...state,
        isBlockirAccount: payload,
      };
    case SET_DATA_USER_LOGIN:
      return {
        ...state,
        user: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: "" },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_USER_LOGIN:
      return {
        ...state,
        user: payload,
      };

    case SET_IPCHECKING:
      return {
        ...state,
        isIpChecking: payload,
      };
    default:
      return state;
  }
};
