// login Reducers
// --------------------------------------------------------

import {
  SET_ALL_FORM_MANAGEMENTUSER,
  SET_CLEAR_USERDATA,
  SET_LOADING,
  SET_LOADING_SUBMIT,
  SET_USER_DATA,
  SET_LOADING_USER_LDAP,
  SET_USER_LDAP,
  SET_ERROR_LDAP,
  SET_CLEAR_ERROR_LDAP,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_ALLROLE,
  SET_LOADING_ALLROLE,
  SET_BRANCH_DATA,
  SET_LOADING_ALLBRANCH,
  SET_LOADING_GROUP_BRANCHID,
  SET_DATA_GROUP_BRANCH_ID,
  SET_ERROR_USER_ALREADY_EXIST,
} from "stores/actions/managementuser";

const initialState = {
  isLoading: false,
  isLoadingSubmit: false,
  error: { isError: false, message: "" },
  userAdminPaging: {},
  userLdap: null,
  isLoadingLdap: false,
  isErrorLdap: false,
  isLoadingRole: false,
  isLoadingBranch: false,
  branch: null,
  role: null,
  isLoadingGroupBranch: false,
  dataGroupBranchId: [],
  userData: {
    branch: null,
    email: null,
    idUser: null,
    name: null,
    nip: null,
    role: null,
    status: null,
    username: null,
    branchCode: null,
    roleId: null,
  },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_ALL_FORM_MANAGEMENTUSER:
      return {
        ...state,
        userAdminPaging: {
          ...payload,
          listPortalUser:
            payload.listPortalUser.map((elm) => ({
              ...elm,
              branchCode:
                payload?.olistBranch.find(
                  ({ branchName }) => branchName === elm?.branch
                )?.branchCode ?? null,
              officeType:
                payload?.olistBranch.find(
                  ({ branchName }) => branchName === elm?.branch
                )?.officeType ?? null,
              roleId: payload?.olistRole?.find(
                ({ roleName }) => roleName === elm?.role
              )?.roleId,
            })) ?? null,
        },
      };

    case SET_USER_DATA:
      return {
        ...state,
        userData: {
          ...state.userData,
          ...payload,
        },
      };

    case SET_LOADING_SUBMIT:
      return {
        ...state,
        isLoadingSubmit: payload,
      };

    case SET_CLEAR_USERDATA:
      return {
        ...state,
        userData: {
          branch: null,
          email: null,
          idUser: null,
          name: null,
          nip: null,
          role: null,
          status: null,
          username: null,
          branchCode: null,
          roleId: null,
        },
      };
    case SET_LOADING_USER_LDAP:
      return {
        ...state,
        isLoadingLdap: payload,
      };
    case SET_USER_LDAP:
      return {
        ...state,
        userLdap: payload,
      };
    case SET_ERROR_LDAP:
      return {
        ...state,
        isErrorLdap: payload,
      };

    case SET_CLEAR_ERROR_LDAP:
      return {
        ...state,
        isErrorLdap: false,
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    case SET_LOADING_ALLBRANCH:
      return {
        ...state,
        isLoadingBranch: payload,
      };

    case SET_LOADING_ALLROLE:
      return {
        ...state,
        isLoadingRole: payload,
      };

    case SET_ALLROLE:
      return {
        ...state,
        role: payload,
      };
    case SET_BRANCH_DATA:
      return {
        ...state,
        branch: payload,
      };
    case SET_LOADING_GROUP_BRANCHID:
      return {
        ...state,
        isLoadingGroupBranch: payload,
      };
    case SET_DATA_GROUP_BRANCH_ID:
      return {
        ...state,
        dataGroupBranchId: payload,
      };

    case SET_ERROR_USER_ALREADY_EXIST:
      return {
        ...state,
        isUserAlreadyExist: payload,
      };
    default:
      return state;
  }
};
