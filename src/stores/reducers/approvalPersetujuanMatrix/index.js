// approvalPersetujuanMatrix Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_CLEAR,
  SET_ERROR,
  SET_INITIAL_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DETAIL_DATA,
  SET_SUCCESS_APPROVE,
  SET_SUCCESS_REJECT,
  SET_LOADING_EXECUTE,
  SET_LOADING_ALL_MENU,
  SET_ALL_MENU,
  SET_DATA_REFF,
} from "stores/actions/approvalPersetujuanMatrix";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isSuccessApprove: { isSuccess: false },
  isSuccessReject: false,
  isLoadingAllMenu: false,
  allMenu: [],
  data: [],
  detail: {},
  error: { isError: false, message: "" },
  dataReffBulk: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_CLEAR:
      return {
        ...state,
        error: { isError: false, message: "" },
        isLoading: false,
        isDoubleSubmit: false,
        isSubmitting: false,
        isSuccessApprove: false,
        isSuccessReject: false,
      };
    case SET_INITIAL_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL_DATA:
      return {
        ...state,
        detail: payload,
      };
    case SET_SUCCESS_APPROVE:
      return {
        ...state,
        isSuccessApprove: payload,
      };
    case SET_SUCCESS_REJECT:
      return {
        ...state,
        isSuccessReject: payload,
      };
    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };
    case SET_LOADING_ALL_MENU:
      return {
        ...state,
        isLoadingAllMenu: payload,
      };
    case SET_ALL_MENU:
      return {
        ...state,
        allMenu: payload,
      };
    case SET_DATA_REFF:
      return {
        ...state,
        dataReffBulk: payload,
      };

    default:
      return state;
  }
};
