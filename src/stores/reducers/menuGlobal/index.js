// menuGlobal Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA_MENU_GELOBAL,
  DATA_DETAIL,
  ADD_SUCCESS,
  SET_DATA_DROPDOWN,
} from "stores/actions/menuGlobal";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  menuAccessList: [],
  dropDown: [],
  dataDetail: [],
  isOpen: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };

    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA_MENU_GELOBAL:
      return {
        ...state,
        menuAccessList: payload,
      };

    case SET_DATA_DROPDOWN:
      return {
        ...state,
        dropDown: payload?.map((item, index) => {
          const bahasa = "US";
          return {
            name: bahasa === "IDN" ? item.name_id : item.name_en,
            id: item.id,
            index,
            // value: bahasa === "IDN" ? item.name_id : item.name_en,
          };
        }),
      };

    case DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };

    case ADD_SUCCESS:
      return {
        ...state,
        isOpen: payload,
      };

    default:
      return state;
  }
};
