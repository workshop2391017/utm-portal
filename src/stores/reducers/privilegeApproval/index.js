// privilrgeApproval Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DETAIL_DATA,
  SET_SUCCESS_REJECT,
  SET_SUCCESS_APPROVE,
  SET_CLEAR,
  SET_LOADING_APPROVE,
  SET_DATA_ID_BULK,
} from "stores/actions/privilegeApproval";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  data: [],
  error: { isError: false, message: "" },
  detailData: {},
  isSuccessApprove: false,
  isSuccessReject: false,
  isLoadingApprove: false,
  idBulk: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: "" },
        isLoading: false,
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL_DATA:
      return {
        ...state,
        detailData: payload,
      };
    case SET_SUCCESS_REJECT:
      return {
        ...state,
        isSuccessReject: payload,
      };
    case SET_SUCCESS_APPROVE:
      return {
        ...state,
        isSuccessApprove: payload,
      };

    case SET_CLEAR:
      return {
        ...state,
        isSuccessApprove: false,
        isSuccessReject: false,
        isLoading: false,
        error: { isError: false, message: "" },
      };

    case SET_LOADING_APPROVE:
      return {
        ...state,
        isLoadingApprove: payload,
      };
    case SET_DATA_ID_BULK:
      return {
        ...state,
        idBulk: payload,
      };
    default:
      return state;
  }
};
