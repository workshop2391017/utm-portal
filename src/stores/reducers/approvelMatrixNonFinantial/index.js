// approvelMatrixNonFinantial Reducers
// --------------------------------------------------------

import {
  SET_LOADING_SIDEBAR,
  SET_LOADING_CARD,
  SET_LOADING_ALL_SERVICE,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_MENU_SIDEBAR,
  SET_DATA_CARD,
  SET_DATA_DROPDOWN,
  SET_SUCCESS,
  SET_MENU_SELECTED,
  SET_ALL_SERVICE,
  SET_IS_CHECK_SERVICE,
  SET_IDCOMPANY,
} from "stores/actions/approvelMatrixNonFinantial";

const initialState = {
  loadingSidebar: false,
  loadingCard: false,
  loadingAllService: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  menuSideBar: [],
  dataCard: null,
  targetGroups: [],
  isOpen: false,
  dataEdit: null,
  menuSelected: {},
  allService: {},
  dataServiceIsCheck: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING_SIDEBAR:
      return {
        ...state,
        loadingSidebar: payload,
      };
    case SET_LOADING_CARD:
      return {
        ...state,
        loadingCard: payload,
      };
    case SET_LOADING_ALL_SERVICE:
      return {
        ...state,
        loadingAllService: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_MENU_SIDEBAR:
      return {
        ...state,
        menuSideBar: payload,
      };

    case SET_DATA_CARD:
      return {
        ...state,
        dataCard: payload,
      };

    case SET_DATA_DROPDOWN:
      return {
        ...state,
        targetGroups: payload,
      };

    case SET_SUCCESS:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_MENU_SELECTED:
      return {
        ...state,
        menuSelected: payload,
      };
    case SET_ALL_SERVICE:
      return {
        ...state,
        allService: payload,
      };
    case SET_IS_CHECK_SERVICE:
      return {
        ...state,
        dataServiceIsCheck: payload,
      };
    case SET_IDCOMPANY:
      return {
        ...state,
        dataCompanyId: payload,
      };

    default:
      return state;
  }
};
