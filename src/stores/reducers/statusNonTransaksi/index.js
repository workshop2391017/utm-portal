// statusTransaksi Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DATA_NON_TRANSAKSI,
  SET_DATA_NON_TRANSAKSI_DETAIL,
  SET_PAGES,
  SET_TASK_INFORMATION,
} from "stores/actions/statusNonTransaksi";

const initialState = {
  isLoading: false,
  error: {},
  dataNonTransaksi: {},
  dataTransaksiDetail: {},
  taskInformation: {},
  pages: 0,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA_NON_TRANSAKSI:
      return {
        ...state,
        dataNonTransaksi: payload,
      };
    case SET_DATA_NON_TRANSAKSI_DETAIL:
      return {
        ...state,
        dataNonTransaksiDetail: payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    case SET_TASK_INFORMATION:
      return {
        ...state,
        taskInformation: payload,
      };
    default:
      return state;
  }
};
