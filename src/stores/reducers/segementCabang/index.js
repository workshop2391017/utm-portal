// segementCabang Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_BRANCH,
  SUCCESS_CONFIRMATION,
  SET_DATA_DETAIL,
  SET_DATA_EDIT,
  CLEAR_DATA_DETAIL,
  SET_LOADING_EXCUTE,
} from "stores/actions/segementCabang";

const initialState = {
  isLoading: false,
  isLoadingExcute: false,
  data: {},
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  dataBranch: [],
  dataDetail: {},
  isOpen: false,
  dataEdit: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_DATA_BRANCH: {
      return {
        ...state,
        dataBranch: payload,
      };
    }

    case SUCCESS_CONFIRMATION: {
      return {
        ...state,
        isOpen: payload,
      };
    }

    case SET_DATA_DETAIL: {
      return {
        ...state,
        dataDetail: payload,
      };
    }
    case SET_DATA_EDIT:
      return {
        ...state,
        dataEdit: payload,
      };
    case CLEAR_DATA_DETAIL:
      return {
        ...state,
        dataDetail: {},
      };

    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };
    default:
      return state;
  }
};
