// matrixApproval Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA_MENU,
  SET_DOUBLE_SUBMIT,
  SET_MATRIX_SQUENCE,
  SET_SELECTED_MENU,
  SET_LOADING_SQUENCE,
  SET_ACTION_APPROVAL_MATRIX_CUSTOMER,
  SET_ACTION_APPROVAL_MATRIX_ADMIN,
  ADD_MATRIX_APPROVAL,
  SET_MATRIX_APPROVAL_TARGET_GRP,
  SET_LOADING_SAVE,
  SET_SUCCESS_SAVE,
  SET_LOADING_DELETE,
  SET_SUCCESS_DELETE,
  SET_CLEAR,
  SET_ALL_LAYANAN,
  SET_LOADING_PACKAGE,
  SET_CLEAR_ERROR,
  SET_SELECTED_KEYS,
} from "stores/actions/matrixApproval";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isLoadingSquence: false,
  isLoadingSave: false,
  isSuccessSave: false,
  isSuccessDelete: false,
  isLoadingDelete: false,
  menuList: [],
  matrixSquence: {},
  selectedMenu: {},
  allLayananMenu: [],
  actionAdmin: ADD_MATRIX_APPROVAL,
  selectedKeys: [],
  error: {},
  group: [
    {
      label: "Group All",
      value: "GROUP_ALL",
    },
    {
      label: "Group Same",
      value: "GROUP_SAME",
    },
    {
      label: "Group Difference",
      value: "GROUP_DIFFERENT",
    },
    {
      label: "Group Spesific",
      value: "GROUP_SPECIFIC",
    },
  ],
  groupNasabah: [
    {
      label: "Group Spesific",
      value: "GROUP_SPECIFIC",
    },
  ],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA_MENU:
      return {
        ...state,
        menuList: payload,
      };
    case SET_MATRIX_SQUENCE:
      return {
        ...state,
        matrixSquence: payload,
      };
    case SET_SELECTED_MENU:
      return {
        ...state,
        selectedMenu: payload,
      };
    case SET_LOADING_SQUENCE:
      return {
        ...state,
        isLoadingSquence: payload,
      };

    case SET_ACTION_APPROVAL_MATRIX_ADMIN:
      return {
        ...state,
        actionAdmin: payload,
      };
    case SET_ACTION_APPROVAL_MATRIX_CUSTOMER:
      return {
        ...state,
        actionCustomer: payload,
      };
    case SET_MATRIX_APPROVAL_TARGET_GRP:
      return {
        ...state,
        targetGroup: payload,
      };
    case SET_LOADING_SAVE:
      return {
        ...state,
        isLoadingSave: payload,
      };
    case SET_SUCCESS_SAVE:
      return {
        ...state,
        isSuccessSave: payload,
      };
    case SET_LOADING_DELETE:
      return {
        ...state,
        isLoadingDelete: payload,
      };
    case SET_SUCCESS_DELETE:
      return {
        ...state,
        isSuccessDelete: payload,
      };
    case SET_CLEAR:
      return {
        ...state,
        isLoading: false,
        isLoadingDelete: false,
        isLoadingSave: false,
        isSuccessDelete: false,
        isSuccessSave: false,
        ...payload,
      };
    case SET_ALL_LAYANAN:
      return {
        ...state,
        allLayananMenu: payload,
      };

    case SET_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: "" },
      };

    case SET_SELECTED_KEYS:
      return {
        ...state,
        selectedKeys: payload,
      };
    default:
      return state;
  }
};
