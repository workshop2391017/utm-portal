// companyLimit Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_DETAIL,
  SET_POP_UP,
  SET_CURRENCY,
  SET_PAYLOAD_DETAIL,
  SET_LOADING_EXCUTE,
  SET_VALIDATION_LIMIT,
  SET_CLEAR_VALIDATION_LIMIT,
} from "stores/actions/companyLimit";

const initialState = {
  isLoading: false,
  isLoadingExcute: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: null,
  dataDetail: null,
  isOpen: false,
  dataCurrency: [],
  payloadDetail: null,
  validationLimit: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };

    case SET_POP_UP:
      return {
        ...state,
        isOpen: payload,
      };

    case SET_CURRENCY:
      return {
        ...state,
        dataCurrency: payload,
      };

    case SET_PAYLOAD_DETAIL:
      return {
        ...state,
        payloadDetail: payload,
      };

    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };
    case SET_VALIDATION_LIMIT:
      return {
        ...state,
        validationLimit: { ...state?.validationLimit, ...payload },
      };
    case SET_CLEAR_VALIDATION_LIMIT:
      return {
        ...state,
        validationLimit: {},
      };
    default:
      return state;
  }
};
