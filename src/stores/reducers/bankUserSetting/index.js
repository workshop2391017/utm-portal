// bankUserSetting Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_USERID,
  SET_DETAIL_DATA,
  SET_BLOCKUNBLOCK,
  SET_ACTION_LOADING,
  SET_SUCCESS,
} from "stores/actions/bankUserSetting";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: null,
  userId: null,
  detail: null,
  blockUnBlock: { action: null },
  actionLoading: {},
  isSuccess: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_SUCCESS:
      return {
        ...state,
        isSuccess: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_USERID:
      return {
        ...state,
        userId: payload,
      };
    case SET_DETAIL_DATA:
      return {
        ...state,
        detail: payload,
      };
    case SET_BLOCKUNBLOCK:
      return {
        ...state,
        blockUnBlock: payload,
      };
    case SET_ACTION_LOADING:
      return {
        ...state,
        actionLoading: payload,
      };
    default:
      return state;
  }
};
