// serviceChargeList Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_DETAIL,
  SET_POP_UP,
  SET_DATA_DROPDOWN,
  SET_LOADING_EXECUTE,
  SET_USED_CHARGE_LIST,
} from "stores/actions/serviceChargeList";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  data: null,
  dataDetail: null,
  isOpen: false,
  error: {},
  dataDropDown: null,
  isLoadingExecute: false,
  usedService: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };

    case SET_POP_UP:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_DATA_DROPDOWN:
      return {
        ...state,
        dataDropDown: payload,
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };

    case SET_USED_CHARGE_LIST:
      return {
        ...state,
        usedService: payload,
      };
    default:
      return state;
  }
};
