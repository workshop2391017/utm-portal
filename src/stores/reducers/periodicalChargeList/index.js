import {
  SET_LOADING,
  SET_ERROR,
  SET_PAGES,
  INIT_DATA,
  SET_DATA_PERIODICAL_CHARGE_LIST,
} from "stores/actions/periodicalChargeList";

const initialState = {
  isLoading: false,
  error: {},
  dataPeriodicalChargeList: [],
  pages: 0,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA_PERIODICAL_CHARGE_LIST:
      return {
        ...state,
        dataPeriodicalChargeList: payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    default:
      return state;
  }
};
