// listPromo Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_PAGES,
  SET_DETAIL,
  SET_TABLE_PAGE,
  SET_PROMO_CATEGORY,
  SET_LOADING_MODAL,
  SET_BRANCH,
  SET_SEGMENTATION,
  SET_ISEXECUTE,
  SET_PROMO_CODE,
  SET_CATEGORIES,
  SET_MERCHANTS,
  SET_CATEGORIES_FIELD,
  SET_POP_UP,
  SET_IS_ALREADY_EXIST,
} from "stores/actions/promo";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: [],
  detail: null,
  pages: 0,
  page: 1,
  isLoadingCategory: false,
  promoCategory: [],
  segmentation: [],
  branch: [],
  categories: [],
  isExecute: false,
  promoId: null,
  promoCode: null,
  merchants: [],
  isCategoriesField: {
    code: 0,
    name: "",
  },
  popUp: false,
  isAlreadyExist: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_PROMO_CODE:
      return {
        ...state,
        promoCode: payload,
      };
    case SET_MERCHANTS:
      return {
        ...state,
        merchants: payload,
      };
    case SET_CATEGORIES:
      return {
        ...state,
        categories: payload,
      };
    case SET_BRANCH:
      return {
        ...state,
        branch: payload,
      };

    case SET_SEGMENTATION:
      return {
        ...state,
        segmentation: payload,
      };

    case SET_PROMO_CATEGORY:
      return {
        ...state,
        promoCategory: payload,
      };

    case SET_LOADING_MODAL:
      return {
        ...state,
        isLoadingCategory: false,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_TABLE_PAGE:
      return {
        ...state,
        page: payload,
      };
    case SET_ISEXECUTE:
      return {
        ...state,
        isExecute: payload,
      };
    case SET_CATEGORIES_FIELD:
      return {
        ...state,
        isCategoriesField: payload,
      };
    case SET_POP_UP:
      return {
        ...state,
        popUp: payload,
      };
    case SET_IS_ALREADY_EXIST:
      return {
        ...state,
        isAlreadyExist: payload,
      };
    default:
      return state;
  }
};
