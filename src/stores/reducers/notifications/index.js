// nofitInformation Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DATA,
  SET_LOADING_EXCUTE,
  SET_DETAIL,
  SET_NOTIF_ID,
  SET_SUCCESS,
  SET_PROMOTION_TITLES,
} from "stores/actions/notifications";

const initialState = {
  isLoading: false,
  isLoadingExcute: false,
  error: {},
  data: {},
  detail: null,
  notificationId: null,
  isSuccess: false,
  promotionTitles: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_NOTIF_ID:
      return {
        ...state,
        notificationId: payload,
      };
    case SET_SUCCESS:
      return {
        ...state,
        isSuccess: payload,
      };
    case SET_PROMOTION_TITLES:
      return {
        ...state,
        promotionTitles: payload,
      };
    default:
      return state;
  }
};
