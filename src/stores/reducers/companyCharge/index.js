// companyCharge Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DETAIL,
  SET_DATA_FORM,
  SET_SUCCESS_CONFIRMATION,
  SET_HANDLE_CLEAR_ERROR,
  SET_LIST_CURRENCY,
  SET_LIST_TIERING,
  SET_SAVE_SPECIAL_CHARGE,
} from "stores/actions/companyCharge";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  data: {},
  detail: {},
  formDataPayload: {},
  isOpen: false,
  listCurrency: [],
  listTiering: [],
  error: { isError: false, message: "" },
  dataSpecial: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DATA_FORM:
      return {
        ...state,
        formDataPayload: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };
    case SET_LIST_CURRENCY:
      return {
        ...state,
        listCurrency: payload,
      };
    case SET_LIST_TIERING:
      return {
        ...state,
        listTiering: payload,
      };
    case SET_SUCCESS_CONFIRMATION:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    case SET_SAVE_SPECIAL_CHARGE:
      return {
        ...state,
        dataSpecial: payload,
      };
    default:
      return state;
  }
};
