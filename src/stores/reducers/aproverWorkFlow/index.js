// aproverWorkFlow Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_FILTER,
  SET_DATA_DETAIL,
  SET_CONFIRMATION,
  SET_DATA_REFF_NUMBER,
  SET_DATA_EMPATY_FILTER,
  SET_SELECTED_APPROVE,
  GET_IS_DETAIL_APPROVAL,
  SET_PAGE,
  SET_SIDEBAR_MENU,
  SET_ACTIVATED_MENU,
  SET_INDIKASI_REJECT_OR_APPROVE,
  CLEAN_FILTER,
  SET_DATA_BULK_REFF_NUMBER,
  ALL_SERVICES_MATRIX,
} from "stores/actions/aproverWorkFlow";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: {},
  dataFilter: {},
  dataDetail: {},
  isOpen: false,
  reffNo: "",
  approveSelected: {},
  isGetDetail: false,
  dataSidebar: [],
  reffNoBulk: [],
  activeMenuSideBar: "",
  isReject: 5,
  allServicesMatrix: null,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case CLEAN_FILTER:
      return {
        ...state,
        dataFilter: {},
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_PAGE:
      return {
        ...state,
        pageSelected: payload,
      };
    case SET_SELECTED_APPROVE:
      return {
        ...state,
        approveSelected: payload,
      };
    case GET_IS_DETAIL_APPROVAL:
      return {
        ...state,
        isGetDetail: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_DATA: {
      return {
        ...state,
        data: payload,
      };
    }
    case SET_DATA_FILTER: {
      return {
        ...state,
        dataFilter: payload,
      };
    }

    case SET_DATA_DETAIL: {
      return {
        ...state,
        dataDetail: payload,
      };
    }

    case SET_CONFIRMATION: {
      return {
        ...state,
        isOpen: payload.isSuccess,
        isToReleaser: payload.isToReleaser,
      };
    }

    case SET_DATA_REFF_NUMBER: {
      return {
        ...state,
        reffNo: payload,
      };
    }
    case SET_DATA_BULK_REFF_NUMBER: {
      return {
        ...state,
        reffNoBulk: payload,
      };
    }
    case SET_DATA_EMPATY_FILTER:
      return {
        ...state,
        dataDetail: payload,
      };

    case SET_SIDEBAR_MENU:
      return {
        ...state,
        dataSidebar: payload,
      };

    case SET_ACTIVATED_MENU:
      return {
        ...state,
        activeMenuSideBar: payload,
      };

    case SET_INDIKASI_REJECT_OR_APPROVE:
      return {
        ...state,
        isReject: payload,
      };

    case ALL_SERVICES_MATRIX:
      return {
        ...state,
        allServicesMatrix: payload,
      };
    default:
      return state;
  }
};
