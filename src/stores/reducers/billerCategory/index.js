// billerCategory Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_SUCCESS,
  SET_CATEGORIES,
} from "stores/actions/billerCategory";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: [],
  categories: [],
  isSuccess: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...initialState,
        data: payload,
      };
    case SET_SUCCESS:
      return {
        ...initialState,
        isSuccess: payload,
      };
    case SET_CATEGORIES:
      return {
        ...initialState,
        categories: payload,
      };
    default:
      return state;
  }
};
