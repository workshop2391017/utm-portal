// menuKhusus Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_DETAIL,
  SET_DATA_DETAIL_EDIT,
  SET_DATA_GLOBAL,
  SET_POP_UP,
} from "stores/actions/menuKhusus";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: [],
  isOpen: false,
  dataDetail: {},
  dataDetailEdit: [],
  dataMenuGlobal: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };
    case SET_DATA_DETAIL_EDIT:
      return {
        ...state,
        dataDetailEdit: payload,
      };
    case SET_DATA_GLOBAL:
      return {
        ...state,
        dataMenuGlobal: payload,
      };
    case SET_POP_UP:
      return {
        ...state,
        isOpen: payload,
      };
    default:
      return state;
  }
};
