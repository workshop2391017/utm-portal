// chargeList Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SUCCESS_CONFIRMATION,
  SET_DATA_EDIT,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/chargeList";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  data: null,
  error: { isError: false, message: "" },
  dataEdit: [],
  isOpen: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DATA_EDIT:
      return {
        ...state,
        dataEdit: payload,
      };
    case SUCCESS_CONFIRMATION:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    default:
      return state;
  }
};
