// progressBarAction Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_SUCCESS_SAVE,
  SET_CONTENT,
} from "stores/actions/contentManagement";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isSuccessSave: false,
  error: {},
  data: [],
  content: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_SUCCESS_SAVE:
      return {
        ...state,
        isSuccessSave: payload,
      };

    case SET_CONTENT:
      return {
        ...state,
        content: payload,
      };
    default:
      return state;
  }
};
