// menuPackage Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA_DETAIL,
  SET_RESULT_FILTER,
  SET_DATA_MASTER,
  SET_POP_UP,
  SET_ERROR_VALIDASI,
} from "stores/actions/menuPackage";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: {},
  dataDetail: [],
  isOpen: false,
  dataResultFilter: [],
  dataMaster: [],
  isErrorValidasi: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };

    case SET_RESULT_FILTER:
      return {
        ...state,
        dataResultFilter: payload,
      };

    case SET_DATA_MASTER:
      return {
        ...state,
        dataMaster: payload,
      };

    case SET_POP_UP:
      return {
        ...state,
        isOpen: payload,
      };

    case SET_ERROR_VALIDASI:
      return {
        ...state,
        isErrorValidasi: payload,
      };

    default:
      return state;
  }
};
