// consolidationReport Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_CORPORATE_ID,
  SET_CORPORATE_DETAIL,
  SET_NONFINANCIAL_DATA,
  SET_FINANCIAL_DATA,
  SET_COMPANY_LOGIN,
  SET_LIMIT_USAGE,
  SET_FINANCIAL_DETAIL,
  SET_DETAIL_LOADING,
  SET_SOF_LOADING,
  SET_SOF_DATA,
  SET_AUTHORIZATION,
  SET_LOADING_WORKFLOWGROUP,
  SET_WORKFLOW_GROUP,
  SET_COMPANY_LOGIN_ID,
  SET_COMPANY_LOGIN_DETAIL,
  SET_TRANSACTION_GROUP,
  SET_FULL_NAME,
} from "stores/actions/consolidationReport";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  corporateId: null,
  nonFinancial: null,
  detail: null,
  financial: null,
  companyLogin: null,
  limitUsage: null,
  financialDetail: [],
  isDetailLoading: false,
  softData: [],
  isLoadingSof: false,
  authorization: null,
  workflowGroup: [],
  isWorkflowGroupLoading: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_WORKFLOW_GROUP:
      return {
        ...state,
        workflowGroup: payload,
      };
    case SET_LOADING_WORKFLOWGROUP:
      return {
        ...state,
        isWorkflowGroupLoading: payload,
      };
    case SET_TRANSACTION_GROUP:
      return {
        ...state,
        transactionGroup: payload,
      };
    case SET_AUTHORIZATION:
      return {
        ...state,
        authorization: payload,
      };
    case SET_SOF_LOADING:
      return {
        ...state,
        isLoadingSof: payload,
      };
    case SET_SOF_DATA:
      return {
        ...state,
        softData: payload,
      };
    case SET_DETAIL_LOADING:
      return {
        ...state,
        isDetailLoading: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_CORPORATE_ID:
      return {
        ...state,
        corporateId: payload,
      };

    case SET_CORPORATE_DETAIL:
      return {
        ...state,
        detail: payload,
      };
    case SET_NONFINANCIAL_DATA:
      return {
        ...state,
        nonFinancial: payload,
      };

    case SET_FINANCIAL_DATA:
      return {
        ...state,
        financial: payload,
      };

    case SET_COMPANY_LOGIN:
      return {
        ...state,
        companyLogin: payload,
      };
    case SET_COMPANY_LOGIN_ID:
      return {
        ...state,
        companyLoginId: payload,
      };
    case SET_COMPANY_LOGIN_DETAIL:
      return {
        ...state,
        companyLoginDetail: payload,
      };

    case SET_LIMIT_USAGE:
      return {
        ...state,
        limitUsage: payload,
      };

    case SET_FINANCIAL_DETAIL:
      return {
        ...state,
        financialDetail: payload,
      };
    case SET_FULL_NAME:
      return {
        ...state,
        fullName: payload,
      };
    default:
      return state;
  }
};
