import {
  SET_BANK_DOMESTIC_TYPE,
  SET_BANK_DOMESTIC_DETAIL_TYPE,
  SET_LOADING,
  SET_ERROR,
  CLEAR_ERROR,
  SET_DATA_DETAIL,
  SET_SUCCESS,
  SET_LOADING_EXCUTE,
  SET_VALIDATE_CREATE,
} from "stores/actions/domesticBank";

const initialState = {
  data: [],
  dataDetail: null,
  error: {},
  isLoading: false,
  isOpen: false,
  isLoadingExcute: false,
  validate: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_BANK_DOMESTIC_TYPE:
      return {
        ...state,
        data: payload,
      };
    case SET_DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };

    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };

    case SET_SUCCESS:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_VALIDATE_CREATE:
      return {
        ...state,
        validate: payload,
      };

    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };

    default:
      return state;
  }
};
