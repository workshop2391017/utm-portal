// internationalBank Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  CURRENCY_LIST_DATA,
  DETAIL_DATA,
  SET_IS_OPEN,
  SET_VALIDATE_CREATE,
} from "stores/actions/internationalBank";

const initialState = {
  data: null,
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  dataCurrencyList: [],
  error: {},
  dataDetail: null,
  isOpen: false,
  validate: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };

    case CURRENCY_LIST_DATA:
      return {
        ...state,
        dataCurrencyList: payload,
      };

    case DETAIL_DATA:
      return {
        ...state,
        dataDetail: payload,
      };

    case SET_IS_OPEN:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_VALIDATE_CREATE:
      return {
        ...state,
        validate: payload,
      };
    default:
      return state;
  }
};
