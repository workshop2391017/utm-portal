// viaInstitution Reducers
// --------------------------------------------------------

import { SET_DATA } from "stores/actions/workshop";

const initialState = {
  data: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
