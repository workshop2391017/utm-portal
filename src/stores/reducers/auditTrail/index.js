// auditTrail Reducers
// --------------------------------------------------------

import { searchDateStart, searchDateEnd } from "utils/helpers";

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_ACTIVITY_NAME,
  SET_PAYLOAD_GET_DATA,
  SET_DATA_TABLE_AUDIT,
  SET_DETAIL_ACTIVITY,
  SET_OLD_DATA,
  SET_NEW_DATA,
  SET_TYPE_DETAIL,
  SET_DATA_SEARCH,
  SET_CLEAR_PAYLOAD_GET_DATA,
  SET_DETAIL_RESI_TRANSFER,
  SET_MASTER_RESI_PAYMENT,
  SET_DETAIL_RESI_PAYMENT,
  SET_PAGES_MASS_TRANSFER,
  SET_DETAIL_OPENING_SECONDARY_ACCOUNT,
  SET_SEGMENTATION_DETAIL,
  SET_MENU_PACKAGE_DETAIL,
} from "stores/actions/auditTrail";

const today = new Date();
const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  activityName: "",
  dataTableAudit: {},
  payloadGetData: {
    startDate: searchDateStart(today),
    endDate: searchDateEnd(today),
    pageNumber: 0,
    pageSize: 10,
    direction: "DESC", // ASC || DESC
    sortBy: "",
    activityStatus: "",
    searchValue: "",
  },
  detailActivity: {},
  oldData: {},
  newData: {},
  type: "",
  search: "",
  dataResiTransfer: {},
  dataMasterResiPayment: {},
  dataResiPayment: [],
  dataOpeningSecondaryAccount: [],
  pagesMassTransfer: 0,
  segmentationDetail: {},
  menuPackageDetail: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_MASTER_RESI_PAYMENT:
      return {
        ...state,
        dataMasterResiPayment: payload,
      };
    case SET_DETAIL_RESI_PAYMENT:
      return {
        ...state,
        dataResiPayment: payload,
      };
    case SET_DETAIL_RESI_TRANSFER:
      return {
        ...state,
        dataResiTransfer: payload,
      };
    case SET_DETAIL_OPENING_SECONDARY_ACCOUNT:
      return {
        ...state,
        dataOpeningSecondaryAccount: payload,
      };
    case SET_DATA_SEARCH:
      return {
        ...state,
        search: payload,
      };
    case SET_TYPE_DETAIL:
      return {
        ...state,
        type: payload,
      };
    case SET_OLD_DATA:
      return {
        ...state,
        oldData: payload,
      };
    case SET_NEW_DATA:
      return {
        ...state,
        newData: payload,
      };
    case SET_DETAIL_ACTIVITY:
      return {
        ...state,
        detailActivity: payload,
      };
    case SET_SEGMENTATION_DETAIL:
      return {
        ...state,
        segmentationDetail: payload,
      };
    case SET_MENU_PACKAGE_DETAIL:
      return {
        ...state,
        menuPackageDetail: payload,
      };
    case SET_PAYLOAD_GET_DATA:
      return {
        ...state,
        payloadGetData: payload,
      };
    case SET_DATA_TABLE_AUDIT:
      return {
        ...state,
        dataTableAudit: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ACTIVITY_NAME:
      return {
        ...state,
        activityName: payload,
      };
    case SET_PAGES_MASS_TRANSFER:
      return {
        ...state,
        pagesMassTransfer: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case SET_CLEAR_PAYLOAD_GET_DATA:
      return {
        ...state,
        payloadGetData: initialState.payloadGetData,
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
