import {
  SET_SELECTED_DATA,
  SET_SELECTED_STEP_DATA,
  SET_DATA_CHANNEL,
  SET_DATA_MDW_LEFT,
  SET_DATA_MDW_RIGHT,
  SET_DATA_CORE,
  SET_DATA_SURROUNDING,
  SET_DATA_EDGES,
  SET_DATA_NODES,
  RESET_TRANSACTION_HISTORY,
} from "stores/actions/transactionHistory";

const initialState = {
  data: [],
  dataStepDetail: [],
  dataChannel: [],
  dataMdwLeft: [],
  dataMdwRight: [],
  dataSurrounding: [],
  dataCore: [],
  dataEdges: [],
  dataNodes: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_SELECTED_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_SELECTED_STEP_DATA:
      return {
        ...state,
        dataStepDetail: payload,
      };
    case SET_DATA_CHANNEL:
      return {
        ...state,
        dataChannel: payload,
      };
    case SET_DATA_MDW_LEFT:
      return {
        ...state,
        dataMdwLeft: payload,
      };
    case SET_DATA_MDW_RIGHT:
      return {
        ...state,
        dataMdwRight: payload,
      };
    case SET_DATA_CORE:
      return {
        ...state,
        dataCore: payload,
      };
    case SET_DATA_SURROUNDING:
      return {
        ...state,
        dataSurrounding: payload,
      };
    case SET_DATA_EDGES:
      return {
        ...state,
        dataEdges: payload,
      };
    case SET_DATA_NODES:
      return {
        ...state,
        dataNodes: payload,
      };
    case RESET_TRANSACTION_HISTORY:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
