// hhh Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_LOADING_INSERT,
  SET_SUCCESS_INSERT,
  SET_CLEAR_DATA,
  SET_LOADING_MENU_GLOBAL,
  SET_MENU_GLOBAL,
  SET_SELECT_GROUP,
  SET_LOADING_DELETE,
  SET_DELETE_SUCCESS,
} from "stores/actions/pengaturanHakAkses";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isLoadingInsert: false,
  isSuccessInsert: false,
  isLoadingMenuGlobal: false,
  isLoadingDelete: false,
  isDeleteSuccess: false,
  data: [],
  menuGlobal: [],
  error: { isError: false, message: "" },
  selectedGroup: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: "" },
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_LOADING_INSERT:
      return {
        ...state,
        isLoadingInsert: payload,
      };
    case SET_SUCCESS_INSERT:
      return {
        ...state,
        isSuccessInsert: payload,
      };
    case SET_CLEAR_DATA:
      return {
        ...state,
        isSuccessInsert: false,
        isLoadingInsert: false,
        isLoadingDelete: false,
        isDeleteSuccess: false,
      };
    case SET_LOADING_MENU_GLOBAL:
      return {
        ...state,
        isLoadingMenuGlobal: payload,
      };
    case SET_MENU_GLOBAL:
      return {
        ...state,
        menuGlobal: payload,
      };
    case SET_SELECT_GROUP:
      return {
        ...state,
        selectedGroup: payload,
      };

    case SET_DELETE_SUCCESS:
      return {
        ...state,
        isDeleteSuccess: payload,
      };

    case SET_LOADING_DELETE: {
      return {
        ...state,
        isLoadingDelete: payload,
      };
    }
    default:
      return state;
  }
};
