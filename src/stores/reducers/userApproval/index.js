// user approval reducer
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_ALL_APPROVAL_DATA,
  SET_APPROVAL_DETAIL,
  SET_USERS_ID,
  SET_LOADING_APPROVE,
  SET_ERROR,
  SET_ERROR_APPROVE,
  SET_CLEAR_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_HANDLE_ERROR,
} from "stores/actions/userApproval";

const initialState = {
  isLoading: false,
  isLoadingSubmit: false,
  error: { isError: false, message: "" },
  errorApprove: { isError: false, message: "" },
  data: [],
  approvalDetail: [],
  usersId: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_LOADING_APPROVE:
      return {
        ...state,
        isLoadingSubmit: payload,
      };

    case SET_ALL_APPROVAL_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_APPROVAL_DETAIL:
      return {
        ...state,
        approvalDetail: payload.map((elm) => ({
          ...elm,
          newData: JSON.parse(elm?.newData),
          oldData: JSON.parse(elm?.oldData),
        })),
      };

    case SET_USERS_ID:
      return {
        ...state,
        usersId: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case SET_ERROR_APPROVE:
      return {
        ...state,
        errorApprove: payload,
      };
    case SET_CLEAR_ERROR:
      return {
        ...state,
        errorApprove: { isError: false, message: "" },
        error: { isError: false, message: "" },
      };

    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
