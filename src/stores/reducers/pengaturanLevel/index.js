// pengaturanLevel Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_DELETE_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  DATA_SIDEBAR,
  DATA_EDIT,
  DATA_CURRENCY,
  OPEN_SUCCESS,
  SET_PAGES,
  SET_IDCOMPANY,
  SET_IS_VALID_NAME,
} from "stores/actions/pengaturanLevel";

const initialState = {
  isLoading: false,
  isDeleteLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  dataSideBar: [],
  dataEdit: null,
  dataCurrency: [],
  isOpen: false,
  pages: 0,
  dataCompanyId: null,
  isValidName: true,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_IS_VALID_NAME:
      return {
        ...state,
        isValidName: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DELETE_LOADING:
      return {
        ...state,
        isDeleteLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case DATA_SIDEBAR:
      return {
        ...state,
        dataSideBar: payload,
      };

    case DATA_EDIT:
      return {
        ...state,
        dataEdit: payload,
      };

    case DATA_CURRENCY:
      return {
        ...state,
        dataCurrency: payload,
      };

    case OPEN_SUCCESS:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    case SET_IDCOMPANY:
      return {
        ...state,
        dataCompanyId: payload,
      };
    default:
      return state;
  }
};
