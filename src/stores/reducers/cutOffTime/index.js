// cutOffTime Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  SET_SUCCESS,
  SET_DATA,
  SET_CURRENCIES,
} from "stores/actions/cutOffTime";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: {},
  isSuccess: false,
  currencies: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case SET_SUCCESS:
      return {
        ...initialState,
        isSuccess: payload,
      };
    case SET_CURRENCIES:
      return {
        ...initialState,
        currencies: payload,
      };
    default:
      return state;
  }
};
