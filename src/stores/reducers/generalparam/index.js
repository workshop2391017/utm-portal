//  Reducers Dashboard

import {
  SET_GENERAL_PARAMS,
  SET_LOADING,
  SET_GENERALPARAMS_FORM,
  SET_LOADING_SUBMIT,
  SET_EDIT_GENERALPARAMS,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/generalparam";

const initialState = {
  isLoading: false,
  data: [],
  error: { isError: false, message: "" },
  isLoadingSubmit: false,
  generalParamsForm: {},
  editGeneralData: {}, // detail for edit
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_GENERAL_PARAMS:
      return {
        ...state,
        data: payload,
      };
    case SET_GENERALPARAMS_FORM:
      return {
        ...state,
        generalParamsForm: payload,
      };

    case SET_EDIT_GENERALPARAMS:
      return {
        ...state,
        editGeneralData: payload,
      };

    case SET_LOADING_SUBMIT:
      return {
        ...state,
        isLoadingSubmit: payload,
      };

    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
