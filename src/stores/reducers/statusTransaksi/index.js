// statusTransaksi Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA_TRANSAKSI,
  SET_DATA_BULK_TRANSAKSI,
  SET_DATA_TRANSAKSI_DETAIL,
  SET_DATA_DOWNLOAD_TRANSAKSI,
  SET_PAGES,
  SET_ALL_TRANSACTION_STATUS,
  SET_ALL_TRANSACTION_CATEGORY,
  SET_ALL_TRANSACTION_GROUP,
  SET_USER_PROFILE_ID,
  SET_TRANSACTION_GROUP,
} from "stores/actions/statusTransaksi";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  dataTransaksi: {},
  dataBulkTransaksi: {},
  dataTransaksiCategory: null,
  dataTransaksiStatus: null,
  dataTransaksiGroup: null,
  dataTransaksiDetail: {},
  resi: {},
  pages: 0,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA_TRANSAKSI:
      return {
        ...state,
        dataTransaksi: payload,
      };
    case SET_DATA_BULK_TRANSAKSI:
      return {
        ...state,
        dataBulkTransaksi: payload,
      };
    case SET_DATA_TRANSAKSI_DETAIL:
      return {
        ...state,
        dataTransaksiDetail: payload,
      };
    case SET_ALL_TRANSACTION_STATUS:
      return {
        ...state,
        dataTransaksiStatus: payload,
      };
    case SET_ALL_TRANSACTION_CATEGORY:
      return {
        ...state,
        dataTransaksiCategory: payload,
      };
    case SET_ALL_TRANSACTION_GROUP:
      return {
        ...state,
        dataTransaksiGroup: payload,
      };
    case SET_DATA_DOWNLOAD_TRANSAKSI:
      return {
        ...state,
        resi: payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    case SET_USER_PROFILE_ID:
      return {
        ...state,
        userProfileId: payload,
      };
    case SET_TRANSACTION_GROUP:
      return {
        ...state,
        transactionGroup: payload,
      };
    default:
      return state;
  }
};
