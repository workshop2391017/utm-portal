// progressBarAction Reducers
// --------------------------------------------------------

import {
  INIT_DATA,
  SET_LOADING,
  SET_LOADING_SYNC,
} from "stores/actions/merchant";

const initialState = {
  isLoading: false,
  data: [],
  isLoadingSync: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING_SYNC:
      return {
        ...state,
        isLoadingSync: payload,
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    default:
      return state;
  }
};
