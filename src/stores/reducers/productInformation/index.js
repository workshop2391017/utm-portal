// productInformation Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DETAIL,
  SET_PRODUCTS,
  SET_PRODUCT_CODES,
  SET_PRODUCT_NAME_ACCOUNT_TYPE,
  SET_CURRENCIES,
  SET_SUCCESS,
  SET_ADD_SUCCESS,
  SET_ADD_LOADING,
  SET_IS_EDIT,
} from "stores/actions/productInformation";

const initialState = {
  isLoading: false,
  isAddLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: [],
  detail: {},
  products: [],
  productCodes: [],
  productNameAccountType: {},
  currencies: [],
  isSuccess: false,
  isAddSuccess: false,
  isEdit: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_ADD_LOADING:
      return {
        ...state,
        isAddLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };
    case SET_PRODUCTS:
      return {
        ...state,
        products: payload,
      };
    case SET_PRODUCT_CODES:
      return {
        ...state,
        productCodes: payload,
      };
    case SET_PRODUCT_NAME_ACCOUNT_TYPE:
      return {
        ...state,
        productNameAccountType: payload,
      };
    case SET_CURRENCIES:
      return {
        ...state,
        currencies: payload,
      };
    case SET_SUCCESS:
      return {
        ...state,
        isSuccess: payload,
      };
    case SET_ADD_SUCCESS:
      return {
        ...state,
        isAddSuccess: payload,
      };
    case SET_IS_EDIT:
      return {
        ...state,
        isEdit: payload,
      };
    default:
      return state;
  }
};
