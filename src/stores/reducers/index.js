import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import login from "./login";
import {
  transactionHistoryPage,
  activityDetail,
  transactionChart,
  compireTransaction,
} from "./dashboard";
import generalParam from "./generalparam";
import managementuser from "./managementuser";
import managementHardToken from "./managementHardToken";
import userApproval from "./userApproval";
import approvalConfig from "./approvalConfig";
import managementRole from "./managementRole";
import customerSegmentation from "./segmentasiNasabah";
import globalLimit from "./globalLimit";
import domesticBank from "./domesticBank";
import hostErrorMapping from "./hostErrorMapping";
import packageParameterSetting from "./packageParameterSetting";
import {
  pendingTaskRegistration,
  pendingTaskSoftToken,
  pendingTaskChangeAutorization,
  pendingTaskAutoCollection,
  pendingtaskVa,
} from "./pendingTask";
import { vaReport, autoCollectionReport } from "./onlineSubmissonReport";
import errorGeneral from "./errorGeneral";
import { branchPengaturan } from "./branchPengaturan";
import handlingOfficer from "./handlingOfficer";
import authorization from "./authorization";
import responseSetting from "./responseSetting";
import globalLimitTrx from "./globalLimitTrx";
import internationalBank from "./internationalBank";
import limitPackage from "./limitPackage";
import tieringSlab from "./tieringSlab";
import productRekening from "./productRekening";
import menuGlobal from "./menuGlobal";
import jenisRekeningProduk from "./jenisRekeningProduk";
import aproverWorkFlow from "./aproverWorkFlow";
import segementCabang from "./segementCabang";
import menuPackage from "./menuPackage";
import serviceChargeList from "./serviceChargeList";
import companyLimit from "./companyLimit";
import uploadProgress from "./uploadProgress";
import menuKhusus from "./menuKhusus";
import chargeList from "./chargeList";
import productInformation from "./productInformation";
import companyCharge from "./companyCharge";
import pengaturanHakAkses from "./pengaturanHakAkses";
import matrixApproval from "./matrixApproval";
import roleUser from "./roleUser";
import approvalMatrixFinantial from "./approvalMatrixFinantial";
import pengaturanKelompok from "./pengaturanKelompok";
import privilrgeApproval from "./privilegeApproval";
import approvalPersetujuanMatrix from "./approvalPersetujuanMatrix";
import approvelMatrixNonFinantial from "./approvelMatrixNonFinantial";
import viaInstitution from "./viaInstitution";
import chargePackage from "./chargePackage";
import menus from "./menus";
import pengaturanLevel from "./pengaturanLevel";
import notifInformation from "./notifInformation";

import validatedatalms from "./validatedatalms";
import auditTrail from "./auditTrail";
import billerCategory from "./billerCategory";
import promo from "./promo";
import checkBeforeUpdate from "./checkBeforeUpdate";
import consolidationReport from "./consolidationReport";
import bankUserSetting from "./bankUserSetting";
import statusTransaksi from "./statusTransaksi";
import statusNonTransaksi from "./statusNonTransaksi";
import periodicalChargeList from "./periodicalChargeList";

import contentManagement from "./contentManagement";
import validateTaskPortal from "./validateTaskPortal";
import pageContainer from "./pageContainer";
import merchant from "./merchant";
import calendarEvent from "./calendarEvent";
import notifications from "./notifications";
import cutOffTime from "./cutOffTime";
import billerManagers from "./billerManagers";
import faq from "./faq";
import workshop from "./workshop";
import transactionHistory from "./transactionHistory";

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    login,
    transactionHistoryPage,
    activityDetail,
    generalParam,
    managementuser,
    managementHardToken,
    userApproval,
    approvalConfig,
    managementRole,
    errorGeneral,
    transactionChart,
    compireTransaction,
    customerSegmentation,
    branchPengaturan,
    globalLimit,
    domesticBank,
    pendingTaskRegistration,
    pendingTaskSoftToken,
    pendingTaskChangeAutorization,
    pendingTaskAutoCollection,
    hostErrorMapping,
    handlingOfficer,
    authorization,
    uploadProgress,
    globalLimitTrx,
    internationalBank,
    limitPackage,
    tieringSlab,
    productRekening,
    menuGlobal,
    jenisRekeningProduk,
    aproverWorkFlow,
    segementCabang,
    menuPackage,
    companyCharge,
    companyLimit,
    pengaturanHakAkses,
    responseSetting,
    matrixApproval,
    approvalPersetujuanMatrix,
    packageParameterSetting,
    menuKhusus,
    chargeList,
    serviceChargeList,
    roleUser,
    approvalMatrixFinantial,
    privilrgeApproval,
    menus,
    productInformation,
    viaInstitution,
    chargePackage,
    pengaturanKelompok,
    pendingtaskVa,
    vaReport,
    autoCollectionReport,
    pengaturanLevel,
    approvelMatrixNonFinantial,
    validatedatalms,
    promo,
    // segmenTypeSelect,
    periodicalChargeList,
    statusTransaksi,
    statusNonTransaksi,
    auditTrail,
    checkBeforeUpdate,
    consolidationReport,
    bankUserSetting,
    contentManagement,
    validateTaskPortal,
    notifInformation,
    pageContainer,
    merchant,
    calendarEvent,
    notifications,
    cutOffTime,
    billerCategory,
    billerManagers,
    faq,
    workshop,
    transactionHistory,
  });
