// uploadProgress Reducers
// --------------------------------------------------------

import {
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_UPLOAD_PROGRESS,
} from "stores/actions/uploadProgress";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  errorUpload: {},
  uploadProgress: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_UPLOAD_PROGRESS:
      return {
        ...state,
        uploadProgress: {
          ...state.uploadProgress,
          [payload.fileName]: payload.value,
        },
      };
    case SET_ERROR:
      return {
        ...state,
        errorUpload: {
          ...state.errorUpload,
          [payload.fileName]: payload.value,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        errorUpload: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
