import {
  SET_LOADING,
  SET_INITIALDATA,
  SET_ERROR,
  SET_SELECTED_COMPANY,
  SET_DETAIL,
} from "stores/actions/onlineSubmissonReport/vaReport";

const initialState = {
  error: { isError: false, message: "" },
  data: [],
  detail: {},
  isLoading: false,
  detailDataVA: {
    companyInformation: [],
    ownerInformation: [],
    parameterSetting: [],
  },
  selectedCompany: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_INITIALDATA:
      return {
        ...state,
        data: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case SET_SELECTED_COMPANY:
      return {
        ...state,
        selectedCompany: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };
    default:
      return state;
  }
};
