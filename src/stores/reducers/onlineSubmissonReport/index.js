import vaReport from "./vaReport";
import autoCollectionReport from "./autoCollectionReport";

export { vaReport, autoCollectionReport };
