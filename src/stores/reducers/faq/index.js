// listPromo Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_DATA,
  SET_ERROR,
  SET_ADMIN_BANK_DATA,
  SET_IS_EDIT,
  SET_DETAIL_DATA,
  SET_ID,
  SET_PAGES,
} from "stores/actions/faq";

const initialState = {
  error: {},
  data: [],
  adminBankFaq: [],
  isLoading: false,
  isEdit: false,
  detailData: {},
  id: null,
  pages: 0,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_ADMIN_BANK_DATA:
      return {
        ...state,
        adminBankFaq: payload,
      };
    case SET_IS_EDIT:
      return {
        ...state,
        isEdit: payload,
      };
    case SET_DETAIL_DATA:
      return {
        ...state,
        detailData: payload,
      };
    case SET_ID:
      return {
        ...state,
        id: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    default:
      return state;
  }
};
