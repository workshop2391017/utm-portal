// nofitInformation Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DATA,
  SET_LOADING_EXCUTE,
  SET_DETAIL,
  SET_ID,
  SET_SUCCESS,
  SET_OPTION_LIST,
  SET_BILLER_DENOM_LIST,
  SET_DENOM_FORM_MODE,
  SET_BILLER_TEMPLATE_LIST,
  SET_TEMPLATE_FORM_MODE,
  CLEAR_FORM,
  SET_CATEGORIES,
  SET_TRANSACTION_CATEGORIES,
  SET_SPECS,
  SET_TRANSACTION_GROUPS,
  SET_PREFIXES,
  SET_DENOM_LIST,
  SET_TEMPLATE_LIST,
  SET_DATA_LIST,
  SET_IS_LIST_FIELD,
} from "stores/actions/billerManager";

const initialState = {
  prefixes: [],
  denomList: [],
  templateList: [],
  categories: [],
  transactionCategories: [],
  specs: [],
  transactionGroups: [],
  isLoading: false,
  isLoadingExcute: false,
  error: {},
  data: null,
  detail: null,
  id: null,
  isSuccess: false,
  optionsList: null,
  isListField: false,
  dataForm: {
    dataList: {
      billerDenomList: [],
      billerPrefixList: [],
      listFieldInput: [],
      listFieldConfirmation: [],
      listFieldReceipt: [],
      listFieldPartial: [],
    },
    denomFormMode: {
      mode: "add",
      editId: null,
    },
    templateFormMode: {
      mode: "add",
      editId: null,
    },
  },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_DATA_LIST:
      return {
        ...state,
        dataForm: {
          ...state.dataForm,
          dataList: {
            ...state.dataForm.dataList,
            ...payload,
          },
        },
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_TEMPLATE_LIST:
      return {
        ...state,
        templateList: payload,
      };
    case SET_DENOM_LIST:
      return {
        ...state,
        denomList: payload,
      };
    case SET_CATEGORIES:
      return {
        ...state,
        categories: payload,
      };
    case SET_TRANSACTION_CATEGORIES:
      return {
        ...state,
        transactionCategories: payload,
      };
    case SET_SPECS:
      return {
        ...state,
        specs: payload,
      };
    case SET_TRANSACTION_GROUPS:
      return {
        ...state,
        transactionGroups: payload,
      };
    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_ID:
      return {
        ...state,
        id: payload,
      };
    case SET_SUCCESS:
      return {
        ...state,
        isSuccess: payload,
      };
    case SET_OPTION_LIST:
      return {
        ...state,
        optionsList: payload,
      };
    case SET_PREFIXES:
      return {
        ...state,
        prefixes: payload,
      };
    case SET_IS_LIST_FIELD:
      return {
        ...state,
        isListField: payload,
      };
    case SET_BILLER_DENOM_LIST:
      return {
        ...state,
        dataForm: {
          ...state.dataForm,
          dataList: {
            ...state.dataForm.dataList,
            billerDenomList: payload,
          },
        },
      };
    case SET_DENOM_FORM_MODE:
      return {
        ...state,
        dataForm: {
          ...state.dataForm,
          denomFormMode: payload,
        },
      };
    case SET_BILLER_TEMPLATE_LIST:
      return {
        ...state,
        dataForm: {
          ...state.dataForm,
          dataList: {
            ...state.dataForm.dataList,
            [payload.dataRef]: payload.data,
          },
        },
      };
    case SET_TEMPLATE_FORM_MODE:
      return {
        ...state,
        dataForm: {
          ...state.dataForm,
          templateFormMode: payload,
        },
      };
    case CLEAR_FORM:
      return {
        ...state,
        dataForm: {
          dataList: {
            billerDenomList: [],
            billerPrefixList: [],
            listFieldInput: [],
            listFieldConfirmation: [],
            listFieldReceipt: [],
            listFieldPartial: [],
          },
          denomFormMode: {
            mode: "add",
            editId: null,
          },
          templateFormMode: {
            mode: "add",
            editId: null,
          },
        },
      };
    default:
      return state;
  }
};
