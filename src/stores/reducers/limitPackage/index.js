// errorGeneral Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_DATA,
  SET_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_HANDLE_ERROR,
  SET_DETAIL,
  SET_ID,
  SET_LOADING_EXECUTE,
  SET_SUCCESS_EXECUTE,
  SET_CLEAR,
  SET_CURR_MATRIX,
  SET_ADD_LIMIT_PACKAGE,
  SET_LOADING_CURRENCY,
  SET_CURRENCY,
} from "stores/actions/limitPackage";

const initialState = {
  isLoading: false,
  isLoadingExecute: false,
  isSuccessExecute: false,
  isLoadingCurrency: false,
  currency: [],
  error: { isError: false, message: "" },
  data: {},
  detail: {},
  id: null,
  currencyMatrix: [],
  addLimitPackage: { limitPackage: null, globalLimit: [] },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_ID:
      return {
        ...state,
        id: payload,
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };

    case SET_SUCCESS_EXECUTE:
      return {
        ...state,
        isSuccessExecute: payload,
      };

    case SET_CURR_MATRIX:
      return {
        ...state,
        currencyMatrix: payload,
      };

    case SET_ADD_LIMIT_PACKAGE:
      return {
        ...state,
        addLimitPackage: payload,
      };

    case SET_CLEAR:
      return {
        ...state,
        isSuccessExecute: false,
        isLoadingExecute: false,
        isLoading: false,
        error: { isError: false, message: null },
      };

    case SET_LOADING_CURRENCY:
      return {
        ...state,
        isLoadingCurrency: payload,
      };

    case SET_CURRENCY:
      return {
        ...state,
        currency: payload,
      };

    default:
      return state;
  }
};
