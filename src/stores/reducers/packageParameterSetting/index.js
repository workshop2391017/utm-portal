import {
  SET_DATA,
  SET_LOADING,
  SET_ERROR,
  CLEAR_ERROR,
  SET_DETAIL_DATA,
  SET_LOADING_DETAIL,
  SET_PARAMETER_SETTING_ID,
  SET_LOADING_EXECUTE,
  SET_EXECUTE_SUCCESS,
  SET_CLEAR_DATA,
  SET_BUSINESS_FIELD,
  SET_SEGMENTATION,
} from "stores/actions/packageParameterSetting";

const initialState = {
  isLoading: false,
  isLoadingDetail: false,
  parameterSettingId: null,
  error: { isError: false, message: "" },
  data: [],
  detailData: {},
  segmentations: null,
  businessField: null,
  isLoadingExecute: false,
  isSuccessExecute: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    case SET_DETAIL_DATA:
      return {
        ...state,
        detailData: payload,
      };
    case SET_LOADING_DETAIL:
      return {
        ...state,
        isLoadingDetail: false,
      };
    case SET_PARAMETER_SETTING_ID:
      return {
        ...state,
        parameterSettingId: payload,
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };
    case SET_EXECUTE_SUCCESS:
      return {
        ...state,
        isSuccessExecute: payload,
      };

    case SET_CLEAR_DATA:
      return {
        ...initialState,
      };
    case SET_BUSINESS_FIELD:
      return {
        ...state,
        businessField: payload,
      };
    case SET_SEGMENTATION:
      return {
        ...state,
        segmentations: payload,
      };
    default:
      return state;
  }
};
