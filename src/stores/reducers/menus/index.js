// progressBarAction Reducers
// --------------------------------------------------------

import { SET_MENU_ID, INIT_DATA } from "stores/actions/menus";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  menuId: null,
  error: {},
  // activePath: null,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_MENU_ID:
      return {
        ...state,
        menuId: payload,
      };
    case INIT_DATA:
      return {
        ...state,
      };
    // case SET_ACTIVE_PATH:
    //   return {
    //     ...state,
    //     activePath: payload,
    //   };
    default:
      return state;
  }
};
