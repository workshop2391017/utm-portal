// responseSetting Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  ADD_ONE_DATA,
} from "stores/actions/responseSetting";

const initialState = {
  editData: {},
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case ADD_ONE_DATA:
      return {
        ...initialState,
        editData: payload,
      };
    default:
      return state;
  }
};
