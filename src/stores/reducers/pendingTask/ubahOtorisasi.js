import {
  SET_PT_UBAH_OTORISASI,
  SET_PT_UO_LOADING,
  SET_PT_UO_ERROR,
  SET_PT_UO_DETAIL,
  SET_PT_UO_DETAIL_LOADING,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_DETAIL_DATA,
  SET_LOADING_EXECUTE,
  SET_EXECUTE_SUCCESS,
  SET_CLEAR,
  PT_UBAHOTORISASI,
  SET_REJECTED_LIST_OTORISASI,
} from "stores/actions/pendingTask/ubahOtorisasi";

const initialState = {
  isLoading: false,
  isLoadingDetail: false,
  isExeucteSuccess: false,
  isLoadingExecute: false,
  error: { isError: false, message: "" },
  data: {},
  detail: {},
  isExecuted: { action: null, isSuccess: false },
  isExecute: false,
  rejectedList: [],
  detailData: {
    with_admin: {
      userProfileAdminMaker: [],
      userProfileAdminApproval: [],
    },
    no_admin: {
      userProfileChecker: [],
      userProfileApproval: [],
      userProfileMaker: [],
    },
  },
  viewTemplate: {
    multipleWithAdmin: {
      before: {},
      after: {
        label: "Corporate Admin Data",
        template: [
          {
            label: "Registered Account",
            key: "registeredAccount",
            rejectPayload: null,
          },
          {
            label: "Admin Maker Profile",
            key: "userProfileAdminMaker",
            rejectPayload: "profilAdminMakerRejectedList",
          },
          {
            label: "Admin Approver Profile",
            key: "userProfileAdminApproval",
            rejectPayload: "profilAdminApproverRejectedList",
          },
          {
            label: "Power of  Attorney",
            key: "suratKuasa",
          },
        ],
      },
    },
    multipleWithoutAdmin: {
      before: {},
      after: {
        label: "Without Corporate Admin Data",
        template: [
          {
            label: "Registered Account",
            key: "registeredAccount",
            rejectPayload: null,
          },
          {
            label: "Maker Profile",
            key: "userProfileMaker",
            rejectPayload: "profilMakerRejectedList",
          },
          {
            label: "Approver Profile",
            key: "userProfileApproval",
            rejectPayload: "profilApproverRejectedList",
          },
          {
            label: "Releaser Profile",
            key: "userProfileReleaser",
            rejectPayload: "profilReleaserRejectedList",
          },
        ],
      },
    },
  },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_PT_UO_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_PT_UBAH_OTORISASI:
      return {
        ...state,
        data: payload,
      };
    case SET_PT_UO_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_PT_UO_ERROR:
      return {
        ...state,
        error: {
          ...state.error,
          isError: true,
          message: payload,
        },
      };

    case SET_PT_UO_DETAIL_LOADING:
      return {
        ...state,
        isLoadingDetail: payload,
      };

    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    case SET_DETAIL_DATA:
      return {
        ...state,
        detailData: {
          ...state.detailData,
          [payload.key]: {
            ...state.detailData[payload.key],
            ...payload.payload,
          },
        },
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };
    case SET_EXECUTE_SUCCESS:
      return {
        ...state,
        isExeucteSuccess: payload,
      };

    case SET_CLEAR:
      return {
        ...initialState,
      };

    case PT_UBAHOTORISASI:
      return {
        ...state,
        templateMapping: payload,
      };

    case SET_REJECTED_LIST_OTORISASI:
      return {
        ...state,
        rejectedList: payload,
      };

    default:
      return state;
  }
};
