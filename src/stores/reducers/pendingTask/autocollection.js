import {
  SET_LOADING,
  SET_INITIALDATA,
  SET_ERROR,
  SET_CLEAR_ERROR,
  SET_DETAIL,
  SET_LOADING_EXECUTE,
  SET_SUCCESS_EXECUTE,
  SET_USER_AUTOCOLLECTION_DATA,
  SET_SELECTED_COMPANY,
  SET_CLEAR_EXECUTE,
  SET_PT_EXECUTE,
} from "stores/actions/pendingTask/autocollection";

const initialState = {
  error: { isError: false, message: "" },
  data: [],
  detail: {},
  isSuccessExecute: { action: null, isSuccess: false },
  isLoading: false,
  isExecute: false,
  isExecuted: {},
  isLoadingExecute: false,
  detailDataAutoCollection: {
    companyInformation: [],
    ownerInformation: [],
    parameterSetting: [],
  },
  selectedCompany: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_PT_EXECUTE:
      return {
        ...state,
        isExecuted: payload,
      };
    case SET_CLEAR_EXECUTE:
      return {
        ...state,
        isExecuteLoading: false,
        isExecuted: {},
        isExecuteError: { isError: false, message: "" },
      };
    case SET_SELECTED_COMPANY:
      return {
        ...state,
        selectedCompany: payload,
      };
    case SET_USER_AUTOCOLLECTION_DATA:
      return {
        ...state,
        detailDataAutoCollection: {
          ...state.detailDataVA,
          ...payload,
        },
      };
    case SET_INITIALDATA:
      return {
        ...state,
        data: payload,
      };
    case SET_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: "" },
      };

    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };
    case SET_SUCCESS_EXECUTE:
      return {
        ...state,
        isSuccessExecute: payload,
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };
    default:
      return state;
  }
};
