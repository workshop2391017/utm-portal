import {
  SET_LOADING_EXECUTE,
  SET_LOADING_SOFTTOKEN,
  SET_PT_DATA_SOFTTOKEN,
  SET_PT_DETAIL_SOFTTOKEN,
  SET_PT_ERROR_SOFTTOKEN,
  SET_SUCCESS_EXECUTE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_DATA_REFF,
} from "stores/actions/pendingTask/softToken";

const initialState = {
  isLoading: false,
  error: { isError: false, message: "" },
  data: {},
  detail: {},
  isExecuted: { action: null, isSuccess: false, isToReleaser: true },
  isExecute: false,
  dataReffNo: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING_SOFTTOKEN:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_PT_DATA_SOFTTOKEN:
      return {
        ...state,
        data: payload,
      };
    case SET_PT_DETAIL_SOFTTOKEN:
      return {
        ...state,
        detail: payload,
      };
    case SET_DATA_REFF:
      return {
        ...state,
        dataReffNo: payload,
      };

    case SET_PT_ERROR_SOFTTOKEN:
      return {
        ...state,
        error: {
          ...state.error,
          isError: true,
          message: payload,
        },
      };

    case SET_SUCCESS_EXECUTE:
      return {
        ...state,
        isExecuted: payload,
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isExecute: payload,
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
