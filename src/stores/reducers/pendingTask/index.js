import pendingTaskRegistration from "./registration";
import pendingTaskSoftToken from "./softToken";
import pendingTaskChangeAutorization from "./ubahOtorisasi";
import pendingtaskVa from "./va";
import pendingTaskAutoCollection from "./autocollection";

export {
  pendingTaskRegistration,
  pendingTaskSoftToken,
  pendingTaskChangeAutorization,
  pendingtaskVa,
  pendingTaskAutoCollection,
};
