import {
  SET_LOADING_PT_REGISTRATION,
  SET_PT_DATA,
  SET_PT_DETAIL,
  SET_PT_ERROR,
  SET_PT_EXECUTE,
  SET_PT_EXECUTE_ERROR,
  SET_PT_EXECUTE_LOADING,
  SET_REF_NUMBER,
  SET_CLEAR_EXECUTE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_DATA_PROFILE_BIDANG,
  SET_DATA_PEMILIK_USAHA,
  SET_IMAGE_CHECKED,
  SET_OPEN_PREVIEW_IMAGE,
  SET_AFTER_LOGIN_USER_DATA,
  SET_BEFORE_LOGIN_USER_DATA,
  PENDINGTASK_REGIS_TEMPLATE,
  SET_PT_BRANCH,
  SET_PT_SEGMENTATION,
  SET_V_MENU_PACKAGE,
  SET_V_LIMIT_PACAGE,
  SET_REJECTED_LIST,
  SET_LOADING_PT_REGISTRATION_DOWNLOAD,
} from "stores/actions/pendingTask/registration";

const initialState = {
  isLoading: false,
  isLoadingDownload: false,
  error: { isError: false, message: "" },
  data: {},
  detail: {},
  menuPackage: [],
  limitPackage: { data: [] },
  idAndFase: {
    referenceNumber: null,
    fase: null,
  },
  isExecuteLoading: false,
  isExecuteError: { isError: false, message: "" },
  isExecuted: {},
  branch: {},
  segmentation: {},
  rejectedList: [],
  detailDataBeforeLogin: {
    dataProfileBidang: [],
    dataPemilikUsaha: [],
    imageChecked: [],
    dataDocument: {},
  },
  openPreviweImage: {
    isOpen: false,
    image: null,
  },
  registrastionCase: null,
  detailDataAfterLogin: {
    with_admin: {
      userProfileAdminMaker: [],
      userProfileAdminApproval: [],
      userProfileMaker: [],
      userProfile: [],
      corporateProfile: [],
    },
    no_admin: {
      userProfileMaker: [],
      userProfileApproval: [],
      userProfileReleaser: [],
    },
  },
  viewTemplate: {
    singleOtorisasi: {
      before: {
        label: "Applicant Data Single Authorization",
        template: [
          {
            label: "Registered Account",
            key: "registeredAccount",
            rejectPayload: null,
          },
          {
            label: "Business Field Profile",
            key: "corporateProfile",
            rejectPayload: "profilBidangUsahaRejectedList",
          },
          {
            label: "User Profile",
            key: "userProfile",
            rejectPayload: "profilPemilikUsahaRejectedList",
          },
          {
            label: "Addition Of Other Account",
            key: "additionalAccounts",
            rejectPayload: "additionalAccountRejectedList",
          },
          {
            label: "Branch",
            key: "branch",
            rejectPayload: null,
          },
          {
            label: "Segmentasi",
            key: "segmentationId",
            rejectPayload: null,
          },
        ],
      },
    },
    multipleWithAdmin: {
      before: {
        label: "Applicant Data Multiple Authorization",
        template: [
          {
            label: "Registered Account",
            key: "registeredAccount",
            rejectPayload: null,
          },
          {
            label: "Business Field Profile",
            key: "corporateProfile",
            rejectPayload: "profilBidangUsahaRejectedList",
          },
          {
            label: "User Profile",
            key: "userProfile",
            rejectPayload: "profilPemilikUsahaRejectedList",
          },
          {
            label: "Addition Of Other Account",
            key: "additionalAccounts",
            rejectPayload: "additionalAccountRejectedList",
          },
          {
            label: "Branch",
            key: "branch",
            rejectPayload: null,
          },
          {
            label: "Segmentasi",
            key: "segmentationId",
            rejectPayload: null,
          },
        ],
      },
      after: {
        label: "Corporate Admin Data",
        template: [
          // {
          //   label: "Registered Account",
          //   key: "registeredAccount",
          //   rejectPayload: null,
          // },
          {
            label: "Admin Maker Profile",
            key: "userProfileAdminMaker",
            rejectPayload: "profilAdminMakerRejectedList",
          },
          {
            label: "Admin Approver Profile",
            key: "userProfileAdminApproval",
            rejectPayload: "profilAdminApproverRejectedList",
          },
          {
            label: "Power of  Attorney",
            key: "suratKuasa",
            // rejectPayload: "powerOfAttorneyAdminMakerRejectedList",
          },
        ],
      },
    },
    multipleWithoutAdmin: {
      before: {
        label: "Applicant Data Multiple Authorization",
        template: [
          {
            label: "Business Field Profile",
            key: "corporateProfile",
            rejectPayload: "profilBidangUsahaRejectedList",
          },
          {
            label: "User Profile",
            key: "userProfile",
            rejectPayload: "profilPemilikUsahaRejectedList",
          },
          {
            label: "Addition Of Other Account",
            key: "additionalAccounts",
            rejectPayload: "additionalAccountRejectedList",
          },
          {
            label: "Branch",
            key: "branch",
            rejectPayload: null,
          },
          {
            label: "Segmentasi",
            key: "segmentationId",
            rejectPayload: null,
          },
        ],
      },
      after: {
        label: "Without Corporate Admin Data",
        template: [
          {
            label: "Maker Profile",
            key: "userProfileMaker",
            rejectPayload: "profilMakerRejectedList",
          },
          {
            label: "Approver Profile",
            key: "userProfileApproval",
            rejectPayload: "profilApproverRejectedList",
          },
          {
            label: "Releaser Profile",
            key: "userProfileReleaser",
            rejectPayload: "profilReleaserRejectedList",
          },
        ],
      },
    },
  },
  templateMapping: [],
  // afterLoginKeys: {
  //   with_admin: [
  //     {
  //       label: "Admin Maker Profile",
  //       key: "userProfileAdminMaker",
  //     },
  //     {
  //       label: "Profil Admin Approver",
  //       key: "userProfileAdminApproval",
  //     },
  //     {
  //       label: "Business Profile",
  //       key: "corporateProfile",
  //     },
  //     {
  //       label: "Owner Business Profile",
  //       key: "userProfile",
  //     },
  //   ],
  //   no_admin: [
  //     { label: "Maker Profile", key: "userProfileMaker" },
  //     {
  //       label: "Approval Profile",
  //       key: "userProfileApproval",
  //     },
  //     {
  //       label: "Releaser Profile",
  //       key: "userProfileReleaser",
  //     },
  //   ],
  // },
  // beforeLoginKeys: [
  //   {
  //     label: "Business Field Profile",
  //     key: "corporateProfile",
  //     rejectPayload: "profilBidangUsahaRejectedList",
  //   },
  //   {
  //     label: "User Profile",
  //     key: "userProfile",
  //     rejectPayload: "profilPemilikUsahaRejectedList",
  //   },
  //   {
  //     label: "Addition Of Other Account",
  //     key: "additionalAccounts",
  //     rejectPayload: "profilPemilikUsahaRejectedList",
  //   },
  //   {
  //     label: "Branch",
  //     key: "branch",
  //     rejectPayload: "profilPemilikUsahaRejectedList",
  //   },
  //   {
  //     label: "Segmentasi",
  //     key: "segmentationId",
  //     rejectPayload: "profilPemilikUsahaRejectedList",
  //   },
  // ],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING_PT_REGISTRATION:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_LOADING_PT_REGISTRATION_DOWNLOAD:
      return {
        ...state,
        isLoadingDownload: payload,
      };
    case SET_PT_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_PT_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_REF_NUMBER:
      return {
        ...state,
        idAndFase: {
          referenceNumber: payload.referenceNumber,
          fase: payload.fase,
        },
      };

    case SET_PT_EXECUTE:
      return {
        ...state,
        isExecuted: payload,
      };
    case SET_PT_EXECUTE_LOADING:
      return {
        ...state,
        isExecuteLoading: payload,
      };
    case SET_PT_EXECUTE_ERROR:
      return {
        ...state,
        isExecuteError: payload,
      };

    case SET_CLEAR_EXECUTE:
      return {
        ...state,
        isExecuteLoading: false,
        isExecuted: {},
        isExecuteError: { isError: false, message: "" },
      };
    case SET_PT_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    case SET_BEFORE_LOGIN_USER_DATA:
      return {
        ...state,
        detailDataBeforeLogin: {
          ...state.detailDataBeforeLogin,
          ...payload,
        },
      };

    case SET_IMAGE_CHECKED:
      return {
        ...state,
        detailDataBeforeLogin: {
          ...state.detailDataBeforeLogin,
          imageChecked: payload,
        },
      };
    case SET_OPEN_PREVIEW_IMAGE:
      return {
        ...state,
        openPreviweImage: {
          ...state.openPreviweImage,
          ...payload,
        },
      };
    case SET_AFTER_LOGIN_USER_DATA:
      return {
        ...state,
        detailDataAfterLogin: {
          ...state.detailDataAfterLogin,
          [payload.key]: {
            ...state.detailDataAfterLogin[payload.key],
            ...payload.payload,
          },
        },
      };
    // new cr
    case PENDINGTASK_REGIS_TEMPLATE:
      return {
        ...state,
        templateMapping: payload,
      };
    case SET_PT_BRANCH:
      return {
        ...state,
        branch: payload,
      };
    case SET_PT_SEGMENTATION:
      return {
        ...state,
        segmentation: payload,
      };
    case SET_V_MENU_PACKAGE:
      return {
        ...state,
        menuPackage: payload,
      };
    case SET_V_LIMIT_PACAGE:
      return {
        ...state,
        limitPackage: payload,
      };
    case SET_REJECTED_LIST: {
      return {
        ...state,
        rejectedList: payload,
      };
    }
    // end new cr
    default:
      return state;
  }
};
