// progressBarAction Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  INIT_DATA,
  SET_EVENT_BY_DATE,
  SET_ISSUBMIT,
  SET_EVENT,
  SET_IS_ALREADY_EXIST,
} from "stores/actions/calendarEvent";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  data: [],
  eventByDate: [],
  event: {},
  isAlreadyExist: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case INIT_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_EVENT_BY_DATE:
      return {
        ...state,
        eventByDate: payload,
      };

    case SET_ISSUBMIT:
      return {
        ...state,
        isSubmitting: payload,
      };

    case SET_EVENT:
      return {
        ...state,
        event: payload,
      };

    case SET_IS_ALREADY_EXIST:
      return {
        ...state,
        isAlreadyExist: payload,
      };

    default:
      return state;
  }
};
