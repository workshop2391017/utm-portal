// chargePackage Reducers
// --------------------------------------------------------

import { PlaylistAddOutlined } from "@material-ui/icons";
import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_INITIAL_DATA,
  SET_DETAIL_DATA,
  SET_LOADING_ECECUTE,
  SET_SUCCESS_EXECUTE,
  SET_LOADING_CURRENCY,
  SET_LOADING_TIERINGSLAB,
  SET_LOADING_SERVICES,
  SET_CURRENCY,
  SET_TIERINGSLAB,
  SET_SERVICES,
  SET_FORMDATA,
  SET_UPDATE_DETAIL,
  SET_EDIT_DATA,
  SET_CLEAR,
  SET_DETAIL_DATA_TEMP,
} from "stores/actions/chargePackage";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isLoadingExecute: false,
  isSuccessExecute: false,
  isLoadingCurrency: false,
  isLoadingTieringSlab: false,
  isLoadingServices: false,
  currency: [],
  tieringslab: [],
  services: [],
  data: {},
  detailData: {},
  error: {},
  editData: null,
  temp: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case SET_INITIAL_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL_DATA:
      return {
        ...state,
        detail: payload,
      };

    case SET_LOADING_ECECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };

    case SET_SUCCESS_EXECUTE:
      return {
        ...state,
        isSuccessExecute: payload,
      };

    case SET_LOADING_CURRENCY:
      return {
        ...state,
        isLoadingCurrency: payload,
      };
    case SET_LOADING_TIERINGSLAB:
      return {
        ...state,
        isLoadingTieringSlab: payload,
      };
    case SET_LOADING_SERVICES:
      return {
        ...state,
        isLoadingServices: payload,
      };
    case SET_CURRENCY:
      return {
        ...state,
        currency: payload,
      };
    case SET_TIERINGSLAB:
      return {
        ...state,
        tieringslab: payload,
      };
    case SET_SERVICES:
      return {
        ...state,
        services: payload,
      };

    case SET_FORMDATA:
      return {
        ...state,
        services: payload,
      };

    case SET_UPDATE_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_EDIT_DATA:
      return {
        ...state,
        editData: payload,
      };
    case SET_DETAIL_DATA_TEMP:
      return {
        ...state,
        temp: payload,
      };
    case SET_CLEAR:
      return {
        ...state,
        isLoadingExecute: false,
        isSuccessExecute: false,
        isLoadingCurrency: false,
        isLoadingTieringSlab: false,
        isLoadingServices: false,
        detailData: {},
      };
    default:
      return state;
  }
};
