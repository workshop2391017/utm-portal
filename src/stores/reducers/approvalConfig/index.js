// login Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_ALL_CONFIG_DATA,
  SET_CONIFG_ID,
  SET_ERROR_APPROVE,
  SET_CLEAR_ERROR,
  SET_APPROVAL_DETAIL,
  SET_LOADING_APPROVE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/approvalConfig";

const initialState = {
  isLoading: false,
  isLoadingSubmit: false,
  error: { isError: false, message: "" },
  errorApprove: { isError: false, message: "" },
  configId: null,
  approvalDetail: {
    newData: null,
    oldData: null,
  },
  data: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_ALL_CONFIG_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_APPROVAL_DETAIL:
      return {
        ...state,
        approvalDetail: payload,
      };
    case SET_CONIFG_ID:
      return {
        ...state,
        configId: payload,
      };
    case SET_ERROR_APPROVE:
      return {
        ...state,
        errorApprove: payload,
      };
    case SET_CLEAR_ERROR:
      return {
        ...state,
        errorApprove: { isError: false, message: "" },
        error: { isError: false, message: "" },
      };
    case SET_LOADING_APPROVE:
      return {
        ...state,
        isLoadingSubmit: payload,
      };

    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
