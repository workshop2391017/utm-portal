//  Reducers Dashboard

import {
  SET_DATA_FORM,
  SET_SEGMENTATION_FORM_ACTIVE,
  SET_ACTIVE_MENU,
  SET_MENU_DATA,
  SET_MENU_KHUSUS_DATA,
  SET_MENU_ADVANCE_DATA,
  SET_DATA_LIST,
  SET_LOADING,
  SET_ERROR,
  SET_CLEAR_ERROR,
  SET_PACKAGE_DATA,
  SET_LOADING_PACKAGE,
  SET_LOADING_DETAIL,
  SET_LOADING_INSERT,
  SET_SUCCESS_INSERT,
  SET_LOADING_UPLOAD_FILE,
  SET_UPLOAD_FILE,
  SET_CLEAR_DATA,
  SET_CHARGE_PACKAGE_TABS,
  SET_SEGMENTASI_ID,
  SET_LOADING_UNDUH,
  SET_PREVIEW_ADVANCE,
  SET_ERROR_UPLOAD,
} from "stores/actions/segmentasiNasabah";

export const menusSegmentasiCONF = {
  JENIS_PRODUK_REKENING: "Account Type",
  LIMIT_PACKAGE: "Limit Package",
  MENU_PACKGE: "Menu Package",
  CHARGE_PACKAGE: "Charge Package",
  MENU_KHUSUS: "Special Menu",
  SEGMEN_CABANG: "Branch Segment",
  UNGGAH_FILE: "Upload File",

  // for active tabs
  CHARGE_PACKAGE_TIERINGSLAB: "CHARGE_PACKAGE_TIERINGSLAB",
};

const initialMenus = {
  [menusSegmentasiCONF.JENIS_PRODUK_REKENING]: {
    label: menusSegmentasiCONF.JENIS_PRODUK_REKENING,
    subLabel: "Product segmentation account settings",
    selected: false,
    data: { jenisProductId: null },
    idKey: "jenisProductId",
  },
  [menusSegmentasiCONF.LIMIT_PACKAGE]: {
    label: menusSegmentasiCONF.LIMIT_PACKAGE,
    subLabel: "Transaction limit settings for this segment",
    selected: false,
    data: { limitPackageId: null },
    idKey: "limitPackageId",
  },
  [menusSegmentasiCONF.MENU_PACKGE]: {
    label: menusSegmentasiCONF.MENU_PACKGE,
    subLabel: "Menu settings for this segment",
    selected: false,
    data: { menuPackageId: null },
    idKey: "menuPackageId",
  },
  [menusSegmentasiCONF.CHARGE_PACKAGE]: {
    label: menusSegmentasiCONF.CHARGE_PACKAGE,
    subLabel: "Transaction charge settings for this segment",
    selected: false,
    data: { chargePackageId: null },
    idKey: "chargePackageId",
  },
};

const initialMenuKhusus = {
  [menusSegmentasiCONF.MENU_KHUSUS]: {
    label: menusSegmentasiCONF.MENU_KHUSUS,
    subLabel: "Settings certain menus that can be accessed",
    selected: false,
    data: { menuKhususId: null },
    idKey: "menuKhususId",
  },
  [menusSegmentasiCONF.SEGMEN_CABANG]: {
    label: menusSegmentasiCONF.SEGMEN_CABANG,
    subLabel: "Set the type of savings at the branch location",
    selected: false,
    data: { segmentBranchId: null },
    idKey: "segmentBranchId",
  },
};

const initialMenuAdvance = {
  [menusSegmentasiCONF.UNGGAH_FILE]: {
    label: menusSegmentasiCONF.UNGGAH_FILE,
    subLabel: "Accommodating customers outside the segment",
    selected: false,
    data: null,
  },
};

const initialState = {
  activeTabs: 1,
  segmentasiId: null,
  isLoading: false,
  formPackageActive: null,
  activeMenu: null,
  isLoadingInsert: false,
  isSuccessInsert: false,
  isLoadingUnduh: false,
  uploadFile: null,
  errorUpload: false,
  dataForm: {
    name: null,
    description: null,
  },
  isLoadingPackage: {},
  isLoadingDetail: {},
  error: { isError: false, message: "" },
  menus: initialMenus,
  menuKhusus: initialMenuKhusus,
  menuAdvance: initialMenuAdvance,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_DATA_FORM:
      return {
        ...state,
        dataForm: payload,
      };
    case SET_SEGMENTATION_FORM_ACTIVE:
      return {
        ...state,
        formPackageActive: payload,
      };

    case SET_ACTIVE_MENU: {
      return {
        ...state,
        activeMenu: payload,
      };
    }

    case SET_MENU_DATA:
      // eslint-disable-next-line no-case-declarations
      const mCurrent = state?.menus[payload.key];

      return {
        ...state,
        menus: {
          ...state.menus,
          [payload.key]: {
            ...state.menus[payload.key],
            data:
              mCurrent?.data[mCurrent?.idKey] === payload.payload?.id
                ? {
                    [mCurrent?.idKey]: null,
                  }
                : payload.payload,
          },
        },
      };

    case SET_MENU_KHUSUS_DATA:
      // eslint-disable-next-line no-case-declarations
      const mkCurrent = state?.menuKhusus[payload.key];

      return {
        ...state,
        menuKhusus: {
          ...state.menuKhusus,
          [payload.key]: {
            ...state.menuKhusus[payload.key],
            data:
              mkCurrent?.data[mkCurrent?.idKey] === payload.payload?.id
                ? {
                    [mkCurrent?.idKey]: null,
                  }
                : payload.payload,
          },
        },
      };
    case SET_MENU_ADVANCE_DATA:
      return {
        ...state,
        menuAdvance: {
          ...state.menuAdvance,
          [payload.key]: {
            ...state.menuAdvance[payload.key],
            data: payload.payload,
          },
        },
      };

    case SET_DATA_LIST:
      return {
        ...state,
        dataList: payload,
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: "" },
      };

    case SET_PACKAGE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          ...payload.payload,
        },
      };

    case SET_LOADING_PACKAGE:
      return {
        ...state,
        isLoadingPackage: {
          ...state.isLoadingPackage,
          ...payload,
        },
      };

    case SET_LOADING_DETAIL:
      return {
        ...state,
        isLoadingDetail: {
          ...state.isLoadingDetail,
          ...payload,
        },
      };

    case SET_LOADING_INSERT:
      return {
        ...state,
        isLoadingInsert: payload,
      };

    case SET_SUCCESS_INSERT:
      return {
        ...state,
        isSuccessInsert: payload,
      };

    case SET_LOADING_UPLOAD_FILE:
      return {
        ...state,
        isLoadingUploadFile: payload,
      };

    case SET_UPLOAD_FILE:
      return {
        ...state,
        uploadFile: payload,
      };

    case SET_CLEAR_DATA:
      return {
        uploadFile: null,
        ...initialState,
      };

    case SET_SEGMENTASI_ID:
      return {
        ...state,
        segmentasiId: payload,
      };

    case SET_CHARGE_PACKAGE_TABS:
      return {
        ...state,
        activeTabs: payload,
      };

    case SET_LOADING_UNDUH:
      return {
        ...state,
        isLoadingUnduh: payload,
      };

    case SET_PREVIEW_ADVANCE:
      return {
        ...state,
        isPreviewAdvance: payload,
      };

    case SET_ERROR_UPLOAD:
      return {
        ...state,
        errorUpload: payload,
      };

    default:
      return state;
  }
};
