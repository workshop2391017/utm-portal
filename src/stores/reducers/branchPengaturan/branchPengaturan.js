import {
  SET_BRANCH_PENGATURAN_TYPE,
  SET_BRANCH_PENGATURAN_UPDATE_TYPE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_LOADING,
  SET_LOADINGSYNC,
} from "stores/actions/branchPengaturan/branchPengaturan";

const initialState = {
  data: [],
  isLoading: true,
  isLoadingSync: 0,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_BRANCH_PENGATURAN_TYPE:
      return {
        ...state,
        data: payload,
      };

    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_LOADINGSYNC:
      return {
        ...state,
        isLoadingSync: payload,
      };

    default:
      return state;
  }
};
