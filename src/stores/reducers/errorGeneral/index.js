// errorGeneral Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_POP_UP_EXPIRED,
  SET_POP_UP_OFFLINE,
  SET_POP_UP_ERROR_BE,
  SET_IS_RELOADED,
  SET_ERROR_ALREADY_USED,
} from "stores/actions/errorGeneral";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: { isError: false, code: "", message: "" },
  isPopUpExpired: false,
  isPopUpOffline: false,
  isPopUpBE: false,
  isReloaded: false,
  errorAlreadyUsed: {
    isError: false,
    title: "",
    code: "",
    message: "",
    redirect: true,
  },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_IS_RELOADED:
      return {
        ...state,
        isReloaded: payload,
      };
    case SET_POP_UP_OFFLINE:
      return {
        ...state,
        isPopUpOffline: payload,
      };
    case SET_POP_UP_ERROR_BE:
      return {
        ...state,
        isPopUpBE: payload,
      };
    case SET_POP_UP_EXPIRED:
      return {
        ...state,
        isPopUpExpired: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };

    case SET_ERROR_ALREADY_USED:
      return {
        ...state,
        errorAlreadyUsed: payload,
      };

    case CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false },
      };

    case INIT_DATA:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
