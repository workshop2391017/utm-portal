// import monthOptionsData from 'assets/dummy-data/month-options.json';
import {
  SET_LOADING_DETAIL,
  SET_ACTIVITY_DETAILS,
  SET_ERROR_DETAIL,
  CLEAR_DETAIL_DATA,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/dashboard/activityDetail";

const initialState = {
  isLoading: false,
  error: { isError: false, message: "" },
  data: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING_DETAIL:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_ACTIVITY_DETAILS:
      return {
        ...state,
        ...payload,
      };
    case SET_ERROR_DETAIL:
      return {
        ...state,
        error: {
          ...state.error,
          isError: true,
          message: payload,
        },
      };
    case CLEAR_DETAIL_DATA:
      return {
        isLoading: false,
        error: { isError: false, message: "" },
        data: {},
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
