// import monthOptionsData from 'assets/dummy-data/month-options.json';
import {
  SET_LOADING,
  SET_TRANSACTION_HISTORY_PAGE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/dashboard/transactionHistoryPage";

const initialState = {
  isLoading: false,
  error: { isError: false, message: "" },
  data: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_TRANSACTION_HISTORY_PAGE:
      return {
        ...state,
        data: payload,
      };

    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    default:
      return state;
  }
};
