// import monthOptionsData from 'assets/dummy-data/month-options.json';
import {
  SET_LOADING_CHART,
  SET_TRANSACTION_CHART,
  SET_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_HANDLE_ERROR,
} from "stores/actions/dashboard/transactionChart";

const initialState = {
  isLoadingChart: false,
  error: { isError: false, message: "" },
  dataChart: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING_CHART:
      return {
        ...state,
        isLoadingChart: payload,
      };
    case SET_TRANSACTION_CHART: {
      console.warn("payload", payload);

      const data = {};
      Object.keys(payload).forEach((elm, index) => {
        // data[elm] = payload[elm][0];

        data[elm] = Object.keys(payload[elm][0]).map((elm2) => ({
          [elm2]: {
            data: {
              name: "Transaksi",
              data: payload[elm][0][elm2].map(
                ({ transactionQty }) => transactionQty || 0
              ),
            },

            categories: payload[elm][0][elm2].map(
              (elm) => elm?.transactionDate || elm?.transactionDateRange || null
            ),
          },
        }));
      });

      return {
        ...state,
        dataChart: data,
      };
    }
    case SET_ERROR:
      return {
        ...state,
        error: {
          ...state.error,
          isError: true,
          message: payload,
        },
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    default:
      return state;
  }
};
