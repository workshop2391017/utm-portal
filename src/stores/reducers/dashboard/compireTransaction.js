// import monthOptionsData from 'assets/dummy-data/month-options.json';
import {
  SET_LOADING_COMPIRE,
  SET_ERROR_COMPIRE,
  SET_DATA_COMPIRE,
  SET_HANDLE_ERROR,
  SET_HANDLE_CLEAR_ERROR,
} from "stores/actions/dashboard/compireTransaction";
import { firstLowerCase } from "utils/helpers";

const initialState = {
  isLoadingCompire: false,
  error: { isError: false, message: "" },
  dataCompire: [],
};

const calculation = (current, before) => {
  let status = "";
  let percentage = 0;
  if (before < current) {
    status = "up";
  } else status = "down";

  if (before === 0 && current > 0) {
    percentage = 100;
  } else percentage = ((current - before) / before) * 100;

  return {
    status: percentage === 0 ? undefined : status, // undefined for netral status
    percentage: `${(percentage || 0).toFixed(2)}%`, // 0 if percentage isNaN or unidentify
  };
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    // Activity history page
    case SET_LOADING_COMPIRE:
      return {
        ...state,
        isLoadingCompire: payload,
      };
    case SET_DATA_COMPIRE:
      return {
        ...state,
        dataCompire: payload.reduce((acc, curr) => {
          acc[firstLowerCase(curr.summaryType.replace(" ", ""))] = {
            ...curr,
            ...calculation(curr.amountNow, curr.amountBefore),
          };
          return acc;
        }, {}),
      };
    case SET_ERROR_COMPIRE:
      return {
        ...state,
        error: {
          ...state.error,
          isError: true,
          message: payload,
        },
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    default:
      return state;
  }
};
