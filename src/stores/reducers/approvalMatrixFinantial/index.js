// approvalMatrixFinantial Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA_SIDBAR_MENU,
  SET_DATA_DETAIL,
  SET_POP_UP,
  SET_CURRENCY_DATA,
  SET_SELECT_CURRENCY,
  SET_DATA_GROUP,
  SET_POP_UP_CURR,
  SET_DATA_DROPDOWN_APPV,
  SET_DATA_SIDEBAR_CARD,
  SET_DATA_EDIT,
  SET_LOADING_EXCUTE,
  SET_IDCOMPANY,
  SET_ALL_SERVICE,
  SET_IS_CHECK,
  SET_MENU_CHILD_ID,
  SET_SELECTED_MENU,
  SET_SELECT_ALL,
  SET_LOADING_DETAIL,
  SET_LOADING_ALL_SERVICES,
} from "stores/actions/approvalMatrixFinantial";

const initialState = {
  isLoading: false,
  isLoadingDetail: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isLoadingExcute: false,
  dataSideBar: [],
  error: {},
  dataDetail: [],
  isOpen: false,
  dataCurrency: [],
  dataSelectCurrency: null,
  dataGroup: [],
  isOpenCurr: false,
  dataGroupApprovel: [],
  dataSideBarPayload: {},
  dataEdit: null,
  allService: [],
  serviceIsCheck: [],
  idChildMenu: null,
  dataCompanyId: null,
  selectedMenu: {
    nameEn: null,
    parentId: null,
    currency: {
      value: "IDR",
      label: "Indonesian Rupiah",
    },
  },
  selectedAll: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA_SIDBAR_MENU:
      return {
        ...state,
        dataSideBar: payload,
      };
    case SET_DATA_DETAIL:
      return {
        ...state,
        dataDetail: payload,
      };
    case SET_POP_UP:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_CURRENCY_DATA:
      return {
        ...state,
        dataCurrency: payload,
      };

    // case SET_SELECT_CURRENCY:
    //   return {
    //     ...state,
    //     dataSelectCurrency: payload,
    //     isOpenCurr: false,
    //   };

    case SET_DATA_GROUP:
      return {
        ...state,
        dataGroup: payload,
      };

    case SET_POP_UP_CURR:
      return {
        ...state,
        isOpenCurr: payload,
      };

    case SET_DATA_DROPDOWN_APPV:
      return {
        ...state,
        dataGroupApprovel: payload,
      };

    case SET_DATA_SIDEBAR_CARD:
      return {
        ...state,
        dataSideBarPayload: payload,
      };

    case SET_DATA_EDIT:
      return {
        ...state,
        dataEdit: payload,
      };

    case SET_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };
    case SET_IDCOMPANY:
      return {
        ...state,
        dataCompanyId: payload,
      };
    case SET_ALL_SERVICE:
      return {
        ...state,
        allService: payload,
      };
    case SET_IS_CHECK:
      return {
        ...state,
        serviceIsCheck: payload,
      };
    case SET_MENU_CHILD_ID:
      return {
        ...state,
        idChildMenu: payload,
      };

    case SET_SELECTED_MENU:
      return {
        ...state,
        selectedMenu: payload,
      };

    case SET_SELECT_ALL:
      return {
        ...state,
        selectedAll: payload,
      };

    case SET_LOADING_DETAIL:
      return {
        ...state,
        isLoadingDetail: payload,
      };

    case SET_LOADING_ALL_SERVICES:
      return {
        ...state,
        isLoadingAllServies: payload,
      };
    default:
      return state;
  }
};
