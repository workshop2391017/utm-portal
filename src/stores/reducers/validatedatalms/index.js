// validatedatalms Reducers
// --------------------------------------------------------

import {
  SET_IS_ALREADY_EXIST,
  SET_LOADING_VALIDATE,
  SET_LOADING_VALIDATE_BY_FIELD,
  SET_CLEAR_ALREADY_EXIST,
} from "stores/actions/validatedatalms";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  isAlreadyExist: { name: false, code: false },
  error: {},
  isLoadingValidate: false,
  isLoadingByField: {},
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_IS_ALREADY_EXIST:
      return {
        ...state,
        isAlreadyExist: {
          ...state.isAlreadyExist,
          ...payload,
        },
      };

    case SET_LOADING_VALIDATE:
      return {
        ...state,
        isLoadingValidate: payload,
      };

    case SET_LOADING_VALIDATE_BY_FIELD:
      return {
        ...state,
        isLoadingByField: {
          ...state.isLoadingByField,
          ...payload,
        },
      };

    case SET_CLEAR_ALREADY_EXIST:
      return { ...initialState };

    default:
      return state;
  }
};
