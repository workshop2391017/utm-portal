// tieringSlab Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_DETAIL,
  SUCCESS_CONFIRMATION,
  SET_DATA_ID,
} from "stores/actions/tieringSlab";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  data: {},
  detail: [],
  isOpen: false,
  id: null,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };

    case SET_DATA_DETAIL: {
      return {
        ...state,
        detail: payload,
      };
    }
    case SET_DATA_ID: {
      return {
        ...state,
        id: payload,
      };
    }

    case SUCCESS_CONFIRMATION:
      return {
        ...state,
        isOpen: payload,
      };
    default:
      return state;
  }
};
