// errorGeneral Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  SET_DATA,
  SET_ERROR,
  SET_HANDLE_CLEAR_ERROR,
  SET_HANDLE_ERROR,
  SET_DETAIL,
  SET_ID,
  SET_LOADING_EXECUTE,
  SET_SUCCESS_EXECUTE,
  SET_CLEAR,
  SET_PRODUCT_REKENING,
  SET_ACCOUNT_TYPE,
  SET_LOADING_PRODUCT_ACCOUNTTYPE,
  SET_PRODUCT_ACCOUNTTYPE,
} from "stores/actions/jenisRekeningProduk";

const initialState = {
  isLoading: false,
  isLoadingExecute: false,
  isSuccessExecute: false,
  productAccountType: {},
  loadingProductAccountType: false,
  error: { isError: false, message: "" },
  data: {},
  detail: null,
  accountType: null,
  productRekening: null,
  id: null,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };

    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_HANDLE_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };

    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };

    case SET_ID:
      return {
        ...state,
        id: payload,
      };

    case SET_LOADING_EXECUTE:
      return {
        ...state,
        isLoadingExecute: payload,
      };

    case SET_SUCCESS_EXECUTE:
      return {
        ...state,
        isSuccessExecute: payload,
      };

    case SET_PRODUCT_REKENING:
      return {
        ...state,
        productRekening: payload,
      };

    case SET_ACCOUNT_TYPE:
      return {
        ...state,
        accountType: payload,
      };

    case SET_CLEAR:
      return {
        ...state,
        isSuccessExecute: false,
        isLoadingExecute: false,
        isLoading: false,
        error: { isError: false, message: null },
        detail: null,
      };

    case SET_LOADING_PRODUCT_ACCOUNTTYPE:
      return {
        ...state,
        loadingProductAccountType: payload,
      };

    case SET_PRODUCT_ACCOUNTTYPE:
      return {
        ...state,
        productAccountType: payload,
      };

    default:
      return state;
  }
};
