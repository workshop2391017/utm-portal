// roleUser Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  SET_DATA,
  SET_DATA_USER_ROLE,
  SET_DATA_USER_DETAIL,
  SET_SAVE_PAYLOAD,
  SET_DATA_LIST_MENU,
  SET_HANDLE_CLEAR_ERROR,
  SET_PAGES,
  SET_MENU_SELECTED,
} from "stores/actions/roleUser";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: { isError: false, message: "" },
  data: [],
  dataUserRole: [],
  dataUserDetails: {},
  dataPayload: {},
  dataListMenu: {},
  pages: 0,
  menuSelected: "",
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: { isError: true, message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    case INIT_DATA:
      return {
        ...initialState,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DATA_USER_ROLE:
      return {
        ...state,
        dataUserRole: payload,
      };
    case SET_DATA_USER_DETAIL:
      return {
        ...state,
        dataUserDetails: payload,
      };
    case SET_SAVE_PAYLOAD:
      return {
        ...state,
        dataPayload: payload,
      };

    case SET_DATA_LIST_MENU:
      return {
        ...state,
        dataListMenu: payload,
      };
    case SET_HANDLE_CLEAR_ERROR:
      return {
        ...state,
        error: { isError: false, message: null },
      };
    case SET_PAGES:
      return {
        ...state,
        pages: payload,
      };
    case SET_MENU_SELECTED:
      return {
        ...state,
        menuSelected: payload,
      };
    default:
      return state;
  }
};
