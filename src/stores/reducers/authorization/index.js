import { SET_PAGE_PRIVILEGE } from "stores/actions/authorization";

const initialState = {
  access: null,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_PAGE_PRIVILEGE:
      return {
        ...state,
        access: payload,
      };
    default:
      return state;
  }
};
