import {
  SET_HOST_ERROR_MAPPING_TYPE,
  SET_HOST_ERROR_MAPPING_LOADING,
  SET_HOST_ERROR_MAPPING_ERROR,
  SET_HOST_ERROR_MAPPING_CLEAR_ERROR,
  SET_HOST_ERROR_MAPPING_SUCCESS,
  SET_HOST_ERROR_MAPPING_DELETE_CONFIRM,
  SET_HOST_ERROR_MAPPING_LOADING_EXCUTE,
  SET_HOST_ERROR_MAPPING_OPEN_MODAL,
  SET_HOST_ERROR_MAPPING_OPEN_DELETE,
} from "stores/actions/hostErrorMapping";

const initialState = {
  data: [],
  isLoading: false,
  isLoadingExcute: false,
  error: {},
  isOpen: false,
  isDelete: false,
  openModal: false,
  isOpenDelete: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_HOST_ERROR_MAPPING_TYPE:
      return {
        ...state,
        data: payload,
      };

    case SET_HOST_ERROR_MAPPING_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case SET_HOST_ERROR_MAPPING_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };

    case SET_HOST_ERROR_MAPPING_CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };

    case SET_HOST_ERROR_MAPPING_SUCCESS:
      return {
        ...state,
        isOpen: payload,
      };
    case SET_HOST_ERROR_MAPPING_DELETE_CONFIRM:
      return {
        ...state,
        isDelete: payload,
      };

    case SET_HOST_ERROR_MAPPING_LOADING_EXCUTE:
      return {
        ...state,
        isLoadingExcute: payload,
      };

    case SET_HOST_ERROR_MAPPING_OPEN_MODAL:
      return {
        ...state,
        openModal: payload,
      };

    case SET_HOST_ERROR_MAPPING_OPEN_DELETE:
      return {
        ...state,
        isOpenDelete: payload,
      };

    default:
      return state;
  }
};
