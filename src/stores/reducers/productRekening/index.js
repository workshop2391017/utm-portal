// productRekening Reducers
// --------------------------------------------------------

import {
  SET_LOADING,
  CLEAR_ERROR,
  SET_ERROR,
  INIT_DATA,
  SET_DOUBLE_SUBMIT,
  PRODUCT_ACCUNT_TYPE,
  ACCOUNT_PRODUCT,
  ADD_DATA,
  ACCOUNT_PRODUCT_TEMP,
  SET_LOADINGSUBMIT,
} from "stores/actions/productRekening";

const initialState = {
  isLoading: false,
  isDoubleSubmit: false,
  isSubmitting: false,
  error: {},
  accountTypeList: [],
  accountProducts: [],
  accountProductsTemp: [],
  openSucccess: false,
  isLoadingSubmit: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_DOUBLE_SUBMIT:
      return {
        ...state,
        isDoubleSubmit: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: {
          isError: true,
          message: payload,
        },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {
          isError: false,
          message: null,
        },
      };
    case INIT_DATA:
      return {
        ...initialState,
      };

    case PRODUCT_ACCUNT_TYPE:
      return {
        ...state,
        accountTypeList: payload,
      };

    case ACCOUNT_PRODUCT:
      return {
        ...state,
        accountProducts: payload,
      };

    case ADD_DATA: {
      return {
        ...state,
        openSucccess: payload,
      };
    }

    case ACCOUNT_PRODUCT_TEMP:
      return {
        ...state,
        accountProductsTemp: payload,
      };

    case SET_LOADINGSUBMIT:
      return {
        ...state,
        isLoadingSubmit: payload,
      };
    default:
      return state;
  }
};
