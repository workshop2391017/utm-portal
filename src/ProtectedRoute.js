import React from "react";
import { Redirect, Route } from "react-router-dom";
import { getLocalStorage } from "utils/helpers";

const isAuthenticated = () => {
  const lsData = getLocalStorage("portalUserLogin");

  const userData = lsData ? JSON.parse(lsData) : null;
  return userData;
};

const ProtectedRoute = ({ children, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      !isAuthenticated() ? <Redirect to="/portal/dashboard" /> : children
    }
  />
);

export default ProtectedRoute;
