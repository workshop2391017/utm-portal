# Use an official Node.js runtime as a base image for building
FROM node:16-alpine AS builder

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json or yarn.lock to the working directory
COPY package*.json ./

# Install the app dependencies
RUN npm install --legacy-peer-deps

# Copy the rest of the application code to the working directory
COPY . .

# Build the React app for production
RUN npm run build

# Use an official Nginx runtime as a base image for serving the built files
FROM nginx:alpine

# Set the working directory in the container
WORKDIR /usr/share/nginx/html

# Copy the built assets from the builder stage to the Nginx web root
COPY --from=builder /usr/src/app/build .


# Command to run Nginx
CMD ["nginx", "-g", "daemon off;"]