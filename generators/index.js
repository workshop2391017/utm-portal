const { exec } = require('child_process');
const storesGenerator = require('./stores/index.js');

module.exports = (plop) => {
  plop.setGenerator('stores', storesGenerator);
  plop.setHelper(
    'prefix-component-style-name',
    (folderName) => `${folderName.toLowerCase().substring(0, 1)}`
  );
  plop.setActionType('lint:fix', () => exec(`yarn lint:fix`));
};
